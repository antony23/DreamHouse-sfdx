<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Assegnazione_incarico_template</fullName>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Assegnazione incarico</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_commerciale</template>
    </alerts>
    <alerts>
        <fullName>Avvisa_Bellantone</fullName>
        <ccEmails>bellantone@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Avvisa Bellantone</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Bellantone</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_per_Incarico_a_Buratta</fullName>
        <ccEmails>buratta.est@edidomus.it</ccEmails>
        <ccEmails>frigerio@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a Buratta</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Buratta</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_per_Incarico_a_Galli</fullName>
        <ccEmails>galli.est@edidomus.it</ccEmails>
        <ccEmails>frigerio@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a Galli</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Galli</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_per_Incarico_a_IBI</fullName>
        <ccEmails>commerciale@bbb.srl</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <ccEmails>bellantone@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a IBI</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_IBI</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_per_Incarico_a_Palladino</fullName>
        <ccEmails>palladino.est@edidomus.it</ccEmails>
        <ccEmails>bellantone@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a Palladino</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Palladino</template>
    </alerts>
    <alerts>
        <fullName>Invio_email_per_Incarico_a_Paon</fullName>
        <ccEmails>mpaon@infocarnet.net</ccEmails>
        <ccEmails>bellantone@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a Paon</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Paon</template>
    </alerts>
    <alerts>
        <fullName>Professional_Avvisa_Frigerio</fullName>
        <ccEmails>frigerio@edidomus.it</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Professional - Avvisa Frigerio</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_Frigerio</template>
    </alerts>
    <alerts>
        <fullName>Professional_Invio_email_per_Incarico_a_IBI</fullName>
        <ccEmails>commerciale@bbb.srl</ccEmails>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <ccEmails>bellantone@edidomus.it</ccEmails>
        <ccEmails>deprezzo@edidomus.it</ccEmails>
        <description>Professional - Invio email per Incarico a IBI</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Incarico_Notifica_a_IBI</template>
    </alerts>
    <fieldUpdates>
        <fullName>RagioneSociale</fullName>
        <field>Ragione_Sociale__c</field>
        <formula>Account.Ragione_Sociale__c</formula>
        <name>Ragione Sociale</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Aggiungi Ragione Sociale in Operazione</fullName>
        <actions>
            <name>RagioneSociale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Creazione Operazione Interesse Futuro</fullName>
        <actions>
            <name>ChiamataInteressato</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Data_Interesse_Futuro__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Incarico - Notifica a Bellantone</fullName>
        <actions>
            <name>Avvisa_Bellantone</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Andrea Zucchi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Manuel Tito</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Michele Paon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Emilia Palladino</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>B srl</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>De Prezzo</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Incarico - Notifica a Frigerio</fullName>
        <actions>
            <name>Professional_Avvisa_Frigerio</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Pantaleo De Vita</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Lorenzo Buratta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Bruno Galli</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>De Prezzo</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Invio mail per incarico</fullName>
        <actions>
            <name>Assegnazione_incarico_template</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 AND 2) OR (3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10 AND 11))</booleanFilter>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
            <value>De Prezzo</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Andrea Zucchi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Manuel Tito</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Emilia Palladino</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Michele Paon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>B srl</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Pantaleo De Vita</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Lorenzo Buratta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>notEqual</operation>
            <value>Bruno Galli</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
            <value>Domus</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Invio mail per incarico a IBI</fullName>
        <actions>
            <name>Invio_email_per_Incarico_a_IBI</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(AgenteCommerciale__r.Name = &quot;I.B.I. SRL&quot;, AgenteCommerciale__r.Name = &quot;B di Cosimo Barberini&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Invio mail per incarico a PALLADINO</fullName>
        <actions>
            <name>Invio_email_per_Incarico_a_Palladino</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AgenteCommerciale__r.Name  = &quot;PALLADINO EMILIA&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Invio mail per incarico a PAON</fullName>
        <actions>
            <name>Invio_email_per_Incarico_a_Paon</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AgenteCommerciale__r.Name  = &quot;PAON MICHELE&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>QP - Invia task ad Agente esterno</fullName>
        <active>false</active>
        <formula>AgenteCommerciale__c  =  &apos;Licciardello Roberto&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>ChiamataInteressato</fullName>
        <assignedToType>owner</assignedToType>
        <description>Operazione Generata Automaticamente per  &quot;Interesse Futuro&quot; dichiarato</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Task.Data_Interesse_Futuro__c</offsetFromField>
        <priority>Alta</priority>
        <protected>false</protected>
        <status>Richiamare</status>
        <subject>Chiamata Interessato</subject>
    </tasks>
</Workflow>
