<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Invia_email_a_Commerciale</fullName>
        <ccEmails>licciardello@edidomus.it</ccEmails>
        <description>Invia email al Commerciale del Lead</description>
        <protected>false</protected>
        <recipients>
            <field>Commerciale__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>bdc@edidomus.it</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Template_Professional/Notifica_DEM</template>
    </alerts>
    <alerts>
        <fullName>Mail_conferma_Richiesta_Commerciale</fullName>
        <description>Mail conferma Richiesta Commerciale</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Template_Professional/Lead_notifica_utente</template>
    </alerts>
    <alerts>
        <fullName>Professional_Notifica_Lead_a_BDC</fullName>
        <description>Professional - Notifica Lead a BDC</description>
        <protected>false</protected>
        <recipients>
            <recipient>deprezzo@edidomus.it</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>licciardello_prof@edidomus.it</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Professional/Notifica_Lead_a_BDC</template>
    </alerts>
    <fieldUpdates>
        <fullName>Data_Consenso_Lead</fullName>
        <field>Data_Inserimento_Consensi__c</field>
        <formula>TODAY()</formula>
        <name>Data Consenso Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Data Inserimento Privacy Lead</fullName>
        <actions>
            <name>Data_Consenso_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Trattamento_Dati__c</field>
            <operation>equals</operation>
            <value>SI,NO</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Invio mail a BDC per Lead</fullName>
        <actions>
            <name>Professional_Notifica_Lead_a_BDC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
		<criteriaItems>
            <field>Lead.Codice_Agente__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Professional - Lead - Conferma registrazione dati</fullName>
        <actions>
            <name>Mail_conferma_Richiesta_Commerciale</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.FirstName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Registrazione Lead da Web</fullName>
        <actions>
            <name>Invia_email_a_Commerciale</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Commerciale__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Se il Lead proviene da una form web invia una mail al proprietario della form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
