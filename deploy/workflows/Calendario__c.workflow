<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AggiornaData</fullName>
        <field>Data_Uscita_Attesa__c</field>
        <formula>Data_Uscita_Effettiva__c</formula>
        <name>AggiornaData</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Aggiorna Data uscita</fullName>
        <actions>
            <name>AggiornaData</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Data_Uscita_Effettiva__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
