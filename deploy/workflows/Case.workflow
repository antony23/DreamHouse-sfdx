<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Invia_email_al_Contatto_del_Case</fullName>
        <description>Invia email al Contatto del Case</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@edidomus.it</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Template_Professional/Registrazione_Case</template>
    </alerts>
    <alerts>
        <fullName>Notifica_Sollecito_Caso</fullName>
        <description>Notifica Sollecito Caso</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Case/Sollecito_Caso</template>
    </alerts>
    <alerts>
        <fullName>Send_email_out_of_office</fullName>
        <description>Invia email di out-of-office</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@edidomus.it</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EdiDomus_Out_of_office</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_status_Nuovo</fullName>
        <field>Status</field>
        <literalValue>Nuovo</literalValue>
        <name>Case status Nuovo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Category_Unfiled</fullName>
        <field>Categoria__c</field>
        <literalValue>Non Categorizzato</literalValue>
        <name>Category Unfiled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MacroCategoria_B2C</fullName>
        <field>Macro_Categoria__c</field>
        <literalValue>B2C</literalValue>
        <name>Macro-Categoria B2C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Macro_Categoria_non_classificata</fullName>
        <field>Macro_Categoria__c</field>
        <literalValue>Non Classificata</literalValue>
        <name>Macro-Categoria non classificata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modulo_1_non_disponibile</fullName>
        <field>Modulo_Prodotto_1__c</field>
        <formula>&apos;Non disponibile&apos;</formula>
        <name>Modulo 1 non disponibile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modulo_2_non_disponibile</fullName>
        <field>Modulo_Prodotto_2__c</field>
        <formula>&apos;Non disponibile&apos;</formula>
        <name>Modulo 2 non disponibile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_In_lavorazione</fullName>
        <field>Status</field>
        <literalValue>In lavorazione</literalValue>
        <name>Status In lavorazione</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>B2C modulo non disponibile</fullName>
        <actions>
            <name>Modulo_1_non_disponibile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Modulo_2_non_disponibile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Macro_Categoria__c</field>
            <operation>equals</operation>
            <value>B2C</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case owner User</fullName>
        <actions>
            <name>Status_In_lavorazione</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>CC Web,CC Estero,CC Eventi,CC Vendite Dirette,CC Abbonamenti,Ufficio Amministrazione Abb. e V.D.,Ufficio Marketing Abb. e V.D.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Creazione automatica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Categoria B2C se case da FB</fullName>
        <actions>
            <name>MacroCategoria_B2C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Facebook</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EdiDomus - Out of office</fullName>
        <actions>
            <name>Send_email_out_of_office</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nuovo</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CC Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Macro-Categoria non classificata</fullName>
        <actions>
            <name>Macro_Categoria_non_classificata</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Altro Social Network,Facebook</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Owner coda</fullName>
        <actions>
            <name>Case_status_Nuovo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CC Web,CC Estero,CC Eventi,CC Vendite Dirette,CC Abbonamenti,Ufficio Amministrazione Abb. e V.D.,Ufficio Marketing Abb. e V.D.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Creazione automatica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsEscalated</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registrazione Case da Web%2FEmail</fullName>
        <actions>
            <name>Invia_email_al_Contatto_del_Case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web/APP,Email</value>
        </criteriaItems>
        <description>Se il Case è di tipo Professional e proviene da Web o da Email invia una mail all&apos;email del case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sollecito Caso</fullName>
        <actions>
            <name>Notifica_Sollecito_Caso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Numero_Solleciti__c &gt; 0 &amp;&amp; (  Numero_Solleciti__c &gt; PRIORVALUE(Numero_Solleciti__c)  ||  ISBLANK(PRIORVALUE(Numero_Solleciti__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
