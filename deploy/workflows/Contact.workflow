<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notifica_variazione_anagrafica_Inglese</fullName>
        <description>Notifica variazione anagrafica Inglese</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Revisione_template/Notifica_variazione_anagrafica_Inglese_New2</template>
    </alerts>
    <alerts>
        <fullName>Notifica_variazione_anagrafica_Italiano</fullName>
        <description>Notifica variazione anagrafica Italiano</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Revisione_template/Notifica_variazione_anagrafica_Italiano_New2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Anagrafica_inviata_a_sap</fullName>
        <field>Invia_anagrafica_a_sap__c</field>
        <literalValue>0</literalValue>
        <name>Anagrafica inviata a sap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Data_Aggiornamento_Contact</fullName>
        <field>Data_di_Aggiornamento__c</field>
        <formula>Today()</formula>
        <name>Data Aggiornamento Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Data_Inserimento</fullName>
        <field>Data_Inserimento__c</field>
        <formula>Today()</formula>
        <name>Data Inserimento</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Flag</fullName>
        <description>Reset Invia anagrafica a Sap</description>
        <field>Invia_anagrafica_a_sap__c</field>
        <literalValue>0</literalValue>
        <name>Reset Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Invia_anagrafica_a_SAP</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://esb.edidomus.it/services/createAnagraficaSAP</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>gbarberio.deloitte@edidomus.it</integrationUser>
        <name>Invia anagrafica a SAP</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Data Inserimento Privacy</fullName>
        <actions>
            <name>Data_Inserimento</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( (NOT ((ISNULL( TEXT (Trattamento_Dati__c))))),  Creato_da_Lead__c  = false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Invia anagrafica a SAP</fullName>
        <actions>
            <name>Reset_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Invia_anagrafica_a_SAP</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Invia_anagrafica_a_sap__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notifica variazione anagrafica Inglese</fullName>
        <actions>
            <name>Notifica_variazione_anagrafica_Inglese</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( 	NOT(ISBLANK(Email)), 	Account.RecordType.DeveloperName != &apos;B2BLarge&apos;, 	Codice_Nazione__c != &apos;IT&apos;, 	OR( 		ISCHANGED(Periodo_Ultima_Modifca_Temp_Indirizzo__c), 		ISCHANGED(Codice_Nazione__c ), 		ISCHANGED(MailingCountry ), 		ISCHANGED(Nazione__c), 		ISCHANGED(Cee_Nazione__c), 		ISCHANGED(Area_Nazione__c), 		ISCHANGED(Id_Area_Nazione__c), 		ISCHANGED(Codice_Provincia__c), 		ISCHANGED(MailingState), 		ISCHANGED(MailingCity), 		ISCHANGED(Citta_abbreviato__c), 		ISCHANGED(MailingPostalCode), 		ISCHANGED(Supplemento_CAP__c), 		ISCHANGED(Dug__c), 		ISCHANGED(DugAbbreviataStd__c), 		ISCHANGED(Via__c), 		ISCHANGED(Civico__c), 		ISCHANGED(Supplemento_Civico__c), 		ISCHANGED(MailingStreet), 		ISCHANGED(Indirizzo_Forzato__c), 		ISCHANGED(Via_Multicap__c), 		ISCHANGED(Presso__c), 		ISCHANGED(Frazione__c) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notifica variazione anagrafica Italiano</fullName>
        <actions>
            <name>Notifica_variazione_anagrafica_Italiano</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( 	NOT(ISBLANK(Email)), 	Account.RecordType.DeveloperName != &apos;B2BLarge&apos;, 	Codice_Nazione__c = &apos;IT&apos;, 	OR( 		ISCHANGED(Periodo_Ultima_Modifca_Temp_Indirizzo__c), 		ISCHANGED(Codice_Nazione__c ), 		ISCHANGED(MailingCountry ), 		ISCHANGED(Nazione__c), 		ISCHANGED(Cee_Nazione__c), 		ISCHANGED(Area_Nazione__c), 		ISCHANGED(Id_Area_Nazione__c), 		ISCHANGED(Codice_Provincia__c), 		ISCHANGED(MailingState), 		ISCHANGED(MailingCity), 		ISCHANGED(Citta_abbreviato__c), 		ISCHANGED(MailingPostalCode), 		ISCHANGED(Supplemento_CAP__c), 		ISCHANGED(Dug__c), 		ISCHANGED(DugAbbreviataStd__c), 		ISCHANGED(Via__c), 		ISCHANGED(Civico__c), 		ISCHANGED(Supplemento_Civico__c), 		ISCHANGED(MailingStreet), 		ISCHANGED(Indirizzo_Forzato__c), 		ISCHANGED(Via_Multicap__c), 		ISCHANGED(Presso__c), 		ISCHANGED(Frazione__c) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Privacy Contact</fullName>
        <actions>
            <name>Data_Aggiornamento_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(( ISNEW() || ISCHANGED(  Versione_Informativa__c  ) || ISCHANGED( Comunicazioni_Commerciali_Marketing__c ) || ISCHANGED( Profilazione__c ) || ISCHANGED( Trattamento_Dati__c ) || ISCHANGED( Cessione_a_Terzi__c )  ||  ISCHANGED(  Comunicazioni_Commerciali_Indirette__c  )  || ISCHANGED(  Altro_Consenso__c  )), ( OR(TEXT(Fonte__c) = &apos;Email&apos;, TEXT(Fonte__c) = &apos;Telefono&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
