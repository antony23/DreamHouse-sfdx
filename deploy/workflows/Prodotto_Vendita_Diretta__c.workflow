<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update</fullName>
        <field>Gruppo_Contabilizzazione_Prodotto__c</field>
        <literalValue>21D</literalValue>
        <name>Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Tassazione Prodotti Digitali</fullName>
        <actions>
            <name>Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Prodotto_Vendita_Diretta__c.Tipologia_Prodotto__c</field>
            <operation>equals</operation>
            <value>Servizi_Digitali</value>
        </criteriaItems>
        <criteriaItems>
            <field>Prodotto_Vendita_Diretta__c.Gruppo_Contabilizzazione_Prodotto__c</field>
            <operation>equals</operation>
            <value>21</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
