<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Estrazione_fascettario</fullName>
        <apiVersion>38.0</apiVersion>
        <endpointUrl>https://esb.edidomus.it/services/FascettarioSOAP</endpointUrl>
        <fields>Copia_uscita__c</fields>
        <fields>Data_uscita__c</fields>
        <fields>FTP__c</fields>
        <fields>Fascettari_multipli_per__c</fields>
        <fields>Fascettario__c</fields>
        <fields>Fresco_Arretrato__c</fields>
        <fields>Id</fields>
        <fields>Stato__c</fields>
        <fields>Suddivisione_geografica__c</fields>
        <fields>Testata__c</fields>
        <fields>Tipo_Consegna__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>gbarberio.deloitte@edidomus.it</integrationUser>
        <name>Estrazione fascettario</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Estrazione fascettario</fullName>
        <actions>
            <name>Estrazione_fascettario</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Estrazione_Fascettario__c.Stato__c</field>
            <operation>equals</operation>
            <value>In Attesa Estrazione</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
