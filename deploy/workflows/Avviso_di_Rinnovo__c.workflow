<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Send_Sollecito</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://esb.edidomus.it/services/commitSollecito</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>ams@edidomus.it</integrationUser>
        <name>Send Sollecito</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Send Sollecito</fullName>
        <actions>
            <name>Send_Sollecito</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Avviso_di_Rinnovo__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Avviso di sollecito</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
