<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_mov_stock_record_type_inviata</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Movimentazione_Inviata</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set mov stock record type inviata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_mov_stock_record_type_non_inviata</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Movimentazione_da_inviare</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set mov stock record type non inviata</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Record type movimentazione inviata</fullName>
        <actions>
            <name>Set_mov_stock_record_type_inviata</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Movimentazione_Stock__c.Stato__c</field>
            <operation>equals</operation>
            <value>Trasmesso,Errore di trasmissione cliente,Errore di trasmissione movimentazione,Recepito da SAP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record type movimentazione non inviata</fullName>
        <actions>
            <name>Set_mov_stock_record_type_non_inviata</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Movimentazione_Stock__c.Stato__c</field>
            <operation>equals</operation>
            <value>Nuovo,Da Trasmettere</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
