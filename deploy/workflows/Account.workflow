<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Prova_Email</fullName>
        <description>Prova Email Compleanno</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Template_Campagne/Template_di_prova</template>
    </alerts>
    <fieldUpdates>
        <fullName>Aggiorna_Format_Prossima_Attivit</fullName>
        <field>Prossima_Attivit__c</field>
        <formula>MID( Prossima_Attivit__c , 9, 2)&amp;&quot;/&quot;&amp;MID( Prossima_Attivit__c , 6, 2)&amp;&quot;/&quot;&amp;MID( Prossima_Attivit__c , 1, 4)</formula>
        <name>Aggiorna Format Prossima Attività</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Data_Aggiornamento</fullName>
        <field>Data_di_Aggiornamento__pc</field>
        <formula>Today()</formula>
        <name>Data Aggiornamento</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Data_Ultimo_Aggiornamento</fullName>
        <field>Data_di_Aggiornamento__c</field>
        <formula>Today ()</formula>
        <name>Data Ultimo Aggiornamento</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ImpostaFonteNominativo</fullName>
        <description>Per i clienti attivi la fonte nominativo deve essere impostata a In Portafoglio</description>
        <field>Fonte_nominativo__c</field>
        <literalValue>In portafoglio</literalValue>
        <name>Imposta Fonte Nominativo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ImpostaFonteNominativo2</fullName>
        <description>Per clienti di tipo Prospect profilato la fonte nominativo deve essere impostata a Liste qualificate</description>
        <field>Fonte_nominativo__c</field>
        <literalValue>Liste qualificate</literalValue>
        <name>Imposta Fonte Nominativo 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rendi_NULL_campo_Prossima_Attivit</fullName>
        <field>Prossima_Attivit__c</field>
        <name>Rendi NULL campo &quot;Prossima Attività&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Privacy</fullName>
        <field>Data_Aggiornamento__c</field>
        <formula>Today()</formula>
        <name>Update Privacy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>B2BLarge</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_B2B_Small</fullName>
        <field>RecordTypeId</field>
        <lookupValue>B2BSmall</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type B2B Small</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_altra_email_contact</fullName>
        <field>Altra_email_referente__pc</field>
        <formula>Altra_email__c</formula>
        <name>update altra email contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_cellulare_contact</fullName>
        <field>PersonMobilePhone</field>
        <formula>Cellulare__c</formula>
        <name>update cellulare contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_email_contact</fullName>
        <field>PersonEmail</field>
        <formula>IF(Email__c != null, Email__c, 

IF (Email_secondaria__c != null, Email_secondaria__c, 

IF( Altra_email__c != null,  Altra_email__c, &apos;&apos;)))</formula>
        <name>update email contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_email_secondaria_contact</fullName>
        <field>Email_secondaria_referente__pc</field>
        <formula>Email_secondaria__c</formula>
        <name>update email secondaria contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Happy Birthday</fullName>
        <actions>
            <name>Prova_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Data_di_nascita__c =TODAY()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Imposta Fonte per Clienti Attivi</fullName>
        <actions>
            <name>ImpostaFonteNominativo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Tipologia_cliente__c</field>
            <operation>equals</operation>
            <value>Cliente attivo</value>
        </criteriaItems>
        <description>Se il tipo cliente è Cliente Attivo, allora la Fonte Nominativo deve essere valorizzata con In Portafoglio</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Imposta Fonte per Prospect profilati</fullName>
        <actions>
            <name>ImpostaFonteNominativo2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Tipologia_cliente__c</field>
            <operation>equals</operation>
            <value>Prospect profilato</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Automobili,Trovolavoro</value>
        </criteriaItems>
        <description>Se il tipo cliente è Prospect profilato, allora la Fonte Nominativo deve essere valorizzata con Liste qualificate</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Modifica Format Prossima Attività</fullName>
        <actions>
            <name>Aggiorna_Format_Prossima_Attivit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( $RecordType.Id =&apos;012200000000aea&apos;,$RecordType.Id =&apos;0122000000014FE&apos;) , MID( Prossima_Attivit__c , 5, 1) =&quot;-&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Record Type B2B Large</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Tipo_di_record_account__c</field>
            <operation>equals</operation>
            <value>B2B Large Enterprise</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Record Type B2B Small</fullName>
        <actions>
            <name>Update_Record_Type_B2B_Small</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Tipo_di_record_account__c</field>
            <operation>equals</operation>
            <value>B2B Small Business</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Privacy</fullName>
        <actions>
            <name>Data_Ultimo_Aggiornamento</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Comunicazioni_Commerciali_Marketing__c )  ||  ISCHANGED( Profilazione__c )  ||  ISCHANGED( Trattamento_Dati__c ) ||  ISCHANGED( Cessione_a_Terzi__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update email contact</fullName>
        <active>true</active>
        <formula>ISCHANGED( Email__c ) ||  ISCHANGED( Email_secondaria__c ) ||  ISCHANGED( Altra_email__c ) ||   ISCHANGED( Cellulare__c ) ||  ISCHANGED( PEC__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
