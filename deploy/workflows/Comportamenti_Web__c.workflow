<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Record_Type_Fruizioni</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Fruizione_Beni_Digitali</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Fruizioni</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Interazioni_Web</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Interazioni_Web</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Interazioni Web</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Newsletter</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Newsletter</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Newsletter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Comportamento Fruizioni</fullName>
        <actions>
            <name>Update_Record_Type_Fruizioni</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Comportamenti_Web__c.Tipo_di_record__c</field>
            <operation>equals</operation>
            <value>Fruizioni Beni Digitali</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Record Type Integrazioni Web</fullName>
        <actions>
            <name>Update_Record_Type_Interazioni_Web</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Comportamenti_Web__c.Tipo_di_record__c</field>
            <operation>equals</operation>
            <value>Integrazioni Web</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Record Type Newsletter</fullName>
        <actions>
            <name>Update_Record_Type_Newsletter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Comportamenti_Web__c.Tipo_di_record__c</field>
            <operation>equals</operation>
            <value>Newsletter</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
