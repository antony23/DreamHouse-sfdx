<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Cambia_nome_opty</fullName>
        <field>Name</field>
        <formula>Account.Ragione_Sociale__c  &amp; &apos;_&apos; &amp; 
TEXT(DAY(TODAY())) &amp; &apos;/&apos; &amp;
TEXT(MONTH(TODAY())) &amp; &apos;/&apos; &amp;
TEXT(YEAR(TODAY()))</formula>
        <name>Cambia nome opty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Imposta_Nome</fullName>
        <field>Name</field>
        <formula>Account.Name &amp; &apos;-&apos; &amp;  TEXT(Prodotto__c) &amp; &apos;-&apos; &amp;  TEXT(TODAY())</formula>
        <name>Imposta Nome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cambia nome opportunità</fullName>
        <actions>
            <name>Cambia_nome_opty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the name of opportunity with Ragione sociale (from Account) + created date</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Imposta Nome Opportunita</fullName>
        <actions>
            <name>Imposta_Nome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
