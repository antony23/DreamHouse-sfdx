<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Aggiornamento_telefono</fullName>
        <field>OtherPhone</field>
        <formula>Secondo_telefono_ufficio__c</formula>
        <name>Aggiornamento telefono</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Referente__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Secondo telefono ufficio</fullName>
        <actions>
            <name>Aggiornamento_telefono</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Referente_Banca_Dati__c.Secondo_telefono_ufficio__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
