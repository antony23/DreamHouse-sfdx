<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notifica_rispedizione_Inglese</fullName>
        <description>Notifica rispedizione Inglese</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Revisione_template/Notifica_rispedizione_Inglese_New2</template>
    </alerts>
    <alerts>
        <fullName>Notifica_rispedizione_Italiano</fullName>
        <description>Notifica rispedizione Italiano</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Revisione_template/Notifica_rispedizione_Italiano_New2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_asset_record_type_prod_inviato</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prodotto_inviato</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set asset record type prod inviato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_asset_record_type_prod_non_inviato</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prodotto_non_inviato</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set asset record type prod non inviato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_data_invio_copia</fullName>
        <field>Data_invio_copia__c</field>
        <formula>TODAY()</formula>
        <name>Set data invio copia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Record type prodotto inviato</fullName>
        <actions>
            <name>Set_asset_record_type_prod_inviato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Inviata__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Copia_cartacea__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record type prodotto non inviato</fullName>
        <actions>
            <name>Set_asset_record_type_prod_non_inviato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Asset.Inviata__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Copia_digitale__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Copia_cartacea__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set data invio copia</fullName>
        <actions>
            <name>Set_data_invio_copia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Inviata__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Data_invio_copia__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
