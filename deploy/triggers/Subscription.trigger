trigger Subscription on Zuora__Subscription__c (before insert, before update, after insert, after Update, before delete, after delete) {
   
    if(UserInfo.getUserId() != '00520000004nKENAA2' && UserInfo.getUserId() != '00520000004nKEN '){
   
    if(!SubscriptionTriggerHandler.skipTrigger){
        /* Before Insert */
        if(Trigger.isInsert && Trigger.isBefore){
            SubscriptionTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
        }
        /* After Insert */
        else if(Trigger.isInsert && Trigger.isAfter){
            SubscriptionTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        /* Before Update */
        else if(Trigger.isUpdate && Trigger.isBefore){
            SubscriptionTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* After Update */
        else if(Trigger.isUpdate && Trigger.isAfter){
            SubscriptionTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* Before Delete */
        else if(Trigger.isDelete && Trigger.isBefore){
            SubscriptionTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        /* After Delete */
        else if(Trigger.isDelete && Trigger.isAfter){
            SubscriptionTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}
}