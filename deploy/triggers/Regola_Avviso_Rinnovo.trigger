trigger Regola_Avviso_Rinnovo on Regola_Avviso_Rinnovo__c (before delete, after insert, after update) {
	if(!RegolaAvvisoRinnovoTriggerHandler.skipTrigger){
	    /* Before Insert */
	    if(Trigger.isInsert && Trigger.isBefore){
	        RegolaAvvisoRinnovoTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	    }
	    /* After Insert */
	    else if(Trigger.isInsert && Trigger.isAfter){
	        RegolaAvvisoRinnovoTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	    }
	    /* Before Update */
	    else if(Trigger.isUpdate && Trigger.isBefore){
	        RegolaAvvisoRinnovoTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* After Update */
	    else if(Trigger.isUpdate && Trigger.isAfter){
	        RegolaAvvisoRinnovoTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* Before Delete */
	    else if(Trigger.isDelete && Trigger.isBefore){
	        RegolaAvvisoRinnovoTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	    }
	    /* After Delete */
	    else if(Trigger.isDelete && Trigger.isAfter){
	        RegolaAvvisoRinnovoTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
	    }
	}
}