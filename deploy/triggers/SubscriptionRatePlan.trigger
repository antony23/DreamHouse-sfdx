trigger SubscriptionRatePlan on Zuora__SubscriptionRatePlan__c (before insert, before update, after insert, after Update, before delete, after delete) {
    if(!SubscriptionRatePlanTriggerHandler.skipTrigger){
        /* Before Insert */
        if(Trigger.isInsert && Trigger.isBefore){
            SubscriptionRatePlanTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
        }
        /* After Insert */
        else if(Trigger.isInsert && Trigger.isAfter){
            SubscriptionRatePlanTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        /* Before Update */
        else if(Trigger.isUpdate && Trigger.isBefore){
            SubscriptionRatePlanTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* After Update */
        else if(Trigger.isUpdate && Trigger.isAfter){
            SubscriptionRatePlanTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* Before Delete */
        else if(Trigger.isDelete && Trigger.isBefore){
            SubscriptionRatePlanTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        /* After Delete */
        else if(Trigger.isDelete && Trigger.isAfter){
            SubscriptionRatePlanTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}