trigger AvvisoDiRinnovo on Avviso_di_Rinnovo__c (before insert,before update,after insert) {


    if(!AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger){
        if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
            AvvisoDiRinnovoTriggerHandler.setTipoAvviso(trigger.new);           
        }

        if( (trigger.isBefore && trigger.isUpdate) || (trigger.isAfter && trigger.isInsert) ){
            AvvisoDiRinnovoTriggerHandler.setNumeroAttribuzioneCbll(trigger.new);           
        }

    }




}