trigger Spedizione on Spedizione__c (before insert, before update) {
	if(!SpedizioneTriggerHandler.skip){
	    /* Before Insert */
	    if(Trigger.isInsert && Trigger.isBefore){
	        SpedizioneTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	    }
	    /* After Insert */
	    else if(Trigger.isInsert && Trigger.isAfter){
	        SpedizioneTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	    }
	    /* Before Update */
	    else if(Trigger.isUpdate && Trigger.isBefore){
	        SpedizioneTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* After Update */
	    else if(Trigger.isUpdate && Trigger.isAfter){
	        SpedizioneTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* Before Delete */
	    else if(Trigger.isDelete && Trigger.isBefore){
	        SpedizioneTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	    }
	    /* After Delete */
	    else if(Trigger.isDelete && Trigger.isAfter){
	        SpedizioneTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
	    }
	}
}