trigger BillingAccount on Zuora__CustomerAccount__c (before insert, before update) {

    if(trigger.isBefore && trigger.isInsert){
            BillingAccountTriggerHandler.setFields(trigger.new);
    }
    if(trigger.isBefore && trigger.isUpdate){
            BillingAccountTriggerHandler.setFields(trigger.new);
    }

}