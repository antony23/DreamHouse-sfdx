trigger SubscriptionProductCharge on Zuora__SubscriptionProductCharge__c  (before insert, before update, after insert, after Update, before delete, after delete) {
    if(!SubscriptionProductChargeTriggerHandler.skipTrigger){
        /* Before Insert */
        if(Trigger.isInsert && Trigger.isBefore){
            SubscriptionProductChargeTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
        }
        /* After Insert */
        else if(Trigger.isInsert && Trigger.isAfter){
            SubscriptionProductChargeTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        /* Before Update */
        else if(Trigger.isUpdate && Trigger.isBefore){
            SubscriptionProductChargeTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* After Update */
        else if(Trigger.isUpdate && Trigger.isAfter){
            SubscriptionProductChargeTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* Before Delete */
        else if(Trigger.isDelete && Trigger.isBefore){
            SubscriptionProductChargeTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        /* After Delete */
        else if(Trigger.isDelete && Trigger.isAfter){
            SubscriptionProductChargeTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}