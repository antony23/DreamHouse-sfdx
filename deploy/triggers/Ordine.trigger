trigger Ordine on Ordine__c (before insert, before update, after insert, after update, before delete, after delete) {
    if(!OrdineTriggerHandler.skip){

        OrdineTriggerHandler handler = new OrdineTriggerHandler();

        /* Before Insert */
        if(Trigger.isInsert && Trigger.isBefore){
            handler.onBeforeInsert(Trigger.new, Trigger.newMap);
        }
        /* After Insert */
        else if(Trigger.isInsert && Trigger.isAfter){
            handler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        /* Before Update */
        else if(Trigger.isUpdate && Trigger.isBefore){
            handler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* After Update */
        else if(Trigger.isUpdate && Trigger.isAfter){
            handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        /* Before Delete */
        else if(Trigger.isDelete && Trigger.isBefore){
            handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
        }
        /* After Delete */
        else if(Trigger.isDelete && Trigger.isAfter){
            handler.onAfterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}