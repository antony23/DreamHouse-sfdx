trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
	if(!CaseTriggerHandler.skip){
	    /* Before Insert */
	    if(Trigger.isInsert && Trigger.isBefore){
	        CaseTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	    }
	    /* After Insert */
	    else if(Trigger.isInsert && Trigger.isAfter){
	        CaseTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	    }
	    /* Before Update */
	    else if(Trigger.isUpdate && Trigger.isBefore){
	        CaseTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* After Update */
	    else if(Trigger.isUpdate && Trigger.isAfter){
	        CaseTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* Before Delete */
	    else if(Trigger.isDelete && Trigger.isBefore){
	        CaseTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	    }
	    /* After Delete */
	    else if(Trigger.isDelete && Trigger.isAfter){
	        CaseTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
	    }
	}
}