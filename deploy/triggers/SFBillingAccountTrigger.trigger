trigger SFBillingAccountTrigger on Billing_Account__c (after insert) {

	SFBillingAccountHandler handler = new SFBillingAccountHandler();
	if(Trigger.isInsert && Trigger.isAfter){
    	handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }

}