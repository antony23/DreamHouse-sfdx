trigger Contact on Contact (before insert, before update, after update, after insert, before delete, after delete) {
    System.debug('******[Contact on Contact] ContactTriggerHandler.skip: ' + ContactTriggerHandler.skip);
    System.debug('******[Contact on Contact] UserInfo.getUserId(): ' + UserInfo.getUserId());
    if(CustomPermissions__c.getInstance(UserInfo.getUserId()).Executetrigger__c){
        if(!ContactTriggerHandler.skip /*UserInfo.getUserId() != '00520000004nKENAA2'*/){

            ContactTriggerHandler handler = new ContactTriggerHandler();
            System.debug('******[Contact on Contact] Trigger.isInsert: ' + Trigger.isInsert);
            System.debug('******[Contact on Contact] Trigger.isAfter: ' + Trigger.isAfter);
            /* Before Insert */
            if(Trigger.isInsert && Trigger.isBefore){
                handler.onBeforeInsert(Trigger.new, Trigger.newMap);
            }
            /* After Insert */
            else if(Trigger.isInsert && Trigger.isAfter){
                System.debug('******[Contact on Contact] Trigger.new: ' + Trigger.new);
                System.debug('******[Contact on Contact] Trigger.newMap: ' + Trigger.newMap);
                handler.onAfterInsert(Trigger.new, Trigger.newMap);
            }
            /* Before Update */
            else if(Trigger.isUpdate && Trigger.isBefore){
                handler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            }
            /* After Update */
            else if(Trigger.isUpdate && Trigger.isAfter){
                handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            }
            /* Before Delete */
            else if(Trigger.isDelete && Trigger.isBefore){
                handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
            }
            /* After Delete */
            else if(Trigger.isDelete && Trigger.isAfter){
                handler.onAfterDelete(Trigger.old, Trigger.oldMap);
            }
        }
    }
}