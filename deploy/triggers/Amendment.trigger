trigger Amendment on Amendment__c (before insert, after insert, before update, after update, before delete, after delete) {
	if(!AmendmentTriggerHandler.skipTrigger){
	    /* Before Insert */
	    if(Trigger.isInsert && Trigger.isBefore){
	        AmendmentTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	    }
	    /* After Insert */
	    else if(Trigger.isInsert && Trigger.isAfter){
	        AmendmentTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	    }
	    /* Before Update */
	    else if(Trigger.isUpdate && Trigger.isBefore){
	        AmendmentTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* After Update */
	    else if(Trigger.isUpdate && Trigger.isAfter){
	        AmendmentTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* Before Delete */
	    else if(Trigger.isDelete && Trigger.isBefore){
	        AmendmentTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	    }
	    /* After Delete */
	    else if(Trigger.isDelete && Trigger.isAfter){
	        AmendmentTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
	    }
	}
}