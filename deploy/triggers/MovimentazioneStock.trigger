trigger MovimentazioneStock on Movimentazione_Stock__c (before insert, before update, after update, after insert, before delete, after delete) {

	MovimentazioneStockTriggerHandler handler = new MovimentazioneStockTriggerHandler();
	
	if(!MovimentazioneStockTriggerHandler.skip){
		if(trigger.isBefore && trigger.isInsert){
				handler.onBeforeInsert(Trigger.new);
		}
		if(trigger.isBefore && trigger.isUpdate){
				handler.onBeforeUpdate(Trigger.new);
		}
		if(trigger.isAfter && trigger.isUpdate){
				handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);			
		}
		if(trigger.isAfter && trigger.isInsert){
				handler.onAfterInsert(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);			
		}
	}

}