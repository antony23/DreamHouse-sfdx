trigger Campaign on Campaign (before insert, before Update, after insert, after update, before delete, after delete) {
	if(!CampaignTriggerHandler.skipTrigger){
	    /* Before Insert */
	    if(Trigger.isInsert && Trigger.isBefore){
	        CampaignTriggerHandler.onBeforeInsert(Trigger.new, Trigger.newMap);
	    }
	    /* After Insert */
	    else if(Trigger.isInsert && Trigger.isAfter){
	        CampaignTriggerHandler.onAfterInsert(Trigger.new, Trigger.newMap);
	    }
	    /* Before Update */
	    else if(Trigger.isUpdate && Trigger.isBefore){
	        CampaignTriggerHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* After Update */
	    else if(Trigger.isUpdate && Trigger.isAfter){
	        CampaignTriggerHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	    }
	    /* Before Delete */
	    else if(Trigger.isDelete && Trigger.isBefore){
	        CampaignTriggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
	    }
	    /* After Delete */
	    else if(Trigger.isDelete && Trigger.isAfter){
	        CampaignTriggerHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
	    }
	}

}