trigger NotificaInformativa on Notifica_Informativa__c (/*before insert, before update, */after update, after insert/*, before delete, after delete*/) {
    
    System.debug(LoggingLevel.INFO, '*_* NotificaInformativaTrigger');
    NotificaInformativaTriggerHandler handler = new NotificaInformativaTriggerHandler();

    if(!NotificaInformativaTriggerHandler.skipTrigger){
        /*
        if(trigger.isBefore && trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }
        if(trigger.isBefore && trigger.isUpdate){
            handler.onBeforeUpdate(Trigger.new);
        }
        */
        if(trigger.isAfter && trigger.isInsert){
            handler.onAfterInsert(Trigger.new);         
        }
        if(trigger.isAfter && trigger.isUpdate){
            handler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);          
        }
    }
}