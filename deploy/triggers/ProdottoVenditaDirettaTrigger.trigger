trigger ProdottoVenditaDirettaTrigger on Prodotto_Vendita_Diretta__c (before insert, before update, after update, after insert, before delete, after delete) {
    
    ProdottoVenditaDirettaHandler handler = new ProdottoVenditaDirettaHandler();
    
    if(Trigger.isBefore){
    
        if(Trigger.isInsert){
            
            handler.onBeforeInsert(Trigger.New);
        
        }
        
        if(Trigger.isUpdate){
        
            handler.onBeforeUpdate(Trigger.New);
            
        }
    
    }
    
    if(Trigger.isAfter){
    
        if(Trigger.isInsert){
        
            handler.onAfterInsert(Trigger.New);
        
        }
        
        if(Trigger.isUpdate){
        
            handler.onAfterUpdate(Trigger.New);
            
        }
    
    }
    
}