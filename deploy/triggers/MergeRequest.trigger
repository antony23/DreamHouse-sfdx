trigger MergeRequest on Merge_Request__c (after insert, before insert) {
	MergeRequestTriggerHandler.handle();
}