trigger Lead on Lead (before insert/*, before update, after update, after insert, before delete, after delete*/) {
	
	System.debug(LoggingLevel.INFO, '*_* LeadTrigger');
	
    LeadTriggerHandler handler = new LeadTriggerHandler();

    if(!LeadTriggerHandler.skipTrigger){
        if(trigger.isBefore && trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }
        if(trigger.isBefore && trigger.isUpdate){
            handler.onBeforeUpdate(Trigger.new);
        }
        if(trigger.isAfter && trigger.isInsert){
            handler.onAfterInsert(Trigger.oldMap, Trigger.newMap);         
        }
        if(trigger.isAfter && trigger.isUpdate){
            handler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);          
        }
    }
}