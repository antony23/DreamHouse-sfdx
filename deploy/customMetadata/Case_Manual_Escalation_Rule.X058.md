<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>058</label>
    <protected>false</protected>
    <values>
        <field>Categoria__c</field>
        <value xsi:type="xsd:string">ORDINE</value>
    </values>
    <values>
        <field>Coda__c</field>
        <value xsi:type="xsd:string">Ufficio Amministrazione Abb. e V.D.</value>
    </values>
    <values>
        <field>Sottocategoria__c</field>
        <value xsi:type="xsd:string">RINNOVO ABBO</value>
    </values>
</CustomMetadata>
