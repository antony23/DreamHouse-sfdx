<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Carta di Credito Internet</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Attivazione__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Descrizione_Pagamento__c</field>
        <value xsi:type="xsd:string">Carta di Credito</value>
    </values>
    <values>
        <field>Fatturazione__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Pagamento__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>PianoDeiConti__c</field>
        <value xsi:type="xsd:string">0012601010</value>
    </values>
    <values>
        <field>Sincronizzazione__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>TranslationENG__c</field>
        <value xsi:type="xsd:string">Credit Card</value>
    </values>
    <values>
        <field>ZuoraId__c</field>
        <value xsi:type="xsd:string">2c92a0ff5455bfbe01545f6573766526</value>
    </values>
    <values>
        <field>ZuoraMethod__c</field>
        <value xsi:type="xsd:string">Other</value>
    </values>
</CustomMetadata>
