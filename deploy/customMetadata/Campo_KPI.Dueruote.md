<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DUERUOTE</label>
    <protected>false</protected>
    <values>
        <field>Ageing_effettivo__c</field>
        <value xsi:type="xsd:string">AgeingEffettivoDRT__c</value>
    </values>
    <values>
        <field>Cicli_Ponderati__c</field>
        <value xsi:type="xsd:string">Cicli_Ponderati_DRT__c</value>
    </values>
    <values>
        <field>Cicli__c</field>
        <value xsi:type="xsd:string">Cicli_DRT__c</value>
    </values>
    <values>
        <field>Copie_Previste__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
    <values>
        <field>Copie_gracing__c</field>
        <value xsi:type="xsd:string">Copie_Gracing_DRT__c</value>
    </values>
    <values>
        <field>Subscription_con_Gracing__c</field>
        <value xsi:type="xsd:string">Subscription_con_Gracing_DRt__c</value>
    </values>
    <values>
        <field>Testata_Checkbox__c</field>
        <value xsi:type="xsd:string">Testata_Due_Ruote__c</value>
    </values>
    <values>
        <field>Testata__c</field>
        <value xsi:type="xsd:string">Dueruote</value>
    </values>
</CustomMetadata>
