<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>edidomus.my.salesforce.com</label>
    <protected>false</protected>
    <values>
        <field>ContentType__c</field>
        <value xsi:type="xsd:string">application/json;charset=UTF-8</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">https://api.zuora.com/rest/v1/accounts</value>
    </values>
    <values>
        <field>Method__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>apiAccessKeyId__c</field>
        <value xsi:type="xsd:string">gbarberio_ed_prod@deloitte.it</value>
    </values>
    <values>
        <field>apiSecretAccessKey__c</field>
        <value xsi:type="xsd:string">Deloitte.33</value>
    </values>
    <values>
        <field>invoiceTemplateId__c</field>
        <value xsi:type="xsd:string">2c92a0fe5b1ae79b015b1ed94f9578de</value>
    </values>
</CustomMetadata>
