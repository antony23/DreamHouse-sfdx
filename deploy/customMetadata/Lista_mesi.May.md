<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>May</label>
    <protected>false</protected>
    <values>
        <field>Nome_Italiano__c</field>
        <value xsi:type="xsd:string">Maggio</value>
    </values>
    <values>
        <field>Numero_Mese__c</field>
        <value xsi:type="xsd:string">5</value>
    </values>
</CustomMetadata>
