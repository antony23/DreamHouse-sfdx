<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mezzo Cliente</label>
    <protected>false</protected>
    <values>
        <field>Codice_SAP__c</field>
        <value xsi:type="xsd:string">MCLI</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">Italia</value>
    </values>
    <values>
        <field>Tipo_Consegna_Comunicazioni__c</field>
        <value xsi:type="xsd:string">Come concordato con il cliente</value>
    </values>
    <values>
        <field>Tipo_Spedizione__c</field>
        <value xsi:type="xsd:string">Mezzo Cliente</value>
    </values>
    <values>
        <field>TipoconsegnacomunicazioniENG__c</field>
        <value xsi:type="xsd:string">Delivery arranged with the customer</value>
    </values>
    <values>
        <field>Tipoconsegnapercalcolospese__c</field>
        <value xsi:type="xsd:string">Mezzo Cliente</value>
    </values>
</CustomMetadata>
