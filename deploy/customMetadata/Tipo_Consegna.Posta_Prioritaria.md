<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Posta Prioritaria</label>
    <protected>false</protected>
    <values>
        <field>Codice_SAP__c</field>
        <value xsi:type="xsd:string">PRIO</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">Estero</value>
    </values>
    <values>
        <field>Tipo_Consegna_Comunicazioni__c</field>
        <value xsi:type="xsd:string">Corriere</value>
    </values>
    <values>
        <field>Tipo_Spedizione__c</field>
        <value xsi:type="xsd:string">Posta_Prioritaria</value>
    </values>
    <values>
        <field>TipoconsegnacomunicazioniENG__c</field>
        <value xsi:type="xsd:string">Courier</value>
    </values>
    <values>
        <field>Tipoconsegnapercalcolospese__c</field>
        <value xsi:type="xsd:string">Posta Prioritaria</value>
    </values>
</CustomMetadata>
