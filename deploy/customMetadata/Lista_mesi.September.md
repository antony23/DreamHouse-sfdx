<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>September</label>
    <protected>false</protected>
    <values>
        <field>Nome_Italiano__c</field>
        <value xsi:type="xsd:string">Settembre</value>
    </values>
    <values>
        <field>Numero_Mese__c</field>
        <value xsi:type="xsd:string">9</value>
    </values>
</CustomMetadata>
