<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Fascettario Estero Air</label>
    <protected>false</protected>
    <values>
        <field>Codice_SAP__c</field>
        <value xsi:type="xsd:string">ZN</value>
    </values>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">Estero</value>
    </values>
    <values>
        <field>Tipo_Consegna_Comunicazioni__c</field>
        <value xsi:type="xsd:string">Posta prioritaria</value>
    </values>
    <values>
        <field>Tipo_Spedizione__c</field>
        <value xsi:type="xsd:string">Air Mail</value>
    </values>
    <values>
        <field>TipoconsegnacomunicazioniENG__c</field>
        <value xsi:type="xsd:string">Priority mail</value>
    </values>
    <values>
        <field>Tipoconsegnapercalcolospese__c</field>
        <value xsi:type="xsd:string">Fascettario Estero (FASAIR)</value>
    </values>
</CustomMetadata>
