<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>019</label>
    <protected>false</protected>
    <values>
        <field>Categoria__c</field>
        <value xsi:type="xsd:string">PROBLEMI TECNICI</value>
    </values>
    <values>
        <field>Coda__c</field>
        <value xsi:type="xsd:string">CC Web</value>
    </values>
    <values>
        <field>Sottocategoria__c</field>
        <value xsi:type="xsd:string">ENTITLEMENT/ PROBLEMI ACCESSO DIG</value>
    </values>
</CustomMetadata>
