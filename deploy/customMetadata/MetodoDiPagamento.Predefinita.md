<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Predefinita</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Attivazione__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Descrizione_Pagamento__c</field>
        <value xsi:type="xsd:string">Predefinita</value>
    </values>
    <values>
        <field>Fatturazione__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Pagamento__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>PianoDeiConti__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sincronizzazione__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>TranslationENG__c</field>
        <value xsi:type="xsd:string">Predefined</value>
    </values>
    <values>
        <field>ZuoraId__c</field>
        <value xsi:type="xsd:string">2c92a0ff5455bfbe01545f657351651e</value>
    </values>
    <values>
        <field>ZuoraMethod__c</field>
        <value xsi:type="xsd:string">CC Reference Transaction</value>
    </values>
</CustomMetadata>
