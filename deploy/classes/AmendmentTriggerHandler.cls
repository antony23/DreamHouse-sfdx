public class AmendmentTriggerHandler {
	public static Boolean skipTrigger = false;
    
    public static void onBeforeInsert(List<Amendment__c> triggerNew, Map<Id, Amendment__c> triggerNewMap) { 
    }
    
    public static void onAfterInsert(List<Amendment__c> triggerNew, Map<Id, Amendment__c> triggerNewMap) {
    }

    public static void onBeforeUpdate(List<Amendment__c> triggerOld, List<Amendment__c> triggerNew, Map<Id, Amendment__c> triggerOldMap, Map<Id, Amendment__c> triggerNewMap){   
    }

    public static void onAfterUpdate(List<Amendment__c> triggerOld, List<Amendment__c> triggerNew, Map<Id, Amendment__c> triggerOldMap, Map<Id, Amendment__c> triggerNewMap){   
    	//propagateSospensioneProroga(triggerNew);  // CR PROROGHE
    }

    public static void onBeforeDelete(List<Amendment__c> triggerOld, Map<Id, Amendment__c> triggerOldMap){  
    }

    public static void onAfterDelete(List<Amendment__c> triggerOld, Map<Id, Amendment__c> triggerOldMap){  
    }

	/*
    // dovrebbe essere così: se sul case metti proroga parte un trigger che aggiorna l'amendment
    // questo trigger invece deve: se proroga mettere proroga a true e sospensione a false, se sospensione il contrario
    public static void propagateSospensioneProroga(List<Amendment__c> amendments){
    	Set<Id> subscriptionsProroga = new Set<Id>();
        Set<Id> subscriptionsSospensione = new Set<Id>();
    	for(Amendment__c amendment : amendments){
    		if(amendment.Type__c == 'SuspendSubscription'){
                if(amendment.Tipo_sospensione__c == 'Proroga'){
                    subscriptionsProroga.add(amendment.Subscription__c);
                }else if(amendment.Tipo_sospensione__c == 'Sospensione'){
                    subscriptionsSospensione.add(amendment.Subscription__c);
                }
    		}
    	}
    	List<Asset> copieProrogateSospese = new List<Asset>();
    	for(Zuora__Subscription__c subscription : [SELECT Id,
    												(SELECT Id,Proroga__c, Sospensione__c FROM CopieDigitaliCarta__r WHERE Sospensione__c = true OR Proroga__c = true )
    												FROM Zuora__Subscription__c
    												WHERE Id IN :subscriptionsSospensione OR Id IN :subscriptionsProroga]){
    		
    		for(Asset copia : subscription.CopieDigitaliCarta__r){
    			copia.Proroga__c = subscriptionsProroga.contains(subscription.Id);
                copia.Sospensione__c = subscriptionsSospensione.contains(subscription.Id);
                copieProrogateSospese.add(copia);
            }
    	}
    	update copieProrogateSospese;
    }*/
}