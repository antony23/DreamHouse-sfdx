public class ElencoLinkEdizioniDigitaliController {
	public Boolean enableHTML {get;set;}
	public String langCode {get;set;}
	public List<Link_Edizioni_Digitali__c> EdizioniDigitali {get; private set;}

	public ElencoLinkEdizioniDigitaliController() {
		EdizioniDigitali = new List<Link_Edizioni_Digitali__c>();
		EdizioniDigitali.addAll(Link_Edizioni_Digitali__c.getAll().values());
		EdizioniDigitali.sort();
	}

}