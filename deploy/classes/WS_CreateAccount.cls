global class WS_CreateAccount {
	
	global class WS_CreateAccount_Request{
		webservice List<InputElement> elements;
	}

	global class InputElement{
		webservice Account account;
		webservice Contact contact;
	}

	global class WS_CreateAccount_Response{
		webservice List<OutputElement> elements;
		webservice OperationStatus operationStatus;
	}

	global class OutputElement{
		webservice Account account;
		webservice Contact contact;
		webservice String operationsPerformed;
		webservice OperationStatus operationStatus;
	}

	global class OperationStatus{
		webservice Boolean isError;
		webservice String errorMessage;
	}

	class AccountUpsertResult{
		Boolean isSuccess;
		Id accountId;
		Account originalAccount;
		Contact contact;
		String operationStatus;
		FailureReason failureReason;
	}

	class FailureReason{
		Boolean isDuplicate;
		Id duplicateAccountId;
		Boolean isException;
		String exceptionDetailMessage;
	}

    webservice static WS_CreateAccount.WS_CreateAccount_Response upsertAccounts(WS_CreateAccount.WS_CreateAccount_Request request){

    	System.debug(loggingLevel.Error, 'Call WS_CreateAccount...');

        WS_CreateAccount.WS_CreateAccount_Response response = new WS_CreateAccount.WS_CreateAccount_Response();
        response.elements = new List<WS_CreateAccount.OutputElement>();
        
        response.operationStatus = new WS_CreateAccount.OperationStatus();
        response.operationStatus.isError = false;

        Savepoint sp = !Test.isRunningTest() ? Database.setSavepoint() : null;

        try {
        	AccountTriggerHandler.skip = true;
        	//ContactTriggerHandler.skip = true;

        	if(request.elements != null && !request.elements.isEmpty()){
        		System.debug(loggingLevel.Error, '*** found ' + request.elements.size() + ' elements.');
        		List<Account> accounts = new List<Account>();
        		List<Contact> contacts = new List<Contact>();
        		for(WS_CreateAccount.InputElement inElem : request.elements){
        			accounts.add(inElem.account);
        			contacts.add(inElem.contact);
        		}
        		List<AccountUpsertResult> accountUpsertResultList = new List<AccountUpsertResult>();
        		List<Account> accountsDuplicatiDaAggiornare = new List<Account>();
        		Set<Id> accountsDuplicatiDaAggiornareIds = new Set<Id>();
        		List<Integer> accountsDuplicatiDaAggiornarePosizioni = new List<Integer>();

        		System.debug('*******************ACCOUNT****************' + accounts);
        		System.debug('*******************CONTACT****************' + contacts);
        		

        		// upsert account
        		//List<Database.UpsertResult> accountUpsertResult = Database.upsert(accounts, Account.Fields.Codice_Esterno__c, false);
	            
        		//vado in insert sull'account, considerando che ci sono le regole di deduplica attive
        		
        		List<Database.saveResult> accountUpsertResult = Database.insert(accounts, false);	
        		
        		//preparo la lista per eventuali duplicati
        		List<Account> accDuplicati = new List<Account>();
	            for(Integer i=0; i<accountUpsertResult.size(); i++){
	                Database.saveResult accountUpsertResultElem = accountUpsertResult.get(i);
	                
	                AccountUpsertResult aur = new AccountUpsertResult();
	                aur.isSuccess = accountUpsertResultElem.isSuccess();
	                aur.originalAccount = accounts.get(i);
	                aur.contact = contacts.get(i);
	                aur.accountId = aur.isSuccess ? accountUpsertResultElem.getId() : null;

	                System.debug(loggingLevel.Error, '*** aur.originalAccount: ' + aur.originalAccount);
	                System.debug(loggingLevel.Error, '*** aur.contact: ' + aur.contact);
	                System.debug(loggingLevel.Error, '*** aur.isSuccess: ' + aur.isSuccess);

	                if(aur.isSuccess){
	                	aur.operationStatus = 'Account ' + (accountUpsertResultElem.isSuccess() ? 'created. ' : 'updated. ');
                	}else{
                		aur.operationStatus = 'Error during upsert for account.';
	                	aur.failureReason = new FailureReason();
	                	Database.Error error = accountUpsertResultElem.getErrors()[0];
	                	System.debug(loggingLevel.Error, '*** Errore durante l\'upsert dell\'account: ' + error);
                        if (error instanceof Database.DuplicateError) {
	                		aur.failureReason.isDuplicate = true;
	                		aur.failureReason.isException = false;
	                		
                            Database.DuplicateError duplicateError = (Database.DuplicateError) error;
                            Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                            List<Datacloud.MatchResult> matchResults = duplicateResult.getMatchResults();
                            if(!matchResults.isEmpty()){
                                Datacloud.MatchResult matchResult = matchResults.get(0);
                                List<Datacloud.MatchRecord> matchRecords = matchResult.getMatchRecords();
                                if(!matchRecords.isEmpty()){
                                    Datacloud.MatchRecord matchRecord = matchRecords.get(0);
                                    sObject accountDuplicato = matchRecord.getRecord();
                                    aur.failureReason.duplicateAccountId = (Id) accountDuplicato.get('Id');

                                    System.debug(loggingLevel.Error, '*** Id dell\'account duplicato: ' + aur.failureReason.duplicateAccountId);
                                    
                                    if(!accountsDuplicatiDaAggiornareIds.contains(aur.failureReason.duplicateAccountId)){
	                                    accountsDuplicatiDaAggiornare.add(
	                                    	new Account(
	                                    		Id = aur.failureReason.duplicateAccountId
	                                    		//Codice_ACI__c = aur.originalAccount.Codice_ACI__c,
	                                    		//Codice_Press_Di__c = aur.originalAccount.Codice_Press_Di__c,
	                                    		//Codice_Esterno__c = aur.originalAccount.Codice_Esterno__c
	                                    	)
	                                    );
	                                    accountsDuplicatiDaAggiornarePosizioni.add(i);
	                                	aur.originalAccount.id = aur.failureReason.duplicateAccountId;
	                					accDuplicati.add(aur.originalAccount);    
                                    }
                                }
                            }
                        }else{
                        	aur.failureReason.isDuplicate = false;
	                		aur.failureReason.isException = true;
                            aur.failureReason.exceptionDetailMessage = error.getMessage();

                            System.debug(loggingLevel.Error, '*** Errore generico: ' + aur.failureReason.exceptionDetailMessage);
                        }
	                }
	                System.debug(loggingLevel.Error, '*** aggiungo a accountUpsertResultList l\'elemento ' + aur);
	                accountUpsertResultList.add(aur);
	            }

	            //se ci sono account non inseriti/aggiornati a causa della deduplica, aggiorno sull'account salesforce i codici (con skip della deduplica per sicurezza)
	           	Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                dml.OptAllOrNone = false;

                System.debug(loggingLevel.Error, '*** Ci sono ' + accountsDuplicatiDaAggiornare.size() + ' account duplicati da aggiornare.');
	            System.debug('**************ACCOUNT DUPLICATI METODO *************' + accDuplicati);
	            //List<Database.SaveResult> aggiornamentoCodiciAccountResult = Database.update(accountsDuplicatiDaAggiornare, dml);
	           	//CHIAMO IL METODO PER AGGIORNARE GLI ACCOUNT DUPLICATI

	           	List<Database.SaveResult> aggiornamentoCodiciAccountResult = updateAccountDuplicati(accDuplicati);

	            for(Integer i=0; i<aggiornamentoCodiciAccountResult.size(); i++){
	            	Database.SaveResult aggiornamentoCodiciAccountResultElement = aggiornamentoCodiciAccountResult.get(i);
	            	System.debug(loggingLevel.Error, '*** Risultato aggiornamento account duplicato su Salesforce: ' + aggiornamentoCodiciAccountResultElement);
	            	Integer posizioneAccountUpsertResult = accountsDuplicatiDaAggiornarePosizioni.get(i);
	            	System.debug(loggingLevel.Error, '*** Posizione della lista di accountUpsertResult: ' + posizioneAccountUpsertResult);
	            	AccountUpsertResult aur = accountUpsertResultList.get(posizioneAccountUpsertResult);
	            	System.debug(loggingLevel.Error, '*** Recupero l\'elemento dalla lista: ' + aur);

	            	if(aggiornamentoCodiciAccountResultElement.isSuccess()){
	            		aur.isSuccess = true;
	            		aur.accountId = aggiornamentoCodiciAccountResultElement.getId();
	            		System.debug(loggingLevel.Error, '*** aggiornamento OK, account id aggiornato: ' + aur.accountId);
            		}else{
            			Database.Error error = aggiornamentoCodiciAccountResultElement.getErrors()[0];
            			aur.isSuccess = false;
            			aur.failureReason = new FailureReason();
            			aur.failureReason.isException = true;
            			aur.failureReason.exceptionDetailMessage = error.getMessage();

            			System.debug(loggingLevel.Error, '*** aggiornamento in errore: ' + aur.failureReason);
            		}
	            }

	            System.debug(loggingLevel.Error, '*** Recupero i contatti che posso inserire.');
	            // associazione degli id degli account ai contatti
	            List<Integer> posizioniContattiPerUpsert = new List<Integer>();
	            List<Contact> contattiPerUpsert = new List<Contact>();
	            for(Integer i=0; i<accountUpsertResultList.size(); i++){
	            	AccountUpsertResult aur = accountUpsertResultList.get(i);
	            	if(aur.isSuccess){
	            		aur.contact.AccountId = aur.accountId;
	            		contattiPerUpsert.add(aur.contact);
	            		posizioniContattiPerUpsert.add(i);
	            	}
	            }

	            System.debug(loggingLevel.Error, '*** Posso inserire ' + contattiPerUpsert.size() + ' contatti.');
	            // salvataggio dei contatti
	            List<Contact> contactsDuplicatiDaAggiornare = new List<Contact>();
        		List<Contact> contDuplicati = new List<Contact>();
        		Set<Id> contactsDuplicatiDaAggiornareIds = new Set<Id>();
        		List<Integer> contactsDuplicatiDaAggiornarePosizioni = new List<Integer>();

        		System.debug(loggingLevel.Error, '*** Inserisco i contatti: ');
	            List<Database.UpsertResult> contactUpsertResult = Database.upsert(contattiPerUpsert, false);
	            for(Integer i=0; i<contactUpsertResult.size(); i++){
	                Database.UpsertResult contactUpsertResultElem = contactUpsertResult.get(i);
	                System.debug(loggingLevel.Error, '*** Esito dell\'inserimento del contatto: ' + contactUpsertResultElem);
	                Integer posizioneContatto = posizioniContattiPerUpsert.get(i);
	                System.debug(loggingLevel.Error, '*** Posizione del contatto nella lista di partenza: ' + posizioneContatto);
	                AccountUpsertResult aur = accountUpsertResultList.get(posizioneContatto);
	                System.debug(loggingLevel.Error, '*** AccountUpsertResult element associato al contatto: ' + aur);

	                if(contactUpsertResultElem.isSuccess()){
	                	aur.operationStatus += ' Contact ' + (contactUpsertResultElem.isCreated() ? 'created. ' : 'updated. ');
	                }else{
	                	Database.Error error = contactUpsertResultElem.getErrors()[0];
	                	aur.operationStatus = 'Error during upsert for contact.';
            			aur.isSuccess = false;
            			aur.failureReason = new FailureReason();

	                	if (error instanceof Database.DuplicateError) {
	                		aur.failureReason.isDuplicate = true;
	                		aur.failureReason.isException = false;
	                		
                            Database.DuplicateError duplicateError = (Database.DuplicateError) error;
                            Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                            List<Datacloud.MatchResult> matchResults = duplicateResult.getMatchResults();
                            if(!matchResults.isEmpty()){
                                Datacloud.MatchResult matchResult = matchResults.get(0);
                                List<Datacloud.MatchRecord> matchRecords = matchResult.getMatchRecords();
                                if(!matchRecords.isEmpty()){
                                    Datacloud.MatchRecord matchRecord = matchRecords.get(0);
                                    sObject contactDuplicato = matchRecord.getRecord();
                                    Id duplicateContactId = (Id) contactDuplicato.get('Id');

                                    System.debug(loggingLevel.Error, '*** Id del contatto duplicato: ' + duplicateContactId);
                                    
                                    if(!contactsDuplicatiDaAggiornareIds.contains(duplicateContactId)){
	                                    contactsDuplicatiDaAggiornare.add(
	                                    	new Contact(
	                                    		Id = duplicateContactId
	                                    		//Codice_ACI__c = aur.originalAccount.Codice_ACI__c,
	                                    		//Codice_Press_Di__c = aur.originalAccount.Codice_Press_Di__c,
	                                    		//Codice_Esterno__c = aur.originalAccount.Codice_Esterno__c
	                                    	)
	                                    );
	                                    contactsDuplicatiDaAggiornarePosizioni.add(i);
	                                    aur.contact.id = duplicateContactId;
	                                    contDuplicati.add(aur.contact);
                                    }
                                }
                            }
                        }else{
                        	aur.failureReason.isDuplicate = false;
	                		aur.failureReason.isException = true;
                            aur.failureReason.exceptionDetailMessage = error.getMessage();

            				System.debug(loggingLevel.Error, '*** Errore generico durante l\'inserimento: ' + aur.failureReason);
                        }
	                }
	            }

	            System.debug(loggingLevel.Error, '*** Ci sono ' + contactsDuplicatiDaAggiornare.size() + ' contatti da aggiornare perché duplicati.');
	            // se ci sono contact non inseriti/aggiornati a causa della deduplica, aggiorno sull'contact salesforce i codici (con skip della deduplica per sicurezza)
	            List<Database.SaveResult> aggiornamentoCodiciContactResult = updateContactDuplicati(contDuplicati);
	            
	            for(Integer i=0; i<aggiornamentoCodiciContactResult.size(); i++){
	            	Database.SaveResult aggiornamentoCodiciContactResultElement = aggiornamentoCodiciContactResult.get(i);
	            	Integer posizioneContactUpsertResult = contactsDuplicatiDaAggiornarePosizioni.get(i);
	            	AccountUpsertResult aur = accountUpsertResultList.get(posizioneContactUpsertResult);

	            	if(aggiornamentoCodiciContactResultElement.isSuccess()){
	            		aur.isSuccess = true;
            		}else{
            			Database.Error error = aggiornamentoCodiciContactResultElement.getErrors()[0];
            			aur.isSuccess = false;
            			aur.failureReason = new FailureReason();
            			aur.failureReason.isException = true;
            			aur.failureReason.exceptionDetailMessage = error.getMessage();
            		}
	            }

	            System.debug(loggingLevel.Error, '*** Costruisco la response');
	            for(AccountUpsertResult aur : accountUpsertResultList){
	            	System.debug(loggingLevel.Error, '*** Elemento della response: ' + aur);
	            	WS_CreateAccount.OutputElement outElem = new WS_CreateAccount.OutputElement();
	            	outElem.account = aur.originalAccount;
	            	outElem.contact = aur.contact;
	            	outElem.operationsPerformed = aur.operationStatus;
                	outElem.operationStatus = new WS_CreateAccount.OperationStatus();
                	outElem.operationStatus.isError = !aur.isSuccess;
                	if(!aur.isSuccess){
                		WS_CreateAccount.FailureReason fr = aur.failureReason;
                		outElem.operationStatus.errorMessage = '';
                		if(fr.isDuplicate){
                			outElem.operationStatus.errorMessage += 'Duplicato.';
                		}
                		if(fr.isException){
                			outElem.operationStatus.errorMessage += '\r\n' + fr.exceptionDetailMessage;
                		}
                	}
					response.elements.add(outElem);
	            }
        	}
        } catch(Exception e) {
        	if(!Test.isRunningTest()) Database.rollback(sp);
        	response.operationStatus = new WS_CreateAccount.OperationStatus();
        	response.operationStatus.isError = true;
        	response.operationStatus.errorMessage = e.getMessage() + '. ' + e.getStackTraceString();
        }

        return response;
    }

	private static List<Database.SaveResult> updateAccountDuplicati(List<sObject> duplicati) {
		System.debug('************updateAccountDuplicati*********');
    	List<string> ACCOUNT_FIELDS = new List<String>();
    	for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Account').getDescribe().Fields.getMap().values()){
    		
    		Schema.DescribeFieldResult fd = ft.getDescribe();
    		String fName = fd.getName();
    		
    		if(!fName.startsWith('Person') && !fName.endsWith('__pc') && fName != 'FirstName' && fName != 'LastName'){
    			ACCOUNT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
    		}
        
        }

        List<sObject> sAccounts = Database.query('SELECT ' + String.join(ACCOUNT_FIELDS, ',') +
                                        ' FROM Account ' +
                                        ' WHERE Id IN :duplicati ');

        System.debug('************ sAccounts**********' + sAccounts );
		for (integer i =0; i<sAccounts.size(); i++) {
			for(String nomeCampo : ACCOUNT_FIELDS) {
                // DH-1119: Aggiunta campo 'RecordTypeId'
                if(sAccounts[i].get(nomeCampo) == null || nomeCampo == 'Codice_Esterno__c' || nomeCampo == 'Codice_ACI__c' || nomeCampo == 'Codice_Press_Di__c' || nomeCampo == 'RecordTypeId'){
					if(duplicati[i].get(nomeCampo) != null ) sAccounts[i].put(nomeCampo, duplicati[i].get(nomeCampo));
					//System.debug('*******campo in entrata account ********' + duplicati[i].get(nomeCampo));	
				}

			}
		}
	
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.DuplicateRuleHeader.allowSave = true;
		dml.DuplicateRuleHeader.runAsCurrentUser = true;

		List<Database.SaveResult> aggiornamentoCodiciAccountResult = Database.update(sAccounts, dml);
		System.debug('****************** aggiornamentoCodiciAccountResult ***************' + aggiornamentoCodiciAccountResult);
		return aggiornamentoCodiciAccountResult;
	}	

	private static List<Database.SaveResult> updateContactDuplicati(List<sObject> duplicati) {
		System.debug('************updateContactDuplicati*********');
    	List<string> CONTACT_FIELDS = new List<String>();
    	for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
    		
    		Schema.DescribeFieldResult fd = ft.getDescribe();
    		String fName = fd.getName();
    		
    		if(!fName.startsWith('Person') && !fName.endsWith('__pc') && fName != 'FirstName' && fName != 'LastName'){
    			CONTACT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
    		}
        
        }

        List<sObject> sContacts = Database.query('SELECT ' + String.join(CONTACT_FIELDS, ',') +
                                        ' FROM Contact ' +
                                        ' WHERE Id IN :duplicati ');

		for (integer i =0; i<sContacts.size(); i++) {
			for(String nomeCampo : CONTACT_FIELDS) {
				if(sContacts[i].get(nomeCampo) == null || nomeCampo == 'Codice_Esterno__c' || nomeCampo == 'Codice_ACI__c' || nomeCampo == 'Codice_Press_Di__c'){
					if(duplicati[i].get(nomeCampo) != null ) sContacts[i].put(nomeCampo, duplicati[i].get(nomeCampo));
					//System.debug('*******campo in entrata contatto*******' + duplicati[i].get(nomeCampo));
				}
			}
		}
	
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.DuplicateRuleHeader.allowSave = true;
		dml.DuplicateRuleHeader.runAsCurrentUser = true;

		List<Database.SaveResult> aggiornamentoCodiciContactResult = Database.update(sContacts, dml);
		System.debug('****************** aggiornamentoCodiciContactResult ***************' + aggiornamentoCodiciContactResult );
		return aggiornamentoCodiciContactResult;
	}	
}