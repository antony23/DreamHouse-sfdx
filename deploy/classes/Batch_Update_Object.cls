global class Batch_Update_Object implements Database.Batchable<sObject> {
    private Map<String, Object> values;
    private String query;

	global Batch_Update_Object(String objectName, List<Id> ids, Map<String, Object> values) {
		this.query = 'SELECT ' + String.join(new List<String>(values.keySet()), ',') +
						' FROM ' + objectName + ' WHERE Id IN (' + String.join(quote(ids), ',') + ')';
		this.values = values;
	}
    
	global Batch_Update_Object(String query, Map<String, Object> values){
		this.query = query;
        this.values = values;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('[Batch_Update_Object] >> Starting batch with query \'' + query + '\'.');

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for(sObject sobj : scope) {
			final String objectName = sobj.getSObjectType().getDescribe().getName();

			System.debug('[Batch_Update_Object] >> Start ' + objectName + '[' + sobj.Id + '] update.');
			System.debug('[Batch_Update_Object] >> before: ' + sobj);

            for(String field : values.keySet()) {
                sobj.put(field, values.get(field));
            }

			System.debug('[Batch_Update_Object] >> after: ' + sobj);
			System.debug('[Batch_Update_Object] >> End ' + objectName + '[' + sobj.Id + '] update.');
        }
        
        update scope;
	}

	global void finish(Database.BatchableContext BC) {
		System.debug('[Batch_Update_Object] >> Batch finished.');

	}

	private static List<String> quote(List<Id> ids) {
		final List<String> strIds = new List<String>();

		for(Id sobjId : ids) {
			strIds.add('\'' + sobjId + '\'');
		}

		return strIds;
	}	
}