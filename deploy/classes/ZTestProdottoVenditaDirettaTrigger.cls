@isTest
public class ZTestProdottoVenditaDirettaTrigger {
	
	@isTest static void test_method_one(){

		Test.startTest();

		Testata__c tes = ZTest_Utils.createTestata('Test');
		insert tes;

		Blocco_Selettivo_MS__c bsms = new Blocco_Selettivo_MS__c();
		bsms.Blocco_Attivo__c = true;
		bsms.SetupownerId = UserInfo.getOrganizationId();
		insert bsms;

		Prodotto_Vendita_Diretta__c pvd = new Prodotto_Vendita_Diretta__c();
		pvd.Name = 'Test PVD';
		pvd.Testata__c = tes.Id;
		insert pvd;

		Test.stopTest();

	}

	@isTest static void test_method_two(){

		Test.startTest();

		Testata__c tes = ZTest_Utils.createTestata('Test');
		insert tes;

		Blocco_Selettivo_MS__c bsms = new Blocco_Selettivo_MS__c();
		bsms.Blocco_Attivo__c = true;
		bsms.SetupownerId = UserInfo.getOrganizationId();
		insert bsms;

		Account zuoraAcc= ZTest_Utils.createAccount();
        insert zuoraAcc;

        Contact billTo= ZTest_Utils.createContact(zuoraAcc.id, 'IT');
        insert billTo;        
      
        Contact soldTo= ZTest_Utils.createContact(zuoraAcc.id, null);
        insert soldTo;

		Zuora__CustomerAccount__c billingAccount= ZTest_Utils.createBillingAccount(billTo.id, soldTo.id, zuoraAcc.id);
        billingAccount.Zuora__Zuora_Id__c = 'ABC';
        insert billingAccount;

		Zuora__Subscription__c sub=ZTest_Utils.createSubscriptionRen('Test',billingAccount, 'Pending Activation','SI');
		insert sub;

		Prodotto_Vendita_Diretta__c pvd = new Prodotto_Vendita_Diretta__c();
		pvd.Name = 'Test PVD';
		pvd.Testata__c = tes.Id;
		insert pvd;

		Movimentazione_Stock__c ms = new Movimentazione_Stock__c();
		ms.Subscription__c = sub.Id;
		ms.Prodotto_Vendita_Diretta__c = pvd.Id;
		ms.Quantita__c = 1;
		insert ms;

		pvd.Giacenza__c = 10;
		update pvd;

		Test.stopTest();

	}

}