@isTest
private class ZTest_ScheduledInformativa {
	
	@isTest
	static void testGetPrivacyVersion() {

		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockTestClass('getPrivacyVersion'));
		ScheduledInformativa si = new ScheduledInformativa();
		si.execute(null);
		Test.stopTest();

	}

	@testSetup
	public static void testSetup() {

		Setting__c version = new Setting__c (
			Name = 'getPrivacyVersion',
			URL__c = 'https://login.edidomus.it/v1/getPrivacyVersion'
		);

		Setting__c text = new Setting__c (
			Name = 'getPrivacyText',
			URL__c = 'https://login.edidomus.it/v1/getPrivacyText?version='
		);

		List<Setting__c> settingList = new List<Setting__c>();
		settingList.add(version);
		settingList.add(text);
		insert settingList;

	}
}