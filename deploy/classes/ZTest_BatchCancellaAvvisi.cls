@isTest
private class ZTest_BatchCancellaAvvisi {
	@isTest static void test_method_one() {

		Test.startTest();
		
		String query = 'SELECT Id FROM Avviso_di_Rinnovo__c WHERE Stato_Avviso__c = \'Nuovo\'';

		Database.executeBatch(new BatchCancellaAvvisi(query));
		Database.executeBatch(new Batch_Delete_Object(query));

		Test.stopTest();

	}

	@testSetup
	public static void testSetup() {

 		Product2 product= ZTest_Utils.createProduct('QUATTRORUOTE');
 		insert product;

		zqu__ProductRatePlan__c ratePlan = ZTest_Utils.createzquProductRatePlan(product.id,'2c92c0f85e09726a015e0a79d8db550f','Abbonamento');
        insert ratePlan;
		
        Testata__c testata=ZTest_Utils.createTestata('QUATTRORUOTE');
        insert testata;
        
		List<Calendario__c> calendars=ZTest_Utils.createCalendar(testata.id);
      	insert calendars;
        
		Avviso_di_rinnovo__c avvisoDiRinnovoNoSub= new Avviso_di_Rinnovo__c();
		insert avvisoDiRinnovoNoSub;
                
        Account zuoraAcc= ZTest_Utils.createAccount();
        insert zuoraAcc;

        Contact billTo= ZTest_Utils.createContact(zuoraAcc.id, 'IT');
        insert billTo;        
      
        Contact soldTo= ZTest_Utils.createContact(zuoraAcc.id, null);
        insert soldTo;
      
        Zuora__CustomerAccount__c billingAccount= ZTest_Utils.createBillingAccount(billTo.id, soldTo.id, zuoraAcc.id);
        billingAccount.Zuora__Zuora_Id__c = 'ABC';
        insert billingAccount;

        Zuora__Subscription__c subscription=ZTest_Utils.createSubscriptionRen('Test',billingAccount, 'Pending Activation','SI');
		insert subscription;
	
		Avviso_di_rinnovo__c avviso= ZTest_Utils.createAvvisoDiRinnovo(subscription.id, ratePlan.id, 'Italia');
		avviso.Stato_Avviso__c = 'Nuovo';
		insert avviso;

	}
}