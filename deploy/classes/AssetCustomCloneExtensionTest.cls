@isTest (SeeAllData=true)
private class AssetCustomCloneExtensionTest{
	
	@isTest
	static void test1(){
		MovimentazioneStockTriggerHandler.skip = true;
		Prodotto_Vendita_Diretta__c pvd = [SELECT Id FROM Prodotto_Vendita_Diretta__c WHERE Giacenza__c > 2 LIMIT 1];

		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Stato__c = 'Nuovo', Quantita__c = 1, Prodotto_Vendita_Diretta__c = pvd.Id);
		insert mov;

		AccountTriggerHandler.skip = true;
		Account a = new Account(Name = 'test');
		insert a;

		Asset copia = new Asset(Name = 'test', AccountId = a.Id, Quantity = 999999, Movimentazione_Stock__c = mov.Id);
		insert copia;
		
		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":{"SpedizioneDirette":[{"TipoSpedizione":"Contrassegno","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"COCO","TipoConsegna":"Contrassegno","MessaggioConsegnaITA":"Contrassegno","MessaggioConsegnaENG":"Cash on delivery"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"CONO","TipoConsegna":"Corriere Normale SDA","MessaggioConsegnaITA":"Corriere SDA","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Mezzo Domus","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MDOM","TipoConsegna":"Mezzo Domus","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]}],"SpedizioneAbbonamenti":[{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Standard Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZL","TipoConsegna":"Fascettario Standard Italia","MessaggioConsegnaITA":"Poste italiane","MessaggioConsegnaENG":"Ordinary Mail"}]}]}}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
        Test.startTest();
		AssetCustomCloneExtension ext = new AssetCustomCloneExtension(new ApexPages.StandardController(copia));
		ext.init();

		System.debug(loggingLevel.Error, '*** ext.isTipoSpeseDiSpedizioneAbbonamentoVisibile: ' + ext.isTipoSpeseDiSpedizioneAbbonamentoVisibile);
		System.debug(loggingLevel.Error, '*** ext.isTipoSpeseDiSpedizioneVenditaDirettaVisibile: ' + ext.isTipoSpeseDiSpedizioneVenditaDirettaVisibile);
		System.debug(loggingLevel.Error, '*** ext.copia: ' + ext.copia);
		System.debug(loggingLevel.Error, '*** ext.movStock: ' + ext.movStock);
		System.debug(loggingLevel.Error, '*** ext.quantitaCopia: ' + ext.quantitaCopia);
		System.debug(loggingLevel.Error, '*** ext.tipiSpeseSpedizione: ' + ext.tipiSpeseSpedizione);
		System.debug(loggingLevel.Error, '*** ext.tipiSpeseConsegne: ' + ext.tipiSpeseConsegne);
		System.debug(loggingLevel.Error, '*** ext.tipiConsegnaAbbonamenti: ' + ext.tipiConsegnaAbbonamenti);
		System.debug(loggingLevel.Error, '*** ext.tipiConsegnaVenditeDirette: ' + ext.tipiConsegnaVenditeDirette);
		System.debug(loggingLevel.Error, '*** ext.motiviRispedizione: ' + ext.motiviRispedizione);

		ext.nofunction();
		ext.goBack();
		ext.appendErrorMessage(new DomusException('Test'));

		ext.customClone();
		
		Test.stopTest();
	}

	@isTest
	static void test2(){
		MovimentazioneStockTriggerHandler.skip = true;
		Prodotto_Vendita_Diretta__c pvd = [SELECT Id FROM Prodotto_Vendita_Diretta__c WHERE Giacenza__c > 2 LIMIT 1];

		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Stato__c = 'Trasmesso', Quantita__c = 1, Prodotto_Vendita_Diretta__c = pvd.Id);
		insert mov;

		AccountTriggerHandler.skip = true;
		Account a = new Account(Name = 'test');
		insert a;

		Asset copia = new Asset(Name = 'test', AccountId = a.Id, Quantity = 999999, Movimentazione_Stock__c = mov.Id);
		insert copia;
		
		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":{"SpedizioneDirette":[{"TipoSpedizione":"Contrassegno","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"COCO","TipoConsegna":"Contrassegno","MessaggioConsegnaITA":"Contrassegno","MessaggioConsegnaENG":"Cash on delivery"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"CONO","TipoConsegna":"Corriere Normale SDA","MessaggioConsegnaITA":"Corriere SDA","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Mezzo Domus","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MDOM","TipoConsegna":"Mezzo Domus","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]}],"SpedizioneAbbonamenti":[{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Standard Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZL","TipoConsegna":"Fascettario Standard Italia","MessaggioConsegnaITA":"Poste italiane","MessaggioConsegnaENG":"Ordinary Mail"}]}]}}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
        Test.startTest();
		AssetCustomCloneExtension ext = new AssetCustomCloneExtension(new ApexPages.StandardController(copia));
		ext.init();
		ext.customClone();
		Test.stopTest();
	}

	@isTest
	static void test3(){
		MovimentazioneStockTriggerHandler.skip = true;
		Prodotto_Vendita_Diretta__c pvd = [SELECT Id FROM Prodotto_Vendita_Diretta__c WHERE Giacenza__c > 2 LIMIT 1];

		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Stato__c = 'Trasmesso', Quantita__c = 1, Prodotto_Vendita_Diretta__c = pvd.Id);
		insert mov;

		AccountTriggerHandler.skip = true;
		Account a = new Account(Name = 'test');
		insert a;

		Asset copia = new Asset(Name = 'test', AccountId = a.Id, Quantity = 999999, Movimentazione_Stock__c = mov.Id);
		insert copia;
		
		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":{"SpedizioneDirette":[{"TipoSpedizione":"Contrassegno","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"COCO","TipoConsegna":"Contrassegno","MessaggioConsegnaITA":"Contrassegno","MessaggioConsegnaENG":"Cash on delivery"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"CONO","TipoConsegna":"Corriere Normale SDA","MessaggioConsegnaITA":"Corriere SDA","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Mezzo Domus","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MDOM","TipoConsegna":"Mezzo Domus","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]}],"SpedizioneAbbonamenti":[{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Standard Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZL","TipoConsegna":"Fascettario Standard Italia","MessaggioConsegnaITA":"Poste italiane","MessaggioConsegnaENG":"Ordinary Mail"}]}]}}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
        Test.startTest();
		AssetCustomCloneExtension ext = new AssetCustomCloneExtension(new ApexPages.StandardController(copia));
		ext.init();
		ext.copia.Motivo_Rispedizione__c = 'RISPEDIZIONE ABBO - MANCATA CONSEGNA';
		ext.customClone();
		Test.stopTest();
	}

	@isTest
	static void test4(){
		MovimentazioneStockTriggerHandler.skip = true;
		Prodotto_Vendita_Diretta__c pvd = [SELECT Id FROM Prodotto_Vendita_Diretta__c WHERE Giacenza__c > 2 LIMIT 1];

		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Stato__c = 'Trasmesso', Quantita__c = 1, Prodotto_Vendita_Diretta__c = pvd.Id);
		insert mov;

		AccountTriggerHandler.skip = true;
		Account a = new Account(Name = 'test');
		insert a;

		Asset copia = new Asset(Name = 'test', AccountId = a.Id, Quantity = 1, Movimentazione_Stock__c = mov.Id);
		insert copia;

		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":{"SpedizioneDirette":[{"TipoSpedizione":"Contrassegno","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"COCO","TipoConsegna":"Contrassegno","MessaggioConsegnaITA":"Contrassegno","MessaggioConsegnaENG":"Cash on delivery"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"CONO","TipoConsegna":"Corriere Normale SDA","MessaggioConsegnaITA":"Corriere SDA","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Mezzo Domus","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MDOM","TipoConsegna":"Mezzo Domus","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]}],"SpedizioneAbbonamenti":[{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Standard Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZL","TipoConsegna":"Fascettario Standard Italia","MessaggioConsegnaITA":"Poste italiane","MessaggioConsegnaENG":"Ordinary Mail"}]}]}}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
        Test.startTest();
        Asset input = [SELECT Id FROM Asset WHERE Id = :copia.Id];
		AssetCustomCloneExtension ext = new AssetCustomCloneExtension(new ApexPages.StandardController(input));
		ext.init();
		ext.copia.Motivo_Rispedizione__c = 'RISPEDIZIONE ABBO - MANCATA CONSEGNA';
		ext.customClone();
		Test.stopTest();
	}

}