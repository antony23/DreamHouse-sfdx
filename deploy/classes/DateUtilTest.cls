@isTest
public class DateUtilTest {

	@isTest
	public static void testGetMonthName() {
		final List<Date> dates = setupDatesWithDifferentMonths();
		final List<String> names = new String[]{'JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'};

		Test.startTest();

		for(Integer i = 0; i < 12; i++) {
			Date d = dates[i];
			String monthName = DateUtil.getMonthName(d);
			String shortMonthName = DateUtil.getMonthName(d, true);

			System.debug('[Test] ' + monthName);
			System.assert(monthName.equalsIgnoreCase(names[i]));
			System.assert(shortMonthName.equalsIgnoreCase(names[i].substring(0, 3)));
		}

		Test.stopTest();
	}

	private static List<Date> setupDatesWithDifferentMonths() {
		final List<Date> dates = new List<Date>();

		for(Integer i = 1; i <= 12; i++) {
			dates.add(Date.newInstance(2018, i, 1));
		}

		return dates;
	}
}