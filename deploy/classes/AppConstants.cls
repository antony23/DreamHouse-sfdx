public class AppConstants {

    public static final String QUALIFIED_API_NAME_ATTRIBUTE = 'QualifiedApiName';
    public static final String FULL_NAME_ATTRIBUTE = 'FullName';
    public static final String LABEL_ATTRIBUTE = 'Label';
    public static final String DEV_NAME_ATTRIBUTE = 'DeveloperName';
    public static final String DESC_ATTRIBUTE = 'Description';
    public static final String MDT_SUFFIX = '__mdt';
    
    public static final String SELECT_STRING = 'Select type';
    
    public static final String AVVISI_SI = 'SI';
    public static final String AVVISI_NO = 'NO';

    //error messages 
    public static final String FILE_MISSING = 'Please provide a comma seperated file.';
    public static final String EMPTY_FILE = 'CSV file is empty.';
    public static final String TYPE_OPTION_NOT_SELECTED = 'Please choose a valid custom metadata type.';
    public static final String HEADER_MISSING_DEVNAME_AND_LABEL = 'Header must contain atleast one of these two fields - '+ DEV_NAME_ATTRIBUTE + ', ' + LABEL_ATTRIBUTE +'.';
    public static final String INVALID_FILE_ROW_SIZE_DOESNT_MATCH = 'The number of field values does not match the number of header fields on line ';
    public static final String INVALID_CONTACT = 'Impossibile recuperare il contatto';
    public static final String INVALID_CONSEGNA_VENDITA_DIRETTA = 'Valore di "Consegna Vendite Dirette" richiesto.';
    public static final String INVALID_SPEDIZIONE_ABBONAMENTO = 'Valori di Spedizione Abbonamento non validi';
    public static final String INVALID_QUOTE_STATUS = 'Solo le Quote in stato NEW possono essere cancellate';
    public static final String INSUFFICIENT_PRIVILEGES = 'Non si dispone di privilegi sufficienti';
    public static final String FILE_REQUIRED = 'Errore. Inserire un allegato';

    // Tipo Avvisi
    public static final String AVVISO_RINNOVO = 'Rinnovo';
    public static final String AVVISO_SOLLECITO = 'Sollecito';

    // Tipo Supporto Subscription
    public static final String SUPPORTO_CARTA = 'Carta';
    public static final String SUPPORTO_CARTA_DIGITALE = 'Carta + Digitale';
    public static final String SUPPORTO_DIGITALE = 'Digitale';
    public static final String SUPPORTO_CORRIERE = 'Corriere';

    // Codici contabilizzazione
    public static final String CODICE_DIGITALE = '05';
    public static final String CODICE_ITALIA = '01';
    public static final String CODICE_ESTERO = '02';

    // Stati Quote
    public static final String NEW_QUOTE = 'New';

    // Profili
    public static final String PROFILE_SYSADMIN = 'System Administrator';
    public static final String PROFILO_AMMINISTRATORE = 'Amministratore del sistema';
    public static final String PROFILE_UFFICIO_GESTIONE = 'Ufficio Gestione';

    // Messaggi IBAN
    public static final String IBAN_VALIDO = 'Codice IBAN Valido';
    public static final String NO_IBAN = 'Nessun Codice IBAN inserito';

    // Vari
    public static final String SI = 'SI';
    public static final String NO = 'NO';
    public static final String ZUORA_PLACEHOLDER = '-';

    // Messaggi Email
    public static final String MISSING_CAMPAIGN_MEMBER = 'Nessun Membro della Campagna trovato';
    public static final String EMAIL_ERROR = 'Errore durante l\'invio delle Email';
    public static final String EMAIL_SUCCESS = 'Email inviate correttamente';
    public static final String EMAIL_DRAFT = 'Draft';
    public static final String EMAIL_SENT = 'Sent';
}