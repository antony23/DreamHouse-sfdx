@isTest
public class ElencoLinkEdizioniDigitaliControllerTest {
	
	@isTest
	public static void testFlags() {
		final ElencoLinkEdizioniDigitaliController controller = new ElencoLinkEdizioniDigitaliController();
		controller.enableHTML = true;
		controller.langCode = 'it';

		System.assert(controller.enableHTML);
		System.assertEquals(controller.langCode, 'it');
	}

	@isTest
	public static void test() {
		final List<Link_Edizioni_Digitali__c> linkEdizioniDigitali = setup();

		Test.startTest();

		final ElencoLinkEdizioniDigitaliController controller = new ElencoLinkEdizioniDigitaliController();

		System.assert(controller.EdizioniDigitali != null);
		System.assert(controller.EdizioniDigitali.size() == linkEdizioniDigitali.size());

		for(Link_Edizioni_Digitali__c testLed : linkEdizioniDigitali) {
			Boolean found = false;

			for(Link_Edizioni_Digitali__c led : controller.EdizioniDigitali) {
				if(led.Name.equals(testLed.Name)) {
					found = true;
					System.assertEquals(led.Testata__c, testLed.Testata__c);
					System.assertEquals(led.English__c, testLed.English__c);
					System.assertEquals(led.Italian__c, testLed.Italian__c);
				}
			}

			System.assert(found);
		}

		Test.stopTest();
	}

	public static List<Link_Edizioni_Digitali__c> setup() {
		final List<Link_Edizioni_Digitali__c> linkEdizioniDigitali = new List<Link_Edizioni_Digitali__c>();

		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Domus', Testata__c='Domus', English__c='https://www.domusweb.it/en/shop/digitaledition.html', Italian__c='http://www.domusweb.it/abbonati'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Dueruote', Testata__c='Dueruote', English__c='http://www.dueruote.it/abbonati', Italian__c='http://www.dueruote.it/abbonati'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Meridiani', Testata__c='Meridiani', English__c='http://www.shoped.it/abbonatimeridiani', Italian__c='http://www.shoped.it/abbonatimeridiani'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Quattroruote', Testata__c='Quattroruote', English__c='http://www.quattroruote.it/abbonati', Italian__c='http://www.quattroruote.it/abbonati'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Ruoteclassiche', Testata__c='Ruoteclassiche', English__c='http://www.ruoteclassiche.it/abbonati', Italian__c='http://www.ruoteclassiche.it/abbonati'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='TopGear', Testata__c='TopGear', English__c='http://www.quattroruote.it/abbonatitopgear', Italian__c='http://www.quattroruote.it/abbonatitopgear'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='Tuttotrasporti', Testata__c='Tuttotrasporti', English__c='http://www.tuttotrasporti.it/abbonati', Italian__c='http://www.tuttotrasporti.it/abbonati'));
		linkEdizioniDigitali.add(new Link_Edizioni_Digitali__c(Name='XOffRoad', Testata__c='XOffRoad', English__c='http://www.xoffroad.it/abbonati', Italian__c='http://www.xoffroad.it/abbonati'));

		insert linkEdizioniDigitali;

		return linkEdizioniDigitali;
	}

}