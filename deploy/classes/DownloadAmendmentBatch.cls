global class DownloadAmendmentBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.StateFul {
	
	static final List<String> AMENDMENT_FIELDS;
	String query;
	ZuoraInstance zInstance {get;set;}
	List<Zuora__Subscription__c> subscriptionsAmended;
	String errorMessage = '';
	Set<Id> subscriptionsAmendedIds = null;

	static{
		AMENDMENT_FIELDS = new List<String>{
			'AutoRenew','Code','ContractEffectiveDate','CreatedById','CreatedDate','CurrentTerm','CurrentTermPeriodType',
			'CustomerAcceptanceDate','Description','EffectiveDate','Id','Name','RenewalSetting','RenewalTerm','RenewalTermPeriodType',
			'ResumeDate','ServiceActivationDate','SpecificUpdateDate','Status','SubscriptionId','SuspendDate','TermStartDate',
			'TermType','Type','UpdatedById','UpdatedDate','Tiposospensione__c'
		};
	}
	
	global DownloadAmendmentBatch(Set<Id> subs) {

		Map<Id,Zuora__Subscription__c> subscriptionsAmendedMap = new Map<Id,Zuora__Subscription__c>(
			[SELECT Id, IsAmendment__c FROM Zuora__Subscription__c WHERE Id IN :subs AND IsAmendment__c = true]
		);

        List<string> subscriptionFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Zuora__Subscription__c').getDescribe().Fields.getMap().values()){
            subscriptionFields.add(ft.getDescribe().getName().toLowerCase());
        }
        subscriptionsAmendedIds = subscriptionsAmendedMap.keySet();
        String query = 'SELECT ' + String.join(subscriptionFields, ',') + 
                    ' FROM Zuora__Subscription__c ' +
                    ' WHERE Id IN :subscriptionsAmendedIds ';
        subscriptionsAmended = (List<Zuora__Subscription__c>) Database.query(query);
	}
	
	global Iterable<sObject> start(Database.BatchableContext BC) {
		try {
			zuoraLogin();
			return subscriptionsAmended;
		} catch(Exception e) {
			errorMessage += 'DownloadAmendmentBatch says: ' + e.getMessage() + '. ' + e.getStackTraceString()+'\r\n';
		}
		return null;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	   	List<Zuora__Subscription__c> subscriptions = (List<Zuora__Subscription__c>) scope;
   		try {
   			for(Zuora__Subscription__c subscription : subscriptions){
   				subscription.Amendment_error_message__c = null;
   				subscription.AmendmentIsError__c = false;
   			}
	   		Map<String,Id> subscriptionsByZuoraOriginalId = new Map<String,Id>();
	   		for(Zuora__Subscription__c subscription : subscriptions){
	   			subscriptionsByZuoraOriginalId.put(subscription.Zuora__OriginalId__c,subscription.Id);
	   		}
			String fetchAllRelatedSubscriptionsZOQL = 'SELECT Id,OriginalId FROM Subscription WHERE OriginalId = \'' + 
													String.join(new List<String>(subscriptionsByZuoraOriginalId.keySet()), '\' OR OriginalId = \'') + '\'' ;
			List<Zuora.zObject> relatedSubscriptions = null;
			if(!Test.isRunningTest()) relatedSubscriptions = zInstance.zApiInstance.zquery(fetchAllRelatedSubscriptionsZOQL);
			else{
				relatedSubscriptions = new List<Zuora.zObject>();
				Zuora.zObject obj = new Zuora.zObject('Subscription');
				obj.setValue('Id','Id');
				obj.setValue('OriginalId','OriginalId');
				relatedSubscriptions.add(obj);
			}

			Map<String,String> zuoraIdOriginalId = new Map<String,String>();
			for(Zuora.zObject relatedSubscription : relatedSubscriptions){
				zuoraIdOriginalId.put(
					(String) relatedSubscription.getValue('Id'),
					(String) relatedSubscription.getValue('OriginalId')
				);
			}

			String fetchAllAmendmentsZOQL = 'SELECT ' + String.join(AMENDMENT_FIELDS, ',') + 
											' FROM Amendment WHERE SubscriptionId = \'' +
											String.join(new List<String>(zuoraIdOriginalId.keySet()), '\' OR SubscriptionId = \'') + '\'' ;

			List<Zuora.zObject> zuoraAmendments = null;
			if(!Test.isRunningTest()) zuoraAmendments = zInstance.zApiInstance.zquery(fetchAllAmendmentsZOQL);
			else{
				Zuora.zObject obj1 = new Zuora.zObject('Amendment');
				obj1.setValue('SubscriptionId','SubscriptionId');
				obj1.setValue('CreatedDate', DateTime.now());
				obj1.setValue('Type', 'SuspendSubscription');

				Zuora.zObject obj2 = new Zuora.zObject('Amendment');
				obj2.setValue('SubscriptionId','SubscriptionId');
				obj2.setValue('CreatedDate', DateTime.now());
				obj2.setValue('Type', 'ResumeSubscription');

				Zuora.zObject obj3 = new Zuora.zObject('Amendment');
				obj3.setValue('SubscriptionId','SubscriptionId');
				obj3.setValue('CreatedDate', DateTime.now());
				obj3.setValue('Type', 'TermsAndConditions');
				
				zuoraAmendments = new List<Zuora.zObject>();
				zuoraAmendments.add(obj1);
				zuoraAmendments.add(obj2);
				zuoraAmendments.add(obj3);
			}

			List<Amendment__c> amendments = new List<Amendment__c>();
			for(Zuora.zObject zuoraAmendment : zuoraAmendments){
				amendments.add(
					new Amendment__c(
						Subscription__c 			= subscriptionsByZuoraOriginalId.get(
														zuoraIdOriginalId.get(
															(String) zuoraAmendment.getValue('SubscriptionId')
														)
													),
						Code__c 						= (String) zuoraAmendment.getValue('Code'),
						ContractEffectiveDate__c 	= (Date) zuoraAmendment.getValue('ContractEffectiveDate'),
						CreatedByZuoraId__c 		= (String) zuoraAmendment.getValue('CreatedById'),
						ZuoraCreatedDate__c 		= (Datetime) zuoraAmendment.getValue('CreatedDate'),
						CurrentTerm__c 				= (Decimal) zuoraAmendment.getValue('CurrentTerm'),
						CurrentTermPeriodType__c 	= (String) zuoraAmendment.getValue('CurrentTermPeriodType'),
						CustomerAcceptanceDate__c 	= (Date) zuoraAmendment.getValue('CustomerAcceptanceDate'),
						Description__c 				= (String) zuoraAmendment.getValue('Description'),
						EffectiveDate__c 			= (Date) zuoraAmendment.getValue('EffectiveDate'),
						ZuoraId__c 					= (String) zuoraAmendment.getValue('Id'),
						Name 						= (String) zuoraAmendment.getValue('Code'),
						ResumeDate__c 				= (Date) zuoraAmendment.getValue('ResumeDate'),
						ServiceActivationDate__c 	= (Date) zuoraAmendment.getValue('ServiceActivationDate'),
						SpecificUpdateDate__c 		= (Date) zuoraAmendment.getValue('SpecificUpdateDate'),
						Status__c 					= (String) zuoraAmendment.getValue('Status'),
						SubscriptionId__c 			= (String) zuoraAmendment.getValue('SubscriptionId'),
						SuspendDate__c 				= (Date) zuoraAmendment.getValue('SuspendDate'),
						TermStartDate__c 			= (Date) zuoraAmendment.getValue('TermStartDate'),
						TermType__c 				= (String) zuoraAmendment.getValue('TermType'),
						Type__c 					= (String) zuoraAmendment.getValue('Type'),
						UpdatedByZuoraId__c 		= (String) zuoraAmendment.getValue('UpdatedById'),
						UpdatedDate__c 				= (Datetime) zuoraAmendment.getValue('UpdatedDate'),
						Tipo_sospensione__c 		= String.isNotBlank((String) zuoraAmendment.getValue('Tiposospensione__c')) 
														? (String) zuoraAmendment.getValue('Tiposospensione__c') 
														: 'Sospensione'	// CR PROROGHE
					)
				);
			}
			
			upsert amendments ZuoraId__c;
   		} catch(Exception e) {
   			errorMessage += 'execute says: ' + e.getMessage() + '. ' + e.getStackTraceString()+'\r\n';
   			for(Zuora__Subscription__c subscription : subscriptions){
   				subscription.Amendment_error_message__c = errorMessage;
   				subscription.AmendmentIsError__c = true;
   			}
   		}
		SubscriptionTriggerHandler.skipTrigger = true;
		update subscriptions;
	}
	
	global void finish(Database.BatchableContext BC) {
		Database.executebatch(new ApplyAmendmentBatch(subscriptionsAmendedIds));
	}

	private void zuoraLogin(){
	    zInstance = new ZuoraInstance();
		zInstance.zApiInstance = new Zuora.zApi();
		zInstance.loginResult = zInstance.zApiInstance.zlogin();
		zInstance.zApiInstance.setEndpoint(Setting__c.getValues('ZuoraEndpoint').URL__c);
	}

	class ZuoraInstance{
		Zuora.zApi zApiInstance {get;set;}
		Zuora.zApi.LoginResult loginResult {get;set;}	
	}

	/** Inizio CR PROROGHE **/

	/**
	 *	Scarica tutti gli amendments di una subscription da Zuora e li mappa con l'id della subscription di appartenenza senza salvarli su SFDC.
	 *
	 *	@param subscriptions Lista di subscription di cui recuperare gli amendments.
	 *	@return Mappa contenente tutti gli amendments raggruppati per id della subscription di appartenenza.
	 *
	 *	@note Pur essendo una soluzione assolutamente non efficiente, l'alternativa sarebbe costosa e richiederebbe la
	 *	ristrutturazione di tutto il flusso che si occupa di processare una subscription alla creazione/sospensione di questa.
	**/

	public static Map<Id, List<Amendment__c>> downloadAmendments(List<Zuora__Subscription__c> subscriptions) {
		final Map<Id, List<Amendment__c>> amendmentsBySubscription = new Map<Id, List<Amendment__c>>();

   		try {
   			final Zuora.zApi zApiInstance = new Zuora.zApi();
        	if(!Test.isRunningTest()) zApiInstance.zlogin();

	   		Map<String,Id> subscriptionsByZuoraOriginalId = new Map<String,Id>();

	   		for(Zuora__Subscription__c subscription : subscriptions){
	   			subscriptionsByZuoraOriginalId.put(subscription.Zuora__OriginalId__c, subscription.Id);
	   		}

			String fetchAllRelatedSubscriptionsZOQL = 'SELECT Id, OriginalId FROM Subscription ' + 
													  'WHERE OriginalId = \'' + String.join(new List<String>(subscriptionsByZuoraOriginalId.keySet()), '\' OR OriginalId = \'') + '\'' ;
			List<Zuora.zObject> relatedSubscriptions = null;
			
			if(!Test.isRunningTest()) {
				relatedSubscriptions = zApiInstance.zquery(fetchAllRelatedSubscriptionsZOQL);
			} else{
				relatedSubscriptions = new List<Zuora.zObject>();
				Zuora.zObject obj = new Zuora.zObject('Subscription');
				obj.setValue('Id','Id');
				obj.setValue('OriginalId','OriginalId');
				relatedSubscriptions.add(obj);
			}

			Map<String,String> zuoraIdOriginalId = new Map<String,String>();
			for(Zuora.zObject relatedSubscription : relatedSubscriptions){
				zuoraIdOriginalId.put(
					(String) relatedSubscription.getValue('Id'),
					(String) relatedSubscription.getValue('OriginalId')
				);
			}

			String fetchAllAmendmentsZOQL = 'SELECT ' + String.join(AMENDMENT_FIELDS, ',') + ' ' +
											'FROM Amendment WHERE SubscriptionId = \'' +
											String.join(new List<String>(zuoraIdOriginalId.keySet()), '\' OR SubscriptionId = \'') + '\'' ;

			List<Zuora.zObject> zuoraAmendments = null;

			if(!Test.isRunningTest()) {
				zuoraAmendments = sortAmendments(zApiInstance.zquery(fetchAllAmendmentsZOQL));
			} else {
				final DateTime createdDate = DateTime.now();

				Zuora.zObject obj1 = new Zuora.zObject('Amendment');
				obj1.setValue('SubscriptionId','SubscriptionId');
				obj1.setValue('CreatedDate', createdDate);
				obj1.setValue('Type', 'SuspendSubscription');

				Zuora.zObject obj2 = new Zuora.zObject('Amendment');
				obj2.setValue('SubscriptionId','SubscriptionId');
				obj2.setValue('CreatedDate', createdDate);
				obj2.setValue('Type', 'ResumeSubscription');

				Zuora.zObject obj3 = new Zuora.zObject('Amendment');
				obj3.setValue('SubscriptionId','SubscriptionId');
				obj3.setValue('CreatedDate', createdDate);
				obj3.setValue('Type', 'TermsAndConditions');

				Zuora.zObject obj4 = new Zuora.zObject('Amendment');
				obj4.setValue('SubscriptionId','SubscriptionId');
				obj4.setValue('CreatedDate', createdDate.addSeconds(2));
				obj4.setValue('Type', 'Cancellation');

				zuoraAmendments = new List<Zuora.zObject>();
				zuoraAmendments.add(obj1);
				zuoraAmendments.add(obj4);
				zuoraAmendments.add(obj2);
				zuoraAmendments.add(obj3);
				zuoraAmendments = sortAmendments(zuoraAmendments);
			}

			for(Zuora.zObject zuoraAmendment : zuoraAmendments){
				final Amendment__c amend = new Amendment__c(
												Subscription__c 			= subscriptionsByZuoraOriginalId.get(
																				zuoraIdOriginalId.get(
																					(String) zuoraAmendment.getValue('SubscriptionId')
																				)
																			),
												Code__c 						= (String) zuoraAmendment.getValue('Code'),
												ContractEffectiveDate__c 	= (Date) zuoraAmendment.getValue('ContractEffectiveDate'),
												CreatedByZuoraId__c 		= (String) zuoraAmendment.getValue('CreatedById'),
												ZuoraCreatedDate__c 		= (Datetime) zuoraAmendment.getValue('CreatedDate'),
												CurrentTerm__c 				= (Decimal) zuoraAmendment.getValue('CurrentTerm'),
												CurrentTermPeriodType__c 	= (String) zuoraAmendment.getValue('CurrentTermPeriodType'),
												CustomerAcceptanceDate__c 	= (Date) zuoraAmendment.getValue('CustomerAcceptanceDate'),
												Description__c 				= (String) zuoraAmendment.getValue('Description'),
												EffectiveDate__c 			= (Date) zuoraAmendment.getValue('EffectiveDate'),
												ZuoraId__c 					= (String) zuoraAmendment.getValue('Id'),
												Name 						= (String) zuoraAmendment.getValue('Code'),
												ResumeDate__c 				= (Date) zuoraAmendment.getValue('ResumeDate'),
												ServiceActivationDate__c 	= (Date) zuoraAmendment.getValue('ServiceActivationDate'),
												SpecificUpdateDate__c 		= (Date) zuoraAmendment.getValue('SpecificUpdateDate'),
												Status__c 					= (String) zuoraAmendment.getValue('Status'),
												SubscriptionId__c 			= (String) zuoraAmendment.getValue('SubscriptionId'),
												SuspendDate__c 				= (Date) zuoraAmendment.getValue('SuspendDate'),
												TermStartDate__c 			= (Date) zuoraAmendment.getValue('TermStartDate'),
												TermType__c 				= (String) zuoraAmendment.getValue('TermType'),
												Type__c 					= (String) zuoraAmendment.getValue('Type'),
												UpdatedByZuoraId__c 		= (String) zuoraAmendment.getValue('UpdatedById'),
												UpdatedDate__c 				= (Datetime) zuoraAmendment.getValue('UpdatedDate'),
												Tipo_sospensione__c 		= String.isNotBlank((String) zuoraAmendment.getValue('Tiposospensione__c')) 
																				? (String) zuoraAmendment.getValue('Tiposospensione__c') 
																				: 'Sospensione'	// CR PROROGHE
											);

				if(!amendmentsBySubscription.containsKey(amend.Subscription__c)) {
					amendmentsBySubscription.put(amend.Subscription__c, new List<Amendment__c>());
				}

				amendmentsBySubscription.get(amend.Subscription__c).add(amend);
			}
   		} catch(Exception e) {
   			System.debug(LoggingLevel.ERROR, '*** downloadAmendments ***: ' + e);
   		}

   		return amendmentsBySubscription;
	}

	/**
	 *	Ordina gli amendments in base alla data di creazione e al tipo.
	 *
	 *	@param amendments Lista di amendments da ordinare.
	 *	@return Lista ordinata di amendments.
	**/

	private static List<Zuora.zObject> sortAmendments(List<Zuora.zObject> amendments) {
		Boolean exchangeOccurred;

		do {
			exchangeOccurred = False;

			for(Integer i = 0; i < amendments.size() - 1; i++) {
				final Zuora.zObject currAmend = amendments[i];
				final DateTime caCreatedDate = (Datetime) currAmend.getValue('CreatedDate');
				final String caType = (String) currAmend.getValue('Type');
				final Integer caPriority = getPriority(caType);

				for(Integer j = i + 1; j < amendments.size(); j++) {
					final Zuora.zObject nextAmend = amendments[j];
					final DateTime naCreatedDate = (Datetime) nextAmend.getValue('CreatedDate');
					final String naType = (String) nextAmend.getValue('Type');
					final Integer naPriority = getPriority(naType);

					if(caCreatedDate == naCreatedDate && caPriority > naPriority) {
						exchangeOccurred = True;
						exchange(amendments, i, j);
					} else if(caCreatedDate > naCreatedDate) {
						exchangeOccurred = True;
						exchange(amendments, i, j);
					}
				}
			}

		} while(exchangeOccurred);

		return amendments;
	}

	private static void exchange(List<Zuora.zObject> amendments, Integer a, Integer b) {
		final Zuora.zObject bkp = amendments[b];
		amendments[b] = amendments[a];
		amendments[a] = bkp;
	}

	private static Integer getPriority(String amendType) {
		switch on amendType {
			when 'SuspendSubscription' 	{ return 1; }
			when 'ResumeSubscription' 	{ return 2; }
			when 'TermsAndConditions' 	{ return 3; }
			when 'Cancellation' 		{ return 4; }
		}

		return null;
	}

	/** Fine CR PROROGHE **/
}