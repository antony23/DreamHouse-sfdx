global class SubscriptionRenewalScheduled implements Schedulable {
	global void execute(SchedulableContext sc) {
		SubscriptionRenewalBatch batch = new SubscriptionRenewalBatch();
		Database.executeBatch(batch);
	}
}