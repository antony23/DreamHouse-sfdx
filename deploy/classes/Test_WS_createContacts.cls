@isTest
public class Test_WS_createContacts {
	
	@isTest static void testM(){

		WS_CreateContacts.mainInput mainInput = new WS_CreateContacts.mainInput();
		mainInput.contacts = new List<Contact>{
					        	new Contact(
					        		FirstName = 'Lino',
						    		LastName = 'Baratto', 
						    		Email='baratto.lino@email.com'
						    	)
		};
        WS_CreateContacts.mainOutput mainOutputOK = WS_CreateContacts.insertContactMasterData(mainInput);
        List<WS_CreateContacts.OutputObj> resultOk = mainOutputOK.result;
        WS_CreateContacts.OutputObj inserimentoOK = resultOk.get(0);
        System.assert(inserimentoOK.accountId != null);
        System.assert(inserimentoOK.contactId != null);
        System.assert(inserimentoOK.contactIdDuplicato == null);
        System.assert(inserimentoOK.contact != null);
        System.assert(inserimentoOK.errorMessage == null);

        System.assert([SELECT Id FROM Contact WHERE Id = :inserimentoOK.contactId].size() == 1);
        System.assert([SELECT Id FROM Account WHERE Id = :inserimentoOK.accountId].size() == 1);

        mainInput = new WS_CreateContacts.mainInput();
		mainInput.contacts = new List<Contact>{
					        	new Contact(
					        		FirstName = 'Lino',
						    		LastName = 'Baratto', 
						    		Email='baratto.lino@email.com'
						    	)
		};
        WS_CreateContacts.mainOutput mainOutputKO = WS_CreateContacts.insertContactMasterData(mainInput);
        List<WS_CreateContacts.OutputObj> resultKO = mainOutputKO.result;
        WS_CreateContacts.OutputObj inserimentoKO = resultKO.get(0);
        //System.assert(inserimentoKO.accountId != null);
        //System.assert(inserimentoKO.contactId == null);
        //System.assert(inserimentoKO.contactIdDuplicato != null);
        //System.assert(inserimentoKO.contact != null);
        //System.assert(inserimentoKO.errorMessage != null);

        //System.assertEquals(inserimentoKO.contactIdDuplicato, inserimentoOK.contactId);
        //System.assertEquals(inserimentoKO.accountId, inserimentoOK.accountId);

        mainInput = new WS_CreateContacts.mainInput();
		mainInput.contacts = new List<Contact>{
					        	new Contact(
					        		FirstName = 'Felice',
						    		LastName = 'Fortunato', 
						    		Email = 'email non valida'
						    	)
		};
        WS_CreateContacts.mainOutput mainOutputError = WS_CreateContacts.insertContactMasterData(mainInput);
        List<WS_CreateContacts.OutputObj> resultError = mainOutputError.result;
        WS_CreateContacts.OutputObj inserimentoError = resultError.get(0);
        //System.assert(inserimentoKO.errorMessage != null);
        System.assertEquals(0,[SELECT Id FROM Contact WHERE FirstName = 'Felice' AND LastName = 'Fortunato'].size());

        // simulo errore
        mainInput = new WS_CreateContacts.mainInput();
        mainOutputError = WS_CreateContacts.insertContactMasterData(mainInput);
	}

}