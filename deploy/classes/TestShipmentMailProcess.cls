@isTest
public class TestShipmentMailProcess {
	private static final Integer NUMERO_SPEDIZIONI = 10;
	private static final Integer NUMERO_MOVSTOCK = 4;
	private static Blocco_Selettivo_Notifiche__c bsn;
	
	@isTest
	public static void testEmpty() {
		bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
		bsn.MT4_Abilitato__c = true;
		bsn.MT7_Abilitato__c = true;
		bsn.MT11_Abilitato__c = true;
		insert bsn;

		Test.startTest();

		ShipmentMailProcess smp = new ShipmentMailProcess();
		Database.executeBatch(smp, NUMERO_SPEDIZIONI);

		Test.stopTest();
	}

	@isTest
	public static void testMT4() {
		setup(true, false, false);

		Test.startTest();

		ShipmentMailProcess smp = new ShipmentMailProcess();
		Database.executeBatch(smp, NUMERO_SPEDIZIONI);

		Test.stopTest();
	}

	@isTest
	public static void testMT7() {
		setup(false, true, false);

		Test.startTest();

		ShipmentMailProcess smp = new ShipmentMailProcess();
		Database.executeBatch(smp, NUMERO_SPEDIZIONI);

		Test.stopTest();
	}

	@isTest
	public static void testMT11() {
		setup(false, false, true);

		Test.startTest();

		ShipmentMailProcess smp = new ShipmentMailProcess();
		Database.executeBatch(smp, NUMERO_SPEDIZIONI);

		Test.stopTest();
	}

	@isTest
	public static void testAll() {
		setup(true, true, true);

		Test.startTest();

		ShipmentMailProcess smp = new ShipmentMailProcess();
		Database.executeBatch(smp, NUMERO_SPEDIZIONI);

		Test.stopTest();
	}

	public static void testStatus() {
		Zuora__Subscription__c subscription = [SELECT Notifica_Istruzioni_Digital__c FROM Zuora__Subscription__c WHERE Name = 'A-S00000000' ];
		List<Movimentazione_Stock__c> movStockSpedite = [SELECT Notifica_Spedizione__c FROM Movimentazione_Stock__c WHERE Stato__c = 'Recepito da SAP' AND Rispedizione__c = False];
		List<Movimentazione_Stock__c> movStockRispedite = [SELECT Notifica_Rispedizione__c FROM Movimentazione_Stock__c WHERE Stato__c = 'Recepito da SAP' AND Rispedizione__c = True];

		System.assertEquals(subscription.Notifica_Istruzioni_Digital__c, (bsn.MT4_Abilitato__c && bsn.MT7_Abilitato__c));

		System.debug('[Test] ' + bsn.MT7_Abilitato__c);
		System.debug('[Test] ' + movStockSpedite);

		for(Movimentazione_Stock__c ms : movStockSpedite) {
			System.assertEquals(ms.Notifica_Spedizione__c, bsn.MT7_Abilitato__c);
		}

		for(Movimentazione_Stock__c ms : movStockRispedite) {
			System.assertEquals(ms.Notifica_Rispedizione__c, bsn.MT11_Abilitato__c);
		}
	}

	private static void setup(Boolean mt4, Boolean mt7, Boolean mt11) {
		bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
		bsn.MT4_Abilitato__c = mt4;
		bsn.MT7_Abilitato__c = mt7;
		bsn.MT11_Abilitato__c = mt11;
		insert bsn;

		RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'B2C' LIMIT 1];

		Account account = new Account(Name = 'TestAccount', Notifiche_Email_Disabilitate__c = false, RecordTypeId = rt.Id);
		insert account;

		Contact contact = new Contact(FirstName = 'FirstNameTest', LastName = 'LastNameTest', AccountId = account.Id, Email = 'test@email.com');
		insert contact;

		Zuora__Subscription__c subscription = new Zuora__Subscription__c(Name = 'A-S00000000', Zuora__Account__c = account.Id, Contact_Bill_To__c = contact.Id, Tipo_subscription__c = 'Bundle vendite dirette', Supporto__c = 'Carta + Digitale');
		
		SubscriptionTriggerHandler.skipTrigger = True;
		insert subscription;

		createSubscriptionRatePlan(subscription);

		List<Spedizione__c> spedizioni = new List<Spedizione__c>();

		for(Integer i = 1; i <= NUMERO_SPEDIZIONI; i++) {
			spedizioni.add(new Spedizione__c(Subscription__c = subscription.Id, Name = 'S-0000000' + i, Numero_Lettura_di_Vettura__c = 'abc' + i));
		}

		SpedizioneTriggerHandler.skip = True;
		insert spedizioni;

		final List<Movimentazione_Stock__c> movStockList = new List<Movimentazione_Stock__c>();

		for(Integer j = 0; j < NUMERO_SPEDIZIONI / 2; j++) {
			for(Integer i = 1; i <= NUMERO_MOVSTOCK / 2; i++) {
				movStockList.add(new Movimentazione_Stock__c(Spedizione__c = spedizioni[j].Id, Quantita__c = 1, Stato__c = 'Recepito da SAP', Rispedizione__c = False, Subscription__c = subscription.Id, Segno_del_movimento_del__c = '-'));
			}
		}

		for(Integer j = NUMERO_SPEDIZIONI / 2; j < NUMERO_SPEDIZIONI; j++) {
			for(Integer i = 1; i <= NUMERO_MOVSTOCK / 2; i++) {
				movStockList.add(new Movimentazione_Stock__c(Spedizione__c = spedizioni[j].Id, Quantita__c = 1, Stato__c = 'Recepito da SAP', Rispedizione__c = True, Subscription__c = subscription.Id, Segno_del_movimento_del__c = '-'));
			}
		}

		MovimentazioneStockTriggerHandler.skip = True;
		insert movStockList;
	}

	private static void createSubscriptionRatePlan(Zuora__Subscription__c sub) {
    	Zuora__Product__c prod = new Zuora__Product__c(Name='Bundle Abbonamenti');
    	insert prod;

    	Product2 prod2 = new Product2(Name='Bundle Abbonamenti');
    	insert prod2;

    	zqu__ZProduct__c zprod = new zqu__ZProduct__c(Name='Bundle Abbonamenti');
    	insert zprod;

        zqu__ProductRatePlan__c prp = ZTest_Utils.createzquProductRatePlan(prod2.Id, '12345678901234567890123456789013', 'Diffusione');
        insert prp;

        zqu__ProductRatePlanCharge__c prpc = ZTest_Utils.createzquProductRatePlanCharge(prp.Id, 'Diffusione');
        prpc.Entitlment__c = 'Virtualcomm';
        insert prpc;

        Zuora__SubscriptionRatePlan__c srp = ZTest_Utils.createZuoraSubscriptionRatePlan(prp, sub.Id);
        srp.Name = prod2.Name;

        SubscriptionRatePlanTriggerHandler.skipTrigger = true;
        insert srp;

        Zuora__SubscriptionProductCharge__c spc = new Zuora__SubscriptionProductCharge__c (Name = 'SubscriptionProductCharge', Product_Rate_Plan_Charge__c = prpc.Id, Zuora__Account__c = sub.Zuora__Account__c,   Zuora__Description__c = 'SubscriptionProductCharge', Zuora__ProductSKU__c = 'BDAB000001', Zuora__Product__c = prod.Id, Zuora__Quantity__c = 1.000, Supporto__c = 'Digitale', Zuora__SubscriptionRatePlan__c = srp.Id, Zuora__Subscription__c = sub.Id);
        SubscriptionProductChargeTriggerHandler.skipTrigger = true;
        insert spc;
    }
}