public class ManualQuoteCreationController{

	public class ManualQuoteCreationException extends Exception {}
	private Integer numMonth;
	private Zuora__Subscription__c subscription;
	public Boolean hasError {get;set;}
	public ManualQuoteCreationController(ApexPages.StandardController controller){
        if(!Test.isRunningTest())controller.addFields(new List<String>{'Stato_Sfdc__c','Blocco_Rinnovo__c','ConAvvisi__c','Mantieni_Offerta__c','Regalo__C','Metodo_di_pagamento__c','Intermediario__c','Canale_di_acquisizione__c','Id','Tipo_Spese_Spedizione_Abbonamento__c','Tipo_Spese_Spedizione_Vendite_Dirette__c','Tipo_causale_sconto__c','CIG__c','CUP__c','Codice_Ordine_PressDi__c','Inibisci_Gracing__c','Richiedi_Fattura__c','Fattura_a_intermediario__c','Fattura_a_azienda__c','Zuora__Status__c','Zuora__Account__c','Zuora__SubscriptionEndDate__c','Numero_di_Cicli__c','Label_Run_processed__c','Label_Run_IsError__c'});
            subscription = (Zuora__Subscription__c) controller.getRecord();
            System.debug('************************** Fattura_a_azienda__c **************' + subscription.Fattura_a_azienda__c);
	}

	public PageReference goToAvviso(){
		PageReference ref = new PageReference('/'+subscription.Id);
		return ref;
	}

	public PageReference createQuoteAction(){
        PageReference ref = null;

        try {

            if(subscription.Stato_Sfdc__c.containsIgnoreCase('Chiusa')){
                throw new DomusException('Non è possibile rinnovare una subscription in stato \''+subscription.Stato_Sfdc__c+'\'');
            }

            // se la subscription è in errore non la devi poter rinnovare
            if(!subscription.Label_Run_processed__c || subscription.Label_Run_IsError__c)
                throw new DomusException('Questa subscription è in errore. Non puoi rinnovarla.');

            // controllo se la subscription è stata già rinnovata
            if(Database.countQuery('SELECT COUNT() FROM Zuora__Subscription__c WHERE Subscription_Precedente__c = \'' + subscription.Id + '\'') > 0) 
                throw new DomusException('Questa subscription è stata già rinnovata in precedenza.');

            // controllo se è stata già creata una quote di rinnovo ma non è stata sincronizzata
            List<zqu__Quote__c> quoteRinnovo = [SELECT Id FROM zqu__Quote__c WHERE Subscription_Precedente__c = :subscription.Id AND zqu__Status__c != 'Sent to Z-Billing'];
            System.debug(loggingLevel.Error, '*** quoteRinnovo: ' + quoteRinnovo);
            if(!quoteRinnovo.isEmpty()){
                ref = Page.ViewQuote;
                ref.getParameters().put('id',quoteRinnovo.get(0).Id);
                return ref;
            }

            // controllo che lo stato della subscription sia compatibile con il rinnovo
            if(subscription.Zuora__Status__c != 'Active' && subscription.Zuora__Status__c != 'Expired' && subscription.Zuora__Status__c != 'Cancelled')
                throw new DomusException('Questa subscription non può essere rinnovata. Possono essere rinnovate solo subscriptions attive, scadute o interrotte.');

    		Id quoteId = createQuote();
            System.debug(loggingLevel.Error, '*** quoteId: ' + quoteId);
            ref = Page.WizardCart;
            ref.getParameters().put('id', quoteId);
            ref.getParameters().put('skipToBilling', 'true');
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, e.getMessage()));
            hasError = true;
        }
        return ref;
	}

	private Id createQuote(){
        hasError = false;

        // cerco l'id del rateplan per il rinnovo
        Set<String> originalProductRatePlanIds = new Set<String>();
        for(Zuora__SubscriptionRatePlan__c subRatePlan : [SELECT Id, Zuora__OriginalProductRatePlanId__c 
                                                        FROM Zuora__SubscriptionRatePlan__c
                                                         WHERE Zuora__Subscription__c = :subscription.Id]){
            originalProductRatePlanIds.add(subRatePlan.Zuora__OriginalProductRatePlanId__c);                
        }
        List<zqu__ProductRatePlan__c> productRatePlans = [SELECT Id,TipoProdotto__c
                                                            FROM zqu__ProductRatePlan__c
                                                            WHERE zqu__ZuoraId__c IN :originalProductRatePlanIds];
        Id ratePlanPerRinnovo = null;
        for(zqu__ProductRatePlan__c ratePlan : productRatePlans){
            if(ratePlan.TipoProdotto__c == 'Abbonamento' || ratePlan.TipoProdotto__c == 'Bundle Abbonamenti' || ratePlan.TipoProdotto__c == 'Bundle Misti'){
                ratePlanPerRinnovo = ratePlan.Id;
                break;
            }
        }
        if(ratePlanPerRinnovo == null)
            throw new DomusException('Attenzione: questa subscription non contiene prodotti rinnovabili. Verifica che ci sia un abbonamento, un bundle abbonamenti oppure un bundle misto.');


        zqu.quote quote = zqu.Quote.getNewInstance();
        zqu__Quote__c quoteObj = quote.getSObject();
        quoteObj.zqu__Account__c = subscription.Zuora__Account__c;
        quoteObj.zqu__StartDate__c = subscription.Zuora__SubscriptionEndDate__c;
        quoteObj.zqu__ValidUntil__c  = subscription.Zuora__SubscriptionEndDate__c;
        quoteObj.zqu__InitialTerm__c = 1;
        quoteObj.zqu__RenewalTerm__c = 1;
        quoteObj.zqu__InitialTermPeriodType__c = 'Week';
        quoteObj.zqu__RenewalTermPeriodType__c = 'Week';
        quoteObj.zqu__AutoRenew__c = false;

        if(subscription.Numero_di_Cicli__c  != null){
            quoteObj.Numero_di_Cicli_Text__c = String.valueOf(subscription.Numero_di_Cicli__c + 1);
        }
        quote.save();

        List<Testata__c> testataList = DomusUtil.getTestataList(ratePlanPerRinnovo);
        Set<Id> testataIdSet = new Set<Id>();
        for(Testata__c t : testataList){
            testataIdSet.add(t.id);
        }
        List<Id> testataIdList = new List<Id>(testataIdSet);
        Calendario__c startCalendar = DomusUtil.getCalendarioStartCopy(testataIdList, subscription.Zuora__SubscriptionEndDate__c);

        List<zqu__ProductRatePlanCharge__c> chargeList = DomusUtil.getChargeList(ratePlanPerRinnovo);
        System.debug(loggingLevel.Error, '*** chargeList: ' + chargeList);
        Date startDate = startCalendar.Data_Uscita__c;
        Date endDate = startCalendar.Data_Uscita__c;

        Map<String, Testata__c> testataMap = new Map<String, Testata__c>();
        for(Testata__c t : testataList){
            testataMap.put(t.IssueCalendar__c, t);
        }

        Set<String> supportSet = new Set<String>();
        for(zqu__ProductRatePlanCharge__c charge : chargeList){
            if(charge.TipoProdotto__c == 'Abbonamento'){
                Testata__c testataTmp = testataMap.get(charge.IssueCalendar__c);
                // DH-369
                //Calendario__c startCalendarTmp = DomusUtil.getCalendarioStartCopy(testataTmp.Id, startDate);
                Calendario__c startCalendarTmp = DomusUtil.getCalendarioStartCopySubscription(testataTmp.Id, startDate);
                Calendario__c endCalendarTmp = DomusUtil.getCalendarioEndCopyByTestata(testataTmp.Id, startDate, Integer.valueOf(charge.Uscite__c));
                if(endCalendarTmp == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: Ultima copia d\'uscita non trovata. Inserire ulteriori copie di uscita per la testata "'+charge.IssueCalendar__c+'"'));
                    hasError = true;
                    return null;
                }
                if(endCalendarTmp.Data_Uscita__c > endDate){
                    endDate = endCalendarTmp.Data_Uscita__c;
                }
                if(String.isBlank(quoteObj.Testate__c)){
                    quoteObj.Testate__c = charge.Testata__c;
                    quoteObj.Dettaglio_Copie_per_Testata__c = startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }else{
                    quoteObj.Testate__c += ';'+charge.Testata__c;
                    quoteObj.Dettaglio_Copie_per_Testata__c += ';'+startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }
                if(String.isNotBlank(charge.Supporto__c)){
                    supportSet.add(charge.Supporto__c);
                }
            }
        }
        quoteObj.Supporto__c = String.join(new List<String>(supportSet), ';');
        quoteObj.zqu__AutoRenew__c = false;
        quoteObj.Quote_Status__c = 'Draft';
        DomusUtil.setQuoteDateParameters(startDate, endDate, quoteObj);
        setQuoteValuesFromSub(quoteObj, subscription);

        if(quoteObj.Metodo_di_pagamento__c == 'Carta di credito'){
            quoteObj.zqu__InvoiceTargetDate__c = Date.today().addYears(1);
            quoteObj.zqu__Service_Activation_Date__c = quoteObj.zqu__StartDate__c;
        }
        quote.save();

        String tipoProdotto;
        List<zqu__ProductRatePlan__c> ratePlanList = [SELECT Id, TipoProdotto__c FROM zqu__ProductRatePlan__c WHERE Id = :ratePlanPerRinnovo];
        System.debug(loggingLevel.Error, '*** ratePlanList: ' + ratePlanList);
        for(zqu__ProductRatePlan__c rp : ratePlanList){
            if(rp.TipoProdotto__c == 'Abbonamento'){
                tipoProdotto = rp.TipoProdotto__c;
            }else if(rp.TipoProdotto__c == 'Bundle Misti' || rp.TipoProdotto__c == 'Bundle Abbonamenti'){
                tipoProdotto = rp.TipoProdotto__c;
            }
        }

        Id quoteRatePlanId;
        System.debug(loggingLevel.Error, '*** tipoProdotto: ' + tipoProdotto);
        if(tipoProdotto == 'Abbonamento'){
            quoteRatePlanId = DomusUtil.setAbbonamento(quote.getId(), ratePlanPerRinnovo, startCalendar.Id);
        } else if (tipoProdotto.containsIgnoreCase('Bundle')){
            quoteRatePlanId = DomusUtil.setBundle(quote.getId(), ratePlanPerRinnovo, startCalendar.Id, tipoProdotto);
        }

        if(quoteRatePlanId != null){
            //List<Movimentazione_Stock__c> movimentazioniDaCreare = new List<Movimentazione_Stock__c>();
            List<zqu__QuoteRatePlanCharge__c> qChargeList = DomusUtil.getQuoteChargeList(quoteRatePlanId);
            System.debug(loggingLevel.Error, '*** qChargeList: ' + qChargeList);
            if(qChargeList != null && qChargeList.size()>0){
                for(zqu__QuoteRatePlanCharge__c qCharge : qChargeList){
                    System.debug('Charge ' + qCharge.name + ', tipo: ' + qCharge.Tipo_Charge__c);
                    System.debug(loggingLevel.Error, '*** qCharge: ' + qCharge);
                    if(qCharge.Tipo_Charge__c == 'Abbonamento'){
                        qCharge.zqu__Quantity__c = 1;
                        qCharge.zqu__Total__c = qCharge.zqu__Quantity__c * qCharge.zqu__EffectivePrice__c;
                        qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                    }
                    if(qCharge.Tipo_Charge__c == 'Vendita Diretta'){
                        List<Zuora__SubscriptionProductCharge__c> chargeVenditaDirettaList = [SELECT Id,Prodotto_Vendita_Diretta__c,Prodotto_Vendita_Diretta__r.Supporto__c,Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c,
                                                                                                    IntQuantita__c,Prodotto_Vendita_Diretta__r.Riferimento_Magazzino__c
                                                                                            FROM Zuora__SubscriptionProductCharge__c
                                                                                            WHERE Zuora__Subscription__c = :subscription.Id
                                                                                            AND Product_Rate_Plan_Charge__r.zqu__ProductRatePlan__c = :ratePlanPerRinnovo
                                                                                            AND Tipo_Prodotto__c = 'Vendita Diretta'];
                        System.debug(loggingLevel.Error, '*** chargeVenditaDirettaList: ' + chargeVenditaDirettaList);
                        if(!chargeVenditaDirettaList.isEmpty()){
                            Zuora__SubscriptionProductCharge__c chargeVenditaDiretta = chargeVenditaDirettaList.get(0);
                            qCharge.Prodotto_Vendita_Diretta__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__c;
                            qCharge.Supporto__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Supporto__c; 
                            /*movimentazioniDaCreare.add(
                                new Movimentazione_Stock__c(
                                    Codice_Prodotto_SAP__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c,
                                    Quantita__c = chargeVenditaDiretta.IntQuantita__c,
                                    Segno_del_movimento_del__c = '-',
                                    Riferimento_Magazzino__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Riferimento_Magazzino__c,
                                    Stato__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Supporto__c != 'Digitale' ? 'Nuovo' : 'Recepito da SAP',
                                    Quote__c = quote.getId(),
                                    Prodotto_Vendita_Diretta__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__c
                                )
                            );*/
                        }

                    }
                }
                update qChargeList;
                //insert movimentazioniDaCreare;
            }
        }
        DomusUtil.setQuoteChargeDateParameters(startDate, endDate, quoteObj.Id); 
        return quote.getId();
	}
	
    private void setQuoteValuesFromSub(zqu__Quote__c quoteObj, Zuora__Subscription__c sub){
		quoteObj.Blocco_Rinnovo__c = sub.Blocco_Rinnovo__c;
        quoteObj.ConAvvisi__c = sub.ConAvvisi__c;
        quoteObj.Mantieni_Offerta__c = sub.Mantieni_Offerta__c;
        quoteObj.Regalo__C = sub.Regalo__C;
        quoteObj.Metodo_di_pagamento__c = sub.Metodo_di_pagamento__c;
        quoteObj.Intermediario__c = sub.Intermediario__c;
        quoteObj.Canale_di_acquisizione__c = sub.Canale_di_acquisizione__c;
        quoteObj.Subscription_Precedente__c = sub.Id;
        quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = sub.Tipo_Spese_Spedizione_Abbonamento__c;
        quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = sub.Tipo_Spese_Spedizione_Vendite_Dirette__c;
        quoteObj.Tipo_causale_sconto__c = sub.Tipo_causale_sconto__c;  
        quoteObj.CIG__c = sub.CIG__c;
        quoteObj.CUP__c = sub.CUP__c;
        quoteObj.Codice_Ordine_PressDi__c = sub.Codice_Ordine_PressDi__c;
        quoteObj.Inibisci_Gracing__c = sub.Inibisci_Gracing__c;
        quoteObj.Metodo_di_pagamento__c = sub.Metodo_di_pagamento__c;
        quoteObj.Richiedi_Fattura__c = sub.Richiedi_Fattura__c;
        if(sub.Fattura_a_intermediario__c == true) quoteObj.Fattura_a__c = 'Intermediario';
        if(sub.Fattura_a_azienda__c == true) quoteObj.Fattura_a__c = 'Azienda';
        if(sub.Fattura_a_azienda__c == false && sub.Fattura_a_intermediario__c == false) quoteObj.Fattura_a__c = '--None--';
	}

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }
}