@isTest
private class ZTest_InformativaUtils{
	
	@isTest
	static void insertContact(){
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Account Test' LIMIT 1];
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Contact1', LastName = 'Test1', Email = 'email1@test.it', Fonte__c = 'Email', Cee_Nazione__c = 'CEE');
		insert c;
		Test.stopTest();
	}

	@isTest
	static void updateContact(){
		Test.startTest();
		Contact c = [SELECT Id FROM Contact WHERE Name = 'Contact Test' LIMIT 1];
		c.Trattamento_Dati__c = 'SI';
		update c;
		Test.stopTest();
	}

	@testSetup
	public static void testSetup(){
		Account a = new Account(Name = 'Account Test', RecordTypeId = '0120O000000oofrQAA');
        insert a;

        Contact c = new Contact(AccountId = a.Id, FirstName = 'Contact', LastName = 'Test', Email = 'email@test.it', Fonte__c = 'Email', Trattamento_Dati__c = 'NO',  Cee_Nazione__c = 'CEE');
        insert c;

        Informativa_Privacy__c ip = new Informativa_Privacy__c(Name = 'Informativa Corrente', URL_Informativa__c = 'Test URL', Versione_Informativa__c = '1');
        insert ip;

        CustomPermissions__c cp = new CustomPermissions__c(Executetrigger__c = true);
        insert cp;
	}
}