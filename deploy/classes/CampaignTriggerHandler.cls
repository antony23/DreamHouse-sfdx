public class CampaignTriggerHandler {
    public static Boolean skipTrigger = false;

    public static void onBeforeInsert(List<Campaign> triggerNew, Map<Id, Campaign> triggerNewMap) { 
        setFields(triggerNew);
    }

    public static void onAfterInsert(List<Campaign> triggerNew, Map<Id, Campaign> triggerNewMap) {
        checkCoerenzaProdotti(triggerNew);
    }

    public static void onBeforeUpdate(List<Campaign> triggerOld, List<Campaign> triggerNew, Map<Id, Campaign> triggerOldMap, Map<Id, Campaign> triggerNewMap){   
        setFields(triggerNew);
    }

    public static void onAfterUpdate(List<Campaign> triggerOld, List<Campaign> triggerNew, Map<Id, Campaign> triggerOldMap, Map<Id, Campaign> triggerNewMap){   
        checkCoerenzaProdotti(triggerNew);
    }

    public static void onBeforeDelete(List<Campaign> triggerOld, Map<Id, Campaign> triggerOldMap){  
    }

    public static void onAfterDelete(List<Campaign> triggerOld, Map<Id, Campaign> triggerOldMap){  
    }

    private static void checkCoerenzaProdotti(List<Campaign> triggerNew){
        for(Campaign campagna : triggerNew){
            if(String.isNotBlank(campagna.Prodottoassociatoallosconto__c)){
                Set<String> prodottiAssociatiAlloSconto = new Set<String>(campagna.Prodottoassociatoallosconto__c.split(';'));
                if(
                    (prodottiAssociatiAlloSconto.contains('Abbonamento') && !prodottiAssociatiAlloSconto.contains('Prodotto vendita diretta') 
                    && campagna.Prodotto_Vendita_Diretta__c != null) ||
                    (prodottiAssociatiAlloSconto.contains('Prodotto vendita diretta') && !prodottiAssociatiAlloSconto.contains('Abbonamento') 
                    && campagna.Rate_Plan_in_Campagna__c != null) ){

                    campagna.addError('I prodotti associati allo sconto non sono coerenti con i prodotti associati alla campagna.');
                }
                    
            }
            if(campagna.Tipo_Promozione__c == 'Codice' && String.isBlank(campagna.Descrizione_Codice__c))
                campagna.Codice_campagna__c.addError('Compila il codice della campagna per salvare');
        }

    }
    
    /*
    Omaggio : se seleziono omaggio + PVD e Abb insieme
    Omaggio Abbonamento : se seleziono omaggio + Abb
    Omaggio Vendite Dirette: se seleziono omaggio + PVD
    Sconto % Abbonamenti : omaggio false + tipo sconto perce + tipo abbo + Rate Plan in Promozione blank
    Sconto % Vendite Dirette: omaggio false + tipo sconto perce + tipo PDV + Prodotto Vendita Diretta blank
    Sconto Valore [Abbonamenti]: else finale
    */
    private static void setFields(List<Campaign> triggerNew){
        Map<String,Id> ratePlanScontoByName = new Map<String,Id>();
        for(zqu__ProductRatePlan__c ratePlan : [SELECT Id,Name FROM zqu__ProductRatePlan__c WHERE zqu__ZProduct__r.Name = 'SCONTO']){
            ratePlanScontoByName.put(ratePlan.Name,ratePlan.Id);
        }
        for(Campaign c : triggerNew){
            if(c.Tipo_Promozione__c != 'Altro' && c.Omaggio__c && c.Prodottoassociatoallosconto__c.contains('Abbonamento') && c.Prodottoassociatoallosconto__c.contains('Prodotto vendita diretta')){
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Omaggio');

            }else if(c.Tipo_Promozione__c != 'Altro' && c.Omaggio__c && c.Prodottoassociatoallosconto__c == 'Abbonamento'){
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Omaggio Abbonamento');

            }else if(c.Tipo_Promozione__c != 'Altro' && c.Omaggio__c && c.Prodottoassociatoallosconto__c == 'Prodotto vendita diretta'){
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Omaggio Vendite Dirette');

            }else if(c.Tipo_Promozione__c != 'Altro' && !c.Omaggio__c && c.Tiposconto__c == 'Sconto percentuale' && c.Prodottoassociatoallosconto__c.contains('Abbonamento') && c.Rate_Plan_in_Campagna__c == null){
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Sconto % Abbonamenti');    

            }else if(c.Tipo_Promozione__c != 'Altro' && !c.Omaggio__c && c.Tiposconto__c == 'Sconto percentuale' && c.Prodottoassociatoallosconto__c.contains('Prodotto vendita diretta') && c.Prodotto_Vendita_Diretta__c == null){
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Sconto % Vendite Dirette');    

            }else{
                c.Rate_Plan_Promozionale__c = ratePlanScontoByName.get('Sconto Valore');        
            }
        }
    }
}