public class CreateQuoteUtiliyAP{

    public class CreateQuoteExtensionException extends Exception {}
    private Integer numMonth;
    private Id avvisoID;
    public Boolean hasError {get;set;}
    public CreateQuoteUtiliyAP(ApexPages.StandardController controller){

    }

    public PageReference createQuoteAction(){
        String avvisoDiRinnovoId = ApexPages.currentPage().getParameters().get('id');
        System.debug('avvisoDiRinnovoId: ' + avvisoDiRinnovoId);
        if(avvisoDiRinnovoId != null){
            Id quoteId = createQuote(avvisoDiRinnovoId);
            if (quoteId == null){
                return null;
            }   
            //PageReference ref = Page.ViewQuote;
            //ref.getParameters().put('id', quoteId);
            PageReference ref = Page.WizardCart;
            ref.getParameters().put('id', quoteId);
            ref.getParameters().put('skipToBilling', 'true');
            return ref;
        }
        return null;
    }

    public Id createQuote(Id avvisoDiRinnovoId){
        Savepoint sp = null;
        if(!Test.isRunningTest()){
            sp = Database.setSavepoint();
        }
        hasError = false;
        zqu.quote quote;
        zqu__Quote__c quoteObj;
        try{    
            avvisoID = avvisoDiRinnovoId;

            List<Avviso_di_Rinnovo__c> rinnovoList = [SELECT Abbonamento_Regalo__c,Abbonamento__c,Business_Unit__c,Campagna_di_rinnovo__c,Cancellato__c,CAP_Committente__c,Cbill__c,Cellulare_Committente__c,Ciclo_Abbonamento__c,Civico_Committente__c,Codice_Committente__c,Codice_Destinatario__c,Codice_Nazione_Destinatario__c,Codice_Offerta__c,Codice_SAP__c,Cognome_Committente__c,Cognome_Destinatario__c,Committente__c,Contact_Sold_To__c,Contatto__c,Copie_abbonamento__c,Costo_Abbonamento_Scontato__c,Costo_Abbonamento__c,Costo_Spese_Spedizione_Scontato__c,Costo_Spese_Spedizione__c,CreatedById,CreatedDate,
                                                            Data_estrazione__c,Descrizione_Campagna_di_Partenza__c,Descrizione_Codice_Offerta__c,Descrizione_Rate_Plan_di_Partenza__c,Descrizione_Rate_Plan_Offerta__c,Dipendente_ED__c,DugAbbreviataStd__c,Email_Committente__c,Frazione__c,Id,ID_Campagna_di_Partenza__c,ID_Campagna_Offerta__c,Id_Zuora_Abbonamento__c,Importo_Rateo__c,Indirizzo_Committente__c,Intermediario__c,Invia_a_Destinatario__c,Italia_Estero__c,Localita_Committente__c,Mese_Decorrenza_Nuovo_Abbonamento__c,Mese_Scadenza_Rinnovo__c,Mesi_alla_scadenza__c,Mesi_dalla_sottoscrizione__c,Message__c,
                                                            Metodo_ultimo_pagamento__c,Name,Nazione_Committente__c,Nazione_Destinatario__c,Nome_Campagna_Offerta__c,Nome_Committente__c,Nome_Destinatario__c,Numero_Attribuzione__c,Numero_di_Cicli_Text__c,Nuovo_Rinnovo_Rotomail__c,Nuovo_Rinnovo__c,Offerta_di_Rinnovo__c,Offerta__c,OwnerId,Periodo_Fatturazione__c,Presso__c,Privacy__c,Professione__c,Provincia_Committente__c,Quantita__c,Record_Type_Regola__c,Regalo__c,Regola_Avvisi_e_Solleciti__c,Rivista_Abbr__c,Rivista__c,Sconto_Fisso__c,Sconto_Percentuale__c,Sesso__c,Settore__c,
                                                            SKU_Rate_PlanOfferta__c,SKU_Rate_Plan_di_Partenza__c,SpeseDiSpedizioneOrdinarie__c,SpeseDiSpedizionePrioritarie__c,Spese_di_spedizione_split__c,Stato_Avviso__c,Subscription_Creata__c,Subscription_Zuora_Id__c,SuperatoPeriodoDiLatenza__c,Supplemento_CAP_Committente__c,Supplemento_Civico_Committente__c,SystemModstamp,Testate__c,Tipo_Avviso__c,Tipo_cliente__c,Tipo_Consegna__c,Tipo_Prodotto_di_Partenza__c,Tipo_Prodotto_Offerta__c,Tipo_Sconto__c,Tipo_subscription__c,Tipo__c,Titolo_cod__c,Titolo__c,Totale__c,Ulteriore_Titolo__c
                                                        FROM Avviso_di_Rinnovo__c
                                                        WHERE Id = :avvisoDiRinnovoId];         

            if(rinnovoList != null && rinnovoList.size()>0){
                Avviso_di_Rinnovo__c avvisoRinnovo = rinnovoList.get(0);
                Id subscriptionId = avvisoRinnovo.Abbonamento__c;
                String whereClause2 = 'Id = :subscriptionId';
                // LG cambiato fattura_a__intermediario__c con fattura_a__c
                String additionalFields2 = 'Stato_SFDC__c,Zuora__CustomerAccount__r.Zuora__Zuora_Id__c, Numero_di_Cicli__c, Fattura_a_intermediario__c, Fattura_a_azienda__c';
                String queryString2 = Utils.getSelectAllQuery('Zuora__Subscription__c', whereClause2, null, additionalFields2, true);
                List<Zuora__Subscription__c> subList = (List<Zuora__Subscription__c>) Database.query(queryString2);

                if(subList!=null && subList.size()>0){
                    Zuora__Subscription__c sub = subList.get(0);
                    Id subId = sub.Id;

                    if(avvisoRinnovo.Stato_Avviso__c != 'Confermato'/* LG ISSUE 166== 'Pagato' || avvisoRinnovo.Stato_Avviso__c == 'Rinnovato' || avvisoRinnovo.Stato_Avviso__c == 'Non Utilizzabile'*/ ){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: questo avviso si trova nello stato: \''+avvisoRinnovo.Stato_Avviso__c+'\''));
                        hasError = true;
                        return null;
                    }

                    Integer prevSubCount = Database.countQuery('SELECT COUNT() FROM Zuora__Subscription__c WHERE Subscription_Precedente__c = :subId');
                    if(prevSubCount > 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: questo abbonamento è già stato rinnovato'));
                        hasError = true;
                        return null;
                    }

                    Integer prevQuoteCount = Database.countQuery('SELECT COUNT() FROM zqu__Quote__c WHERE Subscription_Precedente__c = :subId AND zqu__Status__c = \'Sent to Z-Billing\'');
                    if(prevQuoteCount > 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: l\'abbonamento possiede già una quote'));
                        hasError = true;
                        return null;
                    }

                    if(sub.Zuora__Status__c != 'Active' && sub.Zuora__Status__c != 'Expired'){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: l\'abbonamento si trova nello stato \''+sub.Zuora__Status__c+'\' e non è rinnovabile'));
                        hasError = true;
                        return null;
                    }

                    if(sub.Stato_Sfdc__c.containsIgnoreCase('Chiusa')){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: l\'abbonamento si trova nello stato \''+sub.Stato_Sfdc__c+'\' e non è rinnovabile'));
                        hasError = true;
                        return null;
                    }

                    quote = zqu.Quote.getNewInstance();
                    System.debug('Quote Id: ' + quote.getId());
                    quoteObj = quote.getSObject();

                    if(sub.Numero_di_Cicli__c  != null){
                        quoteObj.Numero_di_Cicli_Text__c = String.valueOf(sub.Numero_di_Cicli__c + 1);
                    }
                    quoteObj.Avviso_di_Rinnovo_Iniziale__c = avvisoDiRinnovoId;
                    quoteObj.Subscription_Precedente__c = sub.Id;
                    Date subEndDate = sub.Zuora__SubscriptionEndDate__c;

                    Id ratePlanId;
                    System.debug('Mantieni offerta: ' + sub.Mantieni_Offerta__c);
                    if(sub.Mantieni_Offerta__c != 'SI'){
                        ratePlanId = avvisoRinnovo.Offerta_di_Rinnovo__c;
                    }else{
                        List<Zuora__SubscriptionRatePlan__c> subRatePlanList = [SELECT Original_Product_RatePlan__c FROM Zuora__SubscriptionRatePlan__c WHERE Zuora__Subscription__c = :subscriptionId AND (Original_Product_RatePlan__r.TipoProdotto__c = 'Abbonamento' OR Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle abbonamenti' OR Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle misti') LIMIT 1];
                        if(subRatePlanList != null && subRatePlanList.size()>0){
                            ratePlanId = subRatePlanList.get(0).Original_Product_RatePlan__c;
                        } else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: nessun rate plan trovato dalla subscription iniziale.'));
                            hasError = true;
                            if(!Test.isRunningTest()) Database.rollback(sp);
                            return null;
                        }
                    }
                    //subEndDate = Date.today();
                    //Viene recuperata la data di inizio del nuovo abbonamento
                    List<Testata__c> testataList = DomusUtil.getTestataList(ratePlanId);
                    Set<Id> testataIdSet = new Set<Id>();
                    for(Testata__c t : testataList){
                        testataIdSet.add(t.id);
                    }
                    List<Id> testataIdList = new List<Id>(testataIdSet);

                    Calendario__c startCalendar = DomusUtil.getCalendarioStartCopy(testataIdList, subEndDate);

                    Map<String, Object> subParametersMap = SubscriptionUtil.getSubscriptionParameters(ratePlanId, subEndDate, false);
                    quoteObj.Dettaglio_Copie_per_Testata__c = (String) subParametersMap.get('dettaglioCopiePerTestata');
                    quoteObj.Supporto__c = (String) subParametersMap.get('supporto');
                    quoteObj.Testate__c = (String) subParametersMap.get('testate');
                    Date startDate = (Date) subParametersMap.get('startDate');
                    Date endDate = (Date) subParametersMap.get('endDate'); 
                    quoteObj.Data_fine_ultima_copia_per_calcolo_issue__c = endDate;
                    
                    quoteObj.zqu__Account__c = sub.Zuora__Account__c;
                    //quoteObj.zqu__ZuoraAccountID__c = sub.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c;

                    System.debug('Aggiunta account del cliente');
                    //La campagna impostata dipende se l'opzione mantiene offerta è attiva
                    //Se non è attiva prende la campagna impostata nell'avviso di rinnovo
                    //Se è attiva prende la stessa campagna presente nella subscription di partenza
                    System.debug(loggingLevel.Error, '*** sub.Mantieni_Offerta__c: ' + sub.Mantieni_Offerta__c);
                    System.debug(loggingLevel.Error, '*** sub: ' + sub);
                    if(sub.Mantieni_Offerta__c != 'SI' && avvisoRinnovo.Campagna_di_rinnovo__c != null){
                        quoteObj.Campagna__c = avvisoRinnovo.Campagna_di_rinnovo__c;
                    }else if(sub.Mantieni_Offerta__c == 'SI'){
                        quoteObj.Campagna__c = sub.Campagna__c;
                    }
                    quoteObj.zqu__AutoRenew__c = false;
                    quoteObj.Quote_Status__c = 'Confirmed';
                    
                    DomusUtil.setQuoteDateParameters(startDate, endDate, quoteObj);

                    setQuoteValuesFromSub(quoteObj, sub);

                    if(sub.Numero_di_Cicli__c == null){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: non è impostato il numero di cicli sull\'abbonamento di partenza'));
                        if(!Test.isRunningTest()) Database.rollback(sp);
                        hasError = true;
                        return null;
                    }
                    quoteObj.Numero_di_Cicli_Text__c = String.valueOf(sub.Numero_di_Cicli__c+1);


                    quote.save();



                    //Aggiunta rateplan dell'abbonamento
                    System.debug('Aggiungo il rateplan ' + rateplanId + ' alla quote');

                    String tipoProdotto;
                    List<zqu__ProductRatePlan__c> ratePlanList = [SELECT Id, TipoProdotto__c FROM zqu__ProductRatePlan__c WHERE Id = :ratePlanId];
                    for(zqu__ProductRatePlan__c rp : ratePlanList){
                        if(rp.TipoProdotto__c == 'Abbonamento'){
                            tipoProdotto = rp.TipoProdotto__c;
                        }else if(rp.TipoProdotto__c == 'Bundle Misti' || rp.TipoProdotto__c == 'Bundle Abbonamenti'){
                            tipoProdotto = rp.TipoProdotto__c;
                        }
                    }

                    Id quoteRatePlanId;
                    if(tipoProdotto == 'Abbonamento'){
                        quoteRatePlanId = DomusUtil.setAbbonamento(quote.getId(), ratePlanId, startCalendar.Id);
                    } else if (tipoProdotto.containsIgnoreCase('Bundle')){
                        quoteRatePlanId = DomusUtil.setBundle(quote.getId(), ratePlanId, startCalendar.Id, tipoProdotto);
                    }
                    //Id quoteRatePlanId = DomusUtil.setAbbonamento(quote.getId(), ratePlanId, startCalendar.Id);
                    
                    List<zqu__QuoteRatePlanCharge__c> tmpChrges = [SELECT Id,NumeroUscite__c,Supporto__c,Testata__c
                                                    FROM zqu__QuoteRatePlanCharge__c
                                                    WHERE zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteObj.Id
                                                    ]; //AND Tipo_Charge__c = 'Abbonamento'
                    System.debug('charges :' + tmpChrges);  

                    System.debug('Aggiunto il quote rate plan: ' + quoteRatePlanId);
                    if(quoteRatePlanId != null){

                        //Aggiunta campagna
                        System.debug('Aggiunta campagna di rinnovo: ' + quoteObj.Campagna__c);
                        if(quoteObj.Campagna__c  != null){
                            System.debug('Aggiungo campagna di rinnovo');
                            Campaign cTmp = DomusUtil.setCampagna(quote.getId(), quoteObj.Campagna__c );
                            if(cTmp == null){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: errore durante l\'aggiunta della campagna'));
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                hasError = true;
                                return null;
                            }   
                            setQuoteCampaignValues(quote.getId(), quoteObj.Campagna__c );
                        }
                        //Salvataggio del billing account
                        System.debug('Aggiunta billing account: ' + sub.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c);
                        


                        //Aggiornamento delle charge nel caso mantieni offerta non sia attiva
                        if(sub.Mantieni_Offerta__c != 'SI'){
                            if(avvisoRinnovo.Quantita__c == null){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: non è impostata la quantità sull\'avviso di rinnovo'));
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                hasError = true;
                                return null;
                            }
                            if(avvisoRinnovo.Costo_Spese_Spedizione__c == null){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: non sono impostate le spese di spedizione sull\'avviso di rinnovo'));
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                hasError = true;
                                return null;
                            }
                            Integer numAbbCharge = [SELECT COUNT() FROM zqu__quoterateplancharge__c WHERE zqu__quoterateplan__c = :quoteRatePlanId AND Tipo_Charge__c = 'Abbonamento'];

                            //Aggiornamento campi charge
                            System.debug('Aggiorno le charge della quote');
                            List<zqu__QuoteRatePlanCharge__c> qChargeList = DomusUtil.getQuoteChargeList(quoteRatePlanId);
                            if(qChargeList != null && qChargeList.size()>0){
                                for(zqu__QuoteRatePlanCharge__c qCharge : qChargeList){
                                    System.debug('Charge ' + qCharge.name + ', tipo: ' + qCharge.Tipo_Charge__c);
                                    if(qCharge.Tipo_Charge__c == 'Abbonamento'){
                                        /*if(avvisoRinnovo.Italia_Estero__c == 'Estero' && numAbbCharge > 0){
                                            qCharge.zqu__Total__c = avvisoRinnovo.Costo_Abbonamento__c / numAbbCharge;
                                            qCharge.zqu__ListPrice__c = qCharge.zqu__Total__c/avvisoRinnovo.Quantita__c;
                                            qCharge.zqu__EffectivePrice__c = qCharge.zqu__Total__c/avvisoRinnovo.Quantita__c;
                                        }else{
                                            qCharge.zqu__Total__c = qCharge.zqu__Quantity__c * qCharge.zqu__Quantity__c * qCharge.zqu__EffectivePrice__c;
                                        }*/
                                        qCharge.zqu__Total__c = qCharge.zqu__Quantity__c * qCharge.zqu__Quantity__c * qCharge.zqu__EffectivePrice__c;
                                        qCharge.zqu__Quantity__c = avvisoRinnovo.Quantita__c;
                                        qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                                    }
                                    if(qCharge.Tipo_Charge__c == 'Vendita Diretta'){
                                        qCharge.Prodotto_Vendita_Diretta__c = qCharge.Prodotto_Vendita_Diretta__c;
                                        qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                                    }
                                }
                                update qChargeList;


                            }else {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: nessuna charge trovata corrispondente al rateplan selezionato'));
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                hasError = true;
                                return null;
                            }
                        }else{//Aggiornamento delle charge nel caso mantieni offerta sia attiva
                            Map<Id, Zuora__SubscriptionProductCharge__c> subChargeMap = new Map<Id, Zuora__SubscriptionProductCharge__c>();
                            Set<Id> productRatePlanChargeIds = new Set<Id>();
                            String whereClause4 = 'Zuora__Subscription__c = :subId';
                            String additionalFields4 = 'Prodotto_Vendita_Diretta__r.Supporto__c';
                            String queryString4 = Utils.getSelectAllQuery('Zuora__SubscriptionProductCharge__c', whereClause4, null, additionalFields4, true);
                            List<Zuora__SubscriptionProductCharge__c> subChargeList = (List<Zuora__SubscriptionProductCharge__c>) Database.query(queryString4);

                            for(Zuora__SubscriptionProductCharge__c subCharge : subChargeList){
                                subChargeMap.put(subCharge.Product_Rate_Plan_Charge__c, subCharge);
                                productRatePlanChargeIds.add(subCharge.Product_Rate_Plan_Charge__c);
                            }

                            String whereClause5 = 'zqu__ProductRatePlanCharge__c = :productRatePlanChargeIds AND zqu__QuoteRatePlan__c = :quoteRatePlanId';
                            String additionalFields5 = 'zqu__ProductRatePlanCharge__r.Tier_Price__c,zqu__ProductRatePlanCharge__r.Supporto__c';
                            String queryString5 = Utils.getSelectAllQuery('zqu__QuoteRatePlanCharge__c', whereClause5, null, additionalFields5, false);
                            List<zqu__QuoteRatePlanCharge__c> quoteChargeList = (List<zqu__QuoteRatePlanCharge__c>) Database.query(queryString5);
                            if(quoteChargeList == null || quoteChargeList.size() == 0){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: nessuna charge trovata corrispondente al rateplan selezionato'));
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                hasError = true;
                                return null;
                            }
                            System.debug('quoteChargeList: '+ quoteChargeList);
                            for(zqu__QuoteRatePlanCharge__c qCharge : quoteChargeList){
                                Decimal quantCharge = subChargeMap.get(qCharge.zqu__ProductRatePlanCharge__c).Zuora__Quantity__c;
                                Decimal total = subChargeMap.get(qCharge.zqu__ProductRatePlanCharge__c).Zuora__ExtendedAmount__c;
                                Decimal effectivePrice = subChargemap.get(qCharge.zqu__ProductRatePlanCharge__c).Zuora__Price__c;
                                Decimal discount = subChargemap.get(qCharge.zqu__ProductRatePlanCharge__c).Zuora__DiscountAmount__c;
                                String supporto = subChargemap.get(qCharge.zqu__ProductRatePlanCharge__c).Prodotto_Vendita_Diretta__r.Supporto__c;
                                Id prodottoVenditaDiretta = subChargemap.get(qCharge.zqu__ProductRatePlanCharge__c).Prodotto_Vendita_Diretta__c;
                                if(qCharge.Tipo_Charge__c == 'Abbonamento'){
                                    qCharge.zqu__Quantity__c = quantCharge;
                                    qCharge.zqu__Total__c = total;
                                    qCharge.zqu__EffectivePrice__c = effectivePrice;
                                    qCharge.zqu__ListPrice__c = qCharge.zqu__ProductRatePlanCharge__r.Tier_Price__c;
                                    qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                                }
                                if(qCharge.Tipo_Charge__c == 'Vendita Diretta'){
                                    qCharge.Prodotto_Vendita_Diretta__c = prodottoVenditaDiretta;
                                    qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                                }
                                if(qCharge.Tipo_Charge__c == 'Sconto' && discount != null){
                                    qCharge.zqu__Discount__c = discount;
                                    qCharge.zqu__EffectivePrice__c = discount;
                                }

                            }
                            update quoteChargeList;
                        }

                        DomusUtil.setQuoteChargeDateParameters(startDate, endDate, quote.getId());
                        //Impostazione del billing account
                        quote.save();
                        Id quoteId = quote.getId();

                        /*List<Movimentazione_Stock__c> movimentazioniDaCreare = new List<Movimentazione_Stock__c>();
                        List<Zuora__SubscriptionProductCharge__c> chargeVenditaDirettaList = [SELECT Id,Prodotto_Vendita_Diretta__c,Prodotto_Vendita_Diretta__r.Supporto__c,Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c,
                                                                                                    IntQuantita__c,Prodotto_Vendita_Diretta__r.Riferimento_Magazzino__c
                                                                                            FROM Zuora__SubscriptionProductCharge__c
                                                                                            WHERE Zuora__Subscription__c = :subscriptionId
                                                                                            AND Tipo_Prodotto__c = 'Vendita Diretta'];
                        for(Zuora__SubscriptionProductCharge__c chargeVenditaDiretta : chargeVenditaDirettaList){
                            movimentazioniDaCreare.add(
                                new Movimentazione_Stock__c(
                                    Codice_Prodotto_SAP__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c,
                                    Quantita__c = chargeVenditaDiretta.IntQuantita__c,
                                    Segno_del_movimento_del__c = '-',
                                    Riferimento_Magazzino__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Riferimento_Magazzino__c,
                                    Stato__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__r.Supporto__c != 'Digitale' ? 'Nuovo' : 'Recepito da SAP',
                                    Quote__c = quoteId,
                                    Prodotto_Vendita_Diretta__c = chargeVenditaDiretta.Prodotto_Vendita_Diretta__c
                                )
                            );
                        }
                        insert movimentazioniDaCreare;*/
                        return quoteId;
                    }
                    
                }else{
                    if(!Test.isRunningTest()) Database.rollback(sp);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: non è stata trovata nessun abbonamento corrispondente all\'avviso di rinnovo'));
                }
            }else{
                if(!Test.isRunningTest()) Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: non è stata trovata nessun avviso di rinnovo corrispondente'));
            }
            hasError = true;
            return null;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            if(!Test.isRunningTest()) Database.rollback(sp);
            hasError = true;
            return null;
        }
    }

    public PageReference goToAvviso(){
        PageReference ref = new PageReference('/'+avvisoID);
        return ref;
    }

    private void setQuoteValuesFromSub(zqu__Quote__c quoteObj, Zuora__Subscription__c sub){
        quoteObj.Blocco_Rinnovo__c = sub.Blocco_Rinnovo__c;
        quoteObj.ConAvvisi__c = sub.ConAvvisi__c;
        quoteObj.Mantieni_Offerta__c = sub.Mantieni_Offerta__c;
        quoteObj.Regalo__C = sub.Regalo__C;
        quoteObj.Metodo_di_pagamento__c = sub.Metodo_di_pagamento__c;
        quoteObj.Intermediario__c = sub.Intermediario__c;
        if(sub.Fattura_a_intermediario__c == true) quoteObj.Fattura_a__c = 'Intermediario';
        if(sub.Fattura_a_azienda__c == true) quoteObj.Fattura_a__c = 'Azienda';
        if(sub.Fattura_a_azienda__c == false && sub.Fattura_a_intermediario__c == false) quoteObj.Fattura_a__c = '--None--';
        quoteObj.Canale_di_acquisizione__c = sub.Canale_di_acquisizione__c;
        quoteObj.Subscription_Precedente__c = sub.Id;
        quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = sub.Tipo_Spese_Spedizione_Abbonamento__c;
        quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = sub.Tipo_Spese_Spedizione_Vendite_Dirette__c;
        quoteObj.Tipo_causale_sconto__c = sub.Tipo_causale_sconto__c;  
        quoteObj.CIG__c = sub.CIG__c;
        quoteObj.CUP__c = sub.CUP__c;
        quoteObj.Codice_Ordine_PressDi__c = sub.Codice_Ordine_PressDi__c;
        quoteObj.Inibisci_Gracing__c = sub.Inibisci_Gracing__c;
        quoteObj.Metodo_di_pagamento__c = sub.Metodo_di_pagamento__c;
        quoteObj.Richiedi_Fattura__c = sub.Richiedi_Fattura__c;
    }

    private void setQuoteCampaignValues(Id quoteId, Id campaignId){
        Campaign campagna = DomusUtil.getCampaign(campaignId);
        List<zqu__QuoteRatePlan__c> campaignQuoteList = DomusUtil.getQuoteCampaignList(quoteId);
        zqu__QuoteRatePlan__c campaignQuoteRatePlan;
        for(zqu__QuoteRatePlan__c qrp : campaignQuoteList){
            if(qrp.Campaign__c == campagna.Id){
                campaignQuoteRatePlan = qrp;
                break;
            }
        }

        DomusUtil.updateCampaignCharge(quoteId, campaignQuoteRatePlan.id, campagna);

    }

    private void cancelQuote(zqu.quote quote, zqu__quote__c quoteObj){
        quoteObj.Quote_Status__c = 'Cancelled';
        quote.save();
    }
}