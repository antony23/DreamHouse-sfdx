global class GarbageCollectorBatch implements Database.Batchable<sObject>, Schedulable {
	
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new GarbageCollectorBatch(Schema.zqu__Quote__c.getSObjectType()));
	}

	Map<Schema.sObjectType,String> queries = new Map<Schema.sObjectType,String>{
		Schema.zqu__Quote__c.getSObjectType() => 'SELECT Id,zqu__ZuoraSubscriptionID__c FROM zqu__Quote__c WHERE zqu__ZuoraSubscriptionID__c != null'
	};
	
	Schema.sObjectType oType;

	global GarbageCollectorBatch(Schema.sObjectType o) {
		oType = o;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queries.get(oType));
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		if(scope.getSObjectType() == Schema.zqu__Quote__c.getSObjectType()){
   			Set<String> subscriptionZuoraIds = new Set<String>();
   			for(zqu__Quote__c q : (List<zqu__Quote__c>) scope){
   				subscriptionZuoraIds.add(q.zqu__ZuoraSubscriptionID__c);
   			}
   			Set<Id> quoteIdsToDelete = new Set<Id>();
   			for(Zuora__Subscription__c subscription : [SELECT Id, Quote__c 
   														FROM Zuora__Subscription__c 
   														WHERE Zuora__External_Id__c IN :subscriptionZuoraIds 
   															OR Zuora__OriginalId__c IN :subscriptionZuoraIds 
   															OR Zuora__Zuora_Id__c IN :subscriptionZuoraIds]){
   				quoteIdsToDelete.add(subscription.Quote__c);
   			}
   			delete [SELECT Id FROM zqu__Quote__c WHERE Id IN :quoteIdsToDelete];
   		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}