public class UtilCodiceFiscale {

    private static final Set<String> consonants = new Set<String>{'B','C','D','F','G','H','J','K','L','M','N','P','Q',
                                                     'R','S','T','V','W','X','Y','Z'
                                                 };
    private static final Set<String> vowels = new Set<String>{'A','E','I','O','U'};

    private static final Map<Integer,String> months = new Map<Integer,String>{1=>'A',2=>'B', 3=>'C', 4=>'D',5=>'E',
                                                                 6=>'H',7=>'L',8=>'M',9=>'P',10=>'R',
                                                                 11=>'S',12=>'T'
                                                            };
                                                                
	private static final Map<String,Integer> monthsStringToInt = new Map<String,Integer>{
														'A' => 1,'B' => 2,'C' => 3,'D' => 4,'E' => 5,'H' => 6,
														'L' => 7,'M' => 8,'P' => 9,'R' => 10,'S' => 11,'T' =>12
                                                    };
                                                                
    private static final Map<String,Integer> evenValues = new Map<String,Integer>{'A'=>0, 'B'=>1, 'C'=>2, 'D'=>3, 'E'=>4,'F'=>5, 
                                                                     'G'=>6,'H'=>7, 'I'=>8, 'J'=>9, 'K'=>10,'L'=>11, 
                                                                     'M'=>12, 'N'=>13, 'O'=>14,'P'=>15, 'Q'=>16, 'R'=>17,
                                                                     'S'=>18,'T'=>19, 'U'=>20, 'V'=>21, 'W'=>22,'X'=>23, 
                                                                     'Y'=>24, 'Z'=>25, '0'=>0,'1'=>1, '2'=>2, '3'=>3, 
                                                                     '4'=>4,'5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9
                                                                };
    private static final Map<String,Integer> oddValues = new Map<String,Integer>{'A'=>1, 'B'=>0, 'C'=>5, 'D'=>7, 'E'=>9, 'F'=>13,
                                                                    'G'=>15, 'H'=>17, 'I'=>19, 'J'=>21, 'K'=>2, 'L'=>4,
                                                                    'M'=>18, 'N'=>20, 'O'=>11, 'P'=>3, 'Q'=>6, 'R'=>8,
                                                                    'S'=>12, 'T'=>14, 'U'=>16, 'V'=>10, 'W'=>22, 'X'=>25,
                                                                    'Y'=>24, 'Z'=>23, '0'=>1, '1'=>0, '2'=>5, '3'=>7,
                                                                    '4'=>9, '5'=>13, '6'=>15, '7'=>17, '8'=>19,'9'=>21
                                                                };
    
    private static final Map<Integer,String> controlValues = new Map<Integer,String>{0=>'A', 1=>'B', 2=>'C', 3=>'D',
                                                                        4=>'E', 5=>'F', 6=>'G', 7=>'H',
                                                                        8=>'I', 9=>'J', 10=>'K', 11=>'L',
                                                                        12=>'M', 13=>'N', 14=>'O', 15=>'P',
                                                                        16=>'Q', 17=>'R',18=>'S', 19=>'T',
                                                                        20=>'U', 21=>'V', 22=>'W', 23=>'X',
                                                                        24=>'Y', 25=>'Z'
                                                                    };
                                                                        
    private static final Map<String,String> omocodiaValues = new Map<String,String>{'0'=>'L', '1'=>'M', '2'=>'N', '3'=>'P',
                                                                         '4'=>'Q', '5'=>'R', '6'=>'S', '7'=>'T',
                                                                         '8'=>'U', '9'=>'V'
                                                                    };

    /** DH-1122
     *  Verifica se un codice fiscale è valido utilizzando il controllo base o quello avanzato se abilitato.
     *
     *  @param name             Nome del contatto
     *  @param surname          Cognome del contatto
     *  @param sex              Sesso del contatto
     *  @param birthday         Data di nascita del contatto
     *  @param birthcity        Luogo di nascita del contatto
     *  @param cf               Codice fiscale del contatto
     *
     *  @return Il risultato della validazione.
    **/

    public static Result compactValidateCF(String name, String surname, String sex, Date birthDay, String birthCity, String cf) {
        final Result result = new Result();

        try {
            result.isValid = validateCF(name, surname, sex, birthDay, birthCity, cf, isEnhancedFiscalCodeValidationEnabled(), 
                                            isAutomaticAttemptsEnabled(), getMaxAutomaticAttempts());
            result.isSuccess = true;
        } catch(Exception e) {
            result.error = e.getMessage();
        }

        return result;
    }

    /**
     *  Verifica se un codice fiscale è valido utilizzando il controllo base o quello avanzato se abilitato.
     *
     *  @param name             Nome del contatto
     *  @param surname          Cognome del contatto
     *  @param sex              Sesso del contatto
     *  @param birthday         Data di nascita del contatto
     *  @param birthcity        Luogo di nascita del contatto
     *  @param cf               Codice fiscale del contatto
     *
     *  @return True se il codice fiscale inserito è valido secondo il controllo utilizzato.
    **/

    public static Boolean validateCF(String name, String surname, String sex, Date birthDay, String birthCity, String cf) {
        return validateCF(name, surname, sex, birthDay, birthCity, cf, isEnhancedFiscalCodeValidationEnabled(), 
                            isAutomaticAttemptsEnabled(), getMaxAutomaticAttempts());
    }

    /**
     *  Verifica se un codice fiscale è valido utilizzando il controllo base o quello avanzato.
     *
     *  @param name             Nome del contatto
     *  @param surname          Cognome del contatto
     *  @param sex              Sesso del contatto
     *  @param birthday         Data di nascita del contatto
     *  @param birthcity        Luogo di nascita del contatto
     *  @param cf               Codice fiscale del contatto
     *  @param enhanced         Se True, il metodo effettuerà il controllo avanzato
     *  @param autoAttempts     Se True, in caso di problemi con il WS il metodo ritenterà automaticamente di contattarlo.
     *  @param maxAutoAttempts  Numero massimo di tentativi automatici effettuabili
     *
     *  @return True se il codice fiscale inserito è valido secondo il controllo utilizzato.
    **/

    public static Boolean validateCF(String name, String surname, String sex, Date birthDay, String birthCity, String cf, Boolean enhanced, Boolean autoAttempts, Integer maxAutoAttempts) {
        final String baseURL = (!Test.isRunningTest()) ? Setting__c.getValues('ValidateCF').URL__c : 'https://europe-west1-ed-websites.cloudfunctions.net/checkCFit';
        final String params = getParameterString(name, surname, sex, birthDay, birthCity, cf, enhanced);
        final Integer nAttempts = (autoAttempts && maxAutoAttempts > 1) ? maxAutoAttempts : 1;

        System.debug('URL = ' + baseURL);
        System.debug('Params = ' + params);

        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Length', '0');
        request.setEndpoint(baseURL + '?' + params);
        request.setMethod('POST');

        Http rest = new Http();

        for(Integer i = 0; i < nAttempts; i++) {
            HttpResponse response = rest.send(request);
            String body = response.getBody().trim();
            
            System.debug('Status: ' + response.getStatusCode() + ' ' + response.getStatus());
            System.debug('Message: "' + body + '"');

            if(body.equalsIgnoreCase('OK')) {
                return true;
            } else if(body.equalsIgnoreCase('KO')) {
                return false;
            } else if(i == nAttempts - 1) {
                String message = 'Si è verificato un errore imprevisto durante la validazione del codice fiscale. Il server risponde: ' + 
                                '"' + response.getStatusCode() + ' ' + response.getStatus() + ' - ' + truncate(body, 100) + '"';

                throw new WSException(message); // Risposta del WS inaspettata, probabile errore del server
            }
        }

        throw new WSException('Si è verificato un errore imprevisto durante la validazione del codice fiscale.');
    }

    public class WSException extends Exception {}

    public class FiscalCodeValidationException extends Exception {}

    private static String truncate(String message, Integer n) {
        if(n < 1 || message.length() <= n) return message;

        return message.substring(0, n) + '...';
    }

    private static String getParameterString(String name, String surname, String sex, Date birthDay, String birthCity, String cf, Boolean enhanced) {
        name      = String.isNotBlank(name)      ? name.trim().replaceAll('[\\s]+', '').toUpperCase()    : '';
        surname   = String.isNotBlank(surname)   ? surname.trim().replaceAll('[\\s]+', '').toUpperCase() : '';
        sex       = String.isNotBlank(sex)       ? sex.trim().substring(0, 1).toUpperCase()              : '';
        birthCity = String.isNotBlank(birthCity) ? birthCity.trim().toUpperCase()                        : '';
        cf        = String.isNotBlank(cf)        ? cf.trim().toUpperCase()                               : '';
        
        if(enhanced) {
            validateParameters(name, surname, sex, birthDay, birthCity, cf);
        }

        String birthDate = '';
        if(birthDay != null) {
            final Datetime dt = Datetime.newInstance(birthDay, Time.newInstance(0, 0, 0, 0));
            birthDate = dt.format('dd/MM/yyyy');
        }

        String output = '';
        
        output += 'type='       + ((enhanced) ? 2 : 1)                       + '&';
        output += 'name='       + EncodingUtil.urlEncode(name, 'UTF-8')      + '&';
        output += 'surname='    + EncodingUtil.urlEncode(surname, 'UTF-8')   + '&';
        output += 'sex='        + EncodingUtil.urlEncode(sex, 'UTF-8')       + '&';
        output += 'birthday='   + EncodingUtil.urlEncode(birthDate, 'UTF-8') + '&';
        output += 'birthcity='  + EncodingUtil.urlEncode(birthCity, 'UTF-8') + '&';
        output += 'fiscalcode=' + EncodingUtil.urlEncode(cf, 'UTF-8');

        return output;
    }

    private static void validateParameters(String name, String surname, String sex, Date birthDay, String birthCity, String cf) {
        final String[] invalidParams = new String[]{};

        if(!isNoProfit(cf)) {
            if(String.isBlank(name))        invalidParams.add('Nome');
            if(String.isBlank(surname))     invalidParams.add('Cognome');
            if(String.isBlank(sex))         invalidParams.add('Sesso');
            if(String.isBlank(birthCity))   invalidParams.add('Luogo di nascita');
            if(birthDay == null)            invalidParams.add('Data di nascita');
            if(String.isBlank(cf))          invalidParams.add('Codice Fiscale');
        }

        if(invalidParams.size() > 0) {
            throw new FiscalCodeValidationException('Codice fiscale non verificabile: '
             + String.join(invalidParams, ', ') + ' non trovat' + (invalidParams.size() > 1 ? 'i' : 'o') + '.');
        }
    }

    private static Boolean isNoProfit(String cf) {
        return cf.length() == 11 && cf.isNumeric();
    }

    public static Boolean isEnhancedFiscalCodeValidationEnabled() {
        return  (Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()).Validazione_Avanzata_Abilitata__c :
                (Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()).Validazione_Avanzata_Abilitata__c :
                 Validazione_Codice_Fiscale__c.getOrgDefaults().Validazione_Avanzata_Abilitata__c;
    }

    public static Boolean isAutomaticAttemptsEnabled() {
        return  (Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()).Tentativi_Automatici_Abilitati__c :
                (Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()).Tentativi_Automatici_Abilitati__c :
                 Validazione_Codice_Fiscale__c.getOrgDefaults().Tentativi_Automatici_Abilitati__c;
    }

    public static Integer getMaxAutomaticAttempts() {
        return  (Integer)   ((Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getUserId()).Numero_Massimo_Tentativi__c :
                            (Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()) != null) ? Validazione_Codice_Fiscale__c.getValues(UserInfo.getProfileId()).Numero_Massimo_Tentativi__c :
                             Validazione_Codice_Fiscale__c.getOrgDefaults().Numero_Massimo_Tentativi__c);
    }
	
	/** DH-1122
     *  Risultato della validazione di un codice fiscale.
    **/

    public class Result {
        private Boolean isSuccess;
        private Boolean isValid;
        private String error;

        private Result() {
            isSuccess = false;
            isValid = false;
            error = null;
        }

        public Boolean isSuccess() {
            return isSuccess;
        }

        public Boolean isValid() {
            return isValid;
        }

        public Boolean isNotValid() {
            return !isValid;
        }

        public String getError() {
            return error;
        }
    }

    /*
        Calcola il codice fiscale partendo da cognome,nome,data di nascita,sesso e codice catastale
    */                                                                      
    public static String calculateCodiceFiscale(String lastName,String firstName,Date birthDate,String gender,string codiceCatastale){
        String cf = calculateCognome(lastName) + calculateNome(firstName) + 
                    getYear(birthDate) + getMonth(birthDate) + getDay(birthDate,gender) + 
                    codiceCatastale;
        cf += calculateControllo(cf);
        return cf;
    }

    /*
        Metodo che controlla se cfNoCalc (non calcolato) può essere una trasformazione di cfCalc (calcolato) per omocodia.
        Due diverse persone potrebbero avere uguali tutte e sedici le lettere/cifre generate usando questo schema (omocodia). 
        In questo caso, l'Agenzia delle entrate provvede a sostituire sistematicamente i soli caratteri numerici 
        (a partire dal carattere numerico più a destra) con una lettera, secondo la mappa omocodiaValues.
    */
    public static boolean checkOmocodia(String cfNoCalc,String cfCalc){

        cfNoCalc = cfNoCalc.toUpperCase();

        if(cfNoCalc.length() != cfCalc.length()){
            return false;
        }

        system.debug(cfNoCalc);
        system.debug(cfCalc);

        if(calculateControllo(cfNoCalc.substring(0,cfNoCalc.length()-1)) != getChar(cfNoCalc,cfNoCalc.length()-1)){
            return false;
        }

        for(Integer i=0; i < cfCalc.length()-1; i++){

            if(! (
                    getChar(cfNoCalc,i) == getChar(cfCalc,i) || 
                    (omocodiaValues.keySet().contains(getChar(cfCalc,i)) && getChar(cfNoCalc,i) == omocodiaValues.get(getChar(cfCalc,i))) 
                 )
            ){
                return false;
            }
        }
        return true;
    }

    /*
        Nome (tre lettere)
        Vengono prese le consonanti del nome (o dei nomi, se ve ne è più di uno) nel loro ordine (primo nome, di seguito il secondo e così via) 
        in questo modo: se il nome contiene quattro o più consonanti, si scelgono la prima, la terza e la quarta, altrimenti le prime tre in ordine. 
        Se il nome non ha consonanti a sufficienza, si prendono anche le vocali; in ogni caso le vocali vengono riportate dopo le consonanti. 
        Nel caso in cui il nome abbia meno di tre lettere la parte di codice viene completata aggiungendo la lettera X
    */      
    public static String calculateNome(String firstName){
        if(String.isEmpty(firstName)) return '';

        firstName = normalizeText(firstName);
        String result = '';

        String[] consontansFirstName = new String[]{};
        String[] vowelsFirstName = new String[]{};

        //count consonants amd vowels
        for(Integer i=0; i < firstName.length(); i++){
            if(consonants.contains(getChar(firstName,i))){
               consontansFirstName.add(getChar(firstName,i));
            }else{
                vowelsFirstName.add(getChar(firstName,i));
            }
        }
        
        if(consontansFirstName.size() >= 4){
            result = consontansFirstName[0] + consontansFirstName[2] + consontansFirstName[3];
        }else if(consontansFirstName.size() == 3){
            result = consontansFirstName[0] + consontansFirstName[1] + consontansFirstName[2];
        }else{
            String[] joinChars = new String[]{};
            joinChars.addAll(consontansFirstName);
            joinChars.addAll(vowelsFirstName);
            joinChars.addAll(new String[]{'X','X'});
            result = joinChars[0] + joinChars[1] + joinChars[2];
        }

        return result;  
    }

    /*
        Cognome (tre lettere)
        Vengono prese le consonanti del cognome (o dei cognomi, se ve ne è più di uno) nel loro ordine (primo cognome, di seguito il secondo e così via). 
        Se le consonanti sono insufficienti, si prelevano anche le vocali, sempre nel loro ordine e, comunque, le vocali vengono riportate 
        dopo le consonanti. Nel caso in cui un cognome abbia meno di tre lettere, la parte di codice viene completata aggiungendo la lettera X 
        (per esempio: Fo → FOX). Per le donne, viene preso in considerazione il solo cognome da nubile.
    */
    public static String calculateCognome(String lastName){
        if(String.isEmpty(lastName)) return '';

        lastName = normalizeText(lastName);
        String result = '';
        

        String[] consontansLastName = new String[]{};
        String[] vowelsLastName = new String[]{};

        //count consonants amd vowels
        for(Integer i=0; i < lastName.length(); i++){
            if(consonants.contains(getChar(lastName,i))){
               consontansLastName.add(getChar(lastName,i));
            }else{
                vowelsLastName.add(getChar(lastName,i));
            }
        }

        String[] joinChars = new String[]{};
        joinChars.addAll(consontansLastName);
        joinChars.addAll(vowelsLastName);
        joinChars.addAll(new String[]{'X','X'});
        result = joinChars[0] + joinChars[1] + joinChars[2];

        return result;  
    }
    
    /*
        Anno di nascita (due cifre): si prendono le ultime due cifre dell'anno di nascita;
    */
    public static String getYear(Date data){   
        if(String.isEmpty(String.valueOf(data)) || String.isEmpty(String.valueOf(data.year()))) return '';
        String result = String.valueOf(data.year());                
        return result.substring(2,4);   
    }
    
    /*
        Mese di nascita (una lettera): a ogni mese dell'anno viene associata una lettera in base alla mappa "months"
    */
    public static String getMonth(Date data){
        if(String.isEmpty(String.valueOf(data)) || String.isEmpty(String.valueOf(data.month()))) return '';
        return months.get(data.month());    
    }
    
    /*
        Giorno di nascita e sesso (due cifre): si prendono le due cifre del giorno di nascita 
        (se è compreso tra 1 e 9 si pone uno zero come prima cifra); per i soggetti di sesso femminile, a tale cifra va sommato il numero 40. 
        In questo modo il campo contiene la doppia informazione giorno di nascita e sesso.
    */
    public static String getDay(Date data,String gender){
        if(String.isEmpty(String.valueOf(data)) || String.isEmpty(String.valueOf(data.day())) || String.isEmpty(gender)) return '';
        Integer result = data.day();
        if(gender == 'F')
              result += 40;
        String textResult = string.valueOf(result);
        if(textResult.length() < 2)
            textResult = '0' + textResult;
        return textResult;  
    }
    
    /*
        A partire dai quindici caratteri alfanumerici ricavati in precedenza, si determina il carattere di controllo in base a un particolare algoritmo che opera in questo modo:
        - si mettono da una parte i caratteri alfanumerici che si trovano in posizione dispari (il primo, il terzo eccetera) e da un'altra quelli
          che si trovano in posizione pari (il secondo, il quarto eccetera);
        - fatto questo, i caratteri vengono convertiti in valori numerici secondo le mappe evenValues e oddValues
        - a questo punto, i valori che si ottengono dai caratteri alfanumerici pari e dispari vanno sommati tra di loro e il risultato va diviso per 26; 
          il resto della divisione fornirà il codice identificativo, ottenuto dalla mappa controlValues
    */
    public static String calculateControllo(String value){
        value = value.toUpperCase();
        Integer sum = 0;
        String result = '';
        String currentChar;
        for(Integer i=0;i<value.length();i++){
           currentChar = getChar(value,i);
           //system.debug('chars['+i+']'+currentChar);
           if((math.mod(i, 2))==0){
            if(oddValues.get(currentChar) == null) return null;
            sum += oddValues.get(currentChar);
           }else{
            if(evenValues.get(currentChar) == null) return null;
            sum += evenValues.get(currentChar);
           }            
        }
        Integer controlInt = math.mod(sum, 26);
        result = controlValues.get(controlInt);
        return result;
    }
    
    private static String getChar(String s,Integer i){
        return s.substring(i,i+1);
    }
    
    private static String normalizeText(String text){
        text = text.toUpperCase();
        text = text.replaceAll('[^A-Z]','');     
        return text;
    }
    
    /*

	*/
    public static Boolean isValidCf(String cf){
        if(String.isNotBlank(cf) && cf.length() == 16 && cf.substring(15,16).equalsIgnoreCase(calculateControllo(cf.substring(0,15)))){
            return true;
        }
        /*
        // DH-685 //
        if(String.isNotBlank(cf) && cf.length() == 11 && cf.isNumeric() ) {
            System.debug('Ok per partita IVA');
            return true;
        }
        // FINE DH-685 //
        */
        else{
            return false;
        }
    }

    public class Person{
        public Date d;
        public String sex;
        
        public Person(Date d,String sex){
            this.d = d;
            this.sex = sex;
        }
    }
    
    public static Person getPersonInfo(String cf){
        Integer y = Integer.valueOf(cf.substring(6,8));
        Integer m = monthsStringToInt.get(cf.substring(8,9));
        Integer d = Integer.valueOf(cf.substring(9,11));
        
        String sex = d >= 40 ? 'F' : 'M';
        d = d >= 40 ? (d-40) : d;
        y = y <=10 ? (2000+y) : (1900+y);
        
        Date data = Date.newInstance(y,m,d);
        
        return new Person(data,sex);
    }
    
}