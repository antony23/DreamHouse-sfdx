public class MovimentazioneStockTriggerHandler {

	public static Boolean skip = false;

	public void onBeforeInsert(List<Movimentazione_Stock__c> triggerNew){
		setFields(triggerNew);
	}	

	public void onBeforeUpdate(List<Movimentazione_Stock__c> triggerNew){
		setFields(triggerNew);
	}

	private void setFields(List<Movimentazione_Stock__c> triggerNew){
		Set<Id> subscriptionIds = new Set<Id>();
		Set<Id> prodottiVendita = new Set<Id>();
		for(Movimentazione_Stock__c mov : triggerNew){
			subscriptionIds.add(mov.Subscription__c);
			prodottiVendita.add(mov.Prodotto_Vendita_Diretta__c);
		}
		Map<Id,Prodotto_Vendita_Diretta__c> prodottiById = new Map<Id,Prodotto_Vendita_Diretta__c>(
			[SELECT Id,Supporto__c FROM Prodotto_Vendita_Diretta__c WHERE Id IN :prodottiVendita]
		);

		Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>(
			[SELECT Id,Tipo_Consegna_Vendite_Dirette__c,Tipo_Spese_Spedizione_Vendite_Dirette__c,Zuora__CustomerAccount__r.Sold_To_Salesforce__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]
		);

		for(Movimentazione_Stock__c mov : triggerNew){
			Prodotto_Vendita_Diretta__c pvd = prodottiById.get(mov.Prodotto_Vendita_Diretta__c);
			if(pvd != null && pvd.Supporto__c == 'Digitale'){
				mov.Stato__c = 'Recepito da SAP';
			}
			if(mov.Stato__c == 'Trasmesso') continue;
			Zuora__Subscription__c subscription = subscriptionsMap.get(mov.Subscription__c);
			if(subscription != null){
				mov.Contact__c = mov.Contact__c == null ? subscription.Zuora__CustomerAccount__r.Sold_To_Salesforce__c : mov.Contact__c;
				mov.Tipo_consegna__c = mov.Tipo_consegna__c == null ? subscription.Tipo_Consegna_Vendite_Dirette__c : mov.Tipo_consegna__c;
				mov.Tipo_Spedizione__c = mov.Tipo_Spedizione__c == null ? subscription.Tipo_Spese_Spedizione_Vendite_Dirette__c : mov.Tipo_Spedizione__c;
			}
		}

		Map<Id,Asset> mapMSAsset = new Map<Id,Asset>();
		List<Asset> assetList = [SELECT Id, Movimentazione_Stock__c FROM Asset WHERE Movimentazione_Stock__c != null AND Movimentazione_Stock__c IN :triggerNew];
		for(Asset ass : assetList){
			mapMSAsset.put(ass.Movimentazione_Stock__c,ass);
		}
		
	}

	 public void onAfterUpdate(List<Movimentazione_Stock__c> triggerOld, List<Movimentazione_Stock__c> triggerNew, Map<Id, Movimentazione_Stock__c> triggerOldMap, Map<Id, Movimentazione_Stock__c> triggerNewMap){   
	 	sendFullfilmentManageAssets(triggerNew);
	 }

	 public void onAfterInsert(List<Movimentazione_Stock__c> triggerOld, List<Movimentazione_Stock__c> triggerNew, Map<Id, Movimentazione_Stock__c> triggerOldMap, Map<Id, Movimentazione_Stock__c> triggerNewMap){   
	 	sendFullfilmentManageAssets(triggerNew);
	 }

	 private static void sendFullfilmentManageAssets(List<Movimentazione_Stock__c> triggerNew){
	 	Map<Id,Movimentazione_Stock__c> movimentazioniStock = new Map<Id,Movimentazione_Stock__c>(
	 		[SELECT Id, Stato__c, Quantita__c, Subscription__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__c, Subscription_Charge__c,
            Subscription__r.Zuora__Account__c, Subscription__c, Subscription__r.Zuora__Account__r.Codice_Cliente__c, Subscription__r.Zuora__Account__r.Name,
            Prodotto_Vendita_Diretta__r.Name, Prodotto_Vendita_Diretta__r.Supporto__c, Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c, Subscription__r.Zuora__Zuora_Id__c,
            Subscription__r.Tipo_Spese_Spedizione_Vendite_Dirette__c, Subscription__r.Totale__c, Subscription__r.Tipo_Consegna_Vendite_Dirette__c
            FROM Movimentazione_Stock__c 
            WHERE Id IN :triggerNew]
        );

		Set<Id> contactIdList = new Set<Id>();
	 	Map<Id,List<Asset>> movimentazioniStockRecepiteDaSAP = new Map<Id,List<Asset>>();
	 	Set<Id> subscriptionIdsDaInviareASAP = new Set<Id>();
	 	for(Movimentazione_Stock__c movimentazioneStock : movimentazioniStock.values()){
	 		if(movimentazioneStock.Stato__c == 'Recepito da SAP'){
	 			if(movimentazioneStock.Subscription__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__c != null){
	 				contactIdList.add(movimentazioneStock.Subscription__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);
	 			}
	 			movimentazioniStockRecepiteDaSAP.put(movimentazioneStock.Id, new List<Asset>());
	 		}
	 		if(movimentazioneStock.Stato__c == 'Da Trasmettere'){
	 			subscriptionIdsDaInviareASAP.add(movimentazioneStock.Subscription__c);
	 		}
	 	}
	 	System.debug(loggingLevel.Error, '*** subscriptionIdsDaInviareASAP: ' + subscriptionIdsDaInviareASAP);

	 	Map<Id, Contact> contactMap = new Map<Id,Contact> ([SELECT Id, Name, MailingCountry, Dug__c, MailingStreet, Civico__c, MailingCity, MailingPostalCode, 
                                        MailingState, Id_Area_Nazione__c, Area_Nazione__c, Presso__c 
                                        FROM Contact WHERE Id IN :contactIdList]);

	 	List<Asset> prodottiVenditaDiretta = new List<Asset>();
	 	for(Asset prodottoVenditaDiretta : [SELECT Id, Inviata__c, Movimentazione_Stock__c 
	 										FROM Asset 
	 										WHERE Movimentazione_Stock__c IN :movimentazioniStockRecepiteDaSAP.keySet()]){
			movimentazioniStockRecepiteDaSAP.get(prodottoVenditaDiretta.Movimentazione_Stock__c).add(prodottoVenditaDiretta);
	 	}

	 	for(Id movimentazioneStockId : movimentazioniStockRecepiteDaSAP.keySet()){
	 		System.debug(loggingLevel.Error, '*** movimentazioneStockId: ' + movimentazioneStockId);
	 		if(movimentazioniStockRecepiteDaSAP.get(movimentazioneStockId).isEmpty()){ // non è ancora stato creato un Asset
	 			Asset a = creaProdottoVenditaDiretta(movimentazioniStock.get(movimentazioneStockId),contactMap);
	 			System.debug(loggingLevel.Error, '*** a: ' + a);
	 			prodottiVenditaDiretta.add(a); // lo creo
	 		}else{ // è stato creato
	 			for(Asset prodottoVenditaDiretta : movimentazioniStockRecepiteDaSAP.get(movimentazioneStockId)){
	 				prodottoVenditaDiretta.Inviata__c = true; // lo metto ad inviato
	 				prodottiVenditaDiretta.add(prodottoVenditaDiretta);
	 			}
	 		}
	 	}
	 	
	 	Database.upsert(prodottiVenditaDiretta,false);

	 	List<Zuora__Subscription__c> subscriptionsDaInviareASAP = new List<Zuora__Subscription__c>();        
        for(Id subscriptionIdDaInviareASAP : subscriptionIdsDaInviareASAP){
        	subscriptionsDaInviareASAP.add(new Zuora__Subscription__c(Id=subscriptionIdDaInviareASAP, Movimentazione_da_inviare__c = true));
        	System.debug('@@@ '+subscriptionsDaInviareASAP);
        }
        System.debug(loggingLevel.Error, '*** subscriptionsDaInviareASAP: ' + subscriptionsDaInviareASAP);
        //SubscriptionTriggerHandler.skipTrigger = true;
        update subscriptionsDaInviareASAP;
        //SubscriptionTriggerHandler.skipTrigger = false;
	 }

	 private static Asset creaProdottoVenditaDiretta(Movimentazione_Stock__c movimentazioneStock, Map<Id, Contact> contactMap){
        Asset prodottoVenditaDiretta = new Asset(
            AccountId = movimentazioneStock.Subscription__r.Zuora__Account__c,
            Name = movimentazioneStock.Prodotto_Vendita_Diretta__r.Name,
            Subscription_Vendite_Dirette__c = movimentazioneStock.Subscription__c,
            Subscription_Vendite_Dirette_Zuora_Id__c = movimentazioneStock.Subscription__r.Zuora__Zuora_Id__c,
            Subscription_Charge__c = movimentazioneStock.Subscription_Charge__c,
            Copia_digitale__c = movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Digitale' || movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Carta + Digitale',
            Copia_cartacea__c = movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Carta + Digitale' || movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Carta' || movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Cartaceo' || movimentazioneStock.Prodotto_Vendita_Diretta__r.Supporto__c == 'Altro',
            Inviata__c = true,
            Disponibile__c = true,
            Codice_Prodotto__c = movimentazioneStock.Prodotto_Vendita_Diretta__r.Codice_Prodotto_SAP__c,
            Movimentazione_Stock__c = movimentazioneStock.Id,
            Quantity = movimentazioneStock.Quantita__c,
            Tipoconsegna__c = movimentazioneStock.Subscription__r.Tipo_Consegna_Vendite_Dirette__c,
            Tipospedizione__c = movimentazioneStock.Subscription__r.Tipo_Spese_Spedizione_Vendite_Dirette__c,
            Copia_omaggio__c = movimentazioneStock.Subscription__r.Totale__c == 0
        );
        Contact soldTo = contactMap.get(movimentazioneStock.Subscription__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);
        if(soldTo != null){
	    	prodottoVenditaDiretta.ContactId = soldTo.Id;
	        prodottoVenditaDiretta.Dettagli_Cliente__c = soldTo.Name;
	        prodottoVenditaDiretta.Nazione__c = soldTo.MailingCountry;
	        prodottoVenditaDiretta.Indirizzo__c = soldTo.MailingStreet;
	        prodottoVenditaDiretta.Citt__c = soldTo.MailingCity;
	        prodottoVenditaDiretta.Informazioni_Indirizzo__c = soldTo.MailingPostalCode + ' ' +  soldTo.MailingCity;
	        prodottoVenditaDiretta.Codice_Postale__c = soldTo.MailingPostalCode;
	        prodottoVenditaDiretta.Provincia__c = soldTo.MailingState;
	        prodottoVenditaDiretta.Dettaglio_Zona__c = String.isNotBlank(soldTo.Id_Area_Nazione__c) ? Integer.valueOf(soldTo.Id_Area_Nazione__c) : null;
	        prodottoVenditaDiretta.Zona__c = soldTo.Area_Nazione__c;
	        prodottoVenditaDiretta.Presso__c = soldTo.Presso__c;
        }else{
        	prodottoVenditaDiretta.Error__c = true;
        	prodottoVenditaDiretta.Error_Message__c = 'Il billing account della subscription non ha un Sold To valido';
        }
        
        return prodottoVenditaDiretta;
	 }

}