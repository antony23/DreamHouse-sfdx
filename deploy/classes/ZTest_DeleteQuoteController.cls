@isTest
private class ZTest_DeleteQuoteController {
	
	@isTest
	static void testCancelQuoteFlow1() {

		Test.startTest();

		Account a = [SELECT Id FROM Account WHERE Name = 'Test Account'];
		Test.setCurrentPage(Page.DeleteQuote);
		ApexPages.currentPage().getParameters().put('id', a.Id);
		zqu__Quote__c quote = [SELECT Id FROM zqu__Quote__c WHERE Name = 'Test Quote 1'];
		DeleteQuoteController deleteController = new DeleteQuoteController(new ApexPages.StandardController(quote));
		deleteController.cancelQuote();
		//System.assertEquals(deleteController, deleteController);

		Test.stopTest();

	}
	
	@isTest
	static void testCancelQuoteFlow2() {

		Test.startTest();
		try{
			Account a = [SELECT Id FROM Account WHERE Name = 'Test Account'];
			Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Ufficio Gestione'];
			String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
			User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
							  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							  LocaleSidKey='en_US', ProfileId = p.Id,
							  TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName);
			System.runAs(u) {
				Test.setCurrentPage(Page.DeleteQuote);
				ApexPages.currentPage().getParameters().put('id', a.Id);
				zqu__Quote__c quote = [SELECT Id FROM zqu__Quote__c WHERE Name = 'Test Quote 1'];
				DeleteQuoteController deleteController = new DeleteQuoteController(new ApexPages.StandardController(quote));
				deleteController.cancelQuote();
				//System.assertEquals(deleteController, deleteController);
			}
		}catch(Exception ex){

		}

		Test.stopTest();

	}

	@isTest
	static void testCancelQuoteError1() {

		Test.startTest();
		try{
			Account a = [SELECT Id FROM Account WHERE Name = 'Test Account'];
			Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Ufficio Gestione'];
			String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
			User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
							  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							  LocaleSidKey='en_US', ProfileId = p.Id,
							  TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName);
			System.runAs(u) {
				Test.setCurrentPage(Page.DeleteQuote);
				ApexPages.currentPage().getParameters().put('id', a.Id);
				zqu__Quote__c quote = [SELECT Id FROM zqu__Quote__c WHERE Name = 'Test Quote 2'];
				DeleteQuoteController deleteController = new DeleteQuoteController(new ApexPages.StandardController(quote));
				deleteController.cancelQuote();
				//System.assertEquals(deleteController, deleteController);
			}
		}catch(Exception ex){

		}

		Test.stopTest();

	}

	@isTest
	static void testCancelQuoteError2() {

		Test.startTest();

		try{
			Account a = [SELECT Id FROM Account WHERE Name = 'Test Account'];
			Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Ufficio Marketing'];
			String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
			User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
							  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							  LocaleSidKey='en_US', ProfileId = p.Id,
							  TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName);
			System.runAs(u) {
				Test.setCurrentPage(Page.DeleteQuote);
				ApexPages.currentPage().getParameters().put('id', a.Id);
				zqu__Quote__c quote = [SELECT Id FROM zqu__Quote__c WHERE Name = 'Test Quote 1'];
				DeleteQuoteController deleteController = new DeleteQuoteController(new ApexPages.StandardController(quote));
				deleteController.cancelQuote();
				//System.assertEquals(deleteController, deleteController);
			}
		}catch(Exception ex){

		}

		Test.stopTest();

	}

	@testSetup
	public static void testSetup() {
		
		Account a = new Account(Name = 'Test Account');
		insert a;
		
		zqu__Quote__c q1 = new zqu__Quote__c(Name = 'Test Quote 1', zqu__Status__c = 'New');
		zqu__Quote__c q2 = new zqu__Quote__c(Name = 'Test Quote 2', zqu__Status__c = 'Confirmed');
		List<zqu__Quote__c> listQuote = new List<zqu__Quote__c>();
		listQuote.add(q1);
		listQuote.add(q2);
		insert listQuote;

	}

}