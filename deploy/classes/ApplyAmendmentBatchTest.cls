@isTest (SeeAllData = true)
private class ApplyAmendmentBatchTest{

	@isTest
	static void testM1(){
		Test.startTest();
		Database.executeBatch(new ApplyAmendmentBatch());
		Test.stopTest();	
	}

	@isTest
	static void testM2(){
		Test.startTest();
		Database.executeBatch(new ApplyAmendmentBatch(Date.today()));
		Test.stopTest();	
	}

	@isTest
	static void testM3(){
		Test.startTest();
		Set<Id> subs = new Set<Id>();
		List<Amendment__c> amendments = [SELECT Id,LabelRunEntitlementApplied__c,SubscriptionApplied__c,Subscription__c FROM Amendment__c LIMIT 5000];
		for(Amendment__c amendment : amendments){
			amendment.LabelRunEntitlementApplied__c = false;
			amendment.SubscriptionApplied__c = false;
			subs.add(amendment.Subscription__c);
		}
		Database.update(amendments,false);
		Database.executeBatch(new ApplyAmendmentBatch(subs));
		Test.stopTest();
	}

	@isTest
	static void testM4(){
		Test.startTest();
		ApplyAmendmentSchedule sched = new ApplyAmendmentSchedule();
		sched.execute(null);
		Test.stopTest();
	}
}