public class CheckIBAN {
	
	@future(callout = true)
	public static void validateIban(Id idContact, String iban){

		String codiceIban = iban.deleteWhiteSpace().replaceAll('[^a-zA-Z0-9]', '');

		Http h = new Http();
		HttpRequest req = new HttpRequest();
		String endpoint = Setting__c.getInstance('ValidateIban').URL__c;
		endpoint += codiceIban;
		req.setEndpoint(endpoint);
		req.setMethod('GET');
		req.setHeader('Content-Type', 'application/json');
		HttpResponse res = h.send(req);
		System.debug(res.getBody());
		WrapperValidationIBAN wrap = null;
		WrapperErrorIBAN wraperror = null;
		try{
			wrap = (WrapperValidationIBAN) JSON.deserialize(res.getBody(),WrapperValidationIBAN.class);
		    System.debug('*** Response Valid: ' + wrap);
		}catch(Exception ex){
		    wraperror = (WrapperErrorIBAN) JSON.deserialize(res.getBody(),WrapperErrorIBAN.class);
		    System.debug('*** Response Error: ' + wraperror);
		}

		Contact tempCon = [SELECT Id, IBAN_Valido__c, Descrizione_Errore_IBAN__c, BIC__c, Istituto_di_Credito__c, Filiale__c, Indirizzo_Filiale__c, Nazione_Banca__c, Citta_Banca__c, Provincia_Banca__c, CAP_Banca__c, Paese_Banca__c, Numero_Conto_Corrente__c, Chiave_Banca__c, Chiave_Controllo__c 
						   FROM Contact WHERE Id = :idContact LIMIT 1];

		tempCon.IBAN_Valido__c = true;
		tempCon.Descrizione_Errore_IBAN__c = '';

		if(wraperror != null){

			tempCon.IBAN_Valido__c = false;
			for(CommonWrapper.Errors err : wraperror.errors){
				tempCon.Descrizione_Errore_IBAN__c += err.code + ' - ' + err.message + '\n';
			}

		}else if(wrap != null){

			Set<String> errorCodes = new Set<String>{'201','202','203','205'};
			if(wrap.validations != null && !wrap.validations.isEmpty()){
				for(CommonWrapper.Validation val : wrap.validations){
					if(errorCodes.contains(val.code)){
						tempCon.IBAN_Valido__c = false;
						tempCon.Descrizione_Errore_IBAN__c += val.code + ' - ' + val.message + '\n';
					}
				}
			}

			if(tempCon.IBAN_Valido__c) {

				tempCon.Codice_IBAN__c = codiceIban;
				tempCon.BIC__c = wrap.bank_data.bic;
				tempCon.Filiale__c = wrap.bank_data.branch;
				tempCon.Nazione_Banca__c = wrap.bank_data.country;
				tempCon.Citta_Banca__c = wrap.bank_data.city;
				tempCon.CAP_Banca__c = wrap.bank_data.zip;

				String bank = wrap.bank_data.bank;
				if(bank != null && String.isNotBlank(bank)) {
					if(bank.length() > 60) {
                        tempCon.Istituto_di_Credito__c = bank.substring(0, 60);
                    } else {
                        tempCon.Istituto_di_Credito__c = bank;
                    }
				}

                String address = wrap.bank_data.address;
                if(address != null && String.isNotBlank(address)) {
                    if(address.length() > 35) {
                        tempCon.Indirizzo_Filiale__c = address.substring(0, 35);
                    } else {
                        tempCon.Indirizzo_Filiale__c = address;
                    }
                }

                String state = wrap.bank_data.state;
                if(state != null && String.isNotBlank(state)) {
                    if(state.length() <= 3) {
                        tempCon.Provincia_Banca__c = state;
                    }
                }

                String countryISO = wrap.bank_data.country_iso;
                if(countryISO != null && String.isNotBlank(countryISO)){
                    if(countryISO.equalsIgnoreCase('IT')){
                        tempCon.Paese_Banca__c = wrap.bank_data.country_iso;
                        tempCon.Numero_Conto_Corrente__c = wrap.bank_data.account;
                        tempCon.Chiave_Banca__c = codiceIban.substring(5, 15);
                        tempCon.Chiave_Controllo__c = codiceIban.substring(4, 5);
                    }
                }
			}	
		}

		if(!tempCon.IBAN_Valido__c){
			tempCon.BIC__c = null;
			tempCon.Istituto_di_Credito__c = null;
			tempCon.Filiale__c = null;
			tempCon.Indirizzo_Filiale__c = null;
			tempCon.Nazione_Banca__c = null;
			tempCon.Citta_Banca__c = null;
			tempCon.Provincia_Banca__c = null;
			tempCon.CAP_Banca__c = null;
		}

		ContactTriggerHandler.skip = true;
		update tempCon;
		ContactTriggerHandler.skip = false;
	}
}