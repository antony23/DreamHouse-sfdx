@isTest (SeeAllData=true)
public class TestRegolaAvvisoRinnovoTrigger {
	
	@isTest static void testM(){
		List<string> fields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Regola_Avviso_Rinnovo__c').getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        List<Regola_Avviso_Rinnovo__c> regole = (List<Regola_Avviso_Rinnovo__c>) Database.query('SELECT ' + String.join(fields, ',') + ' FROM Regola_Avviso_Rinnovo__c LIMIT 100');

        List<Regola_Avviso_Rinnovo__c> nuoveRegole = new List<Regola_Avviso_Rinnovo__c>();
        for(Regola_Avviso_Rinnovo__c regola : regole){
        	nuoveRegole.add(regola.clone(false,true,false,false));
        }

        insert nuoveRegole;
        update nuoveRegole;
        Database.delete(regole,false);
	}
}