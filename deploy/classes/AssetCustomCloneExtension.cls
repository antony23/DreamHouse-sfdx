public class AssetCustomCloneExtension {

	public Asset copia {get;set;}
    public Movimentazione_stock__c movStock {get;set;}
    public Integer quantitaCopia {get;set;}
    public String tipoConsegnaOriginale {get;set;} //DH-633

    public Boolean isTipoSpeseDiSpedizioneAbbonamentoVisibile{
        get{
            return copia.SubscriptionAllCopy__c != null;
        }
    }

    public Boolean isTipoSpeseDiSpedizioneVenditaDirettaVisibile{
        get{
            return copia.Subscription_Vendite_Dirette__c != null;
        }
    }

    public Map<String,List<SelectOption>> tipiSpeseSpedizione {get;set;}
    public ServiceUtil.TipiSpeseDiSpedizioneResponse tipiSpeseConsegne {get;set;}
    public List<SelectOption> tipiConsegnaAbbonamenti {
        get{
            List<SelectOption> res = new List<SelectOption>();
            if(copia.Codice_Nazione__c == 'IT'){
                res.add(new SelectOption('ZO','Arretrato'));
            }else{
                res.add(new SelectOption('ZQ','Arretrato'));
            }
            return res;
        }
    }
    public List<SelectOption> tipiConsegnaVenditeDirette {
        get{
            List<SelectOption> res = new List<SelectOption>();
            if(tipiSpeseConsegne.tipiSpeseDiSpedizioneVenditeDirette != null){
                List<ServiceUtil.TipoConsegna> tipiConsegna = tipiSpeseConsegne.tipiSpeseDiSpedizioneVenditeDirette.get(copia.Tipospedizione__c);
                if(tipiConsegna != null){
                    for(ServiceUtil.TipoConsegna tc : tipiConsegna){
                        res.add(new SelectOption(tc.codicesap,tc.tipoconsegna));
                    }
                }
            }
            return res;
        }
    }

    public List<SelectOption> motiviRispedizione{
        get{
            List<SelectOption> motivi = new List<SelectOption>();
            if(copia != null){
                Boolean prodottoVenditaDiretta = copia.Movimentazione_Stock__c != null;
                Boolean abbonamento = !prodottoVenditaDiretta;
                if(abbonamento){
                    motivi.add(new SelectOption('RISPEDIZIONE ABBO - MANCATA CONSEGNA','Mancata consegna'));
                    motivi.add(new SelectOption('RISPEDIZIONE ABBO - COPIA DANNEGGIATO','Copia danneggiata'));
                    motivi.add(new SelectOption('RISPEDIZIONE ABBO - CARING','Caring'));
                }
                if(prodottoVenditaDiretta){
                    motivi.add(new SelectOption('RISPEDIZIONE VD - MANCATA CONSEGNA','Mancata consegna'));
                    motivi.add(new SelectOption('RISPEDIZIONE VD - PRODOTTO DANNEGGIATO','Prodotto danneggiato'));
                    motivi.add(new SelectOption('RISPEDIZIONE VD - PRODOTTO ERRATO','Prodotto errato'));
                    motivi.add(new SelectOption('RISPEDIZIONE VD - CONSEGNA PARZIALE','Consegna parziale'));
                    motivi.add(new SelectOption('RISPEDIZIONE VD - CARING','Caring'));
                }
            }
            return motivi;
        }
    }

    public AssetCustomCloneExtension(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) stdController.addFields(new List<String>{'Motivo_Rispedizione__c','Name','Subscription_Charge__r.Zuora__Subscription__r.Metodo_di_pagamento__c','Tipoconsegna__c','Tipospedizione__c','Codice_Nazione__c','SubscriptionAllCopy__c','Subscription_Vendite_Dirette__c','Movimentazione_Stock__c','Quantity'});
        this.copia = (Asset)stdController.getRecord();
        if(Test.isRunningTest()){
            List<string> assetFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Asset').getDescribe().Fields.getMap().values()){
                assetFields.add(ft.getDescribe().getName().toLowerCase());
            }
            this.copia = (Asset) Database.query('SELECT ' + String.join(assetFields, ',') + ' FROM Asset WHERE Id = \'' + copia.Id + '\'');
        }
        this.copia.Motivo_Rispedizione__c = null;
        quantitaCopia = Integer.valueOf(this.copia.Quantity);
        tipoConsegnaOriginale = this.copia.Tipoconsegna__c; //DH-633
        if(copia.Movimentazione_Stock__c != null){
            movStock = (Movimentazione_Stock__c) Database.query(Utils.getSelectAllQuery('Movimentazione_Stock__c','Id = \'' + copia.Movimentazione_Stock__c + '\'','','Prodotto_Vendita_Diretta__r.Giacenza__c',false,false));
        }
    }

    public void init(){
        ServiceUtil.TipiSpeseDiSpedizioneResponse response = ServiceUtil.invokeTipiSpeseDiSpedizione(this.copia.Codice_Nazione__c);
        tipiSpeseConsegne = response;
        if(String.isNotBlank(response.error)) throw new DomusException('Errore durante la ricerca dei tipi di spedizione applicabili. '+response.error);

        tipiSpeseSpedizione = new Map<String,List<SelectOption>>();
    
        tipiSpeseSpedizione.put('VenditeDirette', new List<SelectOption>());
        for(String vd : response.tipiSpeseDiSpedizioneVenditeDirette.keySet()){
           // DH-469
           /*if(vd != 'Contrassegno'){*/
                tipiSpeseSpedizione.get('VenditeDirette').add(new SelectOption(vd,vd));
           /*}*/
        }

        tipiSpeseSpedizione.put('Abbonamenti', new List<SelectOption>());
        for(String vd : response.tipiSpeseDiSpedizioneAbbonamenti.keySet()){
            tipiSpeseSpedizione.get('Abbonamenti').add(new SelectOption(vd,vd));
        }

    }

    public PageReference customClone() {
        PageReference res = null;
        try {
            if(movStock != null){
                if(movStock.Stato__c == 'Nuovo' || movStock.Stato__c == 'Da Trasmettere'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Impossibile rispedire una movimentazione non ancora inviata'));
                    return null;
                }
                if(movStock.Quantita__c > movStock.Prodotto_Vendita_Diretta__r.Giacenza__c){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'La quantità selezionata eccede la giacenza del magazzino.'));
                    return null;
                }
            }
            if(copia.Motivo_Rispedizione__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Specificare il motivo della rispedizione.'));
                return null;
            }

            update copia;

            Movimentazione_stock__c movStockCloned = null;
            if(movStock != null){
                movStockCloned = movStock.clone(false,true,false,false);
                movStockCloned.Quantita__c = movStock.Quantita__c;
                movStockCloned.Stato__c = 'Da Trasmettere';
                movStockCloned.Tipo_Spedizione__c = copia.Tipospedizione__c;
                movStockCloned.Tipo_consegna__c = copia.Tipoconsegna__c;
                movStockCloned.Rispedizione__c = true; // DH-145
                insert movStockCloned;
            }

            Asset copiaOriginale = (Asset) Database.query(Utils.getSelectAllQuery('Asset','Id = \'' + this.copia.Id + '\'','','',false,false,new Set<String>{'parentid','rootassetid'}));
            Asset clonedCopia = copiaOriginale.clone(false,true,false,false);
            clonedCopia.Inviata__c = false;
            clonedCopia.Rispedizione__c = true;
            clonedCopia.Rispedizione_di__c = copiaOriginale.Id;
            clonedCopia.Quantity = movStock != null ? movStock.Quantita__c : quantitaCopia;
            if(movStockCloned != null){
                clonedCopia.Movimentazione_Stock__c = movStockCloned.Id;
            }
            clonedCopia.Arretrata__c = copiaOriginale.Data_uscita__c <= Date.today();
            insert clonedCopia;

            /** Inizio: DH-972 **/
            /*Case c = new Case(
                OwnerId = UserInfo.getUserId(),
                Origin = 'Creazione automatica',
                Subject = 'Richiesta rispedizione',
                Description = 'Il cliente richiede la rispedizione di ' + copiaOriginale.Name + '. Motivo: ' + copia.Motivo_Rispedizione__c +'.',
                Status = 'Chiuso',
                AccountId = copiaOriginale.accountid,
                ContactId = copiaOriginale.contactid,
                Macro_Categoria__c = 'Diffusione',
                Categoria__c = 'RISPEDIZIONI/PROROGHE',
                Sottocategoria__c = copia.Motivo_Rispedizione__c,
                Prodotto_1__c = copiaOriginale.Product2Id__c,
                Caseautomatico__c = true
            );
            c.Subscription__c = copiaOriginale.SubscriptionAllCopy__c != null ? copiaOriginale.SubscriptionAllCopy__c : copiaOriginale.Subscription_Vendite_Dirette__c;
            insert c;*/
            /** Fine: DH-972 **/

            //DH-633
            copia.Tipoconsegna__c = tipoConsegnaOriginale;
            update copia;

            res = new ApexPages.StandardController(clonedCopia).view();
        } catch(Exception e) {
            res = null;
            appendErrorMessage(e);
        }
        return res;
    }

    public void nofunction(){

    }

    public PageReference goBack(){
        return new ApexPages.StandardController(this.copia).view();
    }

    public void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }

}