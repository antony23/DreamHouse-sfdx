public class SubscriptionProductChargeTriggerHandler{

    public static Boolean skipTrigger = false;
    
    public static void onBeforeInsert(List<Zuora__SubscriptionProductCharge__c  > triggerNew, Map<Id, Zuora__SubscriptionProductCharge__c> triggerNewMap) { 
        setValuesOnCharge(triggerNew);
    }
    
    public static void onAfterInsert(List<Zuora__SubscriptionProductCharge__c  > triggerNew, Map<Id, Zuora__SubscriptionProductCharge__c> triggerNewMap) {
        System.debug('************ Zuora__SubscriptionProductCharge__c AFTER INSERT ***************');
        aggiornaTipoCliente(triggerNew);
        try {
            Set<Id> subscriptionIds = new Set<Id>();
            Map<Id,Map<String,Integer>> tipoSubscription = new Map<Id,Map<String,Integer>>();
            Map<Id, List<Zuora__SubscriptionProductCharge__c>> chargeSubscriptionMap = new Map<Id, List<Zuora__SubscriptionProductCharge__c>>();
            Map<Id, Map<Id, Zuora__SubscriptionProductCharge__c>> subProdottoChargeMap = new Map<Id, Map<Id, Zuora__SubscriptionProductCharge__c>>();

            for(Zuora__SubscriptionProductCharge__c charge : triggerNew){
                if(!tipoSubscription.containsKey(charge.Zuora__Subscription__c)){
                    tipoSubscription.put(charge.Zuora__Subscription__c,new Map<String,Integer>());
                }
                if(!tipoSubscription.get(charge.Zuora__Subscription__c).containsKey(charge.Tipo_Prodotto__c)){
                    tipoSubscription.get(charge.Zuora__Subscription__c).put(charge.Tipo_Prodotto__c,0);
                }
                tipoSubscription.get(charge.Zuora__Subscription__c).put(
                    charge.Tipo_Prodotto__c,
                    tipoSubscription.get(charge.Zuora__Subscription__c).get(charge.Tipo_Prodotto__c) +1
                );

                subscriptionIds.add(charge.Zuora__Subscription__c);

                if(chargeSubscriptionMap.get(charge.Zuora__Subscription__c) == null){
                    chargeSubscriptionMap.put(charge.Zuora__Subscription__c, new List<Zuora__SubscriptionProductCharge__c>());
                }
                chargeSubscriptionMap.get(charge.Zuora__Subscription__c).add(charge);

                if(charge.Prodotto_Vendita_Diretta__c != null){
                    if(subProdottoChargeMap.get(charge.Zuora__Subscription__c) == null){
                        subProdottoChargeMap.put(charge.Zuora__Subscription__c, new Map<Id, Zuora__SubscriptionProductCharge__c>());
                    }
                    Map<Id, Zuora__SubscriptionProductCharge__c> tmpMap = subProdottoChargeMap.get(charge.Zuora__Subscription__c);
                    tmpMap.put(charge.Prodotto_Vendita_Diretta__c, charge);
                    subProdottoChargeMap.put(charge.Zuora__Subscription__c, tmpMap);
                }
                
            }

            // Riconciliazione gracing post su rinnovo
            List<Zuora__Subscription__c> currentSubscriptions = [SELECT Id,Zuora__Zuora_Id__c,Subscription_Precedente__c,Zuora__CustomerAccount__r.Sold_To_Salesforce__c,Zuora__Status__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds];
            Set<Id> subscriptionsPrecedenti = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            for(Zuora__Subscription__c currentSubscription : currentSubscriptions){
                if(currentSubscription.Subscription_Precedente__c != null){
                    subscriptionsPrecedenti.add(currentSubscription.Subscription_Precedente__c);
                    contactIds.add(currentSubscription.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);
                }
            }

            Map<Id,Contact> contactsMap = new Map<Id,Contact>(
                [SELECT Id,Name,MailingCountry,MailingStreet,MailingPostalCode,MailingCity,MailingState,
                        Dug__c,DugAbbreviataStd__c,Civico__c,Presso__c,Id_Area_Nazione__c,Area_Nazione__c,Nazione__c,Codiceclienteselected__c,
                        Codice_Nazione__c,Frazione__c,Supplemento_CAP__c, Via__c, Supplemento_Civico__c, FirstName, LastName
                        FROM Contact WHERE Id IN :contactIds]
            );

            if(subscriptionsPrecedenti.size() > 0){
                Map<Id,Zuora__Subscription__c> subscriptionPrecedentiWithCopie = new Map<Id,Zuora__Subscription__c>(
                    [SELECT Id,Zuora__SubscriptionEndDate__c,Zuora__Zuora_Id__c,
                        // Prima del DH-904 (SELECT Id,Tipo_gracing__c,Subscription_Zuora_Id_All_Copy__c,SubscriptionAllCopy__c,Testata__c,Supporto__c,GracingAllowed__c,Copia_cartacea__c, Inviata__c FROM CopieDigitaliCarta__r WHERE GracingAllowed__c = true AND Tipo_gracing__c = 'Rinnovo'),
                        (SELECT Id,Tipo_gracing__c,Subscription_Zuora_Id_All_Copy__c,SubscriptionAllCopy__c,Testata__c,Supporto__c,GracingAllowed__c,Copia_cartacea__c, Inviata__c, Annullata__c FROM CopieDigitaliCarta__r WHERE GracingAllowed__c = true AND (Tipo_gracing__c = 'Rinnovo' AND Annullata__c = false)),
                        (SELECT Id,Confirmed__c,expire_date__c,Subscription_Zuora_Id__c,RecordTypeId FROM Entitlements__r)
                    FROM Zuora__Subscription__c
                    WHERE Id IN :subscriptionsPrecedenti]
                );

                System.debug(loggingLevel.Error, '*** subscriptionPrecedentiWithCopie: ' + subscriptionPrecedentiWithCopie);

                List<Asset> copieDaRiconciliare = new List<Asset>();
                Set<Id> copieDaRiconciliareId = new Set<Id>();
                List<Entitlement__c> entitlementsDaDisattivare = new List<Entitlement__c>();
                Set<Id> entitlementsDaDisattivareId = new Set<Id>();
                Map<String,CapDetails__c> capDetails = DomusUtil.getCapDetails();

                for(Zuora__Subscription__c subscription : currentSubscriptions){
                    System.debug(loggingLevel.Error, '*** subscription.Id: ' + subscription.Id);

                    if(subscription.Subscription_Precedente__c != null){
                        Zuora__Subscription__c subscriptionPrecedente = subscriptionPrecedentiWithCopie.get(subscription.Subscription_Precedente__c);
                        List<Zuora__SubscriptionProductCharge__c> chargesCurrentSubscription = chargeSubscriptionMap.get(subscription.Id);
                        Set<String> testateSubscription = new Set<String>();
                        String supportoAbbonamento = null;

                        for(Zuora__SubscriptionProductCharge__c chargeCurrentSubscription : chargesCurrentSubscription){
                            if(chargeCurrentSubscription.Testata__c != null){
                                testateSubscription.add(chargeCurrentSubscription.Testata__c.toUpperCase());
                            }

                            if(chargeCurrentSubscription.Tipo_Prodotto__c == 'Abbonamento' && chargeCurrentSubscription.Supporto__c != null){
                                supportoAbbonamento = chargeCurrentSubscription.Supporto__c;
                            }
                        }

                        // ricalcolo nuovo indirizzo
                        Contact cont = contactsMap.get(subscription.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);
                        String nazione = cont.MailingCountry;
                        String indirizzo = '';
                        if(cont.DugAbbreviataStd__c != null || cont.Dug__c != null) indirizzo += (cont.DugAbbreviataStd__c != null ? cont.DugAbbreviataStd__c : cont.Dug__c) + ' ';
                        indirizzo += cont.Via__c + ' ' + 
                            (cont.Civico__c != null ? cont.Civico__c : '') + 
                            (cont.Supplemento_Civico__c != null ? cont.Supplemento_Civico__c : '');

                        String citta = cont.MailingCity;
                        String informazioniIndirizzo = cont.MailingPostalCode + ' ' +  cont.MailingCity;
                        String codicePostale = cont.MailingPostalCode;
                        String provincia = cont.MailingState;
                        Integer dettaglioZona = String.isNotBlank(cont.Id_Area_Nazione__c) ? Integer.valueOf(cont.Id_Area_Nazione__c) : null;
                        String zona = cont.Area_Nazione__c;
                        String frazione = cont.Frazione__c;
                        String areaGeografica = cont.Area_Nazione__c;
                        String supplementoCAP = cont.Supplemento_CAP__c;

                        String codiceProvincia = null;
                        String codiceRegione = null;
                        String siglaRegione = null;
                        String zonaDItalia = null;
                        String siglaRegioneAbbr = null;
                        String suddGeo2 = null;

                        if(cont.MailingPostalCode != null && cont.Codice_Nazione__c == 'IT'){
                            for(CapDetails__c capDetail : capDetails.values()){
                                Boolean found = false;
                                if(capDetail.Name == cont.MailingPostalCode) found = true;
                                if(!found && capDetail.Name.contains('x')){
                                    String capPrefix = capDetail.Name.replaceAll('x','');
                                    if(cont.MailingPostalCode.startsWith(capPrefix)) found = true;
                                }
                                if(!found && capDetail.Name.contains('-')){
                                    String[] range = capDetail.Name.split('-');
                                    if(Integer.valueOf(cont.MailingPostalCode) >= Integer.valueOf(range[0]) && Integer.valueOf(cont.MailingPostalCode) <= Integer.valueOf(range[1])) found = true;
                                }
                                if(found){
                                    codiceRegione = capDetail.CodiceRegione__c;
                                    siglaRegione = capDetail.SiglaRegione__c;
                                    siglaRegioneAbbr = capDetail.SiglaRegioneAbbr__c;
                                    zonaDItalia = capDetail.ZonadItalia__c;
                                    suddGeo2 = capDetail.Suddivisione_geografica_2__c;
                                    if(codiceRegione == 'SAR'){ // la sardergna ha per un cap più province
                                        codiceProvincia = provincia;
                                    }else{
                                        codiceProvincia = capDetail.CodiceProvincia__c;
                                    }
                                    break;
                                }
                            }
                        }

                        System.debug(loggingLevel.Error, '*** subscriptionPrecedente: ' + subscriptionPrecedente);
                        for(Asset copia : subscriptionPrecedente.CopieDigitaliCarta__r){
                            String testataCopia = copia.Testata__c;
                            if(testataCopia != null){
                                testataCopia = testataCopia.toUpperCase();
                                if(testateSubscription.contains(testataCopia) && (copia.Supporto__c.contains(supportoAbbonamento) || supportoAbbonamento.contains(copia.Supporto__c))){

                                    //*** DH-851 ***//
                                    if(!copia.Inviata__c){
                                        copia.Copia_cartacea__c = supportoAbbonamento.containsIgnoreCase('Carta');
                                        copia.Copia_digitale__c = supportoAbbonamento.containsIgnoreCase('Digitale');
                                    }
                                    //*** DH-851 - Fine ***//

                                    Boolean isGracing = subscription.Zuora__Status__c == 'Pending Activation' &&
                                        copia.Copia_cartacea__c;

                                    copia.ContactId = cont.Id;
                                    copia.SubscriptionAllCopy__c = subscription.Id;
                                    copia.Subscription_Zuora_Id_All_Copy__c = subscription.Zuora__Zuora_Id__c;
                                    //copia.GracingAllowed__c = isGracing;
                                    //copia.Tipo_gracing__c = isGracing ? 'Nuovo' : null;
                                    copia.Nazione__c = nazione;
                                    copia.Indirizzo__c = indirizzo;
                                    copia.Citt__c = citta;
                                    copia.Informazioni_Indirizzo__c = informazioniIndirizzo;
                                    copia.Codice_Postale__c = codicePostale;
                                    copia.Provincia__c = provincia;
                                    copia.Dettaglio_Zona__c = dettaglioZona;
                                    copia.Zona__c = zona;
                                    copia.Frazione__c = frazione;
                                    copia.AreaGeografica__c = areaGeografica;
                                    copia.SupplementoCAP__c = supplementoCAP;
                                    copia.CodiceProvincia__c = codiceProvincia;
                                    copia.CodiceRegione__c = codiceRegione;
                                    copia.SiglaRegione__c = siglaRegione;
                                    copia.SiglaRegioneAbbr__c = siglaRegioneAbbr;
                                    copia.Suddivisione_geografica_2__c = suddGeo2;
                                    copia.ZonadItalia__c = zonaDItalia;

                                    //*** DH-718 ***//
                                    if(!copia.Inviata__c){
                                        copia.GracingAllowed__c = isGracing;
                                        copia.Tipo_gracing__c = isGracing ? 'Nuovo' : null;
                                    }
                                    //*** DH-718 - Fine ***//                            

                                    if(!copieDaRiconciliareId.contains(copia.Id)){
                                        copieDaRiconciliare.add(copia);
                                        copieDaRiconciliareId.add(copia.Id);
                                    }
                                }
                            }
                        }

                        System.debug(loggingLevel.Error, '*** subscriptionPrecedente.Entitlements__r: ' + subscriptionPrecedente.Entitlements__r);

                        for(Entitlement__c entitlement : subscriptionPrecedente.Entitlements__r){
                            if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('Virtualcomm').getRecordTypeId()){
                                entitlement.confirmed__c = 0;
                            }else if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('San Pietro').getRecordTypeId()){
                                entitlement.Data_disattivazione_ordine_professional__c = Date.today();
                                entitlement.SP_Data_scadenza_ordine__c = Date.today().year() + String.valueOf(Date.today().month()).leftPad(2).replace(' ','0') + String.valueOf(Date.today().day()).leftPad(2).replace(' ','0');
                            }
                            entitlement.Marker__c = true;
                            entitlement.Subscription_Zuora_Id__c = subscription.Zuora__Zuora_Id__c;
                            if(!entitlementsDaDisattivareId.contains(entitlement.Id)){
                                entitlementsDaDisattivareId.add(entitlement.Id);
                                entitlementsDaDisattivare.add(entitlement);
                            }
                        }
                        System.debug(loggingLevel.Error, '*** subscriptionPrecedente.Entitlements__r: ' + subscriptionPrecedente.Entitlements__r);
                    }
                }
                update copieDaRiconciliare;
                update entitlementsDaDisattivare;
            }

            if(!Test.isRunningTest()){
                CustomPermissions__c custPerm = CustomPermissions__c.getInstance(UserInfo.getProfileId());
                Database.executeBatch(new SubscriptionRenewalBatch(subscriptionIds));
                if(custPerm.GrantProcessSubscription__c){
                    Database.executeBatch(new ProcessSubscription(subscriptionIds),100);
                }
            }

            List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();

            for(Id subId : chargeSubscriptionMap.keySet()){
                Set<String> businesUnits = new Set<String>();
                if(chargeSubscriptionMap.get(subId) != null){
                    Decimal total = 0;
                    Decimal totaleRecurring = 0;
                    Decimal totaleOneTime = 0;
                    Decimal speseDiSpedizione = 0;

                    for(Zuora__SubscriptionProductCharge__c c : chargeSubscriptionMap.get(subId)){

                        //*** DH-860 ***//
                        if(c.Tipo_Prodotto__c != 'Sconto') {
                            businesUnits.add(c.Business_Unit__c);
                        }
                        //*** DH-860 - Fine ***//

                        if(c.Zuora__ExtendedAmount__c != null){

                            //*** DH-81 ***// 
                            if(c.Zuora__RatePlanDescription__c == 'Spese di Spedizione Vendite Dirette'){
                                speseDiSpedizione += c.Zuora__ExtendedAmount__c;
                            }else if(c.Zuora__Type__c.contains('One-Time') && c.Tipo_Prodotto__c != 'Sconto' && c.Zuora__RatePlanDescription__c != 'Spese di Spedizione Vendite Dirette'){
                                totaleOneTime += c.Zuora__ExtendedAmount__c;
                            }else if(c.Zuora__Type__c.contains('Recurring') && c.Tipo_Prodotto__c != 'Sconto' && c.Zuora__RatePlanDescription__c != 'Spese di Spedizione Vendite Dirette'){
                                totaleRecurring += c.Zuora__ExtendedAmount__c;
                            }
                            /*
                            if(c.Zuora__Type__c.contains('One-Time') && c.Tipo_Prodotto__c != 'Sconto'){
                                totaleOneTime += c.Zuora__ExtendedAmount__c;
                            } else if(c.Zuora__Type__c.contains('Recurring') && c.Tipo_Prodotto__c != 'Sconto'){
                                totaleRecurring += c.Zuora__ExtendedAmount__c;
                            }
                            */
                            //*** DH-81 - Fine ***//
                        }
                    }

                    for(Zuora__SubscriptionProductCharge__c c : chargeSubscriptionMap.get(subId)){
                        if(c.Zuora__Model__c == 'Discount-Fixed Amount'){
                            total -= c.Zuora__ExtendedAmount__c;
                        } else if(c.Zuora__Model__c == 'Discount-Percentage' && c.Zuora__DiscountPercentage__c != null){
                            if(c.Zuora__ApplyDiscountTo__c.contains('One Time')){
                                total -= totaleOneTime * c.Zuora__DiscountPercentage__c / 100;
                            }
                            if(c.Zuora__ApplyDiscountTo__c.contains('Recurring')){
                                total -= totaleRecurring * c.Zuora__DiscountPercentage__c / 100;
                            }
                            
                        }

                    }
                    // DH-81
                    total = total + totaleRecurring + totaleOneTime + speseDiSpedizione;
                    //total = total + totaleRecurring + totaleOneTime;
                    if(total < 0){
                        total = 0;
                    }
                    //Zuora__ApplyDiscountTo__c One Time Recurring Usage
                    //zqu__Model__c 'Discount-Percentage' 'Discount-Fixed Amount'
                    //Zuora__DiscountPercentage__c

                    String tipoSub = null;
                    if(tipoSubscription.containsKey(subId)){
                        if(tipoSubscription.get(subId).containsKey('Abbonamento')
                            && tipoSubscription.get(subId).get('Abbonamento') > 0){
                            tipoSub = tipoSubscription.get(subId).get('Abbonamento') == 1 ? 'Abbonamento' : 'Bundle Abbonamenti';
                        }
                        if(tipoSubscription.get(subId).containsKey('Vendita Diretta') 
                            && tipoSubscription.get(subId).get('Vendita Diretta') > 0){
                            if(tipoSub == 'Abbonamento'){
                                tipoSub =  'Bundle Misti';
                            }
                            if(tipoSub == null){
                                tipoSub = tipoSubscription.get(subId).get('Vendita Diretta') == 1 ? 'Vendita Diretta' : 'Bundle Vendite Dirette';
                            }
                        }
                    }
                    List<String> bUnits = new List<String>(businesUnits);
                    bUnits.sort();
                    String bUnitMulti = null;
                    if(!bUnits.isEmpty()){
                        bUnitMulti = String.join(bUnits, ';');
                    }
                    subscriptionList.add( 
                        new Zuora__Subscription__c(
                            Id = subId, 
                            Tipo_subscription__c = tipoSub, 
                            Totale__c = total, 
                            Invio_Conferma_Ordine__c = true,
                            Business_Unit__c = bUnitMulti
                        )
                    );
                }
            }

            update subscriptionList;

            MovimentazioneStockTriggerHandler.skip = true;
            List<Movimentazione_Stock__c> movStockList = [SELECT Id,Subscription__c, Prodotto_Vendita_Diretta__c, Subscription_Charge__c 
                                                            FROM Movimentazione_Stock__c
                                                            WHERE Subscription__c = :subProdottoChargeMap.keySet()];
            for(Movimentazione_Stock__c ms : movStockList){
                if(subProdottoChargeMap.get(ms.Subscription__c) != null && subProdottoChargeMap.get(ms.Subscription__c).get(ms.Prodotto_Vendita_Diretta__c) != null){
                    ms.Subscription_Charge__c = subProdottoChargeMap.get(ms.Subscription__c).get(ms.Prodotto_Vendita_Diretta__c).Id;
                }
            }
            update movStockList;
            MovimentazioneStockTriggerHandler.skip = false;

            //creaCaseAutomatici(chargeSubscriptionMap.keySet());   //DH-1164

            /*
            // KPI ID-17
            Map<Id,Zuora__Subscription__c> subscriptionsMapKPI = new Map<Id,Zuora__Subscription__c>(
                [SELECT Id, Zuora__SubscriptionEndDate__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]
            );
            Map<Id,Map<String,Date>> testateToActivateBySubscription = new Map<Id,Map<String,Date>>();
            List<Zuora__SubscriptionProductCharge__c> charges = [SELECT Id,Zuora__Subscription__r.Contact_Sold_To__r.Account.Id__c,Testata__c,Zuora__Subscription__c
                                                                FROM Zuora__SubscriptionProductCharge__c
                                                                WHERE Id IN :triggerNew];
            for(Zuora__SubscriptionProductCharge__c charge : charges){
                if(!testateToActivateBySubscription.containsKey(charge.Zuora__Subscription__r.Contact_Sold_To__r.Account.Id__c)){
                    testateToActivateBySubscription.put(charge.Zuora__Subscription__r.Contact_Sold_To__r.Account.Id__c, new Map<String,Date>());
                }
                if(charge.Testata__c != null && charge.Testata__c != 'Vendite Dirette'){
                    Date endDateSubscription = subscriptionsMapKPI.get(charge.Zuora__Subscription__c).Zuora__SubscriptionEndDate__c;
                    Date maxDateSubscription = testateToActivateBySubscription.get(charge.Zuora__Subscription__r.Contact_Sold_To__r.Account.Id__c).get(charge.Testata__c);
                    if(maxDateSubscription == null || maxDateSubscription < endDateSubscription){
                        maxDateSubscription = endDateSubscription;
                    }
                    // 
                    testateToActivateBySubscription.get(charge.Zuora__Subscription__r.Contact_Sold_To__r.Account.Id__c).put(charge.Testata__c.toUpperCase(), maxDateSubscription);
                }
            }

            List<Campo_KPI__mdt> kpiFieldList = [SELECT MasterLabel,Copie_gracing__c,Ageing_effettivo__c,Cicli__c,Cicli_Ponderati__c,Copie_Previste__c,Subscription_con_Gracing__c,Testata__c,Testata_Checkbox__c  FROM Campo_KPI__mdt];
            Map<String, Campo_KPI__mdt> kpiFieldMap = new Map<String, Campo_KPI__mdt>();
            for(Campo_KPI__mdt k : kpiFieldList){
                kpiFieldMap.put(k.MasterLabel.toUpperCase(), k);
            }
            Map<Id,Account> accountsKPI = new Map<Id,Account>();
            for(Id accountId : testateToActivateBySubscription.keySet()){
                if(!accountsKPI.containsKey(accountId)){
                    accountsKPI.put(accountId, new Account (Id = accountId));
                }
                Account accountKPI = accountsKPI.get(accountId);
                for(String testata : testateToActivateBySubscription.get(accountId).keySet()){
                    if(kpiFieldMap.containsKey(testata)){
                        System.debug(loggingLevel.Error, '*** kpiFieldMap.get(testata).Testata_Checkbox__c: ' + kpiFieldMap.get(testata).Testata_Checkbox__c);
                        System.debug(loggingLevel.Error, '*** testateToActivateBySubscription.get(accountId).get(testata): ' + testateToActivateBySubscription.get(accountId).get(testata));
                        if(kpiFieldMap.get(testata).Testata_Checkbox__c != null && testateToActivateBySubscription.get(accountId).get(testata) != null)
                        accountKPI.put(kpiFieldMap.get(testata).Testata_Checkbox__c, testateToActivateBySubscription.get(accountId).get(testata));
                    }
                }
            }
            
            AccountTriggerHandler.skip = true;
            Database.update(accountsKPI.values(),false);
            AccountTriggerHandler.skip = false;
            */

            ricalcolaSuscriptionTestata(triggerNew); 



        } catch(Exception e) {
            sendErrorEmail(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    public static void onBeforeUpdate(List<Zuora__SubscriptionProductCharge__c  > triggerOld, List<Zuora__SubscriptionProductCharge__c> triggerNew, Map<Id, Zuora__SubscriptionProductCharge__c> triggerOldMap, Map<Id, Zuora__SubscriptionProductCharge__c> triggerNewMap){   
    }

    public static void onAfterUpdate(List<Zuora__SubscriptionProductCharge__c  > triggerOld, List<Zuora__SubscriptionProductCharge__c> triggerNew, Map<Id, Zuora__SubscriptionProductCharge__c> triggerOldMap, Map<Id, Zuora__SubscriptionProductCharge__c> triggerNewMap){
        ricalcolaSuscriptionTestata(triggerNew);   
    }

    public static void onBeforeDelete(List<Zuora__SubscriptionProductCharge__c  > triggerOld, Map<Id, Zuora__SubscriptionProductCharge__c> triggerOldMap){  
    }
 
    public static void onAfterDelete(List<Zuora__SubscriptionProductCharge__c  > triggerOld, Map<Id, Zuora__SubscriptionProductCharge__c  > triggerOldMap){ 
        ricalcolaSuscriptionTestata(triggerOld);
    }

    public static void creaCaseAutomatici(Set<Id> subscriptionsId){
        try {
            List<Zuora__Subscription__c> subscriptions = [SELECT Id,Zuora__OriginalId__c,Zuora__Zuora_Id__c,Zuora__Account__c,Contatto_Principale_Committente__c,Testate__c,Numero_di_Cicli__c
                                                        FROM Zuora__Subscription__c
                                                        WHERE Id IN :subscriptionsId];

            Map<Id,List<Zuora__SubscriptionProductCharge__c>> chargesBySub = DomusUtil.getSubscriptionRatePlanCharges(subscriptionsId);

            List<Case> cases = new List<Case>();
            for(Zuora__Subscription__c subscription : subscriptions){
                if(subscription.Zuora__OriginalId__c == subscription.Zuora__Zuora_Id__c){
                    Case c = new Case(
                        OwnerId = UserInfo.getUserId(),
                        Subscription__c = subscription.Id,
                        Origin = 'Creazione automatica',
                        Status = 'Chiuso',
                        AccountId = subscription.Zuora__Account__c,
                        ContactId = subscription.Contatto_Principale_Committente__c,
                        Macro_Categoria__c = 'Diffusione',
                        Categoria__c = 'ORDINE',
                        Caseautomatico__c = true
                    );
                    Boolean abbonamento = false;
                    Boolean venditaDiretta = false;
                    for(Zuora__SubscriptionProductCharge__c charge : chargesBySub.get(subscription.Id)){
                        abbonamento = abbonamento || charge.Tipo_Prodotto__c == 'Abbonamento';
                        venditaDiretta = venditaDiretta || charge.Tipo_Prodotto__c == 'Vendita Diretta';
                        if(abbonamento || venditaDiretta){
                            if(c.Prodotto_1__c == null){
                                c.Prodotto_1__c = charge.Product_Rate_Plan_Charge__r.zqu__ProductRatePlan__r.zqu__Product__c;
                            }else if(c.Prodotto_2__c == null){
                                c.Prodotto_2__c = charge.Product_Rate_Plan_Charge__r.zqu__ProductRatePlan__r.zqu__Product__c;
                            }
                        }
                    }
                    if(venditaDiretta && !abbonamento){
                        c.Sottocategoria__c = 'PRODOTTO VD';
                        c.Subject = 'Vendita Diretta/Split';
                        c.Description = 'Vendita Diretta/Split';
                    }
                    if(abbonamento && !venditaDiretta){
                        if(subscription.Numero_di_Cicli__c == 1){
                            c.Sottocategoria__c = 'NUOVO ABBO';
                            c.Subject = 'Nuovo Abbonamento';
                            c.Description = 'Nuovo Abbonamento';
                        }else if(subscription.Numero_di_Cicli__c > 1){
                            c.Sottocategoria__c = 'RINNOVO ABBO';
                            c.Subject = 'Rinnovo Abbonamento';
                            c.Description = 'Rinnovo Abbonamento';
                        }
                    }
                    if(abbonamento && venditaDiretta){
                        c.Sottocategoria__c = 'ORDINE MISTO';
                        c.Subject = 'Ordine misto';
                        c.Description = 'Ordine misto';
                    }
                    cases.add(c);
                }
            }
            insert cases;
        } catch(Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }

    private static void setValuesOnCharge(List<Zuora__SubscriptionProductCharge__c> subscriptionChargeList){
        Set<String> chargeZuoraIds = new Set<String>();
        for(Zuora__SubscriptionProductCharge__c subscriptionCharge : subscriptionChargeList){
            chargeZuoraIds.add(subscriptionCharge.Zuora__OriginalProductRatePlanChargeId__c);
        }

        Map<String,zqu__ProductRatePlanCharge__c> productChargeMap = new Map<String,zqu__ProductRatePlanCharge__c>();
        for(zqu__ProductRatePlanCharge__c productCharge : [SELECT Id,zqu__ZuoraId__c,Busine__c 
                                                        FROM zqu__ProductRatePlanCharge__c 
                                                        WHERE zqu__ZuoraId__c IN :chargeZuoraIds]){ 
            productChargeMap.put(productCharge.zqu__ZuoraId__c,productCharge);
        }
        for(Zuora__SubscriptionProductCharge__c subscriptionCharge : subscriptionChargeList){
            if (productChargeMap.get(subscriptionCharge.Zuora__OriginalProductRatePlanChargeId__c) != null){
                subscriptionCharge.Product_Rate_Plan_Charge__c = productChargeMap.get(subscriptionCharge.Zuora__OriginalProductRatePlanChargeId__c).Id;
                subscriptionCharge.Business_Unit__c = productChargeMap.get(subscriptionCharge.Zuora__OriginalProductRatePlanChargeId__c).Busine__c;
                if(String.isNotBlank(subscriptionCharge.Prodotto_Vendita_Diretta_Text__c)){
                    subscriptionCharge.Prodotto_Vendita_Diretta__c = subscriptionCharge.Prodotto_Vendita_Diretta_Text__c;
                }
            }

        }
    }

    private static void sendErrorEmail(String messageBody){
        try {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'domus.support@reply.it' };
            message.subject = 'Salesforce.com - Errore';
            message.plainTextBody =  messageBody;
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {message});
        } catch(Exception e) {
        }
    }

    private static void aggiornaTipoCliente(List<Zuora__SubscriptionProductCharge__c> subProductCharge){
        System.debug('**************** aggiornaTipoCliente trigger after insert ****************');
        try{ 
            String prof = 'Professional';
            String diff = 'Diffusione';
            Set<Id> accountIdsProf = new Set<Id>();
            Set<Id> accountIdsDiff = new Set<Id>();
            for (Zuora__SubscriptionProductCharge__c spc : [SELECT id,Zuora__Subscription__c,Business_Unit__c,Zuora__Subscription__r.Zuora__Account__c,Zuora__Subscription__r.Zuora__Account__r.Tipo_cliente__c,Descrizione_Prodotto__c FROM Zuora__SubscriptionProductCharge__c WHERE Id IN :subProductCharge AND Supporto__c != null ]) {
                    System.debug('******************** spc ***************' + spc);
                    if(spc.Business_Unit__c == 'Professional') {
                       accountIdsProf.add(spc.Zuora__Subscription__r.Zuora__Account__c);
                    } else {
                       accountIdsDiff.add(spc.Zuora__Subscription__r.Zuora__Account__c);
                    }
            }

            
            List<Account> accountProf = [SELECT Id,Tipo_cliente__c FROM Account WHERE Id IN :accountIdsProf];
            List<Account> accountDiff = [SELECT Id,Tipo_cliente__c FROM Account WHERE Id IN :accountIdsDiff];
            System.debug('******************** accountProf ***************' + accountProf);
            System.debug('******************** accountDiff ***************' + accountDiff);
            for(Account acc : accountProf){
                    if(acc.Tipo_Cliente__c != 'Diffusione'){
                            acc.Tipo_Cliente__c = 'Professional';
                    } else { 
                        acc.Tipo_cliente__c = 'Professional;Diffusione';
                    }
            }

            for(Account acc : accountDiff){
                if(acc.Tipo_Cliente__c != 'Professional'){
                            acc.Tipo_Cliente__c = 'Diffusione';
                    } else { 
                        acc.Tipo_cliente__c = 'Professional;Diffusione';
                    }
            }


            
            AccountTriggerHandler.skip = true;
            update accountDiff;
            update accountProf;
            AccountTriggerHandler.skip = false;

            }catch(Exception e){
            System.debug('*******************ERRORE TIPOCLIENTE********' +e.getMessage() + ' ' + e.getStackTraceString());
        }
    }


    private static void ricalcolaSuscriptionTestata(List<Zuora__SubscriptionProductCharge__c> charges) { 
        //ogni volta che una charge viene createa/modificata/eliminata ricalcolo da zero il campo Testate__c della
        //subscription ad essa associata

        Zuora__Subscription__c subscriptionToUpdate = new Zuora__Subscription__c();
        List<String> testateList = new List<String>();
        Set<String> testateSet = new Set<String>();
        Boolean isTestataAlreadyInList = false;
        Set<Id> subscriptionIds = new Set<Id>();


        // recupero le subscription ad esse associate
        for(Zuora__SubscriptionProductCharge__c charge : charges) {
            subscriptionIds.add(charge.Zuora__Subscription__c);
        }
        Map<Id,Zuora__Subscription__c> subscriptionMap = new Map<Id,Zuora__Subscription__c>(
            [SELECT Id, Testate__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]);

        // resetto il campo testate delle subscription
        for (Zuora__Subscription__c subscription: subscriptionMap.values()) {
            subscription.Testate__c = '';
        }

        update subscriptionMap.values();



        //recupero TUTTE le charge associate alla subscription (non solo quelle appena create/aggiornate/eliminate)
        //Map<Id,List<Zuora__SubscriptionProductCharge__c>> chargesBySub = DomusUtil.getSubscriptionRatePlanCharges(subscriptionMap.keySet());
        charges = [SELECT Id, Testata__c, Zuora__Subscription__c
                    FROM Zuora__SubscriptionProductCharge__c
                    WHERE Zuora__Subscription__c IN :subscriptionIds];

        // aggiorno i campi testate
        for(Zuora__SubscriptionProductCharge__c charge: charges) {
            if (charge.Testata__c != null && charge.Testata__c != '') {
                //recupero la subscription associata
                subscriptionToUpdate = subscriptionMap.get(charge.Zuora__Subscription__c);
                // creo la lista delle testate
                if (subscriptionToUpdate.Testate__c != null) {
                    testateList = subscriptionToUpdate.Testate__c.split(';');
                    for(String testata: testateList) {
                        testateSet.add(testata.toUpperCase());
                    }
                }
                //controllo se la testata che voglio aggiungere e' gia' presente nella lista
                isTestataAlreadyInList = testateSet.contains(charge.Testata__c.toUpperCase()) ? true : false;

                if (!isTestataAlreadyInList) {
                    if (subscriptionToUpdate.Testate__c == '' || subscriptionToUpdate.Testate__c == null) {
                        subscriptionToUpdate.Testate__c = charge.Testata__c; 
                    } else {
                        subscriptionToUpdate.Testate__c += ';' + charge.Testata__c; 
                    }
                    subscriptionMap.put(charge.Zuora__Subscription__c, subscriptionToUpdate);
                }

                testateSet.clear();
            }
        } 

        update subscriptionMap.values();
    }


    
}