global class ApplyAmendmentBatch implements Database.Batchable<sObject>,Database.StateFul,Database.AllowsCallouts {
	String query;
	Set<Id> subIds = null;
	
	global ApplyAmendmentBatch() {
		List<string> amendmentFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Amendment__c').getDescribe().Fields.getMap().values()){
            amendmentFields.add(ft.getDescribe().getName().toLowerCase());
        }
        query = 'SELECT ' + String.join(amendmentFields, ',') + ' FROM Amendment__c WHERE Applied__c = false ORDER BY ZuoraCreatedDate__c, Priority__c';	// CR PROROGHE
        if(Test.isRunningTest()) query += ' LIMIT 100';
	}

	global ApplyAmendmentBatch(Date d) {
		List<string> amendmentFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Amendment__c').getDescribe().Fields.getMap().values()){
            amendmentFields.add(ft.getDescribe().getName().toLowerCase());
        }
        String dateStr = d.year() + '-' + (d.month() < 10 ? '0' + d.month() : '' + d.month()) + '-' + (d.day() < 10 ? '0' + d.day() : '' + d.day());
        query = 'SELECT ' + String.join(amendmentFields, ',') + ' FROM Amendment__c WHERE Date__c <= ' + dateStr +'  AND Applied__c = false ORDER BY ZuoraCreatedDate__c, Priority__c';	// CR PROROGHE
        if(Test.isRunningTest()) query += ' LIMIT 100';
	}

	global ApplyAmendmentBatch(Set<Id> subs) {
		subIds = subs;
		List<string> amendmentFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Amendment__c').getDescribe().Fields.getMap().values()){
            amendmentFields.add(ft.getDescribe().getName().toLowerCase());
        }
        query = 'SELECT ' + String.join(amendmentFields, ',') + ' FROM Amendment__c WHERE Applied__c = false AND Subscription__c IN :subIds ORDER BY ZuoraCreatedDate__c, Priority__c';	// CR PROROGHE
        if(Test.isRunningTest()) query += ' LIMIT 100';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Amendment__c> amendments = (List<Amendment__c>) scope;
		Set<Id> subscriptionIds = new Set<Id>();
		for(Amendment__c amendment : amendments){
			subscriptionIds.add(amendment.Subscription__c);
		}

		List<string> subscriptionFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Zuora__Subscription__c').getDescribe().Fields.getMap().values()){
            subscriptionFields.add(ft.getDescribe().getName().toLowerCase());
        }
		List<string> assetFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Asset').getDescribe().Fields.getMap().values()){
            assetFields.add(ft.getDescribe().getName().toLowerCase());
        }
        List<string> entitlementFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Entitlement__c').getDescribe().Fields.getMap().values()){
            entitlementFields.add(ft.getDescribe().getName().toLowerCase());
        }
        List<string> movStocksFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Movimentazione_Stock__c').getDescribe().Fields.getMap().values()){
            movStocksFields.add(ft.getDescribe().getName().toLowerCase());
        }
        String query = 'SELECT ' + String.join(subscriptionFields, ',') + 
                    ',( SELECT ' + String.join(assetFields, ',') + ' FROM CopieDigitaliCarta__r ) ' +
                    ',( SELECT ' + String.join(entitlementFields, ',') + ' FROM Entitlements__r ) ' +
                    ',( SELECT ' + String.join(movStocksFields, ',') + ' FROM Movimentazione_Stocks__r ) ' +
                ' FROM Zuora__Subscription__c ' +
                ' WHERE Id IN :subscriptionIds ';

		Map<Id,Zuora__Subscription__c> subscriptions = new Map<Id,Zuora__Subscription__c>((List<Zuora__Subscription__c>)Database.query(query));
		
		List<Asset> copieToUpdate = new List<Asset>();
		List<Entitlement__c> entitlementsToUpdate = new List<Entitlement__c>();
		List<Entitlement__c> entitlementsToCreate = new List<Entitlement__c>();
		List<Movimentazione_Stock__c> movimentazioneStockToUpdate = new List<Movimentazione_Stock__c>();

		for(Amendment__c amendment : amendments){
			System.debug(loggingLevel.Error, '*** amendment: ' + amendment);
			Zuora__Subscription__c subscription = subscriptions.get(amendment.Subscription__c);
			if(amendment.Type__c == 'SuspendSubscription'){
				if(!amendment.SubscriptionApplied__c && amendment.Date__c < Date.today()){
					subscription.Sospesa__c = true;
					subscription.Data_sospensione__c = amendment.Date__c;
					amendment.SubscriptionApplied__c = true;
				}
				if(!amendment.LabelRunEntitlementApplied__c){
					for(Asset copia : subscription.CopieDigitaliCarta__r){
						if(copia.Data_rateo__c >= amendment.Date__c){
							/** INIZIO CR PROROGHE **/
							//copia.Sospensione__c = true;
							if(amendment.Tipo_sospensione__c == 'Proroga'){
								copia.Proroga__c = true;
							}else if(amendment.Tipo_sospensione__c == 'Sospensione'){
								copia.Sospensione__c = true;
							}
							/** FINE CR PROROGHE **/
							upsertObjectInList(copia,copieToUpdate);
						}
					}
					System.debug(loggingLevel.Error, '*** subscription.Entitlements__r: ' + subscription.Entitlements__r);
					for(Entitlement__c entitlement : subscription.Entitlements__r){
						System.debug(loggingLevel.Error, '*** entitlement: ' + entitlement);
						if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('Virtualcomm').getRecordTypeId()){
							entitlement.expire_date__c = amendment.Date__c;
							if(entitlement.expire_date__c.day() <= 5){
				                entitlement.expire_date__c = entitlement.expire_date__c.addDays(7 - entitlement.expire_date__c.day());
				            }
				            entitlement.Marker__c = true;
						}else if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('San Pietro').getRecordTypeId()){
							entitlement.Data_disattivazione_ordine_professional__c = amendment.Date__c;
                        	entitlement.SP_Data_scadenza_ordine__c = amendment.Date__c.year() + String.valueOf(amendment.Date__c.month()).leftPad(2).replace(' ','0') + String.valueOf(amendment.Date__c.day()).leftPad(2).replace(' ','0');
                        	entitlement.Marker__c = true;
						}
			            upsertObjectInList(entitlement,entitlementsToUpdate);
					}
					amendment.LabelRunEntitlementApplied__c = true;
				}
			}else if(amendment.Type__c == 'ResumeSubscription'){
				if(!amendment.SubscriptionApplied__c && amendment.Date__c < Date.today()){
					subscription.Sospesa__c = false;
					subscription.Data_fine_sospensione__c = amendment.Date__c;
					amendment.SubscriptionApplied__c = true;
				}
				if(!amendment.LabelRunEntitlementApplied__c){
					for(Asset copia : subscription.CopieDigitaliCarta__r){
						if(copia.Data_rateo__c >= amendment.Date__c){
							/** INIZIO CR PROROGHE **/
							//copia.Sospensione__c = false;
							if(amendment.Tipo_sospensione__c == 'Proroga'){
								copia.Proroga__c = false;
							}else if(amendment.Tipo_sospensione__c == 'Sospensione'){
								copia.Sospensione__c = false;
							}
							/** FINE CR PROROGHE **/
							upsertObjectInList(copia,copieToUpdate);
						}
					}
					Entitlement__c entitlementDiffusione = null;
					List<Entitlement__c> entitlementProfessional = new List<Entitlement__c>();
					System.debug(loggingLevel.Error, '*** subscription.Entitlements__r: ' + subscription.Entitlements__r);
					for(Entitlement__c ent : subscription.Entitlements__r){
						System.debug(loggingLevel.Error, '*** ent: ' + ent);
						System.debug(loggingLevel.Error, '*** rt virtualcomm: ' + (ent.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('Virtualcomm').getRecordTypeId()));
                        if(ent.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('Virtualcomm').getRecordTypeId()){
    						entitlementDiffusione = ent;
                        }
                        if(ent.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('San Pietro').getRecordTypeId()){
                            entitlementProfessional.add(ent);
                        }
					}

					System.debug(loggingLevel.Error, '*** entitlementDiffusione: ' + entitlementDiffusione);
					if(entitlementDiffusione != null){
						System.debug(loggingLevel.Error, '*** entitlementDiffusione: ' + entitlementDiffusione);
						Entitlement__c resumedEntitlement = entitlementDiffusione.clone(false, true, false, false);
						resumedEntitlement.activation_date__c = amendment.Date__c;
						if(resumedEntitlement.activation_date__c.day() <= 5){
			                resumedEntitlement.activation_date__c = resumedEntitlement.activation_date__c.addDays(resumedEntitlement.activation_date__c.day() - 7);
			            }
			            resumedEntitlement.expire_date__c = subscription.Fine_Periodo_Gracing_Rinnovo__c;
						if(resumedEntitlement.expire_date__c.day() <= 5){
			                resumedEntitlement.expire_date__c = resumedEntitlement.expire_date__c.addDays(7 - resumedEntitlement.activation_date__c.day());
			            }
						entitlementDiffusione.Marker__c = false;
						upsertObjectInList(entitlementDiffusione,entitlementsToUpdate);
						resumedEntitlement.Marker__c = true;
						System.debug(loggingLevel.Error, '*** resumedEntitlement: ' + resumedEntitlement);
						System.debug(loggingLevel.Error, '*** entitlementsToCreate: ' + entitlementsToCreate);
			            upsertObjectInList(resumedEntitlement,entitlementsToCreate);
			            System.debug(loggingLevel.Error, '*** entitlementsToCreate: ' + entitlementsToCreate);
					}

					for(Entitlement__c ent : entitlementProfessional){
						Entitlement__c entitlementProfessionalCloned = ent.clone(false, true, false, false);
						entitlementProfessionalCloned.Data_attivazione_ordine_professional__c = amendment.Date__c;
                        entitlementProfessionalCloned.SP_Data_creazione_ordine__c = amendment.Date__c.year() + String.valueOf(amendment.Date__c.month()).leftPad(2).replace(' ','0') + String.valueOf(amendment.Date__c.day()).leftPad(2).replace(' ','0');
						entitlementProfessionalCloned.Data_disattivazione_ordine_professional__c = subscription.Zuora__SubscriptionEndDate__c;
                        entitlementProfessionalCloned.SP_Data_scadenza_ordine__c = subscription.Zuora__SubscriptionEndDate__c.year() + String.valueOf(subscription.Zuora__SubscriptionEndDate__c.month()).leftPad(2).replace(' ','0') + String.valueOf(subscription.Zuora__SubscriptionEndDate__c.day()).leftPad(2).replace(' ','0');
						ent.Marker__c = false;
						upsertObjectInList(ent,entitlementsToUpdate);
						entitlementProfessionalCloned.Marker__c = true;
						upsertObjectInList(entitlementProfessionalCloned,entitlementsToCreate);	
					}
					amendment.LabelRunEntitlementApplied__c = true;
				}
			}else if(amendment.Type__c == 'Cancellation'){
				if(!amendment.LabelRunEntitlementApplied__c){
					for(Entitlement__c entitlement : subscription.Entitlements__r){
						if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('Virtualcomm').getRecordTypeId()){
							entitlement.expire_date__c = subscription.Zuora__SubscriptionEndDate__c;
							if(entitlement.expire_date__c.day() <= 5){
				                entitlement.expire_date__c = entitlement.expire_date__c.addDays(7 - entitlement.activation_date__c.day());
				            }
						}else if(entitlement.RecordTypeId == Schema.SObjectType.Entitlement__c.getRecordTypeInfosByName().get('San Pietro').getRecordTypeId()){
							entitlement.Data_disattivazione_ordine_professional__c = subscription.Zuora__SubscriptionEndDate__c;
                        	entitlement.SP_Data_scadenza_ordine__c = subscription.Zuora__SubscriptionEndDate__c.year() + String.valueOf(subscription.Zuora__SubscriptionEndDate__c.month()).leftPad(2).replace(' ','0') + String.valueOf(subscription.Zuora__SubscriptionEndDate__c.day()).leftPad(2).replace(' ','0');
						}
			            upsertObjectInList(entitlement,entitlementsToUpdate);
					}
					List<Movimentazione_Stock__c> movimentazioniReso = new List<Movimentazione_Stock__c>();
					Map<Id,Integer> deltaMovimentazioni = new Map<Id,Integer>();
					Map<Id,Movimentazione_Stock__c> movimentazioniToClone = new Map<Id,Movimentazione_Stock__c>();
					for(Movimentazione_Stock__c movimentazioneStock : subscription.Movimentazione_Stocks__r){
						movimentazioniToClone.put(movimentazioneStock.Prodotto_Vendita_Diretta__c,movimentazioneStock);
						if(!deltaMovimentazioni.containsKey(movimentazioneStock.Prodotto_Vendita_Diretta__c)){
							deltaMovimentazioni.put(movimentazioneStock.Prodotto_Vendita_Diretta__c,0);
						}
						Integer delta = deltaMovimentazioni.get(movimentazioneStock.Prodotto_Vendita_Diretta__c);
						if(movimentazioneStock.Segno_del_movimento_del__c == '+'){
							delta += Integer.valueOf(movimentazioneStock.Quantita__c);
						}else if(movimentazioneStock.Segno_del_movimento_del__c == '-'){
							delta -= Integer.valueOf(movimentazioneStock.Quantita__c);
						}
					}
					for(Id prodottoVenditaDiretta : deltaMovimentazioni.keySet()){
						Integer deltaMovimentazione = deltaMovimentazioni.get(prodottoVenditaDiretta);
						if(deltaMovimentazione < 0){
							Movimentazione_Stock__c movimentazioneStockToClone = movimentazioniToClone.get(prodottoVenditaDiretta);
							Movimentazione_Stock__c reso = movimentazioneStockToClone.clone(false,true,false,false);
							reso.Segno_del_movimento_del__c = '+';
							reso.Quantita__c = deltaMovimentazione;
							movimentazioniReso.add(reso);
							
						}
					}
					insert movimentazioniReso;
					amendment.LabelRunEntitlementApplied__c = true;
				}	
			}else if(amendment.Type__c == 'OwnerTransfer'){
				movimentazioneStockToUpdate.addAll(subscription.Movimentazione_Stocks__r);
			}
		}

		SubscriptionTriggerHandler.skipTrigger = true;
		update subscriptions.values();
		update copieToUpdate;
		System.debug(loggingLevel.Error, '*** entitlementsToCreate: ' + entitlementsToCreate);
		update entitlementsToUpdate;
		insert entitlementsToCreate;
		update movimentazioneStockToUpdate;
		update amendments; 
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private void upsertObjectInList(sObject obj, List<sObject> objList){
		Integer indexToReplace = null;
        for(Integer i=0; i<objList.size(); i++){
        	sObject o = objList.get(i);
        	if(o.get('Id') != null && o.get('Id') == obj.get('Id')){
        		indexToReplace = i;
        		break;
        	}
        	
        }
        if(indexToReplace != null)objList.remove(indexToReplace);
		objList.add(obj);
	}

}