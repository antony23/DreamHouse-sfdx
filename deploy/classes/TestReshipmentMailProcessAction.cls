@isTest(SeeAllData = true)
public class TestReshipmentMailProcessAction {
	private static Blocco_Selettivo_Notifiche__c bsn;
	private static Zuora__Subscription__c subscription;
	private static Contact contact;
	private static Account account;
	private static Map<Id, Asset> assetMap;

	@isTest
	public static void testSendEmail() {
		setup();

		Test.startTest();

		ReshipmentMailProcessAction.sendEmail(assetMap.values());

		Test.stopTest();
	}

	@isTest
	public static void testFetchEmailData() {
		setup();

		Test.startTest();

		final List<EmailDataBundle> data = ReshipmentMailProcessAction.fetchEmailData(assetMap.values(), bsn);

		for(EmailDataBundle edb : data) {
			System.assertEquals(edb.getRecipient().Id, contact.Id);
			System.assertEquals(edb.getTemplate().DeveloperName, 'MT10');

			Asset relatedTo = (Asset) edb.getRelatedTo();

			System.assert(assetMap.containsKey(relatedTo.Id));
			System.assertEquals(relatedTo.Id, assetMap.get(relatedTo.Id).Id);
		}

		Test.stopTest();
	}


	private static void setup() {
		bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
		bsn.MT10_Abilitato__c = true;
		update bsn;

		SubscriptionTriggerHandler.skipTrigger = true;

		RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'B2C' LIMIT 1];

		account = new Account(Name = 'TestAccount', Notifiche_Email_Disabilitate__c = false, RecordTypeId = rt.Id);
		insert account;

		contact = new Contact(FirstName = 'FirstNameTest', LastName = 'LastNameTest', AccountId = account.Id, Email = 'test@email.com');
		insert contact;

		subscription = new Zuora__Subscription__c(Name = 'A-S00000000', Zuora__Account__c = account.Id, Contact_Bill_To__c = contact.Id);
		insert subscription;

		final List<Asset> assetList = new List<Asset>();
		for(Integer i = 1; i <= 6; i++) {
			assetList.add(new Asset(Name = 'Asset_' + i, Notifica_Rispedizione__c = false, Inviata__c = true, Rispedizione__c = true, AccountId = account.Id, ContactId = contact.Id, SubscriptionAllCopy__c = subscription.Id));
		}

		insert assetList;

		assetMap = new Map<Id, Asset>([SELECT Id, Name, Notifica_Rispedizione__c, Inviata__c, Rispedizione__c, AccountId, ContactId, SubscriptionAllCopy__c FROM Asset WHERE SubscriptionAllCopy__c = :subscription.Id]);
	}

}