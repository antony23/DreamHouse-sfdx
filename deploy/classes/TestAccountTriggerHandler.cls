@isTest
public class TestAccountTriggerHandler {
	
	@isTest static void testM(){
		Account b2b = new Account(
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Small Business').getRecordTypeId(),
			Name = 'Test Account'
		);
		insert b2b;

		// se creo un account b2b viene creato un contatto in automatico
		System.assertEquals(1, [SELECT Id FROM Contact WHERE AccountId = :b2b.Id].size());

		// non puoi modificare un cliente, devi modificare il contatto principale
		b2b.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId();
		b2b.Name = 'New name';
		Database.SaveResult result = Database.update(b2b,false);
		//System.assert(! result.isSuccess());
	}

	static testMethod void testCheckNameB2C(){
        Account aB2C = new Account();
        aB2C.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId();
        String name = 'Campeon Moreno';
        aB2C.Name = name;

        insert aB2C;
        try{
            aB2C.Name = 'Cristiano Moreno';
            update aB2C;

        }catch(Exception e){
            List<Account> accList = [SELECT Id,Name FROM Account WHERE Id = :aB2C.Id];
            if(accList != null && accList.size() > 0){
                System.assertEquals(accList.get(0).Name, name);
            }
        }

    }

    static testMethod void testInsertMainContact(){
        Account aB2B = new Account();
        aB2B.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId();
        aB2B.Name = 'Uchiha Madara';

        insert aB2B;

        System.debug(aB2B.Id);
        List<Contact> c =  [SELECT AccountId, LastName, MainContact__c
                            FROM Contact
                            WHERE AccountId = :aB2B.Id];
        System.assert(c!=null && c.size() >0);
        if(c!=null && c.size() >0){
            System.assertEquals(c.get(0).LastName, aB2B.Name);
            System.assertEquals(c.get(0).MainContact__c, true);
        }

    }
}