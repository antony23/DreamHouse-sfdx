public class CaseTriggerHandler {
	public static Boolean skip = false;

	public static void onBeforeInsert(List<Case> triggerNew, Map<Id, Case> triggerNewMap) {
		setFields(triggerNew);
    }
    
    public static void onAfterInsert(List<Case> triggerNew, Map<Id, Case> triggerNewMap) {
        updateSubscriptions(triggerNew);
    }

    public static void onBeforeUpdate(List<Case> triggerOld, List<Case> triggerNew, Map<Id, Case> triggerOldMap, Map<Id, Case> triggerNewMap){   
    	setFields(triggerNew);
    }

    public static void onAfterUpdate(List<Case> triggerOld, List<Case> triggerNew, Map<Id, Case> triggerOldMap, Map<Id, Case> triggerNewMap){   
        updateSubscriptions(triggerOld);
        //propagateSospensioneProroga(triggerNew);    // CR PROROGHE
    }

    public static void onBeforeDelete(List<Case> triggerOld, Map<Id, Case> triggerOldMap){  
    }

    public static void onAfterDelete(List<Case> triggerOld, Map<Id, Case> triggerOldMap){  
    }

    private static void propagateSospensioneProroga(List<Case> cases){
        Set<Id> subscriptionsProroga = new Set<Id>();
        Set<Id> subscriptionsSospensione = new Set<Id>();
        for(Case c : cases){
            /*if(c.Sottocategoria__c == 'Proroga'){
                subscriptionsProroga.add(c.Subscription__c);
            }else if(c.Sottocategoria__c == 'Sospensione Temporanea'){
                subscriptionsSospensione.add(c.Subscription__c);
            }*/
            if(c.Sottocategoria__c != null && c.Sottocategoria__c.startsWith('PROROGA')){
                subscriptionsProroga.add(c.Subscription__c);
            }
        }

        List<Zuora__Subscription__c> subscriptions = [SELECT Id,
                                                        (SELECT Id,Tipo_sospensione__c FROM Amendments__r WHERE Type__c = 'SuspendSubscription')
                                                        FROM Zuora__Subscription__c
                                                        WHERE Id IN :subscriptionsSospensione OR Id IN :subscriptionsProroga];
        List<Amendment__c> amendmentsToUpdate = new List<Amendment__c>();
        for(Zuora__Subscription__c subscription : subscriptions){
            for(Amendment__c amendment : subscription.Amendments__r){
                if(subscriptionsProroga.contains(subscription.Id)){
                    amendment.Tipo_sospensione__c = 'Proroga';
                }else if(subscriptionsSospensione.contains(subscription.Id)){
                    amendment.Tipo_sospensione__c = 'Sospensione';
                }
                amendmentsToUpdate.add(amendment);
            }
        }

        update amendmentsToUpdate;
    }

    public static void setFields(List<Case> cases){
    	Set<Id> subscriptionIds = new Set<Id>();
    	for(Case c : cases){
            if(c.Subscription__c != null){
    		  subscriptionIds.add(c.Subscription__c);
            }
    	}
    	Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>(
    		[SELECT Id,Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]
    	);
    	for(Case c : cases){
    		Zuora__Subscription__c subscription = subscriptionsMap.get(c.Subscription__c);
            if(subscription != null){
    		  c.Subscription_Zuora_Id__c = subscription.Zuora__Zuora_Id__c;
            }
    	}

    }

    private static void updateSubscriptions(List<Case> cases){
        List<Id> caseIds = new List<Id>();
        for(Case c : cases){
            if(c.Categoria__c == 'RECESSO' || c.Sottocategoria__c == 'MODIFICA INDIRIZZO CON VARIAZIONE SPESE'){
                caseIds.add(c.Id);
            }
        }
        if(caseIds.size() > 0){
            updateSubscriptions(caseIds);
        }
    }

    @future(callout=true)
    private static void updateSubscriptions(List<Id> caseIds){ 
        try {
            Set<Id> subscriptionIds = new Set<Id>();
            List<Case> cases = [SELECT Id,Subscription__c,Sottocategoria__c FROM Case WHERE Id IN :caseIds];
            for(Case c : cases){
                if(c.Subscription__c != null){
                  subscriptionIds.add(c.Subscription__c);
                }
            }
            Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>(
                [SELECT Id,Zuora__External_Id__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]
            );
            
            Zuora.zApi zApiInstance = new Zuora.zApi();
            Zuora.zApi.LoginResult loginResult = zApiInstance.zlogin();
            
            List<Zuora.zObject> zSubscriptions = new List<Zuora.zObject>();
            for(Case c : cases){
                Zuora__Subscription__c subscription = subscriptionsMap.get(c.Subscription__c);
                if(subscription != null){
                    Zuora.zObject zSubscription = new Zuora.zObject('Subscription');
                    zSubscription.setValue('Id', subscription.Zuora__External_Id__c);
                    zSubscription.setValue('Tipointerruzione__c', c.Sottocategoria__c);
                    zSubscriptions.add(zSubscription);
                }
            }

            if(zSubscriptions.size() > 0){

                List<List<Zuora.zObject>> zSubscriptionBatches = new List<List<Zuora.zObject>>();
                for(Integer batchIndex = 0 ; batchIndex < (zSubscriptions.size()/50)+1 ; batchIndex++){
                    List<Zuora.zObject> zSubscriptionsBatch = new List<Zuora.zObject>();
                    for(Integer j = (batchIndex*50); (j<(batchIndex*50)+50) && j<zSubscriptions.size() ; j++){
                        zSubscriptionsBatch.add(zSubscriptions.get(j));
                    }
                    zSubscriptionBatches.add(zSubscriptionsBatch);
                }

                Map<String,String> errors = new Map<String,String>();
                for(List<Zuora.zObject> zSubscriptionsBatch : zSubscriptionBatches){
                    for (Zuora.zApi.SaveResult result : zapiInstance.zUpdate(zSubscriptionsBatch)) {
                        if (result.success) continue;
                        for (Zuora.zObject error : result.errors){
                            errors.put((String) error.getValue('Id'),(String) error.getValue('Message'));
                        }
                    }
                }

                if(errors.size() > 0){
                    String messageBody = 'Di seguito le subscriptions non aggiornate: \r\n';
                    for(String subscriptionId : errors.keySet()){
                        messageBody += 'Subscription: ' + subscriptionId + ' - Errore: ' + errors.get(subscriptionId) + '\r\n';
                    }
                    sendErrorEmail(messageBody);
                }
                
            }
        }catch(Exception e){
            sendErrorEmail(e.getMessage() + ' - ' + e.getStackTraceString());
        }
    }

    private static void sendErrorEmail(String messageBody){
        try {
            final String to = 'domus.support@reply.it';
            final String subject = 'Salesforce.com - Errore';
            final String body = 'Non è stato possibile aggiornare il tipo di interruzione delle subscriptions su Zuora.\r\n' + messageBody;

            final Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ EmailSendingUtil.prepareEmail(to, subject, body) });
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e);
        }
    }

}