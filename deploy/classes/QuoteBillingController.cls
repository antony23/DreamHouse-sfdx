public class QuoteBillingController {
	
	public zqu.Quote quote {get;set{quote = value; initBilling();}}
	public zqu__Quote__c quoteObj {get;set;}
	public List<Zuora__CustomerAccount__c> existingBillingList {get;set;}
	public String existingBillingAccountId {get;set;}
    public String metodoDiPagamento{get;set;}
	public String billingAccountType {get;set;}
    public Id quoteIdEdit {get;set;}
	public Boolean doInit = true;
    public Boolean isAbbonamenti {get;set;}
    public Boolean isPubblAmministrazione {get;set;}
    public String billinAccountIntermediarioId {get;set;}
    public Boolean isCartaDiCredito {get;set;}
    public List<SelectOption> billingAccountOptions {get;set;}
    public String selectedBillingAccountOption {get;set;}
    public boolean isWarning {get;set;}
	public Boolean isExistingBillingSectionVisible {
		get{
            try {
                return existingBillingList.size() > 0;
            } catch(Exception e) {
                appendErrorMessage(e);
            }
            return false;
		}
	}

    private Map<String,String> paymentMethodsMap;
	public List<SelectOption> paymentMethods {get;set;}

	public QuoteBillingController() {

	}

    private void initPaymentMethods(){
        paymentMethodsMap = new Map<String,String>();
        List<SelectOption> res = new List<SelectOption>();

        try {
            List<MetodoDiPagamento__mdt> paymentMethodList = [SELECT MasterLabel,ZuoraMethod__c,Active__c,Order__c FROM MetodoDiPagamento__mdt WHERE Active__c=true /*ORDER BY Order__c ASC*/];
            List<MetodoDiPagamento__mdt> paymentMethodList2 = [SELECT MasterLabel FROM MetodoDiPagamento__mdt WHERE MasterLabel='Paypal' ORDER BY Order__c ASC];
            Set<MetodoDiPagamento__mdt> paymentMethodSet = new Set<MetodoDiPagamento__mdt>(paymentMethodList);
            List<zqu__quoterateplan__c> quoteRatePlanList = [SELECT Id, zqu__productrateplan__r.MetodidiPagamento__c
                                                            FROM zqu__quoterateplan__c 
                                                            WHERE zqu__quote__c = :quote.getId()
                                                            AND zqu__productrateplan__r.MetodidiPagamento__c != null];
            System.debug('*********paymentMethodList2************'+paymentMethodList2);
            string t = '';
            for ( MetodoDiPagamento__mdt r : paymentMethodSet) t += '\n' + r;
            System.debug('***********************STRINGA VALORI MAPPA***********************'+t);


            for(zqu__quoterateplan__c tmpRatePlan : quoteRatePlanList){
                System.debug('Method: ' +tmpRatePlan.zqu__productrateplan__r.MetodidiPagamento__c);
                if(tmpRatePlan.zqu__productrateplan__r.MetodidiPagamento__c!=null && tmpRatePlan.zqu__productrateplan__r.MetodidiPagamento__c != 'All'){
                    String metodoListString = tmpRatePlan.zqu__productrateplan__r.MetodidiPagamento__c.toLowerCase();
                    Set<String> metodoDiPagamento = new Set<String>(metodoListString.split(', '));
                        //COMMENTATO DA LG PER PROVA
                    System.debug('## '+metodoListString);
                    System.debug('## '+metodoDiPagamento);
                    for(MetodoDiPagamento__mdt method : paymentMethodSet){
                        if(!metodoDiPagamento.contains(method.MasterLabel.toLowerCase())){
                            System.debug('@@@ '+method);
                            System.debug('@@@ '+method.MasterLabel.toLowerCase());
                            paymentMethodSet.remove(method);
                        }
                    }
                }else if (tmpRatePlan.zqu__productrateplan__r.MetodidiPagamento__c == 'All'){
                    //Do nothing
                    System.debug('All methods accepted');
                } else{
                    paymentMethodSet = new Set<MetodoDiPagamento__mdt>();
                }
            }
            System.debug('Accepted payment methods: ' + paymentMethodSet);
            if(paymentMethodSet.size() > 0){
                for(MetodoDiPagamento__mdt method : paymentMethodSet){
                    paymentMethodsMap.put(method.MasterLabel,method.ZuoraMethod__c);
                    res.add(new SelectOption(method.MasterLabel,method.MasterLabel));
                }
            }else{
                for(MetodoDiPagamento__mdt method: paymentMethodList){
                    if(method.MasterLabel == 'Carta di credito'){
                        paymentMethodsMap.put(method.MasterLabel,method.ZuoraMethod__c);
                        res.add(new SelectOption(method.MasterLabel,method.MasterLabel));
                        break;
                    }   
                }
            }

            System.debug(loggingLevel.Error, '*** paymentMethodsMap: ' + paymentMethodsMap);
            string q = '';
            for ( string r : paymentMethodsMap.keySet()) q += '\n' + r;
            System.debug('***********************STRINGA VALORI MAPPA INIZIALE ***********************'+ q);


        } catch(Exception e) {
            appendErrorMessage(e);
        }
        paymentMethods = new List<SelectOption>();
        paymentMethods.add(new SelectOption('','- Seleziona -'));
        paymentMethods.addAll(res);

    }


	private void initBilling(){
		try {
            if(doInit){
                initPaymentMethods();
                isAbbonamenti = false;
                isCartaDiCredito = false;
    	   		quoteObj = quote.getSObject();
                //Valori di default
                quoteObj.zqu__Currency__c = 'EUR';
                //quoteObj.Metodo_di_pagamento__c = null;

                //if(String.isBlank(quoteObj.zqu__BillingMethod__c)){
                    /*quoteObj.zqu__BillingMethod__c = 'Carta';*/
                //}
                if(String.isBlank(quoteObj.Regalo__c)){
                    quoteObj.Regalo__c = 'NO';
                }
                if(String.isBlank(quoteObj.Blocco_Rinnovo__c)){
                    quoteObj.Blocco_Rinnovo__c = 'NO';
                }
                if(String.isBlank(quoteObj.Inibisci_Gracing__c)){
                    quoteObj.Inibisci_Gracing__c = 'NO';
                }
                

                List<Account> accountList = [SELECT Id, Industry FROM Account WHERE Id = :quoteObj.zqu__Account__c];
                String industry;
                if(accountList != null && accountList.size()>0){
                    industry = accountList.get(0).Industry;
                }
                isPubblAmministrazione = false;
                String tipoCliente = UtilFatturazione.getTipoCliente(quoteObj.zqu__Account__c);
                if (tipoCliente == 'Pubblica Amministrazione' || industry == 'Ente Parastatale'){
                    isPubblAmministrazione = true;
                }

    			List<Contact> contattiPrincipaleAccount = [SELECT Id FROM Contact WHERE AccountId = :quoteObj.zqu__Account__c AND MainContact__c = true];
                if(contattiPrincipaleAccount != null && contattiPrincipaleAccount.size() > 0){
                    quoteObj.zqu__BillToContact__c = contattiPrincipaleAccount.get(0).Id;
                    refreshContactData();
                }
                List<zqu__QuoteRatePlanCharge__c> chargeAbbonamentiList = DomusUtil.getChargeAbbonamenti(quote.getId());
                if(chargeAbbonamentiList != null && chargeAbbonamentiList.size()>0){
                    isAbbonamenti=true;
                    quoteObj.ConAvvisi__c = chargeAbbonamentiList.get(0).zqu__QuoteRatePlan__r.zqu__ProductRatePlan__r.ConAvvisi__c;
                }

                billingAccountOptions = new List<SelectOption>();
                billingAccountOptions.add(new SelectOption('new', 'Crea un nuovo billing account'));
                billingAccountOptions.add(new SelectOption('old', 'Utilizza un billing account esistente'));
                // MODIFICHE LG 12/06/2017
           /*     if(quoteObj.Intermediario__c != null && quoteObj.Fattura_a_intermediario__c ){
                    loadExistingBillingListFromIntermediario();
                }else{
                    loadExistingBillingListFromAccount();
                }*/

                if(quoteObj.Intermediario__c != null){
                    quoteObj.Emissione_Documento__c = 'NO';
                }

                if(quoteObj.Intermediario__c !=null && quoteObj.Fattura_a__c == 'Intermediario'){
                    loadExistingBillingListFromIntermediario();
                } else if (quoteObj.Azienda__c !=null && quoteObj.Fattura_a__c == 'Azienda'){
                    loadExistingBillingListFromAzienda();
                }   else {
                    loadExistingBillingListFromAccount();
                }

                System.debug(loggingLevel.Error, '*** quoteObj.Referenteintestatariodocumentocontabi__c: ' + quoteObj.Referenteintestatariodocumentocontabi__c);
                quoteObj.Referenteintestatariodocumentocontabi__c = null;
                quoteObj.zqu__BillingMethod__c = 'Print';
            }
            doInit = false;
		} catch(Exception e) {
			appendErrorMessage(e);
		}
	}

    public PageReference loadExistingBillingListFromAccount(){
        quoteObj.zqu__ZuoraAccountID__c = null;
        quoteObj.zqu__PaymentMethod__c = null;
        quoteObj.zqu__BillingMethod__c = null;
        System.debug(loggingLevel.Error, '*** loadExistingBillingListFromAccount');
        
		existingBillingList = [
                                SELECT Id, Name, Zuora__Zuora_Id__c, Zuora__AccountNumber__c, Zuora__BillToName__c, Zuora__SoldToName__c,
                                BillToFullAddress__c, BillToAddressCO__c, SoldToFullAddress__c, SoldToAddressCO__c, Zuora__PaymentTerm__c, Zuora__Credit_Balance__c, 
                                Zuora__DefaultPaymentMethod__c, Credit_Card_Summary__c,Metodo_di_pagamento__c, Zuora__Balance__c
                                FROM Zuora__CustomerAccount__c 
                                WHERE Zuora__Account__c = :quoteObj.zqu__Account__c
                                AND Zuora__Payment_Term__r.Name != 'Intermediario'
                                AND Metodo_di_pagamento__c = :paymentMethodsMap.keySet()
                                //order by createddate desc limit 200
                                /** Inizio DH-1404 **/
                                AND Obsoleto__c != 'SI'
                                ORDER BY LastModifiedDate DESC, CreatedDate DESC
                                LIMIT 500
                                /** Fine DH-1404 **/
                            ];
        selectedBillingAccountOption = existingBillingList.size() > 0 ? 'old' : 'new';
        return null;
    }

    public PageReference loadExistingBillingListFromIntermediario(){
        quoteObj.zqu__ZuoraAccountID__c = null;
        System.debug(loggingLevel.Error, '*** loadExistingBillingListFromIntermediario');
        System.debug(loggingLevel.Error, '*** quoteObj.Intermediario__c: ' + quoteObj.Intermediario__c);
        System.debug(loggingLevel.Error, '*** paymentMethodsMap.keySet(): ' + paymentMethodsMap.keySet());
        existingBillingList = [SELECT Id, Name, Zuora__Zuora_Id__c, Zuora__AccountNumber__c, Zuora__BillToName__c, Zuora__SoldToName__c,
                                BillToFullAddress__c, BillToAddressCO__c, SoldToFullAddress__c, SoldToAddressCO__c, Zuora__PaymentTerm__c, Zuora__Credit_Balance__c, 
                                Zuora__DefaultPaymentMethod__c, Credit_Card_Summary__c,Metodo_di_pagamento__c, Zuora__Balance__c
                                FROM Zuora__CustomerAccount__c 
                                WHERE Zuora__Account__c = :quoteObj.Intermediario__c
                                AND Metodo_di_pagamento__c = :paymentMethodsMap.keySet()
                                //order by createddate desc limit 200
                                /** Inizio DH-1404 **/
                                AND Obsoleto__c != 'SI'
                                ORDER BY LastModifiedDate DESC, CreatedDate DESC
                                LIMIT 500
                                /** Fine DH-1404 **/
                            ];
        System.debug(loggingLevel.Error, '*** existingBillingList.size(): ' + existingBillingList.size());
        selectedBillingAccountOption = existingBillingList.size() > 0 ? 'old' : 'new';
        System.debug(loggingLevel.Error, '*** selectedBillingAccountOption: ' + selectedBillingAccountOption);
        return null;
    }

     public PageReference loadExistingBillingListFromAzienda(){
        quoteObj.zqu__ZuoraAccountID__c = null;
        System.debug(loggingLevel.Error, '*** loadExistingBillingListFromAzienda');
        System.debug(loggingLevel.Error, '*** quoteObj.Azienda__c: ' + quoteObj.Azienda__c);
        System.debug(loggingLevel.Error, '*** paymentMethodsMap.keySet(): ' + paymentMethodsMap.keySet());
        existingBillingList = [SELECT Id, Name, Zuora__Zuora_Id__c, Zuora__AccountNumber__c, Zuora__BillToName__c, Zuora__SoldToName__c,
                                BillToFullAddress__c, BillToAddressCO__c, SoldToFullAddress__c, SoldToAddressCO__c, Zuora__PaymentTerm__c, Zuora__Credit_Balance__c, 
                                Zuora__DefaultPaymentMethod__c, Credit_Card_Summary__c,Metodo_di_pagamento__c, Zuora__Balance__c
                                FROM Zuora__CustomerAccount__c 
                                WHERE Zuora__Account__c = :quoteObj.Azienda__c
                                AND Metodo_di_pagamento__c = :paymentMethodsMap.keySet()
                                //order by createddate desc limit 200
                                /** Inizio DH-1404 **/
                                AND Obsoleto__c != 'SI'
                                ORDER BY LastModifiedDate DESC, CreatedDate DESC
                                LIMIT 500
                                /** Fine DH-1404 **/
                            ];
        System.debug(loggingLevel.Error, '*** existingBillingList.size(): ' + existingBillingList.size());
        selectedBillingAccountOption = existingBillingList.size() > 0 ? 'oldAzienda' : 'new';
        System.debug(loggingLevel.Error, '*** selectedBillingAccountOption: ' + selectedBillingAccountOption);
        string s = '';
        for ( string r : paymentMethodsMap.keySet()) s += '\n' + r;
        System.debug('***********************STRINGA VALORI MAPPA***********************'+s);

        return null;
    }


	public PageReference goToCart(){
        return Page.WizardCart;
    }

    public PageReference exitWizard(){
        PageReference res = null;
        try {
            quoteObj.Quote_Status__c = 'Cancelled';
            quote.save();
            if(quoteObj.Subscription_Precedente__c != null){
                res = new PageReference('/'+quoteObj.Subscription_Precedente__c);
            }
            else if(quoteIdEdit != null){
                res = new PageReference('/'+quoteObj.Id);
            }else{
                if(quoteObj.zqu__Account__c != null){
                    res = new PageReference('/'+quoteObj.zqu__Account__c);
                }else{
                    res = new PageReference('/001');
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return res;
    }

    public PageReference selectBillingAccount(){
        System.debug('*******************FATTURA A ****************'+ quoteObj.Fattura_a__c); 
        if(quoteObj.Fattura_a__c == '--None--' || quoteObj.Fattura_a__c == null ){
            return goToSpeseSpedizione();
        } /*else if (quoteObj.Fattura_a__c == 'Intermediario'){  // DH-818
        // intermediario
        System.debug(loggingLevel.Error, '*** existingBillingAccountId: ' + existingBillingAccountId);
        billinAccountIntermediarioId = existingBillingAccountId;
        quoteObj.zqu__ZuoraAccountID__c = billinAccountIntermediarioId;
        quoteObj.zqu__PaymentMethod__c = 'Other';
        quoteObj.Metodo_di_pagamento__c = 'Intermediario';
        quoteObj.zqu__PaymentTerm__c = 'Intermediario';
        quoteObj.zqu__BillingMethod__c = 'Print';
        return null;
        }*/ else { 
            System.debug(loggingLevel.Error, '*** existingBillingAccountId: ' + existingBillingAccountId);
            
            billinAccountIntermediarioId = existingBillingAccountId;
            quoteObj.zqu__ZuoraAccountID__c = billinAccountIntermediarioId;
            Zuora__CustomerAccount__c informazioniAzienda = null;
            System.debug('************** ID PER LA QUERY************* ' + billinAccountIntermediarioId +'**************' + existingBillingAccountId);
            informazioniAzienda = [SELECT Zuora__PaymentMethod_Type__c, Zuora__PaymentTerm__c, Metodo_di_pagamento__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :billinAccountIntermediarioId];
            System.debug('************INFORMAZIONI AZIENDA ***************' + informazioniAzienda);
            quoteObj.zqu__PaymentMethod__c = 'Other';
            quoteObj.Metodo_di_pagamento__c = informazioniAzienda.Metodo_di_pagamento__c;
            quoteObj.zqu__PaymentTerm__c = informazioniAzienda.Zuora__PaymentTerm__c;
            quoteObj.zqu__BillingMethod__c = 'Print';
            return null;
        }
    }

    public PageReference goToSpeseSpedizione() {
        PageReference res = null;
        try {
            quoteObj.Punto_di_scarico__c = quoteObj.Puntodiscarico__c;
            if(isPubblAmministrazione && quoteObj.CIG__c == null && quoteObj.CUP__c == null){
                appendErrorMessage('Valorizzare almeno un valore tra CIG e CUP.');
                return null;
            }


            System.debug('goToSpeseSpedizione');
            if(quoteObj.Fattura_a__c == 'Intermediario' || quoteObj.Fattura_a__c == 'Azienda'){
                billingAccountType = 'new';
                existingBillingAccountId = null;   
            } 
            System.debug(loggingLevel.Error, '*** quoteObj.zqu__BillingMethod__c: ' + quoteObj.zqu__BillingMethod__c);
            if(billingAccountType == 'existing' && existingBillingAccountId == null){
                appendErrorMessage('Scegli un billing account per procedere.');
                return null;
            }
            quoteObj.zqu__ZuoraAccountID__c = existingBillingAccountId;

            System.debug('existingBillingAccountId:' + existingBillingAccountId);

            if(billingAccountType == 'existing'){
                System.debug('existing');
                quoteObj.zqu__PaymentMethod__c = null;
                quoteObj.zqu__BillingMethod__c = null;
                quoteObj.zqu__BillToContact__c = null;
                quoteObj.zqu__SoldToContact__c = null;
                quoteObj.Metodo_di_pagamento__c = metodoDiPagamento;

                Zuora__CustomerAccount__c custAcc;
                if(String.isNotBlank(quoteObj.zqu__InvoiceOwnerId__c)){
                    custAcc = [SELECT Id, Bill_To_Salesforce__c,Bill_To_Salesforce__r.ContactAddress__c ,Zuora__PaymentTerm__c,Metodo_di_pagamento__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :quoteObj.zqu__InvoiceOwnerId__c LIMIT 1]; //quoteObj.zqu__ZuoraAccountID__c
                }else{
                    custAcc = [SELECT Id, Bill_To_Salesforce__c,Zuora__PaymentTerm__c,Metodo_di_pagamento__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :existingBillingAccountId LIMIT 1]; //quoteObj.zqu__ZuoraAccountID__c
                    
                }

                DomusUtil.setInvoiceAndActivationDate(quoteObj,custAcc.Metodo_di_pagamento__c,custAcc.Zuora__PaymentTerm__c);

                if(quoteObj.Referenteintestatariodocumentocontabi__c == null){
                    quoteObj.Referenteintestatariodocumentocontabi__c = custAcc.Bill_To_Salesforce__c;
                }

            }else{ //Creazione nuovo billing
                if(String.isBlank(quoteObj.Metodo_di_pagamento__c)){
                    appendErrorMessage('Scegliere un metodo di pagamento per procedere.');
                    return null;   
                }
                System.debug(loggingLevel.Error, '***new');
                Boolean inputError = false;
                quoteObj.zqu__ZuoraAccountID__c = null;
                System.debug(loggingLevel.Error, '*** quoteObj.Fattura_a__c: ' + quoteObj.Fattura_a__c);
                System.debug(loggingLevel.Error, '*** quoteObj.zqu__BillToContact__c: ' + quoteObj.zqu__BillToContact__c);
                System.debug(loggingLevel.Error, '*** quoteObj.zqu__SoldToContact__c: ' + quoteObj.zqu__SoldToContact__c);

                if((quoteObj.Fattura_a__c == '--None--' && quoteObj.zqu__BillToContact__c == null) || quoteObj.zqu__SoldToContact__c == null){
                    appendErrorMessage('Per proseguire è necessario inserire referente di fatturazione e referente di spedizione.');
                    return null; 
                }

                if(quoteObj.Referenteintestatariodocumentocontabi__c == null){
                    quoteObj.Referenteintestatariodocumentocontabi__c = quoteObj.zqu__BillToContact__c;
                }

                if(quoteObj.zqu__BillingMethod__c == null){
                    quoteObj.zqu__BillingMethod__c.addError('Selezionare un metodo di fatturazione');
                    inputError = true;
                }   


                Set<Id> contactSet = new Set<Id>();
                contactSet.add(quoteObj.zqu__BillToContact__c);
                contactSet.add(quoteObj.zqu__SoldToContact__c);
                contactSet.add(quoteObj.Referenteintestatariodocumentocontabi__c);
                List<Contact> contactList = [SELECT Id, Name, ContactAddress__c, Email, Codice_SAP__c FROM Contact WHERE Id = :contactSet];
                for(Contact c : contactList){
                    if(c.ContactAddress__c==null){
                        appendErrorMessage('Per proseguire è necessario inserire un indirizzo per il referente "'+c.Name+'". <a href="/'+c.Id+'/e" target="_blank">Clicca qui</a> per modificare.');
                        return null;
                    }
                    if((quoteObj.zqu__BillingMethod__c == 'email' || quoteObj.zqu__BillingMethod__c == 'entrambi') && String.isBlank(c.Email)){
                        appendErrorMessage('Per proseguire è necessario inserire un indirizzo email per il referente "'+c.Name+'". <a href="/'+c.Id+'/e" target="_blank">Clicca qui</a> per modificare.');
                        return null;   
                    }
                    /* DH-1309 */
                    if(c.Codice_SAP__c != null && !c.Codice_SAP__c.isNumeric()){
                        appendErrorMessage('Il contatto "' + c.Name + '" con Codice SAP "' + c.Codice_SAP__c + '"" non è selezionabile in quanto il codice non è numerico');
                        return null;
                    }
                    /* DH-1309 */
                }

                if(quoteObj.Blocco_Rinnovo__c == null){
                    quoteObj.Blocco_Rinnovo__c.addError('Selezionare un valore per Blocco Rinnovo');
                    inputError = true;
                }
                if(quoteObj.Regalo__c == null){
                    quoteObj.Regalo__c.addError('Selezionare un valore per Regalo');
                    inputError = true;
                }
                if(inputError){
                    return null;
                }

                DomusUtil.setInvoiceAndActivationDate(quoteObj,quoteObj.Metodo_di_pagamento__c,quoteObj.zqu__PaymentTerm__c);

                System.debug(loggingLevel.Error, '*** quoteObj.Intermediario__c: ' + quoteObj.Intermediario__c);
                if(quoteObj.Fattura_a__c != 'Intermediario'){
                    quoteObj.zqu__PaymentMethod__c = paymentMethodsMap.get(quoteObj.Metodo_di_pagamento__c);
                }
                Boolean hasError = false;
                String billToEmail;
                if(quoteObj.Fattura_a__c == 'Intermediario' || quoteObj.Fattura_a__c == 'Azienda'){
                    
                    List<Zuora__CustomerAccount__c> customerAccountList = [SELECT Id, Bill_To_Salesforce__c, Bill_To_Salesforce__r.Email 
                                                                    FROM Zuora__CustomerAccount__c 
                                                                    WHERE Zuora__Zuora_Id__c = :billinAccountIntermediarioId];
                    if(customerAccountList != null && customerAccountList.size() > 0){
                        billToEmail = customerAccountList.get(0).Bill_To_Salesforce__r.Email;
                        quoteObj.Referenteintestatariodocumentocontabi__c = customerAccountList.get(0).Bill_To_Salesforce__c;
                    }
                }else{
                    Contact billTo = [Select Email,Codice_Fiscale__c,Account.Partita_IVA__c,Account.RecordType.DeveloperName 
                                    From Contact 
                                    Where Id =: quoteObj.zqu__BillToContact__c];
                    billToEmail = billTo.email;
                }
                if((quoteObj.zqu__BillingMethod__c == 'Email' || quoteObj.zqu__BillingMethod__c == 'Both')&& quoteObj.Fattura_a__c != 'Intermediario' ){
                    if(String.isBlank(billToEmail)){
                        quoteObj.zqu__BillingMethod__c.addError('Il referente di fatturazione non ha associato un indirizzo email');
                        hasError = true;
                    }
                }

                if(hasError){
                    return null;
                }
            }
            //Check emissione fattura
            System.debug(loggingLevel.Error, '*** quoteObj.Metodo_di_pagamento__c: ' + quoteObj.Metodo_di_pagamento__c);
            if((/*quoteObj.Metodo_di_pagamento__c == 'Carta di credito' || */quoteObj.Metodo_di_pagamento__c =='Paypal')
                && quoteObj.Emissione_Documento__c == 'NO'){
                appendErrorMessage('L\'emissione del documento è obbligatoria per '+quoteObj.Metodo_di_pagamento__c);
                return res;
            }

            if(quoteObj.Metodo_di_pagamento__c == 'Carta di credito' && quoteObj.Emissione_Documento__c == 'NO'){
                //appendWarningMessage('Attenzione, non si sta emettendo il documento per metodo di pagamento  '+quoteObj.Metodo_di_pagamento__c);
                ApexPages.Message warning = new ApexPages.Message(ApexPages.Severity.WARNING, 'Attenzione, non si sta emettendo il documento con metodo di pagamento Carta di Credito');
                ApexPages.addMessage(warning);
            }



            // intermediario
            System.debug(loggingLevel.Error, '*** quoteObj.Intermediario__c: ' + quoteObj.Intermediario__c);
            System.debug(loggingLevel.Error, '*** quoteObj.Fattura_a__c: ' + quoteObj.Fattura_a__c);
            System.debug(loggingLevel.Error, '*** billinAccountIntermediarioId: ' + billinAccountIntermediarioId);
            if((quoteObj.Intermediario__c != null && quoteObj.Fattura_a__c == 'Intermediario')||(quoteObj.Azienda__c !=null && quoteObj.Fattura_a__c == 'Azienda')){
                List<Zuora__CustomerAccount__c> intermediarioBillingAccountList = [SELECT Id,Name,Zuora__Zuora_Id__c 
                                                                            FROM Zuora__CustomerAccount__c 
                                                                            WHERE Zuora__Zuora_Id__c = :billinAccountIntermediarioId];
                System.debug(loggingLevel.Error, '*** intermediarioBillingAccountList: ' + intermediarioBillingAccountList);
                if(intermediarioBillingAccountList.size() > 0){
                    quoteObj.zqu__InvoiceOwnerId__c = intermediarioBillingAccountList.get(0).Zuora__Zuora_Id__c;
                    quoteObj.zqu__InvoiceOwnerName__c = intermediarioBillingAccountList.get(0).Name;
                    System.debug(loggingLevel.Error, '*** quoteObj.zqu__InvoiceOwnerName__c: ' + quoteObj.zqu__InvoiceOwnerName__c);
                }
            }
            Contact intDocCont = [SELECT FirstName, LastName, ContactAddress__c FROM Contact WHERE Id = :quoteObj.Referenteintestatariodocumentocontabi__c];
            //*** CR Rimozione Trattino ***//
            String contactFirstName = intDocCont.FirstName != null ? intDocCont.FirstName + ' ' : '';
            quoteObj.IndirizzoIntestatarioDocumentoCont__c = contactFirstName + intDocCont.LastName + ' ' + intDocCont.ContactAddress__c; 
            //*** CR Rimozione Trattino - Fine ***//
            res = Page.WizardSpese;
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return res;
    }

    public Contact billToContact {get;set;}
    public Contact shipToContact {get;set;}
    public Contact refFatturazioneToContact {get;set;}
    public Contact puntoDiScarico {get;set;}

    public void copyReferenteFatturazione() {
        //copio le informazioni da "referente fatturazioni" a "referente spedizione"
        if (billToContact != null) {
            shipToContact = billToContact;
            quoteObj.zqu__SoldToContact__c = quoteObj.zqu__BillToContact__c;
        }
        refreshContactData();
    
    }

    public void refreshContactData(){
        try{
            billToContact = null;
            shipToContact = null;
            refFatturazioneToContact = null;
            puntoDiScarico = null;
            System.debug('********refreshContactData zqu__BillToContact__c **********' + quoteObj.zqu__BillToContact__c);
            System.debug('********refreshContactData zqu__SoldToContact__c **********' + quoteObj.zqu__SoldToContact__c);
            System.debug('********refreshContactData Referenteintestatariodocumentocontabi__c **********' + quoteObj.Referenteintestatariodocumentocontabi__c);
            System.debug('********refreshContactData Puntodiscarico__c **********' + quoteObj.Puntodiscarico__c);
            if(String.isNotBlank(quoteObj.zqu__BillToContact__c)){
                List<Contact> conts = (List<Contact>) Database.query(Utils.getSelectAllQuery('Contact', 'Id = \'' + quoteObj.zqu__BillToContact__c + '\'', '', '', false));
                billToContact = conts.get(0);

            }
                
            if(String.isNotBlank(quoteObj.zqu__SoldToContact__c)){
                //questo codice non viene eseguito se l'utente ha modificato il referente fatturazioni
                List<Contact> conts = (List<Contact>) Database.query(Utils.getSelectAllQuery('Contact', 'Id = \'' + quoteObj.zqu__SoldToContact__c + '\'', '', '', false));
                shipToContact = conts.get(0);
            }
            if(String.isNotBlank(quoteObj.Referenteintestatariodocumentocontabi__c)){
                List<Contact> conts = (List<Contact>) Database.query(Utils.getSelectAllQuery('Contact', 'Id = \'' + quoteObj.Referenteintestatariodocumentocontabi__c + '\'', '', '', false));
                refFatturazioneToContact = conts.get(0);
            }
            if(String.isNotBlank(quoteObj.Puntodiscarico__c)){
                List<Contact> conts = (List<Contact>) Database.query(Utils.getSelectAllQuery('Contact', 'Id = \'' + quoteObj.Puntodiscarico__c + '\'', '', '', false));
                puntoDiScarico = conts.get(0);
            }
        } catch(Exception e) {
            
            appendErrorMessage(e);
            System.debug('************** quotebillingcontroller.refreshContactData *************' + e.getMessage() + '. ' + e.getStackTraceString());
        }
    }

    //*** DH-666 ***//
    public void aggiungiReferente(){
        try{
            if(selectedBillingAccountOption == 'new' && quoteObj.Subscription_Precedente__c != null){
                Zuora__Subscription__c subPrecedente = [SELECT Id, Contact_Sold_To__c FROM Zuora__Subscription__c WHERE Id = : quoteObj.Subscription_Precedente__c LIMIT 1];
                quoteObj.zqu__SoldToContact__c = subPrecedente.Contact_Sold_To__c;
                System.debug('*_* quoteObj.zqu__SoldToContact__c: ' + quoteObj.zqu__SoldToContact__c);
            }
        }catch(Exception e) {
            appendErrorMessage(e);
            System.debug('*_* quotebillingcontroller.aggiungiReferente *************' + e.getMessage() + '. ' + e.getStackTraceString());
        }
    }
    //*** DH-666 - Fine ***//

    public Boolean canShowContactsData{
        get{
            return billToContact != null || shipToContact != null;
        }
    }

    public void checkCartaDiCredito(){
        isCartaDiCredito = quoteObj.Metodo_di_pagamento__c == 'Carta di credito';
    }

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }

    private void appendWarningMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, messageError));
    }

    public void isWarning(){
        if(quoteObj.Metodo_di_pagamento__c == 'Carta di credito' && quoteObj.Emissione_Documento__c == 'NO'){
                //appendWarningMessage('Attenzione, non si sta emettendo il documento per metodo di pagamento  '+quoteObj.Metodo_di_pagamento__c);
                ApexPages.Message warning = new ApexPages.Message(ApexPages.Severity.WARNING, 'Attenzione, non si sta emettendo il documento con metodo di pagamento Carta di Credito');
                ApexPages.addMessage(warning);
            }

    }
}