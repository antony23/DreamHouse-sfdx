global with sharing class zgsGlobalREST_Utility { 
	private static Zuora.zApi zuoraApi; 
	private static String session; 
	private static String apiURL; 
	private static String MINOR_VERSION = 'v1/'; 
	private static String CHARGE_EXTENSION_NAME = 'Issue Based Prorate'; 
	public static Zuora.zApi zuoraApiAccess() { 
		Zuora.zApi zApi = new Zuora.zApi(); 
		Zuora.zApi.LoginResult loginResult = zApi.zlogin(); 
		session = loginResult.Session; 
		apiURL = loginResult.ServerURL; 
		if (apiURL.contains('apps')){ 
			Integer appsIndex = apiURL.indexOf('apps'); 
			apiURL = apiURL.substring(0,appsIndex); 
			apiURL += 'rest/' + MINOR_VERSION; 
		} 
		return zApi; 
	} 

	private static void checkZAPISet(){ 
		if (String.isEmpty(session) && !Test.isRunningTest()) { 
			zuoraApi = zuoraApiAccess(); 
		} 
	} 

	public static void setExtensionLink(String chargeId){ 
		String restURL = 'extension-links/product-charges/' + chargeId; 
		ExtensionData dataToSerialise = new ExtensionData(); 
		dataToSerialise.extensionKey = CHARGE_EXTENSION_NAME; 
		runRequest(JSON.serialize(dataToSerialise), 'POST', restURL); 
	} 

	@future (Callout=true)
	public static void runRequest(String payload, String method, String url){ 
		checkZAPISet(); 
		HttpRequest reqData = new HttpRequest(); 
		reqData.setHeader('Content-Type','application/json'); 
		reqData.setHeader('Connection','keep-alive'); 
		reqData.setHeader('Session',session); 
		reqData.setTimeout(20000); 
		System.debug(loggingLevel.Error, '*** apiURL: ' + apiURL);
		apiURL = 'https://apisandbox-api.zuora.com/rest/v1/';
		reqData.setEndpoint(apiURL + url); 
		reqData.setBody(payload); 
		reqData.setMethod(method); 
		System.debug(loggingLevel.Error, '*** session: ' + session);
		System.debug(loggingLevel.Error, '*** reqData: ' + reqData);
		Http http = new Http(); 
		HTTPResponse res = http.send(reqData); 
		System.debug(loggingLevel.Error, '*** res: ' + res);
	} 

	public class ExtensionData { 
		public String extensionKey {get;set;} 
	}
}