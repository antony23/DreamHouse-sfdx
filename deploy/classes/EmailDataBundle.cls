public class EmailDataBundle { // Oggetto rappresentante i dati necessari per inviare una singola email con template
	SObject what;
	SObject recipient;
	EmailTemplate template;
	String recipientClassName;
	String whatClassName;

	public EmailDataBundle(SObject what, User recipient, EmailTemplate template) {
		this(what, (SObject) recipient, template);
	}

	public EmailDataBundle(SObject what, Contact recipient, EmailTemplate template) {
		this(what, (SObject) recipient, template);
	}

	public EmailDataBundle(SObject what, Lead recipient, EmailTemplate template) {
		this(what, (SObject) recipient, template);
	}

	private EmailDataBundle(SObject what, SObject recipient, EmailTemplate template) {
		this.what = what;
		this.recipient = recipient;
		this.template = template;
		
		try {
			this.recipientClassName = (recipient != null) ? recipient.getSObjectType().getDescribe().getName() : 'SObject';
			this.whatClassName = (what != null) ? what.getSObjectType().getDescribe().getName() : 'SObject';
		} catch(Exception e) {
			this.recipientClassName = 'SObject';
			this.whatClassName = 'SObject';
		}
	}

	public SObject getRelatedTo() {
		return what;
	}

	public SObject getRecipient() {
		return recipient;
	}

	public EmailTemplate getTemplate() {
		return template;
	}

	override public String toString() {
		return 'EmailDataBundle[' + hashCode() + ']:{ ' +
			 	((recipient != null) ? 'recipient = ' + recipientClassName + '[' + recipient.Id + '], ' : '') +
				((what != null) ? 'relatedTo = ' + whatClassName + '[' + what.Id + '], ' : '') +
				'template = ' + template +
		' }';
	}
}