@isTest
public class EmailSendingUtilTest {

	@isTest
	public static void testLoadTemplates() {
		final Map<String, EmailTemplate> testTemplates = setupTemplates(5);

		Test.startTest();

		final Map<String, EmailTemplate> templates = EmailSendingUtil.loadTemplates(new List<String>(testTemplates.keySet()));

		for(String name : testTemplates.keySet()) {
			System.assert(templates.containsKey(name));
		}

		Test.stopTest();
	}

	@isTest
	public static void testGetOrgWideEmailAddressByDisplayName() {
		final OrgWideEmailAddress[] orgWideEmailAddresses = [SELECT Id, DisplayName FROM OrgWideEmailAddress];

		Test.startTest();

		for(OrgWideEmailAddress testOWEA : orgWideEmailAddresses) {
			OrgWideEmailAddress owea = EmailSendingUtil.getOrgWideEmailAddressByDisplayName(testOWEA.DisplayName);

			System.assertEquals(owea.Id, testOWEA.Id);
		}

		Test.stopTest();
	}

	@isTest(SeeAllData = True)
	public static void testPrepareEmail() {
		final OrgWideEmailAddress sender = EmailSendingUtil.getOrgWideEmailAddressByDisplayName('Editoriale Domus - No reply');
		final String[] to = new String[]{'toaddress@email.com'};
		final String[] cc = new String[]{'ccaddress@email.com'};
		final String[] bcc = new String[]{'bccaddress@email.com'};
		final String subject = 'Subject';
		final String body = 'Test message.';

		Test.startTest();

		Messaging.SingleEmailMessage email = EmailSendingUtil.prepareEmail(sender, to, cc, bcc, subject, body);
		System.assertEquals(email.getOrgWideEmailAddressId(), sender.Id);
		System.assertEquals(email.getToAddresses()[0], to[0]);
		System.assertEquals(email.getCcAddresses()[0], cc[0]);
		System.assertEquals(email.getBccAddresses()[0], bcc[0]);
		System.assertEquals(email.getSubject(), subject);
		System.assertEquals(email.getPlainTextBody(), body);

		email = EmailSendingUtil.prepareEmail(sender, to[0], cc[0], bcc[0], subject, body);
		System.assertEquals(email.getOrgWideEmailAddressId(), sender.Id);
		System.assertEquals(email.getToAddresses()[0], to[0]);
		System.assertEquals(email.getCcAddresses()[0], cc[0]);
		System.assertEquals(email.getBccAddresses()[0], bcc[0]);
		System.assertEquals(email.getSubject(), subject);
		System.assertEquals(email.getPlainTextBody(), body);

		email = EmailSendingUtil.prepareEmail(sender, to[0], subject, body);
		System.assertEquals(email.getOrgWideEmailAddressId(), sender.Id);
		System.assertEquals(email.getToAddresses()[0], to[0]);
		System.assertEquals(email.getCcAddresses(), null);
		System.assertEquals(email.getBccAddresses(), null);
		System.assertEquals(email.getSubject(), subject);
		System.assertEquals(email.getPlainTextBody(), body);

		email = EmailSendingUtil.prepareEmail(to[0], subject, body);
		System.assertEquals(email.getOrgWideEmailAddressId(), null);
		System.assertEquals(email.getToAddresses()[0], to[0]);
		System.assertEquals(email.getCcAddresses(), null);
		System.assertEquals(email.getBccAddresses(), null);
		System.assertEquals(email.getSubject(), subject);
		System.assertEquals(email.getPlainTextBody(), body);

		Test.stopTest();
	}

	@isTest(SeeAllData = True)
	public static void testPrepareEmails() {
		final Account relatedTo = new Account(Name = 'TestAccount');
		insert relatedTo;

		final Contact recipient = new Contact(FirstName = 'FirstNameTest', LastName = 'LastNameTest', AccountId = relatedTo.Id, Email = 'test@email.com');
		insert recipient;

		final OrgWideEmailAddress fromAddress = EmailSendingUtil.getOrgWideEmailAddressByDisplayName('Editoriale Domus - No reply');

		final EmailTemplate template = [SELECT Id, Name, DeveloperName FROM EmailTemplate LIMIT 1];

		Test.startTest();

		EmailDataBundle edb = new EmailDataBundle(relatedTo, recipient, template);
		Messaging.SingleEmailMessage email = EmailSendingUtil.prepareEmails(new EmailDataBundle[]{edb}, fromAddress)[0];

		System.assertEquals(email.getToAddresses()[0], recipient.Id);
		System.assertEquals(email.getTargetObjectId(), recipient.Id);
		System.assertEquals(email.getWhatId(), relatedTo.Id);
		System.assertEquals(email.getTemplateId(), template.Id);
		System.assertEquals(email.getOrgWideEmailAddressId(), fromAddress.Id);

		List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
		EmailSendingUtil.debug(results, new EmailDataBundle[]{edb});

		Test.stopTest();
	}

	private static Map<String, EmailTemplate> setupTemplates(Integer n) {
		Map<String, EmailTemplate> templates = new Map<String, EmailTemplate>();

		Organization folder = [SELECT Id FROM Organization WHERE Name = 'Editoriale Domus Spa' LIMIT 1];
		
		for(Integer i = 1; i <= n; i++) {
			templates.put('Template_' + i, new EmailTemplate(Name = 'Template_' + i, DeveloperName='Template_' + i, Body='', Description='', TemplateType = 'custom', FolderId = folder.Id));
		}

		insert templates.values();
		return templates;
	}

}