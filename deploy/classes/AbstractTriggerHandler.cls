public virtual class AbstractTriggerHandler {
	private final Map<System.TriggerOperation, Integer> recordsOnBefore;
	private final String sObjectName;
	private Boolean disabled;

	protected  AbstractTriggerHandler(String sObjectName) {
		this.disabled = false;
		this.sObjectName = sObjectName;
		this.recordsOnBefore = new Map<System.TriggerOperation, Integer>();
	}

	public void handle() {
		if(disabled) return;
		if(!Trigger.isExecuting) throw new OutOfTriggerExecutionException('Method run() can be called only inside Trigger execution.');
		System.debug('[TriggerHandler][' + sObjectName + '].OperationType: ' + Trigger.operationType);

		switch on Trigger.operationType {
			when BEFORE_INSERT	{ 
				onBeforeInsert();
				recordsOnBefore.put(TriggerOperation.BEFORE_INSERT, Trigger.new.size());
			}
			when BEFORE_UPDATE	{ 
				onBeforeUpdate();
				recordsOnBefore.put(TriggerOperation.BEFORE_UPDATE, Trigger.new.size());
			}
			when BEFORE_DELETE	{ 
				onBeforeDelete();
				recordsOnBefore.put(TriggerOperation.BEFORE_DELETE, Trigger.old.size());
			}
			when AFTER_INSERT	{ 
				onAfterInsert();
				recordsOnBefore.put(TriggerOperation.BEFORE_INSERT, 0);
			}
			when AFTER_UPDATE	{ 
				onAfterUpdate();
				recordsOnBefore.put(TriggerOperation.BEFORE_UPDATE, 0);
			}
			when AFTER_DELETE 	{ 
				onAfterDelete();
				recordsOnBefore.put(TriggerOperation.BEFORE_DELETE, 0);
			}
			when AFTER_UNDELETE	{ onAfterUndelete(); }
		}
	}

	/**
	 *	Calculates the difference between the number of records passed to onBefore method and the number of records passed to onAfter method.
	 *	If the difference is major than 0, then some records have been descarded due to errors.
	**/

	public Integer beforeAfterLoss() {
		switch on Trigger.operationType {
			when AFTER_INSERT	{ return recordsOnBefore.get(TriggerOperation.BEFORE_INSERT) - Trigger.new.size(); }
			when AFTER_UPDATE	{ return recordsOnBefore.get(TriggerOperation.BEFORE_UPDATE) - Trigger.new.size(); }
			when AFTER_DELETE	{ return recordsOnBefore.get(TriggerOperation.BEFORE_DELETE) - Trigger.old.size(); }
			when else { return 0; }
		}
	}

	/**
	 *	Enables the Trigger Handler.
	**/

	public void enable() {
		disabled = false;

		System.debug('[TriggerHandler][' + sObjectName + '] enabled.');
	}

	/**
	 *	Disables the Trigger Handler.
	**/

	public void disable() {
		disabled = true;

		System.debug('[TriggerHandler][' + sObjectName + '] disabled.');
	}

	public virtual void onBeforeInsert(){}

	public virtual void onAfterInsert(){}

	public virtual void onBeforeUpdate(){}

	public virtual void onAfterUpdate(){}

	public virtual void onBeforeDelete(){}

	public virtual void onAfterDelete(){}

	public virtual void onAfterUndelete(){}

	public class OutOfTriggerExecutionException extends TriggerHandlerException {}

	public virtual class TriggerHandlerException extends Exception {}
}