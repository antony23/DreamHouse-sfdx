global class SubscriptionRenewalBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.StateFul {
    String query;
    Set<Id> subscriptionsId;
    Boolean createAllAvvisi;
    Boolean tempozero;
    Map<Id,List<Regola_Avviso_Rinnovo__c>> regoleDiRinnovoByRatePlanMap = null;
    Set<String> timingSollecito = null;
    Map<String,Testata__c> testate = null;
    
    global SubscriptionRenewalBatch() {
        try {
            createAllAvvisi = true;
            query = 'SELECT Id FROM Zuora__Subscription__c WHERE Rinnovabile__c = true ';
        } catch(Exception e) {
			System.debug('Errore.\r\nMessage:' + e.getMessage()+'\r\nStack:' + e.getStackTraceString());
		}
    }

    global SubscriptionRenewalBatch(Set<Id> subsId, Boolean complete) {
        if(complete) {
            // Genera avvisi di tipo rinnovo            
            subscriptionsId = subsId;
            createAllAvvisi = true;
            tempozero = false;

            query = 'SELECT Id FROM Zuora__Subscription__c WHERE Rinnovabile__c = true ' + 
                            ((subscriptionsId != null && subscriptionsId.size() > 0) ? 'AND Id IN :subscriptionsId' : '');
        } else {
            // Genera avvisi di tipo sollecito
            this(subsId);
        }
    }

    global SubscriptionRenewalBatch(Set<Id> subsId) {
        try {
            subscriptionsId = subsId;
            String whereClause = 'Id IN :subscriptionsId AND Zuora__Status__c = \'Pending Activation\'';
            List<String> subscriptionFields = new List<String>{ 
                                                                'Business_Unit__c', 'Campagna__c', 'Campagna__r.Omaggio__c', 'ConAvvisi__c', 'Contatto_Principale_Committente__c', 
                                                                'Id', 'Intermediario__c', 'Italia_Estero_Spedizione__c', 'Mantieni_Offerta__c', 
                                                                'Mesi_alla_scadenza__c', 'Mesi_dalla_sottoscrizione__c', 'Metodo_di_pagamento__c', 'Name', 'Nazione_Spedizione__c', 
                                                                'Numero_di_Cicli__c', 'Rate_Plan_Abbonamento__c', 'Regalo__c', 'Stato_Sfdc__c', 'Supporto__c', 
                                                                'Tipo_Spese_Spedizione_Abbonamento__c', 'Tipo_Consegna_Abbonamento__c', 'Tipo_subscription__c', 'Zuora__Account__c', 'Zuora__CustomerAccount__c', 
                                                                'Zuora__CustomerAccount__r.Bill_To_Salesforce__c', 'Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Codice_Nazione__c', 
                                                                'Zuora__CustomerAccount__r.Sold_To_Salesforce__c', 'Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Codice_Nazione__c', 
                                                                'Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Name', 'Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Name', 
                                                                'Zuora__CustomerAccount__r.Metodo_di_pagamento__c', 'Zuora__Status__c', 'Zuora__SubscriptionEndDate__c', 
                                                                'Zuora__SubscriptionStartDate__c', 'Zuora__Zuora_Id__c', 'Label_Run_IsError__c', 'Label_Run_processed__c', 'Label_Run_error_message__c'
                                                            };

            query = 'SELECT ' + String.join(subscriptionFields, ',') + ' FROM Zuora__Subscription__c ' + ' WHERE ' + whereClause;

            createAllAvvisi = false;
            tempozero = true;
        } catch(Exception e) {
            System.debug('Errore.\r\nMessage:' + e.getMessage()+'\r\nStack:' + e.getStackTraceString());
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try {

            regoleDiRinnovoByRatePlanMap = SubscriptionRenewalUtil.getRegoleDiRinnovoByRatePlanMap();
            testate = DomusUtil.getMapTestateByName();
            timingSollecito = SubscriptionRenewalUtil.getTimingSollecito();

            if(scope.size() > 0){
                String whereClause = 'Rinnovabile__c = true';
                List<String> subscriptionFields = new List<String>{ 
                                                                'Business_Unit__c', 'Campagna__c', 'Campagna__r.Omaggio__c', 'ConAvvisi__c', 'Contatto_Principale_Committente__c', 
                                                                'Id', 'Intermediario__c', 'Italia_Estero_Spedizione__c', 'Mantieni_Offerta__c', 
                                                                'Mesi_alla_scadenza__c', 'Mesi_dalla_sottoscrizione__c', 'Metodo_di_pagamento__c', 'Name', 'Nazione_Spedizione__c', 
                                                                'Numero_di_Cicli__c', 'Rate_Plan_Abbonamento__c', 'Regalo__c', 'Stato_Sfdc__c', 'Supporto__c', 
                                                                'Tipo_Spese_Spedizione_Abbonamento__c', 'Tipo_Consegna_Abbonamento__c', 'Tipo_subscription__c', 'Zuora__Account__c', 'Zuora__CustomerAccount__c', 
                                                                'Zuora__CustomerAccount__r.Bill_To_Salesforce__c', 'Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Codice_Nazione__c', 
                                                                'Zuora__CustomerAccount__r.Sold_To_Salesforce__c', 'Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Codice_Nazione__c', 
                                                                'Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Name', 'Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Name', 
                                                                'Zuora__CustomerAccount__r.Metodo_di_pagamento__c', 'Zuora__Status__c', 'Zuora__SubscriptionEndDate__c', 
                                                                'Zuora__SubscriptionStartDate__c', 'Zuora__Zuora_Id__c', 'Label_Run_IsError__c', 'Label_Run_processed__c', 'Label_Run_error_message__c'
                                                            };

                List<String> avvisoFields = new List<String>{ 'Mesi_alla_scadenza__c', 'Mesi_dalla_sottoscrizione__c' };

                Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>(
                    (List<Zuora__Subscription__c>)
                    Database.query('SELECT ' + String.join(subscriptionFields, ',') + 
                                            ',( SELECT ' + String.join(avvisoFields, ',') + ' FROM Avvisi_di_Rinnovo__r WHERE Stato_Avviso__c != \'In Errore\' ORDER By CreatedDate DESC LIMIT 1) ' +
                                            ',( SELECT Id FROM SubscriptionSuccessive__r ) ' +
                                    ' FROM Zuora__Subscription__c ' + 
                                    ' WHERE Id IN :scope ')
                  );

                //Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>((List<Zuora__Subscription__c>) scope);
                Map<Zuora__Subscription__c,List<Regola_Avviso_Rinnovo__c>> subscriptionsForAvvisi = new Map<Zuora__Subscription__c,List<Regola_Avviso_Rinnovo__c>>();
                if(createAllAvvisi){
                    subscriptionsForAvvisi = SubscriptionRenewalUtil.getSubscriptionsDaRinnovare( 
                        subscriptionsMap.values(),
                        regoleDiRinnovoByRatePlanMap,
                        timingSollecito
                    );
                } else {
                    for(Zuora__Subscription__c subscription : subscriptionsMap.values()){
                        subscriptionsForAvvisi.put(subscription,null);
                    }
                }

                System.debug('Subscriptions da rinnovare: ' + subscriptionsForAvvisi.keySet());

                if(subscriptionsForAvvisi.size() > 0){
                    List<Avviso_di_Rinnovo__c> avvisiDiRinnovo = SubscriptionRenewalUtil.createAvvisiDiRinnovo(
                        subscriptionsForAvvisi,
                        testate,
                        tempozero);
                    DomusUtil.labelRunUpsertSObject(avvisiDiRinnovo, subscriptionsMap);
                }
            }
        } catch(Exception e) {
            System.debug('Errore.\r\nMessage:' + e.getMessage()+'\r\nStack:' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}