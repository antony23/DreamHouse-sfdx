@isTest(SeeAllData = true)
public class TestSubscriptionMailProcessAction {
    private static Id zuoraProduct = [SELECT Id FROM Zuora__Product__c WHERE Name = 'VENDITE DIRETTE' LIMIT 1].Id;
    private static Id abbQVA = [SELECT Id FROM Product2 WHERE Name = 'Abbonamento Valore Assicurato' LIMIT 1].Id;
    private static Id abbQuattroruote = [SELECT Id FROM Product2 WHERE Name = 'Abbonamento Quattroruote' LIMIT 1].Id;

    private static Blocco_Selettivo_Notifiche__c bsn;
	private static Zuora__Subscription__c subscription;
    private static Zuora__SubscriptionRatePlan__c subscriptionRatePlan;
	private static Contact contact;
	private static Account account;

    @isTest
    public static void testSendEmail() {
        setup(true, 'Abbonamento', 'Carta', 'CBill', true, false, false, false, false);

        Test.startTest();

        SubscriptionMailProcessAction.sendEmail(new Zuora__Subscription__c[]{subscription});

        Test.stopTest();
    }

    @isTest
    static void testMT1() {
        setup(true, 'Abbonamento', 'Carta', 'Bonifico / SEPA', true, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT1');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT2() {
        setup(true, 'Abbonamento', 'Carta', 'CBill', false, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT2');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT3() {
        setup(true, 'Abbonamento', 'Carta', 'CBill', true, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT3');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT4_Abbonamento() {
        setup(true, 'Abbonamento', 'Carta + Digitale', 'Bonifico / SEPA', false, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT4');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT5_Intermediario() {
        setup(true, 'Abbonamento', 'Carta', 'Bonifico / SEPA', true, false, true, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT5');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT5_Azienda() {
        setup(true, 'Abbonamento', 'Carta', 'Bonifico / SEPA', true, false, false, true, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT5');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT6() {
        setup(true, 'Vendita Diretta', 'Carta', 'Bonifico / SEPA', true, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT6');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT4_Vendita_Diretta() {
        setup(true, 'Vendita Diretta', 'Carta + Digitale', 'Bonifico / SEPA', false, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        
        System.assert(data.isEmpty());

        Test.stopTest();
    }

    @isTest
    static void testMT8() {
        setup(true, 'Vendita Diretta', 'Carta', 'Contrassegno', true, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT8');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT9() {
        setup(true, 'Bundle misti', 'Carta', 'Bonifico / SEPA', true, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT9');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    @isTest
    static void testMT4_Bundle_misti() {
        setup(true, 'Bundle misti', 'Carta + Digitale', 'Bonifico / SEPA', false, false, false, false, false);

        Test.startTest();

        final List<EmailDataBundle> data = SubscriptionMailProcessAction.fetchEmailData(new Zuora__Subscription__c[]{subscription}, bsn);
        final EmailDataBundle edb = data[0];

        System.assertEquals(edb.getRecipient().Id, contact.Id);
        System.assertEquals(edb.getTemplate().DeveloperName, 'MT4');

        Zuora__Subscription__c relatedTo = (Zuora__Subscription__c) edb.getRelatedTo();

        System.assertEquals(subscription.Id, relatedTo.Id);

        Test.stopTest();
    }

    static void setup(Boolean b2c, String subType, String subFormat, String payMethod, Boolean status, 
                        Boolean suspended, Boolean intermediario, Boolean azienda, Boolean professional){
        bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
		bsn.MT1_Abilitato__c = true;
        bsn.MT2_Abilitato__c = true;
        bsn.MT3_Abilitato__c = true;
        bsn.MT4_Abilitato__c = true;
        bsn.MT5_Abilitato__c = true;
        bsn.MT6_Abilitato__c = true;
        bsn.MT8_Abilitato__c = true;
        bsn.MT9_Abilitato__c = true;
		upsert bsn;

        RecordType rtB2C = [SELECT Id FROM RecordType WHERE DeveloperName = 'B2C' LIMIT 1];
        RecordType rtB2BSmall = [SELECT Id FROM RecordType WHERE DeveloperName = 'B2BSmall' LIMIT 1];

        account = new Account(Name = 'TestAccount', Notifiche_Email_Disabilitate__c = false, RecordTypeId = (b2c) ? rtB2C.Id : rtB2BSmall.Id);
		AccountTriggerHandler.skip = true;
        insert account;

        contact = new Contact(FirstName = 'FirstNameTest', LastName = 'LastNameTest', AccountId = account.Id, Email = 'test@email.com');
        ContactTriggerHandler.skip = true;
        insert contact;

        Account thirdParty = new Account(Name = 'TestThirdParty', Notifiche_Email_Disabilitate__c = true, RecordTypeId = rtB2BSmall.Id);
        AccountTriggerHandler.skip = true;
        insert thirdParty;

        Contact thirdPartyContact = new Contact(FirstName = 'FirstNameTest', LastName = 'LastNameTest', AccountId = account.Id);
        ContactTriggerHandler.skip = true;
        insert thirdPartyContact;

        Zuora__CustomerAccount__c firstCA = ZTest_Utils.createBillingAccount(contact.Id, contact.Id, account.Id);
        insert firstCA;

        Zuora__CustomerAccount__c secondCA = ZTest_Utils.createBillingAccount(thirdPartyContact.Id, thirdPartyContact.Id, thirdParty.Id);
        insert secondCA;

		subscription = new Zuora__Subscription__c(Name = 'A-S00000000', Zuora__Account__c = account.Id, Contact_Bill_To__c = contact.Id, Zuora__OriginalId__c = '12345678901234567890123456789011');
        subscription.Notifica_Scontrino__c = false;
        subscription.Notifica_Decorrenza__c = false;
        subscription.Notifica_Pagamento__c = false;
        subscription.Notifica_Istruzioni_Digital__c = false;
        subscription.Zuora__Zuora_Id__c = (suspended) ? '12345678901234567890123456789012' : subscription.Zuora__OriginalId__c;
        subscription.Zuora__CustomerAccount__c = firstCA.Id;
        subscription.Zuora__InvoiceOwner__c = (intermediario || azienda) ? secondCA.Id : firstCA.Id;
        subscription.Intermediario__c = (intermediario) ? thirdParty.Id : null;
        subscription.Azienda__c = (azienda) ? thirdParty.Id : null;
        subscription.Contact_Bill_To__c = contact.Id;
        subscription.Contact_Sold_To__c = contact.Id;
        subscription.Tipo_Subscription__c = subType;
        subscription.Supporto__c = subFormat;
        subscription.Zuora__Status__c = (status) ? 'Active' : 'Pending Activation';
        subscription.Metodo_di_Pagamento__c = payMethod;

        SubscriptionTriggerHandler.skipTrigger = true;
        insert subscription;

        account = (Account) reload('Account', account.Id);
        thirdParty = (Account) reload('Account', thirdParty.Id);
        contact = (Contact) reload('Contact', contact.Id);
        subscription = (Zuora__Subscription__c) reload('Zuora__Subscription__c', subscription.Id, new String[]{'Zuora__Account__r.Notifiche_Email_Disabilitate__c', 'Campagna__r.Omaggio__c', 'Contact_Bill_To__r.Email', 'Rate_Plan_Abbonamento__r.Name'});
        subscriptionRatePlan = (professional) 
                                    ? createSubscriptionRatePlan(subscription, 'Professional', abbQVA)
                                    : createSubscriptionRatePlan(subscription, 'Diffusione', abbQuattroruote);
    }

    private static Zuora__SubscriptionRatePlan__c createSubscriptionRatePlan(Zuora__Subscription__c sub, String prodType, Id prodId) {
        zqu__ProductRatePlan__c prp = ZTest_Utils.createzquProductRatePlan(prodId, '12345678901234567890123456789013', prodType);
        insert prp;

        zqu__ProductRatePlanCharge__c prpc = ZTest_Utils.createzquProductRatePlanCharge(prp.Id, prodType);
        prpc.Entitlment__c = 'Virtualcomm';
        insert prpc;

        Zuora__SubscriptionRatePlan__c srp = ZTest_Utils.createZuoraSubscriptionRatePlan(prp, sub.Id);
        srp.Name = [SELECT Name FROM Product2 WHERE Id = :prodId].Name;

        SubscriptionRatePlanTriggerHandler.skipTrigger = true;
        insert srp;

        Zuora__SubscriptionProductCharge__c spc = new Zuora__SubscriptionProductCharge__c (Name = 'SubscriptionProductCharge', Product_Rate_Plan_Charge__c = prpc.Id, Zuora__Account__c = sub.Zuora__Account__c,   Zuora__Description__c = 'SubscriptionProductCharge', Zuora__ProductSKU__c = 'BDAB000001', Zuora__Product__c = zuoraProduct, Zuora__Quantity__c = 1.000, Supporto__c = 'Digitale', Zuora__SubscriptionRatePlan__c = srp.Id, Zuora__Subscription__c = sub.Id);
        SubscriptionProductChargeTriggerHandler.skipTrigger = true;
        insert spc;

        return (Zuora__SubscriptionRatePlan__c) reload('Zuora__SubscriptionRatePlan__c', srp.Id);
    }

    private static SObject reload(String objName, Id objId) {
        return reload(objName, objId, new String[]{});
    }

    private static SObject reload(String objName, Id objId, String[] plus) {
        final String query = 'SELECT ' + String.join(fieldsOf(objName, plus), ',') + ' FROM ' + objName + ' WHERE Id = \'' + objId + '\'';
        final SObject sobj = Database.query(query);
        //sobj.recalculateFormulas();

        return sobj;
    }

    private static List<string> fieldsOf(String className, String[] plus) {
        final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        fields.addAll(plus);

        return fields;
    }
}