global class WS_CreateContacts {

    global class mainInput{
        webservice List<Contact> contacts;
    }

    global class mainOutput{
        webservice List<OutputObj> result;
    }

    global class OutputObj{
        webservice Id accountId;
        webservice Id contactId;
        webservice Id contactIdDuplicato;
        webservice Contact contact;
        webservice String errorMessage;
    }

    webservice static WS_CreateContacts.mainOutput insertContactMasterData(WS_CreateContacts.mainInput cmdInput){

        WS_CreateContacts.mainOutput mainOutput = new WS_CreateContacts.mainOutput();
        List<OutputObj> contactResponseList = new List<OutputObj>();

        Savepoint sp = null;
        if(!Test.isRunningTest()) sp = Database.setSavepoint();

        try {
            if(!cmdInput.contacts.isEmpty()) {
                List<Id> contattiDuplicatiIds = new List<Id>(); // usato per recuperare gli accounts, da restituire nella response
                List<Id> contattiInseritiIds = new List<Id>(); // usato per agganciare contatti appena inseriti con account appena inseriti
                List<Account> accountsToCreate = new List<Account>(); // accounts da creare contestualmente ai contatti
                System.debug(loggingLevel.Error, '*** cmdInput.contacts: ' + cmdInput.contacts);
                List<Database.SaveResult> saveResults  = Database.insert(cmdInput.contacts, false);
                System.debug(loggingLevel.Error, '*** saveResults: ' + saveResults);
                for(Integer i=0; i<saveResults.size(); i++){
                    Database.SaveResult saveResult = saveResults.get(i);
                    System.debug(loggingLevel.Error, '*** saveResult: ' + saveResult);

                    OutputObj contactResponse = new OutputObj();
                    contactResponse.contact = cmdInput.contacts.get(i);
                    System.debug('***********CONTACT*********' + contactResponse.contact);      
                    System.debug('***********CODICE CLIENTE*********' + contactResponse.contact.CodiceCliente__c );
                    if(saveResult.isSuccess()){
                        contactResponse.contactId = saveResult.getId();
                        contattiInseritiIds.add(contactResponse.contactId);
                        accountsToCreate.add(getAccountFromContact(contactResponse.contact));
                       
                    }else{
                        for (Database.Error error : saveResult.getErrors()) {
                            if (error instanceof Database.DuplicateError) {
                                Database.DuplicateError duplicateError = (Database.DuplicateError) error;
                                Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                                
                                contactResponse.errorMessage = duplicateResult.getErrorMessage();
                                
                                List<Datacloud.MatchResult> matchResults = duplicateResult.getMatchResults();
                                if(!matchResults.isEmpty()){
                                    Datacloud.MatchResult matchResult = matchResults.get(0);
                                    List<Datacloud.MatchRecord> matchRecords = matchResult.getMatchRecords();
                                    if(!matchRecords.isEmpty()){
                                        Datacloud.MatchRecord matchRecord = matchRecords.get(0);
                                        sObject contattoDuplicato = matchRecord.getRecord();
                                        contactResponse.contactIdDuplicato = (Id) contattoDuplicato.get('Id');
                                        contattiDuplicatiIds.add(contactResponse.contactIdDuplicato);
                                    }
                                }
                            }else{
                                contactResponse.errorMessage = error.getMessage();
                            }
                        }
                    }
                    contactResponseList.add(contactResponse);
                }

                Map<Id,Contact> contattiDuplicati = new Map<Id,Contact>([SELECT Id,AccountId FROM Contact WHERE Id IN :contattiDuplicatiIds AND AccountId != null]);

                for(OutputObj contactResponseElement : contactResponseList){
                    if(String.isNotBlank(contactResponseElement.contactIdDuplicato)){
                    	Contact c = contattiDuplicati.get(contactResponseElement.contactIdDuplicato);
                        contactResponseElement.accountId = c.AccountId;
                    }
                }

                AccountTriggerHandler.skip = true;

                Map<Id,Object> insertAccountResults = new Map<Id,Object>();
                List<Database.SaveResult> saveResultsAccount = Database.insert(accountsToCreate,false);
                for(Integer i=0; i<saveResultsAccount.size(); i++){
                    Database.SaveResult saveResult = saveResultsAccount.get(i);
                    insertAccountResults.put(contattiInseritiIds.get(i), saveResult.isSuccess() ? saveResult.getId() : saveResult.getErrors().get(0).getMessage());
                }

                List<Contact> contactsToJoinAccount = new List<Contact>();
                for(OutputObj contactResponse : contactResponseList){
                    if(insertAccountResults.containsKey(contactResponse.contactId)){
                        Object result = insertAccountResults.get(contactResponse.contactId);
                        if(result instanceof Id){
                            contactResponse.accountId = (Id) result;
                            contactsToJoinAccount.add(new Contact( Id = contactResponse.contactId, AccountId = contactResponse.accountId ));
                        }else{
                          contactResponse.errorMessage = (String) result;
                        }    
                    }
                }
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                ContactTriggerHandler.skip = true;
                Database.update(contactsToJoinAccount,dml);

            }            
        } catch(Exception e) {

        	if(!Test.isRunningTest()) Database.rollback(sp);
        	for(OutputObj contactResponse : contactResponseList){
        		contactResponse.errorMessage = e.getMessage() + ' ' + e.getStackTraceString();
        	}
        }

        mainOutput.result = contactResponseList;

        return mainOutput;
    }

    private static Account getAccountFromContact(Contact c){        
        
        return new Account(
            Name = c.LastName + ' ' + c.FirstName,
            Stato__c = 'Prospect',
            Professione__c = c.Professione__c,
            Id_Facebook__c = c.Id_Facebook__c,
            PersonMobilePhone = c.MobilePhone,
            Web_ID__c = c.Id_Web__c,
            BillingPostalCode = c.MailingPostalCode,
            Codice_Fiscale__c = c.Codice_Fiscale__c,
            Data_Conferma_Optin__c = c.Data_Conferma_Optin__c,
            Data_di_Cancellazione__c = c.Data_di_Cancellazione__c,
            Data_di_nascita__c = c.Birthdate,
            Data_di_Ultima_Modifica__c = c.Data_di_Ultima_Modifica__c,
            Data_Registrazione__c = c.Data_Registrazione__c,
            Lingua__c = c.Lingua__c,
            Luogo_di_nascita__c = c.Luogo_di_nascita__c,
            Sesso__c = c.Sesso__c,
            Phone = c.Phone,
            User_Name__c = c.username__c,
            Email__c = c.Email,
            Titolo__c = c.Titolo__c,
            Codice_Nazione__c = c.Codice_Nazione__c,
            Codice_ACI__c = c.Codice_ACI__c,
            Codice_Press_Di__c = c.Codice_Press_Di__c,
            Codice_Cliente__c = c.CodiceCliente__c,
            Cessione_a_Terzi__c = c.Cessione_a_Terzi__c,
            Comunicazioni_Commerciali_Marketing__c = c.Comunicazioni_Commerciali_Marketing__c,
            RecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId()
        );
    }

}