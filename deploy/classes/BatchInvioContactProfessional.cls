global class BatchInvioContactProfessional implements Database.Batchable<sObject> {
	
	String queryString;
	
	global BatchInvioContactProfessional(String query) {
		this.queryString = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		try{
        	List<Contact> listContact = (List<Contact>)scope;
            for(Contact c : listContact){
				c.Invia_anagrafica_a_sap__c = true;
            }
            ContactTriggerHandler.skip = true;
            update scope;
            ContactTriggerHandler.skip = false;
        }catch(Exception ex){   
        	System.debug(ex.getMessage()+'\n'+ex.getStackTraceString());
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}