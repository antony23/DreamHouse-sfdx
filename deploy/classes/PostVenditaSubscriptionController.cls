public class PostVenditaSubscriptionController {

    public Zuora__Subscription__c subscription {get;set;}
    public List<AssetWrapper> prodotti {get;set;}
    public Boolean selectedAtLeastOne {get;set;}
    public Boolean resoDigitale {get;set;}
    public Integer prodottiDisponibiliPerReso {
        get{
            Integer res = 0;
            for(AssetWrapper aw :prodotti){
                res += Integer.valueOf(aw.prodotto.Quantita__c);
            }
            return res;
        }
    }
    public Boolean isSelect {get; set;}
    
    public PostVenditaSubscriptionController() {
        try {
            Id subscriptionId = Id.valueOf(ApexPages.currentPage().getParameters().get('Id'));
            isSelect = false;
            List<string> subscriptionFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Zuora__Subscription__c').getDescribe().Fields.getMap().values()){
                subscriptionFields.add(ft.getDescribe().getName().toLowerCase());
            }
            List<string> assetFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Asset').getDescribe().Fields.getMap().values()){
                assetFields.add(ft.getDescribe().getName().toLowerCase());
            }

            String query = 'SELECT ' + String.join(subscriptionFields, ',') +  
                        ',( SELECT ' + String.join(assetFields, ',') + ' FROM AssetsVD__r WHERE Quantita__c > 0) ' +
                    ' FROM Zuora__Subscription__c ' +
                    ' WHERE Id = :subscriptionId ';

            prodotti = new List<AssetWrapper>();
            List<Zuora__Subscription__c> subscriptions = (List<Zuora__Subscription__c>) Database.query(query);
            if(subscriptions.size() > 0){
                subscription = subscriptions.get(0);
                for(Asset asst : subscription.AssetsVD__r){
                    AssetWrapper aw = new AssetWrapper();
                    aw.selected = true;
                    /*aw.selected = false;*/
                    aw.prodotto = asst;
                    if(aw.prodotto.QuantitaReso__c == null){
                        aw.prodotto.QuantitaReso__c = 0;
                    }
                    aw.quantitaReso = 1;
                    prodotti.add(aw);
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public PageReference selectAll(){

        if(prodotti != null && !prodotti.isEmpty()){

            for(AssetWrapper aw : prodotti){
                aw.selected = true;
            }

        }

        isSelect = false;

        return null;
    }

    public PageReference deselectAll(){

        if(prodotti != null && !prodotti.isEmpty()){

            for(AssetWrapper aw : prodotti){
                aw.selected = false;
            }

        }

        isSelect = true;

        return null;
    }

    public PageReference richiediReso(){
        selectedAtLeastOne = false;
        resoDigitale = false;
        for(AssetWrapper aw : prodotti){
            selectedAtLeastOne = selectedAtLeastOne || aw.selected;
            resoDigitale = resoDigitale || (aw.selected && aw.prodotto.Copia_digitale__c);
        }
        if(!selectedAtLeastOne) appendErrorMessage('Seleziona almeno un prodotto per creare un reso.');
        if(selectedAtLeastOne && !resoDigitale) return reso();
        return null;
    }

    public PageReference reso(){
        PageReference pr = null;
        try {
            List<Asset> assetToUpdate = new List<Asset>();
            Map<Id,Decimal> movimentazioneStock = new Map<Id,Decimal>();
            for(AssetWrapper aw : prodotti){
                if((resoDigitale || aw.selected) && aw.quantitaReso > 0){
                    aw.prodotto.Reso__c = true;
                    if(resoDigitale) aw.prodotto.QuantitaReso__c = aw.prodotto.Quantity;
                    else if(aw.selected) aw.prodotto.QuantitaReso__c += aw.quantitaReso;
                    System.debug('@@ '+aw.prodotto);
                    System.debug('@@ '+aw.prodotto.Copia_cartacea__c);
                    System.debug('@@ '+aw.prodotto.Movimentazione_Stock__c);
                    if(aw.prodotto.Copia_cartacea__c){
                        movimentazioneStock.put(aw.prodotto.Movimentazione_Stock__c,aw.quantitaReso);
                    }
                    assetToUpdate.add(aw.prodotto);
                }
            }
            System.debug('@@ r18 ms : '+movimentazioneStock);
            System.debug('@@ r18 asset : '+assetToUpdate);
            List<Movimentazione_Stock__c> movimentazioniReso = new List<Movimentazione_Stock__c>();
            for(Movimentazione_Stock__c mv : [SELECT Id,Area_di_Vendita__c,Codice_Cliente_SAP__c,Codice_Prodotto_SAP__c,Contact__c,NrSpedizioneSAP__c,
                                                    Numero_Subscription__c,Prodotto_Vendita_Diretta__c,Quantita__c,Quote__c,Riferimento_Magazzino__c,
                                                    Segno_del_movimento_del__c,Spedizione__c,Stato__c,Subscription__c,SubscriptionName__c,Subscription_Zuora_Id__c,
                                                    Tipo_consegna__c,Tipo_Spedizione__c,Totale_rispedizioni_vendite_dirette__c, Subscription_Charge__c
                                            FROM Movimentazione_Stock__c
                                            WHERE Id IN :movimentazioneStock.keySet()]){
                Movimentazione_Stock__c reso = mv.clone(false,true,false,false);
                System.debug('@@ r18 reso : '+reso);
                reso.Segno_del_movimento_del__c = '+';
                reso.Quantita__c = movimentazioneStock.get(mv.Id);
                reso.Stato__c = 'Da Trasmettere';
                movimentazioniReso.add(reso);
            }
            System.debug('@@ r18 : insert e update');
            insert movimentazioniReso;
            update assetToUpdate;

            List<string> entitlementFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Entitlement__c').getDescribe().Fields.getMap().values()){
                entitlementFields.add(ft.getDescribe().getName().toLowerCase());
            }
            List<Entitlement__c> entitlements = (List<Entitlement__c>) Database.query('SELECT ' + String.join(entitlementFields,',') + ' FROM Entitlement__c WHERE Subscription__c = \'' + subscription.Id + '\'');
            if(!entitlements.isEmpty()){
                Entitlement__c newEntitlement = entitlements.get(0).clone(false,true,false,false);
                newEntitlement.expire_date__c = subscription.Zuora__SubscriptionEndDate__c;
                for(Entitlement__c entitlement : entitlements){
                    entitlement.expire_date__c = Date.today();
                }
                update entitlements;
                List<String> cartItems = new List<String>();
                Map<Id,List<Zuora__SubscriptionProductCharge__c>> charges = DomusUtil.getSubscriptionRatePlanCharges(new Set<Id>{subscription.Id});
                for(Zuora__SubscriptionProductCharge__c charge : charges.get(subscription.Id)){
                    if(charge.Supporto__c != null && charge.Supporto__c.toUpperCase().contains('DIGITALE')){
                        Boolean inserisciProdottoDigitale = true;
                        for(AssetWrapper aw : prodotti){
                            if(aw.prodotto.QuantitaReso__c == aw.prodotto.Quantita__c){
                                inserisciProdottoDigitale = false;
                                break;
                            }
                        }
                        if(inserisciProdottoDigitale){
                            String productId = null;
                            if(charge.Tipo_Prodotto__c == 'Abbonamento'){
                                productId = charge.Product_Rate_Plan_Charge__r.zqu__ProductRatePlan__c;
                            }else if(charge.Tipo_Prodotto__c == 'Vendita Diretta'){
                                productId = charge.Codice_SAP__c;
                            }else continue;
                            JSONGenerator gen = JSON.createGenerator(true);
                            gen.writeStartObject();
                            if(productId == null){
                                throw new DomusException('Product id mancante');
                            }
                            gen.writeStringField('product_id', productId);
                            gen.writeStringField('product_internalId', '');
                            gen.writeStringField('price', '');
                            gen.writeEndObject();
                            cartItems.add(gen.getAsString());
                        }
                    }
                }
                if(!cartItems.isEmpty()){ // c'è almeno un prodotto digitale
                    newEntitlement.cart__c = String.join(cartItems,',');
                    insert newEntitlement;
                }
            }
            pr = goToSubscription();
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return pr;
    }

    public PageReference goToSubscription(){
        PageReference pr = null;
        try {
            pr = new PageReference('/'+subscription.Id);
            pr.setRedirect(true);
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return pr;
    }

    public class AssetWrapper{
        public Boolean selected {get;set;}
        public Asset prodotto {get;set;}
        public Integer quantitaReso {get;set;}
    }

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }
}