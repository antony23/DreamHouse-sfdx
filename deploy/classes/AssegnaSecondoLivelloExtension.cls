public with sharing class AssegnaSecondoLivelloExtension {
public Case c {get;set;}
    Id oldOwnerId;
    public Boolean notifyOwner {get;set;}
    public boolean canChangeOwner {get;set;}     
    
    public AssegnaSecondoLivelloExtension(ApexPages.StandardController controller) {
        c = (Case) controller.getRecord();
     
        List<Case_Manual_Escalation_Rule__mdt> escalationRules = [SELECT Coda__c FROM Case_Manual_Escalation_Rule__mdt WHERE Categoria__c = :c.Categoria__c AND Sottocategoria__c = :c.Sottocategoria__c];
        if(!escalationRules.isEmpty()){
    		canChangeOwner = true;
            id newOwner = [select QueueId from QueueSobject where Queue.Name =: escalationRules.get(0).Coda__c].QueueId;
            c.OwnerId = newOwner;
            c.Status = 'Assegnato al 2° livello';
            c.IsEscalated = true;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'è necessario cambiare coda manualmente'));
            canChangeOwner = false;
        }
    } 
    
    
    public PageReference changeOwner(){
        Savepoint sp = null;
        if(!Test.isRunningTest()){
            sp = Database.setSavepoint();
        }
        PageReference p = new PageReference('/500');

        if(c.OwnerId != oldOwnerId){
            try{
                
                Database.DMLOptions options = new Database.DMLOptions();
                options.EmailHeader.triggerUserEmail = true;
                c.setOptions(options);
                update c;
                
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
                if(!Test.isRunningTest()) Database.rollback(sp);
                p = null;
                
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Per riassegnare il Caso è necessario modificare il titolare'));
            p = null;
        }
        return p;
    }
}