global class BonificaCopie implements Database.Batchable<sObject> {
    
    String query;
    Set<Id> assetIds;
    
    global BonificaCopie() {
        query = 'select Id,Contact.MailingState,Codice_Postale__c,CodiceProvincia__c,CodiceRegione__c,SiglaRegione__c,SiglaRegioneAbbr__c,ZonadItalia__c,Suddivisione_geografica_2__c from asset where Codice_Nazione__c = \'IT\' and Codice_Postale__c != null AND Inviata__c=false';
    }

    global BonificaCopie(Set<Id> assetIds) {
        this.assetIds = assetIds;
        query = 'SELECT Id, Contact.MailingState, Codice_Postale__c, CodiceProvincia__c, CodiceRegione__c, SiglaRegione__c, SiglaRegioneAbbr__c, ZonadItalia__c, Suddivisione_geografica_2__c from Asset WHERE Codice_Nazione__c = \'IT\' AND Codice_Postale__c != null AND Inviata__c = false AND Id IN :assetIds';
    }

    global BonificaCopie(String queryString) {
        this.query = queryString;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Asset> copie = (List<Asset>) scope;
        List<CapDetails__c> capDetails = [SELECT Id,Name,CodiceProvincia__c,CodiceRegione__c,SiglaRegione__c,SiglaRegioneAbbr__c,ZonadItalia__c,Suddivisione_geografica_2__c FROM CapDetails__c];
        for(Asset copia : copie){
            for(CapDetails__c capDetail : capDetails){
                Boolean found = false;
                if(capDetail.Name == copia.Codice_Postale__c) found = true;
                if(!found && capDetail.Name.contains('x')){
                    String capPrefix = capDetail.Name.replaceAll('x','');
                    if(copia.Codice_Postale__c.startsWith(capPrefix)) found = true;
                }
                if(!found && capDetail.Name.contains('-')){
                    String[] range = capDetail.Name.split('-');
                    if(Integer.valueOf(copia.Codice_Postale__c) >= Integer.valueOf(range[0]) && Integer.valueOf(copia.Codice_Postale__c) <= Integer.valueOf(range[1])) found = true;
                }
                if(found){
                    copia.CodiceRegione__c = capDetail.CodiceRegione__c;
                    copia.SiglaRegione__c = capDetail.SiglaRegione__c;
                    copia.SiglaRegioneAbbr__c = capDetail.SiglaRegioneAbbr__c;
                    copia.ZonadItalia__c = capDetail.ZonadItalia__c;
                    copia.Suddivisione_geografica_2__c = capDetail.Suddivisione_geografica_2__c;
                    if(copia.CodiceRegione__c == 'SAR'){ // la sardergna ha per un cap più province, ma pensa te....
                        copia.CodiceProvincia__c = copia.Contact.MailingState;
                    }else{
                        copia.CodiceProvincia__c = capDetail.CodiceProvincia__c;
                    }
                    break;
                }
            }
        }
        Database.update(copie,false);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}