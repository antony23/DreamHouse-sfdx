@isTest
private class ZTest_SendNotification{
	
	@isTest
	static void invokeSendEmail(){
		Test.startTest();
		Id batchId = Database.executeBatch(new SendNotification());
		//Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        //System.assertEquals(1, invocations);
	}

	@isTest
	static void invokeSendEmailById(){
		Notifica_Informativa__c notifica = [SELECT Id FROM Notifica_Informativa__c WHERE Stato__c = 'Draft' AND Invio_Manuale__c = false LIMIT 1];
		Test.startTest();
		Set<Id> notificaId = new Set<Id>{notifica.Id};
		Id batchId = Database.executeBatch(new SendNotification(notificaId));
		//Integer invocations = Limits.getEmailInvocations();
		Test.stopTest();
		//System.assertEquals(1, invocations);
	}

	@isTest
	static void scheduleSendEmail(){
		Test.startTest();
		SendNotification sn = new SendNotification();
		String sch = '0 0 2 * * ?';
		System.schedule('Send Notification', sch, sn);
        Test.stopTest();
	}

	@isTest
	static void updateNotificaManuale(){
		Notifica_Informativa__c notifica = [SELECT Stato__c FROM Notifica_Informativa__c WHERE Stato__c = 'Draft' AND Invio_Manuale__c = true LIMIT 1];
		Test.startTest();
		notifica.Stato__c = 'Sent';
		update notifica;
        Test.stopTest();
	}

	@testSetup
	public static void testSetup(){
		Account a = new Account(Name = 'Account Test');
		AccountTriggerHandler.skip = true;
        insert a;
        AccountTriggerHandler.skip = false;

        Contact c = new Contact(AccountId = a.Id, FirstName = 'Contact', LastName = 'Test', Email = 'email@test.it');
        ContactTriggerHandler.skip = true;
        insert c;
        ContactTriggerHandler.skip = false;

        Notifica_Informativa__c n1 = new Notifica_Informativa__c(Contatto__c = c.Id, Stato__c = 'Draft');
        Notifica_Informativa__c n2 = new Notifica_Informativa__c(Contatto__c = c.Id, Stato__c = 'Draft', Invio_Manuale__c = true);
        Notifica_Informativa__c n3 = new Notifica_Informativa__c(Contatto__c = c.Id, Stato__c = 'Sent', Invio_Manuale__c = true);
        List<Notifica_Informativa__c> listaNotifiche = new List<Notifica_Informativa__c>{n1, n2, n3};

        NotificaInformativaTriggerHandler.skipTrigger = true;
        insert listaNotifiche;
        NotificaInformativaTriggerHandler.skipTrigger = false;

        List<Contact> contacts = new List<Contact>();

        for(Integer i = 0; i < 50; i++) {
			contacts.add(new Contact(FirstName = 'Contact', LastName = 'Test ' + i, AccountId = a.Id, Email = 'email@test.it'));
		}

		List<Notifica_Informativa__c> listaNotifiche2 = new List<Notifica_Informativa__c>();

		ContactTriggerHandler.skip = True;
		insert contacts;
		ContactTriggerHandler.skip = False;

		for(Contact cont : contacts){
			listaNotifiche2.add(new Notifica_Informativa__c(Contatto__c = cont.Id, Stato__c = 'Draft'));
		}

		NotificaInformativaTriggerHandler.skipTrigger = true;
		insert listaNotifiche2;
		NotificaInformativaTriggerHandler.skipTrigger = false;
	}
}