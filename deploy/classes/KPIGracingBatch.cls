global class KPIGracingBatch implements Database.Batchable<sObject>,Schedulable {
	
    // Schedulable
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new KPIGracingBatch(),100);
    }

    // Batch
	String query;
	
	global KPIGracingBatch() {
		query = 'SELECT Id,AccountId FROM Asset WHERE createddate >= LAST_N_DAYS:3 ORDER BY Lastmodifieddate DESC';
        if(Test.isRunningTest()) query += ' LIMIT 100';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		Set<Id> accountsPerCalcoloKPIId = new Set<Id>();
        for(Asset copiaRecente : (List<Asset>) scope){
            if(copiaRecente.AccountId == null) continue;
            accountsPerCalcoloKPIId.add(copiaRecente.AccountId);
        }
        Map<Id,Account> accountsPerCalcoloKPI = new Map<Id,Account>();
        for(List<Account> accounts : [ SELECT Id,Copie_Gracing__c,SubscriptionConGracing__c,Copie_Gracing_APR__c,Copie_Gracing_DOM__c,Copie_Gracing_DRT__c,Copie_Gracing_MRD__c,Copie_Gracing_MRM__c,Copie_Gracing_QRT__c,
                                                                            Copie_Gracing_RLC__c,Copie_Gracing_TOP__c,Copie_Gracing_TTR__c,Copie_Gracing_XOR__c,Subscription_con_Gracing_APR__c,Subscription_con_Gracing_DOM__c,Subscription_con_Gracing_DRt__c,
                                                                            Subscription_Con_Gracing_MRD__c,Subscription_con_Gracing_MRM__c,Subscription_con_Gracing_QRT__c,Subscription_con_Gracing_RCL__c,Subscription_con_Gracing_TOP__c,Subscription_con_Gracing_TTR__c,
                                                                            Subscription_con_Gracing_XOR__c,
                                                                            (SELECT SubscriptionAllCopy__c, SubscriptionAllCopy__r.Stato_sfdc__c, Inviata__c, GracingAllowed__c, Calendario__r.Testata__r.Name, Testata__c FROM Assets WHERE SubscriptionAllCopy__c != null)
                                                                            FROM Account 
                                                                            WHERE Id IN :accountsPerCalcoloKPIId]){
            for(Account a : accounts){
                accountsPerCalcoloKPI.put(a.Id,a);
            }
        }
                                                                    
		Map<Id,List<Zuora__Subscription__c>> subscriptionsByAccount = new Map<Id,List<Zuora__Subscription__c>>();
		for(Zuora__Subscription__c subscription : [SELECT Id,Testate__c,Stato_sfdc__c,Zuora__Account__c FROM Zuora__Subscription__c WHERE Zuora__Account__c IN : accountsPerCalcoloKPIId]){
            if(!subscriptionsByAccount.containsKey(subscription.Zuora__Account__c)){
                subscriptionsByAccount.put(subscription.Zuora__Account__c, new List<Zuora__Subscription__c>());
            }
            subscriptionsByAccount.get(subscription.Zuora__Account__c).add(subscription);
        }
        
        List<Campo_KPI__mdt> kpiFieldList = [SELECT MasterLabel,Copie_gracing__c,Ageing_effettivo__c,Cicli__c,Cicli_Ponderati__c,Copie_Previste__c,Subscription_con_Gracing__c,Testata__c,Testata_Checkbox__c  FROM Campo_KPI__mdt];
        Map<String, Campo_KPI__mdt> kpiFieldMap = new Map<String, Campo_KPI__mdt>();
        for(Campo_KPI__mdt k : kpiFieldList){
            kpiFieldMap.put(k.MasterLabel.toUpperCase(), k);
        }

        Map<Account,Map<String,List<Asset>>> copieByTestataByAccount = new Map<Account,Map<String,List<Asset>>>();
        Map<Account,Map<String,Set<Id>>> subscriptionsByTestataByAccount = new Map<Account,Map<String,Set<Id>>>();
        for(Account acc : accountsPerCalcoloKPI.values()){
            copieByTestataByAccount.put(acc, new Map<String,List<Asset>>());
            subscriptionsByTestataByAccount.put(acc, new Map<String,Set<Id>>());
            for(Campo_KPI__mdt k : kpiFieldList){
                copieByTestataByAccount.get(acc).put(k.MasterLabel.toUpperCase(), new List<Asset>());
                subscriptionsByTestataByAccount.get(acc).put(k.MasterLabel.toUpperCase(), new Set<Id>());
            }
            for(Asset copia : acc.Assets){
                String nomeTestata = copia.Calendario__r.Testata__r.Name;
                if(String.isNotBlank(nomeTestata)) {
                    copieByTestataByAccount.get(acc).get(nomeTestata.toUpperCase()).add(copia);
                }
            }
            if(subscriptionsByAccount.containsKey(acc.Id)){
                for(Zuora__Subscription__c subscription : subscriptionsByAccount.get(acc.Id)){
                    List<String> testate = subscription.Testate__c != null ? subscription.Testate__c.split(';') : new List<String>();
                    for(String testata : testate){
                        if(testata == 'Vendite Dirette') continue;
                        testata = testata.toUpperCase();
                        if(subscriptionsByTestataByAccount.get(acc).containsKey(testata)){
                        	subscriptionsByTestataByAccount.get(acc).get(testata).add(subscription.Id);
                        }
                    }
                }
            }
        }
		List<Account> accountsKPI = new List<Account>();
        for(Account accountPerCalcoloKPI : accountsPerCalcoloKPI.values()){
            Account accountKPI = new Account(Id = accountPerCalcoloKPI.Id);
            Integer numeroCopieGracing = 0;
            Integer numeroCopieInviate = 0;
            Map<String,Set<Id>> subscriptionsGracingByTestata = new Map<String,Set<Id>>();
            Set<Id> subscriptionsConGracingNonSaldate = new Set<Id>();
            Set<Id> subscriptionsConGracing = new Set<Id>();
            for(String testata : copieByTestataByAccount.get(accountPerCalcoloKPI).keySet()){
                Integer numeroCopieGracingTestata = 0;
                Integer numeroCopieInviateTestata = 0;
                testata = testata.toUpperCase();
                if(copieByTestataByAccount.get(accountPerCalcoloKPI).get(testata) != null){
                    for(Asset copia : copieByTestataByAccount.get(accountPerCalcoloKPI).get(testata)){
                        if(copia.GracingAllowed__c && copia.Inviata__c){
                            numeroCopieGracingTestata ++;
                            subscriptionsConGracing.add(copia.SubscriptionAllCopy__c);
                            if(!subscriptionsGracingByTestata.containsKey(copia.Testata__c.toUpperCase())){
                                subscriptionsGracingByTestata.put(copia.Testata__c,new Set<Id>());
                            }
                            subscriptionsGracingByTestata.get(copia.Testata__c.toUpperCase()).add(copia.SubscriptionAllCopy__c);
                            if(copia.SubscriptionAllCopy__r.Stato_sfdc__c == 'Chiusa: Non saldata'){
                                subscriptionsConGracingNonSaldate.add(copia.SubscriptionAllCopy__c);
                            }
                        }
                        if(copia.Inviata__c){
                            numeroCopieInviateTestata ++;
                        }
                    }
                    Campo_KPI__mdt testataKPI = kpiFieldMap.get(testata);
                    if(numeroCopieGracingTestata != null && numeroCopieInviateTestata != null){
                        if(testataKPI.Copie_gracing__c != null){
                            accountKPI.put(testataKPI.Copie_gracing__c, numeroCopieInviateTestata != 0 ? Decimal.valueOf(numeroCopieGracingTestata) / Decimal.valueOf(numeroCopieInviateTestata) * 100 : 0);
                        }
                        numeroCopieGracing += numeroCopieGracingTestata;
                        numeroCopieInviate += numeroCopieInviateTestata;
                    }
                }
            }
            if(numeroCopieGracing != null && numeroCopieInviate != null && numeroCopieInviate > 0){
                accountKPI.Copie_Gracing__c = Decimal.valueOf(numeroCopieGracing) / Decimal.valueOf(numeroCopieInviate) * 100;
            }
            if(subscriptionsByAccount.get(accountKPI.Id).size() > 0){
                accountKPI.SubscriptionConGracing__c = Decimal.valueOf(subscriptionsConGracing.size()) / Decimal.valueOf(subscriptionsByAccount.get(accountKPI.Id).size()) * 100;
                accountKPI.Abb_Non_Saldato__c = Decimal.valueOf(subscriptionsConGracingNonSaldate.size()) / Decimal.valueOf(subscriptionsByAccount.get(accountKPI.Id).size()) * 100;
                for(Campo_KPI__mdt k : kpiFieldList){
                    Integer numeroSubscriptionsConGracing = subscriptionsGracingByTestata.containsKey(k.MasterLabel.toUpperCase()) ? subscriptionsGracingByTestata.get(k.MasterLabel.toUpperCase().toUpperCase()).size() : 0;
                    Integer numeroSubscription = subscriptionsByTestataByAccount.get(accountPerCalcoloKPI).get(k.MasterLabel.toUpperCase()).size();
                    if(k.Subscription_con_Gracing__c != null){
                        accountKPI.put(k.Subscription_con_Gracing__c, numeroSubscription != 0 ? Decimal.valueOf(numeroSubscriptionsConGracing) / Decimal.valueOf(numeroSubscription) * 100 : 0);
                        System.debug(loggingLevel.Error, '*** accountKPI: ' + accountKPI.get(k.Subscription_con_Gracing__c));
                        System.debug(loggingLevel.Error, '*** 1: ' + (numeroSubscription != 0 ? Decimal.valueOf(numeroSubscriptionsConGracing) / Decimal.valueOf(numeroSubscription) * 100 : 0));
                        System.debug(loggingLevel.Error, '*** 2: ' + (numeroSubscription != 0 ? Decimal.valueOf(numeroSubscriptionsConGracing) / Decimal.valueOf(numeroSubscription) * 100 : 0));
                    }
                }
            }
            accountsKPI.add(accountKPI);
        }

        AccountTriggerHandler.skip = true;
        update accountsKPI;
        AccountTriggerHandler.skip = false;

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}