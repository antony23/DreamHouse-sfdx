global class ProcessAmendment implements Database.Batchable<sObject>,
										Database.StateFul,
										Database.AllowsCallouts,
										Schedulable {
	// Schedulable
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new ProcessAmendment(),100);
	}

	// Batch
	String query;
	Set<Id> subscriptionsToProcessIds;
	
	global ProcessAmendment() {
		query = 'SELECT Id FROM Zuora__Subscription__c WHERE Label_Run_processed__c = false OR Label_Run_IsError__c = true';		
	}

	global ProcessAmendment(Set<Id> subsIds) {
		subscriptionsToProcessIds = subsIds;
		query = 'SELECT Id FROM Zuora__Subscription__c WHERE Id IN :subscriptionsToProcessIds AND IsAmendment__c = true';		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}