public with sharing class QuoteDetailPreviewUtil {

    // Request
	public class SubscribePreview{
		public SubscribeWrapper[] subscribes;
	}

	public class SubscribeWrapper{
		public AccountWrapper Account						{get;set;}
		public ContactWrapper BillToContact					{get;set;}
		public ContactWrapper SoldToContact 				{get;set;}
		public PreviewOptionsWrapper PreviewOptions 		{get;set;}
		public SubscriptionDataWrapper SubscriptionData 	{get;set;}
	}

	public class AccountWrapper{
		public String Name                          		{get;set;}
        public String CurrencyType                  		{get;set;}
        public String BatchName                     		{get;set;}
        public String BillCycleDay                  		{get;set;}
        public String PaymentTerm 							{get;set;}
        public Boolean AutoPay								{get;set;}
	}

	public class ContactWrapper{
		public String FirstName                     		{get; set;}
        public String LastName                      		{get; set;}
        public String Address1                      		{get; set;}
        public String WorkEmail                     		{get; set;}
        public String MobilePhone                   		{get; set;}
        public String WorkPhone                    	 		{get; set;}
        public String City                          		{get; set;}
        public String State                         		{get; set;}
        public String ZipCode                       		{get; set;}
        public String Country                       		{get; set;}
        public String County                        		{get; set;}
	}

	public class PreviewOptionsWrapper{
		public Boolean EnablePreviewMode 					{get; set;}
		public Integer NumberOfPeriods						{get; set;}
		public String PreviewType 							{get; set;}
	}


	public class SubscriptionDataWrapper{
		public RatePlanDataWrapper[] RatePlanData 			{get;set;}
		public SubscriptionWrapper Subscription 			{get;set;}

	}

	public class RatePlanDataWrapper{
		public RatePlanWrapper RatePlan 						{get;set;}
		public RatePlanChargeDataWrapper[] RatePlanChargeData 	{get;set;}
	}

	public class RatePlanWrapper{
		public String ProductRatePlanId 					{get;set;}
	}

	public class RatePlanChargeDataWrapper{
		public RatePlanChargeWrapper RatePlanCharge 		{get;set;}
	}

	public class RatePlanChargeWrapper{
		public String ProductRatePlanChargeId 				{get;set;}
		public Double Quantity 								{get;set;}
		public String BillCycleType 						{get;set;}
		public String ChargeModel 							{get;set;}
		public String ChargeType 							{get;set;}
		public Double MRR 									{get;set;}
		public Double TCV 									{get;set;}
		public String UOM 									{get;set;}
		public String ListPriceBase 						{get;set;}
		public Double Price 	 							{get;set;}
		public Double SpecificBillingPeriod 				{get;set;}
		public String BillingPeriod  						{get;set;}
	}

	public class SubscriptionWrapper{
		public Date ContractEffectiveDate					{get;set;}
		public String TermType 								{get;set;}
		public Integer InitialTerm 							{get;set;}
		public Integer RenewalTerm 							{get;set;}
		public Date ServiceActivationDate					{get;set;}
		public Boolean AutoRenew 							{get;set;}
		public String InitialTermPeriodType 				{get;set;}
		public String RenewalTermPeriodType 				{get;set;}
	}

    // Response
    public class ResponseWrapper{
        public Boolean Success                              {get;set;}
        public String AccountId                             {get;set;}
        public String AccountNumber                         {get;set;}
        public Double TotalMrr                              {get;set;}
        public Double TotalTcv                              {get;set;}
        public ChargeMetricsDataWrapper ChargeMetricsData   {get;set;}
        public InvoiceResultWrapper InvoiceResult           {get;set;}
        public InvoiceDataWrapper[] InvoiceData             {get;set;}
        public ErrorWrapper[] Errors                        {get;set;}
    }

    public class InvoiceDataWrapper{
        public InvoiceWrapper Invoice                       {get;set;}
        public InvoiceItemWrapper[] InvoiceItem             {get;set;}
    }

    public class ChargeMetricsDataWrapper{
        public ChargeMetricsWrapper[] ChargeMetrics         {get;set;}
    }

    public class InvoiceResultWrapper{
        public InvoiceWrapper[] Invoice                     {get;set;}
    }

    public class InvoiceWrapper{
        public String InvoiceNumber                         {get;set;}
        public String AccountId                             {get;set;}
        public Double AdjustmentAmount                      {get;set;}
        public Double Amount                                {get;set;}
        public Double AmountWithoutTax                      {get;set;}
        public Double TaxAmount                             {get;set;}
        public Double TaxExemptAmount                       {get;set;}
    }

    public class InvoiceItemWrapper{
        public Boolean success                              {get;set;}
        public String chargeAmount                          {get;set;}
        public String chargeDescription                     {get;set;}
        public String chargeName                            {get;set;}
        public String productName                           {get;set;}
        public String quantity                              {get;set;}
        public String serviceEndDate                        {get;set;}
        public String serviceStartDate                      {get;set;}
        public String taxAmount                             {get;set;}
        public String unitOfMeasure                         {get;set;}
    }

    public class ErrorWrapper{
        public String Code                                  {get;set;}
        public String Message                               {get;set;}
    }

    public class ChargeMetricsWrapper{
        public String ChargeNumber                          {get;set;}
        public String OriginalId                            {get;set;}
        public String OriginalRatePlanId                    {get;set;}
        public String ProductRatePlanChargeId               {get;set;}
        public String ProductRatePlanId                     {get;set;}
        public Double DMRR                                  {get;set;}
        public Double DTCV                                  {get;set;}
        public Double MRR                                   {get;set;}
        public Double TVC                                   {get;set;}
    }

	public static List<ResponseWrapper> createSubscriptionPreview(Id quoteId, Id billToContactId, Id soldToContactId){

        zqu__Quote__c quote = BillingAccountUtil.fetchQuote(quoteId);      
        Contact billToContact = BillingAccountUtil.fetchContact(billToContactId);
        Contact soldToContact = BillingAccountUtil.fetchContact(soldToContactId);	

        SubscribePreview request = createZuoraPreviewRequest(quote, billToContact, soldToContact);
        HttpResponse restResponse = sendToZuora(request);

        String restResponseJson = restResponse.getBody();
        System.debug('*_* restResponseJson: ' + restResponseJson);

        List<ResponseWrapper> listResponseWrapper = (List<ResponseWrapper>) JSON.deserialize(restResponseJson, List<ResponseWrapper>.class);
        return listResponseWrapper;

    }

    private static SubscribePreview createZuoraPreviewRequest(zqu__Quote__c quote, Contact billToContact, Contact soldToContact){
 
        PreviewOptionsWrapper po = createPreviewOptionsWrapper();
        AccountWrapper za = createAccountWrapper(quote);
        ContactWrapper btc = createContactWrapper(billToContact);
        ContactWrapper stc = createContactWrapper(soldToContact);
        SubscriptionWrapper subscription = createSubscriptionWrapper(quote);
        List<RatePlanDataWrapper> listRatePlanWrapper = createRatePlanDataWrapper(quote);
        
        SubscribeWrapper subscribe = new SubscribeWrapper();
        subscribe.Account 		= za;
        subscribe.BillToContact = btc;
        subscribe.SoldToContact = stc;
        subscribe.PreviewOptions = po;
        subscribe.SubscriptionData = new SubscriptionDataWrapper();
        subscribe.SubscriptionData.Subscription = subscription;
        subscribe.SubscriptionData.RatePlanData = new List<RatePlanDataWrapper>(listRatePlanWrapper);

        SubscribePreview preview = new SubscribePreview();
        preview.subscribes = new SubscribeWrapper[]{subscribe};

        return preview;
    }

    private static HttpResponse sendToZuora(SubscribePreview request){

        final Map<String, String> apiNameMap = new Map<String, String>{
            'CurrencyType'	=>	'Currency',
            'BatchName'     =>	'Batch'
        };

        String requestBody = JSON.serialize(request, true);

        for(String s : apiNameMap.keySet()){
            requestBody = requestBody.replace(s, apiNameMap.get(s));
        }

        Zuora_Credential__c zuoraCredential = Zuora_Credential__c.getInstance('Subscribe');
        System.debug('*_* zuoraCredential: ' + zuoraCredential);
        System.debug('*_* requestBody: ' + requestBody);

        Http rest = new Http();
        HttpRequest restRequest = new HttpRequest();
        restRequest.setMethod('POST');
        restRequest.setHeader('Content-Type', 'application/json');
        restRequest.setEndpoint(zuoraCredential.EndPoint__c);
        restRequest.setHeader('apiAccessKeyId', zuoraCredential.apiAccessKeyId__c);
        restRequest.setHeader('apiSecretAccessKey', zuoraCredential.apiSecretAccessKey__c);
        restRequest.setBody(requestBody);
        HttpResponse restResponse = rest.send(restRequest);

        return restResponse;
    }

    private static List<zqu__QuoteRatePlanCharge__c> getQuoteRatePlanCharge(Id quoteId){
        final String additionalFields = 'zqu__QuoteRatePlan__r.zqu__ProductRatePlanZuoraId__c';
        final String query = 'SELECT ' + String.join(fieldsOf('zqu__QuoteRatePlanCharge__c'), ',') + ',' + additionalFields +
                                ' FROM zqu__QuoteRatePlanCharge__c WHERE Quote_Id__c = :quoteId';
        return (List<zqu__QuoteRatePlanCharge__c>) Database.query(query);
    }

    public static List<String> fieldsOf(String className){
        final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        return fields;
    }

    private static PreviewOptionsWrapper createPreviewOptionsWrapper(){
        PreviewOptionsWrapper po = new PreviewOptionsWrapper();
        po.EnablePreviewMode    = true; 
        po.NumberOfPeriods      = 1;
        po.PreviewType          = 'InvoiceItemChargeMetrics';
        return po;
    }

    private static AccountWrapper createAccountWrapper(zqu__Quote__c quote){
        AccountWrapper a = new AccountWrapper();
        a.AutoPay          = false;
        a.BillCycleDay     = quote.zqu__BillCycleDay__c;
        a.CurrencyType     = quote.zqu__Currency__c;
        a.BatchName        = quote.zqu__BillingBatch__c;
        a.Name             = quote.zqu__Account__r.Name;
        a.PaymentTerm      = quote.zqu__PaymentTerm__c;
        return a;
    }

    private static ContactWrapper createContactWrapper(Contact contatto){
        ContactWrapper c = new ContactWrapper();
        c.LastName        = contatto.LastName;
        c.FirstName       = BillingAccountUtil.checkContactFirstName(contatto);
        c.Address1        = contatto.MailingStreet;
        c.City            = contatto.MailingCity;
        c.State           = contatto.MailingState;
        c.Country         = contatto.MailingCountry;
        c.ZipCode         = contatto.MailingPostalCode;
        c.WorkEmail       = contatto.Email;
        c.MobilePhone     = contatto.MobilePhone;
        c.WorkPhone       = contatto.Phone;
        c.County          = contatto.zqu__County__c;
        return c;
    }

    private static SubscriptionWrapper createSubscriptionWrapper(zqu__Quote__c quote){
        SubscriptionWrapper s = new SubscriptionWrapper();
        s.AutoRenew              = quote.zqu__AutoRenew__c;
        s.ContractEffectiveDate  = quote.zqu__Service_Activation_Date__c;
        s.TermType               = quote.zqu__Subscription_Term_Type__c.toUpperCase();
        s.InitialTerm            = Integer.valueOf(quote.zqu__InitialTerm__c);
        s.RenewalTerm            = Integer.valueOf(quote.zqu__RenewalTerm__c);
        s.ServiceActivationDate  = quote.zqu__Service_Activation_Date__c;
        s.InitialTermPeriodType  = quote.zqu__InitialTermPeriodType__c;
        s.RenewalTermPeriodType  = quote.zqu__RenewalTermPeriodType__c;
        return s;
    }

    private static List<RatePlanDataWrapper> createRatePlanDataWrapper(zqu__Quote__c quote){

        List<zqu__QuoteRatePlanCharge__c> listQuoteRatePlanCharge = getQuoteRatePlanCharge(quote.Id);
        Map<String, List<zqu__QuoteRatePlanCharge__c>> mapQuoteRatePlanCharge = new Map<String, List<zqu__QuoteRatePlanCharge__c>>();
        List<RatePlanDataWrapper> listRatePlanWrapper = new List<RatePlanDataWrapper>();

        for(zqu__QuoteRatePlanCharge__c qrpc : listQuoteRatePlanCharge) {
            if(!mapQuoteRatePlanCharge.containsKey(qrpc.zqu__QuoteRatePlan__r.zqu__ProductRatePlanZuoraId__c)) {
                mapQuoteRatePlanCharge.put(qrpc.zqu__QuoteRatePlan__r.zqu__ProductRatePlanZuoraId__c, new List<zqu__QuoteRatePlanCharge__c>());
            }

            mapQuoteRatePlanCharge.get(qrpc.zqu__QuoteRatePlan__r.zqu__ProductRatePlanZuoraId__c).add(qrpc);
        }

        for(String productRatePlanZuoraId : mapQuoteRatePlanCharge.keySet()){

            RatePlanWrapper rp = new RatePlanWrapper();
            rp.ProductRatePlanId = productRatePlanZuoraId;

            List<RatePlanChargeDataWrapper> listRatePlanChargeWrapper = new List<RatePlanChargeDataWrapper>();
            List<zqu__QuoteRatePlanCharge__c> listaQuoteCharge = mapQuoteRatePlanCharge.get(productRatePlanZuoraId);

            for(zqu__QuoteRatePlanCharge__c quoteRatePlanCharge : listaQuoteCharge){

                RatePlanChargeDataWrapper rpcd = new RatePlanChargeDataWrapper();
                RatePlanChargeWrapper rpc = new RatePlanChargeWrapper();
                rpc.ProductRatePlanChargeId = quoteRatePlanCharge.zqu__ProductRatePlanChargeZuoraId__c;
                rpc.Quantity                = quoteRatePlanCharge.zqu__Quantity__c;
                rpc.BillCycleType           = quoteRatePlanCharge.zqu__BillCycleType__c;
                rpc.ChargeModel             = quoteRatePlanCharge.zqu__Model__c;                        
                rpc.ChargeType              = quoteRatePlanCharge.zqu__ChargeType__c;
                rpc.MRR                     = quoteRatePlanCharge.zqu__MRR__c;
                rpc.TCV                     = quoteRatePlanCharge.zqu__TCV__c;
                rpc.ListPriceBase           = quoteRatePlanCharge.zqu__ListPriceBase__c;
                rpc.Price                   = quoteRatePlanCharge.zqu__ListPrice__c;
                rpc.SpecificBillingPeriod   = quoteRatePlanCharge.zqu__SpecificBillingPeriod__c;
                rpc.UOM                     = quoteRatePlanCharge.zqu__UOM__c;
                rpc.BillingPeriod           = quoteRatePlanCharge.zqu__Period__c;
                rpcd.RatePlanCharge = rpc;
                listRatePlanChargeWrapper.add(rpcd);
                
            }

            RatePlanDataWrapper rpd = new RatePlanDataWrapper();
            rpd.RatePlan = rp;
            rpd.RatePlanChargeData = new List<RatePlanChargeDataWrapper>(listRatePlanChargeWrapper);
            listRatePlanWrapper.add(rpd);

        }

        return listRatePlanWrapper;

    }

    public static void updateRelatedObjects(Id quoteId, ResponseWrapper zuoraResponse){
        zqu__Quote__c quote = BillingAccountUtil.fetchQuote(quoteId);
        quote.zqu__Previewed_SubTotal__c = zuoraResponse.InvoiceData[0].Invoice.AmountWithoutTax;
        quote.zqu__Previewed_Tax__c = zuoraResponse.InvoiceData[0].Invoice.TaxAmount;
        quote.zqu__Previewed_Total__c = zuoraResponse.InvoiceData[0].Invoice.Amount;
        update quote;
    }

    /*
    private static String getApplyDiscountTo(zqu__QuoteRatePlanCharge__c qrpc){

        String applyDiscountTo = null;
        Integer discountApplyType = Integer.valueOf(qrpc.zqu__ProductRatePlanCharge__r.zqu__Discount_Apply_Type__c);

        switch on discountApplyType{
            when 1 {
                applyDiscountTo = 'ONETIME';
            }
            when 2 {
                applyDiscountTo = 'RECURRING';
            }
            when 3 {
                applyDiscountTo = 'ONETIMERECURRING';
            }
            when 4 {
                applyDiscountTo = 'USAGE';
            }
            when 5 {
                applyDiscountTo = 'ONETIMEUSAGE';
            }
            when 6 {
                applyDiscountTo = 'RECURRINGUSAGE';
            }
            when 7 {
                applyDiscountTo = 'ONETIMERECURRINGUSAGE';
            }
        }

        return applyDiscountTo;
    }
    */
    
}