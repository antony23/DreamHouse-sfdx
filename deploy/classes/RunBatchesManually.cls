public with sharing class RunBatchesManually {
    public AsyncApexJob  aaj {get;set;}
    public void run() {
        String batchName = ApexPages.currentPage().getParameters().get('batchName');
        id sfdcJobID = null;
        if(batchName == 'SubscriptionRenewalBatch'){
           sfdcJobID = Database.executeBatch(new SubscriptionRenewalBatch(), 200);
        }
        aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: sfdcJobID ];     
    }
    
    public void getJobStatus(){
        aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: aaj.id];
    }
}