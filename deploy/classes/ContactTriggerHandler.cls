public class ContactTriggerHandler {
    public static Boolean fromContactTrigger = false;
    public static Boolean skip = false;
    
    public void onBeforeInsert(List<Contact> triggerNew, Map<Id, Contact> triggerNewMap) { 
        //*** DH-1041 ***//
        setMainContactFromLead(triggerNew);
        //*** DH-1041 ***//

		calcolaCodiceCliente(triggerNew);
        aggiornaCodiceSapAccount(triggerNew);
        //*** CR Consensi ***//
        setFieldsOnBeforeInsert(triggerNew);
        //*** CR Consensi - Fine ***/)
    }
    
    public void onAfterInsert(List<Contact> triggerNew, Map<Id, Contact> triggerNewMap) {
        
        /*** CR Professional ***/
        for(Contact con : triggerNew){
            if(con.Codice_IBAN__c != null && String.isNotBlank(con.Codice_IBAN__c)){
                CheckIBAN.validateIban(con.Id,con.Codice_IBAN__c);
            }
        }
        /*** CR Professional - Fine ***/

        calcolaCodiceCliente(triggerNew);

        // se inserisco un contatto come principale devo disabilitare gli altri
        Set<Id> accountIds = new Set<Id>();
        Set<Id> contattiPrincipaliIds = new Set<Id>();
        for(Contact contattoInserito : triggerNew){
            if(contattoInserito.MainContact__c && contattoInserito.AccountId != null){
                accountIds.add(contattoInserito.AccountId);
                contattiPrincipaliIds.add(contattoInserito.Id);
            }
        }
        System.debug(loggingLevel.Error, '*** contattiPrincipaliIds: ' + contattiPrincipaliIds);
        System.debug(loggingLevel.Error, '*** accountIds: ' + accountIds);

        if(!accountIds.isEmpty()){
            // disabilito il flag contatto principale su tutti i contatti principali
            List<Contact> contattiPrincipaliDaDisabilitare = [select id,MainContact__c
                                                                from contact 
                                                                where accountid in :accountIds 
                                                                    and id not in :contattiPrincipaliIds
                                                                    and MainContact__c = true];
            for(Contact contattoPrincipaleDaDisabilitare : contattiPrincipaliDaDisabilitare){
                contattoPrincipaleDaDisabilitare.MainContact__c = false;
                System.debug(loggingLevel.Error, '*** contattoPrincipaleDaDisabilitare: ' + contattoPrincipaleDaDisabilitare);
            }
            ContactTriggerHandler.skip = true;
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.update(contattiPrincipaliDaDisabilitare,dml);
            ContactTriggerHandler.skip = false;

        }
        aggiornaCodiceSapAccount(triggerNew);

        //*** CR Notifiche Informativa ***//
        InformativaUtils.setDatiInformativaOnAfterInsert(triggerNew);
        //*** CR Notifiche Informativa - Fine***//
    } 

    public void onBeforeUpdate(List<Contact> triggerOld, List<Contact> triggerNew, Map<Id, Contact> triggerOldMap, Map<Id, Contact> triggerNewMap){   

        //*** CR Consensi ***//
        checkTrattamentoDati(triggerNew);
        //*** CR Consensi - Fine ***//

        //*** CR Professional  ***//
        for(Id tempId : triggerNewMap.keySet()){
            Contact newCont = triggerNewMap.get(tempId);
            Contact oldCont = triggerOldMap.get(tempId);
            if(String.isNotBlank(newCont.Codice_IBAN__c)){
                if(String.isBlank(oldCont.Codice_IBAN__c) || !oldCont.Codice_IBAN__c.equals(newCont.Codice_IBAN__c)){
                    CheckIBAN.validateIban(newCont.Id,newCont.Codice_IBAN__c);
                }
            }else if(String.isNotBlank(oldCont.Codice_IBAN__c)){
                newCont.IBAN_Valido__c = false;
                newCont.Descrizione_Errore_IBAN__c = null;
                newCont.BIC__c = null;
                newCont.Istituto_di_Credito__c = null;
                newCont.Filiale__c = null;
                newCont.Indirizzo_Filiale__c = null;
                newCont.Nazione_Banca__c = null;
                newCont.Citta_Banca__c = null;
                newCont.Provincia_Banca__c = null;
                newCont.CAP_Banca__c = null;
            }
        }
        //*** CR Professional - Fine ***//

        // se abilito un contatto come principale devo disabilitare gli altri
        Set<Id> accountIds = new Set<Id>();
        Set<Id> contattiPrincipaliIds = new Set<Id>();
        for(Contact contattoAggiornato : triggerNew){
            System.debug(loggingLevel.Error, '*** contattoAggiornato: ' + contattoAggiornato);
            if(contattoAggiornato.MainContact__c && !triggerOldMap.get(contattoAggiornato.Id).MainContact__c && contattoAggiornato.AccountId != null){
                accountIds.add(contattoAggiornato.AccountId);
                contattiPrincipaliIds.add(contattoAggiornato.Id);
            }
        }
        System.debug(loggingLevel.Error, '*** contattiPrincipaliIds: ' + contattiPrincipaliIds);

        // disabilito il flag contatto principale su tutti i contatti principali

        Set<Id> contattiTransazione = new Set<Id>();
        // prima lo tolgo dai contatti che eventualmente sono in questa transazione

        for(Contact contattoAggiornato : triggerNew){
            if(contattoAggiornato.MainContact__c && accountIds.contains(contattoAggiornato.AccountId) && !contattiPrincipaliIds.contains(contattoAggiornato.Id)){
                contattoAggiornato.MainContact__c = false;
                contattiTransazione.add(contattoAggiornato.Id);
            }
        }
        System.debug(loggingLevel.Error, '*** contattiTransazione: ' + contattiTransazione);

        // poi da quelli extra transazione
        if(!contattiPrincipaliIds.isEmpty()){
            List<Contact> contattiPrincipaliDaDisabilitare = [select id,MainContact__c
                                                                from contact 
                                                                where accountid in :accountIds 
                                                                    and id not in :contattiPrincipaliIds
                                                                    and id not in :contattiTransazione 
                                                                    and MainContact__c = true];
            for(Contact contattoPrincipaleDaDisabilitare : contattiPrincipaliDaDisabilitare){
                System.debug(loggingLevel.Error, '*** contattoPrincipaleDaDisabilitare: ' + contattoPrincipaleDaDisabilitare);
                contattoPrincipaleDaDisabilitare.MainContact__c = false;
            }
            ContactTriggerHandler.skip = true;
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.update(contattiPrincipaliDaDisabilitare,dml);
            ContactTriggerHandler.skip = false;
        }
    }

    private void calcolaCodiceCliente(List<Contact> contacts){
        List<Contact> contatti = [SELECT Id,CodiceCliente__c,LastName,FirstName,Numero_contatto__c FROM Contact WHERE Id IN :contacts];
        for(Contact cont : contatti){
            if(cont.CodiceCliente__c == null){
                String codiceClienteRaw = cont.Numero_contatto__c + DomusUtil.getCodiceCliente(cont.Numero_contatto__c);
                cont.CodiceCliente__c =  '9' + codiceClienteRaw.leftPad(9).replaceAll(' ','0');
            }
        }
        ContactTriggerHandler.skip = true;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        Database.update(contatti,dml);
        ContactTriggerHandler.skip = false;
    }

    private void aggiornaCodiceSapAccount(List<Contact> triggerNew){
        List<string> contactFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
            contactFields.add(ft.getDescribe().getName().toLowerCase());
        }
        List<Contact> contatti = Database.query('SELECT ' + String.join(contactFields, ',') + 
                                        ' FROM Contact ' +
                                        ' WHERE Id IN : triggerNew ');
        Map<Contact,Id> contactsToUseForUpdatingAccount = new Map<Contact,Id>();
        for(Contact cont : contatti){
            if(cont.MainContact__c){
                contactsToUseForUpdatingAccount.put(cont, cont.AccountId);
            }
        }
        
        Map<Id,Account> accountsToUpdateMap = new Map<Id,Account>(
                [
                 SELECT Id, Name, Codice_Fiscale__c, Codice_Fiscale_Forzato__c, Partita_IVA__c, Codice_SAP__c, Codice_Cliente__c, Cellulare__c, Email__c, Data_Registrazione__c, 
                        Data_di_Aggiornamento__c, BillingCity, BillingState, BillingStreet, BillingCountry, BillingPostalCode, Codice_Nazione__c, 
                        Nazione__c, Cee_Nazione__c, Area_Nazione__c, Id_Area_Nazione__c, Codice_Provincia__c, Citta_abbreviato__c, SupplementoCAP__c, 
                        Dug__c, DugAbbreviataStd__c, Via__c, Civico__c, Supplemento_civico__c, Indirizzo_forzato__c, Via_Multicap__c, Presso__c, Frazione__c, Phone
                 FROM Account 
                 WHERE Id IN :contactsToUseForUpdatingAccount.values()
                 ]
        );

        List<Contact> contactToUpdate = new List<Contact>();
        Profile profile =  [SELECT ID, Name FROM Profile WHERE ID = :UserInfo.getProfileId()];
        String profilename = profile.name;
        string prof = 'Professional';
        string esb = 'ESB';
        string user = UserInfo.getName();
        System.debug('************* PROFILENAME************' + profilename);
        Set<Account> accountsToUpdate = new Set<Account>();
        for(Contact cont : contactsToUseForUpdatingAccount.keySet()){
            Account accToUpdate = accountsToUpdateMap.get(cont.AccountId);
            if(accToUpdate != null){
                System.debug('*****************user + cont.invia_anagrafica_a_SAP********* ' + user + '  +  ' + cont.Invia_anagrafica_a_sap__c);
                if(user.contains(esb) && cont.Invia_anagrafica_a_sap__c == true) {
                    System.debug('**************** SONO ENTRATO NEL TRIGGER CONT ***************');
                    cont.Invia_anagrafica_a_sap__c = false; 
                    contactToUpdate.add(cont);
                    System.debug('********** contactToUpdate ***********' + contactToUpdate);
                } else if (profilename.contains(prof)) {
                    System.debug('*************** codice cliente contatto**************' + cont.CodiceCliente__c);
                    
                    accToUpdate.Codice_Cliente__c = cont.CodiceCliente__c;
                    accToUpdate.Codice_SAP__c = cont.Codice_SAP__c;
                    /** Inizio DH-1040 **/
                    accToUpdate.Cellulare__c = cont.MobilePhone;
                    accToUpdate.Phone = cont.Phone;
                    accToUpdate.Email__c = cont.Email;
                    /** Fine DH-1040 **/

                    System.debug('*************** codice cliente account*************' + accToUpdate.Codice_Cliente__c);
                    System.debug('*************** account to update id **************' + accToUpdate.id);
                    
                    accountsToUpdate.add(accToUpdate);
                    //ContactTriggerHandler.skip = true;
                    //AccontTriggerHandler.skip = true ;
                } else{
            
                     System.debug('*****************ACCOUNT TO UPDATE **************' + accToUpdate);
            
                    accToUpdate.Codice_Fiscale__c = cont.Codice_Fiscale__c;
                    accToUpdate.Codice_Fiscale_Forzato__c = cont.Codice_Fiscale_Forzato__c;
                    accToUpdate.Partita_IVA__c = cont.Partita_IVA__c;
                    accToUpdate.Codice_SAP__c = cont.Codice_SAP__c;
                    accToUpdate.Codice_Cliente__c = cont.CodiceCliente__c;
                    accToUpdate.Cellulare__c = cont.MobilePhone; 
                    accToUpdate.Phone = cont.Phone;
                    accToUpdate.Email__c = cont.Email; 
                    accToUpdate.Data_Registrazione__c = cont.Data_Inserimento__c;
                    accToUpdate.Data_di_Aggiornamento__c = cont.Data_di_Aggiornamento__c;

                    //*** CR Consensi ***//
                    accToUpdate.Trattamento_Dati__c = cont.Trattamento_Dati__c;
                    accToUpdate.Comunicazioni_Commerciali_Marketing__c = cont.Comunicazioni_Commerciali_Marketing__c;
                    accToUpdate.Comunicazioni_Commerciali_Indirette__c = cont.Comunicazioni_Commerciali_Indirette__c;
                    accToUpdate.Cessione_a_Terzi__c = cont.Cessione_a_Terzi__c;
                    accToUpdate.Profilazione__c = cont.Profilazione__c;
                    accToUpdate.Altro_Consenso__c = cont.Altro_Consenso__c;
                    accToUpdate.Fonte__c = cont.Fonte__c;
                    //*** CR Consensi -  Fine ***//
                
                    accToUpdate.BillingCity = cont.MailingCity;
                    accToUpdate.BillingState = cont.MailingState; 
                    accToUpdate.BillingStreet = cont.MailingStreet; 
                    accToUpdate.BillingCountry = cont.MailingCountry;
                    accToUpdate.BillingPostalCode = cont.MailingPostalCode;

                    accToUpdate.Codice_Nazione__c = cont.Codice_Nazione__c;
                    accToUpdate.Nazione__c = cont.Nazione__c;
                    accToUpdate.Cee_Nazione__c = cont.Cee_Nazione__c;
                    accToUpdate.Area_Nazione__c = cont.Area_Nazione__c;
                    accToUpdate.Id_Area_Nazione__c = cont.Id_Area_Nazione__c;
                    accToUpdate.Codice_Provincia__c = cont.Codice_Provincia__c;
                    accToUpdate.Citta_abbreviato__c = cont.Citta_abbreviato__c;
                    accToUpdate.SupplementoCAP__c = cont.Supplemento_CAP__c;
                    accToUpdate.Dug__c = cont.Dug__c;
                    accToUpdate.DugAbbreviataStd__c = cont.DugAbbreviataStd__c;
                    accToUpdate.Via__c = cont.Via__c;
                    accToUpdate.Civico__c = cont.Civico__c;
                    accToUpdate.Supplemento_civico__c = cont.Supplemento_civico__c;
                    accToUpdate.Indirizzo_forzato__c = cont.Indirizzo_forzato__c;
                    accToUpdate.Via_Multicap__c = cont.Via_Multicap__c;
                    accToUpdate.Presso__c = cont.Presso__c;
                    accToUpdate.Frazione__c = cont.Frazione__c;
                    accToUpdate.Codice_Cliente__c = cont.CodiceCliente__c;
                    accToUpdate.Codice_Esterno__c = cont.Codice_Esterno__c;
                    accToUpdate.Codice_Press_Di__c = cont.Codice_Press_Di__c;
                    accToUpdate.Codice_ACI__c = cont.Codice_ACI__c;
                    accToUpdate.Data_di_nascita__c = cont.Birthdate;
                    accToUpdate.Canale_di_Comunicazione_Preferito__c = cont.Canale_di_Comunicazione_Preferito__c;
                    accToUpdate.Luogo_di_Nascita__c = cont.Luogo_di_Nascita__c;
                    accToUpdate.Professione__c = cont.Professione__c;
                    accToUpdate.PEC__c = cont.PEC_Referente__c;
                    accToUpdate.Ruolo__c = cont.Ruolo__c;
                    accToUpdate.Altra_email__c = cont.Altra_email_referente__c;
                    accToUpdate.Comunicazioni_Commerciali_Marketing__c  = cont.Comunicazioni_Commerciali_Marketing__c;
                    System.debug ('*******************CODICE CLIENTE CONT*********' + cont.CodiceCliente__c);
                    accountsToUpdate.add(accToUpdate);
                }   
            }
        }

        fromContactTrigger = true;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        Database.update(new List<Account>(accountsToUpdate),dml);
        Database.update(new List<Contact>(contactToUpdate),dml);
        fromContactTrigger = false;
    }

    public void onAfterUpdate(List<Contact> triggerOld, List<Contact> triggerNew, Map<Id, Contact> triggerOldMap, Map<Id, Contact> triggerNewMap){   

        calcolaCodiceCliente(triggerNew);
        aggiornaCodiceSapAccount(triggerNew);

        //*** CR Notifiche Informativa ***//
        InformativaUtils.setDatiInformativaOnAfterUpdate(triggerNew, triggerOldMap);
        //*** CR Notifiche Informativa - Fine ***//

        //List<Id> contactsIdList = new List<Id>();
        List<Id> assetContactIdlist = new List<Id>();
        List<Id> entitlementContactIdList = new List<Id>();
        List<Id> avvisoContactIdList = new List<Id>();
        List<Id> zuoraContactIdList = new List<Id>();

        Utils.TriggerUtils triggerUtils = new Utils.TriggerUtils(triggerNew, triggerOldMap);
        List<String> assetCheckFields = new List<String>{'MailingStreet','MailingPostalCode', 'MailingCity', 'Id_Area_Nazione__c', 'Name', 
                                                        'MailingCountry', 'MailingState', 'Area_Nazione__c', 'Presso__c', 'DugAbbreviataStd__c', 'Dug__c', 'Via__c', 'Civico__c', 'Supplemento_Civico__c', 'Frazione__c'};
        List<Contact>assetContactList = (List<Contact>) triggerUtils.getChanged(assetCheckFields);

        List<String> entitlementCheckFields = new List<String>{'MailingPostalCode','MailingCity', 'MailingStreet', 'Nazione__c', 'Phone', 'LastName', 'FirstName', 'MobilePhone'};
        List<Contact> entitlementContactList = (List<Contact>) triggerUtils.getChanged(entitlementCheckFields);

        List<String> avvisoCheckFields = new List<String>{'Titolo__c','Titolo_Cod__c', 'Professione__c', 'Sesso__c', 'LastName', 'FirstName', 'DugAbbreviataStd__c', 
                                                        'Via__c', 'Civico__c', 'Supplemento_Civico__c', 'MailingPostalCode', 'Supplemento_CAP__c', 'MailingCity', 'Codice_Provincia__c',
                                                        'Nazione__c', 'AccountId', 'Codice_Nazione__c', 'Presso__c' };
        List<Contact> avvisoContactList = (List<Contact>) triggerUtils.getChanged(avvisoCheckFields);

        List<String> zuoraCheckFields = new List<String>{'MailingStreet', 'MailingCity', 'MailingCountry' ,'FirstName' ,'LastName', 'Civico__c', 'MailingPostalCode', 'MailingState',
                                                        'Email', 'Phone', 'MobilePhone'};
        List<Contact> zuoraContactList = (List<Contact>) triggerUtils.getChanged(zuoraCheckFields);
        System.debug(loggingLevel.Error, '*** zuoraContactList: ' + zuoraContactList);

        for(Contact cont : assetContactList){
            assetContactIdlist.add(cont.Id);
        }
        for(Contact cont : entitlementContactList){
            entitlementContactIdList.add(cont.Id);
        }
        for(Contact cont : avvisoContactList){
            avvisoContactIdList.add(cont.Id);
        }
        for(Contact cont : zuoraContactList){
            zuoraContactIdList.add(cont.Id);
        }
        
        if(!System.isBatch() && !System.isFuture()){
            ContactUtil.updateContactRelatedObjects(assetContactIdlist, entitlementContactIdList, avvisoContactIdList, zuoraContactIdList);
        }
    }

    public void onBeforeDelete(List<Contact> triggerOld, Map<Id, Contact> triggerOldMap){  

    }

    public void onAfterDelete(List<Contact> triggerOld, Map<Id, Contact> triggerOldMap){  
        for(Contact c : triggerOld){
            if(c.MainContact__c){
                c.MainContact__c.addError('Non è possibile eliminare un contatto principale');
            }
        }        
    }

    //*** CR Consensi ***//
    private void setFieldsOnBeforeInsert(List<Contact> triggerNew){
        setFonteLead(triggerNew);
        checkTrattamentoDati(triggerNew);
    }

    private void checkTrattamentoDati(List<Contact> triggerNew){
        for(Contact newContact : triggerNew){
            if(newContact.Trattamento_Dati__c == AppConstants.NO){
                newContact.Comunicazioni_Commerciali_Marketing__c = AppConstants.NO;
                newContact.Comunicazioni_Commerciali_Indirette__c = AppConstants.NO;
                newContact.Cessione_a_Terzi__c = AppConstants.NO;
                newContact.Profilazione__c = AppConstants.NO;
                newContact.Altro_Consenso__c = AppConstants.NO;
            }
        } 
    }

    private void setFonteLead(List<Contact> triggerNew){
        for(Contact newContact : triggerNew){
            if(newContact.Creato_da_Lead__c){
                newContact.Fonte__c = 'Web To Lead';
                newContact.Data_di_Aggiornamento__c = newContact.Data_Inserimento__c;
            }
        } 
    }
    //*** CR Consensi - Fine ***//

    private void setMainContactFromLead(List<Contact> triggerNew){

        List<Contact> creatiDaLead = filterByLeadConversion(triggerNew);

        if(!creatiDaLead.isEmpty()){

            Set<Id> accountIds = new Set<Id>();
            for(Contact newContact : creatiDaLead){
                accountIds.add(newContact.AccountId);
            }

            Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, (SELECT Id FROM Contacts WHERE MainContact__c = true) FROM Account WHERE Id IN :accountIds]);
            for(Contact newContact : creatiDaLead){
                if(accountMap.containsKey(newContact.AccountId)){
                    Account parentAccount = accountMap.get(newContact.AccountId);
                    if(parentAccount.Contacts.isEmpty()){
                        newContact.MainContact__c = true;
                    }else{
                        newContact.MainContact__c = false;
                    }
                }
            }
        }
       
    }

    private List<Contact> filterByLeadConversion(List<Contact> triggerNew) {

        List<Contact> fromLeadConversion = new List<Contact>();

        for(Contact newContact : triggerNew) {
            if(newContact.Creato_da_Lead__c && newContact.AccountId != null) {
                fromLeadConversion.add(newContact);
            }
        }

        return fromLeadConversion;
    }
}