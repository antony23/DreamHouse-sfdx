public class AccountCampaignHistoryExtension {
    
    public CampaignMember[] members {get;set;}
    public Integer l {get;private set;}
    
      public AccountCampaignHistoryExtension(ApexPages.StandardController controller) {
        if(ApexPages.CurrentPage().getParameters().get('showAll') == '1'){
            l = 1000;
        }else{
            l = 3;
        }
        members = [Select Id,Contact.Name,Campaign.Name,Status,
                   			Campaign.Type,Campaign.StartDate,Campaign.EndDate, Campaign.Rate_Plan_in_Campagna__r.Name, Campaign.Rate_Plan_in_Campagna__c
                   From CampaignMember 
                   Where Contact.AccountId =: controller.getId()
                   Order By CreatedDate Desc Limit : l];

    }
}