@isTest (SeeAllData=true)
private class RestCalendarCalculatorTest{
	
	@isTest
	static void testM(){

		RESTCalendarCalculator.RestObject res = RESTCalendarCalculator.getSubscriptionData(null);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'L\'oggetto ricevuto è vuoto');

		RESTCalendarCalculator.RestObject request = new RESTCalendarCalculator.RestObject();
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun abbonamento o avviso di rinnovo ricevuto');

		request.subscriptionData = new RESTCalendarCalculator.SubscriptionData();
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun rateplan ricevuto');

		RESTCalendarCalculator.RatePlanData rp = new RESTCalendarCalculator.RatePlanData();
		rp.ratePlan.productRatePlanId = 'fake id';
		request.subscriptionData.ratePlanDataList.add(rp);
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun ratePlan trovato');

		request.subscriptionData.ratePlanDataList = new List<RESTCalendarCalculator.RatePlanData>();
		for(zqu__ProductRatePlan__c rp1 : [SELECT Id,zqu__ZuoraId__c 
											FROM zqu__ProductRatePlan__c 
											WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
											LIMIT 2]){
			RESTCalendarCalculator.RatePlanData rpd = new RESTCalendarCalculator.RatePlanData();
			rpd.ratePlan.productRatePlanId = rp1.zqu__ZuoraId__c;
			request.subscriptionData.ratePlanDataList.add(rpd);
		}
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'E\' possibile inserire solo un rateplan con charge di tipo abbonamento all\'interno dell\'abbonamento');

		request.subscriptionData.ratePlanDataList = new List<RESTCalendarCalculator.RatePlanData>();
		for(zqu__ProductRatePlan__c rp2 : [SELECT Id,zqu__ZuoraId__c 
											FROM zqu__ProductRatePlan__c 
											WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
											AND Testata__c = 'QUATTRORUOTE'
											LIMIT 1]){
			RESTCalendarCalculator.RatePlanData rpd = new RESTCalendarCalculator.RatePlanData();
			rpd.ratePlan.productRatePlanId = rp2.zqu__ZuoraId__c;
			//rp.ratePlan.rivista
			//rp.ratePlan.codiceRivista
			request.subscriptionData.ratePlanDataList.add(rpd);
		}

		request.accountData = new RESTCalendarCalculator.AccountData();
		request.accountData.billingAccountId = 'fake id';
		//String crmId;
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Impossibile trovare il billing account');

		request.accountData = new RESTCalendarCalculator.AccountData();
		for(Zuora__CustomerAccount__c billing : [SELECT Id, Sold_To_Salesforce__c,Zuora__Account__c,Sold_To_Salesforce__r.AccountId,Zuora__Zuora_Id__c
												FROM Zuora__CustomerAccount__c 
												WHERE Sold_To_Salesforce__c != null
												LIMIT 1]){
			request.accountData.billingAccountId = billing.Zuora__Zuora_Id__c;
		}
		request.intermediarioData = new RESTCalendarCalculator.IntermediarioData();
		request.intermediarioData.billingAccountId = 'fake id';
		res = RESTCalendarCalculator.getSubscriptionData(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Impossibile trovare il billing account dell\'intermediario');

		request.intermediarioData = new RESTCalendarCalculator.IntermediarioData();
		Zuora__CustomerAccount__c intermediario = [SELECT Id, Zuora__Account__c, Sold_To_Salesforce__c,Zuora__Zuora_Id__c 
													FROM Zuora__CustomerAccount__c 
                                                    WHERE Zuora__Account__r.Type = 'Intermediario' 
                                                    LIMIT 1];
        request.intermediarioData.billingAccountId = intermediario.Zuora__Zuora_Id__c;
        res = RESTCalendarCalculator.getSubscriptionData(request);

        request.subscriptionData = null;
        request.avvisoDiRinnovo = new RESTCalendarCalculator.AvvisoDiRinnovo();
        List<Avviso_di_Rinnovo__c> avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__c = null LIMIT 1];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RESTCalendarCalculator.getSubscriptionData(request);
        }
        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Stato_Sfdc__c = 'Rinnovata' LIMIT 1];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RESTCalendarCalculator.getSubscriptionData(request);
        }
        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c != 'Active' AND Abbonamento__r.Zuora__Status__c != 'Expired' LIMIT 1];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RESTCalendarCalculator.getSubscriptionData(request);
        }
        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c = 'Active' AND Abbonamento__r.Mantieni_Offerta__c = 'SI' LIMIT 1];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RESTCalendarCalculator.getSubscriptionData(request);
        }
        
        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c = 'Active' AND Abbonamento__r.Mantieni_Offerta__c != 'SI' AND Italia_Estero__c = 'Italia' LIMIT 1];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RESTCalendarCalculator.getSubscriptionData(request);
        }
       
        
	}

}