@isTest
public class ZTest_BillingAccountUtil {

	@isTest
	static void invokeCreateBillingAccount() {
	
        zqu__Quote__c q = [SELECT Id FROM zqu__Quote__c WHERE Name = 'TestQuote'];
        zqu__Quote__c fetchedQuote = BillingAccountUtil.fetchQuote(q.Id);
        Contact c = [SELECT Id FROM Contact WHERE Name = 'TestContact'];
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockTestClass('CreateAccount'));
		BillingAccountUtil.ResponseMessage zuoraResponse = BillingAccountUtil.createBillingAccount(fetchedQuote, c.Id, c.Id);
		Test.stopTest();
        
		System.assertEquals(true, zuoraResponse.success);
	}

    @isTest
    static void invokeCreateSubscriptionPreview() {
    
        zqu__Quote__c q = [SELECT Id FROM zqu__Quote__c WHERE Name = 'TestQuote'];
        zqu__Quote__c fetchedQuote = BillingAccountUtil.fetchQuote(q.Id);
        Contact c = [SELECT Id FROM Contact WHERE Name = 'TestContact'];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockTestClass('Subscribe'));
        List<QuoteDetailPreviewUtil.ResponseWrapper> zuoraResponses = QuoteDetailPreviewUtil.createSubscriptionPreview(fetchedQuote.Id, c.Id, c.Id);
        Test.stopTest();
        
        System.assertEquals(true, zuoraResponses[0].success);
    }

	@testSetup
	public static void testSetup() {

		Account a = new Account(
			Name = 'TestAccount'
		);
        insert a;

        Contact c = new Contact(
        	AccountId = a.Id, 
        	LastName = 'TestContact', 
        	Email = 'email@test.it',
            Codice_Nazione__c = 'IT'
        );
        insert c;

        zqu__Quote__c zuoraQuote = new zqu__Quote__c(
            Name = 'TestQuote',
            zqu__Account__c = a.Id,
        	zqu__Currency__c = 'EUR',
			zqu__BillCycleDay__c = '28',
            zqu__BillingBatch__c = 'batch1',
			zqu__PaymentTerm__c = 'Due Upon Receipt',
        	zqu__BillingMethod__c = 'Email'
        ); 
        insert zuoraQuote;

        zqu__QuoteAmendment__c quoteAmendment = new zqu__QuoteAmendment__c(
            Name = 'TestQuoteAmendment',
            zqu__Quote__c = zuoraQuote.Id
        );
        insert quoteAmendment;

        zqu__QuoteRatePlan__c zuoraQuoteRatePlan = new zqu__QuoteRatePlan__c(
            Name = 'TestQuoteRatePlan',
            zqu__Quote__c = zuoraQuote.Id,
            zqu__ProductRatePlanZuoraId__c = 'testZuoraId',
            zqu__QuoteAmendment__c = quoteAmendment.Id
        );
        insert zuoraQuoteRatePlan;

        zqu__QuoteRatePlanCharge__c zuoraQuoteRatePlanCharge = new zqu__QuoteRatePlanCharge__c(
            Name = 'TestQuoteRatePlanCharge',
            zqu__QuoteRatePlan__c  = zuoraQuoteRatePlan.Id
        );
        insert zuoraQuoteRatePlanCharge;

        Zuora_Credential__c createAccount = new Zuora_Credential__c(
            Name = 'CreateAccount',
        	EndPoint__c = 'testEndPoint',
            apiAccessKeyId__c = 'testApiAccessKeyId',
            apiSecretAccessKey__c = 'testApiSecretAccessKey'
        );

        Zuora_Credential__c subscribe = new Zuora_Credential__c(
            Name = 'Subscribe',
            EndPoint__c = 'testEndPoint',
            apiAccessKeyId__c = 'testApiAccessKeyId',
            apiSecretAccessKey__c = 'testApiSecretAccessKey'
        );

        List<Zuora_Credential__c> zuoraCredentials = new List<Zuora_Credential__c>{createAccount, subscribe};
        insert zuoraCredentials;
	}

}