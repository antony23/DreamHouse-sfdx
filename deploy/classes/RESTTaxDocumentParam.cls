@RestResource(urlMapping='/getTaxDocumentParams/*')
global with sharing class RESTTaxDocumentParam{

    @HttpPost
    global static List<restObject> getTaxDocumentParams(List<RestObject> restObject){
    
    	Set<Id> accountIdSet = new Set<Id>();
    	Set<Id> contactIdSet = new Set<Id>();
    	for(RestObject rObj : restObject){
    		RestRequest r = rObj.restRequest;
    		contactIdSet.add(r.soldToContactId);
    		contactIdSet.add(r.billToContactId);
    	}

    	Map<Id, Contact> contactMap = new Map<Id,Contact>([SELECT Id, Codice_Nazione__c, Account.RecordType.DeveloperName, Cee_Nazione__c FROM Contact WHERE Id = :contactIdSet]);

    	for(RestObject rObj : restObject){
    		RestRequest r = rObj.restRequest;
    		RestResponse restResponse = new RestResponse();
    		try{
	        	
				Contact billToContact = contactMap.get(r.billToContactId);
				if(billToContact == null){
					rObj.isError = true;
					rObj.errorMsg = 'Referente di fatturazione non trovato';
					continue;
				}

				restResponse.billToCounty = getCounty(billToContact.Account.RecordType.DeveloperName, billToContact, r.splitPayment);
				rObj.restResponse = restResponse;
				
    		} catch(Exception e){
    			rObj.isError = true;
    			rObj.errorMsg = e.getMessage();
    		}

        }

		return restObject;
	}

	private static String getCounty(String recordType, Contact billToContact, String splitPayment){
		String tipoCliente;
        if(splitPayment == 'SI'){
            tipoCliente = 'PUBBLICA_AMMINISTRAZIONE';
        }else{
            if(recordType.containsIgnoreCase('B2C')){
                tipoCliente = 'B2C';
            }else if (recordType.containsIgnoreCase('B2B')){
                tipoCliente = 'B2B';
            }else{
               	return null;
            }
        }

        String country;
        if(billToContact.Codice_Nazione__c == 'IT'){
            country = 'ITALY';
        }else if(billToContact.Codice_Nazione__c ==  'VA' || billToContact.Codice_Nazione__c == 'SM'){
            country = 'SAN_MARINO_VAT';
        }else{
            country = billToContact.Cee_Nazione__c;
        }

        String soldToCounty = country +'_'+tipoCliente;
        return soldToCounty;
	}


     global class RestObject{
        public boolean isError = false;
        public String errorMsg;
        public RestRequest restRequest;
        public RestResponse restResponse;
    }

    global class RestRequest{
    	public Id soldToContactId;
    	public Id billToContactId;
    	public String splitPayment;
    }

    global class RestResponse{
    	public String billToCounty;
    }

}