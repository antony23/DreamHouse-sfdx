@isTest(SeeAllData=true)
global class invokeBillingAccountMockCallout implements HttpCalloutMock {

    String outcome;

    public invokeBillingAccountMockCallout(){

    }

    public invokeBillingAccountMockCallout(String outcome){
        this.outcome = outcome;
    }

    global HTTPResponse respond(HTTPRequest restRequest) {

        Rest_Request_Setting__mdt[] credenzialiCallout = [  SELECT apiAccessKeyId__c, apiSecretAccessKey__c, invoiceTemplateId__c, ContentType__c, EndPoint__c, Method__c
                                                            FROM Rest_Request_Setting__mdt 
                                                            WHERE MasterLabel = :System.URL.getSalesforceBaseUrl().getHost()]; 

        if(this.outcome == 'OK') {
        System.assertEquals(credenzialiCallout[0].EndPoint__c, restRequest.getEndpoint());
        System.assertEquals('POST', restRequest.getMethod());

        // scrivo la fake response per carta di credito;

        HttpResponse restResponseOK = new HttpResponse();
        restResponseOK.setHeader('Content-Type', 'application/json');
        restResponseOK.setBody('{ "success": true, "accountId": "2c92c0fa5c38feda015c3ecc60f90c1b", "accountNumber": "A00003036", "paymentMethodId": "2c92c0fa5c38feda015c3ecc64550c23"  }');
        restResponseOK.setStatusCode(200);
        return restResponseOK;
        } else {// specifico l'endpoint e il metodo
        System.assertEquals(credenzialiCallout[0].EndPoint__c, restRequest.getEndpoint());
        System.assertEquals('POST', restRequest.getMethod());

        // scrivo la fake response per carta di credito;

        HttpResponse restResponseError = new HttpResponse();
        restResponseError.setHeader('Content-Type', 'application/json');
        restResponseError.setBody('{ "success": false, "processId": "803ABCE181AA19A8", "reasons": [ { "code": 51000060, "message": "Transaction declined.<br>incorrect_number - Your card number is incorrect." }]}');
        restResponseError.setStatusCode(200);
        return restResponseError;}
    }
  
}