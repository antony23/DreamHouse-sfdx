@isTest(SeeAllData=true)
private class invokeBillingAccountTest {
	
	@isTest static void test_method_OK() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new invokeBillingAccountMockCallout('OK'));
		Account acct = new Account(Name = 'Zuora Test Account');
		insert acct;
		Contact ct = new Contact(FirstName ='John', LastName = 'Smith', MailingCountry = 'Italy', accountId = acct.Id);
		insert ct;
		Billing_Account__c billAcct = new Billing_Account__c(  Currency__c ='EUR', BillCycleDay__c = 1, AutoPay__c = false, Card_Type__c = 'Visa', 
																Expiration_Month__c = 2 , 
																Expiration_Year__c = 2018, SecurityCodeEnc__c ='111',
																Email__c = true,
																Print__c = true,
																CardNumberEnc__c = '4111111111111111', Bill_To_Contact__c = ct.Id, Account__c = acct.Id, Metodo_di_Pagamento__c = 'Carta di credito', Sold_To_Contact__c = ct.Id);
		
		insert billAcct;
		Test.stopTest();


		billAcct = [SELECT Id,Account__r.Name, Currency__c, Card_Type__c, CardNumberEnc__c, Expiration_Month__c, Expiration_Year__c,
                        SecurityCodeEnc__c, Metodo_di_Pagamento__c, Bill_To_Contact__r.FirstName, Bill_To_Contact__r.LastName,
                        Bill_To_Contact__r.MailingStreet, Bill_To_Contact__r.MailingCity, Bill_To_Contact__r.MailingCountry, 
                        Bill_To_Contact__r.MailingPostalCode, Bill_To_Contact__r.MailingState, Bill_To_Contact__r.Email, 
                        Bill_To_Contact__r.Phone, Bill_To_Contact__r.MobilePhone, Sold_To_Contact__r.FirstName, Sold_To_Contact__r.LastName,
                        Sold_To_Contact__r.MailingStreet, Sold_To_Contact__r.MailingCity, Sold_To_Contact__r.MailingCountry, 
                        Sold_To_Contact__r.MailingPostalCode, Sold_To_Contact__r.MailingState, Sold_To_Contact__r.Email, 
                        Sold_To_Contact__r.Phone, Sold_To_Contact__r.MobilePhone, Batch__c, Account__r.Id, Bill_To_Contact__c, Sold_To_Contact__c,
                        Account__r.Codice_Cliente__c, Sold_To_Contact__r.Supplemento_Civico__c, Account__r.Partita_IVA__c, Account__r.Codice_Fiscale__c, 
                        Account__r.Codice_Pubblica_Amministrazione__c, SalvareDatiCartadiCredito__c, Sold_To_Contact__r.Supplemento_CAP__c,
                        Bill_To_Contact__r.Titolo__c, PaymentGateway__c, PaymentTerm__c, AutoPay__c, BillCycleDay__c, ResponseJSON__c, IsError__c, ErrorMsg__c       
                        FROM Billing_Account__c 
                        WHERE Id = :billAcct.Id ];

        string restResponseOK = '{ "success": true, "accountId": "2c92c0fa5c38feda015c3ecc60f90c1b", "accountNumber": "A00003036", "paymentMethodId": "2c92c0fa5c38feda015c3ecc64550c23"  }';
		System.assertEquals(restResponseOK,billAcct.ResponseJSON__c);
		System.assertEquals(billAcct.IsError__c, false);
		System.assertEquals(billAcct.ErrorMsg__c, null);

	}

	@isTest static void test_method_Error() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new invokeBillingAccountMockCallout());
		Account acct = new Account(Name = 'Zuora Test Account');
		insert acct;
		Contact ct = new Contact(FirstName ='John', LastName = 'Smith', MailingCountry = 'Italy', accountId = acct.Id);
		insert ct;
		Billing_Account__c billAcct = new Billing_Account__c(  Currency__c ='EUR', BillCycleDay__c = 1, AutoPay__c = false, Card_Type__c = 'Visa', 
																Expiration_Month__c = 2 , 
																Email__c = true,
																Print__c = true,
																Expiration_Year__c = 2018, SecurityCodeEnc__c ='111',
																CardNumberEnc__c = '411111111111111', Bill_To_Contact__c = ct.Id, Account__c = acct.Id, Metodo_di_Pagamento__c = 'Carta di credito', Sold_To_Contact__c = ct.Id);
		
		insert billAcct;
		Test.stopTest();


		billAcct = [SELECT Id,Account__r.Name, Currency__c, Card_Type__c, CardNumberEnc__c, Expiration_Month__c, Expiration_Year__c,
                        Metodo_di_Pagamento__c, SecurityCodeEnc__c, Bill_To_Contact__r.FirstName, Bill_To_Contact__r.LastName,
                        Bill_To_Contact__r.MailingStreet, Bill_To_Contact__r.MailingCity, Bill_To_Contact__r.MailingCountry, 
                        Bill_To_Contact__r.MailingPostalCode, Bill_To_Contact__r.MailingState, Bill_To_Contact__r.Email, 
                        Bill_To_Contact__r.Phone, Bill_To_Contact__r.MobilePhone, Sold_To_Contact__r.FirstName, Sold_To_Contact__r.LastName,
                        Sold_To_Contact__r.MailingStreet, Sold_To_Contact__r.MailingCity, Sold_To_Contact__r.MailingCountry, 
                        Sold_To_Contact__r.MailingPostalCode, Sold_To_Contact__r.MailingState, Sold_To_Contact__r.Email, 
                        Sold_To_Contact__r.Phone, Sold_To_Contact__r.MobilePhone, Batch__c, Account__r.Id, Bill_To_Contact__c, Sold_To_Contact__c,
                        Account__r.Codice_Cliente__c, Sold_To_Contact__r.Supplemento_Civico__c, Account__r.Partita_IVA__c, Account__r.Codice_Fiscale__c, 
                        Account__r.Codice_Pubblica_Amministrazione__c, SalvareDatiCartadiCredito__c, Sold_To_Contact__r.Supplemento_CAP__c,
                        Bill_To_Contact__r.Titolo__c, PaymentGateway__c, PaymentTerm__c, AutoPay__c, BillCycleDay__c, ResponseJSON__c, IsError__c, ErrorMsg__c       
                        FROM Billing_Account__c 
                        WHERE Id = :billAcct.Id ];

        string restResponseError = '{ "success": false, "processId": "803ABCE181AA19A8", "reasons": [ { "code": 51000060, "message": "Transaction declined.<br>incorrect_number - Your card number is incorrect." }]}';
		System.assertEquals(restResponseError,billAcct.ResponseJSON__c);
		System.assertEquals(billAcct.IsError__c, true);
		System.assertEquals(billAcct.ErrorMsg__c, 'Transaction declined.<br>incorrect_number - Your card number is incorrect.');


	}
	
	@isTest static void test_method_OKnocredit() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new invokeBillingAccountMockCallout('OK'));
		Account acct = new Account(Name = 'Zuora Test Account');
		insert acct;
		Contact ct = new Contact(FirstName ='John', LastName = 'Smith', MailingCountry = 'Italy', accountId = acct.Id);
		insert ct;
		Billing_Account__c billAcct = new Billing_Account__c(  Currency__c ='EUR', BillCycleDay__c = 1, AutoPay__c = false, Metodo_di_Pagamento__c = 'Paypal - Internet' , Email__c = true,
																Print__c = true, Bill_To_Contact__c = ct.Id, Account__c = acct.Id, Sold_To_Contact__c = ct.Id);
		
		insert billAcct;
		Test.stopTest();


		billAcct = [SELECT Id,Account__r.Name, Currency__c, Card_Type__c, CardNumberEnc__c, Expiration_Month__c, Expiration_Year__c,
                        Metodo_di_Pagamento__c, SecurityCodeEnc__c, Bill_To_Contact__r.FirstName, Bill_To_Contact__r.LastName,
                        Bill_To_Contact__r.MailingStreet, Bill_To_Contact__r.MailingCity, Bill_To_Contact__r.MailingCountry, 
                        Bill_To_Contact__r.MailingPostalCode, Bill_To_Contact__r.MailingState, Bill_To_Contact__r.Email, 
                        Bill_To_Contact__r.Phone, Bill_To_Contact__r.MobilePhone, Sold_To_Contact__r.FirstName, Sold_To_Contact__r.LastName,
                        Sold_To_Contact__r.MailingStreet, Sold_To_Contact__r.MailingCity, Sold_To_Contact__r.MailingCountry, 
                        Sold_To_Contact__r.MailingPostalCode, Sold_To_Contact__r.MailingState, Sold_To_Contact__r.Email, 
                        Sold_To_Contact__r.Phone, Sold_To_Contact__r.MobilePhone, Batch__c, Account__r.Id, Bill_To_Contact__c, Sold_To_Contact__c,
                        Account__r.Codice_Cliente__c, Sold_To_Contact__r.Supplemento_Civico__c, Account__r.Partita_IVA__c, Account__r.Codice_Fiscale__c, 
                        Account__r.Codice_Pubblica_Amministrazione__c, SalvareDatiCartadiCredito__c, Sold_To_Contact__r.Supplemento_CAP__c,
                        Bill_To_Contact__r.Titolo__c, PaymentGateway__c, PaymentTerm__c, AutoPay__c, BillCycleDay__c, ResponseJSON__c, IsError__c, ErrorMsg__c       
                        FROM Billing_Account__c 
                        WHERE Id = :billAcct.Id ];

        string restResponseOK = '{ "success": true, "accountId": "2c92c0fa5c38feda015c3ecc60f90c1b", "accountNumber": "A00003036", "paymentMethodId": "2c92c0fa5c38feda015c3ecc64550c23"  }';
		System.assertEquals(restResponseOK,billAcct.ResponseJSON__c);
		System.assertEquals(billAcct.IsError__c, false);
		System.assertEquals(billAcct.ErrorMsg__c, null);

	}


	
}