public with sharing class DeleteAvvisiFromRegolaController {
    public AsyncApexJob  aaj {get;set;}
    public void run() {
    	Id regolaId = Id.valueOf(ApexPages.currentPage().getParameters().get('regolaId'));
        id sfdcJobID = Database.executeBatch(new DeleteAvvisiFromRegolaBatch(regolaId), 200);
        aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: sfdcJobID ];     
    }
    
    public void getJobStatus(){
        aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: aaj.id];
    }

    public String resultMessage{
    	get{
	    	Document documentResult = [SELECT Id,Body FROM Document WHERE folderId = :UserInfo.getUserId() AND Name LIKE 'Log_DeleteAvvisiFromRegola_%' LIMIT 1];
	    	Blob body = documentResult.body;
	    	return String.valueOf(body.toString());
    	}
    	private set;
    }
}