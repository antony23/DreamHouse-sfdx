@isTest
private class ZTest_LeadTriggerHandler {
	
	@isTest
	static void insertLead(){
		Test.startTest();
		List<Lead> listaLead = new List<Lead>{
			new Lead(FirstName = 'Lead1', LastName = 'Test', Codice_Agente__c = '001', Trattamento_Dati__c = 'SI'),
			new Lead(FirstName = 'Lead2', LastName = 'Test', Codice_Agente__c = '002', Trattamento_Dati__c = 'SI'),
			new Lead(FirstName = 'Lead3', LastName = 'Test', Codice_Agente__c = '003', Trattamento_Dati__c = 'SI'),
			new Lead(FirstName = 'Lead4', LastName = 'Test', Trattamento_Dati__c = 'SI')
		};
            
		List<Database.SaveResult> srList = Database.insert(listaLead);

		Integer successInsert = 0;
		for(Database.SaveResult sr : srList){
			if(sr.isSuccess()){
				successInsert++;
			}
		}
		
		System.assertEquals(successInsert, srList.size());
	}

	@testSetup
	public static void testSetup(){
		Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Standard User'];
		String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
		List<User> listaUser = new List<User>{
			new User(	Alias = 'newUser1',
						Email = 'newuser1@testorg.com',
						EmailEncodingKey = 'UTF-8', 
						LastName = 'Testing', 
						LanguageLocaleKey = 'en_US',
						LocaleSidKey = 'en_US', 
						ProfileId = p.Id,
						TimeZoneSidKey = 'America/Los_Angeles', 
						UserName = 'standarduser1' + DateTime.now().getTime() + '@testorg.com',
						EmployeeNumber = '001'),
			new User(	Alias = 'newUser2',
						Email = 'newuser2@testorg.com',
						EmailEncodingKey = 'UTF-8', 
						LastName = 'Testing', 
						LanguageLocaleKey = 'en_US',
						LocaleSidKey = 'en_US', 
						ProfileId = p.Id,
						TimeZoneSidKey = 'America/Los_Angeles', 
						UserName = 'standarduser2' + DateTime.now().getTime() + '@testorg.com',
						EmployeeNumber = '002')
		};
		insert listaUser;
	}

}