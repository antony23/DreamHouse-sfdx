global class KPIVenditeDiretteClienteBatch implements Database.Batchable<sObject>,Schedulable {
    
    // Schedulable
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new KPIVenditeDiretteClienteBatch(),100);
    }

    // Batch    
    String query;
    
    global KPIVenditeDiretteClienteBatch() {
        query = 'SELECT Id FROM Zuora__SubscriptionProductCharge__c WHERE Tipo_Prodotto__c = \'Vendita Diretta\' AND createddate >= LAST_N_MONTHS:12 ORDER BY Zuora__Account__c ASC';
        if(Test.isRunningTest()) query += ' LIMIT 100';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Account> accountsKPI = new List<Account>();
        for(AggregateResult totaleAccount : [SELECT Zuora__Account__c, SUM(Zuora__Quantity__c) totaleAcquisti
                                            FROM Zuora__SubscriptionProductCharge__c
                                            WHERE Id IN :scope
                                            AND Tipo_Prodotto__c = 'Vendita Diretta'
                                            GROUP BY Zuora__Account__c]){
            accountsKPI.add(new Account( 
                Id = (Id) totaleAccount.get('Zuora__Account__c'), 
                Vendite_Dirette__c = (Decimal) totaleAccount.get('totaleAcquisti')
            ));
        }

        AccountTriggerHandler.skip = true;
        update accountsKPI;
        AccountTriggerHandler.skip = false;

    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}