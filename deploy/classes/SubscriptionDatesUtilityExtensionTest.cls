@isTest (SeeAllData=true)
private class SubscriptionDatesUtilityExtensionTest{
	
	@isTest
	static void testM(){
		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c sub = new Zuora__Subscription__c(Name = 'Test', Zuora__SubscriptionStartDate__c = Date.today().addYears(-1), Zuora__SubscriptionEndDate__c = Date.today(), Zuora__RenewalTerm__c = '48 Weeks', Zuora__TermStartDate__c = Date.today());
		insert sub;

		AccountTriggerHandler.skip = true;
		Account a = new Account(Name = 'Test');
		insert a;

		Calendario__c cal = [SELECT Id FROM Calendario__c WHERE Testata__r.Name = 'QUATTRORUOTE' AND Data_Uscita__c > TODAY LIMIT 1];

		Asset copia = new Asset(Name = 'Test', AccountId = a.Id, SubscriptionAllCopy__c = sub.Id, Calendario__c = cal.Id);
		insert copia;

		SubscriptionDatesUtilityExtension ext = new SubscriptionDatesUtilityExtension(new ApexPages.StandardController(sub));
		ext.goBack();

		System.debug(ext.copie);
		System.debug(ext.copieDaSospendere);
		System.debug(ext.primaCopiaDaSospendere);
		System.debug(ext.copieOptions);
		System.debug(ext.copieDaSospendereOptions);
		System.debug(ext.startDateSosp);
		System.debug(ext.endDateSosp);
		System.debug(ext.numDaysSosp);
		
		ext.copieDaSospendere = '1';
		ext.primaCopiaDaSospendere = cal.Id;
		ApexPages.currentPage().getParameters().put('debug','1');
		ext.calculate();
		ext.suspend();
	}
}