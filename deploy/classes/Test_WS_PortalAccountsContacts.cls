@isTest
private class Test_WS_PortalAccountsContacts {
		
		@isTest static void testEerroreGenericoAccount(){

			User utente = [SELECT Id FROM User  WHERE profile.name = 'Admin - customer portal' LIMIT 1];
			System.runAs(utente){
			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request request = new WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request();

			request.elements = new List<WS_PortalAccountsContacts.InputElement>();
			Account account = new Account ( 
														Name = 'Lino Banfi',
						    							Email__c='banfi.lino@email.com',
						    							Codice_Fiscale__c = 'BNFLNO91T04E435K',
						    							RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId()
						    							
						    							);
			insert account;

			Contact contact = new Contact (				
														AccountId = account.Id,
														FirstName = 'Tonio',
						    							LastName = 'Cartonio', 
						    							Email='tonio.cartonio@email.com',
						    							Codice_Fiscale__c = 'TNICRT91T04E435K',
						    							Via__c = 'melevisione',
						    							Civico__c = '55',
						    							MailingCity = 'Fantabosco',
						    							Id_Web__c = 'idwebdiprova'
														);

			insert contact;

			WS_PortalAccountsContacts.InputElement outElem = new WS_PortalAccountsContacts.InputElement();
			outElem.account = account;
			outElem.contact = contact;
			request.elements.add(outElem);

			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response response =  WS_PortalAccountsContacts.insertAccountContact(request);
			List<WS_PortalAccountsContacts.OutputElement> outputElements = response.elements;
			WS_PortalAccountsContacts.OperationStatus operationStatus = response.operationStatus;

			System.assertEquals('idwebdiprova', outputElements[0].idWeb);
			System.assertEquals(contact.id, outputElements[0].contactId);
			System.assertEquals(contact, outputElements[0].contact);
			System.assertEquals(account, outputElements[0].account);
			}
		}	



		@isTest static void testAccountNonDuplicato(){

			User utente = [SELECT Id FROM User  WHERE profile.name = 'Admin - Customer Portal' LIMIT 1];
			System.runAs(utente){
			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request request = new WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request();

			request.elements = new List<WS_PortalAccountsContacts.InputElement>();
			Account account = new Account ( 
														Name = 'Ops Aps',
														Email__c='Aps@email.com',
						    							Codice_Fiscale__c = 'APSOPS91T04E435K',
						    							RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId()
						    							
						    							);
			

			Contact contact = new Contact (				
														
														FirstName = 'Antonio',
						    							LastName = 'Andr', 
						    							Email='tonio@email.com',
						    							Codice_Fiscale__c = 'TN0CRT91T04E435K',
						    							Via__c = 'asdasdasd',
						    							Civico__c = '54',
						    							MailingCity = 'asdasd',
						    							Id_Web__c = 'idwebdiprovaqweasd'
														);

			
			WS_PortalAccountsContacts.InputElement outElem = new WS_PortalAccountsContacts.InputElement();
			outElem.account = account;
			outElem.contact = contact;
			request.elements.add(outElem);

			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response response =  WS_PortalAccountsContacts.insertAccountContact(request);
			List<WS_PortalAccountsContacts.OutputElement> outputElements = response.elements;
			WS_PortalAccountsContacts.OperationStatus operationStatus = response.operationStatus;

			System.assertEquals('idwebdiprovaqweasd', outputElements[0].idWeb);
			System.assertEquals(contact.id, outputElements[0].contactId);
			System.assertEquals(contact, outputElements[0].contact);
			System.assertEquals(account, outputElements[0].account);
			}
		}


		@isTest static void testAccountNonValido(){

			User utente = [SELECT Id FROM User  WHERE profile.name = 'Admin - customer portal' LIMIT 1];
			System.runAs(utente){
			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request request = new WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request();

			request.elements = new List<WS_PortalAccountsContacts.InputElement>();
			Account account = new Account ( 
														Name = 'Beppe Jonny',
						    							Email__c='beppe@email.com',
						    							Codice_Fiscale__c = 'BNFLNO9K1T04E435QWEQWEQWWESAEADASDASDQWEQ2QWEQTGGGGAQWEQWEQWEQWEQWEQWDASDASFQWEQWEQWASDADQWE',
						    							RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId()
						    							
						    							);
			

			Contact contact = new Contact (				
														
														FirstName = 'Giovanni',
						    							LastName = 'Mario', 
						    							Email='giovanni@email.com',
						    							Codice_Fiscale__c = 'MRIGVN91T04E435K',
						    							Via__c = 'aaaaaaaaaa',
						    							Civico__c = '55',
						    							MailingCity = 'bbbbbbbbb',
						    							Id_Web__c = 'idwebdiprovaWWWWWWWWWWW'
														);

			
			WS_PortalAccountsContacts.InputElement inElem = new WS_PortalAccountsContacts.InputElement();
			inElem.account = account;
			inElem.contact = contact;

			request.elements.add(inElem);

			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response response =  WS_PortalAccountsContacts.insertAccountContact(request);
			List<WS_PortalAccountsContacts.OutputElement> outputElements = response.elements;
			WS_PortalAccountsContacts.OperationStatus operationStatus = response.operationStatus;

			
			System.assertEquals('errore riguardante i contatti : ; errore riguardante gli accountCodice Fiscale: data value too large: BNFLNO9K1T04E435QWEQWEQWWESAEADASDASDQWEQ2QWEQTGGGGAQWEQWEQWEQWEQWEQWDASDASFQWEQWEQWASDADQWE (max length=16)', outputElements[0].respMsg);
			


			}
		}		
@isTest static void testAccountDuplicato(){

			User utente = [SELECT Id FROM User  WHERE profile.name = 'Admin - customer portal' LIMIT 1];
			System.runAs(utente){
			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request request = new WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request();

			request.elements = new List<WS_PortalAccountsContacts.InputElement>();
			Account account1 = new Account ( 
														Name = 'Lino Banfi',
						    							Email__c='banfi.lino@email.com',
						    							Codice_Fiscale__c = 'BNFLNO91T04E435K',
						    							RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId()
						    							
						    							);
			insert account1;

			Contact contact1 = new Contact (				
														AccountId = account1.Id,
														FirstName = 'Tonio',
						    							LastName = 'Cartonio', 
						    							Email='tonio.cartonio@email.com',
						    							Codice_Fiscale__c = 'TNICRT91T04E435K',
						    							Via__c = 'melevisione',
						    							Civico__c = '55',
						    							MailingCity = 'Fantabosco',
						    							Id_Web__c = 'idwebdiprova'
														);

			insert contact1;



			Account account = new Account ( 
														Name = 'Lino Banfi',
						    							Email__c='banfi.lino@email.com',
						    							Codice_Fiscale__c = 'BNFLNO91T04E435K',
						    							RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId()
						    							
						    							);
			
			Contact contact = new Contact (				
														//AccountId = account1.Id,
														FirstName = 'Tonio',
						    							LastName = 'Cartonio', 
						    							Email='tonio.cartonio@email.com',
						    							Codice_Fiscale__c = 'TNICRT91T04E435K',
						    							Via__c = 'melevisione',
						    							Civico__c = '55',
						    							MailingCity = 'Fantabosco',
						    							Id_Web__c = 'idwebdiprova2'
														);

			
			WS_PortalAccountsContacts.InputElement inElem = new WS_PortalAccountsContacts.InputElement();
			inElem.account = account;
			inElem.contact = contact;
			request.elements.add(inElem);

			WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response response =  WS_PortalAccountsContacts.insertAccountContact(request);
			List<WS_PortalAccountsContacts.OutputElement> outputElements = response.elements;
			WS_PortalAccountsContacts.OperationStatus operationStatus = response.operationStatus;

			



			System.assertEquals('idwebdiprova2', outputElements[0].idWeb);
			System.assertEquals(contact.id, outputElements[0].contactId);
			System.assertEquals(contact, outputElements[0].contact);
			System.assertEquals(account, outputElements[0].account);
			

			}
		}	
	@isTest
	static void testConstructedState() {
	   	Test.startTest();
    	WS_PortalAccountsContacts.ContactUpsertResult inst = new WS_PortalAccountsContacts.ContactUpsertResult();
    	WS_PortalAccountsContacts.AccountUpsertResult inst1 = new WS_PortalAccountsContacts.AccountUpsertResult();
    	WS_PortalAccountsContacts.FailureReason inst2 = new WS_PortalAccountsContacts.FailureReason();
    	Test.stopTest();

    	System.assertEquals(null, inst.isSuccess);
    	System.assertEquals(null, inst.contact);
    	System.assertEquals(null, inst.operationStatus);
    	System.assertEquals(null, inst.failureReason);
    	System.assertEquals(null, inst.contactId);

    	System.assertEquals(null, inst1.isSuccess);
    	System.assertEquals(null, inst1.contact);
    	System.assertEquals(null, inst1.operationStatus);
    	System.assertEquals(null, inst1.failureReason);
    	System.assertEquals(null, inst1.accountId);
    	System.assertEquals(null, inst1.originalAccount);


		System.assertEquals(null, inst2.isDuplicate);
    	System.assertEquals(null, inst2.duplicateAccountId);
    	System.assertEquals(null, inst2.duplicateContactId);
    	System.assertEquals(null, inst2.isException);
    	System.assertEquals(null, inst2.exceptionDetailMessage);



    	
}

}