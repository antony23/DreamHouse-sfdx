public class CommonWrapper {
	
	public class Bank_Data{

		public String bic 				{get; set;}
		public String branch 			{get; set;}
		public String bank 				{get; set;}
		public String address 			{get; set;}
		public String city 				{get; set;}
		public String state				{get; set;}
		public String zip 				{get; set;}
		public String phone				{get; set;}
		public String fax 				{get; set;}
		public String www 				{get; set;}
		public String email 			{get; set;}
		public String country 			{get; set;}
		public String country_iso		{get; set;}
		public String account 			{get; set;}

	}

	public class Errors{

		public String code 				{get; set;}
		public String message 			{get; set;}

	}

	public class Sepa_Data{

		public String sct 				{get; set;}
		public String sdd 				{get; set;} 
		public String cor1				{get; set;}
		public String b2b				{get; set;}
		public String scc				{get; set;}

	}

	public class Validation{

		public String code				{get; set;}
		public String message 			{get; set;}

	}
}