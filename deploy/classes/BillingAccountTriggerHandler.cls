public class BillingAccountTriggerHandler{

    public static void setFields(Zuora__CustomerAccount__c[] acc){
        Map<String,Zuora__CustomerAccount__c> subsZuoraIds = new Map<String,Zuora__CustomerAccount__c>();
        System.debug('******** BillingAccountTriggerHandler ***********'  + acc );
        for(Zuora__CustomerAccount__c s : acc){
            if(String.isNotBlank(s.Sold_To_Salesforce_text__c) && s.Sold_To_Salesforce_text__c.startsWith('003')){
                s.Sold_To_Salesforce__c = Id.valueOf(s.Sold_To_Salesforce_text__c);
            }
            if(String.isNotBlank(s.Bill_To_Salesforce_text__c) && s.Bill_To_Salesforce_text__c.startsWith('003')){
                s.Bill_To_Salesforce__c = Id.valueOf(s.Bill_To_Salesforce_text__c);
            }
            if(String.isNotBlank(s.Zuora__Zuora_Id__c)){
                subsZuoraIds.put(s.Zuora__Zuora_Id__c,s);
            }
        }
         System.debug('************ subsZuoraIds ******'  +subsZuoraIds);
        for(zqu__Quote__c q : [Select zqu__ZuoraAccountID__c,zqu__SoldToContact__c,zqu__BillToContact__c, Metodo_di_pagamento__c From zqu__Quote__c Where zqu__ZuoraAccountID__c in : subsZuoraIds.keySet() And zqu__SoldToContact__c != null And zqu__BillToContact__c != null]){
            subsZuoraIds.get(q.zqu__ZuoraAccountID__c).Metodo_di_pagamento__c = q.Metodo_di_pagamento__c;            
        System.debug('************ zqu__Quote__c************ ' + q + 'metodo di pagamento' +  q.Metodo_di_pagamento__c);

        }
    }

}