public with sharing class NuovoIndirizzoExtension {

    public Account account {get;set;}

    private static List<String> ACCOUNT_FIELDS;
    private static List<String> CONTACT_FIELDS;

    static{
        ACCOUNT_FIELDS = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Account').getDescribe().Fields.getMap().values()){
            Schema.DescribeFieldResult fd = ft.getDescribe();
            String fName = fd.getName();
            if(!fName.startsWith('Person') && !fName.endsWith('__pc') && fName != 'FirstName' && fName != 'LastName'){
                ACCOUNT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
            }
        }
        CONTACT_FIELDS = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
            CONTACT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
        }
    }
    
    public NuovoIndirizzoExtension(ApexPages.StandardController controller) {
        account = (Account) Database.query('SELECT ' + String.join(ACCOUNT_FIELDS, ',') + ' FROM Account WHERE Id = \'' + controller.getId() + '\'');
    }
    
    public PageReference back(){
        PageReference pr = new PageReference('/' + account.Id);
        return pr;
    }
    
    public PageReference saveAccountContact(){
        PageReference pr = null;

        try {
            
            System.debug(loggingLevel.Error, '*** account: ' + account);

            // il controllo di deduplica l'ho già fatto in tempo reale quindi qui evito di rifarlo
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;

            AccountTriggerHandler.skip = true;

            // salvo l'account
            Database.SaveResult accSaveResult = account.Id == null ? 
                                        Database.insert(account,dml) : 
                                        Database.update(account,dml);
            
            // se non ci sono riuscito lancio un errore
            if(!accSaveResult.isSuccess()){
                Database.Error error = accSaveResult.getErrors()[0];
                throw new DomusException(error.getMessage());
            }

            pr = new PageReference('/' + accSaveResult.getId());
        } catch(Exception e) {
            // se sono andato in errore qui è perché qualche campo è stato compilato male quindi informo l'utente ma gli permetto di modificare i dati di account e contatto
            appendErrorMessage(e);
        }
        return pr;
    }

    public void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }

    // Cappario
    public String data {get;set;}

    @RemoteAction
    public static Object getResources(String resource)
    {
        System.debug(loggingLevel.Error, '*** JSON.deserializeUntyped(getData(resource)): ' + JSON.deserializeUntyped(getData(resource)));
        return JSON.deserializeUntyped(getData(resource));
    }

    public void getResources()
    {
        String resource = ApexPages.CurrentPage().getParameters().get('resource');
        String civico = ApexPages.currentPage().getParameters().get('civico');
        if(String.isNotBlank(civico)){
            resource += '&civico=' + civico;
        }
        data = getData(resource);
        System.debug(loggingLevel.Error, '*** data: ' + data);
    }

    public static String getData(String resource)
    {
        HttpRequest req = new HttpRequest();
        //ServiceEndpoint__c endpoint = ServiceEndpoint__c.getValues('AccountMock');
        //String url = endpoint.URL__c;
        if(resource!=null && resource.containsIgnoreCase('strade')){
            
            Integer startIndex = resource.indexOf('strade');
            Integer endIndex = resource.indexOf('?idComune=');
            String reqString = resource.substring(startIndex+7, endIndex);

            /*String encodedString = reqString.replace('%20', '');
            encodedString = EncodingUtil.urlEncode(encodedString, 'UTF-8');
            encodedString = encodedString.replace('+', '%20');*/
            String encodedString = reqString;

            //Se cambia l'url da cambiare bisogna rivalutare questa stringa
            System.debug(loggingLevel.Error, '*** resource: ' + resource);
            System.debug(loggingLevel.Error, '*** resource.substring(0, startIndex+7): ' + resource.substring(0, startIndex+7));
            System.debug(loggingLevel.Error, '*** encodedString: ' + encodedString);
            System.debug(loggingLevel.Error, '*** resource.substring(endIndex, resource.length()): ' + resource.substring(endIndex, resource.length()));
            System.debug(loggingLevel.Error, '*** endIndex: ' + endIndex);
            System.debug(loggingLevel.Error, '*** resource.length(): ' + resource.length());
            resource = resource.substring(0, startIndex+7)+encodedString+resource.substring(endIndex, resource.length());

        }

        String url = Setting__c.getInstance('DomusCap').URL__c+resource;
        req.setEndpoint(url);
        req.setTimeout(120000);
        req.setMethod('GET');
        System.debug(url);
        HTTP http = new Http();
        HttpResponse response = http.send(req);
        System.debug(response.getBody());
        return response.getBody();
    }

    @RemoteAction
    public static Map<String,Map<String,Object>> queryAccountContact(String accountId){
        Map<String,Map<String,Object>> result = new Map<String,Map<String,Object>>{
            'Account' => new Map<String,Object>(),
            'Contact' => new Map<String,Object>()
        };

        // recupero le informazioni di account e contatto
        sObject account = (sObject) Database.query('SELECT ' + String.join(ACCOUNT_FIELDS, ',') + ' FROM Account WHERE Id = \'' + accountId + '\'');
        List<sObject> contattiPrincipali = (List<sObject>) Database.query('SELECT ' + String.join(CONTACT_FIELDS, ',') + ' FROM Contact WHERE AccountId = \'' + accountId + '\' AND MainContact__c = true');
        sObject contattoPrincipale = contattiPrincipali.isEmpty() ? null : contattiPrincipali.get(0);
        for(String accountField : ACCOUNT_FIELDS){
            result.get('Account').put(accountField, account.get(accountField));
            System.debug(loggingLevel.Error, accountField + ' = ' + account.get(accountField));
        }

        for(String contactField : CONTACT_FIELDS){
            result.get('Contact').put(contactField, contattoPrincipale != null ? contattoPrincipale.get(contactField) : null);
        }
        result.put('additionalInfos', new Map<String,Object>{'isB2C' => result.get('Account').get('recordtypeid') == Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId()});
        System.debug(loggingLevel.Error, '*** result: ' + result);
        System.debug(loggingLevel.Error, '*** object: ' + result.get('Account').get('recordtypeid'));
        System.debug(loggingLevel.Error, '*** object: ' + Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId());
        return result;
    }
}