public class RefreshInformativa {

	private final Id contactId		{get; set;}

	public RefreshInformativa(ApexPages.StandardController stdController){
		this.contactId = stdController.getRecord().Id;
    }
    

	public PageReference refreshInformativaContact(){
	
		futureRefresh(contactId);

		PageReference pageRef = new PageReference('/'+ contactId);
        pageRef.setRedirect(true);
       	return pageRef;
	}

	@Future
	public static void futureRefresh(Id contactId){
		try{
			Contact contatto = InformativaUtils.fetchContactForUpdate(contactId);
			Informativa_Privacy__c informativa = InformativaUtils.getInformativa();

			if(informativa != null && contatto.Eleggibile_per_Notifica_Informativa__c){
				//if(String.isBlank(contatto.Versione_Informativa__c) || Integer.valueOf(contatto.Versione_Informativa__c) < Integer.valueOf(informativa.Versione_Informativa__c)){
					List<Contact> listaContatti = new List<Contact>{contatto};
                	InformativaUtils.setCampiInformativa(listaContatti, informativa);
                	InformativaUtils.insertNotificaInformativa(listaContatti, informativa);
				//}
			}
		}catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
		}catch(TypeException e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred during record conversion: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }
	}

	/*
	webservice static void WSRefreshInformativaContact(Id contactId){
		try{
			Contact contatto = fetchContact(contactId);
			Informativa_Privacy__c informativa = [SELECT Id, Versione_Informativa__c, URL_Informativa__c, Testo_Informativa__c FROM Informativa_Privacy__c WHERE Name = 'Informativa Corrente' LIMIT 1];

			if(informativa != null){
				if(contatto.Fonte__c != 'Web' && contatto.Record_Type_Account__c == 'B2C'){
					if(String.isBlank(contatto.Versione_Informativa__c) || Integer.valueOf(contatto.Versione_Informativa__c) < Integer.valueOf(informativa.Versione_Informativa__c)){
						List<Contact> listaContatti = new List<Contact>{contatto};
	                	InformativaUtils.setCampiInformativa(listaContatti, informativa);
	                	InformativaUtils.insertNotificaInformativa(listaContatti, informativa);
					}
				}
			}

		}catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
		}catch(TypeException e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred during record conversion: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }
	}

	public void refreshInformativaContact(){
		try{
			if(informativa != null){
				if(contatto.Fonte__c != 'Web' && contatto.Record_Type_Account__c == 'B2C'){
					if(String.isBlank(contatto.Versione_Informativa__c) || Integer.valueOf(contatto.Versione_Informativa__c) < Integer.valueOf(informativa.Versione_Informativa__c)){
						List<Contact> listaContatti = new List<Contact>{contatto};
	                	InformativaUtils.setCampiInformativa(listaContatti, informativa);
	                	InformativaUtils.insertNotificaInformativa(listaContatti, informativa);
	                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Informativa aggiornata correttamente.'));
					}else{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Impossibile proseguire con l\'aggiornamento. L\'informativa privacy del contatto � gi� la pi� recente.'));
					}
				}else{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Impossibile proseguire con l\'aggiornamento. Il contatto non � idoneo al refresh dei dati.'));
				}
			}else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Impossibile proseguire con l\'aggiornamento. Informativa privacy non trovata.'));
			}
        }catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Errore durante l\'aggiornamento dell\'informativa: ' + e.getMessage()));
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Errore durante l\'aggiornamento dell\'informativa: ' + e.getMessage()));
		}catch(TypeException e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred during record conversion: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Errore durante l\'aggiornamento dell\'informativa: ' + e.getMessage()));
        }
	}

	public PageReference refreshInformativaContact(){
		try{
			if(informativa != null){
				if(contatto.Fonte__c != 'Web' && contatto.Record_Type_Account__c == 'B2C'){
					if(String.isBlank(contatto.Versione_Informativa__c) || Integer.valueOf(contatto.Versione_Informativa__c) < Integer.valueOf(informativa.Versione_Informativa__c)){
						List<Contact> listaContatti = new List<Contact>{contatto};
	                	InformativaUtils.setCampiInformativa(listaContatti, informativa);
	                	InformativaUtils.insertNotificaInformativa(listaContatti, informativa);
					}
				}
			}
		}catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
		}catch(TypeException e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred during record conversion: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }	

		PageReference pageRef = new PageReference('/'+ contatto.Id);
        pageRef.setRedirect(true);
       	return pageRef;
	}

    */
}