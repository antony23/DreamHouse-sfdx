public class ManageAccountExtension {

	private Account acc;
    private String operationType;
    private Id recordId;
    private String retURL;
    private Id recordTypeId;

    public ManageAccountExtension(ApexPages.StandardController stdController) {
        this.acc = (Account) stdController.getRecord();
        String save_new = ApexPages.currentPage().getParameters().get('save_new');
        operationType = save_new != null && save_new == '1' ? 'Create' : 'Update';
        recordId = (Id) ApexPages.currentPage().getParameters().get('Id');
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');

    }

    public PageReference automaticRedirect() {
        PageReference pr = Page.ManageAccountContact;
        pr.getParameters().put('operationType',operationType);
        pr.getParameters().put('recordId',recordId);
        pr.getParameters().put('retURL',retURL);
        pr.getParameters().put('RecordType',recordTypeId);
        return pr;
    }
}