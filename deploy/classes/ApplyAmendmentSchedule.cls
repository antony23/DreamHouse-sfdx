global class ApplyAmendmentSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		recoverLostAmendments();
		if(!Test.isRunningTest()) Database.executebatch(new ApplyAmendmentBatch());
	}

	private void recoverLostAmendments(){
		List<string> subscriptionFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Zuora__Subscription__c').getDescribe().Fields.getMap().values()){
            subscriptionFields.add(ft.getDescribe().getName().toLowerCase());
        }
        String query = 'SELECT ' + String.join(subscriptionFields, ',') + 
                    ' FROM Zuora__Subscription__c ' +
                    ' WHERE AmendmentIsError__c = true';
        if(Test.isRunningTest()) query += ' LIMIT 1000 ';
        Map<Id,Zuora__Subscription__c> subscriptionsOnError = new Map<Id,Zuora__Subscription__c>(
            (List<Zuora__Subscription__c>) Database.query(query)
        );
        delete [SELECT Id FROM Amendment__c WHERE Subscription__c IN :subscriptionsOnError.keySet()];
        if(!Test.isRunningTest()) Database.executeBatch(new DownloadAmendmentBatch(subscriptionsOnError.keySet()),150);
	}
}