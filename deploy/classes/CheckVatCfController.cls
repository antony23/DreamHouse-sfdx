public class CheckVatCfController {
    
    public Account a {get;set;}
    public Boolean isValid {get;set;}
    public String ragioneSociale {get;set;}

    public List<sObject> duplicateRecords{get;set;}
    public boolean hasDuplicateResult{get;set;}
    private Boolean ignoreDuplicate;
    private Contact mc;

    public CheckVatCfController(){ }
    public CheckVatCfController(ApexPages.StandardController controller){
        if(!Test.isRunningTest()) {
            controller.addFields(new List < String > {'Codice_Nazione__c', 'Codice_Fiscale__c', 
            'NomeClienteB2C__c', 'CognomeClienteB2C__c', 'Sesso__c', 'Data_di_nascita__c', 'Luogo_di_nascita__c'});
        }

        a = (Account) controller.getRecord();
        
        try {
            mc = [ SELECT Id, FirstName, LastName, Sesso__c, Birthdate, Luogo_di_Nascita__c, Codice_Fiscale__c FROM Contact 
                   WHERE AccountId = :a.Id AND MainContact__c = True AND Codice_Fiscale__c = :a.Codice_Fiscale__c
                   ORDER BY CreatedDate DESC
                   LIMIT 1 ];
        } catch(Exception e) {
            System.debug('No main contact found. Data on account will be used.');

            mc = new Contact();
            mc.FirstName = a.NomeClienteB2C__c;
            mc.LastName = a.CognomeClienteB2C__c;
            mc.Sesso__c = a.Sesso__c;
            mc.Birthdate = a.Data_di_nascita__c;
            mc.Luogo_di_Nascita__c = a.Luogo_di_nascita__c;
            mc.Codice_Fiscale__c = a.Codice_Fiscale__c;
        }

        isValid = false;
        this.duplicateRecords = new List<sObject>();
        this.hasDuplicateResult = false;
        ignoreDuplicate = false;
    }
    
    public void checkVatCf(){
        isValid = false;

        Boolean validPIVA = false;
        Boolean validCF = false;
        Boolean emptyPIVA = true;
        Boolean emptyCF = true;
        Boolean emptyNationCode = true;
        String errorMessage = null;

        System.debug('Account: ' + a);
        System.debug('Account.RecordType = ' + a.recordType.DeveloperName);
        System.debug('Main Contact: ' + mc);
        
        if(a.recordType.DeveloperName != 'B2C'){
            if(String.isNotBlank(a.codice_nazione_iva__c)) {
                emptyNationCode = false;
            }

            if(String.isNotBlank(a.Partita_IVA__c)) {
                emptyPIVA = false;

                a.Partita_IVA__c = a.Partita_IVA__c.toUpperCase().trim();
                
                if(!emptyNationCode) {
                    validPIVA = checkVat();
                }
            }
        }
        
        if(a.recordType.DeveloperName != 'B2BLarge'){
            if(String.isNotBlank(mc.Codice_Fiscale__c)){
                emptyCF = false;

                if(a.Codice_Nazione__c == 'IT'){
                    mc.Codice_Fiscale__c = mc.Codice_Fiscale__c.toUpperCase().trim();

                    try {
                        validCF = UtilCodiceFiscale.validateCF(
                                                                    mc.FirstName,
                                                                    mc.LastName,
                                                                    mc.Sesso__c,
                                                                    mc.Birthdate,
                                                                    mc.Luogo_di_nascita__c,
                                                                    mc.Codice_Fiscale__c
                                                                );
                    } catch(Exception e) {
                        System.debug(e.getMessage() + ' [' + e.getLineNumber() + ']');
                        errorMessage = e.getMessage();
                    }

                    if(errorMessage == null && validCF) {
                    //  if(a.recordType.DeveloperName == 'B2C'){
                    //      UtilCodiceFiscale.Person p = UtilCodiceFiscale.getPersonInfo(a.Codice_Fiscale__c);
                    //      a.Sesso__c = p.sex;
                    //      a.PersonBirthdate = p.d;                        
                    //  }
                    }
                } else {
                    validCF = true;
                }

                System.debug('@valid='+validCF);
            }
        }

        System.debug('validPIVA = ' + validPIVA);
        System.debug('validCF = ' + validCF);
        System.debug('emptyPIVA = ' + emptyPIVA);
        System.debug('emptyCF = ' + emptyCF);
        System.debug('emptyNationCode = ' + emptyNationCode);
        System.debug('errorMessage = ' + errorMessage);

        switch on a.recordType.DeveloperName {
            when 'B2C' {
                if(emptyCF) {
                    isValid = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Codice Fiscale non trovato'));
                    return;
                }

                if(validCF) {
                    isValid = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Codice Fiscale valido'));
                    return;
                }

                if(errorMessage != null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
                    return;
                }

                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Codice Fiscale non valido'));
            }

            when 'B2BSmall' {
                if(emptyCF && emptyPIVA) {
                    isValid = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Codice Fiscale e Partita IVA non trovati'));
                    return;
                }

                if(validCF || validPIVA) {
                    isValid = true;
                    
                    if(validCF) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Codice Fiscale valido'));
                    } else if(errorMessage != null) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
                    } else if(emptyCF) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Codice Fiscale non trovato'));
                    } else {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Codice Fiscale non valido'));
                    }

                    if(validPIVA) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Partita IVA valida'));
                    } else if(emptyNationCode) {
                        a.codice_nazione_iva__c.addError('Selezionare un codice nazione');
                    } else if(emptyPIVA) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Partita IVA non trovata'));
                    } else {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Partita IVA non valida'));
                    }
                } else {
                    if(errorMessage != null) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
                    } else if(emptyCF) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Codice Fiscale non trovato'));
                    } else {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Codice Fiscale non valido'));
                    }

                    if(emptyNationCode) {
                        a.codice_nazione_iva__c.addError('Selezionare un codice nazione');
                    } else if(emptyPIVA) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Partita IVA non trovata'));
                    } else {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Partita IVA non valida'));
                    }
                }
            }

            when 'B2BLarge' {
                if(emptyPIVA) {
                    isValid = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Partita IVA non trovata'));
                    return;
                }

                if(validPIVA) {
                    isValid = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Partita IVA valida'));
                    return;
                }

                if(emptyNationCode) {
                    a.codice_nazione_iva__c.addError('Selezionare un codice nazione');
                    return;
                }

                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Partita IVA non valida'));
            }
        }
    }
    
    public Boolean checkVat(){
        ragioneSociale = '';
        Boolean isVatValid = false;
        try{
            CheckVatService.checkVatPort service = new CheckVatService.checkVatPort();
            System.debug(loggingLevel.Error, '*** a.codice_nazione_iva__c: ' + a.codice_nazione_iva__c);
            System.debug(loggingLevel.Error, '*** a.Partita_IVA__c: ' + a.Partita_IVA__c);
            CheckVatServiceTypes.checkVatResponse_element response = service.checkVat(a.codice_nazione_iva__c,a.Partita_IVA__c);
            if(ApexPages.currentPage().getParameters().get('forceValid') != null){
                response = new CheckVatServiceTypes.checkVatResponse_element();
                response.valid = ApexPages.currentPage().getParameters().get('forceValid') == 'Y';
            }
            if(response.valid){
                isVatValid = true;
                //a.Name = response.name;
                ragioneSociale = response.name;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Partita IVA valida'));
            }else{
                a.Partita_IVA__c.addError('Partita IVA non valida');
            }
        }catch(CalloutException e){
            isVatValid = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Attenzione: Controllo P.Iva non al momento disponibile. Selezionare "Salva" per salvare ugualmente la P.IVA'));
        }catch(Exception e){
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        
        return isVatValid;
    }
    
    public void goBack(){
        isValid = false;
        ignoreDuplicate = false;
        this.duplicateRecords = new List<sObject>();
        this.hasDuplicateResult = false;
    }

    public PageReference saveIgnore(){
        ignoreDuplicate = true;
        return save();
    }

    public PageReference save() {

        Database.DMLOptions dml = new Database.DMLOptions();
        if(ignoreDuplicate){
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            ignoreDuplicate = false;
        }

        Database.SaveResult saveResult = Database.update(a, dml);

        if (!saveResult.isSuccess()) {
            for (Database.Error error : saveResult.getErrors()) {
                if (error instanceof Database.DuplicateError) {
                    Database.DuplicateError duplicateError = 
                            (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = 
                            duplicateError.getDuplicateResult();
                    
                    ApexPages.Message errorMessage = new ApexPages.Message(
                            ApexPages.Severity.Warning, 'Duplicate Error: ' + 
                            duplicateResult.getErrorMessage());
                    ApexPages.addMessage(errorMessage);
                    
                    this.duplicateRecords = new List<sObject>();

                    Datacloud.MatchResult[] matchResults = 
                            duplicateResult.getMatchResults();

                    Datacloud.MatchResult matchResult = matchResults[0];

                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        System.debug('MatchRecord: ' + matchRecord.getRecord());
                        this.duplicateRecords.add(matchRecord.getRecord());
                    }
                    this.hasDuplicateResult = !this.duplicateRecords.isEmpty();
                }
            }
            return null;
        }
        return (new ApexPages.StandardController(a)).view();
    }

    private static List<Contact> fetchContact(Id accountId){
        String query = 'SELECT ' + String.join(fieldsOf('Contact'), ',') + ' FROM Contact WHERE AccountId = :accountId and MainContact__c = true LIMIT 1';
        return (List<Contact>) Database.query(query);
    }
    
    private static List<String> fieldsOf(String className){
        List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        return fields;
    }
}