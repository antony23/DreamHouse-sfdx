global class CalcoloEntityDatiMigrati implements Database.Batchable<sObject> {
	
	String query;
	String testata;
	
	global CalcoloEntityDatiMigrati(String t) {
		testata = t;
		query = 'SELECT Id,Entity__c,Tipo_documento__c,Problemacalcoloentity__c FROM Zuora__Subscription__c WHERE Mig_Tripletta_di_origine__c != null AND Entity__c = null AND Tipo_documento__c = null AND Testata__c = :testata';	
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<Zuora__Subscription__c> subscriptions = (List<Zuora__Subscription__c>) scope;
		SubscriptionTriggerHandler.skipTrigger = true;
		for(Zuora__Subscription__c subscription : subscriptions){
			try {
	        	UtilFatturazione.FatturazioneResponse resp = UtilFatturazione.getParametriFatturazioneSubscription(subscription.Id);
				subscription.Entity__c = resp.mappingDocumento.Tipologia_Num_Doc_Emesso__c;
				subscription.Tipo_documento__c = resp.mappingDocumento.Documento_Emesso__c;
			} catch(Exception e) {
				subscription.Problemacalcoloentity__c = e.getMessage() + '. ' + e.getStackTraceString();
			}
		}
		Database.update(subscriptions,false);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}