public class InformativaUtils {
    
    public static void setDatiInformativaOnAfterInsert(List<Contact> listaContatti){

        Informativa_Privacy__c informativaCorrente = getInformativa();
        if(informativaCorrente != null){
            List<Contact> contactList = new List<Contact>();
            for(Contact c : listaContatti){
                if(c.Eleggibile_per_Notifica_Informativa__c){
                    contactList.add(c);
                }
            }

            if(!contactList.isEmpty()){
                setCampiInformativaInAfterTrigger(contactList, informativaCorrente);
                insertNotificaInformativa(contactList, informativaCorrente);
            }   
        }
    }

    public static void setDatiInformativaOnAfterUpdate(List<Contact> triggerNew, Map<Id, Contact> triggerOldMap){
        
        Informativa_Privacy__c informativaCorrente = getInformativa();
        List<String> fieldsToCheck = getPrivacyFields();

        Utils.TriggerUtils triggerUtils = new Utils.TriggerUtils(triggerNew, triggerOldMap);
        List<Contact> listaContatti = (List<Contact>) triggerUtils.getChanged(fieldsToCheck);

        if(!listaContatti.isEmpty() && informativaCorrente != null){
            List<Contact> contactList = new List<Contact>();
            for(Contact c : listaContatti){
                if(c.Eleggibile_per_Notifica_Informativa__c){
                        contactList.add(c);
                    }
                }

            if(!contactList.isEmpty()){
                setCampiInformativaInAfterTrigger(contactList, informativaCorrente);
                insertNotificaInformativa(contactList, informativaCorrente);
            }   
        }
    }
    
    public static void setCampiInformativa(List<Contact> listaContatti, Informativa_Privacy__c informativaCorrente){

        List<Contact> filteredContact = filterByVersioneInformativa(listaContatti, informativaCorrente);

        for(Contact c : filteredContact){
            c.URL_Informativa__c = informativaCorrente.URL_Informativa__c;
            c.Versione_Informativa__c = informativaCorrente.Versione_Informativa__c;
        }
        ContactTriggerHandler.skip = true;
        update filteredContact;
        ContactTriggerHandler.skip = false;
    }

    public static void setCampiInformativaInAfterTrigger(List<Contact> listaContatti, Informativa_Privacy__c informativaCorrente){

        List<Contact> filteredContact = filterByVersioneInformativa(listaContatti, informativaCorrente);

        List<Contact> contactToUpdate = new List<Contact>();
        for(Contact c : filteredContact){
            Contact contatto = new Contact(Id = c.Id);
            contatto.URL_Informativa__c = informativaCorrente.URL_Informativa__c;
            contatto.Versione_Informativa__c = informativaCorrente.Versione_Informativa__c;
            contactToUpdate.add(contatto);
        }
        ContactTriggerHandler.skip = true;
        update contactToUpdate;
        ContactTriggerHandler.skip = false;
    }

    public static void insertNotificaInformativa(List<Contact> listaContatti, Informativa_Privacy__c informativaCorrente){

        Set<Id> contactIds = new Set<Id>();
        for(Contact c : listaContatti){
            if(c.Fonte__c == 'Telefono' || c.Fonte__c == 'Email'){
            contactIds.add(c.Id);
        }
        }

        List<Contact> listContattiConNotifiche = [SELECT Id, Email, Email__c, (SELECT Id FROM Notifiche_Informativa__r WHERE Versione_Informativa__c = :informativaCorrente.Versione_Informativa__c) FROM Contact WHERE Id IN :contactIds];

        List<Notifica_Informativa__c> notificheDaInserire = new List<Notifica_Informativa__c>();
        for(Contact c : listContattiConNotifiche){
            if(c.Notifiche_Informativa__r.isEmpty()){
                Notifica_Informativa__c notifica = new Notifica_Informativa__c(
                    Contatto__c                 = c.Id,
                    URL_Informativa__c          = informativaCorrente.URL_Informativa__c,
                    Versione_Informativa__c     = informativaCorrente.Versione_Informativa__c,
                    Stato__c                    = AppConstants.EMAIL_DRAFT,
                    Invio_Manuale__c            = String.isEmpty(c.Email__c)
                );
                notificheDaInserire.add(notifica);
            }
        }
        NotificaInformativaTriggerHandler.skipTrigger = true;
        insert notificheDaInserire;
        NotificaInformativaTriggerHandler.skipTrigger = false;
    }

    public static List<Contact> filterByVersioneInformativa(List<Contact> listaContatti, Informativa_Privacy__c informativaCorrente) {
        List<Contact> filteredContact = new List<Contact>();

        for(Contact c : listaContatti) {
            try{
                if(String.isBlank(c.Versione_Informativa__c) || Integer.valueOf(c.Versione_Informativa__c) < Integer.valueOf(informativaCorrente.Versione_Informativa__c)){
                    filteredContact.add(c);
                }
            }catch(TypeException e){
                System.debug(LoggingLevel.ERROR, 'An error has occurred during record conversion: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            }
        }

        return filteredContact;
    }

    public static Contact fetchContact(Id contactId){
        final String query = 'SELECT ' + String.join(fieldsOf('Contact'), ',') + ' FROM Contact WHERE Id = :contactId LIMIT 1';
        final List<Contact> contactList = (List<Contact>) Database.query(query);
        if(contactList.isEmpty()) return new Contact();
        return contactList[0];
    }

    public static Contact fetchContactForUpdate(Id contactId){
        if(contactId == null) return new Contact();
        final String query = 'SELECT ' + String.join(fieldsOf('Contact'), ',') + ' FROM Contact WHERE Id = :contactId LIMIT 1 FOR UPDATE';
        return (Contact) Database.query(query);
    }

    public static Informativa_Privacy__c getInformativa(){
        final String query = 'SELECT ' + String.join(fieldsOf('Informativa_Privacy__c'), ',') + ' FROM Informativa_Privacy__c WHERE Name = \'Informativa Corrente\' LIMIT 1';
        final List<Informativa_Privacy__c> informativaList = (List<Informativa_Privacy__c>) Database.query(query);
        if(informativaList.isEmpty()) return null;
        return informativaList[0];
    }

    public static Informativa_Privacy__c getInformativaByName(String informativaName){
        final String query = 'SELECT ' + String.join(fieldsOf('Informativa_Privacy__c'), ',') + ' FROM Informativa_Privacy__c WHERE Name = :informativaName LIMIT 1';
        final List<Informativa_Privacy__c> informativaList = (List<Informativa_Privacy__c>) Database.query(query);
        if(informativaList.isEmpty()) return new Informativa_Privacy__c();
        return informativaList[0];
    }

    public static List<String> fieldsOf(String className){
        final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        return fields;
    }

    public static List<String> getPrivacyFields() {
        final List<String> privacyFields = new List<String>();

        List<Schema.FieldSetMember> fields = Schema.SObjectType.Contact.fieldSets.Campi_Privacy.getFields();
        for(Schema.FieldSetMember fs: fields){
            privacyFields.add(fs.getFieldPath());
        }

        return privacyFields;
    }

    public static EmailTemplate getTemplateByName(String templateName) {
        return [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE DeveloperName = :templateName LIMIT 1];
    }

    public static OrgWideEmailAddress getOrgWideEmailAddressByDisplayName(String displayName) {
        return [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = :displayName LIMIT 1];
    }

    public static void executeUpdate(Map<Id, SObject> recordsToUpdate){
        if(!recordsToUpdate.isEmpty()){
            List<Database.SaveResult> updateResultList = new List<Database.SaveResult>();

            if(recordsToUpdate.getSobjectType() == Schema.Notifica_Informativa__c.getSObjectType()){
                NotificaInformativaTriggerHandler.skipTrigger = true;
                updateResultList = Database.update((List<Notifica_Informativa__c>)recordsToUpdate.values());
                NotificaInformativaTriggerHandler.skipTrigger = false;
            }else if(recordsToUpdate.getSobjectType() == Schema.Contact.getSObjectType()){
                ContactTriggerHandler.skip = true;
                updateResultList = Database.update((List<Contact>)recordsToUpdate.values());
                ContactTriggerHandler.skip = false;
            }

            for(Database.SaveResult sr : updateResultList){
                if(sr.isSuccess()) {
                    System.debug('Successfully updated record. Record ID: ' + sr.getId());
                }else {            
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred: ' + err.getMessage());                    
                    }
                }
            }
        }
    }
}