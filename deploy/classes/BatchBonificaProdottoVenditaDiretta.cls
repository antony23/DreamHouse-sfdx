global class BatchBonificaProdottoVenditaDiretta implements Database.Batchable<sObject> {
	
	String query;
	
	global BatchBonificaProdottoVenditaDiretta() {
		query = 'SELECT Id,Tipo_Vendibilita__c FROM Prodotto_Vendita_Diretta__c';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
   		try{

   			for(Prodotto_Vendita_Diretta__c pvd : (List<Prodotto_Vendita_Diretta__c>)scope){

   				//pvd.Tipo_Vendibilita__c = (pvd.Obsoleto__c) ? 'X' : '';

   			}

   			ProdottoVenditaDirettaHandler.skipTrigger = true;
   			update scope;
   			ProdottoVenditaDirettaHandler.skipTrigger = false;


		}catch(Exception ex){
			ProdottoVenditaDirettaHandler.skipTrigger = false;
		}

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}