global class EstrazioneFascettarioArretratoScheduled implements Schedulable {
	global void execute(SchedulableContext sc) {
		FTP__c ftp = [SELECT Id FROM FTP__c WHERE Name = 'GENERIC FTP' LIMIT 1];
		Calendario__c copiaUscita = [SELECT Id FROM Calendario__c WHERE Testata__r.Codice__c = 'QRT' AND Data_Uscita_Effettiva__c != null ORDER BY Data_Uscita__c DESC LIMIT 1];
		insert new List<Estrazione_Fascettario__c>{
			new Estrazione_Fascettario__c(
				Fascettario__c = 'Italia + Estero',
				Fresco_Arretrato__c = 'Arretrato',
				FTP__c = ftp.Id,
				Testata__c = 'QRT',
				Copia_uscita__c = copiaUscita.Id,
				Suddivisione_geografica__c = null,
				Fascettari_multipli_per__c = null,
				Note__c = null
			)
		};
	}
}