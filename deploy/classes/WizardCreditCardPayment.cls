public class WizardCreditCardPayment{

    private String quoteId;

    // The Zuora id of the payment page
    public String pageId {
        get {
            if(pageId == null) pageId = '';
            return pageId;
        }
        set;
    }
      
    // Constructor, determines the Zuora page Id based on payment method of the quote
    public WizardCreditCardPayment(ApexPages.StandardController standardController) {
        if(!Test.isRunningTest()){
            standardController.addFields(new List < String > {'zqu__PaymentMethod__c'});
        } 
        
        quoteId = ApexPages.currentPage().getParameters().get('Id');

        zqu__Quote__c quote = (zqu__Quote__c) standardController.getRecord();
        List <zqu__HostedPageLiteSetting__c > settingList = [SELECT zqu__PageId__c, zqu__PaymentMethodType__c, zqu__ComponentName__c FROM zqu__HostedPageLiteSetting__c WHERE zqu__PaymentMethodType__c = :quote.zqu__PaymentMethod__c ORDER BY zqu__Default__c DESC LIMIT 1];
        if(settingList != null && !settingList.isEmpty()) {
            pageId = settingList[0].zqu__PageId__c;
        }
    }

    public PageReference goToQuote(){
        PageReference p = Page.ViewQuote;
        p.getParameters().put('Id',quoteId);
        return p;
    }
}