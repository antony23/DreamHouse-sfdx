@isTest (SeeAllData = true)
private class EditContactCopieSubExtensionTest{

	@isTest
	static void itShould(){
		List<string> subscriptionFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Zuora__Subscription__c').getDescribe().Fields.getMap().values()){
            subscriptionFields.add(ft.getDescribe().getName().toLowerCase());
        }

        List<Zuora__Subscription__c> subscriptions = (List<Zuora__Subscription__c>)
        	Database.query('SELECT ' + String.join(subscriptionFields, ',') + ' ,Zuora__CustomerAccount__r.Sold_To_Salesforce__c' + 
                			' FROM Zuora__Subscription__c ' +
                			' WHERE (NOT Testata__c LIKE \'%Dirett%\') AND Contatto_Principale_Committente__c != null AND Label_Run_IsError__c = false LIMIT 1');
		EditContactCopieSubExtension ext = new EditContactCopieSubExtension(new ApexPages.StandardController(subscriptions.get(0)));
		ext.caricaInfoContatto();
		ext.goToSub();
		ext.saveNewContact();
		ext.subscription.Motivo_modifica_temporanea_indirizzo__c = 'Trasferimento';
		ext.saveNewContact();
		for(EditContactCopieSubExtension.WrapAsset wp : ext.wrapAssetList){
			wp.modificaContatto = true;
		}
		ext.saveNewContact();
	}
}