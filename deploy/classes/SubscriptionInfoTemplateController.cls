global class SubscriptionInfoTemplateController{
    
    public Zuora__Subscription__c sub {get;set;}
    public Contact billContact {get;set;}
    public Contact soldContact {get;set;}
    public List<Zuora__SubscriptionProductCharge__c> chargeList {get; set;}
    public Boolean isPaid {get;set;}
    public String codiceCliente{get;set;}
    public List<WrapAbbonamento> abbonamentiList {get;set;}
    public List<WrapVenditaDiretta> venditeDiretteList {get;set;}
    public String tipoConsegnaAbbonamento {get;set;}
    public String tipoConsegnaVenditaDiretta {get;set;}
    public String descrMetodoDiPagamento {get;set;}
    public Boolean hasSpedizione {get;set;}
    public String language {get;set;}
    public Boolean hasAbbonamento {get;set;}

    public Id subscriptionId{
    	get;
    	set{
    		subscriptionId = value;
            if(ApexPages.currentPage().getParameters().get('Id') != null) subscriptionId = ApexPages.currentPage().getParameters().get('Id');
    		init();
    	}
    }

    
    private void init(){
    	abbonamentiList = new List<WrapAbbonamento>();
        venditeDiretteList = new List<WrapVenditaDiretta>();
        hasSpedizione = false;
        hasAbbonamento = false;

    	List<Zuora__Subscription__c> subList = [SELECT Id, Name, Supporto__c, Metodo_di_Pagamento__c, Tipo_Spese_Spedizione_Abbonamento__c,
    											Zuora__CustomerAccount__r.Bill_To_Salesforce__c, Zuora__CustomerAccount__r.Sold_To_Salesforce__c, Totale__c,
    											Zuora__Status__c, Zuora__Account__r.Codice_SAP__c, Zuora__Account__r.Codice_Cliente__c, Tipo_Spese_Spedizione_Vendite_Dirette__c, 
                                                Tipo_Consegna_Vendite_Dirette__c, Tipo_Consegna_Abbonamento__c, Da_disposizione__c, Zuora__SubscriptionStartDate__c, Zuora__SubscriptionEndDate__c
    											FROM Zuora__Subscription__c
    											WHERE Id =:subscriptionId];

    	if(subList != null && subList.size() > 0){
    		sub = subList.get(0);
    	}
    	if(sub != null){

	    	Set<Id> contactIdSet = new Set<Id>();
	    	contactIdSet.add(sub.Zuora__CustomerAccount__r.Bill_To_Salesforce__c);
	    	contactIdSet.add(sub.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);

	    	Map<Id, Contact> contactMap = new Map<Id, Contact>( [SELECT Id, Name, MailingStreet, MailingCity, Codice_Nazione__c,
	    															MailingState, MailingPostalCode, MailingCountry, Nazione__c
	    															FROM Contact
	    															WHERE Id = :contactIdSet]);
	    	billContact = contactMap.get(sub.Zuora__CustomerAccount__r.Bill_To_Salesforce__c);
	    	soldContact = contactMap.get(sub.Zuora__CustomerAccount__r.Sold_To_Salesforce__c);
  
	    	//Recupero charge
	    	chargeList = [SELECT Id,Name, Zuora__Quantity__c,Testata__c,Tipo_Prodotto__c, Zuora__Price__c, Zuora__ExtendedAmount__c, Supporto__c,
	    					IntQuantita__c, Descrizione_Prodotto__c, NumeroUscite__c, Zuora__EffectiveStartDate__c, Zuora__EffectiveEndDate__c,
	    					Codice_Prodotto_SAP__c, Prodotto_Vendita_Diretta__r.Testata__r.Name,Zuora__Model__c 
	    					FROM Zuora__SubscriptionProductCharge__c 
                            WHERE Zuora__Subscription__c = :sub.Id
                            AND (Zuora__ExtendedAmount__c > 0 OR Tipo_Prodotto__c = 'Sconto')
                            ORDER BY Ordinevisualizzazionetemplate__c ASC];

            for(Zuora__SubscriptionProductCharge__c charge : chargeList){
                System.debug(loggingLevel.Error, '*** charge: ' + charge);
                System.debug(loggingLevel.Error, '*** charge.Prodotto_Vendita_Diretta__r.Testata__r.Name: ' + charge.Prodotto_Vendita_Diretta__r.Testata__r.Name);
                if(charge.Tipo_Prodotto__c == 'Vendita Diretta'){
                    WrapVenditaDiretta wrap = new WrapVenditaDiretta(
                        charge.Codice_Prodotto_SAP__c + ' - ' + charge.Descrizione_Prodotto__c,
                        charge.Supporto__c,
                        charge.Prodotto_Vendita_Diretta__r.Testata__r.Name,
                        language
                    );
                    venditeDiretteList.add(wrap);
                }
            }
	    	
            //metodoDiPagametno
            if(sub.Da_disposizione__c != 'SI'){
                List<MetodoDiPagamento__mdt> metodoDiPagamentoList = [SELECT MasterLabel, Descrizione_Pagamento__c, TranslationENG__c
                                                                        FROM MetodoDiPagamento__mdt 
                                                                        WHERE MasterLabel = :sub.Metodo_di_Pagamento__c 
                                                                        ORDER BY Order__c ASC];
                descrMetodoDiPagamento = sub.Metodo_di_Pagamento__c;
                if(metodoDiPagamentoList != null && metodoDiPagamentoList.size() > 0){
                    descrMetodoDiPagamento = language == 'IT' ? metodoDiPagamentoList.get(0).Descrizione_Pagamento__c : metodoDiPagamentoList.get(0).TranslationENG__c;
                }
            }else{
                descrMetodoDiPagamento = 'Da disposizione';
            }


	    	for(Zuora__SubscriptionProductCharge__c c : chargeList){
	    		if(c.Tipo_Prodotto__c == 'Abbonamento'){
	    			abbonamentiList.add(new WrapAbbonamento(sub,c,language));
                    hasAbbonamento = true;
	    		}
                if(c.Supporto__c == 'Carta' || c.supporto__c =='Cartaceo' || c.supporto__c =='Carta + Digitale'){
                    hasSpedizione = true;
                }
	    	}


	    	isPaid = (sub.Zuora__Status__c == 'Active');
	    	codiceCliente = sub.Zuora__Account__r.Codice_SAP__c != null ? sub.Zuora__Account__r.Codice_SAP__c : sub.Zuora__Account__r.Codice_Cliente__c;

            String nazione = 'Italia';
            if(soldContact.Codice_Nazione__c != 'IT') {
                nazione = 'Estero';
            }

            List<Tipo_Consegna__mdt> consegnaTypeList =  [SELECT Tipo_Consegna_Comunicazioni__c,TipoconsegnacomunicazioniENG__c, Codice_SAP__c 
                                                        FROM Tipo_Consegna__mdt 
                                                        WHERE Codice_SAP__c = :sub.Tipo_Consegna_Vendite_Dirette__c
                                                        OR Codice_SAP__c = :sub.Tipo_Consegna_Abbonamento__c];

            Map<String, Tipo_Consegna__mdt> consegnaTypeMap = new Map<String, Tipo_Consegna__mdt>();
            for(Tipo_Consegna__mdt consegna : consegnaTypeList){
                consegnaTypeMap.put(consegna.Codice_SAP__c, consegna);
            }

            if(sub.Tipo_Consegna_Abbonamento__c != null){
                tipoConsegnaAbbonamento = nazione == 'Italia' ? consegnaTypeMap.get(sub.Tipo_Consegna_Abbonamento__c).Tipo_Consegna_Comunicazioni__c : consegnaTypeMap.get(sub.Tipo_Consegna_Abbonamento__c).TipoconsegnacomunicazioniENG__c;
            }
            if(sub.Tipo_Consegna_Vendite_Dirette__c != null){
                tipoConsegnaVenditaDiretta = nazione == 'Italia' ? consegnaTypeMap.get(sub.Tipo_Consegna_Vendite_Dirette__c).Tipo_Consegna_Comunicazioni__c : consegnaTypeMap.get(sub.Tipo_Consegna_Vendite_Dirette__c).TipoconsegnacomunicazioniENG__c;
            }
            System.debug(loggingLevel.Error, '*** tipoConsegnaAbbonamento: ' + tipoConsegnaAbbonamento);
    	}


    }

    public class WrapVenditaDiretta{
        public String prodotto {get;set;}
        public String supporto {get;set;}
        public String edizioneDigitale {get;set;}
        public String testata {get;set;}

        public WrapVenditaDiretta(String p, String s, String t, String language){
            prodotto = p;
            if(t != null)
            testata = t.toLowerCase();
            supporto = s;

            System.debug(loggingLevel.Error, '*** testata: ' + testata);
            List<Link_Testate__mdt> testataLinkList = [SELECT MasterLabel, Link__c FROM Link_Testate__mdt 
                                                        WHERE MasterLabel LIKE :testata];

            System.debug(loggingLevel.Error, '*** testataLinkList: ' + testataLinkList);
                
            String link;
            if(testataLinkList != null && testataLinkList.size() > 0){
                link = testataLinkList.get(0).Link__c;
            }

            if((supporto == 'Digitale' || supporto=='Carta + Digitale') && String.isNotBlank(link)){
                edizioneDigitale = language == 'IT' ? ' <b><a href="'+link+'">clicchi qui</a> per leggerla </b>' : ' <b>to access your digital edition <a href="'+link+'">click here</a></b>';
            } else{
                edizioneDigitale = language == 'IT' ? '<b>Non inclusa</b>' : '<b>Not included</b>';
            }
        }
    }


    public class WrapAbbonamento{
    	public String testata {get;set;}
    	public String durata {get;set;}
    	public String supporto {get;set;}
    	public Boolean isDigitale {get;set;}
    	public String edizioneDigitale {get;set;}


    	public WrapAbbonamento(Zuora__Subscription__c sub, Zuora__SubscriptionProductCharge__c charge, String language){
    		String formatMonthDate = 'M';
            String formatYearDate = 'yyyy';

            Date startDate = sub.Zuora__SubscriptionStartDate__c;
            Date endDate = sub.Zuora__SubscriptionEndDate__c;

    		/*Date startDate = charge.Zuora__EffectiveStartDate__c;
    		Date endDate = charge.Zuora__EffectiveEndDate__c;*/

    		Datetime startDatetime = Datetime.newInstance(startDate.year(), startDate.month(),startDate.day());
    		Datetime endDatetime = Datetime.newInstance(endDate.year(), endDate.month(),endDate.day());


            String startDateYearString = startDatetime.format(formatYearDate);
            String startDateMonthString = startDatetime.format(formatMonthDate);

            String endDateYearString = endDatetime.format(formatYearDate);
            String endDateMonthString = endDatetime.format(formatMonthDate);

            List<Lista_mesi__mdt> mesiItaList = [SELECT Label,Numero_Mese__c, Nome_Italiano__c FROM Lista_mesi__mdt 
                                                    WHERE Numero_Mese__c=:startDateMonthString OR Numero_Mese__c= :endDateMonthString];
            Map<String, String> mesiItaMap = new Map<String, String>();
            for(Lista_mesi__mdt mese : mesiItaList){
                mesiItaMap.put(mese.Numero_Mese__c, language == 'IT' ? mese.Nome_Italiano__c : mese.Label);
            } 
            String startDateString = mesiItaMap.get(startDateMonthString) + ' ' + startDateYearString;
            String endDateString = mesiItaMap.get(endDateMonthString) + ' ' + endDateYearString;

    		testata = charge.Testata__c;
    		String uscite = String.valueOf(charge.NumeroUscite__c);

    		durata = startDateString + ' / ' + endDateString + ' (' + Integer.valueOf(charge.NumeroUscite__c) + ' numeri)';
    		supporto = charge.Supporto__c;

            List<Link_Testate__mdt> testataLinkList = [SELECT MasterLabel, Link__c FROM Link_Testate__mdt 
                                                    WHERE MasterLabel=:testata];
            
            String link;
            if(testataLinkList != null && testataLinkList.size() > 0){
                link = testataLinkList.get(0).Link__c;
            }

    		if((supporto == 'Digitale' || supporto=='Carta + Digitale') && String.isNotBlank(link)){
    			edizioneDigitale = language == 'IT' ? ' <b><a href="'+link+'">clicchi qui</a> per leggerla </b>' : ' <b>to access your digital edition <a href="'+link+'">click here</a></b>';
    		} else{
    			edizioneDigitale = language == 'IT' ? '<b>Non inclusa</b>' : '<b>Not included</b>';
    		}

    	}
    }


}