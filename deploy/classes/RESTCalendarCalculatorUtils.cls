public class RESTCalendarCalculatorUtils {
	
	public static Map<String,Object> getSubscriptionParameters(Id ratePlanId, Date targetDate){
        System.debug(loggingLevel.Error, '*** ratePlanId: ' + ratePlanId);
        System.debug(loggingLevel.Error, '*** targetDate: ' + targetDate);
		Map<String, Object> resultMap = new Map<String,Object>();
		String testate;
		String dettaglioCopiePerTestata;
		String supporto;
        Integer numVenditeDirette = 0;
		List<Testata__c> testataList = getTestataList(ratePlanId);
		Set<Id> testataIdSet = new Set<Id>();
		for(Testata__c t : testataList){
			testataIdSet.add(t.id);
		}
		List<Id> testataIdList = new List<Id>(testataIdSet);

		Calendario__c startCalendar = getCalendarioStartCopySubscription(testataIdList, targetDate);
        System.debug(loggingLevel.Error, '*** startCalendar: ' + startCalendar);
        Calendario__c endCalendar ;
	
        List<zqu__ProductRatePlanCharge__c> chargeList = getChargeList(ratePlanId);
		System.debug('************** SubscriptionUtil chargeList***********' + '  ' + chargeList);
        Date startDate = startCalendar.Data_Uscita__c;
		Date endDate = startCalendar.Data_Uscita__c;
        //Date startDate = Date.today();
        //Date endDate = Date.today();

        Map<String, Testata__c> testataMap = new Map<String, Testata__c>();
        for(Testata__c t : testataList){
            testataMap.put(t.IssueCalendar__c, t);
        }

        Set<String> supportSet = new Set<String>();
        for(zqu__ProductRatePlanCharge__c charge : chargeList){
            if(charge.TipoProdotto__c == 'Abbonamento'){
                Testata__c testataTmp = testataMap.get(charge.IssueCalendar__c);
                Calendario__c startCalendarTmp = getCalendarioStartCopySubscription(testataTmp.Id, startDate);
                Calendario__c endCalendarTmp = getCalendarioEndCopyByTestata(testataTmp.Id, startDate, Integer.valueOf(charge.Uscite__c));
                System.debug(loggingLevel.Error, '*** startCalendarTmp: ' + startCalendarTmp);
                System.debug(loggingLevel.Error, '*** endCalendarTmp: ' + endCalendarTmp);
                System.debug(loggingLevel.Error, '*** endDate: ' + endDate);
                System.debug(loggingLevel.Error, '*** testataTmp.Id: ' + testataTmp.Id);
                System.debug(loggingLevel.Error, '*** startDate: ' + startDate);
                System.debug(loggingLevel.Error, '*** Integer.valueOf(charge.Uscite__c): ' + Integer.valueOf(charge.Uscite__c));
                if(endCalendarTmp.Data_Uscita__c >= endDate){
                    endDate = endCalendarTmp.Data_Uscita__c;	
                    endCalendar = endCalendarTmp;
                }
                if(String.isBlank(testate)){
                    testate = charge.Testata__c;
                    dettaglioCopiePerTestata = startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }else{
                    testate += ';'+charge.Testata__c;
                    dettaglioCopiePerTestata += ';'+startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }
                if(String.isNotBlank(charge.Supporto__c)){
                    supportSet.add(charge.Supporto__c);
                }
        	}
            if(charge.TipoProdotto__c == 'Vendita diretta'){
               numVenditeDirette++; 
            }
        }
        supporto = String.join(new List<String>(supportSet), ';');

        dettaglioCopiePerTestata = dettaglioCopiePerTestata.length() > 240 ? dettaglioCopiePerTestata.left(240) + '...' : dettaglioCopiePerTestata;
        
        resultMap.put('supporto', supporto);
        resultMap.put('testate', testate);
        resultMap.put('dettaglioCopiePerTestata', dettaglioCopiePerTestata);
        resultMap.put('calendarioInizio', startCalendar.Id);
        resultMap.put('calendarioFine',endCalendar.Id);
        resultMap.put('numVenditeDirette', numVenditeDirette);
        
        Decimal numWeeks = ( (Decimal) startDate.daysBetween(endDate))/7;
        Integer numWeeksInt = numWeeks.round(System.RoundingMode.CEILING).intValue();
        endDate  = startDate.addDays(7*numWeeksInt);
        if(numWeeksInt == 0) numWeeksInt = 1;
        resultMap.put('startDate', startDate);
        resultMap.put('endDate', endDate);
        resultMap.put('term', numWeeksInt);
        resultMap.put('termPeriodType', 'Week');
        resultMap.put('period', 'Specific Weeks');

        return resultMap;
	}   

    // Aggiunto filtro sulla data estrazione fascettario per determinare la prima copia disponibile 
	private static Calendario__c getCalendarioStartCopySubscription(Id testataId, Date startDate){
        List<Calendario__c> calendarioList = [SELECT Name, Data_Uscita__c, Descrizione__c, Numero_Uscita__c, Data_Uscita_Effettiva__c, Data_Uscita_Attesa__c
                                            FROM Calendario__c 
                                            WHERE Testata__c = :testataid 
                                            AND Data_Uscita__c >= :startDate
                                            AND Data_Uscita_Effettiva__c = null
                                            ORDER BY Data_Uscita__c asc];
        if(calendarioList != null && calendarioList.size() >0){
            return calendarioList.get(0);
        }
        return null;
    }

    private static Calendario__c getCalendarioStartCopySubscription(List<Id> testataIdList, Date startDate){
        List<Calendario__c> calendarioList = [SELECT Name, Data_Uscita__c, Descrizione__c, Numero_Uscita__c, Data_Uscita_Effettiva__c, Data_Uscita_Attesa__c
                                            FROM Calendario__c 
                                            WHERE Testata__c = :testataIdList 
                                            AND Data_Uscita__c >= :startDate
                                            AND Data_Uscita_Effettiva__c = null
                                            ORDER BY Data_Uscita__c asc
                                            LIMIT 1];
        if(calendarioList != null && calendarioList.size() >0){
            return calendarioList.get(0);
        }
        return null;
    }

    private static List<Testata__c> getTestataList(Id ratePlanId){
        String tipoProdotto = 'Abbonamento';
        List<zqu__ProductRatePlanCharge__c> chargeList = [SELECT Id,IssueCalendar__c FROM zqu__ProductRatePlanCharge__c WHERE zqu__ProductRatePlan__c = :ratePlanId
                                                            AND TipoProdotto__c = :tipoProdotto];
        Set<String> testataNameList = new Set<String>();
        for(zqu__ProductRatePlanCharge__c charge : chargeList){
            if(!String.isBlank(charge.IssueCalendar__c)){
                testataNameList.add(charge.IssueCalendar__c);
            }
        }

        List<Testata__c> testataList = [SELECT Id, Name, IssueCalendar__c FROM Testata__c WHERE IssueCalendar__c = :testataNameList];
        return testataList;
    }

    private static Calendario__c getCalendarioEndCopyByTestata(Id testataId, Date startDate, Integer copiesNum){
        List<Calendario__c> calendarioList = [SELECT Name, Data_Uscita__c, Descrizione__c, Numero_Uscita__c 
                                                FROM Calendario__c 
                                                WHERE Testata__c = :testataId 
                                                AND Data_Uscita__c >= :startDate
                                                ORDER BY Data_Uscita__c];

        if(calendarioList != null && calendarioList.size() >= copiesNum){
            return calendarioList.get(copiesNum-1);
        }
        return null;
    }

    private static List<zqu__ProductRatePlanCharge__c> getChargeList(String ratePlanId){
        String whereClause = 'zqu__ProductRatePlan__c = :ratePlanId AND (TipoProdotto__c = \'Abbonamento\' OR TipoProdotto__c = \'Vendita Diretta\')' ;
        String additionalFields = 'zqu__ProductRatePlan__r.zqu__Product__r.Name, zqu__ProductRatePlan__r.Name, '+
            'zqu__ProductRatePlan__r.Supporto__c, Tier_Price__c';
        String queryString = Utils.getSelectAllQuery('zqu__ProductRatePlanCharge__c', whereClause, null, additionalFields, true);
        List<zqu__ProductRatePlanCharge__c> chargeList = (List<zqu__ProductRatePlanCharge__c>) Database.query(queryString);

        return chargeList;
    }

    public static SpeseDiSpedizioneResponse invokeSpeseDiSpedizione(List<RestSubscription.RatePlanCharge> chargeList,Map<String,String> mapTestate, String codiceSap, String codiceNazioneSpedizione, Boolean skipSpeseAbbonamenti,Map<String,List<RestSubscription.WrapperTipoSpese>> mapTipoSpese) {

        if(codiceNazioneSpedizione == null || String.isBlank(codiceNazioneSpedizione) || skipSpeseAbbonamenti){
            return new SpeseDiSpedizioneResponse();
        }

        System.debug('@@r18 chargelist : '+chargeList);
        System.debug('@@r18 map : '+mapTestate);

        for(String str : mapTipoSpese.keySet()){
            System.debug('@@ r18 mapKey : '+str);
            System.debug('@@ r18 mapValue : '+mapTipoSpese.get(str));
        }

        System.debug('@@ r18 params : '+codiceSap+' - '+codiceNazioneSpedizione+' - '+skipSpeseAbbonamenti);

        String tipoSpedizioneAbbonamento = '';
        String tipoConsegnaAbbonamenti = '';

        if(mapTipoSpese.containsKey(codiceSap)){

            List<RestSubscription.WrapperTipoSpese> tempList = mapTipoSpese.get(codiceSap);
            for(RestSubscription.WrapperTipoSpese w : tempList){

                if(codiceSap.equalsIgnoreCase('ZM') || codiceSap.equalsIgnoreCase('ZN') || codiceSap.equalsIgnoreCase('ZL')){
                    
                    tipoSpedizioneAbbonamento = w.tipoSpedizione;
                    tipoConsegnaAbbonamenti = w.tipoConsegna;

                    break;

                }else if(codiceNazioneSpedizione.equalsIgnoreCase('IT') && String.isNotBlank(w.codiceNazione) && w.codiceNazione.equalsIgnoreCase('IT')){

                    tipoSpedizioneAbbonamento = w.tipoSpedizione;
                    tipoConsegnaAbbonamenti = w.tipoConsegna;

                    break;

                }else if(!codiceNazioneSpedizione.equalsIgnoreCase('IT') && String.isBlank(w.codiceNazione)){

                    tipoSpedizioneAbbonamento = w.tipoSpedizione;
                    tipoConsegnaAbbonamenti = w.tipoConsegna;

                    break;

                }
            }

        }

        System.debug('@@r18 recuperati : '+tipoSpedizioneAbbonamento+' - '+tipoConsegnaAbbonamenti);

        SpeseDiSpedizioneRequest request = new SpeseDiSpedizioneRequest();
        request.nazioneSpedizione = codiceNazioneSpedizione;
        request.tipoSpedizioneAbbonamenti = tipoSpedizioneAbbonamento;
        request.tipoConsegnaAbbonamenti = tipoConsegnaAbbonamenti;
        request.abbonamenti = new List<SpeseDiSpedizioneAbbonamento>();

        for(RestSubscription.RatePlanCharge charge : chargeList){

            if(charge.supporto == null || String.isBlank(charge.supporto) || charge.supporto.equalsIgnoreCase('Digitale')) continue;

            if(charge.tipoCharge.equalsIgnoreCase('Abbonamento')){
                SpeseDiSpedizioneAbbonamento rec = new SpeseDiSpedizioneAbbonamento();
                rec.testata = mapTestate.get(charge.ratePlanId);
                rec.numeroCopie = (String.isNotBlank(charge.uscite)) ? Integer.valueOf(charge.uscite) : null;
                rec.quantita = (charge.quantity != null) ? Integer.valueOf(charge.quantity) : null;
                request.abbonamenti.add(rec);

                System.debug('@@r18 charge : '+charge);

            }

        }

        JSONGenerator jGenerator = JSON.createGenerator(true);
        jGenerator.writeStartObject();
        jGenerator.writeStringField('NazioneSpedizione', request.nazioneSpedizione);
        if(request.tipoSpedizioneVenditeDirette == null) request.tipoSpedizioneVenditeDirette = '';
        if(request.tipoSpedizioneAbbonamenti == null) request.tipoSpedizioneAbbonamenti = '';
        if(request.tipoSpedizioneVenditeDirette != null) jGenerator.writeStringField('TipoSpedizioneVenditeDirette', request.tipoSpedizioneVenditeDirette);
        if(request.tipoSpedizioneAbbonamenti != null) jGenerator.writeStringField('TipoSpedizioneAbbonamenti', request.tipoSpedizioneAbbonamenti);
        if(request.tipoConsegnaVenditeDirette != null) jGenerator.writeStringField('TipoConsegnaVenditeDirette', request.tipoConsegnaVenditeDirette);
        if(request.tipoConsegnaAbbonamenti != null) jGenerator.writeStringField('TipoConsegnaAbbonamenti', request.tipoConsegnaAbbonamenti);
        if(request.venditeDirette != null) jGenerator.writeObjectField('VenditeDirette',request.venditeDirette);
        if(request.abbonamenti != null) jGenerator.writeObjectField('Abbonamenti',request.abbonamenti);
        jGenerator.writeEndObject();

        String restRequestMessage = jGenerator.getAsString();

        Http rest = new Http();
        HttpRequest restRequest = new HttpRequest();
        restRequest.setHeader('Content-Type', 'application/json');
        restRequest.setEndpoint(Setting__c.getValues('CalcoloSpeseSpedizione').URL__c);
        restRequest.setMethod('POST');
        restRequest.setBody(restRequestMessage);
        HttpResponse restResponse = rest.send(restRequest);
        String restResponseJson = restResponse.getBody();
        
        SpeseDiSpedizioneResponseRaw raw = null;
        try {

            raw = (SpeseDiSpedizioneResponseRaw) JSON.deserialize(restResponseJson, SpeseDiSpedizioneResponseRaw.class);

            System.debug('@@r18 raw : '+raw);
        } catch(Exception e) {

            raw = new SpeseDiSpedizioneResponseRaw();
            raw.data = new SpeseDiSpedizioneResponseRaw1();
            raw.data.error = e.getMessage() + '. ' + e.getStackTraceString();

        }

        SpeseDiSpedizioneResponse response = new SpeseDiSpedizioneResponse();
        if(raw != null && raw.data != null){

            response.error = raw.data.error;
            if(response.error != null && response.error.length() > 0){

                for(SpeseDiSpedizioneAbbonamento reqAbb : request.abbonamenti){
                    response.abbonamenti.add(0);
                }

            }else{

                if(raw.data.abbonamenti != null){

                    for(String spesaAbbonamento : raw.data.abbonamenti){

                        Decimal val = Decimal.valueOf(spesaAbbonamento);

                        if(skipSpeseAbbonamenti && codiceNazioneSpedizione == 'IT'){
                            val = 0;
                        }
                        response.abbonamenti.add(val);
                    }

                }

                if(raw.data.TotaleAbbonamenti_Ordinaria != null){
                    response.totaleAbbonamenti_Ordinaria = raw.data.TotaleAbbonamenti_Ordinaria;
                }

                if(raw.data.TotaleAbbonamenti_Prioritaria != null){
                    response.totaleAbbonamenti_Prioritaria = raw.data.TotaleAbbonamenti_Prioritaria;
                }

            }

        }

        return response;
    }

    public class RatePlanCharge{
        public String productRatePlanChargeId;
        public Integer specificBillingPeriod;
        public String billingPeriod;
        public String tipoCharge;
        public Decimal price;
        public String uscite;
        public Decimal quantity;
        public Decimal discountAmount;
        public Decimal discountPercentage;
        public String discountLevel;
        public String descrizioneProdotto;
        public String ratePlanId;
        public String supporto;
    }

    public class SpeseDiSpedizioneRequest {
        public String nazioneSpedizione {get;set;}
        public String tipoSpedizioneVenditeDirette {get;set;}
        public String tipoSpedizioneAbbonamenti {get;set;}  
        public String tipoConsegnaVenditeDirette {get;set;}
        public String tipoConsegnaAbbonamenti {get;set;}  
        public List<SpeseDiSpedizioneVenditaDiretta> venditeDirette {get;set;}
        public List<SpeseDiSpedizioneAbbonamento> abbonamenti {get;set;}

    }

    public class SpeseDiSpedizioneVenditaDiretta {
        public Decimal Peso {get;set;}
        public Integer Quantita {get;set;}
    }

    public class SpeseDiSpedizioneAbbonamento {
        public String Testata {get;set;}
        public Integer NumeroCopie {get;set;}
        public Integer Quantita {get;set;}
    }

    public class SpeseDiSpedizioneResponse {
        public Decimal venditeDirette {get;set;}
        public List<Decimal> abbonamenti {get;set;}
        public Decimal totaleAbbonamenti_Ordinaria {get;set;}
        public Decimal totaleAbbonamenti_Prioritaria {get;set;}
        public String error {get;set;}
        public SpeseDiSpedizioneResponse(){
            abbonamenti = new List<Decimal>();
        }
    }

    public class SpeseDiSpedizioneResponseRaw{
        public SpeseDiSpedizioneResponseRaw1 data {get;set;}
    }

    public class SpeseDiSpedizioneResponseRaw1{
        public String VenditeDirette {get;set;}
        public List<String> Abbonamenti {get;set;}
        public Decimal TotaleAbbonamenti_Ordinaria {get;set;}
        public Decimal TotaleAbbonamenti_Prioritaria {get;set;}
        public String error {get;set;}
    }
}