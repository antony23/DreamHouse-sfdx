public class CtrlAddressValidation{

    public class CtrlAddressValidationException extends Exception{}

    public String data {get;set;}

    @RemoteAction
    public static Object getResources(String resource)
    {
        System.debug(loggingLevel.Error, '*** JSON.deserializeUntyped(getData(resource)): ' + JSON.deserializeUntyped(getData(resource)));
        return JSON.deserializeUntyped(getData(resource));
    }

    public void getResources()
    {
        String resource = ApexPages.CurrentPage().getParameters().get('resource');
        String civico = ApexPages.currentPage().getParameters().get('civico');
        if(String.isNotBlank(civico)){
            resource += '&civico=' + civico;
        }
        data = getData(resource);
        System.debug(loggingLevel.Error, '*** data: ' + data);
    }

    public static String getData(String resource)
    {
        HttpRequest req = new HttpRequest();
        //ServiceEndpoint__c endpoint = ServiceEndpoint__c.getValues('AccountMock');
        //String url = endpoint.URL__c;
        if(resource.containsIgnoreCase('strade')){
            
            Integer startIndex = resource.indexOf('strade');
            Integer endIndex = resource.indexOf('?idComune=');
            String reqString = resource.substring(startIndex+7, endIndex);

            /*String encodedString = reqString.replace('%20', '');
            encodedString = EncodingUtil.urlEncode(encodedString, 'UTF-8');
            encodedString = encodedString.replace('+', '%20');*/
            String encodedString = reqString;

            //Se cambia l'url da cambiare bisogna rivalutare questa stringa
            System.debug(loggingLevel.Error, '*** resource: ' + resource);
            System.debug(loggingLevel.Error, '*** resource.substring(0, startIndex+7): ' + resource.substring(0, startIndex+7));
            System.debug(loggingLevel.Error, '*** encodedString: ' + encodedString);
            System.debug(loggingLevel.Error, '*** resource.substring(endIndex, resource.length()): ' + resource.substring(endIndex, resource.length()));
            System.debug(loggingLevel.Error, '*** endIndex: ' + endIndex);
            System.debug(loggingLevel.Error, '*** resource.length(): ' + resource.length());
            resource = resource.substring(0, startIndex+7)+encodedString+resource.substring(endIndex, resource.length());

        }

        String url = Setting__c.getInstance('DomusCap').URL__c+resource;
        req.setEndpoint(url);
        req.setTimeout(120000);
        req.setMethod('GET');
        System.debug(url);
        HTTP http = new Http();
        HttpResponse response = http.send(req);
        System.debug(response.getBody());
        return response.getBody();
    }


    @RemoteAction
    public static Map<String,Object> getSObject(String sobjectType,String sobjectId,Map<String,String> fieldMap){
        String fields = String.join(fieldMap.values(),',');
        sobject so = Database.query('Select '+fields+' from '+sobjectType+' where id =: sobjectId');
        Contact c = (Contact) so;
        Map<String,Object> valueMap = new Map<String,Object>();
        for(String s : fieldMap.keySet()){
            System.debug(loggingLevel.Error, s + ': ' + so.get(fieldMap.get(s)));
            valueMap.put(s,so.get(fieldMap.get(s)));
        }
        return valueMap;
    }

    @RemoteAction
    public static void updateAddress(String sobjectType,String sobjectId,Map<String,String> fieldMap,Map<String,Object> valueMap)
    {
        SObject so = Schema.getGlobalDescribe().get(sobjectType).newSObject();

        so.put('Id',sobjectId);
        for(String s : fieldMap.keySet()){
            if(s != 'forceEdit'){
                String v = String.isNotBlank((String)valueMap.get(s)) ? ((String)valueMap.get(s)).toUpperCase() : '';
                so.put(fieldMap.get(s),v);
            }else{
                so.put(fieldMap.get(s),valueMap.get(s));
            }
        }

        update so;

    }



}