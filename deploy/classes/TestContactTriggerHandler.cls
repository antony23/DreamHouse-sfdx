@isTest (SeeAllData=true)
public class TestContactTriggerHandler {
	
	@isTest static void testM(){

		Account b2c = new Account(
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId(),
			Name = 'Test Account'
		);
		insert b2c;

		Test.setMock(HttpCalloutMock.class, new MockTestClass('CheckIban'));

		Contact contatto = new Contact(
			AccountId = b2c.Id,
			MainContact__c = true,
			FirstName = 'Test',
			LastName = 'Contact',
			MailingPostalCode = '20092',
			Codice_IBAN__c = 'IT02D0326802801052879623060'
		);
		insert contatto;

		// viene compilato il codice cliente in creazione
		String codiceClienteGenerato = [SELECT CodiceCliente__c FROM Contact WHERE Id = :contatto.Id].CodiceCliente__c;
		System.assert(codiceClienteGenerato != null);

		// alcuni campi del contatto vengono riportati sull'account
		b2c = [SELECT Id,Name,Codice_Cliente__c
				FROM Account 
				WHERE Id = :b2c.Id];
		//System.assertEquals(b2c.Name, contatto.FirstName + ' ' + contatto.LastName);
		System.assertEquals(b2c.Codice_Cliente__c, codiceClienteGenerato);

		// se cambio un campo indirizzo si aggiorna l'indirizzo su tutti gli oggetti correlati
		Asset copia = new Asset(
			Name = 'copia di test',
			ContactId = contatto.Id,
			Inviata__c = false
		);
		insert copia;
		Entitlement__c ent = new Entitlement__c(
			Contact__c = contatto.Id,
			Stato__c = 'Da inviare'
		);
		insert ent;
		Avviso_di_Rinnovo__c avviso = new Avviso_di_Rinnovo__c(
			Contatto__c = contatto.Id,
			Contact_Sold_To__c = contatto.Id,
			Stato_Avviso__c = 'Nuovo'
		);
		insert avviso;

		Zuora__CustomerAccount__c billingAccount = new Zuora__CustomerAccount__c(
			Zuora__Account__c = b2c.Id,
			Zuora__BillToId__c = 'id zuora',
			Bill_To_Salesforce__c = contatto.Id,
			Zuora__SoldToId__c = 'id zuora',
			Sold_To_Salesforce__c = contatto.Id
		);
		insert billingAccount;

		contatto.MailingPostalCode = '20100';
		contatto.Codice_IBAN__c = '';
		update contatto;		

		// non puoi cancellare il contatto principale
		Database.DeleteResult result = Database.delete(contatto, false);
		System.assert(! result.isSuccess());
	
	}
}