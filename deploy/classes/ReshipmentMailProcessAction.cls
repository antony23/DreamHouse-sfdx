public class ReshipmentMailProcessAction {
	private static final String MT10 = 'MT10';	// Da inviare per ogni copia rispedita
	private static final String SENDER = 'Editoriale Domus - No reply';
	private static final String[] BCCs = new String[]{'sfdc_cc_notifichecrm@edidomus.it'};

	@InvocableMethod(label='Notifica Rispedizione Copia' description='Invia una email al Contact_Bill_To quando una copia di un abbonamento viene rispedita.')
	public static void sendEmail(List<Asset> copie) {
		System.debug('ReshipmentMailProcessAction started');

		final Blocco_Selettivo_Notifiche__c bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
		final List<EmailDataBundle> data = fetchEmailData(copie, bsn);
		final List<Messaging.SingleEmailMessage> emails = EmailSendingUtil.prepareEmails(data, EmailSendingUtil.getOrgWideEmailAddressByDisplayName(SENDER), BCCs);
		final List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails, false);

		EmailSendingUtil.debug(results, data);

		System.debug('ReshipmentMailProcessAction stopped');
	}

	@TestVisible
	// Recupera i dati necessari per l'invio delle email
	private static List<EmailDataBundle> fetchEmailData(List<Asset> copie, Blocco_Selettivo_Notifiche__c bsn) {
		final Map<String, EmailTemplate> templates = EmailSendingUtil.loadTemplates(new String[]{MT10});
		final Map<Id, Zuora__Subscription__c> subscriptions = new Map<Id, Zuora__Subscription__c>([SELECT Id, Name, Contact_Bill_To__c FROM Zuora__Subscription__c WHERE Id IN :fetchSubscriptionIds(copie)]);
		final Map<Id, Contact> billToContacts = new Map<Id, Contact>([SELECT Id, Name, Email, Codice_Nazione__c FROM Contact WHERE Id IN :fetchContactIds(subscriptions.values()) AND Account.Notifiche_Email_Disabilitate__c = False AND (NOT(Email IN ('', NULL)))]);
		final List<EmailDataBundle> data = new List<EmailDataBundle>();

		Zuora__Subscription__c sub;
		Contact billToContact;

		for(Asset copia : copie) {
			sub = subscriptions.get(copia.SubscriptionAllCopy__c);
			if(sub == null) continue; // Subscription non trovata oppure Asset non rispedito

			billToContact = billToContacts.get(sub.Contact_Bill_To__c);
			if(billToContact == null) continue; // Contact non trovato oppure campo Email vuoto

			if(bsn.MT10_Abilitato__c && !copia.Notifica_Rispedizione__c && copia.Inviata__c && copia.Rispedizione__c) {
				data.add(new EmailDataBundle(copia, billToContact, templates.get(MT10)));
			}
		}

		System.debug('EmailDataBundles: ' + data);
		return data;
	}
	
	// Recupera l'id delle Subscriptions a cui appartengono gli Asset
	private static Set<Id> fetchSubscriptionIds(List<Asset> copie) {
		final Set<Id> ids = new Set<Id>();

		for(Asset copia : copie) {
			if(copia.Inviata__c  && copia.Rispedizione__c) {
				ids.add(copia.SubscriptionAllCopy__c);
			}
		}

		System.debug('Subscription-Ids: ' + ids);
		return ids;
	}

	// Recupera l'id dei Contact_Bill_To__c (Referente di fatturazione)
	private static Set<Id> fetchContactIds(List<Zuora__Subscription__c> subscriptions) {
		final Set<Id> ids = new Set<Id>();

		for(Zuora__Subscription__c sub : subscriptions) {
			ids.add(sub.Contact_Bill_To__c);
		}

		System.debug('Contact-Ids: ' + ids);
		return ids;
	}
}