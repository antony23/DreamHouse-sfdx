public with sharing class EditContactExtension {

    Contact cont;
    public Contact[] duplicateRecords {get;set;}
    public Id rcid {get;set;}
    public Id rcidB2C {get;set;}
    public Boolean isNewAccountB2C {get;set;}
    public Boolean isAccountB2C {get;set;}
    public Boolean ignoreDuplicate = false;
    public Boolean isAdminProfessional {get;set;}
    public Boolean isProfessional {get;set;}
    public Boolean isProfessionalBackOffice {get;set;} // CR Professional
	
    public EditContactExtension(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()) controller.addFields(new List<String>{'Email','Tipo_Cliente__c'});
        cont = (Contact) controller.getRecord();
        isNewAccountB2C = ApexPages.CurrentPage().getParameters().get('newAccountB2C') == '1';
        duplicateRecords = new Contact[]{};
    
    //LUIGI
        if(cont.AccountId != null ) {
        rcid = [SELECT Id, Name, RecordTypeId FROM Account WHERE Id = :cont.AccountId].RecordTypeId;  
        Account acct = [SELECT Id, Name, RecordTypeId, Tipo_Cliente__c FROM Account WHERE Id = :cont.AccountId ];
        cont.Tipo_Cliente__c = acct.Tipo_Cliente__c;
        rcidB2C = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId();
        }
        if(rcid == rcidB2C) {
                isAccountB2C = true;
        } else {
                isAccountB2C = false;
            }
        Profile profile =  [SELECT ID, Name FROM Profile WHERE ID = :UserInfo.getProfileId()];
        String profilename = profile.name;
        string adminprof = 'Admin - Professional';
        string prof = 'Professional';
        String backoffice = 'Professional BackOffice'; // CR Professional
        
        if (profilename.contains(adminprof)) {
            isAdminProfessional = true;
        } else {
            isAdminProfessional = false;
        }

        if (profilename.contains(prof)) {
            isProfessional = true;
        } else {
            isProfessional = false;
        }

        /*** CR Professional ***/
        if (profilename.contains(backoffice)) {
            isProfessionalBackOffice = true;
        } else {
            isProfessionalBackOffice = false;
        }
        /*** CR Professional - Fine ***/


    //LUIGI

    }
    public void changeIgnoreDuplicate(){
        duplicateRecords = new Contact[]{};
        ignoreDuplicate = !ignoreDuplicate;
    }

    public PageReference customSaveContact(){
        PageReference p = null;
        duplicateRecords = new Contact[]{};

        Boolean codiceFiscaleValido = false;

        // il codice fiscale va forzato solo se � stato inserito
        if(String.isBlank(cont.Codice_Fiscale__c)) {
            cont.Codice_Fiscale_Forzato__c = false;
        } else {
            cont.Codice_Fiscale__c = cont.Codice_Fiscale__c.toUpperCase();
        }

        try{
            if(String.isNotBlank(cont.Codice_Nazione__c) && String.isNotBlank(cont.Codice_Fiscale__c)){
                String codiceNazione = cont.Codice_Nazione__c.toUpperCase();
                
                try {
                    codiceFiscaleValido = (!cont.Codice_Fiscale_Forzato__c &&
                                            codiceNazione == 'IT') ? UtilCodiceFiscale.validateCF(
                                                                        cont.FirstName,
                                                                        cont.LastName,
                                                                        cont.Sesso__c,
                                                                        cont.Birthdate,
                                                                        cont.Luogo_di_Nascita__c,
                                                                        cont.Codice_Fiscale__c
                                                                    ) : true;
                } catch(Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    return null;
                } 
            }else{
                codiceFiscaleValido = true;
            }
        }catch(Exception e){
            System.debug(e.getMessage() + '. ' + e.getStackTraceString());
            codiceFiscaleValido = false;
        }
        if(!codiceFiscaleValido){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Codice fiscale non valido'));
            return null;
        }
        

        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.AllowSave = ignoreDuplicate;

        Database.SaveResult sr;
        System.debug(loggingLevel.Error, '*** cont: ' + cont);
        if(cont.Id != null){
            sr = Database.update(cont, dml);
        }else{
            sr = Database.insert(cont, dml);
        }

        if(! sr.isSuccess() ){
            for(Database.Error error : sr.getErrors()){
                if(error instanceof Database.DuplicateError){
                    Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Duplicate Error: ' + duplicateResult.getErrorMessage());
                    ApexPages.addMessage(errorMessage);
                    
                    Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
                    Datacloud.MatchResult matchResult = matchResults[0];
                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        duplicateRecords.add( (Contact) matchRecord.getRecord());
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                }
            }
        }else{
            p = new ApexPages.StandardController(cont).view();
        }

        return p;

    }

    /*** CR Professional ***/
    public void checkContactIBAN(){

        if(cont.Codice_IBAN__c != null && String.isNotBlank(cont.Codice_IBAN__c)){

            String iban = cont.Codice_IBAN__c.deleteWhiteSpace().replaceAll('[^a-zA-Z0-9]', '');
            Boolean ibanValido = true;
            String error = '';
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            String endpoint = Setting__c.getInstance('ValidateIban').URL__c;
            endpoint += iban;
            req.setEndpoint(endpoint);
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            HttpResponse res = h.send(req);
            System.debug(res.getBody());
            WrapperValidationIBAN wrap = null;
            WrapperErrorIBAN wraperror = null;
            try{
                wrap = (WrapperValidationIBAN) JSON.deserialize(res.getBody(),WrapperValidationIBAN.class);
                System.debug('*** Response Valid: ' + wrap);
            }catch(Exception ex){
                wraperror = (WrapperErrorIBAN) JSON.deserialize(res.getBody(),WrapperErrorIBAN.class);
                System.debug('*** Response Error: ' + wraperror);
            }

            if(wraperror != null){

                ibanValido = false;
                for(CommonWrapper.Errors err : wraperror.errors){
                    error += err.code + ' - ' + err.message + '<br/>';
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));

            }else if(wrap != null){

                Set<String> errorCodes = new Set<String>{'201','202','203','205'};
                if(wrap.validations != null && !wrap.validations.isEmpty()){
                    
                    for(CommonWrapper.Validation val : wrap.validations){
                        if(errorCodes.contains(val.code)){
                            ibanValido = false;
                            error += val.code + ' - ' + val.message + '<br/>';
                        }
                    }
                }

                if(ibanValido) {

                    cont.Codice_IBAN__c = iban;
                    cont.BIC__c = wrap.bank_data.bic;
                    cont.Filiale__c = wrap.bank_data.branch;
                    cont.Nazione_Banca__c = wrap.bank_data.country;
                    cont.Citta_Banca__c = wrap.bank_data.city;
                    cont.CAP_Banca__c = wrap.bank_data.zip;

                    String bank = wrap.bank_data.bank;
                    if(bank != null && String.isNotBlank(bank)) {
                        if(bank.length() > 60) {
                            cont.Istituto_di_Credito__c = bank.substring(0, 60);
                        } else {
                            cont.Istituto_di_Credito__c = bank;
                        }
                    }

                    String address = wrap.bank_data.address;
                    if(address != null && String.isNotBlank(address)) {
                        if(address.length() > 35) {
                            cont.Indirizzo_Filiale__c = address.substring(0, 35);
                        } else {
                            cont.Indirizzo_Filiale__c = address;
                        }
                    }

                    String state = wrap.bank_data.state;
                    if(state != null && String.isNotBlank(state)) {
                        if(state.length() <= 3) {
                            cont.Provincia_Banca__c = state;
                        }
                    }

                    String countryISO = wrap.bank_data.country_iso;
                    if(countryISO != null && String.isNotBlank(countryISO)){
                        if(countryISO.equalsIgnoreCase('IT')){
                            cont.Paese_Banca__c = wrap.bank_data.country_iso;
                            cont.Numero_Conto_Corrente__c = wrap.bank_data.account;
                            cont.Chiave_Banca__c = iban.substring(5, 15);
                            cont.Chiave_Controllo__c = iban.substring(4, 5);
                        }
                    }

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, AppConstants.IBAN_VALIDO));
                } else {
                    cont.BIC__c = null;
                    cont.Istituto_di_Credito__c = null;
                    cont.Filiale__c = null;
                    cont.Indirizzo_Filiale__c = null;
                    cont.Nazione_Banca__c = null;
                    cont.Citta_Banca__c = null;
                    cont.Provincia_Banca__c = null;
                    cont.CAP_Banca__c = null;

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
                }
            }
        } else {
            cont.BIC__c = null;
            cont.Istituto_di_Credito__c = null;
            cont.Filiale__c = null;
            cont.Indirizzo_Filiale__c = null;
            cont.Nazione_Banca__c = null;
            cont.Citta_Banca__c = null;
            cont.Provincia_Banca__c = null;
            cont.CAP_Banca__c = null;

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, AppConstants.NO_IBAN));
        }
    }

    public void cleanContactIBAN() {
        cont.Codice_IBAN__c = null;
        cont.Descrizione_Errore_IBAN__c = null;
        cont.IBAN_Valido__c = false;
        cont.BIC__c = null;
        cont.Istituto_di_Credito__c = null;
        cont.Filiale__c = null;
        cont.Indirizzo_Filiale__c = null;
        cont.Nazione_Banca__c = null;
        cont.Paese_Banca__c = null;
        cont.Citta_Banca__c = null;
        cont.Provincia_Banca__c = null;
        cont.CAP_Banca__c = null;
        cont.Numero_Conto_Corrente__c = null;
        cont.Chiave_Banca__c = null;
        cont.Chiave_Controllo__c = null;
    }
    /*** CR Professional - Fine ***/
}