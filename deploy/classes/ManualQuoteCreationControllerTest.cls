@isTest (SeeAllData = true)
private class ManualQuoteCreationControllerTest{
	
	@isTest
	static void test1(){
		AccountTriggerHandler.skip = true;
		SubscriptionTriggerHandler.skipTrigger = true;
		ManualQuoteCreationController ctrl = null;

		Account a = new Account(Name = 'test');
		insert a;

		Zuora__Subscription__c sub1 = new Zuora__Subscription__c(Name = 'test', Label_Run_processed__c = false, Zuora__Status__c = 'Pending Activation', Numero_di_Cicli_Text__c = '1', Zuora__Account__c = a.Id, Zuora__SubscriptionEndDate__c = Date.today(), Metodo_di_pagamento__c = 'Carta di credito' );
		insert sub1;
		ctrl = new ManualQuoteCreationController(new ApexPages.StandardController(sub1));
		ctrl.goToAvviso();
		ctrl.createQuoteAction();

		sub1.Label_Run_processed__c = true;
		sub1.Label_Run_IsError__c = false;
		update sub1;

		Zuora__Subscription__c sub2 = new Zuora__Subscription__c(Name = 'test', Subscription_Precedente__c = sub1.Id);
		insert sub2;
		ctrl.createQuoteAction();

		delete sub2;

		zqu__Quote__c quoteDiRinnovo = new zqu__Quote__c(Name = 'test', Subscription_Precedente__c = sub1.Id, zqu__Status__c = 'Confirmed');
		insert quoteDiRinnovo;
		ctrl.createQuoteAction();	

		delete quoteDiRinnovo;
		ctrl.createQuoteAction();	

		sub1.Zuora__Status__c = 'Active';
		update sub1;
		ctrl.createQuoteAction();	

		zqu__ProductRatePlan__c offertaDiRinnovo1 = [SELECT Id,zqu__ZuoraId__c FROM zqu__ProductRatePlan__c WHERE TipoProdotto__c = 'Vendita Diretta' LIMIT 1];
		Zuora__SubscriptionRatePlan__c ratePlan = new Zuora__SubscriptionRatePlan__c(
			Zuora__Subscription__c = sub1.Id,
			Original_Product_RatePlan__c = offertaDiRinnovo1.Id,
			Zuora__OriginalProductRatePlanId__c = offertaDiRinnovo1.zqu__ZuoraId__c
		);
		insert ratePlan;
		ctrl.createQuoteAction();	

		offertaDiRinnovo1 = [SELECT Id,zqu__ZuoraId__c FROM zqu__ProductRatePlan__c WHERE TipoProdotto__c = 'Abbonamento' LIMIT 1];
		ratePlan.Original_Product_RatePlan__c = offertaDiRinnovo1.Id;
		ratePlan.Zuora__OriginalProductRatePlanId__c = offertaDiRinnovo1.zqu__ZuoraId__c;
		update ratePlan;
		ctrl.createQuoteAction();	


	}
}