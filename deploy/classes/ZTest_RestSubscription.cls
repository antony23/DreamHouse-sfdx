/**
 * Test class for RestSubscription apex class
 * 
 */

@isTest 
private class ZTest_RestSubscription {

	public static Zuora__SubscriptionProductCharge__c subscriptionTestNull 		{get; set;}
	public static Zuora__SubscriptionProductCharge__c subscriptionTestNotActive 	{get; set;}
	public static List<Avviso_di_Rinnovo__c> avvisoNull 							{get; set;}
	public static List<Avviso_di_Rinnovo__c> avvisoNotActive 						{get; set;}
    
 	@testSetup 
 	static void setup() {

 		Product2 product= ZTest_Utils.createProduct('QUATTRORUOTE');
 		insert product;

		List<zqu__ProductRatePlan__c> ratePlanList;

		zqu__ProductRatePlan__c rp1 = ZTest_Utils.createzquProductRatePlan(product.id,'2c92c0f85e09726a015e0a79d8db550f','Abbonamento');
		zqu__ProductRatePlan__c rp2 = ZTest_Utils.createzquProductRatePlan(product.id,'2c92c0f85e09726a015e0a79d8e95511','Abbonamento');
		zqu__ProductRatePlan__c rp3 = ZTest_Utils.createzquProductRatePlan(product.id,'2c92c0f85e09726a015e0a79x8e95521','Bundle Misti');
		zqu__ProductRatePlan__c rp4 = ZTest_Utils.createzquProductRatePlan(product.id,'2c92c0f85e09724a015e0a79x8e9d521','Spese di spedizione');

		List<zqu__ProductRatePlan__c> productRatePlanList = new List<zqu__ProductRatePlan__c>();
        productRatePlanList.add(rp1);
        productRatePlanList.add(rp2);
        productRatePlanList.add(rp3);
        productRatePlanList.add(rp4);

        insert productRatePlanList;
		
        Testata__c testata=ZTest_Utils.createTestata('QUATTRORUOTE');
        insert testata;
        
		List<Calendario__c> calendars=ZTest_Utils.createCalendar(testata.id);
      	insert calendars;
        
		Avviso_di_rinnovo__c avvisoDiRinnovoNoSub= new Avviso_di_Rinnovo__c();
		insert avvisoDiRinnovoNoSub;
                
        Account zuoraAcc= ZTest_Utils.createAccount();
        insert zuoraAcc;

        Contact billTo= ZTest_Utils.createContact(zuoraAcc.id, 'IT');
        insert billTo;        
      
        Contact soldTo= ZTest_Utils.createContact(zuoraAcc.id, null);
        insert soldTo;
      
        Zuora__CustomerAccount__c billingAccount= ZTest_Utils.createBillingAccount(billTo.id, soldTo.id, zuoraAcc.id);
        billingAccount.Zuora__Zuora_Id__c = 'ABC';
        insert billingAccount;

        Zuora__Subscription__c subscriptionRen=ZTest_Utils.createSubscriptionRen('Test',billingAccount, 'Pending Activation','SI');
        Zuora__Subscription__c subscriptionAct=ZTest_Utils.createSubscriptionRen('Test',billingAccount, 'Active', 'Si');
		Zuora__Subscription__c subscriptionNotActive=ZTest_Utils.createSubscriptionRen('Test',billingAccount, 'Active', 'NO');

		List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();
		subscriptionList.add(subscriptionAct);
		subscriptionList.add(subscriptionNotActive);
		subscriptionList.add(subscriptionRen);
		insert subscriptionList;

		Zuora__SubscriptionProductCharge__c subscriptionProductCharge = ZTest_Utils.createzuoraSubscriptionProductCharge(subscriptionRen.id);
		insert subscriptionProductCharge;

		Zuora__SubscriptionProductCharge__c subscriptionProductChargeActive = ZTest_Utils.createzuoraSubscriptionProductCharge(subscriptionAct.id);
		Zuora__SubscriptionProductCharge__c subscriptionProductChargeNotActive = ZTest_Utils.createzuoraSubscriptionProductCharge(subscriptionNotActive.id);
		List<Zuora__SubscriptionProductCharge__c> subProductChargeList = new List<Zuora__SubscriptionProductCharge__c>();
		subProductChargeList.add(subscriptionProductChargeActive);
		subProductChargeList.add(subscriptionProductChargeNotActive);

		avvisoNull = [SELECT Id, Abbonamento__c FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c = 'Active' AND Abbonamento__r.Mantieni_Offerta__c = 'Si' LIMIT 1];
        if(!avvisoNull.isEmpty()){
        	subscriptionTestNull = ZTest_Utils.createzuoraSubscriptionProductCharge(avvisoNull.get(0).Abbonamento__c);
        	subProductChargeList.add(subscriptionTestNull);
    	}

    	avvisoNotActive = [SELECT Id, Abbonamento__c FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c = 'Active' AND Abbonamento__r.Mantieni_Offerta__c = 'NO' LIMIT 1];
    	if(!avvisoNotActive.isEmpty()){
        	subscriptionTestNotActive = ZTest_Utils.createzuoraSubscriptionProductCharge(avvisoNotActive.get(0).Abbonamento__c);
    		subProductChargeList.add(subscriptionTestNotActive);
    	}

		insert subProductChargeList;
	
		Avviso_di_rinnovo__c avvisoDiRinnovoActItalia= ZTest_Utils.createAvvisoDiRinnovo(subscriptionAct.id, rp1.id, 'Italia');
		Avviso_di_rinnovo__c avvisoDiRinnovoActEstero= ZTest_Utils.createAvvisoDiRinnovo(subscriptionAct.id, rp1.id, 'Estero');
		Avviso_di_rinnovo__c avvisoDiRinnovoNotActiveItalia= ZTest_Utils.createAvvisoDiRinnovo(subscriptionNotActive.id, rp4.id, 'Italia');
		Avviso_di_rinnovo__c avvisoDiRinnovoNotActiveEstero= ZTest_Utils.createAvvisoDiRinnovo(subscriptionNotActive.id, rp4.id, 'Estero');
		Avviso_di_rinnovo__c avvisoDiRinnovoSubItalia = ZTest_Utils.createAvvisoDiRinnovo(subscriptionRen.id, rp1.id, 'Italia');
		Avviso_di_rinnovo__c avvisoDiRinnovoSubEstero = ZTest_Utils.createAvvisoDiRinnovo(subscriptionRen.id, rp1.id, 'Estero');

		List<Avviso_di_rinnovo__c> avvisoDiRinnovoList = new List<Avviso_di_rinnovo__c>();
		avvisoDiRinnovoList.add(avvisoDiRinnovoActItalia);
		avvisoDiRinnovoList.add(avvisoDiRinnovoActEstero);
		avvisoDiRinnovoList.add(avvisoDiRinnovoNotActiveItalia);
		avvisoDiRinnovoList.add(avvisoDiRinnovoNotActiveEstero);
		avvisoDiRinnovoList.add(avvisoDiRinnovoSubItalia);
		avvisoDiRinnovoList.add(avvisoDiRinnovoSubEstero);

		insert avvisoDiRinnovoList;

		Zuora__SubscriptionRatePlan__c subscriptionRatePlan = ZTest_Utils.createZuoraSubscriptionRatePlan(rp3, subscriptionAct.id);
		Zuora__SubscriptionRatePlan__c subscriptionRatePlan2 = ZTest_Utils.createZuoraSubscriptionRatePlan(rp3, subscriptionNotActive.id);
		List<Zuora__SubscriptionRatePlan__c> subscriptionRatePlanList = new List<Zuora__SubscriptionRatePlan__c>();
		subscriptionRatePlanList.add(subscriptionRatePlan);
		subscriptionRatePlanList.add(subscriptionRatePlan2);

		SubscriptionRatePlanTriggerHandler.skipTrigger = True;
		insert subscriptionRatePlanList;

		List<Zuora__SubscriptionRatePlan__c> subRatePlanList = [SELECT Original_Product_RatePlan__c FROM Zuora__SubscriptionRatePlan__c WHERE Zuora__Subscription__c = :subscriptionAct.Id AND (Original_Product_RatePlan__r.TipoProdotto__c = 'Abbonamento' OR Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle abbonamenti' OR Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle misti') LIMIT 1];
		Id ratePlanId = subRatePlanList.get(0).Original_Product_RatePlan__c;

		RestSubscription.Subscription subs = new RestSubscription.Subscription();

		zqu__ProductRatePlanCharge__c rpc1 = ZTest_Utils.createzquProductRatePlanCharge(rp1.id, 'Abbonamento');
        zqu__ProductRatePlanCharge__c rpc2 = ZTest_Utils.createzquProductRatePlanCharge(rp4.id, 'Abbonamento');
        zqu__ProductRatePlanCharge__c rpc3 = ZTest_Utils.createzquProductRatePlanCharge(rp4.id, 'Spese di spedizione');
        zqu__ProductRatePlanCharge__c rpc4 = ZTest_Utils.createzquProductRatePlanCharge(ratePlanId, 'Abbonamento');
        zqu__ProductRatePlanCharge__c rpc5 = ZTest_Utils.createzquProductRatePlanCharge(ratePlanId, 'Spese di spedizione');
        List<zqu__ProductRatePlanCharge__c> productRatePlanChargeList = new List<zqu__ProductRatePlanCharge__c>();
        productRatePlanChargeList.add(rpc1);
        productRatePlanChargeList.add(rpc2);
        productRatePlanChargeList.add(rpc3);
        productRatePlanChargeList.add(rpc4);
        productRatePlanChargeList.add(rpc5);
        insert productRatePlanChargeList;
		
        List<Mapping_Documento_Emesso__c> mapping = new List<Mapping_Documento_Emesso__c>();
        mapping.add(ZTest_Utils.createMappingDocumentoEmesso('B2C', 'Carta', 'Italia', 'Italia'));
        mapping.add(ZTest_Utils.createMappingDocumentoEmesso('B2C', 'Carta', 'CEE', 'Italia'));
        mapping.add(ZTest_Utils.createMappingDocumentoEmesso('B2C', 'Carta', 'Italia', 'CEE'));
        mapping.add(ZTest_Utils.createMappingDocumentoEmesso('B2C', 'Carta', 'CEE', 'CEE'));
        insert mapping;

        Setting__c sett1 = new Setting__c(Name = 'TipiSpeseDiSpedizione',URL__c = 'http://domuscap.edidomus.it/api/v1/tipospedizioni');
        Setting__c sett2 = new Setting__c(Name = 'CalcoloSpeseSpedizione',URL__c = 'http://domuscap.edidomus.it/api/v1/spedizioni');
        List<Setting__c> settList = new List<Setting__c>{sett1,sett2};
        insert settList;

    }

	@isTest
	static void testErrNullReq(){
		Test.startTest();
		RestSubscription.RestObject res = RestSubscription.getSubscriptionDetails(null);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'L\'oggetto ricevuto è vuoto');
		Test.stopTest();
	}

	@isTest
	static void testErrEmptyReq(){
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res = RestSubscription.getSubscriptionDetails(null);
		res = RestSubscription.getSubscriptionDetails(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun abbonamento o avviso di rinnovo ricevuto');
		Test.stopTest();
	}

	@isTest
	static void testErrNoRtePlan(){
		Test.startTest();
		RestSubscription.RestObject res = RestSubscription.getSubscriptionDetails(null);
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		request.subscriptionData = new RestSubscription.SubscriptionData();
		res = RestSubscription.getSubscriptionDetails(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun rateplan ricevuto');
		Test.stopTest();
	}

	@isTest
	static void testErrNoRatePlan(){
		Test.startTest();
		RestSubscription.RatePlanData rp = new RestSubscription.RatePlanData();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;
		request.subscriptionData = new RestSubscription.SubscriptionData();
		rp.ratePlan.productRatePlanId = 'fake id';
		request.subscriptionData.ratePlanDataList.add(rp);
		res = RestSubscription.getSubscriptionDetails(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'Nessun ratePlan trovato');
		Test.stopTest();
	}

	@isTest
	static void testErrOnlyOneSubPlan(){
		Test.startTest();
		RestSubscription.RestObject res;
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		request.subscriptionData = new RestSubscription.SubscriptionData();
		request.subscriptionData.ratePlanDataList = new List<RestSubscription.RatePlanData>();

		for(zqu__ProductRatePlan__c rp1 : [SELECT Id,zqu__ZuoraId__c 
										   FROM zqu__ProductRatePlan__c 
										   WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
										   LIMIT 2]){
			RestSubscription.RatePlanData rpd = new RestSubscription.RatePlanData();
			rpd.ratePlan.productRatePlanId = rp1.zqu__ZuoraId__c;
			request.subscriptionData.ratePlanDataList.add(rpd);
		}

		res = RestSubscription.getSubscriptionDetails(request);
		System.assert(res.isError);
		System.assertEquals(res.errorMsg, 'E\' possibile inserire solo un rateplan con charge di tipo abbonamento all\'interno dell\'abbonamento');
		Test.stopTest();
	}

	@isTest
	static void testFlow(){
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;

		List<Contact> billtoList=[SELECT id FROM Contact where Codice_Nazione__c=null LIMIT 1];
        List<Contact> soldtoList=[SELECT id FROM Contact where Codice_Nazione__c!=null LIMIT 1];
        
        System.debug('*.*'+ soldtoList.get(0));
        System.debug('*.*'+ billtoList.get(0));

        request.accountData=new RestSubscription.AccountData();
        request.accountData.billToId=billtoList.get(0).id;
        request.accountData.soldToId=soldtoList.get(0).id;
        request.subscriptionData = new RestSubscription.SubscriptionData();
        request.subscriptionData.ratePlanDataList = new List<RestSubscription.RatePlanData>();
		List<zqu__ProductRatePlan__c> testDebug=[SELECT Id, zqu__ZuoraId__c, Testata__c, TipoProdotto__c
												 FROM zqu__ProductRatePlan__c];

		System.debug('Test** '+ testDebug);

		zqu__ProductRatePlan__c rp2 = [SELECT Id,zqu__ZuoraId__c,ConAvvisi__c,SkipSpesediSpedizione__c 
									   FROM zqu__ProductRatePlan__c 
									   WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
									   AND Testata__c = 'QUATTRORUOTE'
									   LIMIT 1];
		RestSubscription.RatePlanData rpd = new RestSubscription.RatePlanData();
		rpd.ratePlan.productRatePlanId = rp2.zqu__ZuoraId__c;
		request.subscriptionData.ratePlanDataList.add(rpd);
        

		res = RestSubscription.getSubscriptionDetails(request);

		
		Test.stopTest();
	}

	@isTest
	static void testFlow1(){
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;

		List<Contact> billtoList=[SELECT id FROM Contact where Codice_Nazione__c!=null LIMIT 1];
        List<Contact> soldtoList=[SELECT id FROM Contact where Codice_Nazione__c=null LIMIT 1];
        
        System.debug('*.*'+ soldtoList.get(0));
        System.debug('*.*'+ billtoList.get(0));

        request.accountData=new RestSubscription.AccountData();
        request.accountData.billToId=billtoList.get(0).id;
        request.accountData.soldToId=soldtoList.get(0).id;
        request.subscriptionData = new RestSubscription.SubscriptionData();
        request.subscriptionData.ratePlanDataList = new List<RestSubscription.RatePlanData>();
		List<zqu__ProductRatePlan__c> testDebug=[SELECT Id, zqu__ZuoraId__c, Testata__c, TipoProdotto__c
												 FROM zqu__ProductRatePlan__c];

		System.debug('Test** '+ testDebug);

		zqu__ProductRatePlan__c rp2 = [SELECT Id,zqu__ZuoraId__c,ConAvvisi__c,SkipSpesediSpedizione__c 
									   FROM zqu__ProductRatePlan__c 
									   WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
									   AND Testata__c = 'QUATTRORUOTE'
									   LIMIT 1];
		RestSubscription.RatePlanData rpd = new RestSubscription.RatePlanData();
		rpd.ratePlan.productRatePlanId = rp2.zqu__ZuoraId__c;
		request.subscriptionData.ratePlanDataList.add(rpd);
        

		res = RestSubscription.getSubscriptionDetails(request);

		
		Test.stopTest();
	}

	@isTest
	static void testFlowBillingAccount(){
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;

		List<Zuora__CustomerAccount__c> billingAccountList=[SELECT id,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c != null LIMIT 1];

		/*List<Contact> billtoList=[SELECT id FROM Contact where Codice_Nazione__c!=null LIMIT 1];
        List<Contact> soldtoList=[SELECT id FROM Contact where Codice_Nazione__c=null LIMIT 1];
        */

        /*System.debug('*.*'+ soldtoList.get(0));
        System.debug('*.*'+ billtoList.get(0));
*/
      	request.accountData=new RestSubscription.AccountData();
        request.subscriptionData = new RestSubscription.SubscriptionData();
        request.subscriptionData.ratePlanDataList = new List<RestSubscription.RatePlanData>();

      	System.debug('@@Test : '+billingAccountList.get(0).Zuora__Zuora_Id__c);
      	request.intermediarioData = new RestSubscription.IntermediarioData();
        request.intermediarioData.billingAccountId = billingAccountList.get(0).Zuora__Zuora_Id__c;
        request.intermediarioData.fatturaAIntermediario = true;
        request.accountData.billingAccountId=billingAccountList.get(0).Zuora__Zuora_Id__c;
		List<zqu__ProductRatePlan__c> testDebug=[SELECT Id, zqu__ZuoraId__c, Testata__c, TipoProdotto__c
												 FROM zqu__ProductRatePlan__c];

		System.debug('Test** '+ testDebug);

		zqu__ProductRatePlan__c rp2 = [SELECT Id,zqu__ZuoraId__c,ConAvvisi__c,SkipSpesediSpedizione__c 
									   FROM zqu__ProductRatePlan__c 
									   WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
									   AND Testata__c = 'QUATTRORUOTE'
									   LIMIT 1];
		RestSubscription.RatePlanData rpd = new RestSubscription.RatePlanData();
		rpd.ratePlan.productRatePlanId = rp2.zqu__ZuoraId__c;
		request.subscriptionData.ratePlanDataList.add(rpd);
        
        
		res = RestSubscription.getSubscriptionDetails(request);
	
		Test.stopTest();
	}

	@isTest
	static void testFlowBillingAccount2(){
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;

		List<Zuora__CustomerAccount__c> billingAccountList=[SELECT id,Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c != null LIMIT 1];

		/*List<Contact> billtoList=[SELECT id FROM Contact where Codice_Nazione__c!=null LIMIT 1];
        List<Contact> soldtoList=[SELECT id FROM Contact where Codice_Nazione__c=null LIMIT 1];
        */

        /*System.debug('*.*'+ soldtoList.get(0));
        System.debug('*.*'+ billtoList.get(0));
*/
      	request.accountData=new RestSubscription.AccountData();
        request.subscriptionData = new RestSubscription.SubscriptionData();
        request.subscriptionData.ratePlanDataList = new List<RestSubscription.RatePlanData>();

        request.subscriptionData.subscription.mantieniOfferta = 'SI';
      	System.debug('@@Test : '+billingAccountList.get(0).Zuora__Zuora_Id__c);
        request.accountData.billingAccountId=billingAccountList.get(0).Zuora__Zuora_Id__c;
		List<zqu__ProductRatePlan__c> testDebug=[SELECT Id, zqu__ZuoraId__c, Testata__c, TipoProdotto__c
												 FROM zqu__ProductRatePlan__c];

		System.debug('Test** '+ testDebug);

		zqu__ProductRatePlan__c rp2 = [SELECT Id,zqu__ZuoraId__c,ConAvvisi__c,SkipSpesediSpedizione__c
									   FROM zqu__ProductRatePlan__c 
									   WHERE TipoProdotto__c IN ('Abbonamento','Bundle Misti','Bundle Abbonamenti')
									   AND Testata__c = 'QUATTRORUOTE'
									   LIMIT 1];
		RestSubscription.RatePlanData rpd = new RestSubscription.RatePlanData();
		rpd.ratePlan.productRatePlanId = rp2.zqu__ZuoraId__c;
		request.subscriptionData.ratePlanDataList.add(rpd);
        
		res = RestSubscription.getSubscriptionDetails(request);

		Test.stopTest();
	}

	@isTest
	static void testFlow2() {
		Test.startTest();

		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;

		List<Contact> billtoList=[SELECT id FROM Contact where Codice_Nazione__c!=null LIMIT 1];
        List<Contact> soldtoList=[SELECT id FROM Contact where Codice_Nazione__c=null LIMIT 1];
        
        System.debug('*.*'+ soldtoList.get(0));
        System.debug('*.*'+ billtoList.get(0));

        request.accountData=new RestSubscription.AccountData();
        request.accountData.billToId=billtoList.get(0).id;
        request.accountData.soldToId=soldtoList.get(0).id;
		request.avvisoDiRinnovo = new RestSubscription.AvvisoDiRinnovo();
        List<Avviso_di_Rinnovo__c> avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__c = null LIMIT 1];
        
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RestSubscription.getSubscriptionDetails(request);
        }

        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Stato_Sfdc__c = 'Rinnovata'];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RestSubscription.getSubscriptionDetails(request);
        }
 
        /*avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c != 'Active' AND Abbonamento__r.Zuora__Status__c != 'Expired'];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	res = RestSubscription.getSubscriptionDetails(request);
        }*/

        avviso = [SELECT Id FROM Avviso_di_Rinnovo__c WHERE Abbonamento__r.Zuora__Status__c = 'Active' AND Abbonamento__r.Mantieni_Offerta__c = 'SI'];
        if(!avviso.isEmpty()){
        	request.avvisoDiRinnovo.id = avviso.get(0).Id;
        	//res = RestSubscription.getSubscriptionDetails(request);
        }

		Test.stopTest();
	}

	@isTest
	static void testSubscriptionNull() {
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;
		request.subscriptionData = null;
		request.avvisoDiRinnovo = new RestSubscription.AvvisoDiRinnovo();

        if(avvisoNull != null && !avvisoNull.isEmpty()){
        	request.avvisoDiRinnovo.id = avvisoNull.get(0).Id;
        	res = RestSubscription.getSubscriptionDetails(request);
        }
        Test.stopTest();
        
    }

    @isTest
	static void testSubscriptionNullNotActive() {
		Test.startTest();
		RestSubscription.RestObject request = new RestSubscription.RestObject();
		RestSubscription.RestObject res;
		request.subscriptionData = null;
		request.avvisoDiRinnovo = new RestSubscription.AvvisoDiRinnovo();

        if(avvisoNotActive != null && !avvisoNotActive.isEmpty()){
        	request.avvisoDiRinnovo.id = avvisoNotActive.get(0).Id;
        	res = RestSubscription.getSubscriptionDetails(request);
        }
        Test.stopTest();
        
    }

}