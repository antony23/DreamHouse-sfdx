public class RegolaAvvisoRinnovoTriggerHandler{

    public static Boolean skipTrigger = false;
    
    public static void onBeforeInsert(List<Regola_Avviso_Rinnovo__c> triggerNew, Map<Id, Regola_Avviso_Rinnovo__c> triggerNewMap) { 
    }
    
    public static void onAfterInsert(List<Regola_Avviso_Rinnovo__c> triggerNew, Map<Id, Regola_Avviso_Rinnovo__c> triggerNewMap) {
        controllaCoerenzaProdottiRegola(triggerNew);
    }

    public static void onBeforeUpdate(List<Regola_Avviso_Rinnovo__c> triggerOld, List<Regola_Avviso_Rinnovo__c> triggerNew, Map<Id, Regola_Avviso_Rinnovo__c> triggerOldMap, Map<Id, Regola_Avviso_Rinnovo__c> triggerNewMap){   
    }

    public static void onAfterUpdate(List<Regola_Avviso_Rinnovo__c> triggerOld, List<Regola_Avviso_Rinnovo__c> triggerNew, Map<Id, Regola_Avviso_Rinnovo__c> triggerOldMap, Map<Id, Regola_Avviso_Rinnovo__c> triggerNewMap){   
        controllaCoerenzaProdottiRegola(triggerNew);
    }

    public static void onBeforeDelete(List<Regola_Avviso_Rinnovo__c> triggerOld, Map<Id, Regola_Avviso_Rinnovo__c> triggerOldMap){  
    	preventUsedRulesToBeDeleted(triggerOld);
    }

    public static void onAfterDelete(List<Regola_Avviso_Rinnovo__c> triggerOld, Map<Id, Regola_Avviso_Rinnovo__c> triggerOldMap){  
    }

    private static void preventUsedRulesToBeDeleted(List<Regola_Avviso_Rinnovo__c> triggerOld){
    	Map<Id,Regola_Avviso_Rinnovo__c> regoleMap = new Map<Id,Regola_Avviso_Rinnovo__c>(triggerOld);
    	for(Regola_Avviso_Rinnovo__c regola : [SELECT Id, 
    											(SELECT Id FROM Avvisi_di_Rinnovo__r LIMIT 1) 
    											FROM Regola_Avviso_Rinnovo__c 
    											WHERE Id IN :regoleMap.keySet()]){
			if(regola.Avvisi_di_Rinnovo__r.size() > 0){
				regoleMap.get(regola.Id).addError('Non è possibile cancellare una regola utilizzata.');
			}    		
    	}
    }

    private static void controllaCoerenzaProdottiRegola(List<Regola_Avviso_Rinnovo__c> regole){
        List<Id> ratePlanIds = new List<Id>();
        for(Regola_Avviso_Rinnovo__c regola : regole){
            if(regola.RecordTypeId == Schema.SObjectType.Regola_Avviso_Rinnovo__c.getRecordTypeInfosByName().get('Avviso di Rinnovo').getRecordTypeId()){
                if(regola.Offerta_di_Partenza__c != null){
                    ratePlanIds.add(regola.Offerta_di_Partenza__c);
                }
                if(regola.Offerta_di_Rinnovo__c != null){
                    ratePlanIds.add(regola.Offerta_di_Rinnovo__c);   
                }
            }
        }
        Map<Id,zqu__ProductRatePlan__c> ratePlans = new Map<Id,zqu__ProductRatePlan__c>(
                                                    [SELECT Id, Testata__c,
                                                        (SELECT Id,Testata__c 
                                                        FROM R00N40000001mFVKEA2__r 
                                                        WHERE TipoProdotto__c IN ('Abbonamento','Vendita Diretta'))
                                                    FROM zqu__ProductRatePlan__c
                                                    WHERE Id IN :ratePlanIds
                                                    ]);

        Map<Regola_Avviso_Rinnovo__c,Map<zqu__ProductRatePlan__c,zqu__ProductRatePlan__c>> ratePlansByRegola = new Map<Regola_Avviso_Rinnovo__c,Map<zqu__ProductRatePlan__c,zqu__ProductRatePlan__c>>();
        for(Regola_Avviso_Rinnovo__c regola : regole){
            try {
                if(regola.RecordTypeId == Schema.SObjectType.Regola_Avviso_Rinnovo__c.getRecordTypeInfosByName().get('Avviso di Rinnovo').getRecordTypeId()){
                    zqu__ProductRatePlan__c offertaDiPartenza = ratePlans.get(regola.Offerta_di_Partenza__c);
                    zqu__ProductRatePlan__c offertaDiRinnovo = ratePlans.get(regola.Offerta_di_Rinnovo__c);
                    if(offertaDiPartenza == null || offertaDiRinnovo == null) continue;
                    Set<String> testateOffertaDiPartenza = new Set<String>();
                    Set<String> testateOffertaDiRinnovo = new Set<String>();
                    if(String.isNotBlank(offertaDiPartenza.Testata__c)){
                        testateOffertaDiPartenza.add(offertaDiPartenza.Testata__c.toUpperCase());
                    }
                    for(zqu__ProductRatePlanCharge__c charge : offertaDiPartenza.R00N40000001mFVKEA2__r){
                        if(String.isNotBlank(charge.Testata__c)){
                            testateOffertaDiPartenza.add(charge.Testata__c.toUpperCase());
                        }
                    }
                    if(String.isNotBlank(offertaDiRinnovo.Testata__c)){
                        testateOffertaDiRinnovo.add(offertaDiRinnovo.Testata__c.toUpperCase());
                    }
                    for(zqu__ProductRatePlanCharge__c charge : offertaDiRinnovo.R00N40000001mFVKEA2__r){
                        if(String.isNotBlank(charge.Testata__c)){
                            testateOffertaDiRinnovo.add(charge.Testata__c.toUpperCase());
                        }
                    }
                    Boolean intersezioneTestate = false;
                    for(String testataOffertaDiPartenza : testateOffertaDiPartenza){
                        for(String testataOffertaDiRinnovo : testateOffertaDiRinnovo){
                            if(testataOffertaDiPartenza == testataOffertaDiRinnovo){
                                intersezioneTestate = true;
                            }
                        }
                    }
                    if(!intersezioneTestate){
                        regola.addError('L\'offerta di rinnovo non è coerente con quella di partenza. Regola: ' + regola);
                    }
                }
            } catch(Exception e) {
                regola.addError(e.getMessage() + ' ' + e.getStackTraceString());
            }
        }


    }

}