global class SendNotification implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
	
	String query;
	String errorMessage;
    OrgWideEmailAddress address;
    EmailTemplate template;
    Set<Id> notificheIds;
    Map<Id, Notifica_Informativa__c> notificheDaAggiornare;
	
	global SendNotification() {
		this.query = 'SELECT Id FROM Notifica_Informativa__c WHERE Invio_Manuale__c = false AND Stato__c = \'Draft\'';
		this.address = InformativaUtils.getOrgWideEmailAddressByDisplayName('Editoriale Domus - No reply');
		this.template = InformativaUtils.getTemplateByName('MT_Informativa');
		this.notificheDaAggiornare = new Map<Id, Notifica_Informativa__c>();
		this.notificheIds = new Set<Id>();
		this.errorMessage = '';
	}

	global SendNotification(Set<Id> notificationIds) {
		this.notificheIds = notificationIds;
		this.query = 'SELECT Id FROM Notifica_Informativa__c WHERE Invio_Manuale__c = false AND Stato__c = \'Draft\' AND Id IN :notificheIds';
		this.address = InformativaUtils.getOrgWideEmailAddressByDisplayName('Editoriale Domus - No reply');
		this.template = InformativaUtils.getTemplateByName('MT_Informativa');
		this.notificheDaAggiornare = new Map<Id, Notifica_Informativa__c>();
		this.errorMessage = '';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug(LoggingLevel.INFO, '*_* SendNotification.start()');
		return Database.getQueryLocator(query);
	}

	global void execute(SchedulableContext SC) {
		System.debug(LoggingLevel.INFO, '*_* SendNotification.execute()');
		//if ([SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){ 
			Database.executeBatch(new SendNotification(), 100);
		/*
		}else{
		   SendNotification sn = new SendNotification();
		   Datetime dt = Datetime.now().addMinutes(30);
		   String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
		   System.Schedule('SendNotification_' + timeForScheduler, timeForScheduler, sn);
		}
		*/
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug(LoggingLevel.INFO, '*_* SendNotification.execute()');
		try{

            List<Notifica_Informativa__c> listaNotifiche = [SELECT Id, Contatto__c, Contatto__r.Email, Contatto__r.Record_Type_Account__c, (SELECT Id FROM Emails) FROM Notifica_Informativa__c WHERE Id IN : scope];

        	List<Notifica_Informativa__c> notificheDaInviare = new List<Notifica_Informativa__c>();
        	for(Notifica_Informativa__c richiestaNotifica : listaNotifiche){
        		if(richiestaNotifica.Emails.isEmpty() && richiestaNotifica.Contatto__r.Record_Type_Account__c == 'B2C' && richiestaNotifica.Contatto__r.Email != null){
        			notificheDaInviare.add(richiestaNotifica);
        		}
        	}

        	if(!notificheDaInviare.isEmpty()){

        		//Messaging.reserveSingleEmailCapacity(notificheDaInviare.size());
        		notificheDaAggiornare.putAll(notificheDaInviare);

        		List<Messaging.SendEmailResult> results = sendEmail(notificheDaInviare);
        		System.debug(LoggingLevel.INFO, '*_* You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');

				for(Messaging.SendEmailResult r : results){
				    if(!r.isSuccess()){
				    	for(Messaging.SendEmailError e : r.getErrors()){
				    		System.debug(LoggingLevel.ERROR, 'An error has occurred during sendEmail: ' + e.getMessage() + ' on target ' + e.getTargetObjectId());
				    		errorMessage += '\r\nAn error has occurred during sendEmail: ' + e.getMessage() + ' on target ' + e.getTargetObjectId();
				    	}
				    }
				}
        	}
        }catch(EmailException e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred in the Batch class: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        	errorMessage += '\r\nAn error has occurred in the Batch class: ' + e.getMessage() + ' on line ' + e.getLineNumber();
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
			errorMessage += '\r\nAn error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber();
		}catch(Exception e){
        	System.debug(LoggingLevel.ERROR, 'An error has occurred in the Batch class: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        	errorMessage += '\r\nAn error has occurred in the Batch class: ' + e.getMessage() + ' on line ' + e.getLineNumber();
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug(LoggingLevel.INFO, '*_* SendNotification.finish()');
		//Savepoint sp = Database.setSavepoint();
		try{
			updateRelatedObject();
			insertLog();
		}catch(QueryException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            //Database.rollback(sp);
        }catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            //Database.rollback(sp);
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber());
            //Database.rollback(sp);
        }
	}

	private List<Messaging.SendEmailResult> sendEmail(List<Notifica_Informativa__c> listaNotifiche){
		System.debug(LoggingLevel.INFO, '*_* sendEmail()');
		System.debug(LoggingLevel.INFO, '*_* You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
		
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for(Notifica_Informativa__c notifica : listaNotifiche){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setOrgWideEmailAddressId(address.Id);
            message.setTemplateId(template.Id);
            message.setTargetObjectId(notifica.Contatto__c);
            message.setWhatId(notifica.Id);
            emails.add(message);
        }
        return Messaging.sendEmail(emails, false);
	}

	private void updateRelatedObject(){
		System.debug(LoggingLevel.INFO, '*_* updateRelatedObject()');
		
		if(!notificheDaAggiornare.isEmpty()){
			
			List<EmailMessage> listaMessaggi = [SELECT Id, RelatedToId FROM EmailMessage WHERE RelatedToId IN :notificheDaAggiornare.keySet()];
			Map<Id, Notifica_Informativa__c> mappaNotifiche = new Map<Id, Notifica_Informativa__c>();

			for(EmailMessage e : listaMessaggi){
				if(notificheDaAggiornare.containsKey(e.RelatedToId)){
					mappaNotifiche.put(e.RelatedToId, notificheDaAggiornare.get(e.RelatedToId));
				}
			}

			//Set<Id> contactIds = new Set<Id>();
			for(Notifica_Informativa__c notifica : mappaNotifiche.values()){
				notifica.Stato__c = AppConstants.EMAIL_SENT;
				notifica.Data_Invio__c = System.today();
				//contactIds.add(notifica.Contatto__c);
			}

			/*
			Map<Id, Contact> mappaContatti = new Map<Id, Contact>([SELECT Id, Data_Invio_Ultima_Informativa__c FROM Contact WHERE Id IN : contactIds]);
			for(Contact contatto : mappaContatti.values()){
				contatto.Data_Invio_Ultima_Informativa__c = System.today();
			}
			*/

			InformativaUtils.executeUpdate(mappaNotifiche);
			//InformativaUtils.executeUpdate(mappaContatti);
		}

	}

	private void insertLog(){
		System.debug(LoggingLevel.INFO, '*_* insertLog()');
		
		Log__c logger;
		if(String.isNotEmpty(errorMessage)){
	        logger = new Log__c(
	            Class__c        = SendNotification.class.getName(),
	            Name 			= 'Log_Invio_Notifica_Informativa_'+ System.now(),
	            Method__c       = 'execute',
	            Stack_Trace__c  = 'Batch process completed with ERRORS: ' + errorMessage,
	            Severity__c     = 'ERROR'
	        );
        }else{
            logger = new Log__c(
                Class__c        = SendNotification.class.getName(),
                Name 			= 'Log_Invio_Notifica_Informativa_'+ System.now(),
                Method__c       = 'execute',
                Stack_Trace__c  = 'Batch process completed SUCCESSFULLY without errors. Total records processed: ' + notificheDaAggiornare.size(),
                Severity__c     = 'INFO'
            );
        }
        insert logger;
	}
}