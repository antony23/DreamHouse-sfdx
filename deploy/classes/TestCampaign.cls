@isTest
private class TestCampaign{

    @isTest static void test_method(){
        Test.startTest();
        
        Product2 prod = ZTest_Utils.createProduct('testata');
        prod.Name = 'SCONTO';
        insert prod;

        zqu__ProductRatePlan__c rp = new zqu__ProductRatePlan__c();
        rp.Name = 'Omaggio Vendite Dirette';
        rp.TipoProdotto__c = 'Sconti';
        rp.zqu__Product__c = prod.Id;
        insert rp;

        rp = [SELECT Id,TipoProdotto__c,zqu__Product__c,zqu__Product__r.Name FROM zqu__ProductRatePlan__c WHERE Id = :rp.Id LIMIT 1];

        Campaign c = new Campaign();
        c.Tipo_Promozione__c = 'Target';
        c.Omaggio__c = true;
        c.Prodottoassociatoallosconto__c = 'Prodotto vendita diretta';
        c.Rate_Plan_Promozionale__c = rp.Id;
        c.Name = 'TestCampaign';
        insert c;
        update c;
        delete c;
        Test.stopTest();
    }

}