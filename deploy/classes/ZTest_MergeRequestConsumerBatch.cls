@isTest(SeeAllData = True)
public class ZTest_MergeRequestConsumerBatch {
	private static final Integer MAX_RECORDS = 10;

	@isTest
	public static void testAccountMerge() {
		setup();

		final List<Account> master = [ SELECT Id FROM Account WHERE Name LIKE '%MASTER%' ];
		final List<Account> slave = [ SELECT Id FROM Account WHERE Name LIKE '%SLAVE%' ];
		final RecordType rt = [ SELECT Id FROM RecordType WHERE DeveloperName = 'Account' AND SObjectType = 'Merge_Request__c' ];

		Test.startTest();

		MergeRequestTriggerHandler.disable();
		MergeRequestTriggerHandler.enable();

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			insert new Merge_Request__c(Master_Account__c = master[i].Id, Slave_Account__c = slave[i].Id, RecordTypeId = rt.Id);
		}

		Test.stopTest();
	}

	//@isTest
	public static void testContactMerge() {
		setup();

		final List<Contact> master = [ SELECT Id FROM Contact WHERE FirstName = 'MASTER' ORDER BY AccountId ];
		final List<Contact> slave = [ SELECT Id FROM Contact WHERE FirstName = 'SLAVE' ORDER BY AccountId ];
		final RecordType rt = [ SELECT Id FROM RecordType WHERE DeveloperName = 'Contact' AND SObjectType = 'Merge_Request__c' ];

		Test.startTest();

		MergeRequestTriggerHandler.disable();
		MergeRequestTriggerHandler.enable();

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			insert new Merge_Request__c(Master_Contact__c = master[i].Id, Slave_Contact__c = slave[i].Id, RecordTypeId = rt.Id);
		}

		Test.stopTest();
	}

	@isTest
	public static void testRollbackCustomerContacts() {
		setup();

		final List<Contact> slave = [ SELECT Id, MailingStreet, MailingCity, MailingCountry, FirstName, LastName, MailingPostalCode, MailingState, Email, Phone, MobilePhone FROM Contact WHERE FirstName = 'SLAVE' ];
		final Map<Contact, Set<String>> mapZIDCC = new Map<Contact, Set<String>>();

		for(Contact c : slave) {
			mapZIDCC.put(c, new Set<String>{'aaa', 'bbb', 'ccc'});
		}

		MergeRequestConsumerBatch.rollbackCustomerContacts(mapZIDCC);
	}

	@isTest
	public static void testRollbackCustomerAccounts1() {
		setup();

		final Account master = [ SELECT Id FROM Account WHERE Name LIKE '%MASTER%' LIMIT 1];
		final Account slave = [ SELECT Id FROM Account WHERE Name LIKE '%SLAVE%' LIMIT 1];
		final List<Zuora__CustomerAccount__c> ca = [SELECT Id, Bill_To_Salesforce__c, Sold_To_Salesforce__c, Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :slave.Id LIMIT 1];
		final RecordType rt = [ SELECT Id FROM RecordType WHERE DeveloperName = 'Account' AND SObjectType = 'Merge_Request__c' ];
		final Merge_Request__c mr = new Merge_Request__c(Master_Account__c = master.Id, Slave_Account__c = slave.Id, RecordTypeId = rt.Id);
		insert mr;

		final Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> mapMRCA = new Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> {
			mr => ca
		};

		MergeRequestConsumerBatch.rollbackCustomerAccounts(mapMRCA);
	}

	//@isTest
	public static void testRollbackCustomerAccounts2() {
		setup();

		final Contact master = [ SELECT Id, AccountId FROM Contact WHERE FirstName = 'MASTER' ORDER BY AccountId LIMIT 1];
		final Contact slave = [ SELECT Id, AccountId FROM Contact WHERE FirstName = 'SLAVE' AND AccountId = :master.AccountId LIMIT 1];
		final List<Zuora__CustomerAccount__c> ca = [SELECT Id, Bill_To_Salesforce__c, Sold_To_Salesforce__c, Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Bill_To_Salesforce__c = :slave.Id OR Sold_To_Salesforce__c = :slave.Id LIMIT 1];
		final RecordType rt = [ SELECT Id FROM RecordType WHERE DeveloperName = 'Contact' AND SObjectType = 'Merge_Request__c' ];
		final Merge_Request__c mr = new Merge_Request__c(Master_Contact__c = master.Id, Slave_Contact__c = slave.Id, RecordTypeId = rt.Id);
		insert mr;

		final Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> mapMRCA = new Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> {
			mr => ca
		};

		MergeRequestConsumerBatch.rollbackCustomerAccounts(mapMRCA);
	}

	public static void setup() {
		final List<Account> accounts = new List<Account>();

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			accounts.add(new Account(Name = 'MASTER ACCOUNT ' + i));
		}

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			accounts.add(new Account(Name = 'SLAVE ACCOUNT ' + i));
		}

		AccountTriggerHandler.skip = True;
		insert accounts;
		AccountTriggerHandler.skip = False;

		final Account sameAccount = new Account(Name = 'TEST ACCOUNT');
		insert sameAccount;

		final List<Contact> contacts = new List<Contact>();

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			contacts.add(new Contact(FirstName = 'MASTER', LastName = 'CONTACT ' + i, AccountId = sameAccount.Id));
		}

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			contacts.add(new Contact(FirstName = 'SLAVE', LastName = 'CONTACT ' + i, AccountId = sameAccount.Id));
		}

		for(Integer i = 0; i < MAX_RECORDS; i++) {
			contacts.add(new Contact(FirstName = 'TEST', LastName = 'CONTACT ' + i, AccountId = accounts[i + MAX_RECORDS].Id));
		}

		ContactTriggerHandler.skip = True;
		insert contacts;
		ContactTriggerHandler.skip = False;

		final List<Zuora__CustomerAccount__c> customerAccounts = new List<Zuora__CustomerAccount__c>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			final Zuora__CustomerAccount__c ca = ZTest_Utils.createBillingAccount(contacts[i].Id, contacts[i].Id, accounts[i].Id);
			ca.Zuora__BillToId__c = 'xxxxxxxxxx' + i;
			ca.Zuora__SoldToId__c = 'yyyyyyyyyy' + i;
			customerAccounts.add(ca);
		}

		insert customerAccounts;

		final List<Zuora__Subscription__c> subscriptions = new List<Zuora__Subscription__c>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			final Zuora__Subscription__c sub = new Zuora__Subscription__c(Name = 'AS-' + i, Zuora__CustomerAccount__c = customerAccounts[i].Id, Zuora__Account__c = accounts[i].Id, Contact_Sold_To__c = contacts[i].Id, Contact_Bill_To__c = contacts[i].Id);
			subscriptions.add(sub);
		}

		SubscriptionTriggerHandler.skipTrigger = true;
		insert subscriptions;
		SubscriptionTriggerHandler.skipTrigger = false;

		final List<Asset> copie = new List<Asset>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			for(Integer j = 0; j < MAX_RECORDS; j++) {
				copie.add(new Asset(Name = 'COPIA ' + i + '-' + j, SubscriptionAllCopy__c = subscriptions[i].Id, AccountId = contacts[i].AccountId, ContactId = contacts[i].Id, Inviata__c = False));
			}
		}

		insert copie;

		final List<Movimentazione_Stock__c> movstocks = new List<Movimentazione_Stock__c>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			for(Integer j = 0; j < MAX_RECORDS; j++) {
				movstocks.add(new Movimentazione_Stock__c(Subscription__c = subscriptions[i].Id, Contact__c = contacts[i].Id, Stato__c = 'Da Trasmettere', Quantita__c = 1));
			}
		}

		MovimentazioneStockTriggerHandler.skip = true;
		insert movstocks;
		MovimentazioneStockTriggerHandler.skip = false;

		final List<Entitlement__c> entitlements = new List<Entitlement__c>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			final Entitlement__c ent = new Entitlement__c(Contact__c = contacts[i].Id, Subscription__c = subscriptions[i].Id, Stato__c = 'Da inviare');
			entitlements.add(ent);
		}

		insert entitlements;

		final List<Avviso_di_Rinnovo__c> avvisi = new List<Avviso_di_Rinnovo__c>();

		for(Integer i = 0; i < MAX_RECORDS * 2; i++) {
			final Avviso_di_Rinnovo__c avv = new Avviso_di_Rinnovo__c(Contact_Sold_To__c = contacts[i].Id, Contatto__c = contacts[i].Id, Committente__c = contacts[i].AccountId, Abbonamento__c = subscriptions[i].Id, Stato_Avviso__c = 'Nuovo');
			avvisi.add(avv);
		}

		AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = true;
		insert avvisi;
		AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = false;
	}

}