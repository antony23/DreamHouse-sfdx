//Batch per eliminare Account e Contatti in Migrazione
//Database.executebatch( new deleteAccountBatch(Select id from account where id ='-----'));
 
global class deleteAccountBatch implements Database.Batchable<sObject>{
    global final String Query;
    global deleteAccountBatch (String q){
        Query=q;
        
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<sObject> scope){
        delete scope;
        database.emptyrecyclebin(scope);
    }

    global void finish(Database.BatchableContext BC){}
}