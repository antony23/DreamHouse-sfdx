public class CheckCfExtension {

    public Contact c {get;set;}
    public Boolean isValid {get;set;}

    public List<sObject> duplicateRecords{get;set;}
    public boolean hasDuplicateResult{get;set;}
    private Boolean ignoreDuplicate;

    public CheckCfExtension(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()) controller.addFields(new List<String>{'Codice_Nazione__c', 'Sesso__c', 'Birthdate', 'Luogo_di_Nascita__c'});
        
        c = (Contact) controller.getRecord();
        this.duplicateRecords = new List<sObject>();
        this.hasDuplicateResult = false;
        ignoreDuplicate = false;
    }

    public void checkCf(){
        isValid = false;

        if(String.isNotBlank(c.Codice_Fiscale__c)){
            if(c.Codice_Nazione__c == 'IT'){
                c.Codice_Fiscale__c = c.Codice_Fiscale__c.toUpperCase().trim();

                try {
                    isValid = UtilCodiceFiscale.validateCF(
                                                                c.FirstName,
                                                                c.LastName,
                                                                c.Sesso__c,
                                                                c.Birthdate,
                                                                c.Luogo_di_Nascita__c,
                                                                c.Codice_Fiscale__c
                                                            );
                } catch(Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    return;
                } 

                if(isValid){
                    //UtilCodiceFiscale.Person p = UtilCodiceFiscale.getPersonInfo(c.Codice_Fiscale__c);
                    //    c.Sesso__c = p.sex;
                    //    a.Birthdate = p.d;                        
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Codice Fiscale valido'));
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Codice Fiscale non valido'));
                }
            } else {
                isValid = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Codice Fiscale valido'));
            }
        } else {
            isValid = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Codice Fiscale non trovato'));
        }
   }

   public PageReference saveIgnore(){
        ignoreDuplicate = true;
        return save();
    }

    public PageReference save() {

        Database.DMLOptions dml = new Database.DMLOptions();
        if(ignoreDuplicate){
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            ignoreDuplicate = false;
        }

        Database.SaveResult saveResult = Database.update(c, dml);

        if (!saveResult.isSuccess()) {
            for (Database.Error error : saveResult.getErrors()) {
                if (error instanceof Database.DuplicateError) {
                    Database.DuplicateError duplicateError = 
                            (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = 
                            duplicateError.getDuplicateResult();
                    
                    ApexPages.Message errorMessage = new ApexPages.Message(
                            ApexPages.Severity.Warning, 'Duplicate Error: ' + 
                            duplicateResult.getErrorMessage());
                    ApexPages.addMessage(errorMessage);
                    
                    this.duplicateRecords = new List<sObject>();

                    Datacloud.MatchResult[] matchResults = 
                            duplicateResult.getMatchResults();

                    Datacloud.MatchResult matchResult = matchResults[0];

                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        System.debug('MatchRecord: ' + matchRecord.getRecord());
                        this.duplicateRecords.add(matchRecord.getRecord());
                    }
                    this.hasDuplicateResult = !this.duplicateRecords.isEmpty();
                }
            }
            return null;
        }
        return (new ApexPages.StandardController(c)).view();
    }

    public void goBack(){
        isValid = false;
    }

    private static List<string> fieldsOf(String className) {
        final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        return fields;
    }
}