public with sharing class BillingAccountUtil {

    public class ZuoraAccount {
            
        public String name                          {get;set;}
        public String currencyType                  {get;set;}
        public String batchName                     {get;set;}
        public Boolean autoPay                      {get;set;} 
        public String billCycleDay                  {get;set;}
        public String paymentTerm                   {get;set;}
        public String paymentGateway                {get;set;}
        public String crmId                         {get;set;}
        public Id billToSalesforceText              {get;set;}
        public Id soldToSalesforceText              {get;set;}
        public String supplementoCivicoSoldTo       {get;set;}
        public String metodoDiPagamento             {get;set;}
        public String partitaIvaBillTo              {get;set;}
        public String codiceClienteBillTo           {get;set;}
        public String codiceFiscaleBillTo           {get;set;}
        public String codiceAmministazione          {get;set;}
        public String salvaDatiCC                   {get;set;}
        public String supplementoCapSoldTo          {get;set;}
        public String titoloBillTo                  {get;set;}
        public ZuoraContact billToContact           {get;set;}
        public ZuoraContact soldToContact           {get;set;}
        public String hpmCreditCardPaymentMethodId  {get;set;}
        public String communicationProfileId        {get;set;}
        public Boolean invoiceDeliveryPrefsEmail    {get;set;}
        public Boolean invoiceDeliveryPrefsPrint    {get;set;}

    }

    public class ZuoraContact {

        public String firstName                     {get; set;}
        public String lastName                      {get; set;}
        public String address1                      {get; set;}
        public String workEmail                     {get; set;}
        public String mobilePhone                   {get; set;}
        public String workPhone                     {get; set;}
        public String city                          {get; set;}
        public String state                         {get; set;}
        public String zipCode                       {get; set;}
        public String country                       {get; set;}
        public String county                        {get; set;}

    }

    public class ResponseMessage {

        public Boolean success                      {get; set;}
        public String accountId                     {get; set;}
        public String accountNumber                 {get; set;}
        public String paymentMethodId               {get; set;}
        public String processId                     {get; set;}
        public List<ResponseError> reasons          {get; set;}

    }

    public class ResponseError {

        public Long code                            {get; set;}
        public String message                       {get; set;}

    }

    public static ResponseMessage createBillingAccount(zqu__Quote__c quote, Id billToContactId, Id soldToContactId){

        ResponseMessage message = null;

        Contact billToContact = fetchContact(billToContactId);
        Contact soldToContact = fetchContact(soldToContactId);

        ZuoraAccount ba = createZuoraAccount(quote, billToContact, soldToContact);
        HttpResponse restResponse = sendToZuora(ba);

        String restResponseJson = restResponse.getBody();
        return message = (ResponseMessage) JSON.deserialize(restResponseJson, ResponseMessage.class);

    }

    private static ZuoraAccount createZuoraAccount(zqu__Quote__c q, Contact billToContact, Contact soldToContact){
 
        ZuoraAccount ba  = new ZuoraAccount();
        ba.autoPay                      = false;
        ba.currencyType                 = q.zqu__Currency__c;
        ba.billCycleDay                 = q.zqu__BillCycleDay__c;
        ba.batchName                    = q.zqu__BillingBatch__c;
        ba.paymentTerm                  = q.zqu__PaymentTerm__c;
        ba.paymentGateway               = q.zqu__PaymentGateway__c;
        ba.crmId                        = q.zqu__Account__c;
        ba.name                         = q.zqu__Account__r.Name;
        ba.invoiceDeliveryPrefsEmail    = q.zqu__BillingMethod__c == 'Email' || q.zqu__BillingMethod__c == 'Both' ? true : false;
        ba.invoiceDeliveryPrefsPrint    = q.zqu__BillingMethod__c == 'Print' || q.zqu__BillingMethod__c == 'Both' ? true : false;
        ba.codiceAmministazione         = q.zqu__Account__r.Codice_Pubblica_Amministrazione__c;
        ba.billToSalesforceText         = billToContact.Id;
        ba.soldToSalesforceText         = soldToContact.Id;
        ba.titoloBillTo                 = billToContact.Titolo__c;
        ba.partitaIvaBillTo             = billToContact.Account.Partita_IVA__c;
        ba.codiceClienteBillTo          = billToContact.Account.Codice_Cliente_BillTo_Contact__c;
        ba.codiceFiscaleBillTo          = billToContact.Codice_Fiscale__c;
        ba.supplementoCivicoSoldTo      = soldToContact.Supplemento_Civico__c;
        ba.supplementoCapSoldTo         = soldToContact.Supplemento_CAP__c;
        ba.metodoDiPagamento            = q.Metodo_di_pagamento__c;
        ba.salvaDatiCC                  = q.SalvareDatiCartadiCredito__c;
        ba.communicationProfileId       = getCommunicationProfileId(q);
        ba.hpmCreditCardPaymentMethodId = setDefaultPaymentMethodId(q);

        ZuoraContact btc = new ZuoraContact();
        btc.lastName                    = billToContact.LastName;
        btc.firstName                   = checkContactFirstName(billToContact);
        btc.address1                    = bilLToContact.MailingStreet;
        btc.city                        = billToContact.MailingCity;
        btc.state                       = billToContact.MailingState;
        btc.country                     = billToContact.MailingCountry;
        btc.zipCode                     = billToContact.MailingPostalCode;
        btc.workEmail                   = billToContact.Email;
        btc.mobilePhone                 = billToContact.MobilePhone;
        btc.workPhone                   = bilLToContact.Phone;

        ZuoraContact stc = new ZuoraContact();
        stc.lastName                    = soldToContact.LastName;
        stc.firstName                   = checkContactFirstName(soldToContact);
        stc.address1                    = soldToContact.MailingStreet;
        stc.city                        = soldToContact.MailingCity;
        stc.state                       = soldToContact.MailingState;
        stc.country                     = soldToContact.MailingCountry;
        stc.zipCode                     = soldToContact.MailingPostalCode;
        stc.workEmail                   = soldToContact.Email;
        stc.mobilePhone                 = soldToContact.MobilePhone;
        stc.workPhone                   = soldToContact.Phone;
        stc.county                      = setCounty(q, billToContact);  

        ba.billToContact = btc;
        ba.soldToContact = stc;

        return ba;
    }

    private static HttpResponse sendToZuora(ZuoraAccount ba){

        final Map<String, String> apiNameMap = new Map<String, String>{
            'currencyType'              => 'currency',
            'batchName'                 => 'batch',
            'billToSalesforceText'      => 'Bill_To_Salesforce_text__c',
            'soldToSalesforceText'      => 'Sold_To_Salesforce_text__c',
            'titoloBillTo'              => 'Titolo__c',
            'partitaIvaBillTo'          => 'Partitita_IVA_BillTO_Contact__c',
            'codiceClienteBillTo'       => 'Codice_Cliente_BillTo_Contact__c',
            'codiceFiscaleBillTo'       => 'Codice_Fiscale_BillTo_Contact__c',
            'codiceAmministazione'      => 'Codice_Pubblica_Amministrazione__c',
            'supplementoCivicoSoldTo'   => 'Supplemento_Civico__c',
            'supplementoCapSoldTo'      => 'SupplementoCAP__c',
            'metodoDiPagamento'         => 'Metodo_di_pagamento__c',
            'salvaDatiCC'               => 'SalvareDatiCartadiCredito__c'
        };

        String requestBody = JSON.serialize(ba, true);

        for(String s : apiNameMap.keySet()){
            requestBody = requestBody.replace(s, apiNameMap.get(s));
        }

        Zuora_Credential__c zuoraCredential = Zuora_Credential__c.getInstance('CreateAccount');
        System.debug('*_* zuoraCredential: ' + zuoraCredential);
        System.debug('*_* requestBody: ' + requestBody);

        Http rest = new Http();
        HttpRequest restRequest = new HttpRequest();
        restRequest.setMethod('POST');
        restRequest.setHeader('Content-Type', 'application/json;charset=UTF-8');
        restRequest.setEndpoint(zuoraCredential.EndPoint__c); 
        restRequest.setHeader('apiAccessKeyId', zuoraCredential.apiAccessKeyId__c);
        restRequest.setHeader('apiSecretAccessKey', zuoraCredential.apiSecretAccessKey__c);
        restRequest.setBody(requestBody);
        HttpResponse restResponse = rest.send(restRequest);

        return restResponse;
    }

    public static String checkContactFirstName(Contact c){
        String firstName = c.FirstName != null ? c.FirstName : AppConstants.ZUORA_PLACEHOLDER;
        return firstName;
    }

    public static String setCounty(zqu__Quote__c q, Contact billTo){

        String tipoCliente;
        if(q.Split_Payment__c == 'SI'){
            tipoCliente = 'PUBBLICA_AMMINISTRAZIONE';
        }else{
            if(billTo.Record_Type_Account__c == 'B2C'){
                tipoCliente = 'B2C';
            }else{
                tipoCliente = 'B2B';
            } 
        }

        String country;
        if(billTo.Codice_Nazione__c == 'IT'){
            country = 'ITALY';
        }else if(billTo.Codice_Nazione__c ==  'VA' || billTo.Codice_Nazione__c == 'SM'){
            country = 'SAN_MARINO_VAT';
        }else{
            country = billTo.Cee_Nazione__c;
        }

        String soldToCounty = country + '_' + tipoCliente;
        return soldToCounty;
    }

    private static String setDefaultPaymentMethodId(zqu__Quote__c q){
        String defaultPaymentMethodId = '';
        Map<String, String> paymentMethodsMap = getPaymentMethods();
        if(q.zqu__ElectronicPaymentMethodId__c != null){
            defaultPaymentMethodId = q.zqu__ElectronicPaymentMethodId__c;
        }else{
            if(paymentMethodsMap.containsKey(q.zqu__PaymentMethod__c)){
                defaultPaymentMethodId = paymentMethodsMap.get(q.zqu__PaymentMethod__c);
            }else{
                defaultPaymentMethodId = paymentMethodsMap.get('Other');
            }
        } 
        return defaultPaymentMethodId;
    }

    private static String getCommunicationProfileId(zqu__Quote__c q){
        String communicationProfileId = '';
        List<zqu__CommunicationProfile__c> communicationProfiles = [SELECT zqu__ZuoraId__c FROM zqu__CommunicationProfile__c WHERE Id = :q.zqu__CommunicationProfile__c LIMIT 1];
        if(!communicationProfiles.isEmpty()){
            communicationProfileId = communicationProfiles[0].zqu__ZuoraId__c;
        }
        return communicationProfileId;
    }

    private static Map<String, String> getPaymentMethods(){
        Map<String, String> paymentMethodsMap = new Map<String, String>();
        List<Zuora_Payment_Method__mdt> paymentMethodList = [SELECT MasterLabel, ZuoraId__c FROM Zuora_Payment_Method__mdt WHERE Active__c = true];
        for(Zuora_Payment_Method__mdt method : paymentMethodList){
            if(method.ZuoraId__c != null){
                paymentMethodsMap.put(method.MasterLabel, method.ZuoraId__c);
            }
        }
        return paymentMethodsMap;
    }

    public static zqu__Quote__c fetchQuote(Id quoteId) {
        //if(quoteId == null) return new zqu__Quote__c();
        final String additionalFields = 'zqu__Account__r.Name, zqu__Account__r.Codice_Pubblica_Amministrazione__c, zqu__Account__r.Id, Campagna__r.Name, zqu__BillToContact__r.Codice_Nazione__c';
        final String query = 'SELECT ' + String.join(fieldsOf('zqu__Quote__c'), ',') + ',' + additionalFields +
                                ' FROM zqu__Quote__c WHERE Id = :quoteId LIMIT 1';

        return (zqu__Quote__c) Database.query(query);
    }

    public static Contact fetchContact(Id contactId) {
        if(contactId == null) return new Contact();
        final String additionalFields = 'Account.Partita_IVA__c, Account.Codice_Cliente_BillTo_Contact__c';
        final String query = 'SELECT ' + String.join(fieldsOf('Contact'), ',') + ',' + additionalFields + 
                                ' FROM Contact WHERE Id = :contactId LIMIT 1';

        return (Contact) Database.query(query);
    }

    public static List<String> fieldsOf(String className) {
        final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

        return fields;
    }
    
    /*
    
	private static Rest_Request_Setting__mdt getZuoraCredential(){
        final List<Rest_Request_Setting__mdt> listZuoraCredential = [SELECT apiAccessKeyId__c, apiSecretAccessKey__c, EndPoint__c FROM Rest_Request_Setting__mdt WHERE MasterLabel = :System.URL.getSalesforceBaseUrl().getHost()];
        if(listZuoraCredential.isEmpty()) return null;
        return listZuoraCredential[0]; 
    }

    try{
        MetodoDiPagamento__mdt[] metodoDiPagamentoId = [SELECT ZuoraId__c FROM MetodoDiPagamento__mdt WHERE MasterLabel = :q.Metodo_di_Pagamento__c];

        Zuora.zApi zApiInstance = new Zuora.zApi();
        zApiInstance.zlogin();

        Zuora.zObject acc = new Zuora.zObject('Account');
        acc.setValue('crmId', q.zqu__Account__c);
        acc.setValue('Name', q.zqu__Account__r.Name);
        acc.setValue('Currency', q.zqu__Currency__c);
        acc.setValue('Batch', q.zqu__BillingBatch__c);
        acc.setValue('AutoPay', false);
        acc.setValue('BillCycleDay', q.zqu__BillCycleDay__c);
        acc.setValue('DefaultPaymentMethodId', metodoDiPagamentoId[0]);
        acc.setValue('PaymentTerm', q.zqu__PaymentTerm__c);
        acc.setValue('PaymentGateway', q.zqu__PaymentGateway__c);
        acc.setValue('Status', 'Draft');
        acc.setValue('Metodo_di_pagamento__c', q.Metodo_di_pagamento__c);
        acc.setValue('Codice_Fiscale_BillTo_Contact__c', billToContact.Codice_Fiscale__c);
        acc.setValue('Partitita_IVA_BillTO_Contact__c', billToContact.Account.Partita_IVA__c);
        acc.setValue('Codice_Cliente_BillTo_Contact__c', billToContact.Account.Codice_Cliente_BillTo_Contact__c);
        acc.setValue('SalvareDatiCartadiCredito__c', q.SalvareDatiCartadiCredito__c);
        acc.setValue('SupplementoCAP__c', soldToContact.Supplemento_CAP__c);
        acc.setValue('Supplemento_Civico__c',soldToContact.Supplemento_Civico__c);
        acc.setValue('Sold_To_Salesforce_text__c', q.zqu__SoldToContact__c);
        acc.setValue('Bill_To_Salesforce_text__c', q.zqu__BillToContact__c);
        acc.setValue('CommunicationProfileId', q.zqu__CommunicationProfile__c);
        acc.setValue('InvoiceDeliveryPrefsEmail', q.zqu__BillingMethod__c == 'Email' || q.zqu__BillingMethod__c == 'Both' ? true : false);
        acc.setValue('InvoiceDeliveryPrefsPrint', q.zqu__BillingMethod__c == 'Print' || q.zqu__BillingMethod__c == 'Both' ? true : false);

        System.debug('*_* ' + acc);
        List<Zuora.zObject> objs = new List<Zuora.zObject>{acc};
        List<Zuora.zApi.SaveResult> results = zApiInstance.zcreate(objs);
        System.debug('*_* ' + results);

        if(results[0].Success){
            String createdId = results[0].Id;
            Zuora.zObject billTo = new Zuora.zObject('Contact');
            billTo.setValue('AccountId', createdId);
            billTo.setValue('Country', billToContact.MailingCountry);
            billTo.setValue('LastName', billToContact.LastName);
            billTo.setValue('FirstName', bilLToContact.FirstName != null ? bilLToContact.FirstName : 'Spett.');
            billTo.setValue('Address1', billToContact.MailingStreet);
            billTo.setValue('State', billToContact.MailingState);
            billTo.setValue('PostalCode', billToContact.MailingPostalCode);

            Zuora.zObject soldTo = new Zuora.zObject('Contact');
            soldTo.setValue('AccountId', createdId);
            soldTo.setValue('Country', soldToContact.MailingCountry);
            soldTo.setValue('LastName', soldToContact.LastName);
            soldTo.setValue('FirstName', soldToContact.FirstName != null ? soldToContact.FirstName : 'Spett.');
            soldTo.setValue('Address1', soldToContact.MailingStreet);
            soldTo.setValue('State', soldToContact.MailingState);
            soldTo.setValue('PostalCode', soldToContact.MailingPostalCode);

            System.debug('*_* ' + billTo);
            System.debug('*_* ' + soldTo);
            List<Zuora.zObject> billToObj = new List<Zuora.zObject>{billTo};
            List<Zuora.zApi.SaveResult> billToResult = zApiInstance.zcreate(billToObj);
            String billToId = billToResult[0].Id;

            List<Zuora.zObject> soldToIdObj = new List<Zuora.zObject>{soldTo};
            List<Zuora.zApi.SaveResult> soldToResult = zApiInstance.zcreate(soldToIdObj);
            String soldToId = soldToResult[0].Id;

            Zuora.zObject accUpdate = new Zuora.zObject('Account');
            accUpdate.setValue('Id', createdId);
            accUpdate.setValue('Status', 'Active');
            accUpdate.setValue('BillToId', billToId);
            accUpdate.setValue('SoldToId', soldToId);
            List<Zuora.zObject> zObjs = new List<Zuora.zObject>{accUpdate};
            System.debug('*_* ' + accUpdate);
            List<Zuora.zApi.SaveResult> updateResult = zApiInstance.zupdate(zObjs);
            System.debug('*_* ' + updateResult);
        
            q.zqu__ZuoraAccountID__c = createdId;
            update q;

        }else {
            return new response(true, results[0].Errors + '');
        } 
    }catch(Zuora.zRemoteException ex) {
        System.debug('*_* ' + ex);
    }catch(Zuora.zAPIException ex) {
        System.debug('*_* ' + ex);
    }catch(Zuora.zForceException ex) {
        System.debug('*_* ' + ex);
    }

    JSONGenerator gen = JSON.createGenerator(true);
    gen.writeStartObject();
    gen.writeBooleanField('autoPay', false);
    gen.writeStringField('currency', q.zqu__Currency__c);
    gen.writeNumberField('billCycleDay', Decimal.valueOf(q.zqu__BillCycleDay__c));
    gen.writeStringField('batch', q.zqu__BillingBatch__c);
    gen.writeStringField('paymentGateway', q.zqu__PaymentGateway__c);
    gen.writeStringField('paymentTerm', q.zqu__PaymentTerm__c);
    gen.writeStringField('crmId', q.zqu__Account__c);        
    gen.writeStringField('name', q.zqu__Account__r.Name);
    gen.writeStringField('Codice_Pubblica_Amministrazione__c', q.zqu__Account__r.Codice_Pubblica_Amministrazione__c);
    gen.writeStringField('Bill_To_Salesforce_text__c', billToContact.Id);
    gen.writeStringField('Sold_To_Salesforce_text__c', soldToContact.Id);
    gen.writeStringField('Titolo', billToContact.Titolo__c);
    gen.writeStringField('Partitita_IVA_BillTO_Contact__c', billToContact.Account.Partita_IVA__c);
    gen.writeStringField('Codice_Fiscale_BillTo_Contact__c', billToContact.Codice_Fiscale__c);
    gen.writeStringField('Supplemento_Civico__c', soldToContact.Supplemento_Civico__c);
    gen.writeStringField('SupplementoCAP__c', soldToContact.Supplemento_CAP__c);
    gen.writeStringField('Metodo_di_pagamento__c', q.Metodo_di_pagamento__c);
    gen.writeStringField('SalvareDatiCartadiCredito__c', q.SalvareDatiCartadiCredito__c);
    gen.writeStringField('invoiceTemplateId', credenzialiCallout[0].invoiceTemplateId__c);
    gen.writeStringField('invoiceDeliveryPrefsEmail', q.zqu__BillingMethod__c == 'Email' || q.zqu__BillingMethod__c == 'Both' ? true : false);
    gen.writeStringField('invoiceDeliveryPrefsPrint', q.zqu__BillingMethod__c == 'Print' || q.zqu__BillingMethod__c == 'Both' ? true : false);
    gen.writeStringField('hpmCreditCardPaymentMethodId', );
    gen.writeFieldName('billToContact');
    gen.writeObject(btc);
    gen.writeFieldName('soldToContact');
    gen.writeObject(stc);
    gen.writeEndObject();

    String requestBody = gen.getAsString();
    System.debug('*_* Json request body' + requestBody);
    */             
}