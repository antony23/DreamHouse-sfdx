@isTest
public class TestRigaOrdineTriggerHandler {
	private static final Integer NUM_OF_ORDERS = 3;
	private static final Integer NUM_OF_ORDER_LINES = 3;

	private static Account account;
	private static Contact contact;
	private static Map<Ordine__c, RigaOrdine__c[]> orderMap;
	private static final String[] ProfessionalProdotto = getProfessionalProdottoValues();

	@isTest
	public static void test() {
		Map<String, String[]> orderPMap = new Map<String, String[]> {
			extractProduct() => extractProducts(NUM_OF_ORDER_LINES),
			extractProduct() => extractProducts(NUM_OF_ORDER_LINES),
			extractProduct() => extractProducts(NUM_OF_ORDER_LINES)
		};

		setup(orderPMap);

		assertAll();

		for(Ordine__c ordine : orderMap.keySet()) {
			for(RigaOrdine__c ro : orderMap.get(ordine)) {
				ro.Prodotto__c = extractProduct();
			}

			update orderMap.get(ordine);
		}

		reloadAll();
		assertAll();

		final List<Ordine__c> ordini = new List<Ordine__c>(orderMap.keySet());

		delete orderMap.get(ordini[0]);
		reloadAll();
		assertAll();

		delete ordini[0];
		delete ordini[1];
		reloadAll();
		assertAll();
	}

	private static void assertAll() {
		final Set<String> contactProducts = new Set<String>();

		for(Ordine__c ordine : orderMap.keySet()) {
			final String righeOrdineProducts = joinProducts(orderMap.get(ordine));
			final String ordineProdotti = joinProducts(splitProducts(ordine.Prodotto__c));

			System.debug('>> Ordine[' + ordine.Id + '].Prodotto__c = ' + ordine.Prodotto__c);
			System.assertEquals(ordineProdotti, righeOrdineProducts);

			contactProducts.addAll(splitProducts(righeOrdineProducts));
		}

		System.debug('>> Contact[' + contact.Id + '].Prodotto__c = ' + contact.Prodotto__c);
		System.assertEquals(joinProducts(splitProducts(contact.Prodotto__c)), joinProducts(contactProducts));
	}

	private static String[] splitProducts(String products) {
		if(String.isBlank(products)) return new String[]{};
		
		String[] iterable = products.split('[;]+');

		for(Integer i = 0; i < iterable.size(); i++) {
			iterable[i] = iterable[i].toLowerCase();
		}

		return iterable;
	}

	private static String joinProducts(Set<String> prodSet) {
		final Set<String> products = new Set<String>();

		for(String prod : prodSet) {
			products.add(prod.toLowerCase());
		}

		String[] iterable = new List<String>(products);
		iterable.sort();

		return String.join(iterable, ';');
	}

	private static String joinProducts(List<String> prodSet) {
		final Set<String> products = new Set<String>();

		for(String prod : prodSet) {
			products.add(prod.toLowerCase());
		}

		String[] iterable = new List<String>(products);
		iterable.sort();

		return String.join(iterable, ';');
	}

	private static String joinProducts(RigaOrdine__c[] righeOrdine) {
		final Set<String> products = new Set<String>();

		for(RigaOrdine__c ro : righeOrdine) {
			products.add(ro.Prodotto__c.toLowerCase());
		}

		String[] iterable = new List<String>(products);
		iterable.sort();

		return String.join(iterable, ';');
	}

	private static void setup(Map<String, String[]> orderPMap) {
		final RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'B2C' LIMIT 1];

		account = new Account(Name='Test Account', Type='Cliente', RecordTypeId=rt.Id, BillingStreet='TEST STREET 1', BillingCity='ROMA', BillingState='RM', BillingPostalCode='00100', BillingCountry='ITALY', Phone='1234567890',Codice_Fiscale__c='TSTCNT80A01H501X',Nazione__c='ITALIA',Cellulare__c='1234567890',Comunicazioni_Commerciali_Marketing__c='NO',Email__c='test@email.com', Stato__c='Cliente',Cessione_a_Terzi__c='NO',Codice_Nazione__c='IT', Con_Avvisi__c=true, Con_Fattura__c=false, Copie_di_Gracing__c=false,Is_Test__c=false,Lingua__c='Italiano',Trattamento_Dati__c='SI',Tipo_Cliente__c='Diffusione', CognomeClienteB2C__c='Account', Creato_da_Lead__c=false, NomeClienteB2C__c='Test', Notifiche_Email_Disabilitate__c = true);
		AccountTriggerHandler.skip = true;
		insert account;

		contact = new Contact(AccountId=account.Id, LastName='Account', FirstName='Test', MailingStreet='TEST STREET 1', MailingCity='ROMA', MailingState='RM', MailingPostalCode='00100', MailingCountry='ITALY', Phone='1234567890', MobilePhone='1234567890', Codice_Fiscale__c='TSTCNT80A01H501X', Email='test@email.com',MainContact__c=true,Sesso__c='M',Cessione_a_Terzi__c='NO',  Comunicazioni_Commerciali_Marketing__c='NO',Trattamento_Dati__c='SI',Tipo_Cliente__c='Diffusione', Comunicazioni_Commerciali_Indirette__c='NO');
		//ContactTriggerHandler.skip = true;
		insert contact;

		Set<Id> ordiniIds = new Set<Id>();
		Integer i = 1;

		for(String key : orderPMap.keySet()) {
			final String oName = String.valueOf(i++).leftPad(10, '0');

			ordiniIds.add(createOrder(contact, oName, key, orderPMap.get(key)));
		}

		reloadAll();
	}

	private static void reloadAll() {
		final Map<Id, Ordine__c> ordini = new Map<Id, Ordine__c>([SELECT Id, Contatto_Committente__c, Prodotto__c FROM Ordine__c]);
		final Map<Id, RigaOrdine__c> righe = new Map<Id, RigaOrdine__c>([SELECT Id, Ordine__c, Prodotto__c FROM RigaOrdine__c]);
		orderMap = new Map<Ordine__c, RigaOrdine__c[]>();

		for(RigaOrdine__c riga : righe.values()) {
			final Ordine__c ordine = ordini.get(riga.Ordine__c);

			if(!orderMap.containsKey(ordine)) {
				orderMap.put(ordine, new List<RigaOrdine__c>());
			}

			orderMap.get(ordine).add(riga);
		}

		contact = [SELECT Id, FirstName, LastName, Prodotto__c FROM Contact WHERE Id = :contact.Id];
	}

	private static Id createOrder(Contact contact, String name, String prodottoAcquistato, String[] prodotti) {
		final Ordine__c ordine = new Ordine__c(Name = name, Nr_Ordine__c = name, Data_ordine__c = Date.today(), Contatto_Committente__c = contact.Id);
		OrdineTriggerHandler.skip = true;
		insert ordine;
		OrdineTriggerHandler.skip = false;

		final Prodotto_Acquistato__c pa = new Prodotto_Acquistato__c(Name = prodottoAcquistato);
		insert pa;

		for(Integer i = 0; i < prodotti.size(); i++) {
			final String rName = name + '_' + String.valueOf(i + 1).leftPad(10, '0');

			final RigaOrdine__c rigaOrdine = new RigaOrdine__c(Name = rName, Nr_Riga_d_ordine__c = rName, Ordine__c = ordine.Id, Prodotto_Acquistato__c = pa.Id, Prodotto__c = prodotti[i]);
			insert rigaOrdine;
		}

		return ordine.Id;
	}

	private static String extractProduct() {
		return ProfessionalProdotto[(Integer) (Math.random() * ProfessionalProdotto.size())];
	}
	
	private static String[] extractProducts(Integer n) {
		final String[] products = new String[]{};

		for(Integer i = 0; i < n; i++) {
			products.add(ProfessionalProdotto[(Integer) (Math.random() * ProfessionalProdotto.size())]);
		}

		return products;
	}

	private static String[] getProfessionalProdottoValues() {
		List<String> pickListValuesList= new List<String>();

		SObjectField fProdotto = SObjectType.Opportunity.fields.Prodotto__c.getSObjectField();
		List<PicklistEntry> entries = fProdotto.getDescribe().getPicklistValues();

		for( Schema.PicklistEntry pickListVal : entries){
			pickListValuesList.add(pickListVal.getLabel());
		}     

		return pickListValuesList;
	}
}