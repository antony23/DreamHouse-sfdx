global class BatchCancellaAvvisi implements Database.Batchable<sObject>, Schedulable {
	
	String queryString;
	
	global BatchCancellaAvvisi(String query) {
		this.queryString = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}

	global void execute(SchedulableContext SC) {
		Database.executeBatch(new BatchCancellaAvvisi(queryString));
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		try{
        	List<Avviso_di_Rinnovo__c> listaAvvisi = (List<Avviso_di_Rinnovo__c>)scope;
            for(Avviso_di_Rinnovo__c a : listaAvvisi){
				a.Stato_Avviso__c = 'Cancellato';
            }
 			AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = true;
            update scope;
            AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = false;
        }catch(Exception ex){   
        	System.debug(ex.getMessage()+'\n'+ex.getStackTraceString());
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}