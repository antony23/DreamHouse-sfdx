@isTest(SeeAllData=true)
private class TestProductRateDelete {
	
	@isTest static void testDelete() {
				Test.startTest();
				zqu__ProductRatePlanCharge__c prpc = new zqu__ProductRatePlanCharge__c (zqu__BillCycleType__c = 'ChargeTriggerDay',
																						zqu__ProductRatePlanChargeFullName__c = 'Prova Prova',
																						zqu__RecurringPeriod__c = 'Annual',
																						zqu__Model__c = 'Tiered Pricing',
																						zqu__Type__c = 'OneTime',
																						zqu__ProductRatePlan__c = [SELECT id, Name FROM zqu__ProductRatePlan__c	LIMIT 1].id,
																						zqu__TriggerEvent__c = 'CustomerAcceptance',
																						zqu__UseDiscountSpecificAccountingCode__c = false,
																						zqu__Deleted__c = true );


				insert prpc;
				

				ProductRatePlanChargeDelete.DeleteRatePlan();

				zqu__ProductRatePlanCharge__c prpcDeleted =[ SELECT id, isDeleted FROM zqu__ProductRatePlanCharge__c WHERE id = :prpc.id ALL ROWS];
				System.assertEquals(prpcDeleted.IsDeleted, true);

				Test.stopTest();



	}
	
}