@isTest
private class ZTest_AddAttachmentExtension
{
	@isTest
	static void testError() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		Test.setCurrentPage(Page.AddAttachment);
		ApexPages.currentPage().getParameters().put('id', t.Id);
		AddAttachmentExtension controller = new AddAttachmentExtension(new ApexPages.StandardController(t));
		controller.uploadAttachment();
		controller.back();
		Test.stopTest();
	}

	@isTest
	static void testAttach() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		Test.setCurrentPage(Page.AddAttachment);
		ApexPages.currentPage().getParameters().put('id', t.Id);
		AddAttachmentExtension controller = new AddAttachmentExtension(new ApexPages.StandardController(t));
		Attachment attach = new Attachment();
		attach.OwnerId = t.OwnerId;
		attach.ParentId = t.Id;
		attach.Name='Test';
		Blob body=Blob.valueOf('Test Body');
		attach.Body=body;
		controller.file = attach;
		controller.uploadAttachment();
		Test.stopTest();
	}

	@isTest
	static void testRedirect() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];

		User u = [SELECT Id FROM User WHERE Alias = 'newUser2' LIMIT 1];

		System.runAs(u){
			Test.setCurrentPage(Page.AddAttachment);
			ApexPages.currentPage().getParameters().put('id', t.Id);
			AddAttachmentExtension controller = new AddAttachmentExtension(new ApexPages.StandardController(t));
			controller.checkPermission();
		}

		Test.stopTest();
	}

	@testSetup
	public static void testSetup() {
		Account a = new Account(Name='Test Attachment Account');
        insert a;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Professional'];
		String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
		User u = new User(	Alias = 'newUser', Email='newuser@testorg.com',
							EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							LocaleSidKey='en_US', ProfileId = p.Id,
							TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName);
		insert u;

		String uniqueUserName2 = 'standarduser2' + DateTime.now().getTime() + '@testorg.com';
		User u2 = new User(	Alias = 'newUser2', Email='newuser@testorg.com',
							EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							LocaleSidKey='en_US', ProfileId = p.Id,
							TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName2);
		insert u2;

		Task task = new Task(WhatId = a.Id, OwnerId = u.Id, Subject = 'Email', Priority = 'Normale', Status = 'In corso');
        insert task;

	}
	
}