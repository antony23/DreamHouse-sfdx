public class SpedizioneTriggerHandler {
	public static Boolean skip = false;

	public static void onBeforeInsert(List<Spedizione__c> triggerNew, Map<Id, Spedizione__c> triggerNewMap) {
		setFields(triggerNew);
    }
    
    public static void onAfterInsert(List<Spedizione__c> triggerNew, Map<Id, Spedizione__c> triggerNewMap) {
    }

    public static void onBeforeUpdate(List<Spedizione__c> triggerOld, List<Spedizione__c> triggerNew, Map<Id, Spedizione__c> triggerOldMap, Map<Id, Spedizione__c> triggerNewMap){   
    	setFields(triggerNew);
    }

    public static void onAfterUpdate(List<Spedizione__c> triggerOld, List<Spedizione__c> triggerNew, Map<Id, Spedizione__c> triggerOldMap, Map<Id, Spedizione__c> triggerNewMap){   
    }

    public static void onBeforeDelete(List<Spedizione__c> triggerOld, Map<Id, Spedizione__c> triggerOldMap){  
    }

    public static void onAfterDelete(List<Spedizione__c> triggerOld, Map<Id, Spedizione__c> triggerOldMap){  
    }

    public static void setFields(List<Spedizione__c> spedizioni){
    	Set<Id> subscriptionIds = new Set<Id>();

        System.debug('[SpedizioneTriggerHandler][setFields] >> spedizioni: ' + JSON.serialize(spedizioni));

    	for(Spedizione__c spedizione : spedizioni){
    		subscriptionIds.add(spedizione.Subscription__c);
    	}

    	Map<Id,Zuora__Subscription__c> subscriptionsMap = new Map<Id,Zuora__Subscription__c>(
    		[SELECT Id,Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionIds]
    	);

        System.debug('[SpedizioneTriggerHandler][setFields] >> subscriptionsMap: ' + JSON.serialize(subscriptionsMap));

    	for(Spedizione__c spedizione : spedizioni){
    		Zuora__Subscription__c subscription = subscriptionsMap.get(spedizione.Subscription__c);
    		spedizione.Subscription_Zuora_Id__c = subscription.Zuora__Zuora_Id__c;
    	}
    }
}