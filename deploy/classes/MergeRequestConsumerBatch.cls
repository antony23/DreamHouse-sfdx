public class MergeRequestConsumerBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.StateFul {
    private static final List<MergeRequestConsumerBatch> queue = new List<MergeRequestConsumerBatch>();
    private static MergeRequestConsumerBatch currentBatch;
    private static final Integer ZUORA_MAX_ATTEMPTS = 5;

    final String BATCH_QUERY;
    final Set<Id> REQUEST_IDS;
    final Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> BAS_TO_ROLLBACK;
    final Map<Contact, Set<String>> CC_TO_ROLLBACK;
    
    private MergeRequestConsumerBatch(Set<Id> requestIds) {
        REQUEST_IDS = requestIds;

        final Map<String, String[]> lookupFilterMap = new Map<String, String[]>{
            'Merge_Request__c' => new String[]{ 'Master_Account__c', 'Slave_Account__c', 'Master_Contact__c', 'Slave_Contact__c', 'RecordTypeId' }
        };

        final String[] fields = SOQLTools.fieldsOf('Merge_Request__c', lookupFilterMap, 1);
        
        BATCH_QUERY =   'SELECT ' + String.join(fields, ',') + ' ' +
                        'FROM Merge_Request__c ' +
                        'WHERE Id IN :REQUEST_IDS AND Status__c = \'Nuovo\' ';

        BAS_TO_ROLLBACK = new Map<Merge_Request__c, List<Zuora__CustomerAccount__c>>();
        CC_TO_ROLLBACK = new Map<Contact, Set<String>>();
    }
	
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('[MergeRequestConsumerBatch] Job started: ' + BC.getJobId());
        return Database.getQueryLocator(BATCH_QUERY);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        final Merge_Request__c request = (Merge_Request__c) scope.get(0);
        System.debug('[MergeRequestConsumerBatch] Processing Merge Request: ' + request.Id);

        try {
            switch on request.RecordType.DeveloperName {
                when 'Account' { mergeAccounts(request); }
                when 'Contact' { mergeContacts(request); }
                when else { throw new DomusException.IllegalStateException('[MergeRequestConsumerBatch] RecordType \'' + request.RecordType.Name + '\' is not supported.'); }
            }
        } catch(Exception e) {
            request.Error_Message__c = generateErrorMessage(e);
            request.Status__c = 'Errore';
        }

        clean(request);
        System.debug('[MergeRequestConsumerBatch] Updating Merge Request: ' + request.Id);
        
        try {
            update request;
        } catch(Exception e) {
            System.debug('[MergeRequestConsumerBatch] MergeRequestUpdateException: ' + generateErrorMessage(e));
        }
    }

    public void finish(Database.BatchableContext BC) {
        try {
            rollbackCustomerContacts(CC_TO_ROLLBACK);
        } catch(Exception e) {
            System.debug('[MergeRequestConsumerBatch] RollbackException: ' + generateErrorMessage(e));
        }

        try {
            rollbackCustomerAccounts(BAS_TO_ROLLBACK);
        } catch(Exception e) {
            System.debug('[MergeRequestConsumerBatch] RollbackException: ' + generateErrorMessage(e));
        }

        System.debug('[MergeRequestConsumerBatch] Job finished: ' + BC.getJobId());
        tail();
    }

    public static void consume(Set<Id> requestIds) {
        final MergeRequestConsumerBatch mrcBatch = new MergeRequestConsumerBatch(requestIds);

        if(currentBatch == null) {
            if(Database.executeBatch(mrcBatch, 1) != '000000000000000') {
                currentBatch = mrcBatch;
                System.debug('[MergeRequestConsumerBatch] New batch on execution.');
            } else {
                System.debug('[MergeRequestConsumerBatch] Batch execution failed.');
            }
        } else {
            queue.add(mrcBatch);
            System.debug('[MergeRequestConsumerBatch] New batch on queue.');
        }

        System.debug('[MergeRequestConsumerBatch] Waiting batches: ' + queue.size());
    }

    private void tail() {
        final MergeRequestConsumerBatch bkpBatch = currentBatch;

        while(queue.size() > 0) {
            final MergeRequestConsumerBatch nextBatch = queue.get(0);
            queue.remove(0);

            if(Database.executeBatch(nextBatch, 1) != '000000000000000') {
                currentBatch = nextBatch;
                System.debug('[MergeRequestConsumerBatch] Batch pulled out from queue and executed.');
                break;
            }

            System.debug('[MergeRequestConsumerBatch] Batch pulled out from queue and descarded.');
        }

        System.debug('[MergeRequestConsumerBatch] Waiting batches: ' + queue.size());
        
        if(currentBatch == bkpBatch) {
            currentBatch = null;
        }
    }

    private Merge_Request__c clean(Merge_Request__c request) {
        switch on request.RecordType.DeveloperName {
            when 'Account' {
                try{SObject sobj = [ SELECT Id FROM Account WHERE Id = :request.Master_Account__c LIMIT 1 ];}
                catch(Exception e){ request.Master_Account__c = null; }

                try{SObject sobj = [ SELECT Id FROM Account WHERE Id = :request.Slave_Account__c LIMIT 1 ];}
                catch(Exception e){ request.Slave_Account__c = null; }
            }
            when 'Contact' {
                try{SObject sobj = [ SELECT Id FROM Contact WHERE Id = :request.Master_Contact__c LIMIT 1 ];}
                catch(Exception e){ request.Master_Contact__c = null; }

                try{SObject sobj = [ SELECT Id FROM Contact WHERE Id = :request.Slave_Contact__c LIMIT 1 ];}
                catch(Exception e){ request.Slave_Contact__c = null; }
            }
        }

        return request;
    }

    private void mergeAccounts(Merge_Request__c request) {
        final List<Zuora__CustomerAccount__c> customerAccounts = [ SELECT Id, Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :request.Slave_Account__c ];
        final SavePoint sp;

        try {
            moveCustomerAccounts(request, customerAccounts);
            sp = Database.setSavePoint();

            if(request.Status__c != 'Errore') {
                moveContacts(request);

				if(request.Status__c != 'Errore') {
					final Account master = [ SELECT Id FROM Account WHERE Id = :request.Master_Account__c LIMIT 1 ];
					final Account slave = [ SELECT Id FROM Account WHERE Id = :request.Slave_Account__c LIMIT 1 ];
					Database.merge(master, slave, true);
				}
            }
        } catch(Exception e) {
            request.Error_Message__c = generateErrorMessage(e);
            request.Status__c = 'Errore';
        }

        if(request.Status__c == 'Errore') {
            if(sp != null) Database.rollback(sp);
            BAS_TO_ROLLBACK.put(request, customerAccounts);
        } else {
            request.Status__c = 'Elaborato';
            //request.Slave_Account__c = null;
        }
    }

    private void mergeContacts(Merge_Request__c request) {
        final List<Zuora__CustomerAccount__c> customerAccounts = [ SELECT Id, Zuora__Zuora_Id__c, Bill_To_Salesforce__c, Sold_To_Salesforce__c, Codice_Cliente_BillTo_Contact__c FROM Zuora__CustomerAccount__c WHERE Bill_To_Salesforce__c = :request.Slave_Contact__c OR Sold_To_Salesforce__c = :request.Slave_Contact__c ];
        final Set<String> slaveZIDs = fetchCustomerContactZIDs(request.Slave_Contact__c);

        final SavePoint sp;

        try {
            moveCustomerAccounts(request, customerAccounts);

            if(request.Status__c != 'Errore') {
                updateCustomerContacts(request, slaveZIDs);

                if(request.Status__c != 'Errore') {
                    sp = Database.setSavePoint();
                    updateAssets(request);

                    if(request.Status__c != 'Errore') {
                        updateMovStocks(request);

                        if(request.Status__c != 'Errore') {
                            updateEntitlements(request);

                            if(request.Status__c != 'Errore') {
                                updateAvvisi(request);

								if(request.Status__c != 'Errore') {
									final Contact master = [ SELECT Id, MainContact__c FROM Contact WHERE Id = :request.Master_Contact__c LIMIT 1 ];
									final Contact slave = [ SELECT Id, MainContact__c FROM Contact WHERE Id = :request.Slave_Contact__c LIMIT 1 ];
									
									if(slave.MainContact__c && !master.MainContact__c) {
										master.MainContact__c = True;
										update master;
									}

									Database.merge(master, slave, true);
								}
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            if(true) throw e;
            request.Error_Message__c = generateErrorMessage(e);
            request.Status__c = 'Errore';
        }

        if(request.Status__c == 'Errore') {
            if(sp != null) Database.rollback(sp);
            BAS_TO_ROLLBACK.put(request, customerAccounts);
            CC_TO_ROLLBACK.put(request.Slave_Contact__r, slaveZIDs);
        } else {
            request.Status__c = 'Elaborato';
            //request.Slave_Contact__c = null;
        }
    }

    private static void updateCustomerContacts(Merge_Request__c request, Set<String> slaveZIDs) {
        if(slaveZIDs.size() == 0) return;

        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        final Map<String, SObject> zObjMap = new Map<String, SObject>();
        final List<Zuora.zObject> zObjList = new List<Zuora.zObject>();

        for(String slaveZID : slaveZIDs) {
            if(String.isBlank(slaveZID)) continue;
            zObjMap.put(slaveZID, request.Master_Contact__r);

            final Zuora.zObject zObj = new Zuora.zObject('Contact');

            zObj.setValue('Id', slaveZID);
            zObj.setValue('Address1', request.Master_Contact__r.MailingStreet);
            zObj.setValue('City', request.Master_Contact__r.MailingCity);
            zObj.setValue('Country', request.Master_Contact__r.MailingCountry);
            zObj.setValue('FirstName', checkContactFirstName(request.Master_Contact__r));
            zObj.setValue('LastName', request.Master_Contact__r.LastName);
            zObj.setValue('PostalCode', request.Master_Contact__r.MailingPostalCode);
            zObj.setValue('State', request.Master_Contact__r.MailingState);
            zObj.setValue('WorkEmail', request.Master_Contact__r.Email);
            zObj.setValue('WorkPhone', request.Master_Contact__r.Phone);
            zObj.setValue('MobilePhone', request.Master_Contact__r.MobilePhone);

            zObjList.add(zObj);
        }

        if(zObjList.size() == 0) return;

        final List<Zuora.zApi.SaveResult> results = zupdate(zApiInstance, zObjList);
        final String messageError = generateErrorMessage(results, 'Zuora__CustomerAccount__c', zObjMap);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    @testVisible
    private static void rollbackCustomerContacts(Map<Contact, Set<String>> mapZIDCC) {
        if(mapZIDCC.size() == 0) return;

        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        final List<Zuora.zObject> zObjList = new List<Zuora.zObject>();

        for(Contact slave : mapZIDCC.keySet()) {
            final Set<String> zuoraIds = mapZIDCC.get(slave);

            for(String slaveZID : zuoraIds) {
                if(String.isBlank(slaveZID)) continue;

                final Zuora.zObject zObj = new Zuora.zObject('Contact');

                zObj.setValue('Id', slaveZID);
                zObj.setValue('Address1', slave.MailingStreet);
                zObj.setValue('City', slave.MailingCity);
                zObj.setValue('Country', slave.MailingCountry);
                zObj.setValue('FirstName', checkContactFirstName(slave));
                zObj.setValue('LastName', slave.LastName);
                zObj.setValue('PostalCode', slave.MailingPostalCode);
                zObj.setValue('State', slave.MailingState);
                zObj.setValue('WorkEmail', slave.Email);
                zObj.setValue('WorkPhone', slave.Phone);
                zObj.setValue('MobilePhone', slave.MobilePhone);

                zObjList.add(zObj);
            }
        }

        if(zObjList.size() == 0) return;

        try {
            zupdate(zApiInstance, zObjList);
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e);
        }
    }

    private static void moveCustomerAccounts(Merge_Request__c request, List<Zuora__CustomerAccount__c> customerAccounts) {
        if(customerAccounts.size() == 0) return;

        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        final List<Zuora.zObject> zObjList = new List<Zuora.zObject>();
        final Map<String, SObject> zObjMap = new Map<String, SObject>();

        for(Zuora__CustomerAccount__c ca : customerAccounts) {
            zObjMap.put(ca.Zuora__Zuora_Id__c, ca);

            final Zuora.zObject zObj = new Zuora.zObject('Account');
            zObj.setValue('Id', ca.Zuora__Zuora_Id__c);

            switch on request.RecordType.DeveloperName {
                when 'Account' {
                    zObj.setValue('CrmId', request.Master_Account__c);
                    zObj.setValue('Name', request.Master_Account__r.Name);
                }
                when 'Contact' {
                    if(ca.Bill_To_Salesforce__c == request.Slave_Contact__c) {
                        zObj.setValue('Bill_To_Salesforce_text__c', request.Master_Contact__c);
                    }

                    if(ca.Codice_Cliente_BillTo_Contact__c == request.Slave_Contact__r.CodiceCliente__c) {
                        zObj.setValue('Codice_Cliente_BillTo_Contact__c', request.Master_Contact__r.CodiceCliente__c);
                    }

                    if(ca.Sold_To_Salesforce__c == request.Slave_Contact__c) {
                        zObj.setValue('Sold_To_Salesforce_text__c', request.Master_Contact__c);
                    }
                }
            }
            
            zObjList.add(zObj);
        }

        if(zObjList.size() == 0) return;

        final List<Zuora.zApi.SaveResult> results = zupdate(zApiInstance, zObjList);
        final String messageError = generateErrorMessage(results, 'Zuora__CustomerAccount__c', zObjMap);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    @testVisible
    private static void rollbackCustomerAccounts(Map<Merge_Request__c, List<Zuora__CustomerAccount__c>> mapMRCA) {
        if(mapMRCA.size() == 0) return;

        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        final List<Zuora.zObject> zObjList = new List<Zuora.zObject>();

        for(Merge_Request__c mr : mapMRCA.keySet()) {
            for(Zuora__CustomerAccount__c ca : mapMRCA.get(mr)) {
                final Zuora.zObject zObj = new Zuora.zObject('Account');
                zObj.setValue('Id', ca.Zuora__Zuora_Id__c);

                switch on mr.RecordType.DeveloperName {
                    when 'Account' {
                        zObj.setValue('CrmId', mr.Slave_Account__c);
                        zObj.setValue('Name', mr.Slave_Account__r.Name);
                    }
                    when 'Contact' {
                        if(ca.Bill_To_Salesforce__c == mr.Slave_Contact__c) {
                            zObj.setValue('Bill_To_Salesforce_text__c', mr.Slave_Contact__c);
                        }

                        if(ca.Codice_Cliente_BillTo_Contact__c == mr.Slave_Contact__r.CodiceCliente__c) {
                            zObj.setValue('Codice_Cliente_BillTo_Contact__c', mr.Slave_Contact__r.CodiceCliente__c);
                        }

                        if(ca.Sold_To_Salesforce__c == mr.Slave_Contact__c) {
                            zObj.setValue('Sold_To_Salesforce_text__c', mr.Slave_Contact__c);
                        }
                    }
                }
                
                zObjList.add(zObj);
            }
        }

        if(zObjList.size() == 0) return;

        try {
            zupdate(zApiInstance, zObjList);
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e);
        }
    }

    private static void updateAvvisi(Merge_Request__c request) {
        final List<Avviso_di_Rinnovo__c> avvisi = [
                                                    SELECT  Id, Contatto__c, Contact_Sold_To__c, Dipendente_ED__c, Titolo__c,
                                                            Titolo_Cod__c, Professione__c, Sesso__c, Codice_Committente__c,
                                                            Cognome_Committente__c, Nome_Committente__c, Cellulare_Committente__c,
                                                            Email_Committente__c, DugAbbreviataStd__c, Indirizzo_Committente__c, 
                                                            Civico_Committente__c, Supplemento_Civico_Committente__c, CAP_Committente__c,
                                                            Supplemento_CAP_Committente__c, Localita_Committente__c, Provincia_Committente__c,
                                                            Nazione_Committente__c, Codice_Destinatario__c, Nome_Destinatario__c, Presso__c,
                                                            Cognome_Destinatario__c, Codice_Nazione_Destinatario__c, Codice_Nazione_Committente__c,
                                                            Italia_Estero__c, Italia_Estero_Fatturazione__c, Nazione_Destinatario__c,
                                                            Abbonamento__c, Regola_Avvisi_e_Solleciti__r.Invia_avviso_al_destinatario__c,
                                                            Abbonamento__r.Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Codice_Nazione__c,
                                                            Abbonamento__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Codice_Nazione__c,
                                                            Abbonamento__r.Name, Abbonamento__r.Regalo__c, Tipo__c
                                                    FROM    Avviso_di_Rinnovo__c
                                                    WHERE   (Contatto__c = :request.Slave_Contact__c OR Contact_Sold_To__c = :request.Slave_Contact__c)
                                                            AND Stato_Avviso__c IN ('Nuovo', 'Bozza')
                                                ];

        if(avvisi.size() == 0) return;

        final Map<String, String> fixAvvisiPressDi = loadFixAvvisiPressDi();

        for(Avviso_di_Rinnovo__c ar : avvisi) {
            final Boolean isRinnovo = ar.Tipo__c == 'Rinnovo';
            final Boolean isPressDi = !(isRinnovo && ar.Regola_Avvisi_e_Solleciti__r.Invia_avviso_al_destinatario__c) && 
                                        (fixAvvisiPressDi != null && fixAvvisiPressDi.get(ar.Abbonamento__r.Name) != null);

            if(ar.Contatto__c == request.Slave_Contact__c) {
                ar.Dipendente_ED__c = request.Master_Contact__r.Dipendente_Editoriale_Domus__c;
                ar.Titolo__c = request.Master_Contact__r.Titolo__c;
                ar.Titolo_Cod__c = request.Master_Contact__r.Titolo_Cod__c;
                ar.Professione__c = request.Master_Contact__r.Professione__c;
                ar.Sesso__c = (request.Master_Contact__r.Sesso__c == 'M') ? 1 : 2;
                ar.Cognome_Committente__c = request.Master_Contact__r.LastName;
                ar.Nome_Committente__c = request.Master_Contact__r.FirstName;
                ar.Cellulare_Committente__c = request.Master_Contact__r.MobilePhone;
                ar.Email_Committente__c = request.Master_Contact__r.Email;
                ar.DugAbbreviataStd__c = request.Master_Contact__r.DugAbbreviataStd__c;
                ar.Indirizzo_Committente__c = request.Master_Contact__r.Via__c;
                ar.Civico_Committente__c = request.Master_Contact__r.Civico__c;
                ar.Supplemento_Civico_Committente__c = request.Master_Contact__r.Supplemento_Civico__c;
                ar.CAP_Committente__c = String.valueOf(request.Master_Contact__r.MailingPostalCode);
                ar.Supplemento_CAP_Committente__c = request.Master_Contact__r.Supplemento_CAP__c;
                ar.Localita_Committente__c = request.Master_Contact__r.MailingCity;
                ar.Provincia_Committente__c = request.Master_Contact__r.MailingState;
                ar.Nazione_Committente__c = request.Master_Contact__r.Nazione__c;
                ar.Codice_Nazione_Committente__c = request.Master_Contact__r.Codice_Nazione__c;
                ar.Presso__c = request.Master_Contact__r.Presso__c;

                if(isPressDi) {
                    if(ar.Codice_Committente__c == request.Slave_Contact__r.CodiceClienteSelected__c) {
                        ar.Codice_Committente__c = request.Master_Contact__r.CodiceClienteSelected__c;
                    }

                    ar.Italia_Estero_Fatturazione__c = request.Master_Contact__r.Codice_Nazione__c == 'IT' ? 'Italia' : 'Estero';
                } else {
                    ar.Italia_Estero_Fatturazione__c = ar.Abbonamento__r.Zuora__CustomerAccount__r.Bill_To_Salesforce__r.Codice_Nazione__c == 'IT' ? 'Italia' : 'Estero';
                }
            }

            if(ar.Contact_Sold_To__c == request.Slave_Contact__c) {
                ar.Codice_Destinatario__c = request.Master_Contact__r.CodiceClienteSelected__c;
                ar.Nome_Destinatario__c = request.Master_Contact__r.FirstName;
                ar.Cognome_Destinatario__c = request.Master_Contact__r.LastName;
                ar.Codice_Nazione_Destinatario__c = request.Master_Contact__r.Codice_Nazione__c;
                ar.Nazione_Destinatario__c = request.Master_Contact__r.MailingCountry;
                ar.Italia_Estero__c = ar.Abbonamento__r.Zuora__CustomerAccount__r.Sold_To_Salesforce__r.Codice_Nazione__c == 'IT' ? 'Italia' : 'Estero';
            }

            if (isPressDi && ar.Cognome_Committente__c != ar.Cognome_Destinatario__c) {
                ar.Regalo__c = 'SI';
            } else {
                ar.Regalo__c = String.isNotBlank(ar.Abbonamento__r.Regalo__c) ? ar.Abbonamento__r.Regalo__c.substring(0,2).toUpperCase() : null;
            }
        }

        AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = true;
        final List<Database.SaveResult> results = Database.update(avvisi, true);
        AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = false;

        final String messageError = generateErrorMessage(results, 'Avviso_di_Rinnovo__c', avvisi);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    private static Map<String, String> loadFixAvvisiPressDi() {
        final List<Fix_Avvisi_PressDi__c> listSubPressDi = [SELECT Id, Id_Contact__c, Name_Subscription__c, Id_Subscription__c, Codice_Cliente__c, Data_Aggiornamento__c FROM Fix_Avvisi_PressDi__c WHERE Data_Aggiornamento__c=null AND Note__c=null];
        final Map<String, String> mappaSub = new Map<String, String>();
        
        for(Fix_Avvisi_PressDi__c fa: listSubPressDi) {
            mappaSub.put(fa.Name_Subscription__c, fa.Id_Contact__c);
        }

        return mappaSub;
    }

    private static void updateEntitlements(Merge_Request__c request) {
        final List<Entitlement__c> entitlements = [
                                                    SELECT Id, customerId__c, codice_fascettario__c, zip__c, city__c, mobile__c, 
                                                                address__c, nation__c, telephone__c, surname__c, name__c
                                                    FROM Entitlement__c
                                                    WHERE Contact__c = :request.Slave_Contact__c AND Stato__c = 'Da inviare'
                                                ];

        if(entitlements.size() == 0) return;

        for(Entitlement__c e : entitlements) {
            e.customerId__c = request.Master_Contact__r.Club_Motori_Id__c;
            e.codice_fascettario__c = request.Master_Contact__r.Codiceclienteselected__c;
            e.zip__c = String.valueOf(request.Master_Contact__r.MailingPostalCode);
            e.city__c = request.Master_Contact__r.MailingCity;
            e.address__c = request.Master_Contact__r.MailingStreet;
            e.nation__c = request.Master_Contact__r.Nazione__c;
            e.telephone__c = request.Master_Contact__r.Phone;
            e.surname__c = request.Master_Contact__r.LastName;
            e.name__c = request.Master_Contact__r.FirstName;
            e.mobile__c = request.Master_Contact__r.MobilePhone;
        }

        final List<Database.SaveResult> results = Database.update(entitlements, true);
        final String messageError = generateErrorMessage(results, 'Entitlement__c', entitlements);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    private static void updateMovStocks(Merge_Request__c request) {
        final List<Movimentazione_Stock__c> movstocks = [
                                                            SELECT Id, Codice_Cliente_SAP__c
                                                            FROM Movimentazione_Stock__c
                                                            WHERE Contact__c = :request.Slave_Contact__c AND (NOT Stato__c IN ('Trasmesso', 'Recepito da SAP'))
                                                        ];

        if(movstocks.size() == 0) return;

        for(Movimentazione_Stock__c ms : movstocks) {
            ms.Codice_Cliente_SAP__c = request.Master_Contact__r.CodiceCliente__c;
        }

        MovimentazioneStockTriggerHandler.skip = true;
        final List<Database.SaveResult> results = Database.update(movstocks, true);
        MovimentazioneStockTriggerHandler.skip = false;

        final String messageError = generateErrorMessage(results, 'Movimentazione_Stock__c', movstocks);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    private static void updateAssets(Merge_Request__c request) {
        final List<Asset> copie = [ 
                                    SELECT Id, ContactId, Dettagli_Cliente__c, Nazione__c, Indirizzo__c, Citt__c, 
                                            Informazioni_Indirizzo__c, Codice_Postale__c, Provincia__c, Dettaglio_Zona__c,
                                            Zona__c, Presso__c, Frazione__c, CodiceRegione__c, SiglaRegione__c, SiglaRegioneAbbr__c,
                                            ZonadItalia__c, Suddivisione_geografica_2__c, CodiceProvincia__c
                                    FROM Asset 
                                    WHERE ContactId = :request.Slave_Contact__c AND Inviata__c = False
                                ];

        if(copie.size() == 0) return;

        final Map<String,CapDetails__c> capDetails = DomusUtil.getCapDetails();

        for(Asset a : copie) {
            a.Dettagli_Cliente__c = request.Master_Contact__r.Name;
            a.Nazione__c = request.Master_Contact__r.MailingCountry;
            a.Indirizzo__c = buildAddress(request.Master_Contact__r);
            a.Citt__c = request.Master_Contact__r.MailingCity;
            a.Informazioni_Indirizzo__c = request.Master_Contact__r.MailingPostalCode + ' ' +  request.Master_Contact__r.MailingCity;
            a.Codice_Postale__c = request.Master_Contact__r.MailingPostalCode;
            a.Provincia__c = request.Master_Contact__r.MailingState;
            a.Dettaglio_Zona__c = String.isNotBlank(request.Master_Contact__r.Id_Area_Nazione__c) ? Integer.valueOf(request.Master_Contact__r.Id_Area_Nazione__c) : null;
            a.Zona__c = request.Master_Contact__r.Area_Nazione__c;
            a.Presso__c = request.Master_Contact__r.Presso__c;
            a.Frazione__c = request.Master_Contact__r.Frazione__c;

            if(request.Master_Contact__r.Codice_Nazione__c == 'IT' || request.Master_Contact__r.Codice_Nazione__c == 'VA' || request.Master_Contact__r.Codice_Nazione__c == 'SM'){
                for(CapDetails__c capDetail : capDetails.values()){
                    Boolean found = false;

                    if(capDetail.Name == request.Master_Contact__r.MailingPostalCode) found = true;

                    if(!found && capDetail.Name.contains('-')){
                        String[] range = capDetail.Name.split('-');
                        if(Integer.valueOf(request.Master_Contact__r.MailingPostalCode) >= Integer.valueOf(range[0]) && Integer.valueOf(request.Master_Contact__r.MailingPostalCode) <= Integer.valueOf(range[1])) found = true;
                    }

                    if(found){
                        a.CodiceRegione__c = capDetail.CodiceRegione__c;
                        a.SiglaRegione__c = capDetail.SiglaRegione__c;
                        a.SiglaRegioneAbbr__c = capDetail.SiglaRegioneAbbr__c;
                        a.ZonadItalia__c = capDetail.ZonadItalia__c;
                        a.Suddivisione_geografica_2__c = capDetail.Suddivisione_geografica_2__c;
                        a.CodiceProvincia__c = (capDetail.CodiceRegione__c == 'SAR') ? request.Master_Contact__r.MailingState : capDetail.CodiceProvincia__c;
                        break;
                    }
                }
            }
        }

        final List<Database.SaveResult> results = Database.update(copie, true);
        final String messageError = generateErrorMessage(results, 'Asset', copie);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    private static String buildAddress(Contact master) {
        String indirizzo = '';

        if(master.DugAbbreviataStd__c != null || master.Dug__c != null)  {
            indirizzo += (master.DugAbbreviataStd__c != null ? master.DugAbbreviataStd__c : master.Dug__c) + ' ';
        }

        indirizzo += master.Via__c + ' ';
        indirizzo += (master.Civico__c != null ? master.Civico__c : '') + ' ';
        indirizzo += (master.Supplemento_Civico__c != null ? master.Supplemento_Civico__c : '');

        return indirizzo.trim();
    }

    private static void moveContacts(Merge_Request__c request) {
        final List<Contact> contacts = [ SELECT Id, AccountId FROM Contact WHERE AccountId = :request.Slave_Account__c ];

        for(Contact contact : contacts) {
            contact.AccountId = request.Master_Account__c;
            contact.MainContact__c = false;
        }

        if(contacts.size() == 0) return;

        final Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.OptAllOrNone = true;

        ContactTriggerHandler.skip = true;
        final List<Database.SaveResult> results = Database.update(contacts, dml);
        ContactTriggerHandler.skip = false;

        final String messageError = generateErrorMessage(results, 'Contact', contacts);

        if(String.isNotBlank(messageError)) {
            request.Status__c = 'Errore';
            request.Error_Message__c = messageError;
        }
    }

    private static Set<String> fetchCustomerContactZIDs(Id contactId) {
        final Set<String> zuoraIds = new Set<String>();
        final List<Zuora__CustomerAccount__c> customerAccounts = [
                                                                    SELECT Id, Zuora__BillToId__c, Bill_To_Salesforce__c, Zuora__SoldToId__c, Sold_To_Salesforce__c
                                                                    FROM Zuora__CustomerAccount__c
                                                                    WHERE Bill_To_Salesforce__c = :contactId OR Sold_To_Salesforce__c = :contactId
                                                                ];

        for(Zuora__CustomerAccount__c ca : customerAccounts) {
            if(ca.Bill_To_Salesforce__c == contactId) {
                zuoraIds.add(ca.Zuora__BillToId__c);
            }

            if(ca.Sold_To_Salesforce__c == contactId) {
                zuoraIds.add(ca.Zuora__SoldToId__c);
            }
        }

        return zuoraIds;
    }

    private static String generateErrorMessage(List<Zuora.zApi.SaveResult> results, String sObjName, Map<String, SObject> zObjMap) {
        Boolean errorFound = false;

        final JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        jsonGen.writeFieldName('ErrorMessage');
        jsonGen.writeStartArray();

        for(Integer i = 0; i < results.size(); i++) {
            final Zuora.zApi.SaveResult result = results[i];
            final SObject sObj = (result.Id != null) ? zObjMap.get(result.Id) : null;
            final String sObjId = ((sObj != null) ? sObj.Id : null) + '';

            if(result.Success) continue;
            errorFound = true;

            jsonGen.writeStartObject();
            jsonGen.writeStringField('SObject', sObjName);
            jsonGen.writeStringField('Id', sObjId);

            for(Zuora.zObject error : result.Errors) {
                jsonGen.writeStringField('Field', error.getValue('Field') + '');
                jsonGen.writeStringField('Message', error.getValue('Message') + '');
                jsonGen.writeStringField('Code', error.getValue('Code') + '');
            }

            jsonGen.writeEndObject();
        }

        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
        jsonGen.close();

        if(errorFound) {
            return jsonGen.getAsString();
        }

        return null;
    }

    private static String generateErrorMessage(List<Database.SaveResult> results, String sObjName, List<SObject> sObjects) {
        Boolean errorFound = false;

        final JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        jsonGen.writeFieldName('ErrorMessage');
        jsonGen.writeStartArray();

        for(Integer i = 0; i < results.size(); i++) {
            final Database.SaveResult result = results[i];
            final SObject sObj = sObjects[i];
            final String sObjId = ((sObj != null) ? sObj.Id : null) + '';

            if(result.isSuccess()) continue;
            errorFound = true;

            jsonGen.writeStartObject();
            jsonGen.writeStringField('SObject', sObjName);
            jsonGen.writeStringField('Id', sObjId);

            for(Database.Error error : result.getErrors()) {
                jsonGen.writeFieldName('Fields');
                jsonGen.writeStartArray();
                for(String field : error.getFields()) {
                    jsonGen.writeString(field);
                }
                jsonGen.writeEndArray();
                jsonGen.writeStringField('Message', error.getMessage());
                jsonGen.writeStringField('StatusCode', error.getStatusCode() + '');
            }

            jsonGen.writeEndObject();
        }

        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
        jsonGen.close();

        if(errorFound) {
            return jsonGen.getAsString();
        }

        return null;
    }

    private static String generateErrorMessage(Exception e) {
        Boolean errorFound = false;

        final JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        jsonGen.writeNumberField('Line', e.getLineNumber());
        jsonGen.writeStringField('Type', e.getTypeName());
        jsonGen.writeStringField('Message', e.getMessage());
        jsonGen.writeEndObject();
        jsonGen.close();

        return jsonGen.getAsString();
    }

    private static String checkContactFirstName(Contact c){
        String firstName = c.FirstName != null ? c.FirstName : AppConstants.ZUORA_PLACEHOLDER;
        return firstName;
    }

    private static List<Zuora.zApi.SaveResult> zupdate(Zuora.zApi zApiInstance, List<Zuora.zObject> zObjList) {
        final List<Zuora.zApi.SaveResult> results = new List<Zuora.zApi.SaveResult>();
        final List<List<Zuora.zObject>> zbatches = zsplit(zObjList, 50);

        if(Test.isRunningTest()) {
            return new List<Zuora.zApi.SaveResult>();
        }

        for(List<Zuora.zObject> zbatch : zbatches) {
            Integer attempt = 0;

            while(true){
                ++attempt;

                try {
                    results.addAll(zApiInstance.zupdate(zbatch));
                    break;  // The call has been successful, just go on
                } catch(Exception e) {
                    if(!e.getMessage().startsWith('The callout was unsuccessful after 4 attempts')) {
                        throw e;    // Is not a Request Timeout Exception, notify it
                    } else if(attempt >= ZUORA_MAX_ATTEMPTS) {
                        throw e;    // The maximum number of attempts has been exceeded
                    }
                }
            }
        }

        return results;
    }

    private static List<List<Zuora.zObject>> zsplit(List<Zuora.zObject> zObjList, Integer maxSize) {
        if(!Test.isRunningTest() && zObjList.size() <= maxSize) return new List<List<Zuora.zObject>>{ zObjList };

        final List<List<Zuora.zObject>> batches = new List<List<Zuora.zObject>>();
        List<Zuora.zObject> currentList = null;

        for(Zuora.zObject zObj : zObjList) {
            if(currentList == null) { currentList = new List<Zuora.zObject>(); }

            currentList.add(zObj);

            if(currentList.size() >= maxSize) {
                batches.add(currentList);
                currentList = null;
            }
        }

        if(currentList != null) { batches.add(currentList); }

        return batches;
    }
}