@isTest
private class EditContactExtensionTest{
	
	@isTest
	static void testM(){

		ContactTriggerHandler.skip = true;
		Contact c = new Contact(
			FirstName = 'FirstName',
			LastName = 'LastName',
			Codice_Fiscale__c = '11111111111',
			Codice_Nazione__c = 'IT'
		);
		insert c;

		ApexPages.CurrentPage().getParameters().put('newAccountB2C','1');
		EditContactExtension ext = new EditContactExtension(new ApexPages.StandardController(c));
		//ext.goNext();
		ext.changeIgnoreDuplicate();

		/* DH-685 */
		/*
		ApexPages.CurrentPage().getParameters().put('newAccountB2C','1');
		ext = new EditContactExtension(new ApexPages.StandardController(c));
		ext.isAccountB2C = false;
		ext.customSaveContact();
		*/
		/* DH-685 */
	}

	@isTest
	static void testIbanValido() {

		Test.startTest();

		Contact c = [SELECT Id, AccountId, Codice_IBAN__c FROM Contact WHERE FirstName = 'Test' LIMIT 1];
		Test.setCurrentPage(Page.EditContact);
		ApexPages.currentPage().getParameters().put('id', c.Id);
		EditContactExtension ctrl = new EditContactExtension(new ApexPages.StandardController(c));

		Test.setMock(HttpCalloutMock.class, new MockTestClass('CheckIban'));

		c.Codice_IBAN__c = 'IT02D0326802801052879623060';

		ctrl.checkContactIBAN();
		ctrl.cleanContactIBAN();

		c.Codice_IBAN__c = '';
		ctrl.checkContactIBAN();

		Test.stopTest();

	}

	@isTest
	static void testIbanErrore() {

		Test.startTest();

		Contact c = [SELECT Id, AccountId, Codice_IBAN__c FROM Contact WHERE FirstName = 'Test' LIMIT 1];
		Test.setCurrentPage(Page.EditContact);
		ApexPages.currentPage().getParameters().put('id', c.Id);
		EditContactExtension ctrl = new EditContactExtension(new ApexPages.StandardController(c));

		Test.setMock(HttpCalloutMock.class, new MockTestClass('ErrorIban'));

		c.Codice_IBAN__c = 'Errore';

		ctrl.checkContactIBAN();

		Test.stopTest();

	}

	@testSetup
	public static void testSetup() {
		
		Account a = new Account(Name = 'Test Account');
		insert a;
	
		Contact contatto = new Contact(
			AccountId = a.Id,
			MainContact__c = true,
			FirstName = 'Test',
			LastName = 'Contact',
			MailingPostalCode = '20092'
		);
		insert contatto;

		Setting__c iban = new Setting__c (
			Name = 'ValidateIban',
			URL__c = 'https://api.iban.com/clients/api/ibanv2.php?api_key=e1e6e2210b6417841fbe823199e393b9&format=json&iban='
		);
		insert iban;

	}
}