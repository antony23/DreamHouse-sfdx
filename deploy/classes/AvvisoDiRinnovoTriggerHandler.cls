public class AvvisoDiRinnovoTriggerHandler{

    public static Boolean SkipAvvisoTrigger = false;

    /*
        Calcolo del tipo avviso sull'avviso di rinnovo in base alla tabella di configurazione
    */
    public static void setTipoAvviso(Avviso_di_Rinnovo__c[] avvisi){

        /*
        set<Id> subscriptionId = new set<Id>();
        for(Avviso_di_Rinnovo__c ar : avvisi){
            subscriptionId.add(ar.Abbonamento__c);
        }

        Map<String,Decimal> avvisiCount = new Map<String,Decimal>(); //Numero di avvisi già inviati per ogni subscription
        for(AggregateResult ar : [Select count(Id) c,Abbonamento__c abb,Regola_Avvisi_e_Solleciti__r.RecordType.DeveloperName nr From Avviso_di_Rinnovo__c Where Abbonamento__c in : subscriptionId And Id not in : avvisi And Stato_Avviso__c = 'Confermato' Group By Abbonamento__c,Regola_Avvisi_e_Solleciti__r.RecordType.DeveloperName]){
            avvisiCount.put( ((String)ar.get('abb'))+'-'+((String)ar.get('nr')),(Decimal)ar.get('c'));
        }
        */

        List<Configurazione_Tipo_Avviso_Rinnovo__c> configurazioni = [Select Tipo_Avviso__c,Intermediario__c,Italia_Estero__c,Metodo_ultimo_pagamento__c,Nuovo_Rinnovo__c,Regalo__c,Tipo_Prodotto__c,Business_Unit__c,Tipo_Cliente__c,Testata__c
                                                                From Configurazione_Tipo_Avviso_Rinnovo__c ];

        List<Avviso_di_Rinnovo__c> listaRinnovi = new List<Avviso_di_Rinnovo__c>();
        List<Avviso_di_Rinnovo__c> listaSolleciti = new List<Avviso_di_Rinnovo__c>();

        for(Avviso_di_Rinnovo__c ar : avvisi) {
            if (ar.Tipo__c == AppConstants.AVVISO_RINNOVO) {
                listaRinnovi.add(ar);
            } else if (ar.Tipo__c == AppConstants.AVVISO_SOLLECITO) {
                listaSolleciti.add(ar);
            }
        }

        setTipoAvvisoRinnovo(configurazioni, listaRinnovi);
        setTipoAvvisoSollecito(configurazioni, listaSolleciti);

        /*                                                       
        for(Avviso_di_Rinnovo__c ar : avvisi){
            if(String.isBlank(ar.Tipo_Avviso__c)){ //Viene ricalcolato solo se è vuoto
                String bUnit = ar.Business_Unit__c != null && ar.Business_Unit__c.split(';').size() == 1 ? ar.Business_Unit__c : null;
                for(Configurazione_Tipo_Avviso_Rinnovo__c conf : configurazioni){
                    if(
                        (conf.Tipo_Prodotto__c == null || conf.Tipo_Prodotto__c == ar.Tipo_subscription__c) &&
                        (conf.Intermediario__c == null || conf.Intermediario__c == ar.Intermediario__c) &&
                        (conf.Metodo_ultimo_pagamento__c == null || conf.Metodo_ultimo_pagamento__c == ar.Metodo_ultimo_pagamento__c) &&
                        (conf.Italia_Estero__c == null || conf.Italia_Estero__c == ar.Italia_Estero_Fatturazione__c) &&
                        (conf.Nuovo_Rinnovo__c == null || conf.Nuovo_Rinnovo__c == ar.Nuovo_Rinnovo__c) &&
                        (conf.Regalo__c == null || conf.Regalo__c == ar.Regalo__c) &&
                        (conf.Business_Unit__c == null || conf.Business_Unit__c == bUnit) &&
                        (matchMultiselectPicklist(ar.Tipo_cliente__c,conf.Tipo_Cliente__c)) &&
                        (matchMultiselectPicklist(ar.Testate__c,conf.Testata__c))
                    ){
                        ar.Tipo_Avviso__c = conf.Tipo_Avviso__c;
                        Decimal count = avvisiCount.get(ar.Abbonamento__c+'-'+ar.Record_Type_Regola__c);
                        count = count != null ? count+1 : 1;
                        ar.Tipo_Avviso__c += count;
                    }
                }
            }
        }
        */
    }


    /**
    *  Imposta il Tipo Avviso per i rinnovi
    */
    private static void setTipoAvvisoRinnovo(List<Configurazione_Tipo_Avviso_Rinnovo__c> configurazioni, List<Avviso_di_Rinnovo__c> avvisi) {

        set<Id> subscriptionId = new set<Id>();
        for(Avviso_di_Rinnovo__c ar : avvisi){
            subscriptionId.add(ar.Abbonamento__c);
        }

        Map<String,Decimal> avvisiCount = new Map<String,Decimal>(); //Numero di avvisi già inviati per ogni subscription

        for(AggregateResult ar : [Select count(Id) c,Abbonamento__c abb,Regola_Avvisi_e_Solleciti__r.RecordType.DeveloperName nr From Avviso_di_Rinnovo__c Where Abbonamento__c in : subscriptionId And Id not in : avvisi And Stato_Avviso__c IN ('Confermato','Cancellato') Group By Abbonamento__c,Regola_Avvisi_e_Solleciti__r.RecordType.DeveloperName]){  // [DH-987]
            avvisiCount.put( ((String)ar.get('abb'))+'-'+((String)ar.get('nr')),(Decimal)ar.get('c'));
        }
                                                        
        for(Avviso_di_Rinnovo__c ar : avvisi){
            if(String.isBlank(ar.Tipo_Avviso__c)){ //Viene ricalcolato solo se è vuoto

                String prefix = getCongfigurazione(configurazioni, ar);
                if (prefix != null) {
                    ar.Tipo_Avviso__c = prefix;
                    Decimal count = avvisiCount.get(ar.Abbonamento__c+'-'+ar.Record_Type_Regola__c);
                    count = count != null ? count+1 : 1;
                    ar.Tipo_Avviso__c += count;
                }

                /*
                String bUnit = ar.Business_Unit__c != null && ar.Business_Unit__c.split(';').size() == 1 ? ar.Business_Unit__c : null;
                for(Configurazione_Tipo_Avviso_Rinnovo__c conf : configurazioni){
                    if(
                        (conf.Tipo_Prodotto__c == null || conf.Tipo_Prodotto__c == ar.Tipo_subscription__c) &&
                        (conf.Intermediario__c == null || conf.Intermediario__c == ar.Intermediario__c) &&
                        (conf.Metodo_ultimo_pagamento__c == null || conf.Metodo_ultimo_pagamento__c == ar.Metodo_ultimo_pagamento__c) &&
                        (conf.Italia_Estero__c == null || conf.Italia_Estero__c == ar.Italia_Estero_Fatturazione__c) &&
                        (conf.Nuovo_Rinnovo__c == null || conf.Nuovo_Rinnovo__c == ar.Nuovo_Rinnovo__c) &&
                        (conf.Regalo__c == null || conf.Regalo__c == ar.Regalo__c) &&
                        (conf.Business_Unit__c == null || conf.Business_Unit__c == bUnit) &&
                        (matchMultiselectPicklist(ar.Tipo_cliente__c,conf.Tipo_Cliente__c)) &&
                        (matchMultiselectPicklist(ar.Testate__c,conf.Testata__c))
                    ){
                        ar.Tipo_Avviso__c = conf.Tipo_Avviso__c;
                        Decimal count = avvisiCount.get(ar.Abbonamento__c+'-'+ar.Record_Type_Regola__c);
                        count = count != null ? count+1 : 1;
                        ar.Tipo_Avviso__c += count;
                    }
                }
                */

            }
        }
    }

    /**
    *  Imposta il Tipo Avviso per i solleciti
    */
    private static void setTipoAvvisoSollecito(List<Configurazione_Tipo_Avviso_Rinnovo__c> configurazioni, List<Avviso_di_Rinnovo__c> avvisi) {

        Integer mesiDallaSottoscrizione = 0;

        for(Avviso_di_Rinnovo__c ar:avvisi){
            if(String.isBlank(ar.Tipo_Avviso__c)){ //Viene ricalcolato solo se è vuoto

                if(ar.Mesi_dalla_sottoscrizione__c == null || ar.Mesi_dalla_sottoscrizione__c == '') {
                    mesiDallaSottoscrizione = 0;
                } else {
                     mesiDallaSottoscrizione = Integer.valueOf(ar.Mesi_dalla_sottoscrizione__c);
                     if(mesiDallaSottoscrizione < 0) {
                        mesiDallaSottoscrizione = 0;
                    } 
                }

                String prefix = getCongfigurazione(configurazioni, ar);
                if (prefix != null) {
                    mesiDallaSottoscrizione++;
                    ar.Tipo_Avviso__c = prefix + '' + mesiDallaSottoscrizione;
                }
            }
        }
    }

    private static String getCongfigurazione(List<Configurazione_Tipo_Avviso_Rinnovo__c> configurazioni, Avviso_di_Rinnovo__c ar) {

        String bUnit = ar.Business_Unit__c != null && ar.Business_Unit__c.split(';').size() == 1 ? ar.Business_Unit__c : null;
        for(Configurazione_Tipo_Avviso_Rinnovo__c conf : configurazioni) {

            if(
                (conf.Tipo_Prodotto__c == null || conf.Tipo_Prodotto__c == ar.Tipo_subscription__c) &&
                (conf.Intermediario__c == null || conf.Intermediario__c == ar.Intermediario__c) &&
                (conf.Metodo_ultimo_pagamento__c == null || conf.Metodo_ultimo_pagamento__c == ar.Metodo_ultimo_pagamento__c) &&
                (conf.Italia_Estero__c == null || conf.Italia_Estero__c == ar.Italia_Estero_Fatturazione__c) &&
                (conf.Nuovo_Rinnovo__c == null || conf.Nuovo_Rinnovo__c == ar.Nuovo_Rinnovo__c) &&
                (conf.Regalo__c == null || conf.Regalo__c == ar.Regalo__c) &&
                (conf.Business_Unit__c == null || conf.Business_Unit__c == bUnit) &&
                (matchMultiselectPicklist(ar.Tipo_cliente__c,conf.Tipo_Cliente__c)) &&
                (matchMultiselectPicklist(ar.Testate__c,conf.Testata__c))
            ) {
                return conf.Tipo_Avviso__c;
            }
        }

        return null;
    }


    private static Boolean matchMultiselectPicklist(String picklistAvviso, String picklistConfiguratore){
        List<String> picklistAvvisoElements = picklistAvviso != null ? picklistAvviso.split(';') : new List<String>();
        List<String> picklistConfiguratoreElements = picklistConfiguratore != null ? picklistConfiguratore.split(';') : new List<String>();
        if(picklistConfiguratoreElements.isEmpty()) return true;
        for(String picklistAvvisoElement : picklistAvvisoElements){
            if(picklistAvvisoElement.equalsIgnoreCase('ALTRO')) continue;   // DH-1116
            Boolean found = false;
            for(String picklistConfiguratoreElement : picklistConfiguratoreElements){
                found = found || picklistAvvisoElement == picklistConfiguratoreElement;
            }
            if(!found) return false;
        }
        return true;
    }

    /*
        Calcolo dei campi Numero Attribuzione (per bollettino) e Cbill per la riconciliazione dei pagamenti
    */
    public static void setNumeroAttribuzioneCbll(Avviso_di_Rinnovo__c[] avvisi){
        Avviso_di_Rinnovo__c[] avvisiToUpdate = new Avviso_di_Rinnovo__c[]{}; //Devo forzare un update perchè mi serve il campo name che non è popolato nel before insert

        for(Avviso_di_Rinnovo__c ar : avvisi){
            Avviso_di_Rinnovo__c avviso = ar;

            if(trigger.isAfter && trigger.isInsert){
                avviso = new Avviso_di_Rinnovo__c(
                    Id = ar.Id
                );
            }

            Boolean changed = false;
            /*if(ar.Metodo_ultimo_pagamento__c == 'Cbill' && String.isBlank(ar.Cbill__c) && ar.Totale__c != null){
                avviso.Numero_Attribuzione__c = '';
                avviso.Cbill__c = getCBill(ar.Name,ar.Totale__c);
                changed = true;
            }else if(ar.Metodo_ultimo_pagamento__c == 'Bollettino Postale' && String.isBlank(ar.Numero_Attribuzione__c)){
                avviso.Numero_Attribuzione__c = getNumeroAttribuzione(ar.Name);
                avviso.Cbill__c = '';
                changed = true;
            }*/
            if(String.isBlank(ar.Cbill__c) && ar.Totale__c != null){
                avviso.Cbill__c = getCBill(ar.Name,ar.Totale__c);
                changed = true;
            }
            if(String.isBlank(ar.Numero_Attribuzione__c)){
                avviso.Numero_Attribuzione__c = getNumeroAttribuzione(ar.Name);
                changed = true;
            }


            if(trigger.isAfter && trigger.isInsert){
                avvisiToUpdate.add(avviso);
            }

        }

        if(avvisiToUpdate.size() > 0){
            Boolean oldSkipValue = AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger;

            AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = true;
            update avvisiToUpdate;
            AvvisoDiRinnovoTriggerHandler.SkipAvvisoTrigger = oldSkipValue;
        }


    }


    public final static Long divisore = 93;
    public final static Long codiceBillerDomus = 33416;

    public static String getNumeroAttribuzione(String codice){
        String atext = codice.split('-')[1];

        Long a = Long.valueof(atext);
        

        Long b = Math.mod(a,divisore);
        
        String btext = (b+'').leftPad(2).replace(' ','0');
        atext = atext.leftPad(16).replace(' ','0');

        return atext+btext;
    }

    public static String getCBill(String codice,Decimal totale){
        String atext = codice.split('-')[1];
        Long a = Long.valueof(atext);

        Long totaleLong = Long.valueOf(String.valueOf(totale).replace('.',''));
        
        Long b = a+totaleLong+codiceBillerDomus;

        Long c = Math.mod(b,divisore);

        String ctext = (c+'').leftPad(2).replace(' ','0');
        atext = atext.leftPad(16).replace(' ','0');

        return atext+ctext;
    }
}