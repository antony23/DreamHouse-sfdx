public class EmailSendingUtil {

	private EmailSendingUtil(){}

	// Recupera i template delle email in base al nome
	public static Map<String, EmailTemplate> loadTemplates(String[] templateNames) {
		final Map<String, EmailTemplate> templates = new Map<String, EmailTemplate>();

		for(String tName : templateNames) {
			templates.put(tName, [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE DeveloperName = :tName LIMIT 1]);
		}

		System.debug('Templates: ' + templates);

		return templates;
	}

	public static OrgWideEmailAddress getOrgWideEmailAddressByDisplayName(String displayName) {
		return [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = :displayName LIMIT 1];
	}

	public static Messaging.SingleEmailMessage prepareEmail(String to, String subject, String body) {
		return prepareEmail(
								null, 
								(to == null) ? '' : to,
								(String) null, (String) null, subject, body
							);
	}

	public static Messaging.SingleEmailMessage prepareEmail(OrgWideEmailAddress sender, String to, String subject, String body) {
		return prepareEmail(
								sender, 
								(to == null) ? '' : to,
								(String) null, (String) null, subject, body
							);
	}

	public static Messaging.SingleEmailMessage prepareEmail(OrgWideEmailAddress sender, String to, String cc, 
															String bcc, String subject, String body) {
		return prepareEmail(
								sender, 
								(to == null) ? new String[]{} : new String[]{to},
								(cc == null) ? new String[]{} : new String[]{cc},
								(bcc == null) ? new String[]{} : new String[]{bcc},
								subject, body
							);
	}

	// Crea una email di tipo SingleEmailMessage
	public static Messaging.SingleEmailMessage prepareEmail(OrgWideEmailAddress sender, String[] to, String[] cc, 
															String[] bcc, String subject, String body) {
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

		if(sender != null) {
			message.setOrgWideEmailAddressId(sender.Id);
		}

		if(to != null && !to.isEmpty()) {
			message.setToAddresses(to);
		}

		if(cc != null && !cc.isEmpty()) {
			message.setCcAddresses(cc);
		}

		if(bcc != null && !bcc.isEmpty()) {
			message.setBccAddresses(bcc);
		}

		message.setSubject(subject == null ? '' : subject);
		message.setPlainTextBody(body == null ? '' : body);

		return message;
	}

	// Crea le email da inviare in batch
	public static List<Messaging.SingleEmailMessage> prepareEmails(List<EmailDataBundle> data, OrgWideEmailAddress fromAddr) {
		return prepareEmails(data, fromAddr, null);
	}

	// Crea le email da inviare in batch
	public static List<Messaging.SingleEmailMessage> prepareEmails(List<EmailDataBundle> data, OrgWideEmailAddress fromAddr, String[] bcc) {
		final List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

		Messaging.SingleEmailMessage message;
		SObject recipient, relatedTo;
		EmailTemplate template;

		for(EmailDataBundle bundle : data) {
			recipient = bundle.getRecipient();
			relatedTo = bundle.getRelatedTo();
			template = bundle.getTemplate();

			message = new Messaging.SingleEmailMessage();
			message.setToAddresses(new String[]{recipient.Id});
			message.setTargetObjectId(recipient.Id);
			message.setWhatId(relatedTo.Id);
			message.setTemplateId(template.Id);

			if(bcc != null && !bcc.isEmpty()) {
				message.setBccAddresses(bcc);
			}
			
			if(fromAddr != null) {
				message.setOrgWideEmailAddressId(fromAddr.Id);
			}

			emails.add(message);
		}

		return emails;
	}

	// Funzione per debuggare gli esiti dell'invio
	public static void debug(List<Messaging.SendEmailResult> results, List<EmailDataBundle> data) {
		Messaging.SendEmailResult result;
		EmailDataBundle bundle;

		for(Integer i = 0; i < results.size(); i++) {
			result = results.get(i);
			bundle = data.get(i);

			//System.debug(LoggingLevel.DEBUG, bundle.toString());	// BUG: System.LimitException: Apex CPU time limit exceeded

			if(result.isSuccess()) {
				System.debug(LoggingLevel.DEBUG, 'EmailDataBundle[' + bundle.hashCode() + '] => Email sending succeeded');
			} else {
				System.debug(LoggingLevel.DEBUG, 'EmailDataBundle[' + bundle.hashCode() + '] => Email sending failed');

				for(Messaging.SendEmailError error : result.getErrors()) {
					System.debug(LoggingLevel.ERROR, 'EmailDataBundle[' + bundle.hashCode() + '] => ' + error.getMessage());
				}
			}
		}
	}
}