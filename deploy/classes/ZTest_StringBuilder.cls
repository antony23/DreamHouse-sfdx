@isTest
public class ZTest_StringBuilder {
	@isTest
	public static void testConstructors() {
		final StringBuilder sb1 = new StringBuilder();
		final StringBuilder sb2 = new StringBuilder('Test');

		System.assertEquals('', sb1.buffer);
		System.assertEquals('Test', sb2.buffer);
		System.assertEquals(4, sb2.length());
	}

	@isTest
	public static void testAddMethods() {
		final StringBuilder sb1 = new StringBuilder();
		final Datetime now = Datetime.now();
		final String[] format = new String[]{ 'yyyy/MM/dd', 'HH:mm:ss', 'yyyy/MM/dd HH:mm:ss' };
		final Id testId = '0012600000ry3Ho';

		sb1.append(testId);
		System.assertEquals(testId + '', sb1.buffer);

		sb1.push(testId);
		System.assertEquals(testId + '' + testId, sb1.buffer);

		sb1.add((testId + '').length(), testId);
		System.assertEquals((testId + '') + testId + testId, sb1.buffer);
		sb1.clear();

		sb1.append('Test');
		System.assertEquals('Test', sb1.buffer);

		sb1.push('A');
		System.assertEquals('ATest', sb1.buffer);

		sb1.add(5, 'B');
		System.assertEquals('ATestB', sb1.buffer);

		sb1.add(3, 'C');
		System.assertEquals('ATeCstB', sb1.buffer);
		sb1.clear();

		sb1.append(true);
		System.assertEquals('true', sb1.buffer);

		sb1.push(true);
		System.assertEquals('truetrue', sb1.buffer);

		sb1.add(4, false);
		System.assertEquals('truefalsetrue', sb1.buffer);
		sb1.clear();

		sb1.append(1L);
		System.assertEquals(1L + '', sb1.buffer);

		sb1.push(1L);
		System.assertEquals(1L + '' + 1L, sb1.buffer);

		sb1.add((1L + '').length(), 2L);
		System.assertEquals(('' + 1L) + 2L + 1L, sb1.buffer);
		sb1.clear();

		sb1.append(1);
		System.assertEquals(1 + '', sb1.buffer);

		sb1.push(1);
		System.assertEquals(1 + '' + 1, sb1.buffer);

		sb1.add(1, 2);
		System.assertEquals(('' + 1) + 2 + 1, sb1.buffer);
		sb1.clear();

		sb1.append((Double) 1.0);
		System.assertEquals(1.0 + '', sb1.buffer);

		sb1.push((Double) 1.0);
		System.assertEquals(1.0 + '' + 1.0, sb1.buffer);

		sb1.add((1.0 + '').length(), (Double) 2.0);
		System.assertEquals(('' + 1.0) + 2.0 + 1.0, sb1.buffer);
		sb1.clear();

		sb1.append((Double) 1.0, 2);
		System.assertEquals(1.0 + '0', sb1.buffer);

		sb1.push((Double) 1.0, 2);
		System.assertEquals(1.0 + '0' + 1.0 + '0', sb1.buffer);

		sb1.add((1.0 + '0').length(), (Double) 2.0, 2);
		System.assertEquals(1.0 + '0' + 2.0 + '0' + 1.0 + '0', sb1.buffer);
		sb1.clear();

		sb1.append((Double) 1.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0', sb1.buffer);

		sb1.push((Double) 1.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0' + 1.6 + '0', sb1.buffer);

		sb1.add((1.6 + '0').length(), (Double) 2.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0' + 2.6 + '0' + 1.6 + '0', sb1.buffer);
		sb1.clear();

		sb1.append((Decimal) 1.0);
		System.assertEquals(1.0 + '', sb1.buffer);

		sb1.push((Decimal) 1.0);
		System.assertEquals(1.0 + '' + 1.0, sb1.buffer);

		sb1.add((1.0 + '').length(), (Decimal) 2.0);
		System.assertEquals(('' + 1.0) + 2.0 + 1.0, sb1.buffer);
		sb1.clear();

		sb1.append((Decimal) 1.0, 2);
		System.assertEquals(1.0 + '0', sb1.buffer);

		sb1.push((Decimal) 1.0, 2);
		System.assertEquals(1.0 + '0' + 1.0 + '0', sb1.buffer);

		sb1.add((1.0 + '0').length(), (Decimal) 2.0, 2);
		System.assertEquals(1.0 + '0' + 2.0 + '0' + 1.0 + '0', sb1.buffer);
		sb1.clear();

		sb1.append((Decimal) 1.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0', sb1.buffer);

		sb1.push((Decimal) 1.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0' + 1.6 + '0', sb1.buffer);

		sb1.add((1.6 + '0').length(), (Decimal) 2.595, 2, System.RoundingMode.UP);
		System.assertEquals(1.6 + '0' + 2.6 + '0' + 1.6 + '0', sb1.buffer);
		sb1.clear();

		sb1.append(now.date());
		System.assertEquals(now.date() + '', sb1.buffer);

		sb1.push(now.date());
		System.assertEquals(now.date() + '' + now.date(), sb1.buffer);

		sb1.add((now.date() + '').length(), now.date());
		System.assertEquals(('' + now.date()) + now.date() + now.date(), sb1.buffer);
		sb1.clear();

		sb1.append(now.date(), format[0]);
		System.assertEquals(now.format(format[0]), sb1.buffer);

		sb1.push(now.date(), format[0]);
		System.assertEquals(now.format(format[0]) + now.format(format[0]), sb1.buffer);

		sb1.add(now.format(format[0]).length(), now.date(), format[0]);
		System.assertEquals(now.format(format[0]) + now.format(format[0]) + now.format(format[0]), sb1.buffer);
		sb1.clear();

		sb1.append(now.time());
		System.assertEquals(now.time() + '', sb1.buffer);

		sb1.push(now.time());
		System.assertEquals(now.time() + '' + now.time(), sb1.buffer);

		sb1.add((now.time() + '').length(), now.time());
		System.assertEquals(('' + now.time()) + now.time() + now.time(), sb1.buffer);
		sb1.clear();

		sb1.append(now.time(), format[1]);
		System.assertEquals(now.format(format[1]), sb1.buffer);

		sb1.push(now.time(), format[1]);
		System.assertEquals(now.format(format[1]) + now.format(format[1]), sb1.buffer);

		sb1.add(now.format(format[1]).length(), now.time(), format[1]);
		System.assertEquals(now.format(format[1]) + now.format(format[1]) + now.format(format[1]), sb1.buffer);
		sb1.clear();

		sb1.append(now);
		System.assertEquals(now + '', sb1.buffer);

		sb1.push(now);
		System.assertEquals(now + '' + now, sb1.buffer);

		sb1.add((now + '').length(), now);
		System.assertEquals((now + '') + now + now, sb1.buffer);
		sb1.clear();

		sb1.append(now, format[2]);
		System.assertEquals(now.format(format[2]), sb1.buffer);

		sb1.push(now, format[2]);
		System.assertEquals(now.format(format[2]) + now.format(format[2]), sb1.buffer);

		sb1.add(now.format(format[2]).length(), now, format[2]);
		System.assertEquals(now.format(format[2]) + now.format(format[2]) + now.format(format[2]), sb1.buffer);
		sb1.clear();

		sb1.append(new CustomObject());
		System.assertEquals('CustomObject', sb1.buffer);

		sb1.push(new CustomObject());
		System.assertEquals('CustomObjectCustomObject', sb1.buffer);

		sb1.add(12, new CustomObject());
		System.assertEquals('CustomObjectCustomObjectCustomObject', sb1.buffer);
		sb1.clear();
	}

	@isTest
	public static void testRemoveMethods() {
		StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals('T', sb1.getCharAt(0));
		System.assertEquals('est String', sb1.removeCharAt(0).buffer);
		System.assertEquals('est Strin', sb1.removeCharAt(9).buffer);
		System.assertEquals('estStrin', sb1.removeCharAt(3).buffer);

		sb1 = new StringBuilder('Test String');
		System.assertEquals('Test ', sb1.remove(5).buffer);
		System.assertEquals(' ', sb1.remove(0, 4).buffer);

		sb1 = new StringBuilder('2 Test String Test 2');
		System.assertEquals('2TestStringTest2', sb1.remove(' ').buffer);
		System.assertEquals('2TestStringTest', sb1.removeEnd('2').buffer);
		System.assertEquals('TestStringTest', sb1.removeStart('2').buffer);
		System.assertEquals('TestString', sb1.removeEndIgnoreCase('test').buffer);
		System.assertEquals('String', sb1.removeStartIgnoreCase('test').buffer);
		System.assertEquals('', sb1.remove(0, sb1.length()).buffer);
	}

	@isTest
	public static void testIndexOfMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals(3, sb1.indexOf('t'));
		System.assertEquals(6, sb1.indexOf('t', 4));
		System.assertEquals(0, sb1.indexOfIgnoreCase('t'));
		System.assertEquals(3, sb1.indexOfIgnoreCase('t', 1));
		System.assertEquals(9, sb1.indexOfAny('zn'));
		System.assertEquals(0, sb1.indexOfAnyBut('zn'));
		System.assertEquals(5, sb1.indexOfDifference('Test '));

		System.assertEquals(6, sb1.lastIndexOf('t'));
		System.assertEquals(3, sb1.lastIndexOf('t', 5));
		System.assertEquals(6, sb1.lastIndexOfIgnoreCase('T'));
		System.assertEquals(3, sb1.lastIndexOfIgnoreCase('T', 5));
	}

	@isTest
	public static void testSubstringMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals('String', sb1.substring(5));
		System.assertEquals('Test', sb1.substring(0, 4));
		System.assertEquals('String', sb1.substringAfter(' '));
		System.assertEquals('ring', sb1.substringAfterLast('t'));
		System.assertEquals('Test', sb1.substringBefore(' '));
		System.assertEquals('Test S', sb1.substringBeforeLast('t'));
		System.assertEquals(' S', sb1.substringBetween('t'));
		System.assertEquals('est Strin', sb1.substringBetween('T', 'g'));

		System.assertEquals('Test', sb1.left(4));
		System.assertEquals('String', sb1.right(6));
		System.assertEquals('est', sb1.mid(1, 3));
	}

	@isTest
	public static void testReplaceMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals('Empty String', sb1.replace(0, 4, 'Empty').buffer);
		System.assertEquals('Empty String', sb1.replace(6, 12, 'String').buffer);
		System.assertEquals('Test String', sb1.replace('Empty', 'Test').buffer);
		System.assertEquals('String', sb1.replace('String').buffer);
		System.assertEquals('Ring', sb1.replaceAll('[Str]+', 'R').buffer);
		System.assertEquals('Sing', sb1.replaceFirst('[Rn]+', 'S').buffer);
		System.assertEquals('Song', sb1.replace(1, 2, 'o').buffer);
	}

	@isTest
	public static void testContainsMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals(True, sb1.contains('Test'));
		System.assertEquals(True, sb1.containsIgnoreCase('string'));
		System.assertEquals(True, sb1.containsAny('ST'));
		System.assertEquals(True, sb1.containsNone('Z'));
		System.assertEquals(True, sb1.containsOnly('TestSring '));
		System.assertEquals(True, sb1.containsWhitespace());

		System.assertEquals(True, sb1.startsWith('T'));
		System.assertEquals(True, sb1.startsWithIgnoreCase('t'));
		System.assertEquals(True, sb1.endsWith('g'));
		System.assertEquals(True, sb1.endsWithIgnoreCase('G'));
	}

	@isTest
	public static void testComparisonMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals('Test String'.compareTo('test string'), sb1.compareTo('test string'));
		System.assertEquals(0, sb1.compareToIgnoreCase('test string'));
		System.assertEquals(True, sb1.equals('Test String'));
		System.assertEquals(True, sb1.equalsIgnoreCase('test string'));
	}

	@isTest
	public static void testIsMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals(False, sb1.isAllLowerCase());	
		System.assertEquals(False, sb1.isAllUpperCase());
		System.assertEquals(False, sb1.isAlpha());
		System.assertEquals(True, sb1.isAlphaSpace());
		System.assertEquals(False, sb1.isAlphanumeric());
		System.assertEquals(True, sb1.isAlphanumericSpace());
		System.assertEquals(True, sb1.isAsciiPrintable());
		System.assertEquals(False, sb1.isEmpty());
		System.assertEquals(True, sb1.isNotEmpty());
		System.assertEquals(False, sb1.isNumeric());
		System.assertEquals(False, sb1.isNumericSpace());
		System.assertEquals(False, sb1.isWhitespace());
	}

	@isTest
	public static void testToStringMethods() {
		final StringBuilder sb1 = new StringBuilder('Test String');

		System.assertEquals('Test String', sb1.toString());
		System.assertEquals('Test…', sb1.toString(5));
		System.assertEquals('Te...', sb1.toString(5, false));
		System.assertEquals('Test…', sb1.toString(5, 0, true));
		System.assertEquals('Te...', sb1.toString(5, 0, false));
		System.assertEquals('…est…', sb1.toString(5, 1, true));
		System.assertEquals('...est...', sb1.toString(9, 1, false));
		System.assertEquals('…Str…', sb1.toString(5, 5, true));
		System.assertEquals('...Str...', sb1.toString(9, 5, false));
		System.assertEquals('…g', sb1.toString(2, 10, true));
		System.assertEquals('...g', sb1.toString(4, 10, false));
		System.assertEquals('…n…', sb1.toString(3, 9, true));
		System.assertEquals('...n...', sb1.toString(7, 9, false));
	}

	@isTest
	public static void test() {
		StringBuilder sb1 = new StringBuilder('   Test      String  ');

		System.assertEquals('Test String', sb1.normalizeSpace().buffer);
		System.assertEquals('Test String', sb1.replace(' Test String ').trim().buffer);
		System.assertEquals('tEST sTRING', sb1.swapcase().buffer);
		System.assertEquals('TEST STRING', sb1.uppercase().buffer);
		System.assertEquals('test string', sb1.lowercase().buffer);
		System.assertEquals('TEST STRING', sb1.uppercase('it').buffer);
		System.assertEquals('test string', sb1.lowercase('it').buffer);
		System.assertEquals('Test string', sb1.capitalize().buffer);
		System.assertEquals('test string', sb1.uncapitalize().buffer);

		System.assertEquals(' test string', sb1.leftPad(12).buffer);
		System.assertEquals(' test string ', sb1.rightPad(13).buffer);
		System.assertEquals('* test string ', sb1.leftPad(14, '*').buffer);
		System.assertEquals('* test string *', sb1.rightPad(15, '*').buffer);
		System.assertEquals(' * test string * ', sb1.centerPad(17).buffer);
		System.assertEquals('# * test string * #', sb1.centerPad(19, '#').buffer);
		System.assertEquals('#*teststring*#', sb1.deleteWhitespace().buffer);
		System.assertEquals('#*teststring*#'.reverse(), sb1.reverse().buffer);
		System.assertEquals(3, sb1.countMatches('t'));

		final StringBuilder.Range r = new StringBuilder.Range(0, 10);
		final StringBuilder.Range first = r.remove(0)[0];
		final StringBuilder.Range last = r.remove(10)[0];
		final StringBuilder.Range[] mid = r.remove(5);

		System.assert(first.first == 1 && first.last == 10);
		System.assert(last.first == 0 && last.last == 9);
		System.assert(mid[0].first == 0 && mid[0].last == 4 && mid[1].first == 6 && mid[1].last == 10);
	}

	private class CustomObject {

		override
		public String toString() {
			return 'CustomObject';
		}
	}
}