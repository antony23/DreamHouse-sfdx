public without sharing class AddAttachmentExtension {

    public Task task                        {get; set;}
    public Id taskId                        {get; set;}
	public Boolean isError                  {get; set;}
    public Id userId                        {get; set;}
    public Attachment file                  {get; set;}
    public String errorMessage              {get; set;}
    public Boolean errorAttachment          {get; set;}

    public AddAttachmentExtension(ApexPages.StandardController controller){
        isError = false;
        errorAttachment = false;
        taskId = controller.getRecord().Id;
        userId = UserInfo.getUserId();
        file = new Attachment();
        List<String> taskFields = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Task').getDescribe().Fields.getMap().values()){
            taskFields.add(ft.getDescribe().getName().toLowerCase());
        }
        this.task = (Task) Database.query('SELECT ' + String.join(taskFields, ',') + ' FROM Task WHERE Id = \'' + taskId + '\'');
    }

    public PageReference uploadAttachment(){
        try{
           if(file.name != null && file.body != null){
                Attachment attach = new Attachment(
                    ownerId = task.OwnerId,
                    parentId = taskId,
                    name = file.name,
                    body = file.body
                );
                insert attach;
            }else{
                errorAttachment = true;
            	errorMessage = AppConstants.FILE_REQUIRED;
                return new PageReference(ApexPages.currentPage().getUrl());
            }
        }catch(Exception e){
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return new PageReference(ApexPages.currentPage().getUrl());
        }
        return new ApexPages.StandardController(task).view(); 
    }

    public PageReference back(){
        return new ApexPages.StandardController(task).view();
    }
    
    public PageReference checkPermission(){
        PageReference p = null;
		if(userId != task.CreatedById && userId != task.OwnerId){
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, AppConstants.INSUFFICIENT_PRIVILEGES));
        } 
        return p;
    }
}