global class SOQLTools {

	/**
	 *	Returns all the SObject fields with a depth of 0 (no lookup crossing).
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName) {
		return fieldsOf(sObjectName, null, 0, null);
	}

	/**
	 *	Returns all the SObject fields with a depth of 1 (SObject lookup crossing) 
	 *	for the lookup fields indicated into the map.
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@param lookupFilterMap Map containing lookup fields to be crossed for each SObject type
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName, Map<String, String[]> lookupFilterMap) {
		return fieldsOf(sObjectName, lookupFilterMap, 1, null);
	}

	/**
	 *	Returns all the SObject fields with an arbitrary depth.
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@param depth Lookup crossing depth. (e.g. 	
	 * 					depth < 1	=>	No Lookup Crossing
	 *					depth = 1	=>	Direct Lookup Crossing (lookups on the SObject)
	 *					depth > 1	=>	Indirect Lookup Crossing (lookups on SObjects pointed by direct lookups)
	 *				)
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName, Integer depth) {
		return fieldsOf(sObjectName, null, depth, null);
	}

	/**
	 *	Returns all the SObject fields with an arbitrary depth and a disambiguation map.
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@param depth Lookup crossing depth. (e.g. 	
	 *					depth < 1	=>	No Lookup Crossing
	 *					depth = 1	=>	Direct Lookup Crossing (lookups on the SObject)
	 *					depth > 1	=>	Indirect Lookup Crossing (lookups on SObjects pointed by direct lookups)
	 *				)
	 *	@param disambiguationMap Sometimes a lookup field points to different SObject types. This map provides an handy 
	 			way to cut out the ambiguities by associating to each SObject ambiguous lookups a specific SObject type.
	 			(e.g. EmailMessage => (RelatedToId => Account))
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName, Integer depth, Map<String, Map<String, String>> disambiguationMap) {
		return fieldsOf(sObjectName, null, depth, disambiguationMap);
	}

	/**
	 *	Returns all the SObject fields with an arbitrary depth for the lookup fields indicated into the map.
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@param lookupFilterMap Map containing lookup fields to be crossed for each SObject type
	 *	@param depth Lookup crossing depth. (e.g. 	
	 *					depth < 1	=>	No Lookup Crossing
	 *					depth = 1	=>	Direct Lookup Crossing (lookups on the SObject)
	 *					depth > 1	=>	Indirect Lookup Crossing (lookups on SObjects pointed by direct lookups)
	 *				)
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName, Map<String, String[]> lookupFilterMap, Integer depth) {
		return fieldsOf(sObjectName, lookupFilterMap, depth, null);
	}

	/**
	 *	Returns all the SObject fields with an arbitrary depth for the lookup fields indicated into the map 
	 *	and a disambiguation map.
	 *	
	 *	@param sObjectName Name of the SObject
	 *	@param lookupFilterMap Map containing lookup fields to be crossed for each SObject type
	 *	@param depth Lookup crossing depth. (e.g. 	
	 *					depth < 1	=>	No Lookup Crossing
	 *					depth = 1	=>	Direct Lookup Crossing (lookups on the SObject)
	 *					depth > 1	=>	Indirect Lookup Crossing (lookups on SObjects pointed by direct lookups)
	 *				)
	 *	@param disambiguationMap Sometimes a lookup field points to different SObject types. This map provides an handy 
	 			way to cut out the ambiguities by associating to each SObject ambiguous lookups a specific SObject type.
	 			(e.g. EmailMessage => (RelatedToId => Account))
	 *	@return List of fields
	**/

	global static List<String> fieldsOf(String sObjectName, Map<String, String[]> lookupFilterMap, Integer depth, Map<String, Map<String, String>> disambiguationMap) {
		if(sObjectName == null) throw new DomusException.NullPointerException('SObject Name cannot be null');
		if(!Schema.getGlobalDescribe().containsKey(sObjectName)) throw new DomusException.NoSuchSObjectException('No sObject with name "' + sObjectName + '" has been found.');
		if(depth == null) throw new DomusException.NullPointerException('Depth cannot be null');

		final Map<String, String> mapField2Type = (disambiguationMap != null && disambiguationMap.containsKey(sObjectName)) ? disambiguationMap.get(sObjectName) : new Map<String, String>();
		final String[] lookupFields = (lookupFilterMap != null && lookupFilterMap.containsKey(sObjectName)) ? lookupFilterMap.get(sObjectName) : new String[]{};
		final List<String> fields = new List<String>();
		
        for (Schema.SObjectField field : Schema.getGlobalDescribe().get(sObjectName).getDescribe().Fields.getMap().values()){
        	final Schema.DescribeFieldResult dfr = field.getDescribe();
        	final List<Schema.sObjectType> sobjRefs = dfr.getReferenceTo();
        	final String fieldName = dfr.getName();

    		fields.add(fieldName);	// First, adds the field name as-is

    		if(depth < 1) 														continue;	// No depth required
    		if(sobjRefs.size() < 1)												continue;	// Is not a lookup
    		if(sobjRefs.size() > 1 && !mapField2Type.containsKey(fieldName))	continue;	// An ambiguous multi-type lookup field
    		if(lookupFields.size() > 0 && !lookupFields.contains(fieldName))	continue;	// Field doesn't need to be walked through

    		// Turns the fieldName into a relationship-crossable fieldName
    		final String luFieldName = asRelationshipFieldName(sObjectName, fieldName);

    		// Fetches the name of the SObject pointed by the lookup
    		final String pSObjectName = selectFieldType(sObjectName, fieldName, sobjRefs, mapField2Type.get(fieldName));

    		// Fetches the fields of the SObject pointed by the lookup	
    		for(String pSObjectField : fieldsOf(pSObjectName, lookupFilterMap, depth - 1, disambiguationMap)) {
    			fields.add(luFieldName + '.' + pSObjectField);
    		}
        }

        return fields;
	}

	/** INTERNAL METHODS **/

	private static String selectFieldType(String sObjectName, String fieldName, List<Schema.sObjectType> sobjRefs, String requiredType) {
		if(sobjRefs.size() < 1) throw new DomusException.IllegalStateException('No sObject type found for field ' + sObjectName + '.' + fieldName);
		if(sobjRefs.size() == 1) return sobjRefs.get(0).getDescribe().getName();

		// sobjRefs.size() > 1
		for(Schema.sObjectType sobjType : sobjRefs) {
			final String sType = sobjType.getDescribe().getName();

			if(sType.equalsIgnoreCase(requiredType)) {
				return sType;
			}
		}

		throw new DomusException.NoSuchSObjectTypeException('The sObject type "' + requiredType + '" is not supported by the field ' + sObjectName + '.' + fieldName);
	}

	private static String asRelationshipFieldName(String sObjectName, String fieldName) {
		if(fieldName.endsWith('Id')) {					// Standard Lookup Field
			return fieldName.replace('Id', '');
		} else if(fieldName.endsWith('__c')) {			// Custom Lookup Field
			return fieldName.replace('__c', '__r');
		}

		throw new DomusException.IllegalStateException('Field ' + sObjectName + '.' + fieldName + ' has an unsupported format.');
	}

	/*	UNUSED
	private static Map<String, String> loadSObjectPrefixes(){
		final Map<String, String> prefixMap = new Map<String, String>();

        for(Schema.SObjectType stype : Schema.getGlobalDescribe().values()){
            Schema.DescribeSObjectResult r = stype.getDescribe();

            prefixMap.put(r.getName(), r.getKeyPrefix());
        }

        return prefixMap;
    }
    */

}