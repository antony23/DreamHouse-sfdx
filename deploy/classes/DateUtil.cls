public class DateUtil {

	private DateUtil() {}

	public static String getMonthName(Date d) {
		return getMonthName(d, false);
	}

	public static String getMonthName(Date d, Boolean shortName) {
		String name = null;

		switch on d.month() {
			when 1 {
				name = Label.JANUARY;
			}
			when 2 {
				name = Label.FEBRUARY;
			}
			when 3 {
				name = Label.MARCH;
			}
			when 4 {
				name = Label.APRIL;
			}
			when 5 {
				name = Label.MAY;
			}
			when 6 {
				name = Label.JUNE;
			}
			when 7 {
				name = Label.JULY;
			}
			when 8 {
				name = Label.AUGUST;
			}
			when 9 {
				name = Label.SEPTEMBER;
			}
			when 10 {
				name = Label.OCTOBER;
			}
			when 11 {
				name = Label.NOVEMBER;
			}
			when 12 {
				name = Label.DECEMBER;
			}
		}

		if(name != null && shortName) {
			name = name.substring(0, 3);
		}

		return name;
	}

}