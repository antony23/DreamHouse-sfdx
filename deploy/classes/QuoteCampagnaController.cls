public class QuoteCampagnaController{

	private Id accountId;
    private Set<Id> addedProductIdList;
    
    public Boolean isAbbCampaignAdded{get;set;}

	public String campaignCode {get;set;}
    public Integer campaignIndex {get;set;}
    public Id campaignId {get;set;}

    public List<WrapCampagna> wrapCampaignList {get;set;}
    public List<WrapCampagna> wrapDisabledCampaignList {get;set;}
    public Campaign campagnaCodice {get;set;}
    public Integer campaignCodeError {get; set;}
    public String codiceConvenzionePerScontistica {get;set;}
    public zqu.Quote quote {get;set;}

    public Id quoteId {get;set{
			if(String.isBlank(quoteId)){
				quoteId = value;
                initCampaigns();

				//subscriptionList = DomusUtil.getAbbonamenti(quoteId);
			}
		}
	}

	public QuoteCampagnaController(){

	}

	public class WrapCampagna {
        public Campaign campagna {get; set;}
        public Boolean isSelected {get; set;}
        public Boolean isEnabled {get; set;}    
        public String prezzo {get;set;}  
        public String prodottoName {get;set;}
        public String prodottoId {get;set;}
        //public String status {get;set; }

        public WrapCampagna(Campaign c){
            campagna = c;
            isSelected = false;
            isEnabled = false;
            if(c.Sconto_Fisso__c != null){
                prezzo = c.Sconto_Fisso__c + ' €';
            }else if(c.Sconto_Percentuale__c != null){
                prezzo = c.Sconto_Percentuale__c + ' %';
            }
          //  status = '';
        }

        public WrapCampagna(Campaign c, String p){
            campagna = c;
            isSelected = false;
            isEnabled = false;
            prezzo = p;
        }
    }

    private void initCampaigns(){
        try {
            System.debug(loggingLevel.Error, 'INIT CAMPAIGNS');
            isAbbCampaignAdded = false;
            campaignCodeError = 0;
           
            zqu.Quote quote = zqu.Quote.getInstance(quoteId);
            zqu__quote__c quoteWithIntermerdiario = [SELECT Id, Intermediario__c,Intermediario__r.Codice_Convenzione__c FROM zqu__quote__c WHERE Id = :quoteId];
            codiceConvenzionePerScontistica = quoteWithIntermerdiario.Intermediario__c != null ? quoteWithIntermerdiario.Intermediario__r.Codice_Convenzione__c : null;

            zqu__Quote__c quoteObj = quote.getSObject();
            accountId = quoteObj.zqu__Account__c;
            
            //Recupero tutte le campagne di cui l'utente è membro
            List<Campaign> campaignList = DomusUtil.getUserCampaignList(accountId);
            wrapCampaignList = new List<WrapCampagna>();
            wrapDisabledCampaignList = new List<WrapCampagna>();

            //Recupero gli Id di abbonamenti, bundle e vendite dirette già inserite.
            List<zqu__QuoteRatePlanCharge__c> quoteChargeList = [SELECT Id, zqu__QuoteRatePlan__r.Prodotto_Vendita_Diretta__c, zqu__QuoteRatePlan__r.zqu__ProductRatePlan__c
                                                                FROM zqu__QuoteRatePlanCharge__c
                                                                WHERE (Tipo_Charge__c = 'Abbonamento' or Tipo_Charge__c = 'Vendita Diretta')
                                                                AND zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteId];
            addedProductIdList = new Set<Id>();
            for(zqu__QuoteRatePlanCharge__c charge : quoteChargeList){
                if(charge.zqu__QuoteRatePlan__r.Prodotto_Vendita_Diretta__c != null){
                    addedProductIdList.add(charge.zqu__QuoteRatePlan__r.Prodotto_Vendita_Diretta__c);    
                }
                if(charge.zqu__QuoteRatePlan__r.zqu__ProductRatePlan__c != null){
                    addedProductIdList.add(charge.zqu__QuoteRatePlan__r.zqu__ProductRatePlan__c);    
                }
            }

            //Recupero i quoteRateplan delle campagne già inserite
            List<zqu__QuoteRatePlanCharge__c> quoteChargeCampaignList = DomusUtil.getChargeCampaignList(quoteId);
            Set<Id> targetCampaignIds = new Set<Id>();
            Set<Id> codiceCampaignIds = new Set<Id>();
            Set<Id> productVDCampaignIds = new Set<Id>();
            //Set che tiene conto degli id dei PVD presenti in campagne Soglia o Convenzioni o altre campagne codice/target (serve per disabilitare le campagna a target e codice);
            Set<Id> addedCampaignProductIds = new Set<Id>(); 

            Map<Id, Decimal> campaignPVDCostMap = new Map<Id, Decimal>();

            for(zqu__QuoteRatePlanCharge__c charge : quoteChargeCampaignList){
                if(charge.zqu__QuoteRatePlan__r.Campaign__r.Tipo_Promozione__c == 'Altro'){
                    campaignPVDCostMap.put(charge.zqu__QuoteRatePlan__c, charge.zqu__EffectivePrice__c);
                    System.debug(charge.zqu__EffectivePrice__c);
                    productVDCampaignIds.add(charge.zqu__QuoteRatePlan__r.Campaign__c);
                }else{
                    if(charge.zqu__QuoteRatePlan__r.Campaign__r.Rate_Plan_in_Campagna__c != null){
                        addedCampaignProductIds.add(charge.zqu__QuoteRatePlan__r.Campaign__r.Rate_Plan_in_Campagna__c);
                    }else if(charge.zqu__QuoteRatePlan__r.Campaign__r.Prodotto_Vendita_Diretta__c != null){
                        addedCampaignProductIds.add(charge.zqu__QuoteRatePlan__r.Campaign__r.Prodotto_Vendita_Diretta__c);
                    }
                }
                if(charge.zqu__QuoteRatePlan__r.Campaign__r.Tipo_Promozione__c == 'Target'){
                    targetCampaignIds.add(charge.zqu__QuoteRatePlan__r.Campaign__c);
                }
                if(charge.zqu__QuoteRatePlan__r.Campaign__r.Tipo_Promozione__c == 'Codice'){
                    codiceCampaignIds.add(charge.zqu__QuoteRatePlan__r.Campaign__c);
                }
            }
            System.debug(campaignPVDCostMap.keySet());
            List<zqu__QuoteRatePlan__c> quoteCampaignPVDList = DomusUtil.getQuoteCampaignList(campaignPVDCostMap.keySet());

            Map<Id, Campaign> campaignPVDMap = new Map<Id, Campaign>(DomusUtil.getCampaignList(productVDCampaignIds));

            for(zqu__QuoteRatePlan__c cRatePlan : quoteCampaignPVDList){
                Campaign c = campaignPVDMap.get(cRatePlan.Campaign__c);
                if(c!=null){
                    Decimal prezzo = 0;
                    if(campaignPVDCostMap.get(cRatePlan.Id)!=null){
                        prezzo = campaignPVDCostMap.get(cRatePlan.Id);
                    }
                    String prezzoString = prezzo.setScale(2) + ' €';
                    if(cRatePlan.Campaign__r.Descrizione_Codice__c == 'CONVENZIONE' || cRatePlan.Campaign__r.Descrizione_Codice__c == 'SOGLIA'){
                        addedCampaignProductIds.add(cRatePlan.Prodotto_Vendita_Diretta__c);
                    }
                    WrapCampagna wc = new WrapCampagna(c, prezzoString);
                    wc.prodottoName = cRatePlan.Prodotto_Vendita_Diretta__r.Name;
                    wc.prodottoId = cRatePlan.Prodotto_Vendita_Diretta__c;
                    wc.isSelected = true;
                    wc.isEnabled = false;
                    wrapCampaignList.add(wc);
                }
            }

            List<Campaign> campaignCodiceList = DomusUtil.getCampaignList(codiceCampaignIds);
            for(Campaign c : campaignCodiceList){
                WrapCampagna wc = new WrapCampagna(c);
                wc.isSelected = true;
                wc.isEnabled = true;
                wrapCampaignList.add(wc);
            }
            System.debug('addedProductIdList: ' + addedProductIdList);

            for(Campaign c : campaignList)
            {
                System.debug('Campaign  Id:' + c.Id +', Name: ' +c.Name + ', RatePlanCampagna Id: ' + c.Rate_Plan_in_Campagna__c + ' -  ' + c.Rate_Plan_in_Campagna__r.Name);
                System.debug('Rate_Plan_in_Campagna__c: ' +c.Rate_Plan_in_Campagna__c +' Prodotto_Vendita_Diretta__c: '+ c.Prodotto_Vendita_Diretta__c);

                WrapCampagna wc = new WrapCampagna(c);
                //Campagne senza prodotti legati vengono aggiunte alle campagne disponibili
                if(c.Rate_Plan_in_Campagna__c == null && c.Prodotto_Vendita_Diretta__c == null){ 
                    wc.isEnabled = true;
                    if(targetCampaignIds.contains(c.Id)){
                        wc.isSelected = true;
                    }
                    wrapCampaignList.add(wc); 
                } //Campagna con un certo abbonamento, bundle o vendita diretta nel carrello vengono aggiunte alle campagne disponibili.
                else if((addedProductIdList.contains(c.Rate_Plan_in_Campagna__c) && c.Rate_Plan_in_Campagna__c != null) 
                            || (addedProductIdList.contains(c.Prodotto_Vendita_Diretta__c) && c.Prodotto_Vendita_Diretta__c != null)){
                    wc.isEnabled = true;
                    if(targetCampaignIds.contains(c.Id)){
                        wc.isSelected = true;
                    }
                    if(!wc.isSelected && ((addedCampaignProductIds.contains(c.Rate_Plan_in_Campagna__c) && c.Rate_Plan_in_Campagna__c != null) 
                            || (addedCampaignProductIds.contains(c.Prodotto_Vendita_Diretta__c) && c.Prodotto_Vendita_Diretta__c != null))){
                        wc.isEnabled = false;
                    }
                    wrapCampaignList.add(wc); 
                } //Le campagne legate a un abbonamento, bundle o  vendita diretta che non sono presenti nel carrello non sono disponibili.
                else{
                    wc.isEnabled = false;
                    wrapDisabledCampaignList.add(wc);
                }

            }



        } catch(Exception e) {
            appendErrorMessage(e);
        }

    }

    public PageReference refresh(){
        DomusUtil.aggiornaScontiVenditeDiretteNewV(quote, quoteId, codiceConvenzionePerScontistica);
        //DomusUtil.aggiornaScontiVenditeDirette(quoteId);
        return Page.WizardCart;
    }

     //Seaching a campaign given a campaign code
    public void searchCampaign()
    {    
        try{
            campaignCodeError = 0;  
            if(String.isBlank(campaignCode)){
                return ;
            }

            List<Campaign> campaignList = DomusUtil.getCodeCampaign(campaignCode);

            if (campaignList == null || campaignList.size() <= 0)
            {
                campaignCodeError = 3;
                return ;
            }

            //Campaign campaign = campaignList.get(0);
            for(Campaign campaign : campaignList){
                

                List<CampaignMember> campaignMemberList = [SELECT Id, Status FROM CampaignMember
                                                WHERE Campaign.Id = :campaign.Id
                                                AND Campaign.Visibilit_Sistemi__c INCLUDES ('SFDC')
                                                AND Contact.Account.id = :accountId];
                CampaignMember cMember;
                if(campaignMemberList != null && campaignMemberList.size() > 0)
                {
                    cMember = campaignMemberList.get(0);
                }

                for(WrapCampagna wc : wrapCampaignList){
                    if(wc.campagna.id == campaign.id)
                    {
                        campaignCodeError = 4;
                        return ;
                    }
                }        

                if (cMember != null && cMember.status == 'Responded'){
                    campaignCodeError = 1;
                    return;
                }
                /*if(campaign.maxUtilizzi__c != null && campaign.maxUtilizzi__c > 0 && campaign.maxUtilizzi__c <= campaign.numeroUtilizzi__c){
                    campaignCodeError = 2;
                    return;
                }*/


                WrapCampagna tempWc = new WrapCampagna(campaign);

                /*if(cMember != null){
                    tempWc.status = cMember.status;
                }
                else
                {
                    tempWc.status = 'Non attivata';
                }*/
                if(addedProductIdList.contains(tempWc.campagna.Rate_Plan_in_Campagna__c) || 
                    (tempWc.campagna.Rate_Plan_in_Campagna__c == null && tempWc.campagna.Prodotto_Vendita_Diretta__c == null)|| 
                    addedProductIdList.contains(tempWc.campagna.Prodotto_Vendita_Diretta__c)){
                    tempWc.isEnabled = true;
                    wrapCampaignList.add(tempWc);
                }else{
                    wrapDisabledCampaignList.add(tempWc);
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void addCampaign(){
        try{
            if(wrapCampaignList != null && campaignIndex>=0 && campaignIndex<wrapCampaignList.size()){
                WrapCampagna wc = wrapCampaignList.get(campaignIndex);
                wc.isSelected = true;
                if(wc.campagna.Prodotto_Vendita_Diretta__c != null){
                    Id pvdId = wc.campagna.Prodotto_Vendita_Diretta__c;
                    List<zqu__QuoteRatePlan__c> campaignRatePlanList = [SELECT Id, Prodotto_Vendita_Diretta__c
                                                            FROM zqu__QuoteRatePlan__c
                                                            WHERE zqu__Quote__c = :quoteId
                                                            AND Prodotto_Vendita_Diretta__c = :pvdId
                                                            AND Campaign__r.Descrizione_Codice__c = 'LISTINO'];
                    //la campagna a listino è una al massimo
                    if(campaignRatePlanList != null &&  campaignRatePlanList.size()>0){
                        Id ratePLanToBeRemovedId = campaignRatePlanList.get(0).Id;
                        Id tmpPVDId = campaignRatePlanList.get(0).Prodotto_Vendita_Diretta__c;
                        for(Integer i = wrapCampaignList.size()-1; i>=0; i--){
                            WrapCampagna tmpWc = wrapCampaignList.get(i);
                            if(tmpWc.prodottoId == tmpPVDId){
                                wrapCampaignList.remove(i);
                            }
                            if(!tmpWc.isSelected && tmpWc.campagna.Prodotto_Vendita_Diretta__c ==tmpPVDId){
                                wrapCampaignList.get(i).isEnabled = false;
                            }
                        }
                        DomusUtil.removeRatePlan(quoteId, ratePLanToBeRemovedId);
                    }
                   
                }

                DomusUtil.setCampagna(quoteId, wc.campagna.Id);
                
                if(wc.campagna.Prodotto_Vendita_Diretta__c == null){
                    isAbbCampaignAdded = true;
                }

                zqu.Quote quote = zqu.Quote.getInstance(quoteId);
                zqu__Quote__c quoteObj = quote.getSObject(); 
                quoteObj.Campagna__c = wc.campagna.id;
                quote.save();


                List<zqu__QuoteRatePlan__c> campaignQuoteList = DomusUtil.getQuoteCampaignList(quoteId);
                zqu__QuoteRatePlan__c campaignQuoteRatePlan;
                for(zqu__QuoteRatePlan__c qrp : campaignQuoteList){
                    if(qrp.Campaign__c == wc.campagna.Id){
                        campaignQuoteRatePlan = qrp;
                        break;
                    }
                }

                DomusUtil.updateCampaignCharge(quoteId, campaignQuoteRatePlan.id, wc.Campagna);
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }

    }

    public void removeCampaign(){
        try{
            List<zqu__QuoteRatePlan__c> campaignQuoteList = DomusUtil.getQuoteCampaignList(quoteId);
            if(campaignQuoteList!=null && campaignQuoteList.size()>0){
                zqu__QuoteRatePlan__c quoteRatePlan;
                for(zqu__QuoteRatePlan__c qrp : campaignQuoteList){
                    if(qrp.Campaign__c == campaignId){
                        System.debug('campaignId: ' +campaignId);
                        quoteRatePlan = qrp;
                        campaignId = null;
                        break;
                    }
                }

                if(quoteRatePlan != null){
                    System.debug('quoteRatePlan.zqu__ProductRatePlan__c:' + quoteRatePlan.zqu__ProductRatePlan__c);
                    System.debug('quoteRatePlan.Prodotto_Vendita_Diretta__c: ' + quoteRatePlan.Prodotto_Vendita_Diretta__c);
                    if(quoteRatePlan.Campaign__r.Rate_Plan_in_Campagna__c != null && quoteRatePlan.Campaign__r.Prodotto_Vendita_Diretta__c == null){
                        isAbbCampaignAdded = false;
                    }
                    

                    DomusUtil.removeRatePlan(quoteId, quoteRatePlan.Id);
                    for(WrapCampagna wc : wrapCampaignList){
                        if(wc.campagna.id == quoteRatePlan.Campaign__c){
                            wc.isSelected = false;
                        }
                        if((wc.campagna.Prodotto_Vendita_Diretta__c == quoteRatePlan.Prodotto_Vendita_Diretta__c) && quoteRatePlan.Prodotto_Vendita_Diretta__c != null){
                            wc.isEnabled = true;
                        }
                        if((wc.campagna.Rate_Plan_in_Campagna__c == quoteRatePlan.zqu__ProductRatePlan__c) && quoteRatePlan.zqu__ProductRatePlan__c != null){
                            wc.isEnabled = true;
                        }
                    }

                    zqu.Quote quote = zqu.Quote.getInstance(quoteId);
                    zqu__Quote__c quoteObj = quote.getSObject(); 
                    quoteObj.Campagna__c = null;
                    quote.save();
                }
                if(quoteRatePlan.Prodotto_Vendita_Diretta__c != null){
                    DomusUtil.aggiornaScontiVenditeDiretteNewV(quote,quoteId,codiceConvenzionePerScontistica);
                    //DomusUtil.aggiornaScontiVenditeDirette(quoteId);
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }

}