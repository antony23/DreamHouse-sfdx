/* Scheduled Job: Invio Notifiche Email Spedizioni */
global class ShipmentMailProcess implements Database.Batchable<sObject>, Database.StateFul, Schedulable {
	private static final String MT4 = 'MT4';	// Da inviare una sola volta x Subscription
	private static final String MT7 = 'MT7';	// Da inviare una sola volta x Lettera di Vettura
	private static final String MT11 = 'MT11';	// Da inviare per ogni prodotto rispedito
	private static final String SENDER = 'Editoriale Domus - No reply';
	private static final String[] BCCs = new String[]{'sfdc_cc_notifichecrm@edidomus.it'};

	private final String BATCH_QUERY;
	private final Set<Id> SUBSCRIPTION_IDS;
	private final Blocco_Selettivo_Notifiche__c BLOCCO_SELETTIVO_NOTIFICHE;
	private final Map<String, EmailTemplate> EMAIL_TEMPLATES;
	private final OrgWideEmailAddress SENDER_EMAIL_ADDRESS;

	private final List<EmailDataBundle> processedData;
	private final Map<Id, List<Movimentazione_Stock__c>> movStock2SubIdMap;

    global void execute(SchedulableContext SC) {
    	final Blocco_Selettivo_Notifiche__c bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());

    	if(bsn.MT4_Abilitato__c || bsn.MT7_Abilitato__c || bsn.MT11_Abilitato__c) {
    		Database.executeBatch(new ShipmentMailProcess(), 100);
    	}
	}

	global ShipmentMailProcess() {
		this(fetchSubscriptionIds(calculateFromDateTime(), null));
	}

	global ShipmentMailProcess(Datetime startDate) {
		this(fetchSubscriptionIds(startDate, null));
	}

	global ShipmentMailProcess(Datetime startDate, Datetime endDate) {
		this(fetchSubscriptionIds(startDate, endDate));
	}

	global ShipmentMailProcess(Set<Id> subIds) {
		BLOCCO_SELETTIVO_NOTIFICHE = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId()); 	
		SENDER_EMAIL_ADDRESS = EmailSendingUtil.getOrgWideEmailAddressByDisplayName(SENDER);	
 		EMAIL_TEMPLATES = EmailSendingUtil.loadTemplates(new String[]{MT4, MT7, MT11});
 		SUBSCRIPTION_IDS = subIds;

 		/*
			Operando direttamente sulle Subscription siamo sicuri che tutte le Movimentazioni Stock 
			relative ad una stessa spedizione vengano processate insieme
 		*/
 		BATCH_QUERY = 'SELECT Id, Name, Supporto__c, Contact_Bill_To__c, Notifica_Istruzioni_Digital__c FROM Zuora__Subscription__c WHERE Id IN :SUBSCRIPTION_IDS';
		
		processedData = new List<EmailDataBundle>();
		movStock2SubIdMap = new Map<Id, List<Movimentazione_Stock__c>>();

		System.debug('[ShipmentMailProcess][MT4][Enabled] ' + BLOCCO_SELETTIVO_NOTIFICHE.MT4_Abilitato__c);
		System.debug('[ShipmentMailProcess][MT7][Enabled] ' + BLOCCO_SELETTIVO_NOTIFICHE.MT7_Abilitato__c);
		System.debug('[ShipmentMailProcess][MT11][Enabled] ' + BLOCCO_SELETTIVO_NOTIFICHE.MT11_Abilitato__c);
		System.debug('[ShipmentMailProcess][SENDER] ' + SENDER_EMAIL_ADDRESS);
		System.debug('[ShipmentMailProcess][TEMPLATES] ' + EMAIL_TEMPLATES);
		System.debug('[ShipmentMailProcess][SUBSCRIPTION_IDS] ' + SUBSCRIPTION_IDS);
		System.debug('[ShipmentMailProcess][BATCH_QUERY] ' + BATCH_QUERY);
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('ShipmentMailProcess started');

		return Database.getQueryLocator(BATCH_QUERY);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if(scope.isEmpty()) return;

		final Map<Id, Zuora__Subscription__c> subscriptions = new Map<Id, Zuora__Subscription__c>((List<Zuora__Subscription__c>) scope);
		final Map<Id, Movimentazione_Stock__c> movimentazioniStock = fetchMovimentazioniStock(subscriptions.keySet());
		final Map<Id, Contact> billToContacts = new Map<Id, Contact>([SELECT Id, Name, Email, Codice_Nazione__c FROM Contact WHERE Id IN :fetchContactIds(subscriptions.values())]);
		final Map<Id, Integer> ve2SubMap = countVirtualcommEntitlements(subscriptions.keySet());

		System.debug('[ShipmentMailProcess][MOVSTOCK_IDS] ' + movimentazioniStock.keySet());
		System.debug('[ShipmentMailProcess][SUBSCRIPTION_IDS] ' + subscriptions.keySet());
		System.debug('[ShipmentMailProcess][CONTACT_IDS] ' + billToContacts.keySet());

		movStock2SubIdMap.putAll(groupMovStockBySubId(movimentazioniStock.values()));

		List<Movimentazione_Stock__c> movStockList;
		List<EmailDataBundle> data;

		for(Id subId : subscriptions.keySet()) {
			movStockList = movStock2SubIdMap.get(subId);
			data = fetchEmailData(BLOCCO_SELETTIVO_NOTIFICHE, subscriptions.get(subId), EMAIL_TEMPLATES, ve2SubMap, billToContacts, movStockList);
			processedData.addAll(data);
		}
	}

	// Prepara le email, le spedisce e ne notifica l'invio avvalorando i relativi flag
	global void finish(Database.BatchableContext BC) {
		if(processedData.isEmpty()) {
			System.debug('ShipmentMailProcess stopped');
			return;
		}

		final List<Messaging.SingleEmailMessage> preparedEmails = EmailSendingUtil.prepareEmails(processedData, SENDER_EMAIL_ADDRESS, BCCs);
		final List<Messaging.SendEmailResult> results = Messaging.sendEmail(preparedEmails, false);

		System.debug('[ShipmentMailProcess][EMAIL_DATA_BUNDLES] ' + JSON.serialize(processedData));
		//System.debug('[ShipmentMailProcess][EMAIL_MESSAGES] ' + preparedEmails);
		EmailSendingUtil.debug(results, processedData);

		checkNotificationFlag(results, processedData, movStock2SubIdMap);

		if(Test.isRunningTest()) {
			TestShipmentMailProcess.testStatus();
		}

		System.debug('ShipmentMailProcess stopped');
	}

	// Raggruppa le Movimentazioni Stock per Id Subscription in una mappa
	private static Map<Id, List<Movimentazione_Stock__c>> groupMovStockBySubId(List<Movimentazione_Stock__c> movStockList) {
		final Map<Id, List<Movimentazione_Stock__c>> movStock2SubIdMap = new Map<Id, List<Movimentazione_Stock__c>>();

		for(Movimentazione_Stock__c ms : movStockList) {
			if(!movStock2SubIdMap.containsKey(ms.Subscription__c)) {
				movStock2SubIdMap.put(ms.Subscription__c, new List<Movimentazione_Stock__c>());
			}

			movStock2SubIdMap.get(ms.Subscription__c).add(ms);
		}

		return movStock2SubIdMap;
	}

	// Imposta a True i flag di notifica email (Notifica_*__c) se l'invio dell'email è avvenuto con successo
	private static void checkNotificationFlag(List<Messaging.SendEmailResult> results, List<EmailDataBundle> data, Map<Id, List<Movimentazione_Stock__c>> movStock2SubIdMap) {
		final List<Zuora__Subscription__c> subToUpdateList = new List<Zuora__Subscription__c>();
		final List<Movimentazione_Stock__c> movToUpdateList = new List<Movimentazione_Stock__c>();
		final Set<Id> subToUpdateIdSet = new Set<Id>();
		final Set<Id> movToUpdateIdSet = new Set<Id>();

		Zuora__Subscription__c sub;
		Movimentazione_Stock__c mov;
		List<Movimentazione_Stock__c> movList;

		Messaging.SendEmailResult result;
		EmailDataBundle bundle;
		EmailTemplate template;

		for(Integer i = 0; i < results.size(); i++) {
			result = results.get(i);
			bundle = data.get(i);

			if(result.isSuccess() || Test.isRunningTest()) {
				template = bundle.getTemplate();

				if(template.DeveloperName.equals(MT4) && bundle.getRelatedTo() instanceof Zuora__Subscription__c) {
					sub = (Zuora__Subscription__c) bundle.getRelatedTo();
					sub.Notifica_Istruzioni_Digital__c = True;
					
					if(!subToUpdateIdSet.contains(sub.Id)) {
						subToUpdateList.add(sub);
						subToUpdateIdSet.add(sub.Id);
					}
				} else if(template.DeveloperName.equals(MT7) && bundle.getRelatedTo() instanceof Movimentazione_Stock__c) {
					mov = (Movimentazione_Stock__c) bundle.getRelatedTo();
					movList = movStock2SubIdMap.get(mov.Subscription__c);

					// Il Template MT7 riguarda l'intera Spedizione e non i singoli prodotti spediti
					for(Movimentazione_Stock__c ms : movList) {
						ms.Notifica_Spedizione__c = True;

						if(!movToUpdateIdSet.contains(ms.Id)) {						
							movToUpdateList.add(ms);
							movToUpdateIdSet.add(ms.Id);
						}
					}
				} else if(template.DeveloperName.equals(MT11) && bundle.getRelatedTo() instanceof Movimentazione_Stock__c) {
					mov = (Movimentazione_Stock__c) bundle.getRelatedTo();
					mov.Notifica_Rispedizione__c = True;

					if(!movToUpdateIdSet.contains(mov.Id)) {						
						movToUpdateList.add(mov);
						movToUpdateIdSet.add(mov.Id);
					}
				}
			}
		}

		System.debug('[ShipmentMailProcess][UPDATED_SUBSCRIPTIONS] ' + fetchSubscriptionIds(subToUpdateList));
		System.debug('[ShipmentMailProcess][UPDATED_MOVIMENTAZIONI_STOCK] ' + fetchMovimentazioneStockIds(movToUpdateList));

		update subToUpdateList;
		update movToUpdateList;
	}

	// Impacchetta tutti i dati relativi alle email da inviare circa una singola spedizione
	private static List<EmailDataBundle> fetchEmailData(Blocco_Selettivo_Notifiche__c bsn, Zuora__Subscription__c sub,
														Map<String, EmailTemplate> templates, Map<Id, Integer> ve2SubMap, 
														Map<Id, Contact> billToContacts, List<Movimentazione_Stock__c> movimentazioniStock) {
		final List<EmailDataBundle> data = new List<EmailDataBundle>();

		Contact billToContact;
		Boolean notifiedMT4 = false;
		Boolean notifiedMT7 = false;

		for(Movimentazione_Stock__c ms : movimentazioniStock) {
			billToContact = billToContacts.get(sub.Contact_Bill_To__c);
			if(billToContact == null) continue; // Contact non trovato, notifiche email NON abilitate oppure campo Email vuoto

			if(bsn.MT11_Abilitato__c && !ms.Notifica_Rispedizione__c && ms.Rispedizione__c) {
				// Il Template MT11 è abilitato
				// Il prodotto è stato rispedito
				// Non è stata ancora notificata la rispedizione di questo prodotto
				data.add(new EmailDataBundle(ms, billToContact, templates.get(MT11)));
			} else if(bsn.MT7_Abilitato__c && !String.isBlank(sub.Supporto__c) && sub.Supporto__c.contains('Carta')) {
				if(!ms.Notifica_Spedizione__c && !notifiedMT7) {
					// Il Template MT7 è abilitato
					// Il prodotto è stato spedito
					// Non è stata ancora notificata la spedizione
					data.add(new EmailDataBundle(ms, billToContact, templates.get(MT7)));
					notifiedMT7 = true;
				}

				if(bsn.MT4_Abilitato__c && !sub.Notifica_Istruzioni_Digital__c && !notifiedMT4 
					&& sub.Supporto__c.contains('Digitale') && ve2SubMap.get(sub.Id) > 0) {
					// Una email con template MT7 è stata già inviata
					// Il Template MT4 è abilitato
					// Il supporto è Carta + Digitale
					// Non è stata ancora notificata l'invio delle Istruzioni Digital
					data.add(new EmailDataBundle(sub, billToContact, templates.get(MT4)));
					notifiedMT4 = true;
				}
			}
		}

		return data;
	}

	@TestVisible
	// Conta il numero di Subscription Product Charge con supporto digitale il cui entitlement è di tipo Virtualcomm
    private static Map<Id, Integer> countVirtualcommEntitlements(Set<Id> subIds) {
        final Map<Id, Integer> countMap = new Map<Id, Integer>();
        final AggregateResult[] results = [SELECT Zuora__Subscription__c, COUNT(id) N FROM Zuora__SubscriptionProductCharge__c 
                                            WHERE Zuora__Subscription__c IN :subIds AND Supporto__c LIKE '%Digitale%' AND Entitlement__c = 'Virtualcomm' 
                                            GROUP BY Zuora__Subscription__c];

        for(AggregateResult ar : results) {
            countMap.put((Id) ar.get('Zuora__Subscription__c'), Integer.valueOf(ar.get('N')));
        }

        return countMap;
    }

	// Recupera l'id delle Movimentazioni Stock
	private static Set<Id> fetchMovimentazioneStockIds(List<Movimentazione_Stock__c> movimentazioniStock) {
		final Set<Id> ids = new Set<Id>();

		for(Movimentazione_Stock__c ms : movimentazioniStock) {
			ids.add(ms.Id);
		}

		return ids;
	}

	// Recupera tutte le Movimentazioni Stock relative alle Subscription indicate
	private static Map<Id, Movimentazione_Stock__c> fetchMovimentazioniStock(Set<Id> subIds) {
		final String query = 'SELECT ' + String.join(fieldsOf('Movimentazione_Stock__c'), ',') + ' FROM Movimentazione_Stock__c WHERE Subscription__c IN :subIds';

		try {
			return new Map<Id, Movimentazione_Stock__c>((List<Movimentazione_Stock__c>) Database.query(query));
		} catch(Exception e) {
			System.debug(LoggingLevel.ERROR, e.getMessage());
		}

		return new Map<Id, Movimentazione_Stock__c>();
	}

	/*
		Recupera tutti gli Id delle Subscription a cui sono associate Movimentazioni Stock inviate o rispedite 
		il cui stato non è stato notificato al Referente di fatturazione.
	*/
	private static Set<Id> fetchSubscriptionIds(Datetime startDate, Datetime endDate) {
		final String msQuery = buildQueryToFetchSubscriptionIds(startDate, endDate);
		System.debug('[ShipmentMailProcess][SUBSCRIPTION_IDS_QUERY] ' + msQuery);

		final Set<Id> subIds = new Set<Id>();

		try {
			for(Movimentazione_Stock__c ms : (List<Movimentazione_Stock__c>) Database.query(msQuery)) {
				subIds.add(ms.Subscription__c);
			}
		} catch(Exception e) {
			System.debug(LoggingLevel.ERROR, e.getMessage());
		}

		return subIds;
	}

	// Recupera l'id delle subscriptions
	private static Set<Id> fetchSubscriptionIds(List<Zuora__Subscription__c> subscriptions) {
		final Set<Id> ids = new Set<Id>();

		for(Zuora__Subscription__c sub : subscriptions) {
			ids.add(sub.Id);
		}

		return ids;
	}

	// Recupera l'id dei Contact_Bill_To__c (Referente di fatturazione)
	private static Set<Id> fetchContactIds(List<Zuora__Subscription__c> subscriptions) {
		final Set<Id> ids = new Set<Id>();

		for(Zuora__Subscription__c sub : subscriptions) {
			ids.add(sub.Contact_Bill_To__c);
		}

		return ids;
	}

	private static List<string> fieldsOf(String className) {
		final List<String> fields = new List<String>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

	    return fields;
	}

	public static String buildQueryToFetchSubscriptionIds(Datetime startDate) {
		return buildQueryToFetchSubscriptionIds(startDate, null);
	}

	public static String buildQueryToFetchSubscriptionIds(Datetime startDate, Datetime endDate) {
		String query = 'SELECT Subscription__c FROM Movimentazione_Stock__c WHERE ' + 
						'Spedizione__c != NULL AND ' + 
						'Subscription__c != NULL AND ' +
						'Subscription__r.Zuora__Account__c != NULL AND ' +
						'Subscription__r.Contact_Bill_To__c != NULL AND ' + 
						'Subscription__r.isAmendment__c = False AND ' +
						'Subscription__r.Tipo_Cliente__c IN (\'B2C\', \'B2BSmall\') AND ' + 
						'(Subscription__r.Campagna__c IN (\'\', NULL) OR Subscription__r.Campagna__r.Omaggio__c = False) AND ' + 
						'(NOT(Subscription__r.Contact_Bill_To__r.Email IN (\'\', NULL))) AND ' +
						'(' + 
							'(Tipo_Spedizione__c IN (\'Corriere Estero\', \'Raccomandata Estero\')) OR ' + 
							'(Tipo_Spedizione__c = \'Corriere Italia\' AND Tipo_consegna__c IN (\'CORR\', \'CONO\', \'COCO\')) OR ' +
							'(Tipo_Spedizione__c IN (\'Posta Prioritaria\', \'PRIO\') AND Tipo_consegna__c = \'PRIO\') OR ' +
							'(Tipo_Spedizione__c IN (\'Contrassegno\', \'COCO\') AND Tipo_consegna__c = \'COCO\')' +
						') AND ' +
						//'(NOT(Spedizione__r.Numero_Lettura_di_Vettura__c IN (\'\', NULL))) AND ' +
						'Subscription__r.Zuora__Account__r.Notifiche_Email_Disabilitate__c = False AND ' +
						'Stato__c = \'Recepito da SAP\' AND (Notifica_Spedizione__c = False OR (Rispedizione__c = True AND Notifica_Rispedizione__c = False)) AND ' +
						'Segno_del_movimento_del__c = \'-\'';

		if(startDate != null) {
			query += ' AND LastModifiedDate >= ' + startDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		}
		
		if(endDate != null) {
			query += ' AND LastModifiedDate < ' + endDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		}

		return query;					
	}

	public static Datetime calculateFromDateTime() {
		final Datetime now = System.now();
		
		return Datetime.newInstance(now.year(), now.month(), now.day() - 1, now.hour(), now.minute() - 1, 0);
	}
}