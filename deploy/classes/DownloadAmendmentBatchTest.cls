@isTest
private class DownloadAmendmentBatchTest{
	
	@isTest(SeeAllData = True)
	static void testM(){
		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c sub = new Zuora__Subscription__c(Name = 'test');
		insert sub;

		DownloadAmendmentBatch b = new DownloadAmendmentBatch(new Set<Id>{sub.Id});
		try {
			b.start(null);
		} catch(Exception e) {
		}
		try {
			b.execute(null,new List<sObject>{sub});
		} catch(Exception e) {
		}
	}

	@isTest(SeeAllData = True)
	static void testDownloadAmendments(){
		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c sub = new Zuora__Subscription__c(Name = 'test');
		insert sub;

		DownloadAmendmentBatch b = new DownloadAmendmentBatch(new Set<Id>{sub.Id});

		try {
			DownloadAmendmentBatch.downloadAmendments(new List<Zuora__Subscription__c>{sub});
		} catch(Exception e) {}
	}
}