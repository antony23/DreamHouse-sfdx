@isTest
private class RESTTaxDocumentParamTest{
	
	@isTest
	static void testM(){
		RESTTaxDocumentParam.RestObject request = new RESTTaxDocumentParam.RestObject();
		request.restRequest = new RESTTaxDocumentParam.RestRequest();
		request.restRequest.soldToContactId = null;
		request.restRequest.billToContactId = null;
		List<RESTTaxDocumentParam.RestObject> response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});
		System.assert(response.get(0).isError);
		System.assertEquals('Referente di fatturazione non trovato', response.get(0).errorMsg);

		Account a = new Account(Name = 'Test', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId());
		AccountTriggerHandler.skip = true;
		insert a;

		Contact c = new Contact(FirstName = 'T', LastName = 'L', AccountId = a.Id);
		ContactTriggerHandler.skip = true;
		insert c;

		request.restRequest = new RESTTaxDocumentParam.RestRequest();
		request.restRequest.soldToContactId = c.Id;
		request.restRequest.billToContactId = c.Id;
		request.restRequest.splitPayment = 'SI';
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

		request.restRequest.splitPayment = 'NO';
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

		a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Small Business').getRecordTypeId();
		update a;
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

		c.Codice_Nazione__c = 'IT';
		update c;
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

		c.Codice_Nazione__c = 'VA';
		update c;
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

		c.Codice_Nazione__c = 'US';
		update c;
		response = RESTTaxDocumentParam.getTaxDocumentParams(new List<RESTTaxDocumentParam.RestObject>{request});

	}

}