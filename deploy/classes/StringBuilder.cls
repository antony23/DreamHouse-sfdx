public class StringBuilder {
	@TestVisible
	private String buffer;

	public StringBuilder() {
		buffer = '';
	}

	public StringBuilder(String start) {
		this.buffer = start;
	}

	public StringBuilder push(Id e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Id e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Id e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(String e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(String e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, String e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Boolean e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Boolean e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Boolean e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Long e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Long e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Long e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Integer e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Integer e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Integer e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Double e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Double e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Double e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Double e, Integer precision) {
		push((Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder append(Double e, Integer precision) {
		append((Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder add(Integer index, Double e, Integer precision) {
		put(index, (Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder push(Double e, Integer precision, System.RoundingMode roudingMode) {
		push((Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder append(Double e, Integer precision, System.RoundingMode roudingMode) {
		append((Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder add(Integer index, Double e, Integer precision, System.RoundingMode roudingMode) {
		put(index, (Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder push(Decimal e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Decimal e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Decimal e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Decimal e, Integer precision) {
		push((Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder append(Decimal e, Integer precision) {
		append((Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder add(Integer index, Decimal e, Integer precision) {
		put(index, (Object) Tools.format(e, precision));

		return this;
	}

	public StringBuilder push(Decimal e, Integer precision, System.RoundingMode roudingMode) {
		push((Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder append(Decimal e, Integer precision, System.RoundingMode roudingMode) {
		append((Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder add(Integer index, Decimal e, Integer precision, System.RoundingMode roudingMode) {
		put(index, (Object) Tools.format(e, precision, roudingMode));

		return this;
	}

	public StringBuilder push(Date e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Date e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Date e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Date e, String format) {
		push((Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder append(Date e, String format) {
		append((Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder add(Integer index, Date e, String format) {
		put(index, (Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder push(Time e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Time e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Time e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Time e, String format) {
		push((Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder append(Time e, String format) {
		append((Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder add(Integer index, Time e, String format) {
		put(index, (Object) Tools.format(e, format));

		return this;
	}

	public StringBuilder push(Datetime e) {
		push((Object) e);

		return this;
	}

	public StringBuilder append(Datetime e) {
		append((Object) e);

		return this;
	}

	public StringBuilder add(Integer index, Datetime e) {
		put(index, (Object) e);

		return this;
	}

	public StringBuilder push(Datetime e, String format) {
		push((Object) e.format(format));

		return this;
	}

	public StringBuilder append(Datetime e, String format) {
		append((Object) e.format(format));

		return this;
	}

	public StringBuilder add(Integer index, Datetime e, String format) {
		put(index, (Object) e.format(format));

		return this;
	}

	public StringBuilder push(Object e) {
		add(0, e);

		return this;
	}

	public StringBuilder append(Object e) {
		add(this.buffer.length(), e);

		return this;
	}

	public StringBuilder add(Integer index, Object e) {
		if(e instanceof Id) return add(index, (Id) e);
		if(e instanceof String) return add(index, (String) e);
		if(e instanceof Boolean) return add(index, (Boolean) e);
		if(e instanceof Long) return add(index, (Long) e);
		if(e instanceof Integer) return add(index, (Integer) e);
		if(e instanceof Double) return add(index, (Double) e);
		if(e instanceof Decimal) return add(index, (Decimal) e);
		if(e instanceof Date) return add(index, (Date) e);
		if(e instanceof Time) return add(index, (Time) e);
		if(e instanceof DateTime) return add(index, (DateTime) e);

		return put(index, e);
	}

	private StringBuilder put(Integer index, Object e) {
		if(index == 0) 
			this.buffer = e + this.buffer;
		else if(index == this.buffer.length()) 
			this.buffer += e;
		else
			this.buffer = this.buffer.substring(0, index) + e + this.buffer.substring(index);

		return this;
	}

	public Integer length() {
		return this.buffer.length();
	}

	public String getCharAt(Integer index) {
		return this.buffer.substring(index, index + 1);
	}

	public StringBuilder removeCharAt(Integer index) {
		if(index == 0)
			this.buffer = this.buffer.substring(index + 1);
		else if(index == this.buffer.length() - 1)
			this.buffer = this.buffer.substring(0, index);
		else
			this.buffer = this.buffer.substring(0, index) + this.buffer.substring(index + 1, this.buffer.length());

		return this;
	}

	public StringBuilder remove(Integer startIndex) {
		remove(startIndex, this.buffer.length());

		return this;
	}

	public StringBuilder remove(Integer startIndex, Integer endIndex) {
		if(startIndex == 0 && endIndex == this.buffer.length()) {
			this.buffer = '';
		} else {
			final Range startRange = new Range(0, this.buffer.length() - 1);
			final Range remRange = new Range(startIndex, endIndex - 1);
			final Range[] diffRanges = startRange.remove(remRange);
			String diffStr = '';

			for(Range dr : diffRanges) {
				diffStr += this.buffer.substring(dr.first, dr.last + 1);
			}
			
			this.buffer = diffStr;
		}

		return this;
	}

	public StringBuilder remove(String substring){
		this.buffer = this.buffer.remove(substring);

		return this;
	}

	public StringBuilder removeEnd(String substring){
		this.buffer = this.buffer.removeEnd(substring);

		return this;
	}

	public StringBuilder removeEndIgnoreCase(String substring){
		this.buffer = this.buffer.removeEndIgnoreCase(substring);

		return this;
	}

	public StringBuilder removeStart(String substring){
		this.buffer = this.buffer.removeStart(substring);

		return this;
	}

	public StringBuilder removeStartIgnoreCase(String substring){
		this.buffer = this.buffer.removeStartIgnoreCase(substring);

		return this;
	}

	public Integer indexOf(String str){
		return this.buffer.indexOf(str);
	}

	public Integer indexOf(String str, Integer index){
		return this.buffer.indexOf(str, index);
	}

	public Integer indexOfIgnoreCase(String str){
		return this.buffer.indexOfIgnoreCase(str);
	}

	public Integer indexOfIgnoreCase(String str, Integer index){
		return this.buffer.indexOfIgnoreCase(str, index);
	}

	public Integer indexOfAny(String str){
		return this.buffer.indexOfAny(str);
	}

	public Integer indexOfAnyBut(String str){
		return this.buffer.indexOfAnyBut(str);
	}

	public Integer indexOfDifference(String strToCmp){
		return this.buffer.indexOfDifference(strToCmp);
	}

	public Integer lastIndexOf(String str){
		return this.buffer.lastIndexOf(str);
	}

	public Integer lastIndexOf(String str, Integer endPosition){
		return this.buffer.lastIndexOf(str, endPosition);
	}

	public Integer lastIndexOfIgnoreCase(String str){
		return this.buffer.lastIndexOfIgnoreCase(str);
	}

	public Integer lastIndexOfIgnoreCase(String str, Integer endPosition){
		return this.buffer.lastIndexOfIgnoreCase(str, endPosition);
	}

	public String substring(Integer startIndex) {
		return this.buffer.substring(startIndex);
	}

	public String substring(Integer startIndex, Integer endIndex) {
		return this.buffer.substring(startIndex, endIndex);
	}

	public String substringAfter(String separator){
		return this.buffer.substringAfter(separator);
	}

	public String substringAfterLast(String separator){
		return this.buffer.substringAfterLast(separator);
	}

	public String substringBefore(String separator){
		return this.buffer.substringBefore(separator);
	}

	public String substringBeforeLast(String separator){
		return this.buffer.substringBeforeLast(separator);
	}

	public String substringBetween(String tag){
		return this.buffer.substringBetween(tag);
	}

	public String substringBetween(String open, String close){
		return this.buffer.substringBetween(open, close);
	}

	public String left(Integer length) {
		return this.buffer.left(length);
	}

	public String right(Integer length) {
		return this.buffer.right(length);
	}

	public String mid(Integer startIndex, Integer length) {
		return this.buffer.mid(startIndex, length);
	}

	public StringBuilder replace(String replacement) {
		return replace(0, this.buffer.length(), replacement);
	}

	public StringBuilder replace(Integer startIndex, Integer endIndex, String replacement){
		if(startIndex == 0 && endIndex == this.buffer.length()) {
			this.buffer = replacement;
		} else {
			final Range startRange = new Range(0, this.buffer.length() - 1);
			final Range remRange = new Range(startIndex, endIndex - 1);
			final Range[] diffRanges = startRange.remove(remRange);
			String changedStr = '';

			if(diffRanges.size() == 1) {
				final String diffStr = this.buffer.substring(diffRanges[0].first, diffRanges[0].last + 1);

				if(startIndex == 0)
					changedStr = replacement + diffStr;
				else if(endIndex == this.buffer.length())
					changedStr = diffStr + replacement;
				else
					throw new DomusException.IllegalStateException();
			} else if(diffRanges.size() == 2) {
				final String diffStr1 = this.buffer.substring(diffRanges[0].first, diffRanges[0].last + 1);
				final String diffStr2 = this.buffer.substring(diffRanges[1].first, diffRanges[1].last + 1);

				changedStr = diffStr1 + replacement + diffStr2;
			} else {
				throw new DomusException.IllegalStateException();
			}
			
			this.buffer = changedStr;
		}

		return this;
	}

	public StringBuilder replace(String target, String replacement){
		this.buffer = this.buffer.replace(target, replacement);

		return this;
	}

	public StringBuilder replaceAll(String regExp, String replacement){
		this.buffer = this.buffer.replaceAll(regExp, replacement);

		return this;
	}

	public StringBuilder replaceFirst(String regExp, String replacement){
		this.buffer = this.buffer.replaceFirst(regExp, replacement);

		return this;
	}

	public StringBuilder normalizeSpace() {
		this.buffer = this.buffer.normalizeSpace();

		return this;
	}

	public StringBuilder swapcase() {
		this.buffer = this.buffer.swapCase();

		return this;
	}

	public StringBuilder capitalize() {
		this.buffer = this.buffer.capitalize();

		return this;
	}

	public StringBuilder uncapitalize() {
		this.buffer = this.buffer.uncapitalize();

		return this;
	}

	public StringBuilder uppercase() {
		this.buffer = this.buffer.toUppercase();

		return this;
	}

	public StringBuilder uppercase(String locale) {
		this.buffer = this.buffer.toUppercase(locale);

		return this;
	}

	public StringBuilder lowercase() {
		this.buffer = this.buffer.toLowercase();

		return this;
	}

	public StringBuilder lowercase(String locale) {
		this.buffer = this.buffer.toLowercase(locale);

		return this;
	}

	public StringBuilder leftPad(Integer size) {
		this.buffer = this.buffer.leftPad(size);

		return this;
	}

	public StringBuilder leftPad(Integer size, String padding) {
		this.buffer = this.buffer.leftPad(size, padding);

		return this;
	}

	public StringBuilder rightPad(Integer size) {
		this.buffer = this.buffer.rightPad(size);

		return this;
	}

	public StringBuilder rightPad(Integer size, String padding) {
		this.buffer = this.buffer.rightPad(size, padding);

		return this;
	}

	public StringBuilder centerPad(Integer size) {
		this.buffer = this.buffer.center(size);

		return this;
	}

	public StringBuilder centerPad(Integer size, String padding) {
		this.buffer = this.buffer.center(size, padding);

		return this;
	}

	public Integer compareTo(String str) {
		return this.buffer.compareTo(str);
	}

	public Integer compareToIgnoreCase(String str) {
		return this.buffer.toLowercase().compareTo(str.toLowerCase());
	}

	public Boolean contains(String substring){
		return this.buffer.contains(substring);
	}

	public Boolean containsAny(String inputString){
		return this.buffer.containsAny(inputString);
	}

	public Boolean containsIgnoreCase(String substring){
		return this.buffer.containsIgnoreCase(substring);
	}

	public Boolean containsNone(String inputString){
		return this.buffer.containsNone(inputString);
	}

	public Boolean containsOnly(String inputString){
		return this.buffer.containsOnly(inputString);
	}

	public Boolean containsWhitespace(){
		return this.buffer.containsWhitespace();
	}

	public Boolean startsWith(String suffix) {
		return this.buffer.startsWith(suffix);
	}

	public Boolean endsWith(String suffix) {
		return this.buffer.endsWith(suffix);
	}

	public Boolean startsWithIgnoreCase(String suffix) {
		return this.buffer.startsWithIgnoreCase(suffix);
	}

	public Boolean endsWithIgnoreCase(String suffix) {
		return this.buffer.endsWithIgnoreCase(suffix);
	}

	public Boolean equals(String str) {
		return this.buffer.equals(str);
	}

	public Boolean equalsIgnoreCase(String str) {
		return this.buffer.equalsIgnoreCase(str);
	}

	public Boolean isAllLowerCase(){
		return this.buffer.isAllLowerCase();
	}

	public Boolean isAllUpperCase(){
		return this.buffer.isAllUpperCase();
	}

	public Boolean isAlpha(){
		return this.buffer.isAlpha();
	}

	public Boolean isAlphaSpace(){
		return this.buffer.isAlphaSpace();
	}

	public Boolean isAlphanumeric(){
		return this.buffer.isAlphanumeric();
	}

	public Boolean isAlphanumericSpace(){
		return this.buffer.isAlphanumericSpace();
	}

	public Boolean isAsciiPrintable(){
		return this.buffer.isAsciiPrintable();
	}

	public Boolean isEmpty(){
		return String.isEmpty(this.buffer);
	}

	public Boolean isNotEmpty(){
		return String.isNotEmpty(this.buffer);
	}

	public Boolean isNumeric(){
		return this.buffer.isNumeric();
	}

	public Boolean isNumericSpace(){
		return this.buffer.isNumericSpace();
	}

	public Boolean isWhitespace(){
		return this.buffer.isWhitespace();
	}

	public Integer countMatches(String substring) {
		return this.buffer.countMatches(substring);
	}

	public StringBuilder deleteWhitespace() {
		this.buffer = this.buffer.deleteWhitespace();

		return this;
	}

	public StringBuilder trim() {
		this.buffer = this.buffer.trim();

		return this;
	}

	public StringBuilder reverse() {
		this.buffer = this.buffer.reverse();

		return this;
	}

	public StringBuilder clear() {
		this.buffer = '';

		return this;
	}

	override 
	public String toString() {
		return this.buffer;
	}

	public String toString(Integer maxWidth) {
		return this.toString(maxWidth, 0, true);
	}

	public String toString(Integer maxWidth, Boolean useEllipsis) {
		return this.toString(maxWidth, 0, useEllipsis);
	}

	public String toString(Integer maxWidth, Integer offset, Boolean useEllipsis) {
		if(maxWidth < 1 || maxWidth >= this.buffer.length()) return this.buffer;
		if(offset < 0 || offset >= this.buffer.length()) return this.buffer;

		final String truncStr = (useEllipsis) ? '…' : '...';
		final Boolean offsetOnStart = (offset == 0);
		final Boolean offsetOnEnd = (offset == this.buffer.length() - 1);
		final Integer ttStrLen = (offsetOnStart || offsetOnEnd) ? truncStr.length() : truncStr.length() * 2;

		if(maxWidth < ttStrLen + 1) return this.buffer;
		if(offset + (maxWidth - ttStrLen) > this.buffer.length()) return this.buffer;

		return (offsetOnStart ? '' : truncStr) + this.buffer.mid(offset, maxWidth - ttStrLen) + (offsetOnEnd ? '' : truncStr);
	}

	@TestVisible
	private class Range {
		@TestVisible
		public final Integer first, last;

		public Range(Integer first, Integer last) {
			if(first > last) throw new DomusException.IllegalArgumentException('First value must be minor than last value');

			this.first = first;
			this.last = last;
		}

		public Range[] remove(Integer index) {
			if(index < first || index > last) throw new DomusException.IndexOutOfBoundsException();
			if(index == first && index == last) return new Range[]{};
			if(index == first) return new Range[]{ new Range(first + 1, last) };
			if(index == last) return new Range[]{ new Range(first, last - 1) };

			return new Range[]{ new Range(first, index - 1), new Range(index + 1, last) };
		}

		public Range[] remove(Range range) {
			if(!(range.first >= first && range.last <= last)) throw new DomusException.IndexOutOfBoundsException('Argument range is not a proper subset of main range');
			if(range.first == first && range.last == last) return new Range[]{};
			if(range.first == first) return new Range[]{ new Range(range.last + 1, last) };
			if(range.last == last) return new Range[]{ new Range(first, range.first - 1) };

			return new Range[]{ new Range(first, range.first - 1), new Range(range.last + 1, last) };
		}
	}
}