global class MovimentazioneDaInviareScheduled implements Schedulable {
    global void execute(SchedulableContext sc) {
        List<AggregateResult> aggregateResultList = [SELECT Subscription__c FROM Movimentazione_stock__c WHERE Stato__c = 'Da trasmettere' AND Subscription__c != null AND CreatedDate >= 2018-12-18T00:00:00Z GROUP BY Subscription__c];
		List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();        
        
        for(AggregateResult aggr : aggregateResultList){
        	Id subId = (Id) aggr.get('Subscription__c');
        	Zuora__Subscription__c newSub = new Zuora__Subscription__c(Id=subId, Movimentazione_da_inviare__c = true);
        	subscriptionList.add(newSub);
        }
		
		update subscriptionList;        
        //System.debug(subscriptionList);

        for(Zuora__Subscription__c sub : subscriptionList){
        	sub.Movimentazione_da_inviare__c = false;
        }

        update subscriptionList;
        //System.debug(subscriptionList);

    }

    /*global void execute(SchedulableContext sc) {
        List<AggregateResult> aggregateResultList = [SELECT Subscription__c FROM Movimentazione_stock__c WHERE Stato__c = 'Da trasmettere' AND Subscription__c != null GROUP BY Subscription__c];
        List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();        

        Set<Id> subsIds = new Set<Id>();

        for(AggregateResult aggr : aggregateResultList){

            subsIds.add((Id)aggr.get('Subscription__c'));

        }

        List<AggregateResult> subsChargeList = [SELECT Zuora__Subscription__c FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__ProductSKU__c = '' AND Zuora__Subscription__c IN :subsIds GROUP BY Zuora__Subscription__c];
        
        for(AggregateResult aggr : subsChargeList){

            if(subsIds.contains((Id)aggr.get('Zuora__Subscription__c'))){
                subsIds.remove((Id)aggr.get('Zuora__Subscription__c'));
            }

        }

        for(Id i : subsIds){
            Zuora__Subscription__c newSub = new Zuora__Subscription__c(Id=i, Movimentazione_da_inviare__c = true);
            subscriptionList.add(newSub);
        }
        
        update subscriptionList;
    }*/
}