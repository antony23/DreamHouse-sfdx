public class InformativaPrivacyController {

    public Id contactId;
    public Contact contatto                     {get;set;}
    public Boolean marketingDiretto             {get;set;}
    public Boolean marketingIndiretto           {get;set;}
    public Boolean profilazione                 {get;set;}
    public Informativa_Privacy__c informativa   {get;set;}

    public InformativaPrivacyController() {
        this.informativa = InformativaUtils.getInformativaByName('Informativa Corrente');
    }
    
    public Id getContactId() {
        return contactId;
    }

    public void setContactId(Id idContatto) {

        contactId = idContatto;

        if(contactId == null) {
            contatto = new Contact();
            System.debug(LoggingLevel.ERROR, 'Contact Id not found.');
            return;
        }

        contatto = InformativaUtils.fetchContact(contactId);
        marketingDiretto = contatto.Comunicazioni_Commerciali_Marketing__c == 'SI' ? true : false;
        marketingIndiretto = contatto.Comunicazioni_Commerciali_Indirette__c == 'SI' ? true : false;
        profilazione = contatto.Profilazione__c == 'SI' ? true : false;
    }
}