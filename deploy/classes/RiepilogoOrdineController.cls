public class RiepilogoOrdineController {
	@TestVisible private static final String THOUSANDS_SEP = '.';
	@TestVisible private static final String DECIMALS_SEP = ',';
	@TestVisible private static final String CURRENCY_SYMB = '€';

	public Id SubscriptionId;
	public Boolean enableHTML {get;set;}
	public Boolean showRiepilogoOrdine {get;set;}
	public Boolean showColumnSupport {get;set;}

	public Boolean showInfoAbbonamento {get;set;}
	public Boolean showNomeAbbonamento {get;set;}
	public Boolean showDurataAbbonamento {get;set;}
	public Boolean showSupportoAbbonamento {get;set;}

	public Boolean showInfoPagamento {get;set;}
	public Boolean showModalitaPagamento {get;set;}
	public Boolean showStatoPagamento {get;set;}
	public Boolean showDataPagamento {get;set;}

	public Boolean showIndirizzi {get;set;}
	public Boolean showIndirizzoSpedizione {get;set;}
	public Boolean showIndirizzoFatturazione {get;set;}

	public Zuora__Subscription__c Subscription {get; private set;}
	public List<FormattedSubscriptionProductCharge> FProductCharges {get; private set;}
	public Contact BillToContact {get; private set;}
	public Contact SoldToContact {get; private set;}
	public String DataInizio {get; private set;}
	public String DataFine {get; private set;}
	public String MetodoDiPagamento {get; private set;}
	public String DataPagamento {get; private set;}
	public String Totale {get; private set;}

	public RiepilogoOrdineController() {
		
	}

	public Id getSubscriptionId() {
		return SubscriptionId;
	}

	/**
	 * Specifica la Subscription di riferimento.
	 */

	public void setSubscriptionId(Id subId) {
		SubscriptionId = subId;

		if(SubscriptionId == null) {
			initialize();
			System.debug(LoggingLevel.ERROR, 'Subscription Id not found.');
			return;
		}

		Subscription = fetchSubscription(SubscriptionId);
		FProductCharges = formatAll(fetchProductCharges(SubscriptionId));
		Totale = CURRENCY_SYMB + '  ' + numberFormatter(Subscription.Totale__c, 2, THOUSANDS_SEP, DECIMALS_SEP);

		if(Subscription != null) {
			DataPagamento = formatDate(Subscription.Data_pagamento_subscription__c);
			BillToContact = fetchContact(Subscription.Contact_Bill_To__c);
			SoldToContact = fetchContact(Subscription.Contact_Sold_To__c);
			DataInizio = fetchFormattedDate(Subscription.Calendario_Inizio__c);
			DataFine = fetchFormattedDate(Subscription.Calendario_Fine__c);
			MetodoDiPagamento = translateMetodoDiPagamento(Subscription.Metodo_di_Pagamento__c);
		}
	}

	/**
	 * Corregge la formattazione delle SubscriptionProductCharge pre-formattate in modo da occupare lo stesso spazio.
	 */

	@TestVisible
	private static void homogenizeCurrencyFormat(List<FormattedSubscriptionProductCharge> fProductCharges) {
		Integer lenImportoUnitario = 0, lenImportoTotale = 0;

		for(FormattedSubscriptionProductCharge fSPC : fProductCharges) {
			if(lenImportoUnitario < fSPC.Importo_Unitario.length()) {
				lenImportoUnitario = fSPC.Importo_Unitario.length();
			}

			if(lenImportoTotale < fSPC.Importo_Totale.length()) {
				lenImportoTotale = fSPC.Importo_Totale.length();
			}
		}

		for(FormattedSubscriptionProductCharge fSPC : fProductCharges) {
			fSPC.Importo_Unitario = CURRENCY_SYMB + fSPC.Importo_Unitario.leftPad(lenImportoUnitario + 2, ' ');
			fSPC.Importo_Totale = CURRENCY_SYMB + fSPC.Importo_Totale.leftPad(lenImportoTotale + 2, ' ');
		}
	}

	/**
	 * Formatta un numero utilizzando tSep come separatore delle migliaia, dSep come separatore dei decimali e
	 * nDec come numero massimo di cifre decimali da visualizzare.
	 */

	@TestVisible
	private static String numberFormatter(Decimal dec, Integer nDec, String tSep, String dSep) {
		{	// NullPointerException avoiding
			dec = (dec == null) ? 0.0 : dec;
			nDec = (nDec == null) ? 0 : nDec;
			tSep = (tSep == null) ? ',' : tSep;
			dSep = (dSep == null) ? '.' : dSep;
		}

		final Integer intPart = (Integer) Math.abs(dec);
		final Decimal decPart = (Math.abs(dec) - intPart).setScale(2);
		nDec = Math.abs(nDec);

		String[] sIntPart = String.valueOf(intPart).split('');
		String sDecPart = dSep + ((decPart > 0) ? String.valueOf(decPart).rightPad(4, '0').substring(2) : '00');
		String fsNumber = '';
		Integer tCounter = 0;

		for(Integer i = sIntPart.size() - 1; i >= 0; i--) {
			if(tCounter++ == 3) {
				tCounter = 1;
				fsNumber = tSep + fsNumber;
			}

			fsNumber = sIntPart[i] + fsNumber;
		}

		fsNumber += sDecPart.substring(0, nDec + (nDec == 0 ? 0 : 1));
		return ((dec < 0) ? '-' : '') + fsNumber;
	}

	/**
	 * Inizializza tutti gli attributi.
	 */

	@TestVisible
	private void initialize() {
		Subscription = new Zuora__Subscription__c();
		FProductCharges = new List<FormattedSubscriptionProductCharge>();
		BillToContact = new Contact();
		SoldToContact = new Contact();
		DataInizio = '';
		DataFine = '';
		MetodoDiPagamento = '';
		DataPagamento = '';
		Totale = CURRENCY_SYMB + '  0' + DECIMALS_SEP + '00';
	}

	/**
	 * Formatta una data col formato locale-based.
	 */

	@TestVisible
	private static String formatDate(Date d) {
		if(d == null) return '';

		return d.format();
	}

	/**
	 * Trasforma ogni prima lettera in maiuscolo.
	 */

	@TestVisible
	private static String capitalize(String str, Boolean preserveRomanNums) {
		if(str == null) return '';
		if(String.isBlank(str = str.trim())) return '';

		final String[] words = str.split('[^a-zA-Z0-9]+');
		final String[] seps = str.split('[a-zA-Z0-9]+');
		final Pattern romanNumerals = Pattern.compile('(M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3}))');
		Matcher matcher;
		str = '';

		for(Integer i = 0; i < words.size(); i++) {
			matcher = romanNumerals.matcher(words[i]);

			if(preserveRomanNums && matcher.matches()) {
				words[i] = words[i].toUppercase();
			} else {
				words[i] = words[i].substring(0, 1).toUppercase() + words[i].substring(1).toLowerCase();
			}

			str += words[i] + ((i + 1 < seps.size()) ? seps[i + 1] : '');
		}

		return str;
	}

	/**
	 * Converte i metodi di pagamento nelle corrispettive Custom Label.
	 */

	@TestVisible
	private static String translateMetodoDiPagamento(String metodoDiPagamento) {
		if(String.isBlank(metodoDiPagamento)) return '';

		metodoDiPagamento = metodoDiPagamento.trim().toLowerCase();

		switch on metodoDiPagamento {
			when 'assegno' { return Label.ASSEGNO; }
			when 'cash' { return Label.CASH; }
			when 'cbill' { return Label.CBILL; }
			when 'contrassegno' { return Label.CONTRASSEGNO; }
			when 'paypal' { return Label.PAYPAL; }
			when 'pos' { return Label.POS; }
			when 'predefinita' { return Label.PREDEFINITA; }
			when else {
				if(metodoDiPagamento.contains('bollettino') && metodoDiPagamento.contains('postale')) {
					return Label.BOLLETTINO_POSTALE;
				} else if(metodoDiPagamento.contains('bonifico') && metodoDiPagamento.contains('sepa')) {
					return Label.BONIFICO_SEPA;
				} else if(metodoDiPagamento.contains('carta') && metodoDiPagamento.contains('credito') && !metodoDiPagamento.contains('internet')) {
					return Label.CARTA_DI_CREDITO;
				} else if(metodoDiPagamento.contains('carta') && metodoDiPagamento.contains('credito') && metodoDiPagamento.contains('internet')) {
					return Label.CARTA_DI_CREDITO_INTERNET;
				} else if(metodoDiPagamento.contains('paypal') && metodoDiPagamento.contains('internet')) {
					return Label.PAYPAL_INTERNET;
				}
			}
		}

		return 'unknown';
	}

	/**
	 * Recupera la subscription con Id indicato e ne formatta alcuni campi.
	 */

	@TestVisible
	private static Zuora__Subscription__c fetchSubscription(Id subId) {
		final String query = 'SELECT ' + String.join(fieldsOf('Zuora__Subscription__c'), ',') + 
								' FROM Zuora__Subscription__c WHERE Id = :subId LIMIT 1';

        Zuora__Subscription__c subscription = (Zuora__Subscription__c) Database.query(query);

		try {
			if(subscription.Totale__c == null || String.isBlank(String.valueOf(subscription.Totale__c))) {
				subscription.Totale__c = 0.0;
			}
		}catch(Exception e) {
			subscription.Totale__c = 0.0;
		}

		if(!String.isBlank(subscription.Testate__c)) {
			subscription.Testate__c = capitalize(subscription.Testate__c, false);
		}

		subscription.Supporto__c = formatSupporto(subscription.Supporto__c);

        return subscription;
	}

	/**
	 * Restituisce una versione formattata del supporto (cartaceo o digitale).
	 */

	@TestVisible
	private static String formatSupporto(String supporto) {
		if(String.isBlank(supporto)) {
        	return '';
        } else if(supporto.contains('Carta') && !supporto.contains('Digitale')) {
        	return Label.Supporto_Carta;
        } else if(!supporto.contains('Carta') && supporto.contains('Digitale')) {
        	return Label.Supporto_Digitale;
        } else if(supporto.contains('Carta') && supporto.contains('Digitale')) {
        	return Label.Supporto_Carta + ' + ' + Label.Supporto_Digitale;
        }

		return '';
	}

	/**
	 * Recupera le SubscriptionProductCharge di una subscription, le ordina secondo alcuni principi, 
	 * valuta gli sconti percentuali e pre-formatta alcuni campi.
	 */

	@TestVisible
	private static List<Zuora__SubscriptionProductCharge__c> fetchProductCharges(Id subId) {
		final String query = 'SELECT ' + String.join(fieldsOf('Zuora__SubscriptionProductCharge__c'), ',') + ', Zuora__SubscriptionRatePlan__r.Name, Zuora__SubscriptionRatePlan__r.Original_Product_RatePlan__r.TipoProdotto__c ' +
								'FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Subscription__c = :subId';

		List<Zuora__SubscriptionProductCharge__c> productCharges = (List<Zuora__SubscriptionProductCharge__c>) Database.query(query);
		productCharges = sortSubscriptionProductCharges(productCharges);
		productCharges = calculatePercentageDiscouts(productCharges);
		//productCharges = adaptFields(productCharges);

        return productCharges;
	}

	/**
	 * Metodo di utilità che pre-formatta alcuni campi.
	 */

	/*
	@Deprecated
	@TestVisible
	private static List<Zuora__SubscriptionProductCharge__c> adaptFields(List<Zuora__SubscriptionProductCharge__c> productCharges) {
		for(Zuora__SubscriptionProductCharge__c spc : productCharges) {
			if(spc.Zuora__Quantity__c == null || spc.Zuora__Quantity__c == 0) {
				spc.Zuora__Quantity__c = 1;
			}

			if(spc.Zuora__ExtendedAmount__c == null || spc.Zuora__ExtendedAmount__c == 0) {
				spc.Zuora__ExtendedAmount__c = spc.Zuora__Quantity__c * spc.Zuora__Price__c;
			}

			if(spc.Tipo_Prodotto__c != null) {
				switch on spc.Tipo_Prodotto__c {
					when 'Sconto' {
						spc.Zuora__Price__c *= -1;
						spc.Zuora__ExtendedAmount__c *= -1;
					}
					when else {}
				}
			}
		}

		return productCharges;
	}*/

	/**
	 * Ordina le SubscriptionProductCharge in base al tipo e al tipo di prodotto a cui si riferiscono.
	 */

	@TestVisible
	private static List<Zuora__SubscriptionProductCharge__c> sortSubscriptionProductCharges(List<Zuora__SubscriptionProductCharge__c> productCharges) {
		final List<SubscriptionProductChargeWrapper> wrappers = new List<SubscriptionProductChargeWrapper>();

		for(Zuora__SubscriptionProductCharge__c spc : productCharges) {
			//if((spc.Tipo_Prodotto__c != null && !spc.Tipo_Prodotto__c.equalsIgnoreCase('Sconto')) || (spc.Zuora__Price__c > 0 || spc.Zuora__ExtendedAmount__c > 0)) {
				wrappers.add(new SubscriptionProductChargeWrapper(spc));
			//}
		}

		wrappers.sort();
		productCharges.clear();

		for(SubscriptionProductChargeWrapper wspc : wrappers) {
			productCharges.add(wspc.getSubscriptionProductCharge());
		}

		return productCharges;
	}

	/**
	 * Valuta gli sconti percentuali in base alle SubscriptionProductCharge fornite.
	 */

	@TestVisible
	private static List<Zuora__SubscriptionProductCharge__c> calculatePercentageDiscouts(List<Zuora__SubscriptionProductCharge__c> productCharges) {
		final String VD_LABEL = 'Vendite Dirette', ABB_LABEL = 'Abbonamenti';
		final Map<String, List<Zuora__SubscriptionProductCharge__c>> percSPCMap = new Map<String, List<Zuora__SubscriptionProductCharge__c>> {
																																				VD_LABEL => new List<Zuora__SubscriptionProductCharge__c>(),
																																				ABB_LABEL => new List<Zuora__SubscriptionProductCharge__c>()
																																			};
		final Map<String, PercentageDiscountCalculator> discountMap = new Map<String, PercentageDiscountCalculator>{
																									VD_LABEL => new PercentageDiscountCalculator(false),
																									ABB_LABEL => new PercentageDiscountCalculator(true)
																								};

		for(Zuora__SubscriptionProductCharge__c spc : productCharges) {
			final String tipoProdottoRef = getProductType(spc);

			if(!discountMap.containsKey(tipoProdottoRef)) {
				discountMap.put(tipoProdottoRef, new PercentageDiscountCalculator(false));
			}

			if(spc.Tipo_Prodotto__c != null && spc.Tipo_Prodotto__c.equalsIgnoreCase('Sconto')) {
				if(spc.Zuora__Model__c.equalsIgnoreCase('Discount-Percentage')) {
					if(spc.Zuora__RatePlanName__c.contains(VD_LABEL)) {
						percSPCMap.get(VD_LABEL).add(spc);
					} else if(spc.Zuora__RatePlanName__c.contains(ABB_LABEL)) {
						percSPCMap.get(ABB_LABEL).add(spc);
					}
				} else if(spc.Zuora__Model__c.equalsIgnoreCase('Discount-Fixed Amount')) {
					if(discountMap.containsKey(tipoProdottoRef)) {
						discountMap.get(tipoProdottoRef).sconto += (spc.Zuora__ExtendedAmount__c != null)
							? spc.Zuora__ExtendedAmount__c
							: spc.Zuora__Price__c * spc.Zuora__Quantity__c;
					}
				}
			} else if(spc.Tipo_Prodotto__c != null && spc.Tipo_Prodotto__c.equalsIgnoreCase('Spese di Spedizione')) {
				discountMap.get(tipoProdottoRef).speseSpedizione += spc.Zuora__ExtendedAmount__c;
			} else if(spc.Tipo_Prodotto__c != null && spc.Tipo_Prodotto__c.equalsIgnoreCase('Servizio di contrassegno')) {
				discountMap.get(tipoProdottoRef).speseSpedizione += spc.Zuora__ExtendedAmount__c;
			} else if(spc.Tipo_Prodotto__c != null && spc.Tipo_Prodotto__c.equalsIgnoreCase('Abbonamento')) {
				discountMap.get(ABB_LABEL).spesa += spc.Zuora__ExtendedAmount__c;
			} else if(spc.Tipo_Prodotto__c == null || spc.Tipo_Prodotto__c.equalsIgnoreCase('Vendita Diretta')) {
				discountMap.get(VD_LABEL).spesa += spc.Zuora__ExtendedAmount__c;
			}

			System.debug('>> SubscriptionProductCharge: ' + spc);
			System.debug('>> VD PercentageDiscountCalculator: ' + discountMap.get(VD_LABEL));
			System.debug('>> ABB PercentageDiscountCalculator: ' + discountMap.get(ABB_LABEL));
		}

		for(Zuora__SubscriptionProductCharge__c spc : percSPCMap.get(VD_LABEL)) {
			spc.Zuora__Price__c = discountMap.get(VD_LABEL).calculate(spc.Zuora__Price__c);
			System.debug('>> Sconto % Vendite Dirette: ' + spc);
		}

		for(Zuora__SubscriptionProductCharge__c spc : percSPCMap.get(ABB_LABEL)) {
			spc.Zuora__Price__c = discountMap.get(ABB_LABEL).calculate(spc.Zuora__Price__c);
			System.debug('>> Sconto % Abbonamenti: ' + spc);
		}

		return productCharges;
	}

	/**
	 * Metodo di utilità per raggruppare sotto un'unica voce più tipi di subscription affini.
	 */

	@TestVisible
	private static String getProductType(Zuora__SubscriptionProductCharge__c spc) {
		if(spc != null && spc.Zuora__SubscriptionRatePlan__c != null && 
			spc.Zuora__SubscriptionRatePlan__r.Original_Product_RatePlan__c != null) {
				switch on spc.Zuora__SubscriptionRatePlan__r.Original_Product_RatePlan__r.TipoProdotto__c {
					when 'Abbonamento', 'Bundle Abbonamenti' {
						return 'Abbonamenti';
					}
					when 'Vendita Diretta', 'Bundle Vendite Dirette' {
						return 'Vendite Dirette';
					}
					when else {
						return spc.Zuora__SubscriptionRatePlan__r.Original_Product_RatePlan__r.TipoProdotto__c;
					}
				}
		}

		return 'Vendite Dirette';
	}

	/**
	 * Restituisce un contatto con alcuni campi pre-formattati.
	 */

	@TestVisible
	private static Contact fetchContact(Id contactId) {
		if(contactId == null) return new Contact();

		final String query = 'SELECT ' + String.join(fieldsOf('Contact'), ',') + 
								' FROM Contact WHERE Id = :contactId LIMIT 1';

        final Contact contact = (Contact) Database.query(query);

        contact.FirstName = capitalize(contact.FirstName, true);
        contact.LastName = capitalize(contact.LastName, true);
        contact.MailingStreet = capitalize(contact.MailingStreet, true);
        contact.MailingPostalCode = capitalize(contact.MailingPostalCode, true);
        contact.MailingCity = capitalize(contact.MailingCity, true);
        //contact.MailingState = capitalize(contact.MailingState, true);
        contact.MailingCountry = capitalize(contact.MailingCountry, true);

        return contact;
	}

	/**
	 * Restituisce una data pre-formattata.
	 */

	@TestVisible
	private static String fetchFormattedDate(Id calendarId) {
		if(calendarId == null) return '';

		final String query = 'SELECT ' + String.join(fieldsOf('Calendario__c'), ',') + 
								' FROM Calendario__c WHERE Id = :calendarId LIMIT 1';

        final Calendario__c calendar = (Calendario__c) Database.query(query);
        
        String month = DateUtil.getMonthName(calendar.Data_Uscita__c);
    	String year = calendar.Data_Uscita__c.year() + '';
    	
    	return month + ' ' + year;
	}

	/**
	 * Restituisce tutti i campi di un determinato oggetto.
	 */

	@TestVisible
	private static List<string> fieldsOf(String className) {
		final List<string> fields = new List<string>();

        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }

	    return fields;
	}

	/**
	 * Crea una lista di SubscriptionProductCharge pre-formattate e pronte per essere visualizzate.
	 */

	@TestVisible
	private static List<FormattedSubscriptionProductCharge> formatAll(List<Zuora__SubscriptionProductCharge__c> productCharges) {
		List<FormattedSubscriptionProductCharge> fSPCList = new List<FormattedSubscriptionProductCharge>();
		Decimal sconto = 0.0;

		for(Zuora__SubscriptionProductCharge__c spc : productCharges) {
			final FormattedSubscriptionProductCharge fSPC = new FormattedSubscriptionProductCharge(spc);
			
			if(fSPC.Prodotto.equals(Label.Sconto)) {
				sconto -= fSPC.numImportoTotale;
			} else {
				fSPCList.add(fSPC);
			}
		}

		if(Math.abs(sconto) > 0) {	// Unica linea di sconto non nullo
			fSPCList.add(new FormattedSubscriptionProductCharge(Label.Sconto, 1, sconto, sconto, ''));
		}

		homogenizeCurrencyFormat(fSPCList);

		return fSPCList;
	}

	/**
	 * Classe di utilità per valutare in un secondo momento lo sconto percentuale.
	 */

	@TestVisible
	class PercentageDiscountCalculator {
		Boolean isAbbonamento;
		Decimal spesa, speseSpedizione, sconto;

		public PercentageDiscountCalculator(Boolean isAbbonamento) {
			this.isAbbonamento = isAbbonamento;
			this.spesa = 0.0;
			this.speseSpedizione = 0.0;
			this.sconto = 0.0;
		}

		@TestVisible
		public Decimal calculate(Decimal percentage) {
			Decimal total = 0;

			if(isAbbonamento) {
				total += spesa + speseSpedizione - sconto;
			} else {
				total += spesa;
			}

			return total * percentage / 100;
		}
	}

	@TestVisible
	// Classe wrapper per implementare un ordinamento custom delle Subscription Product Charge
	class SubscriptionProductChargeWrapper implements Comparable {
		Zuora__SubscriptionProductCharge__c spc;

		public SubscriptionProductChargeWrapper(Zuora__SubscriptionProductCharge__c spc) {
			this.spc = spc;
		}


		@TestVisible
		public Zuora__SubscriptionProductCharge__c getSubscriptionProductCharge() {
			return spc;
		}


		@TestVisible
		public Integer compareTo(Object obj) {
			final SubscriptionProductChargeWrapper wrp2 = (SubscriptionProductChargeWrapper) obj;
			final Zuora__SubscriptionProductCharge__c spc2 = wrp2.spc;
			final String srp1 = spc.Zuora__SubscriptionRatePlan__c + '';
			final String srp2 = spc2.Zuora__SubscriptionRatePlan__c + '';

			Integer cmp;

			// Confronta le priorità basate sul tipo di prodotto di riferimento
			cmp = (calculatePriorityOnRefType(spc) - calculatePriorityOnRefType(spc2)) * -1;

			if(cmp == 0) {
				cmp = compare(spc.Testata__c, spc2.Testata__c);

				if(cmp == 0) {
					// Confronta le priorità basate sul tipo di prodotto effettivo
					cmp = (calculatePriorityOnType(spc) - calculatePriorityOnType(spc2)) * -1;
				}
			}

			return cmp;
		}

		/**
		 * Restituisce il tipo di prodotto a cui si rifeisce lo sconto percentuale.
		 */

		@TestVisible
		private String getPercentageDiscountType(Zuora__SubscriptionProductCharge__c spc) {
			final String name = spc.Zuora__SubscriptionRatePlan__r.Name.toLowerCase();

			if((name.contains('vendit') && name.contains('dirett')) || name.contains('vd')) {
				return 'Vendite Dirette';
			} else if(name.contains('abbonament') || name.contains('abb')) {
				return 'Abbonamenti';
			}

			return '';
		}

		@TestVisible
		private Integer compare(String c1, String c2) {
			if(c1 != null && c2 != null) {
				return c1.compareTo(c2);
			} else if(c1 != null && c2 == null) {
				return -1;
			} else if(c1 == null && c2 != null) {
				return 1;
			} else {
				return 0;
			}
		}

		/**
		 * Calcola la priorità in base al prodotto di riferimento della SubscriptionProductCharge.
		 */

		@TestVisible
		private Integer calculatePriorityOnRefType(Zuora__SubscriptionProductCharge__c spc) {
			switch on (spc.Zuora__Model__c.equals('Discount-Percentage') ? getPercentageDiscountType(spc) : getProductType(spc)) {
				when 'Abbonamenti' {
					return 5;
				}
				when 'Bundle Misti' {
					return 4;
				}
				when 'Vendite Dirette' {
					return 3;
				}
				when 'Spese Spedizione' {
					return 2;
				}
				when 'Split' {
					return 1;
				}
				when 'Sconti' {
					return 0;
				}
			}

			return 0;
		}
		
		/**
		 * Calcola la priorità in base al tipo della SubscriptionProductCharge.
		 */

		@TestVisible
		private Integer calculatePriorityOnType(Zuora__SubscriptionProductCharge__c spc) {
			if(spc.Tipo_Prodotto__c != null) {
				switch on spc.Tipo_Prodotto__c {
					when 'Abbonamento' {
						return 4;
					}
					when 'Vendita Diretta' {
						return 3;
					}
					when 'Spese di Spedizione' {
						return 2;
					}
					when 'Sconto' {
						return 1;
					}
				}
			}

			return 0;
		}
	}
	
	/**
	 * Classe di utilità per fornire un output pre-formattato al componente VF.
	 */

	@TestVisible
	class FormattedSubscriptionProductCharge {
		public String Prodotto {get; private set;}
		public String Quantita {get; private set;}
		public String Importo_Unitario {get; private set;}
		public String Importo_Totale {get; private set;}
		public String Supporto {get; private set;}

		@TestVisible private Integer numQuantita {private get; private set;}
		@TestVisible private Decimal numImportoUnitario {private get; private set;}
		@TestVisible private Decimal numImportoTotale {private get; private set;}

		public FormattedSubscriptionProductCharge(String prod, Integer qt, Decimal uAmount, Decimal tAmount, String format) {
			/*Prodotto = prod;
			Quantita = numberFormatter(qt, 0, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Unitario = numberFormatter(uAmount, 2, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Totale = numberFormatter(tAmount, 2, THOUSANDS_SEP, DECIMALS_SEP);
			Supporto = format;*/

			initialize(prod == null ? '' : prod, qt == null ? 0 : qt, uAmount == null ? 0.0 : uAmount, 
						tAmount == null ? 0.0 : tAmount, format == null ? '' : format);
		}

		public FormattedSubscriptionProductCharge(Zuora__SubscriptionProductCharge__c spc) {
			/*Prodotto = getProdotto(spc);
			Quantita = numberFormatter(getQuantita(spc), 0, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Unitario = numberFormatter(getImportoUnitario(spc), 2, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Totale = numberFormatter(getImportoTotale(spc), 2, THOUSANDS_SEP, DECIMALS_SEP);
			Supporto = getSupporto(spc);*/

			initialize(getProdotto(spc), getQuantita(spc), getImportoUnitario(spc), getImportoTotale(spc), getSupporto(spc));
		}

		@TestVisible
		private void initialize(String prod, Integer qt, Decimal uAmount, Decimal tAmount, String format) {
			qt = (qt < 1) ? 1 : qt;

			if(uAmount > 0.0 && tAmount <= 0.0) {
				tAmount = qt * uAmount;
			} else if(tAmount > 0.0 && uAmount <= 0.0) {
				uAmount = tAmount / qt;
			}

			if(prod.equals(Label.Sconto)) {
				uAmount *= -1;
				tAmount *= -1;
			}

			numQuantita = qt;
			numImportoUnitario = uAmount;
			numImportoTotale = tAmount;

			Prodotto = prod;
			Quantita = numberFormatter(numQuantita, 0, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Unitario = numberFormatter(numImportoUnitario, 2, THOUSANDS_SEP, DECIMALS_SEP);
			Importo_Totale = numberFormatter(numImportoTotale, 2, THOUSANDS_SEP, DECIMALS_SEP);
			Supporto = format;
		}

		@TestVisible
		private String getProdotto(Zuora__SubscriptionProductCharge__c spc) {
			if(spc.Tipo_Prodotto__c == null || String.isBlank(spc.Tipo_Prodotto__c)) {
				return capitalize(spc.Name, false);
			}

			switch on spc.Tipo_Prodotto__c {
				when 'Abbonamento' {
					return Label.Abbonamento_a + ' ' + capitalize(spc.Testata__c, false);
				}
				when 'Vendita Diretta' {
					if(!String.isBlank(spc.Descrizione_Prodotto__c)) {
						return capitalize(spc.Descrizione_Prodotto__c, false);
					} else if(!String.isBlank(spc.Zuora__Description__c)) {
						return capitalize(spc.Zuora__Description__c, false);
					} else {
						return capitalize(spc.Name, false);
					}
				}
				when 'Spese di Spedizione' {
					return Label.Spese_Spedizione;
				}
				when 'Sconto' {
					return Label.Sconto;
				}
				when 'Servizio di contrassegno' {
					return Label.Servizio_Contrassegno;
				}
				when else {
					return capitalize(spc.Name, false);
				}
			}
		}

		@TestVisible
		private Integer getQuantita(Zuora__SubscriptionProductCharge__c spc) {
			try {
				if(spc.Zuora__Quantity__c == null || String.isBlank(String.valueOf(spc.Zuora__Quantity__c))) {
					return 0;
				}
			} catch(Exception e) {
				return 0;
			}

			return (Integer) spc.Zuora__Quantity__c;
		}

		@TestVisible
		private Decimal getImportoUnitario(Zuora__SubscriptionProductCharge__c spc) {
			try {
				if(spc.Zuora__Price__c == null || String.isBlank(String.valueOf(spc.Zuora__Price__c))) {
					return 0.0;
				}
			} catch(Exception e) {
				return 0.0;
			}

			return spc.Zuora__Price__c;
		}

		@TestVisible
		private Decimal getImportoTotale(Zuora__SubscriptionProductCharge__c spc) {
			try {
				if(spc.Zuora__ExtendedAmount__c == null || String.isBlank(String.valueOf(spc.Zuora__ExtendedAmount__c))) {
					return 0.0;
				}
			} catch(Exception e) {
				return 0.0;
			}
			
			return spc.Zuora__ExtendedAmount__c;
		}

		@TestVisible
		private String getSupporto(Zuora__SubscriptionProductCharge__c spc) {
			return formatSupporto(spc.Supporto__c);
		}
	}
}