public class SubscriptionRatePlanTriggerHandler {
    public static Boolean skipTrigger = false;
    
    public static void onBeforeInsert(List<Zuora__SubscriptionRatePlan__c> triggerNew, Map<Id, Zuora__SubscriptionRatePlan__c> triggerNewMap) { 
        setValuesOnSubscription(triggerNew);
    }
    
    public static void onAfterInsert(List<Zuora__SubscriptionRatePlan__c> triggerNew, Map<Id, Zuora__SubscriptionRatePlan__c> triggerNewMap) {
        //setValuesOnSubscription(triggerNew);
    }

    public static void onBeforeUpdate(List<Zuora__SubscriptionRatePlan__c> triggerOld, List<Zuora__SubscriptionRatePlan__c> triggerNew, Map<Id, Zuora__SubscriptionRatePlan__c> triggerOldMap, Map<Id, Zuora__SubscriptionRatePlan__c> triggerNewMap){   
    }

    public static void onAfterUpdate(List<Zuora__SubscriptionRatePlan__c> triggerOld, List<Zuora__SubscriptionRatePlan__c> triggerNew, Map<Id, Zuora__SubscriptionRatePlan__c> triggerOldMap, Map<Id, Zuora__SubscriptionRatePlan__c> triggerNewMap){   
    }

    public static void onBeforeDelete(List<Zuora__SubscriptionRatePlan__c> triggerOld, Map<Id, Zuora__SubscriptionRatePlan__c> triggerOldMap){  
    }

    public static void onAfterDelete(List<Zuora__SubscriptionRatePlan__c> triggerOld, Map<Id, Zuora__SubscriptionRatePlan__c> triggerOldMap){  
    }

    private static void setValuesOnSubscription(List<Zuora__SubscriptionRatePlan__c> subscriptionRatePlanList){

        Set<String> productRatePlanZuoraIds = new Set<String>();
        for(Zuora__SubscriptionRatePlan__c subscriptionRatePlan : subscriptionRatePlanList){
            productRatePlanZuoraIds.add(subscriptionRatePlan.Zuora__OriginalProductRatePlanId__c);
        }
        Map<String,zqu__ProductRatePlan__c> productRatePlans = new Map<String,zqu__ProductRatePlan__c>();
        for(zqu__ProductRatePlan__c productRatePlan : [SELECT Id,zqu__ZuoraId__c,TipoProdotto__c 
                                                        FROM zqu__ProductRatePlan__c 
                                                        WHERE zqu__ZuoraId__c IN :productRatePlanZuoraIds
                                                        AND TipoProdotto__c IN ('Abbonamento','Bundle Abbonamenti','Sconti','Bundle Misti')]){ 
            productRatePlans.put(productRatePlan.zqu__ZuoraId__c,productRatePlan);
        }

        Set<Id> subscriptionsId = new Set<Id>();
        List<Zuora__SubscriptionRatePlan__c> subscriptionRatePlanAbbonamentoList = new List<Zuora__SubscriptionRatePlan__c>();
        for(Zuora__SubscriptionRatePlan__c subscriptionRatePlan : subscriptionRatePlanList){
            // se null è un prodotto di tipo non abbonamento, posso non considerarlo
            if(productRatePlans.get(subscriptionRatePlan.Zuora__OriginalProductRatePlanId__c) != null){
                subscriptionsId.add(subscriptionRatePlan.Zuora__Subscription__c);
                subscriptionRatePlanAbbonamentoList.add(subscriptionRatePlan);
            }
        }
        Map<Id,Zuora__Subscription__c> subscriptions = new Map<Id,Zuora__Subscription__c>(
            [SELECT Id,Rate_Plan_Abbonamento__c FROM Zuora__Subscription__c WHERE Id IN :subscriptionsId]
        );

        for(Zuora__SubscriptionRatePlan__c subscriptionRatePlan : subscriptionRatePlanAbbonamentoList){
            Zuora__Subscription__c subscription = subscriptions.get(subscriptionRatePlan.Zuora__Subscription__c);
            zqu__ProductRatePlan__c productRatePlan = productRatePlans.get(subscriptionRatePlan.Zuora__OriginalProductRatePlanId__c);
            if(subscription != null && productRatePlan != null){
                if(productRatePlan.TipoProdotto__c == 'Abbonamento' || productRatePlan.TipoProdotto__c == 'Bundle Abbonamenti' || productRatePlan.TipoProdotto__c == 'Bundle Misti'){
                    subscription.Rate_Plan_Abbonamento__c = productRatePlan.Id;
                }
                subscriptionRatePlan.Original_Product_RatePlan__c = productRatePlan.Id;
            }else{
                subscriptionRatePlan.addError('Rate plan non associato alla subscription o al product rate plan.');
            }
        }
        update subscriptions.values();
    }
}