@isTest
private class ZTestBatchBonificaPVD {
	
	@isTest static void test_method_one() {

		Test.startTest();

		Testata__c tes = ZTest_Utils.createTestata('Test');
		insert tes;
		
		Prodotto_Vendita_Diretta__c pvd = new Prodotto_Vendita_Diretta__c();
		pvd.Name = 'Test PVD';
		pvd.Testata__c = tes.Id;
		insert pvd;

		Database.executeBatch(new BatchBonificaProdottoVenditaDiretta());

		Test.stopTest();

	}
	
}