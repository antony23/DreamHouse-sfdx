@isTest (SeeAllData = true)
private class CheckCfExtensionTest{

	@isTest
	static void testM(){
		Contact c = [SELECT Id,Codice_Nazione__c,Codice_Fiscale__c FROM Contact WHERE Codice_Nazione__c = 'IT' AND Codice_Fiscale__c != null LIMIT 1];
		CheckCfExtension ext = new CheckCfExtension(new ApexPages.StandardController(c));
		System.debug(loggingLevel.Error, '*** ext.c: ' + ext.c);
		System.debug(loggingLevel.Error, '*** ext.isValid: ' + ext.isValid);
		ext.checkCf();
	}
}