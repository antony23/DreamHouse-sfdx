public class WrapperErrorIBAN {

	public List<CommonWrapper.Errors> errors 			{get; set;}
	public CommonWrapper.Sepa_Data sepa_data 			{get; set;}
	public List<CommonWrapper.Validation> validations  	{get; set;}

}