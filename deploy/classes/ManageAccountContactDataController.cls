public class ManageAccountContactDataController {

    private static List<String> ACCOUNT_FIELDS;
    private static List<String> CONTACT_FIELDS;
    public boolean isAdminProfessional {get;set;}
    public boolean isProfessional {get;set;}    // DH-1293
    static{
        ACCOUNT_FIELDS = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Account').getDescribe().Fields.getMap().values()){
            Schema.DescribeFieldResult fd = ft.getDescribe();
            String fName = fd.getName();
            if(!fName.startsWith('Person') && !fName.endsWith('__pc') && fName != 'FirstName' && fName != 'LastName'){
                ACCOUNT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
            }
        }
    
        
        CONTACT_FIELDS = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
            CONTACT_FIELDS.add(ft.getDescribe().getName().toLowerCase());
        }
    }
    
    public String operationType {get;set;}
    private Id recordId;
    private String retURL;
    private String recordTypeId;

    public Boolean allowUserToInputData {get;set;}

    public Account account {get;set;}
    public Contact contact {get;set;}
    public Profile profile {get;set;}

    public Boolean isAccountB2BLarge{
        get{
            return account != null && account.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Large Enterprise').getRecordTypeId();
        }
    }
    public Boolean isAccountB2BSmall{
        get{
            return account != null && account.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2B Small Business').getRecordTypeId();
        }
    }
    public Boolean isAccountB2C{
        get{
            return account != null && account.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId();
        }
    }

    public string profileName {
        get {
             return [SELECT Profile.Name FROM User WHERE id = :UserInfo.getUserId()].Profile.Name;
        }
    }

    public String recordType{
        get{
            String recordTypeName = '';
            if(isAccountB2BLarge) recordTypeName = ' - B2B Large Enterprise';
            if(isAccountB2BSmall) recordTypeName = ' - B2B Small Enterprise';
            if(isAccountB2C) recordTypeName = ' - B2C';
            return recordTypeName;
        }
    }
    public Map<String,Boolean> isSectionAvailable{
        get{
            ManageAccountContactCustomLayout__c customPermissions = ManageAccountContactCustomLayout__c.getInstance();
            System.debug(loggingLevel.Error, '*** customPermissions : ' + customPermissions );
            Map<String,Boolean> sections = new Map<String,Boolean>{
                'Dati Azienda' => allowUserToInputData,
                'Struttura Societaria' => allowUserToInputData && (isAccountB2BLarge || isAccountB2BSmall) && customPermissions.Show_Struttura_Societaria__c,
                'Anagrafica Contatto Principale' => allowUserToInputData,
                'Indirizzo' => allowUserToInputData,
                'Codici Cliente' => allowUserToInputData,
                'Dati Privacy' => allowUserToInputData,
                'Dati Patente' => allowUserToInputData && customPermissions.Show_Dati_Patente__c,
                'Altri Dati' => allowUserToInputData
            };
            return sections;
        }
    }

    public ManageAccountContactDataController() {
        try {
            
            operationType = ApexPages.currentPage().getParameters().get('operationType');
            recordId = (Id) ApexPages.currentPage().getParameters().get('recordId');
            retURL = ApexPages.currentPage().getParameters().get('retURL');
            recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
            allowUserToInputData = true;
            Profile profile =  [SELECT ID, Name FROM Profile WHERE ID = :UserInfo.getProfileId()];
            String profilename = profile.name;
            string adminprof = 'Admin - Professional';
            string prof = 'Professional';


            if (profilename.contains(adminprof)) {
                isAdminProfessional = true;

            } else {
                isAdminProfessional = false;
            }

            /* DH-1293 */
            if (profilename.contains(prof)) {
                isProfessional = true;
            } else {
                isProfessional = false;
            }
            /* DH-1293 - Fine */

            if(String.isBlank(operationType))
                throw new DomusException('Link non valido. Segnalare il problema all\'amministratore di sistema.');

            if(operationType == 'Create'){
                if(profilename.contains(prof)){
                account = new Account(RecordTypeId = recordTypeId, Tipo_cliente__c = 'Professional');
                } else {
                account = new Account(RecordTypeId = recordTypeId, Tipo_cliente__c = 'Diffusione'); 
                }
                contact = new Contact(MainContact__c = true, Tipo_cliente__c = account.Tipo_Cliente__c);
            }else if(operationType == 'Update'){
                account = (Account) Database.query('SELECT ' + String.join(ACCOUNT_FIELDS, ',') + ' FROM Account WHERE Id = \'' + recordId + '\'');
                System.debug('******Edit: ' + account);
                
                List<Contact> contattiPrincipali = (List<Contact>) Database.query('SELECT ' + String.join(CONTACT_FIELDS, ',') + ' FROM Contact WHERE AccountId = \'' + recordId + '\' AND MainContact__c = true');
                
                if(!contattiPrincipali.isEmpty()){
                    contact = contattiPrincipali.get(0);
                }else{
                    // caso particolare: vado in aggiornamento di un cliente ma manca il referente principale associato, non può succedere ma va gestito cmq
                    contact = new Contact(AccountId = account.Id, MainContact__c = true);
                }
            }

            if(account == null || contact == null)
                throw new DomusException('Link non valido. Segnalare il problema all\'amministratore di sistema.');
            
        } catch(Exception e) {
            allowUserToInputData = false;
            appendErrorMessage(e);
        }
    }

    public PageReference saveAccountContact(){
        PageReference pr = null;

        try {
            
            System.debug(loggingLevel.Error, '*** account: ' + account);
            System.debug(loggingLevel.Error, '*** contact: ' + contact);

            // il codice fiscale va forzato solo se è stato inserito
            if(String.isBlank(account.Codice_Fiscale__c)) {
                account.Codice_Fiscale_Forzato__c = false;
            } else {
                account.Codice_Fiscale__c = account.Codice_Fiscale__c.toUpperCase();
            }

            // dal momento che account e contatto principale condividono alcuni campi, prima di salvarli li allineo
            allineaDatiAccountContatto(account,contact);
            
            // il controllo di deduplica l'ho già fatto in tempo reale quindi qui evito di rifarlo
            
            //*** CR Rimozione Trattino ***//
            //if(contact.FirstName == null ) contact.FirstName = '-';
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;

            AccountTriggerHandler.skip = true;

            Savepoint sp = !Test.isRunningTest() ? Database.setSavepoint() : null;

            // salvo l'account
            Database.SaveResult accSaveResult = account.Id == null ? 
                                        Database.insert(account,dml) : 
                                        Database.update(account,dml);
            
            // se non ci sono riuscito lancio un errore
            if(!accSaveResult.isSuccess()){
                Database.Error error = accSaveResult.getErrors()[0];
                throw new DomusException(error.getMessage());
            }

            // altrimenti proseguo e salvo il contatto
            contact.AccountId = accSaveResult.getId();  
            Database.SaveResult contSaveResult = contact.Id == null ? 
                                        Database.insert(contact,dml) : 
                                        Database.update(contact,dml);

            // se non ci sono riuscito lancio un errore (faccio rollback dell'inserimento dell'account)
            if(!contSaveResult.isSuccess()){
                Database.Error error = contSaveResult.getErrors()[0];
                if(!Test.isRunningTest()) Database.rollback(sp);
                throw new DomusException(error.getMessage());
            }

            // tutto ok, faccio redirect dell'utente sulla scheda dell'account
            pr = new PageReference('/' + accSaveResult.getId());
            //AccountTriggerHandler.skip = false;

        } catch(Exception e) {
            // se sono andato in errore qui è perché qualche campo è stato compilato male quindi informo l'utente ma gli permetto di modificare i dati di account e contatto
            allowUserToInputData = true;
            appendErrorMessage(e);
        }
        return pr;
    }

    private void allineaDatiAccountContatto(Account acc, Contact cont){
        if(acc.Id == null){
            acc.Stato__c = 'Prospect';
            acc.Type = 'Cliente';
        }

        cont.Codice_Fiscale__c = acc.Codice_Fiscale__c;
        cont.Codice_Fiscale_Forzato__c = acc.Codice_Fiscale_Forzato__c;
        cont.Partita_IVA__c = acc.Partita_IVA__c;
        cont.Email = acc.Email__c;
        cont.Phone = acc.Phone;
        cont.MobilePhone = acc.Cellulare__c;
        cont.MailingCity = acc.BillingCity;
        cont.MailingState = acc.BillingState;
        cont.MailingStreet = acc.BillingStreet;
        cont.MailingCountry = acc.BillingCountry;
        cont.MailingPostalCode = acc.BillingPostalCode;
        cont.Codice_Nazione__c = acc.Codice_Nazione__c;
        cont.Nazione__c = acc.Nazione__c;
        cont.Cee_Nazione__c = acc.Cee_Nazione__c;
        cont.Area_Nazione__c = acc.Area_Nazione__c;
        cont.Id_Area_Nazione__c = acc.Id_Area_Nazione__c;
        cont.Codice_Provincia__c = acc.Codice_Provincia__c;
        cont.Citta_abbreviato__c = acc.Citta_abbreviato__c;
        cont.Supplemento_CAP__c = acc.SupplementoCAP__c;
        cont.Dug__c = acc.Dug__c;
        cont.DugAbbreviataStd__c = acc.DugAbbreviataStd__c;
        cont.Via__c = acc.Via__c;
        cont.Civico__c = acc.Civico__c;
        cont.Supplemento_civico__c = acc.Supplemento_civico__c;
        cont.Indirizzo_forzato__c = acc.Indirizzo_forzato__c;
        cont.Via_Multicap__c = acc.Via_Multicap__c;
        cont.Presso__c = acc.Presso__c;
        cont.Frazione__c = acc.Frazione__c;
        cont.Codice_Esterno__c = acc.Codice_Esterno__c;
        cont.Codice_Press_Di__c = acc.Codice_Press_Di__c;
        cont.Codice_ACI__c = acc.Codice_ACI__c;
        cont.Birthdate = acc.Data_di_nascita__c;
        cont.Canale_di_Comunicazione_Preferito__c = acc.Canale_di_Comunicazione_Preferito__c;
        cont.Luogo_di_Nascita__c = acc.Luogo_di_Nascita__c;
        cont.Professione__c = acc.Professione__c;
        cont.PEC_Referente__c = acc.PEC__c;
        cont.Ruolo__c = acc.Ruolo__c;
        cont.Altra_email_referente__c = acc.Altra_email__c;
        acc.Sesso__c = cont.Sesso__c;
    }

    public PageReference back(){
        if(retURL == null) retURL = '/home/home.jsp';
        PageReference pr = new PageReference(retURL);
        return pr;
    }

    @RemoteAction
    public static Object verificaAccountDuplicato(String aId, String accountName, String codiceFiscale, String partitaIva, String email, String cellulare, String telefono, String via, String civico, String cap, String citta){
        AccountTriggerHandler.skip = true;

        Savepoint sp = !Test.isRunningTest() ? Database.setSavepoint() : null;
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        Set<Id> idAccountDuplicati = new Set<Id>();
        Id accountId = aId.length() > 0 ? aId : null;
        try {
            Account accountDaVerificare = new Account(
                Id = accountId,
                Name = accountName,
                Codice_Fiscale__c = codiceFiscale,
                Partita_IVA__c = partitaIva,
                Email__c = email,
                Cellulare__c = cellulare,
                Phone = telefono,
                Via__c = via,
                Civico__c = civico,
                BillingPostalCode = cap,
                BillingCity = citta
            );
            System.debug(loggingLevel.Error, '*** accountDaVerificare: ' + accountDaVerificare);
            System.debug(loggingLevel.Error, '*** accountId: ' + accountId);
            Database.UpsertResult fakeUpsertAccount = Database.upsert(accountDaVerificare, Account.Fields.Id, false);
            System.debug('************ skip *******' + AccountTriggerHandler.skip);
            System.debug(loggingLevel.Error, '*** fakeUpsertAccount: ' + fakeUpsertAccount);
            String errorMessage = '';
            if(!fakeUpsertAccount.isSuccess()){
                Database.Error error = fakeUpsertAccount.getErrors()[0];
                if (error instanceof Database.DuplicateError) {
                    Database.DuplicateError duplicateError = (Database.DuplicateError) error;
                    Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                    List<Datacloud.MatchResult> matchResults = duplicateResult.getMatchResults();
                    if(!matchResults.isEmpty()){
                        Datacloud.MatchResult matchResult = matchResults.get(0);
                        List<Datacloud.MatchRecord> matchRecords = matchResult.getMatchRecords();
                        for(Datacloud.MatchRecord matchRecord : matchRecords){
                            sObject accountDuplicato = matchRecord.getRecord();
                            Id accountIdDuplicato = (Id) accountDuplicato.get('Id');
                            if(accountIdDuplicato != accountId){
                                idAccountDuplicati.add(accountIdDuplicato);
                            }
                        }
                    }
                }else{
                    errorMessage = error.getMessage();
                }
            }
            if(!idAccountDuplicati.isEmpty()){
                gen.writeFieldName('accountDuplicati');
                gen.writeStartArray();
                for(Account acc : [select id,Name,Codice_Fiscale__c,Partita_IVA__c,Email__c,Cellulare__c,Phone,Via__c,Civico__c,BillingPostalCode,BillingCity,Indirizzo__c from Account where Id IN :idAccountDuplicati]){
                    gen.writeStartObject();
                    gen.writeStringField('Id', acc.Id);         
                    gen.writeStringField('Name', acc.Name);
                    gen.writeStringField('Indirizzo', acc.Indirizzo__c != null ? acc.Indirizzo__c : '');
                    gen.writeStringField('Codice_Fiscale__c', acc.Codice_Fiscale__c != null ? acc.Codice_Fiscale__c : '');
                    gen.writeStringField('Partita_IVA__c', acc.Partita_IVA__c != null ? acc.Partita_IVA__c : '');
                    gen.writeStringField('Email__c', acc.Email__c != null ? acc.Email__c : '');
                    gen.writeStringField('Cellulare__c', acc.Cellulare__c != null ? acc.Cellulare__c : '');
                    gen.writeStringField('Phone', acc.Phone != null ? acc.Phone : '');
                    gen.writeStringField('Via__c', acc.Via__c != null ? acc.Via__c : '');
                    gen.writeStringField('Civico__c', acc.Civico__c != null ? acc.Civico__c : '');
                    gen.writeStringField('BillingPostalCode', acc.BillingPostalCode != null ? acc.BillingPostalCode : '');
                    gen.writeStringField('BillingCity', acc.BillingCity != null ? acc.BillingCity : '');
                    gen.writeEndObject();
                }
                gen.writeEndArray();
            }
            gen.writeStringField('errore', errorMessage);
            gen.writeEndObject();
        } catch(Exception e) {
            System.debug(e.getMessage() + ' ' + e.getStackTraceString());
            gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('errore', e.getMessage());
            gen.writeFieldName('accountDuplicati');
            gen.writeStartArray();
            gen.writeEndArray();
            gen.writeEndObject();
        }
        if(!Test.isRunningTest()) Database.rollback(sp);
        AccountTriggerHandler.skip = false;
        System.debug('************ skip *******' + AccountTriggerHandler.skip);
        String jsonString = gen.getAsString();
        return JSON.deserializeUntyped(jsonString);
    }

    @RemoteAction
    public static Object validazionePartitaIva(String codiceNazione, String piva){
        Boolean partitaIvaValida = false;
        Boolean canSavePartitaIva = false;
        String ragioneSociale = null;
        String message = 'Partita IVA non valida';
        System.debug(loggingLevel.Error, '*** codiceNazione: ' + codiceNazione);
        System.debug(loggingLevel.Error, '*** piva: ' + piva);
        try{
            CheckVatService.checkVatPort service = new CheckVatService.checkVatPort();
            service.timeout_x = 10000;
            CheckVatServiceTypes.checkVatResponse_element response = service.checkVat(codiceNazione,piva);
            if(response.valid){
                partitaIvaValida = true;
                canSavePartitaIva = true;
                ragioneSociale = response.name;
                message = 'Partita IVA valida';
            }
        }catch(CalloutException e){
            partitaIvaValida = false;
            canSavePartitaIva = true;
            System.debug(e.getMessage() + '. ' + e.getStackTraceString());
            message = 'Controllo Partita IVA non disponibile';
        }catch(Exception e){
            System.debug(e.getMessage() + '. ' + e.getStackTraceString());
            partitaIvaValida = false;
            canSavePartitaIva = false;
            message = 'Controllo Partita IVA non disponibile. ' + e.getMessage();
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeBooleanField('partitaIvaValida', partitaIvaValida);
        gen.writeBooleanField('canSavePartitaIva', canSavePartitaIva);
        gen.writeStringField('ragioneSociale', ragioneSociale != null ? ragioneSociale : '');
        gen.writeStringField('message', message);
        gen.writeEndObject();
        String jsonResponse = gen.getAsString();
        return JSON.deserializeUntyped(jsonResponse);
    }

    @RemoteAction
    public static Object validazioneCodiceFiscale(String name, String surname, String sex, String birthDate, String birthCity, String codiceNazione, String codiceFiscale){
        Boolean codiceFiscaleValido = false;
        Boolean canSaveCodiceFiscale = false;
        Date dataDiNascita = null;
        String sesso = null;
        String message = 'Codice fiscale non valido';
        System.debug(loggingLevel.Error, '*** codiceNazione: ' + codiceNazione);
        System.debug(loggingLevel.Error, '*** codiceFiscale: ' + codiceFiscale);
        try{
            codiceNazione = codiceNazione.toUpperCase();
            codiceFiscale = codiceFiscale.toUpperCase();
            
			if(codiceNazione == 'IT'){
                try {
                    codiceFiscaleValido = UtilCodiceFiscale.validateCF(
                                                            name,
                                                            surname,
                                                            sex,
                                                            String.isNotBlank(birthDate) ? Date.valueOf(birthDate) : null,
                                                            birthCity,
                                                            codiceFiscale
                                                        );

                    if(codiceFiscaleValido){
                        message = 'Codice Fiscale valido';
                        //UtilCodiceFiscale.Person p = UtilCodiceFiscale.getPersonInfo(codiceFiscale);
                        //dataDiNascita = p.d;
                        //sesso = p.sex;
                    }else{
                        message = 'Codice Fiscale non valido';
                    }
                } catch(Exception e) {
                    codiceFiscaleValido = false;
                    message = e.getMessage();
                }

                canSaveCodiceFiscale = codiceFiscaleValido;
            }else{
                codiceFiscaleValido = false;
                canSaveCodiceFiscale = true;
                message = 'Codice fiscale non verificabile';
            }
        }catch(Exception e){
            System.debug(e.getMessage() + '. ' + e.getStackTraceString());
            codiceFiscaleValido = false;
            canSaveCodiceFiscale = false;
            message = 'Controllo Codice Fiscale al momento non disponibile. ' + e.getMessage();
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeBooleanField('codiceFiscaleValido', codiceFiscaleValido);
        gen.writeBooleanField('canSaveCodiceFiscale', canSaveCodiceFiscale);
        if(dataDiNascita != null){
            String year = String.valueOf(dataDiNascita.year());
            String month = String.valueOf(dataDiNascita.month());
            if(month.length() == 1){
                month = '0' + month;
            }
            String day = String.valueOf(dataDiNascita.day());
            if(day.length() == 1){
                day = '0' + day;
            }
            gen.writeStringField('dataDiNascita', year + '/' + month + '/' + day);
        }
        if(String.isNotBlank(sesso)){
            gen.writeStringField('sesso', sesso);
        }
        gen.writeStringField('message', message);
        gen.writeEndObject();
        String jsonResponse = gen.getAsString();
        return JSON.deserializeUntyped(jsonResponse);
   }

    public void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }

    
    // Cappario
    public String data {get;set;}

    @RemoteAction
    public static Object getResources(String resource)
    {
        System.debug(loggingLevel.Error, '*** JSON.deserializeUntyped(getData(resource)): ' + JSON.deserializeUntyped(getData(resource)));
        return JSON.deserializeUntyped(getData(resource));
    }

    public void getResources()
    {
        String resource = ApexPages.CurrentPage().getParameters().get('resource');
        String civico = ApexPages.currentPage().getParameters().get('civico');
        if(String.isNotBlank(civico)){
            resource += '&civico=' + civico;
        }
        data = getData(resource);
        System.debug(loggingLevel.Error, '*** data: ' + data);
    }

    public static String getData(String resource)
    {
        HttpRequest req = new HttpRequest();
        //ServiceEndpoint__c endpoint = ServiceEndpoint__c.getValues('AccountMock');
        //String url = endpoint.URL__c;
        if(resource.containsIgnoreCase('strade')){
            
            Integer startIndex = resource.indexOf('strade');
            Integer endIndex = resource.indexOf('?idComune=');
            String reqString = resource.substring(startIndex+7, endIndex);

            /*String encodedString = reqString.replace('%20', '');
            encodedString = EncodingUtil.urlEncode(encodedString, 'UTF-8');
            encodedString = encodedString.replace('+', '%20');*/
            String encodedString = reqString;

            //Se cambia l'url da cambiare bisogna rivalutare questa stringa
            System.debug(loggingLevel.Error, '*** resource: ' + resource);
            System.debug(loggingLevel.Error, '*** resource.substring(0, startIndex+7): ' + resource.substring(0, startIndex+7));
            System.debug(loggingLevel.Error, '*** encodedString: ' + encodedString);
            System.debug(loggingLevel.Error, '*** resource.substring(endIndex, resource.length()): ' + resource.substring(endIndex, resource.length()));
            System.debug(loggingLevel.Error, '*** endIndex: ' + endIndex);
            System.debug(loggingLevel.Error, '*** resource.length(): ' + resource.length());
            resource = resource.substring(0, startIndex+7)+encodedString+resource.substring(endIndex, resource.length());

        }

        String url = Setting__c.getInstance('DomusCap').URL__c+resource;
        req.setEndpoint(url);
        req.setTimeout(120000);
        req.setMethod('GET');
        System.debug(url);
        HTTP http = new Http();
        HttpResponse response = http.send(req);
        System.debug(response.getBody());
        return response.getBody();
    }

    @RemoteAction
    public static Map<String,Map<String,Object>> queryAccountContact(String accountId){
        Map<String,Map<String,Object>> result = new Map<String,Map<String,Object>>{
            'Account' => new Map<String,Object>(),
            'Contact' => new Map<String,Object>()
        };

        // recupero le informazioni di account e contatto
        sObject account = (sObject) Database.query('SELECT ' + String.join(ACCOUNT_FIELDS, ',') + ' FROM Account WHERE Id = \'' + accountId + '\'');
        List<sObject> contattiPrincipali = (List<sObject>) Database.query('SELECT ' + String.join(CONTACT_FIELDS, ',') + ' FROM Contact WHERE AccountId = \'' + accountId + '\' AND MainContact__c = true');
        sObject contattoPrincipale = contattiPrincipali.isEmpty() ? null : contattiPrincipali.get(0);
        for(String accountField : ACCOUNT_FIELDS){
            result.get('Account').put(accountField, account.get(accountField));
            System.debug(loggingLevel.Error, accountField + ' = ' + account.get(accountField));
        }

        for(String contactField : CONTACT_FIELDS){
            result.get('Contact').put(contactField, contattoPrincipale != null ? contattoPrincipale.get(contactField) : null);
        }
        result.put('additionalInfos', new Map<String,Object>{'isB2C' => result.get('Account').get('recordtypeid') == Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId()});
        System.debug(loggingLevel.Error, '*** result: ' + result);
        System.debug(loggingLevel.Error, '*** object: ' + result.get('Account').get('recordtypeid'));
        System.debug(loggingLevel.Error, '*** object: ' + Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId());
        return result;
    }

    @RemoteAction
    public static Boolean isEnhancedFiscalCodeValidationEnabled() {
        return UtilCodiceFiscale.isEnhancedFiscalCodeValidationEnabled();
    }
}