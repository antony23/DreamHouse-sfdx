@isTest
public class OrdineTriggerHandlerTest {
	
	@isTest static void testM(){
		AccountTriggerHandler.skip = true;
		
		Account a = new Account(Name = 'test', Stato__c = 'Prospect');
		insert a;

		Contact c = new Contact(FirstName = 'test', LastName = 'test', AccountId = a.Id);
		insert c;

		Ordine__c o = new Ordine__c(Name = 'test', Contatto_Committente__c = c.Id, Cliente_Committente__c = a.Id, Nr_Ordine__c = '1');
		insert o;

		Prodotto_Acquistato__c p = new Prodotto_Acquistato__c(Name = 'test', Cliente_Committente__c = a.Id);
		insert p;

		RigaOrdine__c r = new RigaOrdine__c(Name = 'test', Ordine__c = o.Id, Prodotto__c = 'Infocar Repair', Nr_Riga_d_ordine__c = '1', Prodotto_Acquistato__c = p.id, Fine_validit_ordine__c = Date.today());
		insert r;

		/*
		a = [select id, Stato__c, Data_Fine_Ultimo_Ordine__c from Account where id = :a.Id];
		System.assertEquals('Cliente', a.Stato__c);
		System.assertEquals(Date.today(), a.Data_Fine_Ultimo_Ordine__c);
		System.assert([select Ordine_con_Infocar_Repair__c from Ordine__c where id = :o.Id].Ordine_con_Infocar_Repair__c);
		*/
	}
	
}