public class EditQuoteExtension {

    public zqu__Quote__c quote {get;set;}    

    public EditQuoteExtension(ApexPages.StandardController controller) {
    	controller.addFields(new List<String>{'zqu__Status__c'});
        quote = (zqu__Quote__c) controller.getRecord();
    }

    public PageReference redirect(){
        PageReference p = null;
    	if(quote.zqu__Status__c == 'New'){
    		p = Page.WizardSelectAccount;
    	}else{
    		p = Page.ViewQuote;
    	}
        p.getParameters().put('Id',quote.Id);
        return p;
    }

}