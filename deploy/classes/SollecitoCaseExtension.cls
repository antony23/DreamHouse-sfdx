public with sharing class SollecitoCaseExtension {

    public String msg {get;private set;}
    
    public SollecitoCaseExtension(ApexPages.StandardController controller) {
        Case c = (Case) controller.getRecord();
        c.Numero_Solleciti__c = c.Numero_Solleciti__c == null ? 0 : c.Numero_Solleciti__c;

        if(c.Numero_Solleciti__c == 0){
            msg = 'Il caso non ha ancora ricevuto solleciti';
        }else{
            msg = 'Il caso ha già ricevuto ' + c.Numero_Solleciti__c + (c.Numero_Solleciti__c == 1 ? ' sollecito' : ' solleciti');
        }

        c.Numero_Solleciti__c++;
    }
    
}