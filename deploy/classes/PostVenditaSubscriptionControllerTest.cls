@isTest
private class PostVenditaSubscriptionControllerTest{
	
	@isTest
	static void resoVuoto(){
		Account a = new Account(Name = 'Test');
		AccountTriggerHandler.skip = true;
		insert a;

		Contact c = new Contact(FirstName = 'T', LastName = 'L', AccountId = a.Id);
		ContactTriggerHandler.skip = true;
		insert c;

		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c subscription = new Zuora__Subscription__c(Name = 'test');
		insert subscription;

		MovimentazioneStockTriggerHandler.skip = true;
		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Quantita__c = 5);
		insert mov;

		Asset venditaDiretta = new Asset(Name = 'Test', AccountId = a.Id, ContactId = c.Id, Quantity = 5, Subscription_Vendite_Dirette__c = subscription.Id, Copia_digitale__c = true);
		insert venditaDiretta;

		ApexPages.currentPage().getParameters().put('Id', subscription.Id);
		PostVenditaSubscriptionController ctrl = new PostVenditaSubscriptionController();

		ctrl.richiediReso();
	}

	@isTest
	static void resoDigitale(){
		Account a = new Account(Name = 'Test');
		AccountTriggerHandler.skip = true;
		insert a;

		Contact c = new Contact(FirstName = 'T', LastName = 'L', AccountId = a.Id);
		ContactTriggerHandler.skip = true;
		insert c;

		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c subscription = new Zuora__Subscription__c(Name = 'test', Zuora__SubscriptionEndDate__c = Date.today());
		insert subscription;

		SubscriptionProductChargeTriggerHandler.skipTrigger = true;
		//zqu__ProductRatePlanCharge__c prodottoDigitale = [SELECT Id FROM zqu__ProductRatePlanCharge__c WHERE TipoProdotto__c = 'Vendita Diretta' LIMIT 1];
		zqu__ProductRatePlanCharge__c prodottoDigitale = new zqu__ProductRatePlanCharge__c(Name = 'prodotto test', TipoProdotto__c = 'Vendita Diretta');
		insert prodottoDigitale;
		Zuora__SubscriptionProductCharge__c charge = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscription.Id, Product_Rate_Plan_Charge__c = prodottoDigitale.Id, Supporto__c = 'DIGITALE', Codice_SAP__c = '1');
		insert charge;

		MovimentazioneStockTriggerHandler.skip = true;
		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Quantita__c = 5);
		insert mov;

		Asset venditaDiretta = new Asset(Name = 'Test', AccountId = a.Id, ContactId = c.Id, Quantity = 5, Subscription_Vendite_Dirette__c = subscription.Id, Copia_digitale__c = true, Movimentazione_Stock__c = mov.Id);
		insert venditaDiretta;

		ApexPages.currentPage().getParameters().put('Id', subscription.Id);
		PostVenditaSubscriptionController ctrl = new PostVenditaSubscriptionController();

		List<PostVenditaSubscriptionController.AssetWrapper> prodotti = ctrl.prodotti;
		PostVenditaSubscriptionController.AssetWrapper prodotto = prodotti.get(0);
		prodotto.selected = true;
		prodotto.quantitaReso = 2;

		Entitlement__c ent = new Entitlement__c(Subscription__c = subscription.Id, Stato__c = 'Da inviare');
		insert ent;
		System.debug(loggingLevel.Error, '*** ctrl.prodottiDisponibiliPerReso: ' + ctrl.prodottiDisponibiliPerReso);
		ctrl.richiediReso();
		ctrl.reso();
	}

	@isTest
	static void resoCarta(){
		Account a = new Account(Name = 'Test');
		AccountTriggerHandler.skip = true;
		insert a;

		Contact c = new Contact(FirstName = 'T', LastName = 'L', AccountId = a.Id);
		ContactTriggerHandler.skip = true;
		insert c;

		SubscriptionTriggerHandler.skipTrigger = true;
		Zuora__Subscription__c subscription = new Zuora__Subscription__c(Name = 'test', Zuora__SubscriptionEndDate__c = Date.today());
		insert subscription;

		SubscriptionProductChargeTriggerHandler.skipTrigger = true;
		Zuora__SubscriptionProductCharge__c charge = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscription.Id);
		insert charge;

		MovimentazioneStockTriggerHandler.skip = true;
		Movimentazione_Stock__c mov = new Movimentazione_Stock__c(Quantita__c = 5);
		insert mov;

		Asset venditaDiretta = new Asset(Name = 'Test', AccountId = a.Id, ContactId = c.Id, Quantity = 5, Subscription_Vendite_Dirette__c = subscription.Id, Copia_digitale__c = false, Copia_cartacea__c = true, Movimentazione_Stock__c = mov.Id);
		insert venditaDiretta;

		ApexPages.currentPage().getParameters().put('Id', subscription.Id);
		PostVenditaSubscriptionController ctrl = new PostVenditaSubscriptionController();

		List<PostVenditaSubscriptionController.AssetWrapper> prodotti = ctrl.prodotti;
		PostVenditaSubscriptionController.AssetWrapper prodotto = prodotti.get(0);
		prodotto.selected = true;
		prodotto.quantitaReso = 2;
		System.debug(loggingLevel.Error, '*** ctrl.prodottiDisponibiliPerReso: ' + ctrl.prodottiDisponibiliPerReso);
		ctrl.richiediReso();
	}

}