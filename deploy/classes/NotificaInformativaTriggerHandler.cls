public class NotificaInformativaTriggerHandler {

	public static Boolean skipTrigger = false;

	/*
	public void onBeforeInsert(List<EmailMessage> triggerNew){}

	public void onBeforeUpdate(List<EmailMessage> triggerNew){}
	*/

	public void onAfterInsert(List<Notifica_Informativa__c> triggerNew){
		System.debug(LoggingLevel.INFO, '*_* NotificaInformativaTriggerHandler.onAfterInsert()');

		Map<Id, Id> mappaNotificheManuali = new Map<Id, Id>();
		Set<Notifica_Informativa__c> setNotifiche = new Set<Notifica_Informativa__c>();
		for(Notifica_Informativa__c notifica  : triggerNew){
			if(notifica.Stato__c == AppConstants.EMAIL_SENT && notifica.Invio_Manuale__c){
				mappaNotificheManuali.put(notifica.Id, notifica.Contatto__c);
				setNotifiche.add(notifica);
			}
		}

		if(!mappaNotificheManuali.isEmpty()){
			setFieldsOnAfterInsert(mappaNotificheManuali, setNotifiche);
		}
	}

	public void onAfterUpdate(Map<Id, Notifica_Informativa__c> triggerOldMap, Map<Id, Notifica_Informativa__c> triggerNewMap){
		System.debug(LoggingLevel.INFO, '*_* NotificaInformativaTriggerHandler.onAfterUpdate()');
		
		Map<Id, Id> mappaNotificheManuali = new Map<Id, Id>();
		Set<Notifica_Informativa__c> setNotifiche = new Set<Notifica_Informativa__c>();
		for(Id tempId : triggerNewMap.keySet()){
		    Notifica_Informativa__c newNotifica = triggerNewMap.get(tempId);
		    Notifica_Informativa__c oldNotifica = triggerOldMap.get(tempId);
		    if( (newNotifica.Stato__c == AppConstants.EMAIL_SENT && oldNotifica.Stato__c == AppConstants.EMAIL_DRAFT) && newNotifica.Invio_Manuale__c){
		    	mappaNotificheManuali.put(newNotifica.Id, newNotifica.Contatto__c);
		    	setNotifiche.add(newNotifica);
		    }
		}

		if(!mappaNotificheManuali.isEmpty()){
			setFieldsOnAfterUpdate(mappaNotificheManuali, setNotifiche);
		}
	}

	private void setFieldsOnAfterInsert(Map<Id, Id> mappaNotificheManuali, Set<Notifica_Informativa__c> setNotifiche){
		List<Database.SaveResult> saveResultList = insertEmailMessage(setNotifiche);
		updateTask(mappaNotificheManuali, saveResultList);

		Informativa_Privacy__c informativaCorrente = InformativaUtils.getInformativa();
		List<Contact> contattiDaAggiornare = [SELECT Id, Versione_Informativa__c, URL_Informativa__c FROM Contact WHERE Id IN : mappaNotificheManuali.values()];
		if(!contattiDaAggiornare.isEmpty() && informativaCorrente != null){
			InformativaUtils.setCampiInformativa(contattiDaAggiornare, informativaCorrente);
		}
	}

	private void setFieldsOnAfterUpdate(Map<Id, Id> mappaNotificheManuali, Set<Notifica_Informativa__c> setNotifiche){
		List<Database.SaveResult> saveResultList = insertEmailMessage(setNotifiche);
		updateTask(mappaNotificheManuali, saveResultList);
	}

	private List<Database.SaveResult> insertEmailMessage(Set<Notifica_Informativa__c> setNotifiche){
		List<EmailMessage> emailDaInserire = new List<EmailMessage>();
		for(Notifica_Informativa__c notifica : setNotifiche){
			EmailMessage email = new EmailMessage(
	            RelatedToId		= 	notifica.Id,
	            MessageDate		= 	notifica.Data_Invio__c,
	            Status 			= 	'3',
	            Subject			=	'Editoriale Domus – Informativa Privacy'
            );
			emailDaInserire.add(email);
		}

		return Database.insert(emailDaInserire);
	}

	private void updateTask(Map<Id, Id> mappaNotificheManuali, List<Database.SaveResult> saveResultList){
		Set<Id> emailInseriteId = new Set<Id>();
		for(Database.SaveResult sr : saveResultList){
			if(sr.isSuccess()){
				emailInseriteId.add(sr.getId());
			}
		}
	
		List<Task> taskInseriti = [SELECT Id, WhoId, WhatId FROM Task WHERE Id IN (SELECT ActivityId FROM EmailMessage WHERE Id IN :emailInseriteId)];

		for(Task t : taskInseriti){
			t.WhoId = mappaNotificheManuali.get(t.WhatId);
		}

		update taskInseriti;
	}
}