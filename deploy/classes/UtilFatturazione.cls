public class UtilFatturazione{


	public class FatturazioneResponse{
		public Mapping_Documento_Emesso__c mappingDocumento;
		public Boolean isError = false;
		public String errorsMsg;
	}

	public class ChargeInfo{
		String supporto;
		String codiceSap;

		public ChargeInfo(String s, String c){
			supporto = s;
			codiceSap = c;
		}
	}


	public static FatturazioneResponse getParametriFatturazioneSubscription(Id subscriptionId){
		FatturazioneResponse response = new FatturazioneResponse();

		String whereClause = 'Id = :subscriptionId';
        String additionalFields = 'Zuora__CustomerAccount__r.Sold_To_Salesforce__c, '+
                                    'Zuora__CustomerAccount__r.Bill_To_Salesforce__c, Zuora__CustomerAccount__r.Bill_To_Salesforce__r.AccountId,'+
                                    'Zuora__InvoiceOwner__r.Sold_To_Salesforce__c, Zuora__InvoiceOwner__r.Bill_To_Salesforce__c, '+
                                    'Zuora__InvoiceOwner__r.Bill_To_Salesforce__r.AccountId';
        String queryString = Utils.getSelectAllQuery('Zuora__Subscription__c', whereClause, null, additionalFields, true);
        List<Zuora__subscription__c> subList = (List<Zuora__Subscription__c>) Database.query(queryString);

        if(subList == null || subList.size() == 0){
        	response.isError = true;
        	response.errorsMsg = 'Non è stato trovato nessun abbonamento corrispondente all\'id: "'+subscriptionId+'"';
        	return response;
        }

        Zuora__Subscription__c sub = subList.get(0);

        String whereClause2 = 'Zuora__Subscription__c = :subscriptionId';
        String additionalFields2 = null;
        String queryString2 = Utils.getSelectAllQuery('Zuora__SubscriptionProductCharge__c', whereClause2, null, additionalFields2, false);
        List<Zuora__SubscriptionProductCharge__c> chargeList = (List<Zuora__SubscriptionProductCharge__c>) Database.query(queryString2);

        if(chargeList == null || chargeList.size() == 0){
        	response.isError = true;
        	response.errorsMsg = 'Non è stata trovata nessuna charge corrispondente all\'abbonamento: "'+subscriptionId+'"';
        	return response;
        }

        List<ChargeInfo> chargeInfoList = new List<ChargeInfo>();
        for(Zuora__SubscriptionProductCharge__c charge : chargeList){
        	ChargeInfo ci = new ChargeInfo(charge.Supporto__c, charge.Codice_SAP__c);
        	chargeInfoList.add(ci);
        }
        
        String tipoSupporti = getTipoSupporto(chargeInfoList);

        Id accountId, billToId, soldToId;
        if(sub.Zuora__InvoiceOwner__c != null && sub.Zuora__CustomerAccount__c != null &&
        	sub.Zuora__InvoiceOwner__c != sub.Zuora__CustomerAccount__c){
        	billToId = sub.Zuora__InvoiceOwner__r.Bill_To_Salesforce__c;
        	soldToId = sub.Zuora__CustomerAccount__r.Sold_To_Salesforce__c;
        	accountId = sub.Zuora__InvoiceOwner__r.Bill_To_Salesforce__r.AccountId;
    	}else {
    		billToId = sub.Zuora__CustomerAccount__r.Bill_To_Salesforce__c;
        	soldToId = sub.Zuora__CustomerAccount__r.Sold_To_Salesforce__c;
        	accountId = sub.Zuora__CustomerAccount__r.Bill_To_Salesforce__r.AccountId;
    	}

		String fatturazione = getContactCountry(billToId);
        String consegna = getContactCountry(soldToId);
		String tipoCliente = getTipoCliente(accountId);

		if(consegna == null){
        	response.isError = true;
			response.errorsMsg = 'Non è stato trovato il paese di fatturazione';
			return response;
        } 

		if(tipoCliente == null){
			response.isError = true;
			response.errorsMsg = 'Non è stato trovato il paese di spedizione';
			return response;
		}
		if(fatturazione == null){
			response.isError = true;
			response.errorsMsg = 'Non è stato trovato il tipo del cliente';
			return response;
		}


		return getDocumentoEmessoParameters(tipoSupporti, tipoCliente, fatturazione, consegna);
	}

	public static FatturazioneResponse getParametriFatturazioneQuote(Id quoteId){
		return getParametriFatturazioneQuote(quoteId, false);
	}


	public static FatturazioneResponse getParametriFatturazioneQuote(Id quoteId, Boolean skipBillingPreview){
		FatturazioneResponse response = new FatturazioneResponse();

		

		String whereClause = 'Id = :quoteId';
		String additionalFields = null; //zqu__SoldToContact__c, zqu__BillToContact__c
		String queryString = Utils.getSelectAllQuery('zqu__quote__c', whereClause, null, additionalFields, true);
		List<zqu__quote__c> quoteList = (List<zqu__quote__c>) Database.query(queryString);

        if(quoteList == null || quoteList.size() == 0){
        	response.isError = true;
        	response.errorsMsg = 'Non è stato trovata nessuna quote corrispondente all\'id: "'+quoteId+'"';
        	return response;
        }

        zqu__quote__c quote = quoteList.get(0);

        String whereClause2 = 'zqu__quoterateplan__r.zqu__quote__c = :quoteId AND Tipo_Charge__c IN (\'Abbonamento\',\'Vendita Diretta\')';
        String additionalFields2 = 'zqu__ProductRatePlanCharge__r.Supporto__c, Prodotto_Vendita_Diretta__r.Supporto__c';
        String queryString2 = Utils.getSelectAllQuery('zqu__QuoteRatePlanCharge__c', whereClause2, null, additionalFields2, false);
        List<zqu__QuoteRatePlanCharge__c> chargeList = (List<zqu__QuoteRatePlanCharge__c>) Database.query(queryString2);
        if(chargeList == null || chargeList.size() == 0){
        	response.isError = true;
        	response.errorsMsg = 'Non è stata trovata nessuna charge corrispondente all\'abbonamento: "'+quoteId+'"';
        	return response;
        }
       
        List<Zuora__SubscriptionProductCharge__c> subRatePlanChargeList;
        if(String.isNotBlank(quote.zqu__InvoiceOwnerId__c) && !skipBillingPreview){ //Fatturazione Intermediario
            subRatePlanChargeList = getSubscriptionCharges(quote.zqu__InvoiceOwnerId__c);
        } else if(String.isNotBlank(quote.zqu__ZuoraAccountID__c) && !skipBillingPreview)	{  //Billing account esistente
            subRatePlanChargeList = getSubscriptionCharges(quote.zqu__ZuoraAccountID__c);
        } else{ //Nuovo billing account
        	subRatePlanChargeList = new List<Zuora__SubscriptionProductCharge__c>();
        }

       
        List<ChargeInfo> chargeInfoList = new List<ChargeInfo>();
        for(zqu__QuoteRatePlanCharge__c charge : chargeList){
        	String supporto;
        	if(charge.Prodotto_Vendita_Diretta__c != null){
        		supporto = charge.Prodotto_Vendita_Diretta__r.Supporto__c;
        	}else{
        		supporto = charge.zqu__ProductRatePlanCharge__r.Supporto__c;
        	}
        	ChargeInfo ci = new ChargeInfo(supporto, charge.Codice_Prodotto_SAP__c);
        	chargeInfoList.add(ci);
        }

        System.debug(loggingLevel.Error, '*** chargeInfoList: ' + chargeInfoList);
        
		for(Zuora__SubscriptionProductCharge__c c : subRatePlanChargeList){
        	ChargeInfo ci = new ChargeInfo(c.Product_Rate_Plan_Charge__r.Supporto__c, c.Codice_SAP__c);
        	chargeInfoList.add(ci);
        }

        System.debug(loggingLevel.Error, '*** chargeInfoList: ' + chargeInfoList);

        String tipoSupporti = getTipoSupporto(chargeInfoList);

        System.debug(loggingLevel.Error, '*** tipoSupporti: ' + tipoSupporti);

        String billToContact;
        String soldToContact;
        Id accountId;
        System.debug('***************** zqu__ZuoraAccountID__c' + quote.zqu__ZuoraAccountID__c);
        System.debug('*****************quote.zqu__InvoiceOwnerId__c' + quote.zqu__InvoiceOwnerId__c);
        if(quote.zqu__ZuoraAccountID__c != null || quote.zqu__InvoiceOwnerId__c != null){
        	String zuoraBillToAccId;
        	String zuoraSoldToAccId;
        	if(quote.zqu__InvoiceOwnerId__c != null && quote.zqu__ZuoraAccountID__c != null 
        			&& quote.zqu__InvoiceOwnerId__c!= quote.zqu__ZuoraAccountID__c){
				zuoraBillToAccId = quote.zqu__InvoiceOwnerId__c;
				zuoraSoldToAccId = quote.zqu__ZuoraAccountID__c;
        	}else if(quote.zqu__ZuoraAccountID__c != null){
        		zuoraBillToAccId = quote.zqu__ZuoraAccountID__c;
        		zuoraSoldToAccId = quote.zqu__ZuoraAccountID__c;
        	}else if(quote.zqu__InvoiceOwnerId__c != null){
        		zuoraBillToAccId = quote.zqu__InvoiceOwnerId__c;
        		soldToContact = quote.zqu__SoldToContact__c;
        		System.debug('********************soldToContact****************' + quote.zqu__SoldToContact__c);
        	}
        	//String zuoraAccId = quote.zqu__ZuoraAccountID__c;

	        List<Zuora__CustomerAccount__c> billToAccount = [SELECT Id, Bill_To_Salesforce__c, Sold_To_Salesforce__c, Bill_To_Salesforce__r.AccountId
			        	FROM Zuora__CustomerAccount__c
			        	WHERE Zuora__Zuora_Id__c  = :zuoraBillToAccId];
    	 	List<Zuora__CustomerAccount__c> soldToAccount; 

    	 	if (!String.isBlank(zuoraSoldToAccId)) {
    	 			soldToAccount  = [SELECT Id, Bill_To_Salesforce__c, Sold_To_Salesforce__c, Bill_To_Salesforce__r.AccountId
			        				 FROM Zuora__CustomerAccount__c
			        				 WHERE Zuora__Zuora_Id__c  = :zuoraSoldToAccId]; 
			}


			if(((soldToAccount == null || soldToAccount.size() == 0) && soldToContact ==null) || billToAccount == null || billToAccount.size() == 0){
				response.isError = true;
				response.errorsMsg = 'Non è stato trovato nessun account corrispondente all\'Id Zuora: "'+zuoraSoldToAccId+'" e/o: "' +zuoraBillToAccId+
				'"';
				return response;
			}

			accountId = billToAccount.get(0).Bill_To_Salesforce__r.AccountId;
			billToContact = billToAccount.get(0).Bill_To_Salesforce__c;
			System.debug('*************** billToContact ************' + billToContact);
			if(soldToContact == null) {soldToContact = soldToAccount.get(0).Sold_To_Salesforce__c;}
			System.debug('********************soldToContact****************' + soldToContact);
    	}else{
    		billToContact = quote.zqu__BillToContact__c;
    		soldToContact = quote.zqu__SoldToContact__c;
    		accountId = quote.zqu__Account__c;
    	}

        String tipoCliente = getTipoCliente(accountId);
        String fatturazione = getContactCountry(billToContact);
        String consegna = getContactCountry(soldToContact);
        System.debug('*****************getContactCountry(soldToContact)*****************' + consegna);
        System.debug('*****************getContactCountry(billToContact)*****************' + fatturazione);

        if(consegna == null){
        	response.isError = true;
			response.errorsMsg = 'Non è stato trovato il paese di fatturazione';
			return response;
        } 

		if(tipoCliente == null){
			response.isError = true;
			response.errorsMsg = 'Non è stato trovato il paese di spedizione';
			return response;
		}
		if(fatturazione == null){
			response.isError = true;
			response.errorsMsg = 'Non è stato trovato il tipo del cliente';
			return response;
		}


       	return getDocumentoEmessoParameters(tipoSupporti, tipoCliente, fatturazione, consegna);
	}

	public static FatturazioneResponse getDocumentoEmessoParameters(String tipoSupporti, String tipoCliente, String fatturazione, String consegna){
		FatturazioneResponse response = new FatturazioneResponse();
		if(fatturazione == 'EXTRA CEE') fatturazione = 'EXTRA_CEE';
		if(consegna == 'EXTRA CEE') consegna = 'EXTRA_CEE';
		System.debug(
			'\ntipoSupporti = ' + tipoSupporti +
			'\ntipoCliente = ' + tipoCliente +
			'\nfatturazione = ' + fatturazione +
			'\nconsegna = ' + consegna + '\n'
		);

		List<Mapping_Documento_Emesso__c> listaMapping = [SELECT Id, Name, Documento_Emesso__c, Tipologia_Num_Doc_Emesso__c, Tipologia_Num_Nota_Credito__c FROM Mapping_Documento_Emesso__c 
				        		WHERE Supporto__c = :tipoSupporti AND Tipologia_Cliente__c INCLUDES (:tipoCliente)
				        		AND  Fatturazione__c INCLUDES (:fatturazione) AND Consegna__c INCLUDES (:consegna)];
		if(listaMapping == null || listaMapping.size() == 0){
			response.isError = true;
        	response.errorsMsg = 'Non è stato trovato nessun mapping corrispondente ai parametri di input';
			return response;
		} else{
			response.mappingDocumento = listaMapping.get(0);
			if(listaMapping.size()>1){
				response.isError = true;
				response.errorsMsg = 'E\' stata trovata più di una corrispondenza dai parametri di input. In output è stata ritornata la prima corrispondenza trovata';
			}
			return response;
		}

	}

	public static String getContactCountry(Id contactId){
		Contact c = [SELECT Id, Codice_Nazione__c, Cee_Nazione__c FROM Contact WHERE Id = :contactId LIMIT 1];
		System.debug('***************** getContactCountry************' + c.cee_nazione__c);
		if (c == null){
			return null;
		}

		if(c.Codice_Nazione__c == 'IT'){
			return 'Italia';
		}else if(c.Codice_Nazione__c ==  'VA' || c.Codice_Nazione__c == 'SM'){
			return 'San Marino / Vaticano';
		}else{
			return c.Cee_Nazione__c;
		}
	}

	public static String getTipoSupporto( List<ChargeInfo> chargeInfoList ){
		Set<String> tipoSupportiSet = new Set<String>();
		if(chargeInfoList != null && chargeInfoList.size() >0 ){
			for(ChargeInfo c : chargeInfoList){
				if(c.supporto != null){
					if((c.supporto == 'Carta + Digitale') || (c.supporto == 'Cartaceo') || (c.supporto == 'Altro')){
						c.supporto = 'Carta';
					}
					tipoSupportiSet.add(c.supporto);
				}
			}
		}
		List<String> tipoSupportiSetOrdered = new List<String>(tipoSupportiSet);
		tipoSupportiSetOrdered.sort();
		System.debug('tipoSupportiSetOrdered: ' + tipoSupportiSetOrdered);
		return String.join(tipoSupportiSetOrdered, '; ');
	}

	public static String getTipoCliente(String accountId){
		Account acc = [SELECT RecordType.DeveloperName, Codice_Pubblica_Amministrazione__c FROM Account WHERE Id = :accountId LIMIT 1];
		if(acc == null){
			return null;
		}
		if(acc.Codice_Pubblica_Amministrazione__c != null && acc.Codice_Pubblica_Amministrazione__c.length() == 6){	//DH-1264
			return 'Pubblica Amministrazione';
		}

        String recordType = acc.RecordType.DeveloperName;
    	if(recordType.containsIgnoreCase('B2C')){
            return 'B2C';
        }else if (recordType.containsIgnoreCase('B2B')){
            return 'B2B';
        }
    	return null;

	}



	public static List<Zuora__SubscriptionProductCharge__c> getSubscriptionCharges(String accountId){
		Zuora.zApi zApiInstance = new Zuora.zApi();
		Zuora.zApi.LoginResult loginResult = zApiInstance.zlogin();

		ZuoraStubMethods.SOAP zuora = new ZuoraStubMethods.SOAP();
		zuora.endPoint_x = loginResult.ServerUrl;
		zuora.SessionHeader = new ZuoraStubMethods.SessionHeader_element();
		zuora.SessionHeader.session = LoginResult.Session;

		ZuoraStubMethods.BillingPreviewRequest request = new ZuoraStubMethods.BillingPreviewRequest();

		request.AccountId = accountId;
		request.TargetDate = Date.today().addYears(1);
		request.ChargeTypeToExclude = '';

		ZuoraStubMethods.BillingPreviewResult[] results = zuora.billingPreview(new ZuoraStubMethods.BillingPreviewRequest[]{request});
		List<String> chargeZuoraIdList  = new List<String>();

		for(ZuoraStubMethods.BillingPreviewResult r : results){
			System.debug(r);
		    if(r!= null && r.InvoiceItem != null && r.success){
		        for(ZuoraStubObjects.InvoiceItem ii : r.InvoiceItem){
		        	System.debug(ii);
		        	if(ii!=null){
		            	chargeZuoraIdList.add(ii.RatePlanChargeId);
			            System.Debug(
			                'RatePlanChargeId:' + ii.RatePlanChargeId
						);
		        	}
		        }            
		    }
		}		

		List<Zuora__SubscriptionProductCharge__c> subChargeList = [SELECT Id, Product_Rate_Plan_Charge__c, 
																	Product_Rate_Plan_Charge__r.Supporto__c, Codice_SAP__c
																	FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Zuora_Id__c = :chargeZuoraIdList];
		return subChargeList;
	}

}