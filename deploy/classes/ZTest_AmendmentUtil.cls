@isTest(SeeAllData=True)
public class ZTest_AmendmentUtil {
	
    @isTest
    public static void testDeleteAmendments() {
        AmendmentUtil.deleteAmendments(new List<String>{'amendmentZuoraId1', 'amendmentZuoraId2', 'amendmentZuoraId3'});
    }

    @isTest
    public static void testCreateSuspendSubscriptionAmendment() {
        final Zuora__Subscription__c sub = createSubscription();

        AmendmentUtil.createSuspendSubscriptionAmendment(sub, 'Test', Date.newInstance(2019, 9, 1));
    }

    @isTest
    public static void testCreateResumeSubscriptionAmendment() {
        final Zuora__Subscription__c sub = createSubscription();

        AmendmentUtil.createResumeSubscriptionAmendment(sub, 'Test', Date.newInstance(2019, 9, 1), Date.newInstance(2019, 10, 1));
    }

    @isTest
    public static void testCreateTermsAndConditionsAmendment() {
        final Zuora__Subscription__c sub = createSubscription();

        AmendmentUtil.createTermsAndConditionsAmendment(sub, 'Test', Date.newInstance(2019, 9, 1), Date.newInstance(2019, 10, 1));
    }
    
    @isTest
    public static void testSuspend() {
        final Zuora__Subscription__c sub = createSubscription();

        AmendmentUtil.suspend(sub, Date.newInstance(2019, 9, 1), Date.newInstance(2019, 10, 1), true, 'Sospensione');
    }

    @isTest
    public static void testRetrieveAmendments() {
        final Zuora__Subscription__c sub = createSubscription();

        AmendmentUtil.retrieveAmendments(sub);
    }

    private static Zuora__Subscription__c createSubscription() {
        final Zuora__Subscription__c sub = new Zuora__Subscription__c();
        sub.Zuora__TermSettingType__c = 'TERMED';
        sub.Zuora__TermStartDate__c = Date.newInstance(2019, 9, 1);
        sub.Zuora__External_Id__c = '2c92c0f86cccaa55016cd2501fbf333c000';
        sub.Zuora__RenewalTerm__c = '48 Weeks';
        sub.Zuora__SubscriptionEndDate__c = Date.newInstance(2022, 1, 1);
        sub.Zuora__AutoRenew__c = false;
        sub.Zuora__RenewalTermPeriodType__c = 'Week';
        
        SubscriptionTriggerHandler.skipTrigger = true;
        insert sub;
        SubscriptionTriggerHandler.skipTrigger = false;

        return sub;
    }
}