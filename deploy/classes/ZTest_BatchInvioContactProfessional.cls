@isTest
private class ZTest_BatchInvioContactProfessional {
	@isTest static void test_method_one() {

		Test.startTest();
		
		String query = 'SELECT Id, Invia_anagrafica_a_sap__c FROM Contact WHERE FirstName = \'Test\' LIMIT 1';

		Database.executeBatch(new BatchInvioContactProfessional(query));

		Test.stopTest();

	}

	@testSetup
	public static void testSetup() {
		
		Account a = new Account(Name = 'Test Account');
		insert a;
	
		Contact c = new Contact(
			AccountId = a.Id,
			MainContact__c = true,
			FirstName = 'Test',
			LastName = 'Contact',
			MailingPostalCode = '20092', 
			Invia_anagrafica_a_sap__c = false
		);
		insert c;
	}
}