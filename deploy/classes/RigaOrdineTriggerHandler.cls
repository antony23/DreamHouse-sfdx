public class RigaOrdineTriggerHandler {
	public static Boolean skip = false;

	public void onBeforeInsert(List<RigaOrdine__c> triggerNew, Map<Id, RigaOrdine__c> triggerNewMap) { 
		//setFields(triggerNew);
    }
    
    public void onAfterInsert(List<RigaOrdine__c> triggerNew, Map<Id, RigaOrdine__c> triggerNewMap) {
        alignAll(triggerNew);
    }

    public void onBeforeUpdate(List<RigaOrdine__c> triggerOld, List<RigaOrdine__c> triggerNew, Map<Id, RigaOrdine__c> triggerOldMap, Map<Id, RigaOrdine__c> triggerNewMap){   
        //setFields(triggerNew);
    }

    public void onAfterUpdate(List<RigaOrdine__c> triggerOld, List<RigaOrdine__c> triggerNew, Map<Id, RigaOrdine__c> triggerOldMap, Map<Id, RigaOrdine__c> triggerNewMap){   
        alignAll(triggerNew);
    }

    public void onBeforeDelete(List<RigaOrdine__c> triggerOld, Map<Id, RigaOrdine__c> triggerOldMap){  

    }

    public void onAfterDelete(List<RigaOrdine__c> triggerOld, Map<Id, RigaOrdine__c> triggerOldMap){  
        alignAll(triggerOld);
    }

    // @Deprecated: Sostituito da checkProduct(Ordine__c, Map<Id, RigaOrdine__c>, Set<Id>)
    public static void setFields(List<RigaOrdine__c> triggerNew){
    	Set<Id> ordiniInfocareRepair = new Set<Id>();
		for(RigaOrdine__c riga : triggerNew){
			if(riga.Prodotto__c == 'INFOCAR REPAIR' || riga.Prodotto__c == ' INFOCAR REPAIR PACK'){
				ordiniInfocareRepair.add(riga.Ordine__c);
			}
		}
		List<Ordine__c> ordini = [select id,Ordine_con_Infocar_Repair__c
									from Ordine__c
									where id in : ordiniInfocareRepair];
		
        for(Ordine__c o : ordini){
        	o.Ordine_con_Infocar_Repair__c = true;
        }
        OrdineTriggerHandler.skip = true;
        update ordini;
        OrdineTriggerHandler.skip = false;	
    }

    /**
     *  Allinea le informazioni presenti sugli oggetti Contact e Ordine__c associati alle righe d'ordine.
    **/

    private static void alignAll(List<RigaOrdine__c> righeOrdine) {
        System.debug('*_* alignAll *_*');

        final Set<Id> ordineIds = fetchOrdineIds(righeOrdine);
        final List<Ordine__c> ordini = [SELECT Id, Contatto_Committente__c FROM Ordine__c WHERE Id IN :ordineIds];

        final Map<Id, Contact> mapContacts = new Map<Id, Contact>([SELECT Id, Prodotto__c FROM Contact WHERE Id IN :fetchContactIds(ordini)]);
        final Map<Id, Ordine__c> mapOrdini = new Map<Id, Ordine__c>([SELECT Id, Ordine_con_Infocar_Repair__c, Contatto_Committente__c, Prodotto__c FROM Ordine__c WHERE Contatto_Committente__c IN :mapContacts.values()]);
        final Map<Id, RigaOrdine__c> mapRigheOrdine = new Map<Id, RigaOrdine__c>([SELECT Id, Ordine__c, Ordine__r.Contatto_Committente__c, Prodotto__c FROM RigaOrdine__c WHERE Ordine__c IN :mapOrdini.values()]);
        final Map<Id, Map<Id, Set<Id>>> mapTree = mapAll(mapContacts.values(), mapOrdini.values(), mapRigheOrdine.values());

        final List<Contact> contactsToUpdate = new List<Contact>();
        final List<Ordine__c> ordiniToUpdate = new List<Ordine__c>();

        System.debug('>> Contatti: ' + mapContacts.keySet());
        System.debug('>> Ordini: ' + mapOrdini.keySet());
        System.debug('>> Righe d\'ordine: ' + mapRigheOrdine.keySet());
        System.debug('>> Tree: ' + mapTree);
        
        for(Id contactId : mapTree.keySet()) {
            final Contact contact = mapContacts.get(contactId);
            final String[] cProducts = new String[]{};
           
            if(contact != null) {
                System.debug('>> Contact[' +  contact.Id + '].Prodotto__c.old = \'' + contact.Prodotto__c + '\'');
            }

            for(Id ordineId : mapTree.get(contactId).keySet()) {
                final Ordine__c ordine = mapOrdini.get(ordineId);
                if(ordine == null) continue;

                System.debug('>> Ordine[' +  ordine.Id + '].Prodotto__c.old = \'' + ordine.Prodotto__c + '\'');

                final String[] oProducts = new String[]{};

                for(Id rigaOrdineId : mapTree.get(contactId).get(ordineId)) {
                    final RigaOrdine__c rigaOrdine = mapRigheOrdine.get(rigaOrdineId);
                    if(rigaOrdine == null) continue;

                    populateProdottoField(cProducts, oProducts, rigaOrdine);
                }

                checkProduct(ordine, mapRigheOrdine, mapTree.get(contactId).get(ordineId));

                if(!sameProducts(ordine.Prodotto__c, oProducts)) {
                oProducts.sort();
                ordine.Prodotto__c = String.join(oProducts, ';');
                    ordiniToUpdate.add(ordine);
                }

                System.debug('>> Ordine[' +  ordine.Id + '].Prodotto__c.new = \'' + ordine.Prodotto__c + '\'');
            }

            if(contact != null) {
                if(!sameProducts(contact.Prodotto__c, cProducts)) {
                cProducts.sort();
                contact.Prodotto__c = String.join(cProducts, ';');
                    contactsToUpdate.add(contact);
                }

                System.debug('>> Contact[' +  contact.Id + '].Prodotto__c.new = \'' + contact.Prodotto__c + '\'');
            }
        }

        System.debug('>> Ordini To Update: ' + ordiniToUpdate.size());
        System.debug('>> Contact To Update: ' + contactsToUpdate.size());

        OrdineTriggerHandler.skip = true;
        update ordiniToUpdate;
        OrdineTriggerHandler.skip = false;

        ContactTriggerHandler.skip = true;
        update contactsToUpdate;
        ContactTriggerHandler.skip = false;
    }

    /**
     *  Popola il campo Prodotto__c sugli oggetti Contact ed Ordine__c analizzando le righe d'ordine associate.
    **/

    private static void populateProdottoField(String[] cProducts, String[] oProducts, RigaOrdine__c rigaOrdine) {
        System.debug('*_* populateProdottoField *_*');

        final Integer cpIndex = indexOfIgnoreCase(cProducts, rigaOrdine.Prodotto__c);
        final Integer opIndex = indexOfIgnoreCase(oProducts, rigaOrdine.Prodotto__c);

        if(cpIndex < 0) {   // Prodotto non trovato sul contatto
            cProducts.add(rigaOrdine.Prodotto__c);
        }

        if(opIndex < 0) {   // Prodotto non trovato sull'ordine
            oProducts.add(rigaOrdine.Prodotto__c);
        }
    }

    /**
     *  Verifica il campo Prodotto sulle righe d'ordine ed aggiorna di conseguenza alcuni flag sul relativo Ordine
    **/

    private static void checkProduct(Ordine__c ordine, Map<Id, RigaOrdine__c> mapRigheOrdine, Set<Id> setRigheOrdineId) {
        System.debug('*_* checkProduct *_*');
        System.debug('>> Ordine[' +  ordine.Id + '].Ordine_con_Infocar_Repair__c.old = \'' + ordine.Ordine_con_Infocar_Repair__c + '\'');

        ordine.Ordine_con_Infocar_Repair__c = false;

        for(Id rigaOrdineId : setRigheOrdineId) {
            final RigaOrdine__c rigaOrdine = mapRigheOrdine.get(rigaOrdineId);
            if(rigaOrdine == null) continue;

            if(rigaOrdine.Prodotto__c == 'INFOCAR REPAIR' || rigaOrdine.Prodotto__c == ' INFOCAR REPAIR PACK'){
                ordine.Ordine_con_Infocar_Repair__c = true;
                break;
            }
        }

        System.debug('>> Ordine[' +  ordine.Id + '].Ordine_con_Infocar_Repair__c.new = \'' + ordine.Ordine_con_Infocar_Repair__c + '\'');
    }

    /**
     *  Mappa le righe d'ordine con i relativi ordini e gli ordini con i relativi committenti.
    **/

    private static Map<Id, Map<Id, Set<Id>>> mapAll(List<Contact> contatti, List<Ordine__c> ordini, List<RigaOrdine__c> righeOrdine) {
        System.debug('*_* mapAll *_*');

        Map<Id, Map<Id, Set<Id>>> rocMap = new Map<Id, Map<Id, Set<Id>>>();

        // Mi assicuro che i contatti siano mappati anche in presenza di ordini e/o righe d'ordine cancellati
        for(Contact c : contatti) {
            final Id contactId = c.Id;

            if(!rocMap.containsKey(contactId)) {
                rocMap.put(contactId, new Map<Id, Set<Id>>());
            }
        }

        // Mi assicuro che gli ordini siano mappati anche in presenza di righe d'ordine cancellate
        for(Ordine__c o : ordini) {
            final Id contactId = o.Contatto_Committente__c;
            final Id ordineId = o.Id;

            if(!rocMap.containsKey(contactId)) {
                rocMap.put(contactId, new Map<Id, Set<Id>>());
            }

            if(!rocMap.get(contactId).containsKey(ordineId)) {
                rocMap.get(contactId).put(ordineId, new Set<Id>());
            }
        }

        // Mappo le righe d'ordine
        for(RigaOrdine__c ro : righeOrdine) {
            final Id contactId = ro.Ordine__r.Contatto_Committente__c;
            final Id ordineId = ro.Ordine__c;
            
            if(!rocMap.containsKey(contactId)) {
                rocMap.put(contactId, new Map<Id, Set<Id>>());
            }

            if(!rocMap.get(contactId).containsKey(ordineId)) {
                rocMap.get(contactId).put(ordineId, new Set<Id>());
            }

            if(!rocMap.get(contactId).get(ordineId).contains(ro.Id)) {
                rocMap.get(contactId).get(ordineId).add(ro.Id);
            }
        }

        return rocMap;
    }

    /**
     *  Versione case-insensitive del metodo String.indexOf(String).
    **/

    private static Integer indexOfIgnoreCase(String[] products, String product) {
        System.debug('*_* indexOfIgnoreCase *_*');

        for(Integer i = 0; i < products.size(); i++) {
            if(products.get(i).equalsIgnoreCase(product)) {
                return i;
            }
        }

        return -1;
    }

    private static Boolean sameProducts(String products, String[] newProducts) {
        final String[] oldProducts = String.isNotBlank(products) ? products.split(';') : new String[]{};
        if(oldProducts.size() != newProducts.size()) return false;

        oldProducts.sort();
        newProducts.sort();
        Boolean found;

        for(String op: oldProducts) {
            found = false;

            for(String np: newProducts) {
                if(np.equalsIgnoreCase(op)) {
                    found = true;
                    break;
                }
            }

            if(!found) return false;
        }

        return true;
    }

    private static Set<Id> fetchOrdineIds(List<RigaOrdine__c> righeOrdine) {
        System.debug('*_* fetchOrdineIds *_*');

        Set<Id> ids = new Set<Id>();

        for(RigaOrdine__c riga : righeOrdine) {
            ids.add(riga.Ordine__c);
        }

        return ids;
    }

    private static Set<Id> fetchContactIds(List<Ordine__c> ordini) {
        System.debug('*_* fetchContactIds *_*');

        Set<Id> ids = new Set<Id>();

        for(Ordine__c ordine : ordini) {
            ids.add(ordine.Contatto_Committente__c);
        }

        return ids;
    }
}