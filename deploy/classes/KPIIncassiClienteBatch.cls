global class KPIIncassiClienteBatch implements Database.Batchable<sObject>,Schedulable{
    
	// Schedulable
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new KPIIncassiClienteBatch(),100);
    }

    // Batch
    String query;
	
	global KPIIncassiClienteBatch() {
		query = 'SELECT Id,Pagatore__c,Totale__c FROM Zuora__Subscription__c WHERE createddate >= LAST_N_MONTHS:24 ORDER BY Pagatore__c ASC';
		if(Test.isRunningTest()) query += ' LIMIT 100';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Account> accountsKPI = new List<Account>();
		for(AggregateResult totaleAccount : [SELECT Pagatore__c, SUM(Totale__c) totaleSubscriptions
											FROM Zuora__Subscription__c
											WHERE Id IN :scope
											GROUP BY Pagatore__c]){
			System.debug(loggingLevel.Error, '*** totaleAccount: ' + totaleAccount);
			if(totaleAccount.get('Pagatore__c') != null){
				accountsKPI.add(new Account( 
					Id = (Id) totaleAccount.get('Pagatore__c'), 
					Valore_Complessivo_Cliente__c = (Decimal) totaleAccount.get('totaleSubscriptions')
				));
			}
		}

        AccountTriggerHandler.skip = true;
        update accountsKPI;
        AccountTriggerHandler.skip = true;

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}