public class EditContactCopieSubExtension {

    public Zuora__Subscription__c subscription {get;set;}
    public List<WrapAsset> wrapAssetList {get;set;}
    public Contact selectedContact {get;set;}

    public EditContactCopieSubExtension(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()) controller.addFields(new List<String>{'Zuora__CustomerAccount__r.Sold_To_Salesforce__c', 'Zuora__Account__c','Motivo_modifica_temporanea_indirizzo__c'});

        subscription = (Zuora__Subscription__c) controller.getRecord();
        wrapAssetList = new List<WrapAsset>();
        subscription.Contatto_Principale_Committente__c = subscription.Zuora__CustomerAccount__r.Sold_To_Salesforce__c;
        setContact(subscription.Contatto_Principale_Committente__c);
        caricaCopie();
       

    }


    public void caricaCopie(){
        try{
            List<Asset> assetList = [SELECT Id, Name, ContactId, Inviata__c,Stato__c, Indirizzo__c, Dettagli_Cliente__c,
                                        Citt__c, Provincia__c, Codice_Postale__c, Nazione__c
                                    FROM Asset
                                    WHERE SubscriptionAllCopy__c = :subscription.Id];
            wrapAssetList = createWrapAssetList(assetList);
        } catch(Exception e){
            appendErrorMessage(e);
        }
    }

    public void caricaInfoContatto(){
        try{
            System.debug(loggingLevel.Error, '*** carica info contatto: ');
           setContact(subscription.Contatto_Principale_Committente__c);
        }catch (Exception e){
            appendErrorMessage(e);
        }
    }

    public PageReference goToSub(){
        PageReference pr = new PageReference('/'+subscription.Id);
        return pr;

    }

    public PageReference saveNewContact(){
        try{
            if(subscription.Motivo_modifica_temporanea_indirizzo__c == null)
                throw new DomusException('Specificare il motivo della modifica temporanea indirizzo prima di procedere.');
            List<Asset> updatedAsset = new List<Asset>();
            List<String> copieModificateList = new List<String>();

            for(WrapAsset wa : wrapAssetList){
                if(wa.modificaContatto){
                    Asset a = wa.asset;
                    a.ContactId = selectedContact.Id;
                    
                    updatedAsset.add(a);
                    copieModificateList.add(a.Name);
                }
                wa.modificaContatto = false;
            }

            List<Contact> contactUpdateList = new List<Contact>();
            contactUpdateList.add(selectedContact);
            ContactUtil.updateAssets(contactUpdateList, updatedAsset);

            selectedContact.Periodo_Ultima_Modifca_Temp_Indirizzo__c = String.join(copieModificateList, '\n');
            update selectedContact;

            /*Case c = new Case(
                OwnerId = UserInfo.getUserId(),
                Subscription__c = subscription.Id,
                Origin = 'Creazione automatica',
                Subject = 'Modifica dati anagrafici',
                Description = 'Modifica temporanea indirizzo. Motivo: ' + subscription.Motivo_modifica_temporanea_indirizzo__c,
                Status = 'Chiuso',
                AccountId = selectedContact.AccountId,
                ContactId = selectedContact.Id,
                Macro_Categoria__c = 'B2C',
                Categoria__c = 'MODIFICHE ANAGRAFICHE',
                Sottocategoria__c = 'Modifica Dati Anagrafici',
                Caseautomatico__c = true
            );
            insert c;*/
        }
        catch (Exception e){
            appendErrorMessage(e);
        }
        return null;
    }


    public class WrapAsset{
        public Asset asset {get;set;}
        public Boolean modificaContatto {get;set;}

        public WrapAsset(Asset a){
            asset = a;
            modificaContatto = false;
        }

    }

    private List<WrapAsset> createWrapAssetList(list<Asset> assetList){
        List<WrapAsset> wrapAssetList = new List<WrapAsset>();
        for(Asset a : assetList){
            wrapAssetList.add(new WrapAsset(a));
        }
        return wrapAssetList;
    }

    private void setContact(Id contactId){
        /*List<Contact> contactList = [SELECT Id, Name, MailingStreet, MailingPostalCode, Id_Area_Nazione__c, MailingCountry,
                                            MailingCity, MailingState, Area_Nazione__c, Presso__c, AccountId
                                    FROM Contact
                                    WHERE Id = :contactId];
        if(contactList != null && contactList.size()>0){
            System.debug(loggingLevel.Error, '*** contactList: ' + contactList);
            selectedContact = contactList.get(0);
        }
        */
        List<string> contactFields = new List<string>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
            contactFields.add(ft.getDescribe().getName().toLowerCase());
        }
        List<Contact> contactList = (List<Contact>) Database.query(
                'SELECT ' + String.join(contactFields, ',') + 
                ' FROM Contact ' +
                ' WHERE Id = : contactId ');
        if(contactList.size()>0){
            System.debug(loggingLevel.Error, '*** contactList: ' + contactList);
            selectedContact = contactList.get(0);
        }
    }

    public void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
}