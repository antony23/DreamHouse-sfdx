public class QuoteAbbonamentoController {

    public List<zqu__QuoteRatePlan__c> subscriptionList {get;set;}
    public List<zqu__QuoteRatePlanCharge__c> quoteChargeList {get;set;}
    public String selectedTestata {get;set;}
    public String selectedSupport {get;set;}
    public String selectedIssue {get;set;}
    public String searchText {get;set;}

    public List<zqu__ProductRatePlanCharge__c> chargeList {get;set;}
    public zqu__ProductRatePlanCharge__c selectedCharge {get;set;}
    public String choosenSubscription{get;set;}
    public String selectedIndex {get;set;}
    public Boolean isSaveDisabled {get;set;}

    public Boolean displayDetailSection {get;set;}

    private String recordType;

    public String selectedCalendar {get;set;}
    public Integer quantita {get;set;}
    public String updatedQuoteRatePlanId {get;set;}
    public Integer updatedQuantita {get;set;}
    public String deleteQuoteId {get;set;}
    public Boolean editMode {get;set;}
    public Boolean isFilterSection {get;set;}
    public Boolean isRatePlanSection {get;set;}
    public Boolean isDetailSection {get;set;}
    public Boolean isCalendarSection {get;set;}

    public Boolean isRatePlanError {get;set;}
    public Boolean isCalendarError {get;set;}
    public Boolean isSaveError{get;set;}

    public List<Zuora__Subscription__c> previousSubscriptionList {get;set;}
    public Id targetSubscription {get;set;}

    public zqu__ProductRatePlanCharge__c charge {get;set;}

    public zqu.quote quote {get;set{
            quote = value;
            zqu__Quote__c quoteObj = quote.getSObject();
            quoteId = quoteObj.id;
            
            quoteChargeList = DomusUtil.getChargeAbbonamenti(quoteId);
            Account acc = [SELECT RecordType.DeveloperName FROM Account WHERE Id = :quoteObj.zqu__Account__c LIMIT 1];
            recordType = acc.RecordType.DeveloperName;
        }
    }


    public Id quoteId {get;set;}

    public List<SelectOption> productOptionList {get;set;}
    public List<SelectOption> ratePlanOptionList {get;set;}
    public List<SelectOption> calendarOptionList {get;set;}

    private final List<String> productRatePlanChargeFields = new List<String>{
        'Id','Name','zqu__AccountingCode2__c','zqu__AccountingCode__c','zqu__Apply_Discount_To_One_Time_Charges__c','zqu__Apply_Discount_To_Recurring_Charges__c','zqu__Apply_Discount_To_Usage_Charges__c','zqu__BillCycleDay__c','zqu__BillCycleType__c','zqu__BillingPeriodAlignment__c','zqu__Catalog_Sync_History__c','zqu__DefaultQuantity__c','zqu__DeferredRevenueAccount__c','zqu__Deleted__c','zqu__Description__c','zqu__Discount_Apply_Type__c','zqu__Discount_Level__c','zqu__EndDateCondition__c','zqu__IncludedUnits__c','zqu__ListPriceBase__c','zqu__ListPrice__c','zqu__MaxQuantity__c','zqu__MinQuantity__c','zqu__Model__c','zqu__NumberOfPeriod__c','zqu__OverageCalculationOption__c','zqu__OverageUnusedUnitsCreditOption__c','zqu__PrepaymentPeriods__c','zqu__PriceChangeOption__c','zqu__PriceIncreasePercentage__c','zqu__PriceTable__c','zqu__ProductRatePlanChargeFullName__c','zqu__ProductRatePlanChargeName__c','zqu__ProductRatePlan__c','zqu__RecognizedRevenueAccount__c','zqu__RecurringPeriod__c','zqu__RevRecCode2__c','zqu__RevRecCode__c','zqu__RevRecTriggerCondition__c','zqu__RevenueRecognitionRuleName__c','zqu__SmoothingModel__c','zqu__SyncMessage__c','zqu__SyncStatus__c','zqu__TaxCode2__c','zqu__TaxCode__c','zqu__TaxMode__c','zqu__Taxable__c','zqu__TriggerEvent__c','zqu__Type__c','zqu__UOM__c','zqu__UpToPeriodsType__c','zqu__Upto_How_Many_Periods__c','zqu__UsageRecordRatingOption__c','zqu__UseDiscountSpecificAccountingCode__c','zqu__UseTenantDefaultForPriceChange__c','zqu__ZUnitOfMeasure__c','zqu__ZUom_Id__c','zqu__ZuoraId__c','zqu__EntityID__c','IssueCalendar__c','Uscite__c','CodiceSAP__c','DescrizioneProdottoSAP__c','TipoProdotto__c','zqu__WeeklyBillCycleDay__c','CopiePeriododiLatenza__c','NumeroDevicesConsentiti__c','NumeroUtenze__c','GruppodiContabilizzazioneCliente__c','Supporto__c','Testata__c','PrezzodiListino__c','SpesediSpedizione__c','AccessoArchivioDigitale__c','CopiediGracing__c','Gracing__c','Prezzo_di_Copertina__c','Sconto__c','PianodeiContilegatoaGrContCliente__c','zqu__SpecificBillingPeriod__c','Tier_Price__c','SkipSpesediSpedizione__c','Intermediario__c','Busine__c','Entitlment__c',
        'zqu__ProductRatePlan__r.zqu__Product__r.Testata__c','zqu__ProductRatePlan__r.zqu__Product__r.Name','zqu__ProductRatePlan__r.Name','zqu__ProductRatePlan__r.Supporto__c','zqu__ProductRatePlan__r.ConAvvisi__c'
    };
   
    public List<SelectOption> testataOptions {
        get{
            testataOptions = new List<SelectOption>();
            Set<String> testateList = new Set<String>();

            zqu__Quote__c quoteObj = quote.getSObject();
            if(quoteObj.Subscription_Precedente__c != null){
                Id subId = quoteObj.Subscription_Precedente__c;
                List<Zuora__SubscriptionProductCharge__c> chargeList = [SELECT Id, Testata__c FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Subscription__c = :subId AND Tipo_Prodotto__c = 'Abbonamento'];
                
                for(Zuora__SubscriptionProductCharge__c charge : chargeList){
                    testateList.add(charge.Testata__c.toLowerCase());
                }
            }else{
                testataOptions.add(new SelectOption('', 'Seleziona testata'));
            }

            Schema.DescribeFieldResult supportFields = Product2.Testata__c.getDescribe();
            List<Schema.PicklistEntry> supportEntries = supportFields.getPicklistValues();
            for( Schema.PicklistEntry f : supportEntries)   {
                System.debug(f.getLabel()+' - '+f.getValue());
                if(testateList == null || testateList.size() == 0){
                    testataOptions.add(new SelectOption(f.getLabel(), f.getValue()));
                }else{
                    if(testateList.contains(f.getLabel().toLowerCase())){
                        testataOptions.add(new SelectOption(f.getLabel(), f.getValue()));
                    }
                }
            } 
            testataOptions.sort();
            return testataOptions;
        }
        set;
    }

    public List<SelectOption> supportOptions {
        get{
            supportOptions = new List<SelectOption>();
            supportOptions.add(new SelectOption('', 'Seleziona supporto'));
            Schema.DescribeFieldResult supportFields = zqu__ProductRatePlanCharge__c.Supporto__c.getDescribe();
            List<Schema.PicklistEntry> supportEntries = supportFields.getPicklistValues();
            for( Schema.PicklistEntry f : supportEntries)   {
                supportOptions.add(new SelectOption(f.getLabel(), f.getValue()));
            } 
            supportOptions.sort();
            return supportOptions;
        }
        set;
    }

    public List<SelectOption> issueOptions {
        get{
            issueOptions = new List<SelectOption>();
            issueOptions.add(new SelectOption('', 'Seleziona numero uscite'));
            Schema.DescribeFieldResult supportFields = zqu__ProductRatePlanCharge__c.Uscite__c.getDescribe();
            List<Schema.PicklistEntry> supportEntries = supportFields.getPicklistValues();
            for( Schema.PicklistEntry f : supportEntries)   {
                issueOptions.add(new SelectOption(f.getLabel(), f.getValue()));
            } 
            return issueOptions;
        }
        set;
    }

    public QuoteAbbonamentoController(){
        isFilterSection = false;
        displayDetailSection = false;
        resetErrorMessages();
    }

    public void search(){
        try{
            System.debug('Ricerca');
            if(!String.isBlank(selectedTestata)|| !String.isBlank(selectedSupport) || !String.isBlank(selectedIssue) || !String.isBlank(searchText)) {

                Date searchDate = Date.today();

                zqu__Quote__c quoteObj = quote.getSObject();
                if(quoteObj.Subscription_Precedente__c != null){
                    Id subId = quoteObj.Subscription_Precedente__c;
                    Zuora__Subscription__c sub = [SELECT Id, Zuora__SubscriptionEndDate__c FROM Zuora__Subscription__c WHERE Id = :subId LIMIT 1];
                    if(sub != null && sub.Zuora__SubscriptionEndDate__c != null){
                        searchDate = sub.Zuora__SubscriptionEndDate__c;
                    }
                }
                if(searchDate == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Data di ricerca non impostata correttamente'));
                    return;
                }
                System.debug('RECORD TYPE: ' +recordType); 
                String whereClause = 'zqu__productrateplan__r.TipoProdotto__c = \'Abbonamento\' ' +
                                   'AND TipoProdotto__c = \'Abbonamento\' ' +
                                   'AND zqu__productrateplan__r.VisibilitSistemi__c LIKE \'%SFDC%\' ' +
                                   'AND zqu__productrateplan__r.zqu__EffectiveStartDate__c <= :searchDate ' +
                                   'AND zqu__productrateplan__r.zqu__EffectiveEndDate__c >= :searchDate ' +
                                   'AND zqu__productrateplan__r.TipoCliente__c LIKE \'%'+recordType+'%\' ' ;
                if(quoteObj.Intermediario__c != null){
                    Account intermediario;
                    List<Account> intermediarioList = [SELECT Id,Name,Codice_Convenzione__c FROM Account WHERE Id = :quoteObj.Intermediario__c];
                    if(intermediarioList != null && intermediarioList.size()>0){
                        intermediario = intermediarioList.get(0);
                    }
                    whereClause += 'AND zqu__productrateplan__r.Intermediario__c = \''+intermediario.Codice_Convenzione__c +'\' ';
                }else{
                    whereClause += 'AND (zqu__productrateplan__r.Intermediario__c = null OR zqu__productrateplan__r.Intermediario__c = \'-\') ';
                }
                if(!String.isBlank(selectedTestata)){
                    whereClause += 'AND zqu__productrateplan__r.zqu__Product__r.Testata__c = :selectedTestata ';
                }

                if(!String.isBlank(selectedSupport)){
                    whereClause += 'AND Supporto__c = \''+selectedSupport+'\' ';
                }

                if(!String.isBlank(searchText)){
                    whereClause += 'AND (zqu__ProductRatePlan__r.Name LIKE \'%'+searchText+'%\' OR zqu__ProductRatePlan__r.SKURatePlan__c LIKE \'%'+searchText+'%\' ) ';
                }

                List<Id> ratePlanListId;
                if(!String.isBlank(selectedIssue)){
                    whereClause += 'AND Uscite__c = :selectedIssue ';
                }

                System.debug('***Query: '+ whereClause);

                chargeList = (List<zqu__ProductRatePlanCharge__c>) Database.query('SELECT ' + String.join(productRatePlanChargeFields,',') + ' FROM zqu__ProductRatePlanCharge__c WHERE ' + whereClause);
                if(chargeList == null || chargeList.size()==0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Nessun abbonamento trovato'));    
                }

            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Seleziona almeno un filtro'));
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }

    }

    private void resetErrorMessages(){
        isRatePlanError = false;
        isCalendarError = false;
        isSaveError = false;
    }

    public void updateCharge(){
        try{
            List<zqu__QuoteRatePlanCharge__c> chargeList = [SELECT zqu__Period__c, zqu__SpecificBillingPeriod__c, zqu__EffectivePrice__c, 
                                                            zqu__QuoteRatePlan__r.zqu__ProductRatePlan__c
                                                            FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c= :updatedQuoteRatePlanId];

            Set<Id> productRatePlanIdSet = new Set<Id>();

            for(zqu__QuoteRatePlanCharge__c charge : chargeList){
                charge.zqu__Quantity__c = updatedQuantita;
                charge.zqu__Total__c = charge.zqu__Quantity__c * charge.zqu__EffectivePrice__c;
                productRatePlanIdSet.add(charge.zqu__QuoteRatePlan__r.zqu__ProductRatePlan__c);
            }
            update chargeList;


            List<zqu__QuoteRatePlan__c> quoteRatePlanList = [SELECT Id, Campaign__c FROM zqu__QuoteRatePlan__c WHERE Campaign__r.Rate_Plan_in_Campagna__c = :productRatePlanIdSet];
            Set<Id> campaignIdSet = new Set<Id>();
            for(zqu__QuoteRatePlan__c ratePlan : quoteRatePlanList){
                campaignIdSet.add(ratePlan.Campaign__c);
            }

            Map<Id, Campaign> campaignMap = new Map<Id, Campaign>([SELECT Id, Name, Sconto_Fisso__c, Prodotto_Vendita_Diretta__c,
                                                Sconto_Percentuale__c, Omaggio__c
                                                FROM Campaign
                                                WHERE Id = :campaignIdSet]);
            for(zqu__QuoteRatePlan__c qrp : quoteRatePlanList){
                if(campaignMap.get(qrp.Campaign__c) != null){
                    DomusUtil.updateCampaignCharge(quoteId, qrp.Id, campaignMap.get(qrp.Campaign__c));
                }
            }

        } catch(Exception e) {
            System.debug(loggingLevel.Error, '*** errore: ' + e.getMessage() + '. ' + e.getStackTraceString());
            appendErrorMessage(e);
        }
    }

    public PageReference goToCart(){
        System.debug('Redirecting to cart');
        return Page.WizardCart;
    }


    public void removeSubscription(){
        try{
            if(!String.isBlank(deleteQuoteId)){

                List<zqu__QuoteRatePlan__c> quoteRPList = [SELECT zqu__ProductRatePlan__c FROM zqu__QuoteRatePlan__c WHERE Id = :deleteQuoteId];
                List<Id> ratePlanIdList = new List<Id>();
                if(quoteRPList != null && quoteRPList != null){
                    for(zqu__QuoteRatePlan__c qrp : quoteRPList){
                        ratePlanIdList.add(qrp.zqu__ProductRatePlan__c);
                    }
                    List<Campaign> campaignList = [SELECT Id FROM Campaign WHERE Rate_Plan_in_Campagna__c = :ratePlanIdList];
                    List<Id> campaignIdList = new List<Id>();
                    if(campaignList != null && campaignList.size() > 0){
                        for(Campaign c : campaignList){
                            campaignIdList.add(c.Id);
                        }
                        List<zqu__QuoteRatePlan__c> quoteCampaignList = [SELECT Id FROM zqu__QuoteRatePlan__c WHERE Campaign__c  = :campaignIdList];
                        List<Id> quoteCampaignRatePlanId = new List<Id>();
                        if(quoteCampaignList != null && quoteCampaignList.size() > 0){
                            for(zqu__QuoteRatePlan__c q : quoteCampaignList){
                                quoteCampaignRatePlanId.add(q.Id);
                            }
                            DomusUtil.removeRatePlanList(quoteId, quoteCampaignRatePlanId);
                        }
                    }
                }

                DomusUtil.removeRatePlan(quoteId, deleteQuoteId);

                deleteQuoteId = null;

                zqu__Quote__c quoteObj = quote.getSObject();
                quoteObj.zqu__StartDate__c = Date.today();
                quoteObj.zqu__Service_Activation_Date__c = null;
                quoteObj.zqu__ValidUntil__c  = Date.today();
                quoteObj.Testate__c = '';
                quoteObj.Supporto__c = '';
                quoteObj.ConAvvisi__c = null;
                quoteObj.zqu__InitialTerm__c = 1;
                quoteObj.zqu__RenewalTerm__c = 1;
                quoteObj.Calendario_Inizio_Text__c = '';
                quoteObj.Calendario_Fine_Text__c = '';
                if(quoteObj.Subscription_Precedente_Text__c == null){
                    quoteObj.Numero_di_Cicli_Text__c = '1';
                }
                quote.save();
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void selectSubscription(){
        try{            
            isSaveDisabled = false;
            if(!String.isBlank(selectedIndex)){
                Integer index = Integer.valueOf(selectedIndex);
                selectedCharge = chargeList.get(index);
                quantita = 1;
                calendarOptionList = new List<SelectOption>();

                List<Testata__c> testataList = DomusUtil.getTestataList(selectedCharge.zqu__ProductRatePlan__c);
                List<Id> idTestataList = new List<Id>();
                for(Testata__c t : testataList){
                    idTestataList.add(t.id);
                }

                zqu__Quote__c quoteObj = quote.getSObject();
                if(quoteObj.Subscription_Precedente__c != null){
                    Id subId = quoteObj.Subscription_Precedente__c;
                    Zuora__Subscription__c sub = [SELECT Id, Zuora__SubscriptionEndDate__c FROM Zuora__Subscription__c WHERE Id = :subId LIMIT 1];
                    if(sub != null && sub.Zuora__SubscriptionEndDate__c != null){
                        Calendario__c c = DomusUtil.getCalendarioStartCopy(idTestataList, sub.Zuora__SubscriptionEndDate__c);
                        if(c!=null){
                            calendarOptionList.add(new SelectOption(c.id, c.Name));
                            displayDetailSection = true;
                        } else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Nessun calendario per l\'abbonamento selezionato'));
                        }
                    } else{
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Nessun abbonamento corrispondente all\'Id: "'+subId+'"'));
                    }

                    
                }else{
                    List<Calendario__c> calendarioList = DomusUtil.getMultiCalendarioList(idTestataList);

                    if(calendarioList != null && calendarioList.size()>0){
                        isCalendarSection = true;
                        isSaveDisabled = false;
                        //calendarOptionList.add(new SelectOption('','- Seleziona una copia -'));
                        for (Calendario__c c : calendarioList){
                            calendarOptionList.add(new SelectOption(c.id, c.Name));
                            System.debug(loggingLevel.Error, '*** c: ' + c);
                            System.debug(loggingLevel.Error, '*** String.isBlank(selectedCalendar): ' + String.isBlank(selectedCalendar));
                            System.debug(loggingLevel.Error, '*** c.Data_Uscita_Effettiva__c: ' + c.Data_Uscita_Effettiva__c);

                            if(String.isBlank(selectedCalendar) && ((c.Data_Uscita_Effettiva__c != null && c.Data_Uscita_Effettiva__c > Date.today()) || c.Data_Uscita_Attesa__c > Date.today())){
                                System.debug(loggingLevel.Error, '*** c.Id: ' + c.Id);
                                selectedCalendar = c.id;
                            }
                        }
                        displayDetailSection = true;
                    }else{
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Nessun calendario per l\'abbonamento selezionato'));    
                    }

                    Id accountId = quoteObj.zqu__Account__c;
                    String testata = selectedCharge.Testata__c;

                    String whereClause = '(Testate__c LIKE \'' + testata + '\' OR Testate__c LIKE \'%;' + testata + '\' OR Testate__c LIKE \'' + testata + ';%\') AND Zuora__Account__c = :accountId AND Zuora__Status__c IN (\'Active\',\'Pending Activation\')';
                    //String whereClause = 'Testate__c LIKE \'%'+ testata +'%\' AND Zuora__Account__c = :accountId AND Zuora__Status__c IN (\'Active\',\'Pending Activation\')';
                    String additionalFields = 'Zuora__CustomerAccount__r.Sold_To_Salesforce__c,Zuora__CustomerAccount__r.Sold_To_Salesforce__r.ContactAddressCO__c, Zuora__CustomerAccount__r.Bill_To_Salesforce__r.ContactAddressCO__c, Zuora__CustomerAccount__r.Bill_To_Salesforce__c,'+
                                            'Rate_Plan_Abbonamento__r.Name';
                    String queryString = Utils.getSelectAllQuery('Zuora__Subscription__c', whereClause, null, additionalFields, true);
                    previousSubscriptionList = (List<Zuora__Subscription__c>) Database.query(queryString);
                
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void deleteSubList(){
        previousSubscriptionList = null;
    }

    public PageReference goToSubscription(){
        zqu__Quote__c quoteObj = quote.getSObject();
        quoteObj.Quote_Status__c = 'Cancelled';
        quote.save();
        System.debug('TargetSub: ' +targetSubscription);
        PageReference ref = new PageReference('/'+targetSubscription);
        return ref;
    }
    public void checkCalendar(){
        try{
            if(!String.isBlank(selectedCalendar)){
                isSaveDisabled = false;
            }
            else{
                isSaveDisabled = true;
            }   
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void cancel(){
        try{
            calendarOptionList = new List<SelectOption>();
            selectedCalendar = null;
            quantita = 1;
            displayDetailSection = false;
        }
        catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public PageReference save(){
        try{
            resetErrorMessages();
            List<zqu__QuoteRatePlanCharge__c> abbChargeList = DomusUtil.getChargeAbbonamenti(quoteId);
            if(abbChargeList != null && abbChargeList.size() > 0){
                appendErrorMessage('Impossibile aggiungere altri abbonamenti');
                return null;
            }          
            //if(!String.isBlank(quoteId) && !String.isBlank(selectedProduct) && !String.isBlank(selectedRatePlan) && !String.isBlank(selectedCalendar)){
            if(selectedCharge != null && !String.isBlank(selectedCalendar)){
            //if(!String.isBlank(quoteId) && !String.isBlank(selectedTestata) && !String.isBlank(selectedRatePlan) && !String.isBlank(selectedCalendar)){
                System.debug(quoteId);
                String selectedRatePlan = selectedCharge.zqu__ProductRatePlan__c;
                System.debug(loggingLevel.Error, '*** Salvo l\'abbonamento. Il calendario che salvo è: ' + selectedCalendar);
                Id quoteRatePlanId =  DomusUtil.setAbbonamento(quoteId, selectedRatePlan, selectedCalendar);

                if(quoteRatePlanId != null){
                  
                    List<zqu__QuoteRatePlanCharge__c> chargeList = [SELECT zqu__Period__c, Supporto__c,
                                         zqu__SpecificBillingPeriod__c, zqu__EffectivePrice__c, zqu__Quantity__c, 
                                         zqu__Total__c, Tipo_Charge__c, zqu__ProductRatePlanCharge__r.Supporto__c,
                                         zqu__ProductRatePlanCharge__r.Uscite__c,NumeroUscite__c
                                         FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c= :quoteRatePlanId];
                    System.debug('chargeList : ' + chargeList);
                    for(zqu__QuoteRatePlanCharge__c charge : chargeList){
                        if(charge.Tipo_Charge__c == 'Abbonamento'){
                            charge.zqu__Quantity__c = quantita;
                            charge.zqu__Total__c = charge.zqu__Quantity__c * charge.zqu__EffectivePrice__c;
                            charge.Supporto__c = charge.zqu__ProductRatePlanCharge__r.Supporto__c;
                            charge.NumeroUscite__c = charge.zqu__ProductRatePlanCharge__r.Uscite__c;
                        }
                    }
                    update chargeList;
                }
                return goToCart();
            }else{
                isSaveError = true;
                return null;
            }
        } catch(Exception e) {
            appendErrorMessage(e);
            return null;
        }
    }

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }
}