global class Batch_Delete_Object implements Database.Batchable<sObject>, Schedulable {
	
	String queryString;

	global Batch_Delete_Object(String query){
		this.queryString = query;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}

	global void execute(SchedulableContext SC) {
		Database.executeBatch(new Batch_Delete_Object(queryString));
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}

	global void finish(Database.BatchableContext BC) {
		
	}
	
}