@isTest (SeeAllData = true)
private class CtrlAddressValidationTest{

	@isTest
	static void testM1(){
		String resource = 'strade/tortona?idComune=1772&q=tortona';
		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":[{"id":246,"idSestiere":0,"idComune":"1772","DugStd":"VIA","DugComplStd":null,"NomeStd":"TORTONA","Quartiere":null,"DugApiciStd":"VIA","DugComplApiciStd":null,"NomeApiciStd":"TORTONA","QuartiereApici":null,"DenomAbbrevStd":"VIA TORTONA","MultiCap":"N","CAP":"20144","DugAbbreviataStd":"V","idStrada":26153}]}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
		Object obj = CtrlAddressValidation.getResources(resource);
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		Map<String,Object> getSObject = CtrlAddressValidation.getSObject('Contact', c.Id, new Map<String,String>{'Id' => 'Id'});
		CtrlAddressValidation.updateAddress('Contact', c.Id, new Map<String,String>{'FirstName' => 'FirstName'}, new Map<String,Object>{'FirstName' => 'Ciao'});

	}

	@isTest
	static void testM2(){
		ApexPages.CurrentPage().getParameters().put('resource','strade/tortona?idComune=1772&q=tortona');
		Test.setMock(
            HttpCalloutMock.class,
            new MockHttpResponseGenerator(  
                200,
                'OK',
                '{"data":[{"id":246,"idSestiere":0,"idComune":"1772","DugStd":"VIA","DugComplStd":null,"NomeStd":"TORTONA","Quartiere":null,"DugApiciStd":"VIA","DugComplApiciStd":null,"NomeApiciStd":"TORTONA","QuartiereApici":null,"DenomAbbrevStd":"VIA TORTONA","MultiCap":"N","CAP":"20144","DugAbbreviataStd":"V","idStrada":26153}]}',
                new Map<String,String>{'Content-Type' => 'application/json'}
            )
        );
		CtrlAddressValidation ctrl = new CtrlAddressValidation();
		ctrl.getResources();
	}

}