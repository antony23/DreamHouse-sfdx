public with sharing class DeleteQuoteController {

    public Profile loginProfile             {public get; private set;}
    public Boolean inError                  {get; set;}
    public String errorMessage              {get; set;}
	public String quoteId               	{get; set;}
    public zqu__Quote__c quote   			{get; set;}
	public String accId						{get; set;}
    private ApexPages.StandardController controller;

    public DeleteQuoteController(ApexPages.StandardController stdController) {
        
        inError = false;
        controller = stdController;
		
		this.quoteId = controller.getRecord().Id;
        quote = [SELECT zqu__Status__c, zqu__Account__c FROM zqu__Quote__c WHERE Id =:quoteId LIMIT 1];
		accId = quote.zqu__Account__c;

        Id profileId = UserInfo.getProfileId();
        loginProfile = [SELECT Name FROM Profile WHERE Id = :profileId LIMIT 1];
        
    }

    public PageReference cancelQuote() {
        PageReference pageRef = null;
        
		if (loginProfile.Name.equalsIgnoreCase(AppConstants.PROFILE_SYSADMIN) || loginProfile.Name.equalsIgnoreCase(AppConstants.PROFILO_AMMINISTRATORE)) {
			delete quote;
			return returnPage();
		} else if (loginProfile.Name.equalsIgnoreCase(AppConstants.PROFILE_UFFICIO_GESTIONE)) {
			if (quote.zqu__Status__c == AppConstants.NEW_QUOTE) {
				delete quote;
				return returnPage();
			} else {
				inError = true;
				errorMessage = AppConstants.INVALID_QUOTE_STATUS;
			}
		} else {
			inError = true;
			errorMessage = AppConstants.INSUFFICIENT_PRIVILEGES;
		}

        return pageRef;
    }

    public PageReference returnPage() {
      PageReference pageRef = new PageReference('/'+accId);
      pageRef.setRedirect(true);
      return pageRef;
   }

}