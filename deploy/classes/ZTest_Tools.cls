// Ogni linea contenente il campo "Numero_Eventi__c" è stata commentata perché questo campo non è ancora presente in Produzione

@isTest
public class ZTest_Tools {

	@isTest
	public static void test_format_dates() {
		final String dateFormat = 'yyyy-MM-dd';
		final String timeFormat = 'HH:mm:ss.SSS';
		final String datetimeFormat = dateFormat + ' ' + timeFormat;
		final Datetime now = Datetime.now();

		DateTime dt = now;
		Date d = now.date();
		Time t = now.time();

		System.assertEquals(now.format(datetimeFormat), Tools.format(dt, datetimeFormat));
		System.assertEquals(now.format(dateFormat), Tools.format(d, dateFormat));
		System.assertEquals(now.format(timeFormat), Tools.format(t, timeFormat));
	}

	@isTest
	public static void test_format_decimals() {
		System.assertEquals('1.00', Tools.format((Decimal) 1.0, 2));
		System.assertEquals('1.00', Tools.format((Double) 1.0, 2));
		System.assertEquals('2.5', Tools.format((Decimal) 2.55, 1));
		System.assertEquals('2.5', Tools.format((Double) 2.55, 1));
		System.assertEquals('2.6', Tools.format((Decimal) 2.55, 1, System.RoundingMode.UP));
		System.assertEquals('2.6', Tools.format((Double) 2.55, 1, System.RoundingMode.UP));
	}

	@isTest
	public static void test_mapBy() {
		final List<Contact> contactList = loadContacts();
		final Map<Object, SObject> tMap = Tools.mapBy(contactList, 'FirstName');

		System.assertEquals(1, tMap.size());
		System.assertEquals(true, tMap.containsKey('TEST'));
	}

	@isTest
	public static void test_mapByList() {
		final List<Contact> contactList = loadContacts();
		final Map<Object, List<SObject>> tMap = Tools.mapByList(contactList, 'FirstName');

		System.assertEquals(1, tMap.size());
		System.assertEquals(10, tMap.get('TEST').size());
	}

	@isTest
	public static void test_mapBySet() {
		final List<Contact> contactList = loadContacts();
		final Map<Object, Set<SObject>> tMap = Tools.mapBySet(contactList, 'CodiceCliente__c');

		System.assertEquals(10, tMap.size());
		
		for(Object obj : tMap.keySet()) {
			System.assertEquals(1, tMap.get((String) obj).size());
		}
	}

	@isTest
	public static void test_join() {
		List<String> lset = new List<String>{'A', 'B', 'C'};
		System.assertEquals('A-B-C', Tools.join(lset, '-'));
		System.assertEquals('ABC', Tools.join(lset, ''));

		Set<String> sset1 = new Set<String>{'A', 'B', 'C'};
		System.assertEquals('A,B,C', Tools.join(sset1, ','));
		System.assertEquals('ABC', Tools.join(sset1, ''));

		Set<Integer> sset2 = new Set<Integer>{1, 2, 3};
		System.assertEquals('1,2,3', Tools.join(sset2, ','));

		Set<Long> sset3 = new Set<Long>{1, 2, 3};
		System.assertEquals('1,2,3', Tools.join(sset3, ','));

		Set<Boolean> sset4 = new Set<Boolean>{true, false, true};
		System.assertEquals('true,false', Tools.join(sset4, ','));

		Set<Double> sset5 = new Set<Double>{1.0, 2.0, 3.0};
		System.assertEquals('1.00,2.00,3.00', Tools.join(sset5, ',', 2));

		Set<Double> sset5_1 = new Set<Double>{1.0, 2.0, 3.0};
		System.assertEquals(1.0 + ',' + 2.0 + ',' + 3.0, Tools.join(sset5_1, ','));

		Set<Decimal> sset6 = new Set<Decimal>{1.02, 2.03, 3.04};
		System.assertEquals('1.02,2.03,3.04', Tools.join(sset6, ',', 2));

		Set<Decimal> sset6_1 = new Set<Decimal>{1.02, 2.03, 3.04};
		System.assertEquals(1.02 + ',' + 2.03 + ',' + 3.04, Tools.join(sset6_1, ','));

		final String dateFormat = 'yyyy-MM-dd';
		final String timeFormat = 'HH:mm:ss.SSS';
		final String datetimeFormat = dateFormat + ' ' + timeFormat;
		final Datetime now = Datetime.now();

		Set<Datetime> sset7 = new Set<Datetime>{ now, now.addDays(-1), now.addMonths(-1) };
		System.assertEquals(now.format(datetimeFormat) + ','
							+ now.addDays(-1).format(datetimeFormat) + ','
							+ now.addMonths(-1).format(datetimeFormat), Tools.join(sset7, ',', datetimeFormat));

		Set<Datetime> sset7_1 = new Set<Datetime>{ now, now.addDays(-1), now.addMonths(-1) };
		System.assertEquals(now + ',' + now.addDays(-1) + ',' + now.addMonths(-1), Tools.join(sset7_1, ','));

		Set<Date> sset8 = new Set<Date>{ now.date(), now.date().addDays(-1), now.date().addMonths(-1) };
		System.assertEquals(now.format(dateFormat) + ','
							+ now.addDays(-1).format(dateFormat) + ','
							+ now.addMonths(-1).format(dateFormat), Tools.join(sset8, ',', dateFormat));

		Set<Date> sset8_1 = new Set<Date>{ now.date(), now.date().addDays(-1), now.date().addMonths(-1) };
		System.assertEquals(now.date() + ',' + now.date().addDays(-1) + ',' + now.date().addMonths(-1), Tools.join(sset8_1, ','));

		Set<Time> sset9 = new Set<Time>{ now.time(), now.time().addHours(-1), now.time().addMinutes(-10) };
		System.assertEquals(now.format(timeFormat) + ','
							+ now.addHours(-1).format(timeFormat) + ','
							+ now.addMinutes(-10).format(timeFormat), Tools.join(sset9, ',', timeFormat));

		Set<Time> sset9_1 = new Set<Time>{ now.time(), now.time().addHours(-1), now.time().addMinutes(-10) };
		System.assertEquals(now.time() + ',' + now.time().addHours(-1) + ',' + now.time().addMinutes(-10), Tools.join(sset9_1, ','));

		final List<Contact> contactList = loadContacts();

		Set<Id> sset10 = Tools.extractIdSet(contactList);
		System.assertEquals(String.join(new List<Id>(sset10), ','), Tools.join(sset10, ','));

		Set<Object> sset0 = new Set<Object>{ new TestObject('A'), new TestObject('B'), new TestObject('C') };
		System.assertEquals('A,B,C', Tools.join(sset0, ','));
	}

	@isTest
	public static void test_extractIdSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractIdSet(contactList).size());
		System.assertEquals(contactList.size(), Tools.extractIdSet(contactList, 'Id').size());
	}

	@isTest
	public static void test_extractStringSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(1, Tools.extractStringSet(contactList, 'Name').size());
	}

	@isTest
	public static void test_extractBooleanSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(1, Tools.extractBooleanSet(contactList, 'DoNotCall').size());
	}

	@isTest
	public static void test_extractLongSet() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(1, Tools.extractLongSet(contactList, 'Numero_Eventi__c').size());
	}

	@isTest
	public static void test_extractIntegerSet() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(1, Tools.extractIntegerSet(contactList, 'Numero_Eventi__c').size());
	}

	@isTest
	public static void test_extractDoubleSet() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(1, Tools.extractDoubleSet(contactList, 'Numero_Eventi__c').size());
	}

	@isTest
	public static void test_extractDecimalSet() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(1, Tools.extractDecimalSet(contactList, 'Numero_Eventi__c').size());
	}

	@isTest
	public static void test_extractDateSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(1, Tools.extractDateSet(contactList, 'Birthdate').size());
	}

	@isTest
	public static void test_extractTimeSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(1, Tools.extractDatetimeSet(contactList, 'Data_ultima_sincronizzazione_SAP__c').size());
	}

	@isTest
	public static void test_extractDatetimeSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(1, Tools.extractTimeSet(contactList, 'Data_ultima_sincronizzazione_SAP__c').size());
	}

	@isTest
	public static void test_extractSet() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractSet(contactList, 'CodiceCliente__c').size());
	}

	@isTest
	public static void test_extractIdList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractIdList(contactList).size());
		System.assertEquals(contactList.size(), Tools.extractIdList(contactList, 'Id').size());
		System.assertEquals(contactList.size(), Tools.extractIdList(contactList, 'Id', true, true).size());
	}

	@isTest
	public static void test_extractStringList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractStringList(contactList, 'Name').size());
		System.assertEquals(contactList.size(), Tools.extractStringList(contactList, 'Name', true, false).size());
		System.assertEquals(1, Tools.extractStringList(contactList, 'Name', false, true).size());
	}

	@isTest
	public static void test_extractBooleanList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractBooleanList(contactList, 'DoNotCall').size());
		System.assertEquals(contactList.size(), Tools.extractBooleanList(contactList, 'DoNotCall', true, false).size());
		System.assertEquals(1, Tools.extractBooleanList(contactList, 'DoNotCall', false, true).size());
	}

	@isTest
	public static void test_extractLongList() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(contactList.size(), Tools.extractLongList(contactList, 'Numero_Eventi__c').size());
		//System.assertEquals(contactList.size(), Tools.extractLongList(contactList, 'Numero_Eventi__c', true, false).size());
		//System.assertEquals(1, Tools.extractLongList(contactList, 'Numero_Eventi__c', false, true).size());
	}

	@isTest
	public static void test_extractIntegerList() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(contactList.size(), Tools.extractIntegerList(contactList, 'Numero_Eventi__c').size());
		//System.assertEquals(contactList.size(), Tools.extractIntegerList(contactList, 'Numero_Eventi__c', true, false).size());
		//System.assertEquals(1, Tools.extractIntegerList(contactList, 'Numero_Eventi__c', false, true).size());
	}

	@isTest
	public static void test_extractDoubleList() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(contactList.size(), Tools.extractDoubleList(contactList, 'Numero_Eventi__c').size());
		//System.assertEquals(contactList.size(), Tools.extractDoubleList(contactList, 'Numero_Eventi__c', true, false).size());
		//System.assertEquals(1, Tools.extractDoubleList(contactList, 'Numero_Eventi__c', false, true).size());
	}

	@isTest
	public static void test_extractDecimalList() {
		final List<Contact> contactList = loadContacts();

		//System.assertEquals(contactList.size(), Tools.extractDecimalList(contactList, 'Numero_Eventi__c').size());
		//System.assertEquals(contactList.size(), Tools.extractDecimalList(contactList, 'Numero_Eventi__c', true, false).size());
		//System.assertEquals(1, Tools.extractDecimalList(contactList, 'Numero_Eventi__c', false, true).size());
	}

	@isTest
	public static void test_extractDateList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractDateList(contactList, 'Birthdate').size());
		System.assertEquals(contactList.size(), Tools.extractDateList(contactList, 'Birthdate', true, false).size());
		System.assertEquals(1, Tools.extractDateList(contactList, 'Birthdate', false, true).size());
	}

	@isTest
	public static void test_extractTimeList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractTimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c').size());
		System.assertEquals(contactList.size(), Tools.extractTimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c', true, false).size());
		System.assertEquals(1, Tools.extractTimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c', false, true).size());
	}

	@isTest
	public static void test_extractDatetimeList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractDatetimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c').size());
		System.assertEquals(contactList.size(), Tools.extractDatetimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c', true, false).size());
		System.assertEquals(1, Tools.extractDatetimeList(contactList, 'Data_ultima_sincronizzazione_SAP__c', false, true).size());
	}

	@isTest
	public static void test_extractList() {
		final List<Contact> contactList = loadContacts();

		System.assertEquals(contactList.size(), Tools.extractList(contactList, 'CodiceCliente__c').size());
		System.assertEquals(contactList.size(), Tools.extractList(contactList, 'CodiceCliente__c', true, false).size());
		System.assertEquals(contactList.size(), Tools.extractList(contactList, 'CodiceCliente__c', false, true).size());
	}

	@testSetup
	public static void setup() {
		final List<Contact> contactList = new List<Contact>();
		final Date birthDate = Date.newInstance(1994, 01, 01);
		final Datetime now = Datetime.now();

		for(Integer i = 0; i < 10; i++) {
			final Contact contatto = new Contact();

			contatto.FirstName = 'TEST';
			contatto.LastName = 'ACCOUNT';
			contatto.DoNotCall = True;
			//contatto.Numero_Eventi__c = 50;
			contatto.Birthdate = birthDate;
			contatto.Data_ultima_sincronizzazione_SAP__c = now;
			contatto.CodiceCliente__c = i + '';

			contactList.add(contatto);
		}

		insert contactList;
	}

	private static List<Contact> loadContacts() {
		return [SELECT Id, Name, FirstName, LastName, DoNotCall, Birthdate, /*Numero_Eventi__c,*/ Data_ultima_sincronizzazione_SAP__c, CodiceCliente__c FROM Contact];
	}

	class TestObject {
		final String value;

		public TestObject(String value) {
			this.value = value;
		}

		override
		public String toString() {
			return value;
		}
	}
}