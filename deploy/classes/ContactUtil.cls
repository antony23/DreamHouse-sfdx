public class ContactUtil{


    @Future(callout=true)
    public static void updateContactRelatedObjects(List<Id> assetContactIdlist, List<Id> entitlementContactIdList,
                                                    List<Id> avvisoContactIdList, List<Id> zuoraContactIdList){
        try{
            List<Asset> assetList = [SELECT Id, ContactId FROM Asset WHERE ContactId = :assetContactIdlist AND Inviata__c = false];
            List<Entitlement__c> entitlementList = [SELECT Id, Contact__c FROM Entitlement__c WHERE RecordType.DeveloperName = 'Virtualcomm' AND Contact__c = :entitlementContactIdList AND (Stato__c = 'Da Inviare' OR Stato__c = 'Inviato')];
            List<Avviso_di_Rinnovo__c> avvisoDiRinnovoList = [SELECT Id, Contatto__c, Contact_Sold_To__c FROM Avviso_di_Rinnovo__c WHERE Contatto__c = :avvisoContactIdList AND (Stato_Avviso__c = 'Nuovo' OR Stato_Avviso__c = 'Confermato')];

            Set<Id> contactsIdSet = new Set<Id>();
            contactsIdSet.addAll(assetContactIdlist);
            contactsIdSet.addAll(entitlementContactIdList);
            contactsIdSet.addAll(avvisoContactIdList);
            contactsIdSet.addAll(zuoraContactIdList);

            /*String whereClause = 'Id = :contactsIdSet';
            String additionalFields = '';
            String queryString = Utils.getSelectAllQuery('Contact', whereClause, null, additionalFields, false);
            Map<Id,Contact> contactsMap = new Map<Id,Contact>((List<Contact>) Database.query(queryString));
*/
            List<string> contactFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('Contact').getDescribe().Fields.getMap().values()){
                contactFields.add(ft.getDescribe().getName().toLowerCase());
            }
            Map<Id,Contact> contactsMap = new Map<Id,Contact>((List<Contact>) Database.query(
                'SELECT ' + String.join(contactFields, ',') + 
                ' FROM Contact ' +
                ' WHERE Id IN : contactsIdSet '));

            List<Contact> assetContactlist = new List<Contact>();
            List<Contact> entitlementContactList = new List<Contact>();
            List<Contact> avvisoContactList = new List<Contact>();
            List<Contact> zuoraContactList = new List<Contact>();
            
            for(Id cId : assetContactIdlist){
                if(contactsMap.get(cId) != null){
                    assetContactlist.add(contactsMap.get(cId));
                }
            }
            for(Id cId : entitlementContactIdList){
                if(contactsMap.get(cId) != null){
                    entitlementContactList.add(contactsMap.get(cId));
                }
            }
            for(Id cId : avvisoContactIdList){
                if(contactsMap.get(cId) != null){
                    avvisoContactList.add(contactsMap.get(cId));
                }
            }
            for(Id cId : zuoraContactIdList){
                if(contactsMap.get(cId) != null){
                    zuoraContactList.add(contactsMap.get(cId));
                }
            }
           
            List<Zuora__CustomerAccount__c> billingAccountList = [SELECT Id, Zuora__BillToId__c, Bill_To_Salesforce__c, Zuora__SoldToId__c, Sold_To_Salesforce__c
                                                                    FROM Zuora__CustomerAccount__c
                                                                    WHERE Bill_To_Salesforce__c = :zuoraContactIdList OR Sold_To_Salesforce__c = :zuoraContactIdList];

            System.debug(loggingLevel.Error, '*** billingAccountList: ' + billingAccountList);

            Map<String, Contact> zuoraIdContactMap = new Map<String, Contact>();
            for(Zuora__CustomerAccount__c ca : billingAccountList){
                Contact tmpBillContact = contactsMap.get(ca.Bill_To_Salesforce__c);
                if(tmpBillContact != null){
                    zuoraIdContactMap.put(ca.Zuora__BillToId__c, tmpBillContact);
                }

                Contact tmpSoldContact = contactsMap.get(ca.Sold_To_Salesforce__c);
                if(tmpSoldContact != null){
                    zuoraIdContactMap.put(ca.Zuora__SoldToId__c, tmpSoldContact);
                }
            }


            if(zuoraIdContactMap.keySet().size() > 0 ){
                updateZuoraContact(zuoraIdContactMap);
            }
            if(assetList.size() > 0 ){
                updateAssets(assetContactlist, assetList);
            }

            if(entitlementList.size() > 0){
                updateEntitlement(entitlementContactList, entitlementList);
            }

            if(avvisoDiRinnovoList.size() > 0){
                updateAvvisoDiRinnovo(avvisoContactList, avvisoDiRinnovoList);
            }

            // creazione case automatico
            List<Case> cases = new List<Case>();
            Map<Id,Boolean> permettiCreazioneCase = new Map<Id,Boolean>();
            for(AggregateResult caseEsistente : [SELECT ContactId,MAX(CreatedDate) dataUltimaModifica
                                                    FROM Case 
                                                    WHERE ContactId IN :contactsMap.keySet()
                                                    AND Sottocategoria__c = 'MODIFICA DATI ANAGRAFICI'
                                                    GROUP BY ContactId]){
                Datetime dt = (Datetime) caseEsistente.get('dataUltimaModifica');
                System.debug(loggingLevel.Error, '*** caseEsistente.get(0).ContactId: ' + caseEsistente.get('ContactId'));
                System.debug(loggingLevel.Error, 'Data ultima modifica: ' + dt);
                Long dataUltimaModifica = dt.getTime();
                Long ora = Datetime.now().getTime();
                System.debug(loggingLevel.Error, '*** crea case: ' + ((ora - dataUltimaModifica) > 60000));
                permettiCreazioneCase.put((Id) caseEsistente.get('ContactId'), ((ora - dataUltimaModifica) > 60000));
            }

            /*for(Contact cont : contactsMap.values()){
                if(!permettiCreazioneCase.containsKey(cont.Id) || permettiCreazioneCase.get(cont.Id)){
                    cases.add(
                        new Case(
                            OwnerId = UserInfo.getUserId(),
                            Origin = 'Creazione automatica',
                            Subject = 'Modifica dati anagrafici',
                            Description = 'Modifica definitiva indirizzo ',
                            Status = 'Chiuso',
                            AccountId = cont.AccountId,
                            ContactId = cont.Id,
                            Macro_Categoria__c = 'Diffusione',
                            Categoria__c = 'MODIFICHE ANAGRAFICHE',
                            Sottocategoria__c = 'MODIFICA DATI ANAGRAFICI',
                            Caseautomatico__c = true
                        )
                    );
                }
            }
            insert cases;*/
            
        }
        catch (Exception e){
            sendAlert(e.getMessage());
            System.debug(e.getMessage());
        }
    }

    public static void updateAssets(List<Contact> contactList, List<Asset> assetList){
        if(assetList != null && assetList.size() > 0){

            Map<Id, Contact> contactMap = new Map<Id, Contact>(contactList);
            Map<String,CapDetails__c> capDetails = DomusUtil.getCapDetails();
            
            for(Asset a : assetList){
                Contact cont = contactMap.get(a.ContactId);
                if(cont != null){
                   String indirizzo = '';
                    if(cont.DugAbbreviataStd__c != null || cont.Dug__c != null) indirizzo += (cont.DugAbbreviataStd__c != null ? cont.DugAbbreviataStd__c : cont.Dug__c) + ' ';
                    indirizzo += cont.Via__c + ' ' + 
                    (cont.Civico__c != null ? cont.Civico__c : '') + ' ' +  
                    (cont.Supplemento_Civico__c != null ? cont.Supplemento_Civico__c : '');
                    //String indirizzo = cont.MailingStreet;
                    String informazioniIndirizzo = cont.MailingPostalCode + ' ' +  cont.MailingCity;
                    Integer dettaglioZona = String.isNotBlank(cont.Id_Area_Nazione__c) ? Integer.valueOf(cont.Id_Area_Nazione__c) : null;
                    
                    a.Dettagli_Cliente__c = cont.Name;
                    a.Nazione__c = cont.MailingCountry;
                    a.Indirizzo__c = indirizzo;
                    a.Citt__c = cont.MailingCity;
                    a.Informazioni_Indirizzo__c = informazioniIndirizzo;
                    a.Codice_Postale__c = cont.MailingPostalCode;
                    a.Provincia__c = cont.MailingState;
                    a.Dettaglio_Zona__c = dettaglioZona;
                    a.Zona__c = cont.Area_Nazione__c;
                    a.Presso__c = cont.Presso__c;
                    a.Frazione__c = cont.Frazione__c;   //*** DH-900 **//

                    if(cont.Codice_Nazione__c == 'IT' || cont.Codice_Nazione__c == 'VA' || cont.Codice_Nazione__c == 'SM'){
                        for(CapDetails__c capDetail : capDetails.values()){
                            Boolean found = false;
                            if(capDetail.Name == cont.MailingPostalCode) found = true;
                            if(!found && capDetail.Name.contains('-')){
                                String[] range = capDetail.Name.split('-');
                                if(Integer.valueOf(cont.MailingPostalCode) >= Integer.valueOf(range[0]) && Integer.valueOf(cont.MailingPostalCode) <= Integer.valueOf(range[1])) found = true;
                            }
                            if(found){
                                a.CodiceRegione__c = capDetail.CodiceRegione__c;
                                a.SiglaRegione__c = capDetail.SiglaRegione__c;
                                a.SiglaRegioneAbbr__c = capDetail.SiglaRegioneAbbr__c;
                                a.ZonadItalia__c = capDetail.ZonadItalia__c;
                                a.Suddivisione_geografica_2__c = capDetail.Suddivisione_geografica_2__c;
                                if(capDetail.CodiceRegione__c == 'SAR'){ // la sardergna ha per un cap più province
                                    a.CodiceProvincia__c = cont.MailingState;
                                }else{
                                    a.CodiceProvincia__c = capDetail.CodiceProvincia__c;
                                }
                                break;
                            }
                        }
                    }
                }

            }

           update assetList;

        }
    }


    public static void updateEntitlement(List<Contact> contactList, List<Entitlement__c> entitlementList){
        if(entitlementList != null && entitlementList.size() > 0){

            Map<Id, Contact> contactMap = new Map<Id, Contact>(contactList);

            for(Entitlement__c e : entitlementList){
                Contact c = contactMap.get(e.Contact__c);
                if(c!=null){
                    e.zip__c = String.valueOf(c.MailingPostalCode);
                    e.city__c = c.MailingCity;
                    e.address__c = c.MailingStreet;
                    e.nation__c = c.Nazione__c;
                    e.telephone__c = c.Phone;
                    e.surname__c = c.LastName;
                    e.name__c = c.FirstName;
                    e.mobile__c = c.MobilePhone;
                    e.Stato__c = 'Da Inviare';
                }
            }
            update entitlementList;
        }
    }

    public static void updateAvvisoDiRinnovo(List<Contact> contactList, List<Avviso_di_rinnovo__c> avvisoDiRinnovoList){
        if(avvisoDiRinnovoList != null && avvisoDiRinnovoList.size() > 0){
            Map<Id, Contact> contactMap = new Map<Id, Contact>(contactList);

            for(Avviso_di_rinnovo__c av : avvisoDiRinnovoList){
                Contact c = contactMap.get(av.Contatto__c);
                if(c!=null){
                    av.Titolo__c = c.Titolo__c;
                    av.Titolo_Cod__c = c.Titolo_Cod__c;
                    av.Ulteriore_Titolo__c = c.Professione__c;
                    if(c.Sesso__c == 'M') av.Sesso__c = 1;
                    if(c.Sesso__c == 'F') av.Sesso__c = 2;
                    //av.Codice_Committente__c = avvisoAccount.Id; Mettere c.codice SAP?
                    av.Cognome_Committente__c = c.LastName;
                    av.Nome_Committente__c = c.FirstName;
                    av.DugAbbreviataStd__c = c.DugAbbreviataStd__c;
                    av.Indirizzo_Committente__c = c.Via__c;
                    av.Civico_Committente__c = c.Civico__c;
                    av.Supplemento_Civico_Committente__c = c.Supplemento_Civico__c;
                    av.CAP_Committente__c = String.valueOf(c.MailingPostalCode);
                    av.Supplemento_CAP_Committente__c = c.Supplemento_CAP__c;
                    av.Localita_Committente__c = c.MailingCity;
                    // DH-593
                    //av.Provincia_Committente__c = c.Codice_Provincia__c;
                    av.Provincia_Committente__c = c.MailingState;
                    av.Nazione_Committente__c = c.Nazione__c;
                    av.Codice_Destinatario__c = c.AccountId;

                    if(av.Contact_Sold_To__c == av.Contatto__c){
                        av.Nome_Destinatario__c = c.FirstName;
                        av.Cognome_Destinatario__c = c.LastName;
                        av.Codice_Nazione_Destinatario__c = c.Codice_Nazione__c;
                        av.Presso__c = c.Presso__c;
                    }
                }
            }
            update avvisoDiRinnovoList;
        }
    }


    public static void updateZuoraContact(Map<String, Contact> zuoraIdContactMap){

        System.debug(loggingLevel.Error, '*** zuoraIdContactMap: ' + zuoraIdContactMap);

        Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        List<String> zuoraIds = new List<String>(zuoraIdContactMap.keySet());
        System.debug(loggingLevel.Error, '*** zuoraIds: ' + zuoraIds);
        for(Integer i = 0 ; i < (zuoraIds.size() / 50) + 1 ; i++){
            List<Zuora.zObject> objs = new List<Zuora.zObject>();
            for(Integer j=(i*50); (j<(i*50)+50) && j<zuoraIds.size() ; j++){
                String zuoraId = zuoraIds.get(j);
                System.debug(loggingLevel.Error, '*** zuoraId: ' + zuoraId);
                Contact c = zuoraIdContactMap.get(zuoraId);
                System.debug(loggingLevel.Error, '*** c: ' + c);
                if(c != null){
                    Zuora.zObject contact = new Zuora.zObject('Contact');
                    //*** CR Rimozione Trattino ***//
                    contact.setValue('Id', zuoraId);
                    contact.setValue('Address1', c.MailingStreet);
                    contact.setValue('City', c.MailingCity);
                    contact.setValue('Country', c.MailingCountry);
                    //contact.setValue('FirstName', c.FirstName);
                    contact.setValue('FirstName', BillingAccountUtil.checkContactFirstName(c));
                    contact.setValue('LastName', c.LastName);
                    contact.setValue('PostalCode', c.MailingPostalCode);
                    contact.setValue('State', c.MailingState);
                   // contact.setValue('PersonalEmail', );
                    contact.setValue('WorkEmail', c.Email);
                    contact.setValue('WorkPhone', c.Phone);
                    contact.setValue('MobilePhone', c.MobilePhone);
                    //contact.setValue('HomePhone', c.HomePhone);
                    objs.add(contact);

                    System.debug(loggingLevel.Error, '*** contact: ' + contact);
                }
            }
            List<Zuora.zApi.SaveResult> results = zApiInstance.zupdate(objs);
            System.debug(loggingLevel.Error, '*** results: ' + results);
            objs = new List<Zuora.zObject>();

        }

    }

    private static void sendAlert(String errMessage){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String emailAddr = MailingList__c.getValues('ConctactUpdateError').MailingList__c;

        String[] toAddresses = emailAddr.split(';');
        mail.setToAddresses(toAddresses);

        mail.setSubject('Errore aggiornamento contact');
        mail.setPlainTextBody('Attenzione errore durante l\'aggiornamento del contatto: ' + errMessage);
        mail.setHtmlBody('Attenzione errore durante l\'aggiornamento del contatto: <br/>' + errMessage);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}