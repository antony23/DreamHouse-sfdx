public class WizardQuoteExtension {

    public class WizardQuoteExtensionException extends Exception {}

    //CONTROLLER VARIABLES
    public string pageMessage {get;set;}
    public boolean showMessage {get;set;}
    public zqu.Quote quote {get;set;}
    public zqu__Quote__c quoteObj {get;set;}
    public Account account {get;set;}
    private Boolean controllerInitialized = false;
    private Integer numMonth;
    public Decimal totaleTest {get;set;}
    public Boolean isRinnovo {get;set;}

    private Set<String> insertedQuoteRatePlanSet;
    private ApexPages.StandardController controller {get;set;}

    public String selectedCountry{get;set;} 
    public Map<Id,Zuora__CustomerAccount__c> billingAccountMap {get;set;}

    public Id quoteIdEdit {get;set;} // quote id for edit mode wizard
    public Id inputSubId {get;set;}
    public Boolean isEditMode {
        get{ return quoteIdEdit != null; } 
    }

    public Map<String,PageReference> URIs {
        public get{
            try {
                PageReference electronicPaymentPage = Page.WizardCreditCardPayment;
                electronicPaymentPage.getParameters().put('Id',quoteObj.Id);

                return new Map<String,PageReference>{
                    'Account' => Page.WizardSelectAccount,
                    'Cart' => Page.WizardCart,
                    'Subscription' => Page.WizardSubscription,
                    'Bundle' => Page.WizardSelectBundle,
                    'Direct Sales' => Page.WizardDirectSales,
                    'Billing' => Page.WizardBilling,
                    'Payment' => electronicPaymentPage
                };
            } catch(Exception e) {
                appendErrorMessage(e);
            }
            return null;
        }
    }

    public List<SelectOption> paymentMethods{
        get{
            List<SelectOption> res = new List<SelectOption>();
            try {
                List<MetodoDiPagamento__mdt> paymentMethodList = [SELECT MasterLabel,ZuoraMethod__c,Active__c,Order__c FROM MetodoDiPagamento__mdt WHERE Active__c=true ORDER BY Order__c ASC];
                for(MetodoDiPagamento__mdt method : paymentMethodList){
                    res.add(new SelectOption(method.ZuoraMethod__c,method.MasterLabel));
                } 
            } catch(Exception e) {
                appendErrorMessage(e);
            }
            return res;
        }
    }

    public Boolean subscriptionlistEmpty {get;set;}
    public Boolean venditeDiretteListEmpty {get;set;}
    public Boolean bundleAbbonamentoListEmpty{get;set;}
    public Boolean bundleListEmpty{get;set;}

    public WizardQuoteExtension(ApexPages.StandardController ctrl){
        try {
            controller = ctrl;
            System.debug(loggingLevel.Error, '*** INIT WizardQuoteExtension');

            String qIdStr = ApexPages.currentPage().getParameters().get('Id');
            if(String.isNotBlank(qIdStr)){
                quoteIdEdit = Id.valueOf(qIdStr);
            }
            String subIdStr = ApexPages.currentPage().getParameters().get('subId');
            if(String.isNotBlank(subIdStr)){
                inputSubId = Id.valueOf(subIdStr);
            }

            System.debug(loggingLevel.Error, '*** qIdStr: ' + qIdStr);
            System.debug(loggingLevel.Error, '*** quoteIdEdit: ' + quoteIdEdit);

        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void initQuote(){
        try {
            System.debug(loggingLevel.Error, '*** quoteIdEdit: ' + quoteIdEdit);
            if(quoteIdEdit == null){ // create mode
                zqu__Quote__c controllerRecord = (zqu__Quote__c) controller.getRecord();
                quote = zqu.Quote.getNewInstance();
                quoteObj = quote.getSObject();
                quoteObj.zqu__Account__c = controllerRecord.zqu__Account__c;
                quoteObj.Azienda__c = controllerRecord.Azienda__c;
                quoteObj.zqu__StartDate__c = Date.today();
                quoteObj.zqu__ValidUntil__c  = Date.today();
                quoteObj.Fattura_a_intermediario__c = false;
                quoteObj.zqu__InitialTerm__c = 1;
                quoteObj.zqu__RenewalTerm__c = 1;
                quoteObj.zqu__InitialTermPeriodType__c = 'Week';
                quoteObj.zqu__RenewalTermPeriodType__c = 'Week';
                quoteObj.zqu__AutoRenew__c = false;
                if(quoteObj.Subscription_Precedente_Text__c == null){
                    quoteObj.Numero_di_Cicli_Text__c = '1';
                }
                quoteObj.Canale_di_acquisizione__c = 'Telefono';
                quoteObj.Quote_Status__c = 'Draft';
                quote.save();
            }else{ // edit mode
                quote = zqu.Quote.getInstance(quoteIdEdit);
                quoteObj = quote.getSObject();
                quoteObj.Quote_Status__c = 'Draft';
                quoteObj.zqu__Service_Activation_Date__c = null;
                quote.save();
                //DomusUtil.updateTotalsAsync(quoteObj.Id);
            }

           if (quoteObj.zqu__Account__c !=null && 
                [Select Id, Type FROM Account WHERE id = :quoteObj.zqu__Account__c ].Type != 'Intermediario'
                && [Select Id, Type FROM Account WHERE id = :quoteObj.zqu__Account__c ].Type != 'Convenzione' ) {
    
               // quoteObj.Azienda__c = quoteObj.zqu__Account__c;
                
          }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    //Initialization for the cart
    public PageReference initCart(){
        try {

            System.debug(loggingLevel.Error, '*** quoteIdEdit: ' + quoteIdEdit);
            System.debug(loggingLevel.Error, '*** quote: ' + quote);
           

            if(quote == null && quoteIdEdit != null){
               quote = zqu.Quote.getInstance(quoteIdEdit);
               quoteObj = quote.getSObject();
               account = (Account) Database.query(Utils.getSelectAllQuery('Account', 'Id = \''+quoteObj.zqu__Account__c+'\'', null, 'RecordType.DeveloperName', true));
            }
            //Se viene passata una subscription in input
            //Viene generata una nuova quote con lo stesso rateplan della subscription di partenza e le date aggiornate
            
           


            if(quoteObj.Subscription_Precedente__c != null){
                Zuora__Subscription__c subscriptionPrecedente = [SELECT Id,ModificaERiattiva__c FROM Zuora__Subscription__c WHERE Id = :quoteObj.Subscription_Precedente__c];
                if(!subscriptionPrecedente.ModificaERiattiva__c){
                    isRinnovo = true;
                }
            }

            insertedQuoteRatePlanSet = new Set<String>();
            List<zqu__QuoteRatePlan__c> abbonamentiList = DomusUtil.getAbbonamenti(quote.getId());
            subscriptionlistEmpty = abbonamentiList == null || abbonamentiList.isEmpty();

            List<zqu__QuoteRatePlan__c> venditeDiretteList = DomusUtil.getQuoteVenditeDirette(quote.getId());
            venditeDiretteListEmpty = venditeDiretteList == null || venditeDiretteList.isEmpty();
                
            List<zqu__QuoteRatePlan__c> bundleAbbonamentoList = DomusUtil.getQuoteBundleConAbbonamento(quote.getId());
            bundleAbbonamentoListEmpty = bundleAbbonamentoList == null || bundleAbbonamentoList.isEmpty();

            List<zqu__QuoteRatePlan__c> bundleList = DomusUtil.getQuoteBundle(quote.getId());
            bundleListEmpty = bundleList == null || bundleList.isEmpty();
            
            String skipToBilling = ApexPages.currentPage().getParameters().get('skipToBilling');
            if(String.isNotBlank(skipToBilling) && skipToBilling=='true'){
               return goToBilling();
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }    
        return null;
    }

    public void initBilling(){
        try {
            billingAccountMap = new Map<Id,Zuora__CustomerAccount__c>([
                SELECT Id, Name, Zuora__Zuora_Id__c, Zuora__AccountNumber__c, Zuora__BillToName__c, Zuora__SoldToName__c,
                BillToFullAddress__c, SoldToFullAddress__c, 
                Zuora__BillCycleDay__c, Zuora__DefaultPaymentMethod__c, Zuora__Payment_Term__c, Zuora__PaymentTerm__c, Zuora__Currency__c
                FROM Zuora__CustomerAccount__c 
                WHERE Zuora__Account__c = :quoteObj.zqu__Account__c
            ]);
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public PageReference goToSelectAccount(){
        System.debug(loggingLevel.Error, '*** go to select account');
        PageReference pr = URIs.get('Account');
        pr.getParameters().put('Id',quoteObj.Id);
        quoteIdEdit = quoteObj.Id;
        System.debug(loggingLevel.Error, '*** quoteObj.Id: ' + quoteObj.Id);
        return pr;
    }

    public PageReference goToCart(){
        PageReference res = null;
        try {
            Set<String> excludedFields = new Set<String>();
            excludedFields.add('billinggeocodeaccuracy');
            excludedFields.add('shippinggeocodeaccuracy');
            account = (Account) Database.query(Utils.getSelectAllQuery('Account', 'Id = \''+quoteObj.zqu__Account__c+'\'', null, 'RecordType.DeveloperName', true, false, excludedFields));
            quote.save();
            res = URIs.get('Cart');
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return res;
    }

    public PageReference goToDirectSales(){
        return URIs.get('Direct Sales');
    }

    public PageReference goToSubscriptions(){
        return URIs.get('Subscription');
    }

    public PageReference goToBilling(){
        PageReference res = null;
        try {
            Id quoteId = quote.getId();
            doInitSpese = true;
            speseDiSpedizioneResponse = null;
            quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = null;
            quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = null;

            List<zqu__QuoteRatePlan__c> ratePlanList = [SELECT Id, Calendario__c, zqu__ProductRatePlan__c FROM zqu__QuoteRatePlan__c WHERE (Rate_Plan_Type__c = 'Abbonamento' OR 
                                                                                                                        Rate_Plan_Type__c = 'Bundle Abbonamenti' OR
                                                                                                                        Rate_Plan_Type__c = 'Bundle Misti') AND zqu__quote__c = :quoteId];
            if(ratePlanList != null && ratePlanList.size() > 0){
                Id ratePlanId = ratePlanList.get(0).zqu__ProductRatePlan__c;
                Id calendarioId = ratePlanList.get(0).Calendario__c;
                System.debug(loggingLevel.Error, '*** ratePlanId: ' + ratePlanId);
                System.debug(loggingLevel.Error, '*** calendarioId: ' + calendarioId);

                Calendario__c calendar = DomusUtil.getCalendario(calendarioId);
                System.debug(loggingLevel.Error, '*** calendar: ' + calendar);

                Map<String, Object> subParametersMap = SubscriptionUtil.getSubscriptionParameters(ratePlanId, calendar.Data_Uscita__c, false);
                System.debug(loggingLevel.Error, '*** subParametersMap: ' + subParametersMap);
                /* Inizio: DH-841 */
                quoteObj.Dettaglio_Copie_per_Testata__c = (String) subParametersMap.get('dettaglioCopiePerTestata');
                /* Fine: DH-841 */
                quoteObj.Supporto__c = (String) subParametersMap.get('supporto');
                quoteObj.Testate__c = (String) subParametersMap.get('testate');
                Date startDate = (Date) subParametersMap.get('startDate');
                Date endDate = (Date) subParametersMap.get('endDate');
                quoteObj.Calendario_Inizio_Text__c = (String) subParametersMap.get('calendarioInizio');
                quoteObj.Calendario_Fine_Text__c = (String) subParametersMap.get('calendarioFine'); 
                quoteObj.Numero_vendite_dirette__c = (Integer) subParametersMap.get('numVenditeDirette');
                quoteObj.Data_fine_ultima_copia_per_calcolo_issue__c = endDate;
                
                if(quoteObj.Subscription_Precedente_Text__c == null){
                    quoteObj.Numero_di_Cicli_Text__c = String.valueOf(DomusUtil.getCicli(quoteObj.Id));
                }

                DomusUtil.setQuoteDateParameters(startDate, endDate, quoteObj);
                quote.save();
                DomusUtil.setQuoteChargeDateParameters(startDate, endDate, quoteObj.Id);
                quote.save();
            }

            List<zqu__QuoteRatePlan__c> quoteRPCampaignList = [SELECT Id, Campaign__c
                                                                FROM zqu__QuoteRatePlan__c 
                                                                WHERE zqu__Quote__c = :quoteId 
                                                                AND Rate_Plan_Type__c = 'Campagna' 
                                                                AND (Campaign__r.Tipo_Promozione__c = 'Codice'
                                                                OR Campaign__r.Tipo_Promozione__c = 'Target')
                                                                AND (Campaign__r.Rate_Plan_in_Campagna__c != NULL
                                                                OR Campaign__r.Prodotto_Vendita_Diretta__c != NULL)
                                                                AND Campaign__r.Sconto_Fisso__c = NULL];
            List<Id> campaignIdList = new List<Id>();
            if(quoteRPCampaignList != null && quoteRPCampaignList.size() > 0){
                for(zqu__QuoteRatePlan__c qrp : quoteRPCampaignList){
                    campaignIdList.add(qrp.Campaign__c);
                }
                List<Campaign> campaignList = DomusUtil.getCampaignList(campaignIdList);
                if(campaignList != null){
                    Map<Id, Campaign> campaignMap = new Map<Id, Campaign>(campaignList);
                    for(zqu__QuoteRatePlan__c qrp : quoteRPCampaignList){
                        DomusUtil.updateCampaignCharge(quoteId, qrp.id, campaignMap.get(qrp.Campaign__c));
                    }    
                }
            }



            List<zqu__QuoteRatePlanCharge__c> chargeList = DomusUtil.getQuoteChargeCampaignList(quote.getId());
            Integer numMonth = 1;

            List<zqu__QuoteRatePlanCharge__c> chargeAbb = DomusUtil.getChargeAbbonamenti(quote.getId());
            if(chargeAbb!=null && chargeAbb.size()>0){
                numMonth = Integer.valueOf(chargeAbb.get(0).zqu__SpecificBillingPeriod__c);
            }
            for(zqu__QuoteRatePlanCharge__c c : chargeList){
                c.zqu__SpecificBillingPeriod__c = numMonth;
            }
            update chargeList;

            DomusUtil.cleanQuote(quoteId);

            res = URIs.get('Billing');
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return res;
    }

    public PageReference goToBundle(){
        return URIs.get('Bundle');
    }

    /* WIZARD SPESE */
    public Map<String,List<SelectOption>> tipiSpeseSpedizione {get;set;}
    public ServiceUtil.TipiSpeseDiSpedizioneResponse tipiSpeseConsegne {get;set;}
    private Boolean isContrassegno = false;
    public List<SelectOption> tipiConsegnaAbbonamenti {
        get{
            List<SelectOption> res = new List<SelectOption>();
            res.add(new SelectOption('','--Seleziona--'));
            if(!speseDiSpedizioneError){
                if(tipiSpeseConsegne.tipiSpeseDiSpedizioneAbbonamenti != null){
                    List<ServiceUtil.TipoConsegna> tipiConsegna = tipiSpeseConsegne.tipiSpeseDiSpedizioneAbbonamenti.get(quoteObj.Tipo_Spese_Spedizione_Abbonamento__c);
                    System.debug(loggingLevel.Error, '*** tipiConsegna: ' + tipiConsegna);
                    if(tipiConsegna != null){
                        CustomPermissions__c custPerm = CustomPermissions__c.getInstance(UserInfo.getProfileId());
                        for(ServiceUtil.TipoConsegna tc : tipiConsegna){
                            if(!custPerm.GranttuttiiTipiconsegnaAbbonamento__c && tc.codicesap != 'ZL' && tc.codicesap != 'ZM' && tc.codicesap != 'ZN') continue;
                            res.add(new SelectOption(tc.codicesap,tc.tipoconsegna));
                        }
                    }
                }
            }
            return res;
        }
    }
    public List<SelectOption> tipiConsegnaVenditeDirette {
        get{
            if(isContrassegno){
                isContrassegno = false;
                return new List<SelectOption>{new SelectOption('COCO','Contrassegno')};
            }else{
                List<SelectOption> res = new List<SelectOption>();
                res.add(new SelectOption('','--Seleziona--'));
                if(!speseDiSpedizioneError){
                    if(tipiSpeseConsegne.tipiSpeseDiSpedizioneVenditeDirette != null){
                        List<ServiceUtil.TipoConsegna> tipiConsegna = tipiSpeseConsegne.tipiSpeseDiSpedizioneVenditeDirette.get(quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c);
                        if(tipiConsegna != null){
                            for(ServiceUtil.TipoConsegna tc : tipiConsegna){
                                res.add(new SelectOption(tc.codicesap,tc.tipoconsegna));
                            }
                        }
                    }
                }
                return res;
            }
        }
    }
    public Boolean doInitSpese = true;
    public Boolean skipSpeseAbbonamenti {get;set;}
    public PageReference initSpese(){
        if(doInitSpese){
            try {
                speseDiSpedizioneError = false;
                List<zqu__QuoteRatePlanCharge__c> charges = [SELECT Id,Supporto__c,SkipSpesediSpedizione__c,Tipo_Charge__c
                                                            FROM zqu__QuoteRatePlanCharge__c
                                                            WHERE Quote_Id__c = :quoteObj.Id];
                Boolean soloDigitale = true;
                skipSpeseAbbonamenti = true;
                for(zqu__QuoteRatePlanCharge__c charge : charges){
                    if(charge.Supporto__c != 'Digitale' && charge.Supporto__c != null) soloDigitale = false;
                    if(charge.Tipo_Charge__c == 'Abbonamento' && charge.SkipSpesediSpedizione__c == false) skipSpeseAbbonamenti = false;
                }

                if(!soloDigitale){ 
                    String nazioneSpedizione = getCodiceNazione();
                    if(!speseDiSpedizioneError){
                        ServiceUtil.TipiSpeseDiSpedizioneResponse response = null;
                        if(!Test.isRunningTest()){
                            response = ServiceUtil.invokeTipiSpeseDiSpedizione(nazioneSpedizione);
                        }else{
                            response = new ServiceUtil.TipiSpeseDiSpedizioneResponse();
                            ServiceUtil.TipoConsegna tc = new ServiceUtil.TipoConsegna();
                            tc.tipoconsegna = 'Corriere Italia';
                            tc.codiceSap = '0';
                            response.tipiSpeseDiSpedizioneAbbonamenti.put('Corriere Italia',new List<ServiceUtil.TipoConsegna>{tc});
                            response.tipiSpeseDiSpedizioneVenditeDirette.put('Corriere Italia',new List<ServiceUtil.TipoConsegna>{tc});
                        }
                        System.debug(loggingLevel.Error, '*** response: ' + response);
                        tipiSpeseConsegne = response;
                        if(String.isNotBlank(response.error)){
                            throw new WizardQuoteExtensionException('Errore durante la ricerca dei tipi di spedizione applicabili. '+response.error);
                        }
                        tipiSpeseSpedizione = new Map<String,List<SelectOption>>();
                        
                        if(quoteObj.Metodo_di_pagamento__c != 'Contrassegno'){
                            tipiSpeseSpedizione.put('VenditeDirette', new List<SelectOption>{new SelectOption('','--Seleziona--')});
                            for(String vd : response.tipiSpeseDiSpedizioneVenditeDirette.keySet()){
                                if(vd!= 'Contrassegno'){
                                    tipiSpeseSpedizione.get('VenditeDirette').add(new SelectOption(vd,vd));
                                    if(vd == 'Standard' && isTipoSpeseDiSpedizioneVenditaDirettaVisibile){ //default
                                        quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = vd;
                                        quoteObj.Tipo_Consegna_Vendite_Dirette__c = null;
                                    }
                                }
                            }
                        }else{
                            tipiSpeseSpedizione.put('VenditeDirette', new List<SelectOption>{new SelectOption('Contrassegno','Contrassegno')});
                            quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = 'Contrassegno';
                            quoteObj.Tipo_Consegna_Vendite_Dirette__c = 'COCO';
                            isContrassegno = true;
                        }

                        tipiSpeseSpedizione.put('Abbonamenti', new List<SelectOption>{new SelectOption('','--Seleziona--')});
                        for(String vd : response.tipiSpeseDiSpedizioneAbbonamenti.keySet()){
                            String labelAbb = vd;
                            CustomPermissions__c custPerm = CustomPermissions__c.getInstance(UserInfo.getProfileId());
                            if(!custPerm.GranttuttiiTipiconsegnaAbbonamento__c && vd == 'Corriere Italia') continue;
                            if(vd == 'Standard Italia'){
                                //labelAbb = nazioneSpedizione == 'IT' ? 'Spese di spedizione' : vd;
                                if(isTipoSpeseDiSpedizioneAbbonamentoVisibile){ // default
                                    quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = vd;
                                    quoteObj.Tipo_Consegna_Abbonamento__c = 'ZL';
                                }
                            }
                            //impostazioni di default per spedizione abbonamento estero
                            if(vd == 'Standard Estero'){
                                if(isTipoSpeseDiSpedizioneAbbonamentoVisibile){
                                    quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = vd;
                                    quoteObj.Tipo_Consegna_Abbonamento__c = 'ZM';
                                }
                            }
                            tipiSpeseSpedizione.get('Abbonamenti').add(new SelectOption(vd,labelAbb));
                        }
                        if(options == null){
                            options = new zqu.SelectProductComponentOptions();
                            options.mode = zqu.SelectProductComponentOptions.MODE_DETAIL;
                            options.quoteId = quoteObj.Id;
                        }

                        calcolaSpeseDiSpedizione();
                        zqu__Quote__c qu = new zqu__Quote__c(Id = quoteObj.Id, zqu__Service_Activation_Date__c = null);
                        update qu;
                    }
                }else{
                    quoteObj.Tipo_Spese_Spedizione_Abbonamento__c = null;
                    quoteObj.Tipo_Consegna_Abbonamento__c = null;
                    quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c = null;
                    quoteObj.Tipo_Consegna_Vendite_Dirette__c = null;
                    return goToEnd();
                }
            } catch(Exception e) {
                appendErrorMessage(e);
            }            
        }
        doInitSpese = false;
        return null;
    }
    public Boolean isTipoSpeseDiSpedizioneVenditaDirettaVisibile {
        get{
            return [SELECT count() FROM zqu__QuoteRatePlanCharge__c WHERE Tipo_Charge__c = 'Vendita Diretta' AND Supporto__c != 'Digitale' AND Supporto__c != null AND Quote_Id__c = :quoteObj.Id] > 0;
        }
    }
    public Boolean isTipoSpeseDiSpedizioneAbbonamentoVisibile {
        get{
            return [SELECT count() FROM zqu__QuoteRatePlanCharge__c WHERE Tipo_Charge__c = 'Abbonamento' AND Supporto__c != 'Digitale' AND Supporto__c != null AND Quote_Id__c = :quoteObj.Id] > 0;
        }
    }
    public ServiceUtil.SpeseDiSpedizioneResponse speseDiSpedizioneResponse {get;set;}
    public zqu.SelectProductComponentOptions options { get; set; }
    public Boolean speseDiSpedizioneError {get;set;}

    public void blankTipiConsegna(){
        quoteObj.Tipo_Consegna_Abbonamento__c = null;
        quoteObj.Tipo_Consegna_Vendite_Dirette__c = null;
        speseDiSpedizioneResponse = null;
        deleteSpeseDiSpedizione();
    }

    public void deleteSpeseDiSpedizione(){
        // cancello le spese di spedizione per vendite dirette
        if([SELECT count() FROM zqu__QuoteRatePlan__c WHERE zqu__Quote__c = :quoteObj.Id AND Rate_Plan_Type__c = 'Spese Spedizione'] > 0){
            Id ratePlanId = [SELECT Id FROM zqu__QuoteRatePlan__c WHERE zqu__Quote__c = :quoteObj.Id AND Rate_Plan_Type__c = 'Spese Spedizione'].Id;
            DomusUtil.removeRatePlan(quoteObj.Id,ratePlanId);
        }

        // cancello le spese di spedizione abbonamento
        List<zqu__QuoteRatePlanCharge__c> chargesAbbonamento = [SELECT Id,zqu__QuoteRatePlan__c, zqu__Total__c,zqu__ListPrice__c,zqu__EffectivePrice__c,zqu__Quantity__c, Testata__c
                                                                FROM zqu__QuoteRatePlanCharge__c 
                                                                WHERE zqu__QuoteRatePlan__r.Rate_Plan_Type__c = 'Spese Spedizione'
                                                                AND zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteObj.Id
                                                                AND Tipo_Charge__c = 'Spese di Spedizione'];
        for(zqu__QuoteRatePlanCharge__c chargeAbbonamento : chargesAbbonamento){
            chargeAbbonamento.zqu__Total__c = 0;
            chargeAbbonamento.zqu__ListPrice__c = 0;
            chargeAbbonamento.zqu__EffectivePrice__c = 0;
        }
        update chargesAbbonamento;
    }

    public String getCodiceNazione(){
        try {
            speseDiSpedizioneError = false;
            String codiceNazione = '';
            String objId = '';
            if(quoteObj.zqu__ZuoraAccountID__c == null){ // nuovo billing
                Contact c = [SELECT Codice_Nazione__c FROM Contact WHERE Id = :quoteObj.zqu__SoldToContact__c];

                codiceNazione = c.Codice_Nazione__c;
                objId = c.Id;

            }else{ // billing esistente
                Zuora__CustomerAccount__c a = [SELECT Sold_To_Salesforce__r.Codice_Nazione__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :quoteObj.zqu__ZuoraAccountID__c];
                
                codiceNazione = a.Sold_To_Salesforce__r.Codice_Nazione__c;
                objId = a.Id;
            }

            if(String.isBlank(codiceNazione)){
                throw new WizardQuoteExtensionException('Codice nazione non trovato.<br/>Clicca <a href="/'+objId+'" target="_blank">qui</a> per verificare i dati.');
            }
            return codiceNazione;
        } catch(Exception e) {
            speseDiSpedizioneError = true;
            appendErrorMessage(e);
        }
        return null;
    }

    public PageReference calcolaSpeseDiSpedizione(){
        try {
            if(!speseDiSpedizioneError){
                if(
                    (isTipoSpeseDiSpedizioneVenditaDirettaVisibile && (String.isBlank(quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c) || String.isBlank(quoteObj.Tipo_Consegna_Vendite_Dirette__c))) ||
                    (isTipoSpeseDiSpedizioneAbbonamentoVisibile && (String.isBlank(quoteObj.Tipo_Spese_Spedizione_Abbonamento__c) || String.isBlank(quoteObj.Tipo_Consegna_Abbonamento__c))) ){
                    speseDiSpedizioneResponse = null;
                    return null;
                }

                String tipoConsegnaAbbonamento = null;
                if(quoteObj.Tipo_Spese_Spedizione_Abbonamento__c != null){
                    List<ServiceUtil.TipoConsegna> tipiConsegna = tipiSpeseConsegne.tipiSpeseDiSpedizioneAbbonamenti.get(quoteObj.Tipo_Spese_Spedizione_Abbonamento__c);
                    if(tipiConsegna != null){
                        for(ServiceUtil.TipoConsegna tc : tipiConsegna){
                            if(quoteObj.Tipo_Consegna_Abbonamento__c == tc.codicesap){
                                tipoConsegnaAbbonamento = tc.tipoconsegna;
                                break;
                            }
                        }
                    }
                }

                String tipoConsegnaVD = null;
                System.debug(loggingLevel.Error, '*** quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c: ' + quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c);
                System.debug(loggingLevel.Error, '*** quoteObj.Tipo_Consegna_Vendite_Dirette__c: ' + quoteObj.Tipo_Consegna_Vendite_Dirette__c);
                if(quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c != null){
                    List<ServiceUtil.TipoConsegna> tipiConsegna = tipiSpeseConsegne.tipiSpeseDiSpedizioneVenditeDirette.get(quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c);
                    System.debug(loggingLevel.Error, '*** tipiConsegna: ' + tipiConsegna);
                    if(tipiConsegna != null){
                        for(ServiceUtil.TipoConsegna tc : tipiConsegna){
                            if(quoteObj.Tipo_Consegna_Vendite_Dirette__c == tc.codicesap){
                                tipoConsegnaVD = tc.tipoconsegna;
                                break;
                            }
                        }
                    }
                }
                System.debug(loggingLevel.Error, '*** tipoConsegnaVD: ' + tipoConsegnaVD);

                speseDiSpedizioneResponse = ServiceUtil.invokeSpeseDiSpedizione(
                    quoteObj.Id,
                    quoteObj.Tipo_Spese_Spedizione_Abbonamento__c,
                    tipoConsegnaAbbonamento,
                    quoteObj.Tipo_Spese_Spedizione_Vendite_Dirette__c,
                    tipoConsegnaVD,
                    quoteObj.Tipo_Consegna_Vendite_Dirette__c,
                    getCodiceNazione(),
                    skipSpeseAbbonamenti
                );
                if(String.isNotBlank(speseDiSpedizioneResponse.error)){
                    throw new WizardQuoteExtensionException('Errore durante il calcolo delle spese di spedizione. '+speseDiSpedizioneResponse.error);
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return ApexPages.currentPage();
    }

    public PageReference goToEnd(){
        PageReference res = null;
        

        quote.save();
        quoteObj = quote.getSObject();

        if (isTipoSpeseDiSpedizioneVenditaDirettaVisibile && String.isBlank(quoteObj.Tipo_Consegna_Vendite_Dirette__c)) {
            appendErrorMessage(AppConstants.INVALID_CONSEGNA_VENDITA_DIRETTA);            
            return null;
        }

        if (isTipoSpeseDiSpedizioneAbbonamentoVisibile && String.isBlank(quoteObj.Tipo_Consegna_Abbonamento__c)) {
            appendErrorMessage(AppConstants.INVALID_SPEDIZIONE_ABBONAMENTO);
            return null;
        }

        System.debug(loggingLevel.Error, '*** quoteObj.zqu__Previewed_SubTotal__c: ' + quoteObj.zqu__Previewed_SubTotal__c);
        //quoteObj = DomusUtil.updateTotals(quoteObj);
        System.debug(loggingLevel.Error, '*** quoteObj.zqu__Previewed_SubTotal__c: ' + quoteObj.zqu__Previewed_SubTotal__c);
        
        //quoteObj = DomusUtil.updateTotals(quoteObj);
        //Calculating and setting GrContabilizzazioneCliente__c on quote rate plan charge. 
        String grContCliente;
        if(quoteObj.zqu__ZuoraAccountID__c != null){

            String existingBillingAccountId = quoteObj.zqu__ZuoraAccountID__c;
            Zuora__CustomerAccount__c custAcc = [SELECT Id, Bill_To_Salesforce__c FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :existingBillingAccountId LIMIT 1]; //quoteObj.zqu__ZuoraAccountID__c
            
            if(custAcc != null){
                Id billToId = custAcc.Bill_To_Salesforce__c;
                Contact c = [SELECT Id,Codice_Nazione__c FROM Contact WHERE Id = :billToId LIMIT 1];
                if(c != null){
                    if(c.Codice_Nazione__c == 'IT'){
                        grContCliente = '01';
                    }else {
                        grContCliente = '02';
                    }
                }
            }
        
        }else{
            
            Id billToId = quoteObj.zqu__BillToContact__c;
            Contact c = [SELECT Id,Codice_Nazione__c, Cee_Nazione__c FROM Contact WHERE Id = :billToId LIMIT 1];
            
            if(c != null){
                if(c.Codice_Nazione__c == 'IT'){
                    grContCliente = '01';
                }else {
                    grContCliente = '02';
                }
            }

            String recordType = account.RecordType.DeveloperName;

            String tipoCliente;
            if(quoteObj.Split_Payment__c == 'SI'){
                tipoCliente = 'PUBBLICA_AMMINISTRAZIONE';
            }else{
                if(recordType.containsIgnoreCase('B2C')){
                    tipoCliente = 'B2C';
                }else if (recordType.containsIgnoreCase('B2B')){
                    tipoCliente = 'B2B';
                }else{
                    appendErrorMessage('Impossibile riconoscere il tipo del cliente');            
                    return null;
                }
            }

            String country;
            if(c.Codice_Nazione__c == 'IT'){
                country = 'ITALY';
            }else if(c.Codice_Nazione__c ==  'VA' || c.Codice_Nazione__c == 'SM'){
                country = 'SAN_MARINO_VAT';
            }else{
                country = c.Cee_Nazione__c;
            }

            String soldToCounty = country +'_'+tipoCliente;
            System.debug('soldToCounty:' + soldToCounty); 
            update new Contact(id=quoteObj.zqu__SoldToContact__c , zqu__County__c = soldToCounty);

        }

        Id quoteId = quoteObj.Id;
        List<zqu__QuoteRatePlanCharge__c> quoteChargeList = [SELECT Id, zqu__ProductRatePlanCharge__r.Supporto__c, zqu__ProductRatePlanCharge__r.PianodeiContilegatoaGrContCliente__c FROM zqu__QuoteRatePlanCharge__c 
            WHERE zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteId];

        for(zqu__QuoteRatePlanCharge__c qrpc : quoteChargeList){
            if(qrpc.zqu__ProductRatePlanCharge__r.PianodeiContilegatoaGrContCliente__c == 'SI'){
                /*** DH-362 ***/
                if (qrpc.zqu__ProductRatePlanCharge__r.Supporto__c == AppConstants.SUPPORTO_DIGITALE) {
                    qrpc.GrContabilizzazioneCliente__c = AppConstants.CODICE_DIGITALE;
                } else {
                    qrpc.GrContabilizzazioneCliente__c = grContCliente;
                }
                /*** DH-362 - Fine ***/
            }
        }
        update quoteChargeList;



        try {
            quoteObj.Quote_Status__c = 'Confirmed';
            update quoteObj;
            //quote.save();
            //DomusUtil.updateTotals(quoteObj.Id);
            res = new PageReference('/'+quoteObj.Id);
        } catch(Exception e) {
            appendErrorMessage(e);
        }

        return res;
    }

    public PageReference exitWizard(){
        PageReference res = null;
        try {
            if(quoteObj.Subscription_Precedente__c != null){
                res = new PageReference('/'+quoteObj.Subscription_Precedente__c);
            }
            else if(quoteIdEdit != null){
                res = new PageReference('/'+quoteObj.Id);
            }else{
                if(quoteObj.zqu__Account__c != null){
                    res = new PageReference('/'+quoteObj.zqu__Account__c);
                }else{
                    res = new PageReference('/001');
                }
            }
            quoteObj.Quote_Status__c = 'Cancelled';
            quote.save();
        } catch(Exception e) {
            appendErrorMessage(e);
        }
        return res;
    }

    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }

    public void doNothing(){}

     public List<SelectOption> getfatturaList(){
        list<selectoption> options = new list<selectoption>();   
              
        try {          
            
            Schema.DescribeFieldResult fieldResult = Schema.zqu__Quote__c.Fattura_a__c.getDescribe();
            
            list<schema.picklistentry> values = fieldResult.getPickListValues();               
            for (Schema.PicklistEntry a : values) {                  
                options.add(new SelectOption(a.getLabel(), a.getLabel()));
            }           
        }catch (Exception e){             
            pageMessage = e.getmessage();
            showMessage=true;
            return null;        
        }
        return options; 
    }
}