global class ScheduledInformativa implements Schedulable {

	global void execute(SchedulableContext sc) {
		getInformativaFuture();
    }

	@future(callout = true)
	public static void getInformativaFuture(){
		try{
			HttpResponse versionResponse = getPrivacyVersion();
			if(versionResponse.getStatusCode() == 200){
				HttpResponse textResponse = getPrivacyText(versionResponse.getBody());
				if(textResponse.getStatusCode() == 200){
					String privacyVersion = versionResponse.getBody();
					String privacyText = textResponse.getBody();
					executeUpdate(privacyVersion, privacyText);
				}
			}
		}catch(CalloutException e){
			System.debug(LoggingLevel.ERROR, 'An error has occured during callout: ' + e.getMessage());
		}catch(DmlException e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred during record update: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }catch(QueryException e){
			System.debug(LoggingLevel.ERROR, 'An error has occurred during record retrieve: ' + e.getMessage() + ' on line ' + e.getLineNumber());
		}catch(Exception e){
            System.debug(LoggingLevel.ERROR, 'An error has occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber());
        }
	}

	private static HttpResponse getPrivacyVersion(){
		String endpoint = Setting__c.getInstance('getPrivacyVersion').URL__c;
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setMethod('GET');
		request.setHeader('Content-Type', 'application/json');
		request.setEndpoint(endpoint);
		HttpResponse response = http.send(request);
		return response;
	}

	private static HttpResponse getPrivacyText(String result){
		String endpoint = Setting__c.getInstance('getPrivacyText').URL__c + '?version=' + result;
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setMethod('GET');
		request.setHeader('Content-Type', 'application/json');
		request.setEndpoint(endpoint);
		HttpResponse response = http.send(request);
		return response;
	}

	private static void executeUpdate(String privacyVersion, String privacyText){
		Informativa_Privacy__c informativa = InformativaUtils.getInformativaByName('Informativa Corrente');	
		String privacyUrl = Setting__c.getInstance('getPrivacyText').URL__c + '?version=' + privacyVersion;
		informativa.Name = 'Informativa Corrente';
		informativa.Testo_Informativa__c = privacyText;
		informativa.Versione_Informativa__c = privacyVersion;
		informativa.URL_Informativa__c = privacyUrl;
		upsert informativa;
	}

	/*
	private static void executeUpdate(String privacyVersion, String privacyText){
		Informativa_Privacy__c informativa = InformativaUtils.getInformativa();
		if(informativa != null){
			if(Integer.valueOf(informativa.Versione_Informativa__c) < Integer.valueOf(privacyVersion)){
				String privacyUrl = Setting__c.getInstance('getPrivacyText').URL__c + '?version=' + privacyVersion;
				informativa.Testo_Informativa__c = privacyText;
				informativa.Versione_Informativa__c = privacyVersion;
				informativa.URL_Informativa__c = privacyUrl;
				update informativa;
			}
		}
	}
	*/
}