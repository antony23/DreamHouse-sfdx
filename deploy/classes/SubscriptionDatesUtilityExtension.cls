public class SubscriptionDatesUtilityExtension {

    public Asset[] copie {get;set;}
    public String copieDaSospendere {get;set;}
    public String primaCopiaDaSospendere {get;set;}

    public SelectOption[] copieOptions {get;private set;}
    public SelectOption[] copieDaSospendereOptions {get;private set;}
    private Set<Id> testate;

    public Date startDateSosp {get;set;}
    public Date endDateSosp {get;set;}
    public Integer numDaysSosp {get;set;}

    private Zuora__Subscription__c sub;

    /** INIZIO CR PROROGHE **/
    public Boolean showCalculateButton {get;set;}
    public Boolean showSuspendButton {get;set;}
    public String suspensionType {get;set;}
    public String tipoSospensione {get;set;}
    public list<SelectOption> getTipiSospensione(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Sospensione', 'Sospensione'));
        options.add(new SelectOption('Proroga', 'Proroga'));
        return options;
    }
    /** FINE CR PROROGHE **/

    public SubscriptionDatesUtilityExtension(ApexPages.StandardController controller) {
        
        if(!Test.isRunningTest()) controller.addFields(new List<String>{'Zuora__Account__c'});
        sub = (Zuora__Subscription__c) controller.getRecord(); 
        
        Id subId = controller.getId();

        showCalculateButton = true;
        showSuspendButton = false;

        /*
        copie = [Select Id,Name,Calendario__c,Calendario__r.Testata__c,Calendario__r.Name,Calendario__r.Data_Uscita__c From Asset 
                    Where Tipo_Copia__c = 'Standard' 
                    And SubscriptionAllCopy__c =: subId
                    Order By Calendario__r.Data_Uscita__c];
        */

        //*** DH-706 ***//
        copie = [Select Id,Name,Calendario__c,Calendario__r.Testata__c,Calendario__r.Name,Calendario__r.Data_Uscita__c From Asset 
                Where SubscriptionAllCopy__c =: subId
                Order By Calendario__r.Data_Uscita__c];
        //*** DH-706 - Fine ***//

        testate = new Set<Id>();

        copieOptions = new SelectOption[]{};

        for(Asset a : copie){
            copieOptions.add(new SelectOption(a.Calendario__c,a.Name));
            testate.add(a.Calendario__r.Testata__c);
        }

        copieDaSospendereOptions = new SelectOption[]{};
        for(Integer i=1;i<=12;i++){
            copieDaSospendereOptions.add(new SelectOption(i+'',i+''));
        }
    }

    public PageReference goBack(){
        PageReference pr = new PageReference('/'+sub.Zuora__Account__c);
        return pr;
    }

    /** Inizio CR PROROGHE **/

    /*
     *  Calcola la data di sospensione, la data di riattivazione e il numero di giorni di sospensione dati la prima copia da sospendere e
     *  il numero di copie da sospendere a partire da essa.
     *  Il metodo supporta subscription con testate singole, multiple, pubblicazioni regolari (e.g. ogni mese, ogni due mesi, ecc.) ed irregolari.
     *
     *  KNOWN ISSUES:
     *  - A causa dei limiti di Zuora le copie di gracing, essendo fuori dal periodo di esistenza della subscription, 
     *    non sono prorogabili ;
     *  - A causa dei limiti di Zuora, se si tenta di prorogare l'ultima copia (non di gracing) di una subscription,
     *    è necessario effettuare prima un'estensione dei termini fino alla data indicata da "Data riattivazione" e poi
     *    una sospensione (senza estensione dei termini) ;
     */

    public void calculate(){
        try{
            Calendario__c primaCopiaDaSosp = [ SELECT Testata__c, Testata__r.Name, Data_Uscita__c FROM Calendario__c WHERE Id =: primaCopiaDaSospendere ];
            Zuora__Subscription__c subscription = [ SELECT Testate__c FROM Zuora__Subscription__c WHERE Id = :sub.Id ];

            Integer copieDaSospendereInt = Integer.valueOf(copieDaSospendere);
            Integer numOfTestate = calculateTestate(sub.Id).size();
            Integer firstIndex = -1, lastIndex = -1;

            // Calcola firstIndex
            for(Integer i = 0; i < copie.size(); ++i) {
                if(copie[i].Calendario__c.equals(primaCopiaDaSosp.Id)) {
                    firstIndex = i;
                    break;
                }
            }

            // Calcola lastIndex
            Calendario__c lastMatch = null;
            for(Integer i = firstIndex, matches = 0; i < copie.size() && matches < copieDaSospendereInt; ++i) {
                if(lastMatch == null || !copie[i].Calendario__r.Data_Uscita__c.isSameDay(lastMatch.Data_Uscita__c)) {
                    lastMatch = copie[i].Calendario__r;

                    if(++matches == copieDaSospendereInt) {
                        lastIndex = i;
                        break;
                    }
                }
            }

            // Valida gli indici calcolati
            if(firstIndex < 0 || lastIndex < 0 || lastIndex >= copie.size()) {
                System.debug(loggingLevel.Error, 'Impossibile trovare una combinazione di date.');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Impossibile trovare una combinazione di date.'));
                return;
            }


            String ultimaCopiaDaSospendere = copie[lastIndex].Calendario__c;
            Calendario__c ultimaCopiaDaSosp = [ SELECT Testata__c, Data_Uscita__c FROM Calendario__c WHERE Id =: ultimaCopiaDaSospendere ];
            Calendario__c postUltimaCopia = [
                                                SELECT Testata__c, Data_Uscita__c 
                                                FROM Calendario__c 
                                                WHERE Testata__c = :primaCopiaDaSosp.Testata__c AND Data_Uscita__c > :ultimaCopiaDaSosp.Data_Uscita__c 
                                                ORDER BY Data_Uscita__c ASC 
                                                LIMIT 1
                                            ];
            Decimal avgPublicationOffset = calculateAveragePublicationOffset(primaCopiaDaSosp.Testata__r.Name);
            Boolean isLinear = !avgPublicationOffset.toPlainString().containsAny('.,');    // la pubblicazione avviene in modo lineare (e.g ogni mese, ogni due mesi, ecc.)

            // Calcola le date
            startDateSosp = primaCopiaDaSosp.Data_Uscita__c;
            endDateSosp = (postUltimaCopia != null && numOfTestate == 1 && isLinear) 
                                ? postUltimaCopia.Data_Uscita__c.addDays(-1)
                                : ultimaCopiaDaSosp.Data_Uscita__c.addMonths(1).addDays(-1);
            numDaysSosp = startDateSosp.daysBetween(endDateSosp);
            suspensionType = tipoSospensione;
            showSuspendButton = true;

            // Debug Logs
            System.debug(loggingLevel.Error, '*** subStart: ' + sub.Zuora__SubscriptionStartDate__c);
            System.debug(loggingLevel.Error, '*** subEnd: ' + sub.Zuora__SubscriptionEndDate__c);
            System.debug(loggingLevel.Error, '*** firstSuspendedCopy: ' + primaCopiaDaSosp);
            System.debug(loggingLevel.Error, '*** lastSuspendedCopy: ' + ultimaCopiaDaSosp);
            System.debug(loggingLevel.Error, '*** numberOfSuspensionDays: ' + numDaysSosp);
        }catch(Exception e){
            startDateSosp = null;
            endDateSosp = null;
            numDaysSosp = null;
            suspensionType = null;
            showSuspendButton = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }
    }

    public void resetSuspendButton(){
        if(showSuspendButton) showSuspendButton = false;
    }

    public void suspend(){
        try{
            Zuora__Subscription__c subscription = fetchSubscription(sub.Id);
            Zuora.zApi.AmendResult result = AmendmentUtil.suspend(subscription, startDateSosp, endDateSosp, true, suspensionType);
            if(result.Success){
                showCalculateButton = false;
                showSuspendButton = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Amendment inviato correttamente'));
            }else if(result.Errors != null) {
                showSuspendButton = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, (String) result.Errors[0].getValue('Message') ));
            }
        }catch(Exception e){
            resetField();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }
    }

    private static Zuora__Subscription__c fetchSubscription(Id subscriptionId){
        String query = 'SELECT ' + String.join(fieldsOf('Zuora__Subscription__c'), ',') + ' FROM Zuora__Subscription__c WHERE Id = :subscriptionId LIMIT 1';
        List<Zuora__Subscription__c> subscriptionList = (List<Zuora__Subscription__c>) Database.query(query);
        if(subscriptionList.isEmpty()) throw new DomusException.RecordNotFoundException('Record not found for id: "' + subscriptionId + '"');
        return subscriptionList[0];
    }

    private static List<String> fieldsOf(String className){
        List<String> fields = new List<String>();
        for (Schema.SObjectField ft : Schema.getGlobalDescribe().get(className).getDescribe().Fields.getMap().values()){
            fields.add(ft.getDescribe().getName().toLowerCase());
        }
        return fields;
    }

    private void resetField(){
        startDateSosp = null;
        endDateSosp = null;
        numDaysSosp = null;
        suspensionType = null;
        showCalculateButton = false;
        showSuspendButton = false;
    }

    /**
     *  Recupera il numero effettivo di testate presenti all'interno dell'abbonamento.
     *
     *  @param subId id della subscription da analizzare
     *  @return set di testate
    **/

    private static Set<String> calculateTestate(Id subId) {
        final Set<String> testate = new Set<String>();
        final List<Asset> issues = [SELECT Id, Name, Testata__c FROM Asset WHERE SubscriptionAllCopy__c = :subId];

        for(Asset issue : issues) {
            testate.add(issue.Testata__c);
        }

        return testate;
    }

    /**
     *  Calcola il periodo di tempo che intercorre tra un'uscita e la successiva di una testata.
     *
     *  @param testata Nome della testata di cui calcolare il periodo medio di pubblicazione.
    **/

    private static Decimal calculateAveragePublicationOffset(String testata) {
        final List<Calendario__c> dummyCalendar = [SELECT Data_Uscita__c FROM Calendario__c WHERE Testata__r.Name = :testata ORDER BY Data_Uscita__c DESC LIMIT 1 ];
        final Integer dummyYear = (dummyCalendar.size() > 0) ? dummyCalendar.get(0).Data_Uscita__c.year() : Date.today().year();

        final List<Calendario__c> copie = [SELECT Name, Testata__c, Data_Uscita__c FROM Calendario__c WHERE Testata__r.Name = :testata AND CALENDAR_YEAR(Data_Uscita__c) = :dummyYear ORDER BY Data_Uscita__c ASC];
        Decimal avgPubOffset = 0;

        if(copie.size() > 0) {
            for(Integer i = 0; i < copie.size() - 1; i++) {
                avgPubOffset += copie[i].Data_Uscita__c.monthsBetween(copie[i + 1].Data_Uscita__c);
            }

            avgPubOffset += copie[copie.size() - 1].Data_Uscita__c.monthsBetween(copie[0].Data_Uscita__c.addYears(1));

            return avgPubOffset / copie.size();
        }

        return 0;
    }

    /** Fine CR PROROGHE **/
}