@isTest (SeeAllData = true)
private class ChangeOwnerCaseExtensionTest{

	@isTest
	static void itShould(){
		Case c = [SELECT Id,OwnerId FROM Case LIMIT 1];
		ChangeOwnerCaseExtension ext = new ChangeOwnerCaseExtension(new ApexPages.StandardController(c));
		ext.changeOwner();
		ext.c.OwnerId = [SELECT Id FROM User WHERE IsActive = true AND Id != :c.OwnerId LIMIT 1].Id;
		ext.changeOwner();
		for(ChangeOwnerCaseExtension.UserWrapper uw : ext.users){
			uw.selected = true;
		}
		ext.changeOwner();
	}
}