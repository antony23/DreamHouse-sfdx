global class BatchUnlockMovStock implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
	
	List<Id> idsList;

	global BatchUnlockMovStock(Zuora__Subscription__c sub){
		idsList = new List<Id>();
		idsList.add(sub.Id);
	}

	global BatchUnlockMovStock(List<Zuora__Subscription__c> subList){
		idsList = new List<Id>();
		for(Zuora__Subscription__c sub : subList){
			idsList.add(sub.Id);
		}
	}

	global BatchUnlockMovStock(List<Movimentazione_Stock__c> msList){
		idsList = new List<Id>();
		for(Movimentazione_Stock__c ms : msList){
			idsList.add(ms.Subscription__c);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator('SELECT Id,PVD_Disabilitati__c,Movimentazione_da_inviare__c FROM Zuora__Subscription__c WHERE Id IN :idsList');
	}

	global void execute(Database.BatchableContext BC, List<SObject> scope){
		try{
			for(Zuora__Subscription__c agg : (List<Zuora__Subscription__c>)scope){
				agg.PVD_Disabilitati__c = '';
				agg.Movimentazione_da_inviare__c = true;
			}

			update scope;

		}catch(Exception ex){
			System.debug(ex.getMessage()+' - '+ex.getStackTraceString());
		}
	}

	global void finish(Database.BatchableContext BC){

	}
}