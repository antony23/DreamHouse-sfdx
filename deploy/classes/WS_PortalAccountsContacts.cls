global class WS_PortalAccountsContacts {

	global class WS_PortalAccountsContacts_Request{
		webservice List<InputElement> elements;
	}
	
	global class WS_PortalAccountsContacts_Response{
		webservice List<OutputElement> elements;
		webservice OperationStatus operationStatus;
	}

	global class InputElement{
		webservice Account account;
		webservice Contact contact;
	}

	global class OutputElement{
		webservice Account account;
		webservice Contact contact;
		webservice String idWeb;
		webservice id contactId;
		webservice String respMsg;
		webservice Boolean errore;
	}

	global class OperationStatus{
		webservice Boolean isError;
		webservice String errorMessage;
	}

	public class AccountUpsertResult{
		public Boolean isSuccess {get;set;}
		public Id accountId {get;set;}
		public Account originalAccount {get;set;}
		public Contact contact {get;set;}
		public String operationStatus {get;set;}
		public FailureReason failureReason {get;set;}
	}

	public class  ContactUpsertResult{
		public Boolean isSuccess {get;set;}
		public Id contactId {get;set;}
		public Contact contact {get;set;}
		public String operationStatus {get;set;}
		public FailureReason failureReason {get;set;}
	}

	public class FailureReason{
		public Boolean isDuplicate {get;set;}
		public Id duplicateAccountId {get;set;}
		public Id duplicateContactId {get;set;}
		public Boolean isException {get;set;}
		public String exceptionDetailMessage {get;set;}
	}

	webservice static WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response insertAccountContact(WS_PortalAccountsContacts.WS_PortalAccountsContacts_Request request){

		WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response response = new WS_PortalAccountsContacts.WS_PortalAccountsContacts_Response();
		response.elements = new List<WS_PortalAccountsContacts.OutputElement>();
		response.operationStatus = new WS_PortalAccountsContacts.OperationStatus();
		response.operationStatus.isError = false;

		Savepoint sp = !Test.isRunningTest() ? Database.setSavepoint() : null;

		try {
				AccountTriggerHandler.skip = true;
        		//ContactTriggerHandler.skip = true;
        		if(request.elements !=null && !request.elements.isEmpty() && request.elements[0].contact.id != null) {
        			System.debug('************request size **************' + request.elements.size());
        			id ctid = request.elements[0].contact.id;
 					Account accountToUpdate = request.elements[0].account;
 					Contact contactToUpdate = request.elements[0].contact;
 					System.debug('************ WS_PortalAccountsContacts + accountToUpdate***** ' + accountToUpdate);
 					System.debug('************ WS_PortalAccountsContacts + contactToUpdate***** ' + contactToUpdate);
 					Contact ct = [SELECT id,AccountId,MainContact__c FROM Contact where id = :ctid];
 					id  acctid = [SELECT id FROM Account WHERE id = :ct.AccountId ].id;
 					accountToUpdate.id = acctid;
 					System.debug('************ WS_PortalAccountsContacts + accountToUpdate.id***** ' + accountToUpdate.id + '*****' + acctid);
 					Database.DMLOptions dml = new Database.DMLOptions();
                	dml.DuplicateRuleHeader.AllowSave = true;
                	dml.OptAllOrNone = false;

                	contactToUpdate.MainContact__c = ct.MainContact__c;
 					
 					Database.SaveResult accountresult = Database.update(accountToUpdate,dml);
 					Database.SaveResult contactresult = Database.update(contactToUpdate,dml); 

 					System.debug('************ WS_PortalAccountsContacts + accountresult**** ' + accountresult);
 					System.debug('************ WS_PortalAccountsContacts + contactresult**** ' + contactresult);
 					if( accountresult.isSuccess() && contactresult.isSuccess() ){
 						WS_PortalAccountsContacts.OutputElement outElem = new WS_PortalAccountsContacts.OutputElement();
	            		outElem.account = accountToUpdate;
	            		outElem.contact = contactToUpdate; 
	       				response.elements.add(outElem);
	       				response.elements[0].contact = contactToUpdate;
						response.elements[0].contactid = contactToUpdate.id;
						response.elements[0].idWeb = contactToUpdate.id_Web__c;

 					} else {
 						response.operationStatus.isError = true;
 					}

        		}else if(request.elements != null && !request.elements.isEmpty()) {

        			System.debug('************ WS_PortalAccountsContacts + else if **** ');

        			List<Account> accounts = new List<Account>();
        			List<Contact> contacts = new List<Contact>();
        			
        			for(WS_PortalAccountsContacts.InputElement inElem : request.elements){
        				accounts.add(inElem.account);
        				contacts.add(inElem.contact);
        			}

            		List<AccountUpsertResult> accountUpsertResultList = new List<AccountUpsertResult>();
        			List<Account> accountsDuplicatiDaAggiornare = new List<Account>();
        			Set<Id> accountsDuplicatiDaAggiornareIds = new Set<Id>();
        			List<Integer> accountsDuplicatiDaAggiornarePosizioni = new List<Integer>();
        			List<String> errorMsgAcct = new List<String>() ;
        			//isert Account
        			List<Database.SaveResult> accountUpsertResult = Database.insert(accounts, false);
        			Database.Error error;
        			for(Integer i=0; i<accountUpsertResult.size(); i++){
        				System.debug('***********STO INSERENDO GLI ACCOUNT *************');
        				Database.SaveResult accountUpsertResultElem = accountUpsertResult.get(i);
        				AccountUpsertResult aur = new AccountUpsertResult();
	                	aur.isSuccess = accountUpsertResultElem.isSuccess();
	                	aur.originalAccount = accounts.get(i);
	                	System.debug('***********************ORIGINAL ACCOUNT****************************' + aur.originalAccount + ' ' + '*************SUCCESS**************'  + aur.isSuccess );
	                	aur.contact = contacts.get(i);
	                	aur.accountId = aur.isSuccess ? accountUpsertResultElem.getId() : null;

	                	if(aur.isSuccess){
	                		aur.operationStatus = 'Account ' +  'created. ' ;
							aur.contact.accountId = accountUpsertResultElem.getId();
							aur.originalAccount.Id = accountUpsertResultElem.getId();
							errorMsgAcct.add('');
							System.debug('*********** ACCOUNT NON DUPLICATO *************' + aur.operationStatus);
                		} else {

                			aur.operationStatus = 'Error during insert for account.';
                			FailureReason fr = new FailureReason();
	                		error = accountUpsertResultElem.getErrors()[0];

	                		System.debug('*********** ACCOUNT DUPLICATO/ERRORE *************' + aur.operationStatus);
	                		if (error instanceof Database.DuplicateError) {

	                       		fr.isDuplicate = true;
	                		 	fr.isException = false;
                            	System.debug('*********** VERIFICO SE ACCOUNT DUPLICATO *************' + fr.isDuplicate);
                            	Database.DuplicateError duplicateError = (Database.DuplicateError) error;
                            	Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                            	List<Datacloud.MatchResult> matchResults = duplicateResult.getMatchResults();
                            	errorMsgAcct.add('');
                            	if(!matchResults.isEmpty()){
                               	 	Datacloud.MatchResult matchResult = matchResults.get(0);
                              		List<Datacloud.MatchRecord> matchRecords = matchResult.getMatchRecords();
                                	if(!matchRecords.isEmpty()){
                                    	Datacloud.MatchRecord matchRecord = matchRecords.get(0);
                                    	sObject accountDuplicato = matchRecord.getRecord();
                                    	fr.duplicateAccountId = (Id) accountDuplicato.get('Id');
                                    	aur.contact.accountId = (Id) accountDuplicato.get('Id');

                                    }
                                }

                               
	                		}else{
                        	fr.isDuplicate = false;
	                		fr.isException = true;
                            fr.exceptionDetailMessage = error.getMessage();
                           	errorMsgAcct.add(error.getMessage());	
                            System.debug('*********** ERRORE NON DUPLICATO *************' + fr.exceptionDetailMessage);
                            System.debug ('************** ERRORE ACCOUNT STRING FINALE *************' + errorMsgAcct );
                        }
                		}

                	       accountUpsertResultList.add(aur);	
        			}

        		System.debug('**********LISTA ACCOUNT ****************' + accountUpsertResultList);
        		// CONTATTI LEGATI A UN ACCOUNT ; DA INSERIRE
        		
        		List<Integer> posizioniContatti = new List<Integer>();
        		List<Contact> contattiTutti = new List<Contact>();
        		
        		for(Integer i=0; i<accountUpsertResultList.size(); i++){
	            	AccountUpsertResult aur = accountUpsertResultList.get(i);
	            	//if(aur.isSuccess == true ) {
	              		contattiTutti.add(aur.contact);
	            		posizioniContatti.add(i);
	            		System.debug('*************CONTATTI****************' + contattiTutti);
	            //	}
	            }

	          
	            List<Integer> posizioniContattiConID = new List<Integer>();
        		List<Contact> contattiConID= new List<Contact>();
				List<Integer> posizioniContattiSenzaID = new List<Integer>();
        		List<Contact> contattiSenzaID= new List<Contact>();
				List<ContactUpsertResult> contactUpsertResultList = new List<ContactUpsertResult>();
				List<Contact> contattiConIDdml= new List<Contact>();
				List<Contact> contattiSenzaIDdml= new List<Contact>();
				List<Contact> idToDelete = new List<Contact>();
				List<Contact> senzaIdToDelete = new List<Contact>();
			



	            for(Integer i=0; i<contattiTutti.size(); i++){
	            	
	            	
	            	if(contattiTutti[i].Id != null ){
	            		posizioniContattiConID.add(posizioniContatti[i]);
	              		contattiConID.add(contattiTutti[i]);
	              		if (errorMsgAcct[i] != '') idToDelete.add(contattiTutti[i]);  
	            	} else   {
	            			    posizioniContattiSenzaID.add(posizioniContatti[i]);
 			            		contattiSenzaID.add(contattiTutti[i]);
	              				if (errorMsgAcct[i] != '') senzaIdToDelete.add(contattiTutti[i]); 
 			            		
	            	}
	            }
	            
	            

	            System.debug('*************CONTATTI CON ID **************' + contattiConID );

	            System.debug('*************CONTATTI SENZA ID **************' + contattiSenzaID );
        	
        		
	            // se ci sono account non inseriti/aggiornati a causa della deduplica, aggiorno sull'account salesforce i codici (con skip della deduplica per sicurezza)
	           	Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                dml.OptAllOrNone = false;

                List<Database.SaveResult> contattiConIDResult = new List<Database.SaveResult>();
                List<Database.SaveResult> contattiSenzaIDResult = new List<Database.SaveResult>();

                // faccio l'update dei contatti che hanno già un id su SF skippando la deduplica	
	           	if ( !contattiConID.isEmpty()) {
	           	contattiConIDResult = Database.update(contattiConID, dml);
	            }
	           	// Adesso bisogna inserire i contatti che non hanno un id effettuando prima la deduplica

	           	/*** DH-493 ***/
	           	if(contattiSenzaID.size() > 0) {
	           		
	           		Set<Id> accountIds = new Set<Id>();
	           		for(Contact c : contattiSenzaID) {
	           			if(c.AccountId != null) {
	           				accountIds.add(c.AccountId);
	           			}
	           		}
	           	
		           	List<Contact> listaContatti = [SELECT Id, AccountId, MainContact__c FROM Contact WHERE AccountId IN :accountIds];
		           	for(Contact c : contattiSenzaID) {
		           		System.debug('*** ' + c.AccountId);
		           		System.debug('*** ' + c.MainContact__c);
		           		for(Contact con : listaContatti){
		           			System.debug('*** ' + con.AccountId);
		           			if(c.AccountId == con.AccountId){
		           				System.debug('*** ' + c.MainContact__c);
		           				System.debug('*** ' + con.MainContact__c);
		           				if(con.MainContact__c == true){
		           					c.MainContact__c = false;
		           					break;
		           				}
		           			}

		           		}
		           	}
		        }
	           	/*** DH-493 - Fine ***/

	           	if (!contattisenzaid.isEmpty()){
	           	contattiSenzaIDResult = Database.insert(contattiSenzaID, false);
	            }
	           	
	           	Map<Id, String>  coppiaidwebid = new Map<Id, String>();
	            
	            Set<Id> contactsDuplicatiDaAggiornareIds = new Set<Id>();
	           	List<Contact> contactsDuplicatiDaAggiornare = new List<Contact>();
	           	List<Integer> contactsDuplicatiDaAggiornarePosizioni = new List<Integer>();
	           	List<String> errorMsgCt = new List<String>() ;
	           	Database.Error errorCt;
	           	
	           	for (AccountUpsertResult aur : accountUpsertResultList){
	           		errorMsgCt.add('');
	           	}

	           	for (Integer i=0; i<contattiConIDResult.size(); i++){
	           		Database.SaveResult contattiConIDResultElem = contattiConIDResult.get(i);
        			ContactUpsertResult curId = new ContactUpsertResult();
	                curId.isSuccess = contattiConIDResultElem.isSuccess();
	               	curId.contact = contattiConID.get(i);
	               	Integer j = posizioniContattiConID[i];
	               	if(curId.isSuccess){
	                		curId.operationStatus = 'Contact ' +  'updated. ' ;
							curId.contact.Id = contattiConIDResultElem.getId();
							errorMsgCt[j]= '';
							System.debug('*********** VERIFICO INSERIMENTO CONTATTO CON ID*************' + curId.operationStatus );
                		} else{
                			errorCt = contattiConIDResultElem.getErrors()[0];
                			errorMsgCt[j]=errorCt.getMessage();
                			System.debug('**************ERRORE INSERIMENTO CONTATTO CON ID***********' + errorCt.getMessage());
                		}

	           	}

	           	for(Integer i=0; i<contattiSenzaIDResult.size(); i++) {

	           		    Database.SaveResult contattiSenzaIDResultElem = contattiSenzaIDResult.get(i);
        				ContactUpsertResult cur = new ContactUpsertResult();
	                	cur.isSuccess = contattiSenzaIDResultElem.isSuccess();
	                	cur.contact = contattiSenzaID.get(i);
	                	Integer j = posizioniContattiSenzaID[i];
	                	if(cur.isSuccess){
	                		cur.operationStatus = 'Contact ' +  'created. ' ;
							cur.contact.Id = contattiSenzaIDResultElem.getId();
							
							errorMsgCt[j]= '';
							System.debug('*********** VERIFICO INSERIMENTO CONTATTO SENZA ID*************' + cur.operationStatus );
                		} else {
                				errorCt = contattiSenzaIDResultElem.getErrors()[0];
                				FailureReason frct = new FailureReason();
                				System.debug('*********** ERRORE INSERIMENTO CONTATTO*************' + errorCt );
                				if (errorCt instanceof Database.DuplicateError) {
                                 		frct.isDuplicate = true;
	           			     		 	frct.isException = false;
											errorMsgCt[j]= '';
											System.debug('*********** CONTATTO DUPLICATO*************' + frct.isDuplicate );
										Database.DuplicateError duplicateErrorCt = (Database.DuplicateError) errorCt;
                            			Datacloud.DuplicateResult duplicateResultCt = duplicateErrorCt.getDuplicateResult();
                            			List<Datacloud.MatchResult> matchResultsCt = duplicateResultCt.getMatchResults();
                            			if(!matchResultsCt.isEmpty()){
                               	 				Datacloud.MatchResult matchResultCt = matchResultsCt.get(0);
                              					List<Datacloud.MatchRecord> matchRecordsCt = matchResultCt.getMatchRecords();
                                				if(!matchRecordsCt.isEmpty()){
                                    				Datacloud.MatchRecord matchRecordCt = matchRecordsCt.get(0);
                                    				sObject contactDuplicato = matchRecordCt.getRecord();
                                    				frct.duplicateContactId = (Id) contactDuplicato.get('Id');
                                    				cur.contact.Id = (Id) contactDuplicato.get('Id');
                                    				coppiaidwebid.put (cur.contact.Id, cur.contact.id_Web__c);
                                    				 if(!contactsDuplicatiDaAggiornareIds.contains(frct.duplicateContactId)){
	                                   						 contactsDuplicatiDaAggiornare.add(
	                                    						new Contact(
	                                    							Id = frct.duplicateContactId, 
	                                    							id_Web__c = cur.contact.id_Web__c

	                                    							)

	                                    );
	                                    contactsDuplicatiDaAggiornarePosizioni.add(posizioniContattiSenzaID[i]);
	                                   
                                    }

                                    }
                                }
                                	
                				} else{
                					frct.isDuplicate = false;
	                				frct.isException = true;
                            		frct.exceptionDetailMessage = errorCt.getMessage();
                            		System.debug('*********** ERRORACCIO*************' + errorCt.getMessage());
                            		errorMsgCt[j]=(errorCt.getMessage());	
                				}
                		}
	           		
	           		 contactUpsertResultList.add(cur);
	           		 System.debug('***************CONTATTI DA INSERIRE ********************' + contactUpsertResultList);
	           		 System.debug('***************CONTATTI DUPLICATI DA AGGIORNARE INSERIRE ********************' + contactsDuplicatiDaAggiornare);
	           		 System.debug('***************CONTATTI DUPLICATI DA AGGIORNARE POSIZIONI ********************' + contactsDuplicatiDaAggiornarePosizioni);
	           	}



	           	//AGGIORNAMENTO DEI CONTATTI DUPLICATI
	           	List<Database.SaveResult> contactsDuplicatiDaAggiornareResult = Database.update(contactsDuplicatiDaAggiornare, dml);


	           	//COSTRUISCO LA RESPONSEWS_PortalAccountsContacts
	    		 for (AccountUpsertResult aur : accountUpsertResultList){
	       				WS_PortalAccountsContacts.OutputElement outElem = new WS_PortalAccountsContacts.OutputElement();
	            		outElem.account = aur.originalAccount; 
	       				response.elements.add(outElem); 
	       					
				}

				if(!contattiConID.isEmpty()){ 
					for (integer i=0; i<contattiConID.size(); i++) {
						Integer j = posizioniContattiConID[i];
						response.elements[j].contact = contattiConID[i];
						response.elements[j].contactid = contattiConID[i].id;
						response.elements[j].idWeb = contattiConID[i].id_Web__c;
					}
				}

				if (!contattiSenzaID.isEmpty()){
					for (integer i=0; i<contattiSenzaID.size(); i++){
						integer j = posizioniContattiSenzaID[i];
						response.elements[j].contact = contattiSenzaID[i];
						response.elements[j].contactid = contattiSenzaID[i].id;
						response.elements[j].idWeb = contattiSenzaID[i].id_Web__c;

					}
				}

				
				if (!contactsDuplicatiDaAggiornarePosizioni.isEmpty()){
					for (integer i=0; i<contattiSenzaID.size(); i++) {
						Integer j = contactsDuplicatiDaAggiornarePosizioni[i];
						response.elements[j].contact = contactUpsertResultList[i].contact;
						response.elements[j].contactid = contactUpsertResultList[i].contact.id;
						response.elements[j].idWeb = contactUpsertResultList[i].contact.id_Web__c;
					}
				}
				
			
				
				for (integer i=0; i<accountUpsertResultList.size(); i++){
					String messaggio = 'errore riguardante i contatti :' + ' ' + errorMsgCt[i] + ';' + ' ' + 'errore riguardante gli account' + errorMsgAcct[i];
					response.elements[i].respMsg = messaggio;
					System.debug('***************************MESSAGGIO DI ERRORE NELLA RESPONSE******************************' + messaggio);
				}


				for (integer i=0; i<accountUpsertResultList.size(); i++){
					if(errorMsgAcct[i] !='' || errorMsgCt[i] !=''){
					response.elements[i].errore = true;
					} else {
						response.elements[i].errore = false;
					}
				}
 
			}

			}catch (Exception e ){
        	if(!Test.isRunningTest()) Database.rollback(sp);
        	response.operationStatus = new WS_PortalAccountsContacts.OperationStatus();
        	response.operationStatus.isError = true;
        	response.operationStatus.errorMessage = e.getMessage() + '. ' + e.getStackTraceString();
        	System.debug('**************************ECCEZIONE********************' + e.getMessage() + '. ' + e.getStackTraceString()); 
        }


		return response;

	}


}