@isTest
public class TestDataFactory {
    
    public static TestDataFactory.Result data = new TestDataFactory.Result();

    public class Result{
        public List<Account> accounts {get;set;}        
        public List<Contact> contacts {get;set;}
        public List<Avviso_di_Rinnovo__c> avvisiDiRinnovo {get;set;}
        public List<Campaign> campagne {get;set;}
        public List<Zuora__Subscription__c> subscriptions {get;set;}

        // catalogo
        public List<Prodotto_Vendita_Diretta__c> prodottiVenditaDiretta {get;set;}
        public List<Product2> prodotti;
        public List<zqu__ProductRatePlan__c> productRatePlans;
        public List<zqu__ProductRatePlanCharge__c> productRatePlanCharges;

        // configs
        public List<Gracing__c> gracings {get;set;}
        public List<Configurazione_Tipo_Avviso_Rinnovo__c> configurazioniTipoAvvisoRinnovo {get;set;}
        public List<Mapping_Documento_Emesso__c> mappingDocumentoEmesso {get;set;}
        public List<zqu__BillingEntity__c> billingEntities;
        public List<zqu__ZUnitOfMeasure__c> UOMS;
        public List<Regola_Avviso_Rinnovo__c> regoleAvvisoRinnovo {get;set;}

        // calendari
        public List<Testata__c> testate {get;set;}
        public List<Calendario__c> calendari {get;set;}

    }

    public static void load(){
        //load_catalogo();
        //load_configs();
        //load_testate_calendari();
        //load_campagne();
        //load_clienti();
    }

    public static void load_clienti(){
        data.accounts = (List<Account>) Test.loadData(Account.sObjectType, 'TestData_Account');
        data.contacts = (List<Contact>) Test.loadData(Contact.sObjectType, 'TestData_Contact');
    }

    public static void load_catalogo(){
        data.prodottiVenditaDiretta = (List<Prodotto_Vendita_Diretta__c>) Test.loadData(Prodotto_Vendita_Diretta__c.sObjectType, 'TestData_ProdottoVenditaDiretta');
        //data.billingEntities = (List<zqu__BillingEntity__c>) Test.loadData(zqu__BillingEntity__c.sObjectType, 'TestData_BillingEntity');
        //data.prodotti = (List<Product2>) Test.loadData(Product2.sObjectType, 'TestData_Product2');
        //data.productRatePlans = (List<zqu__ProductRatePlan__c>) Test.loadData(zqu__ProductRatePlan__c.sObjectType, 'TestData_ProductRatePlan');
        //data.productRatePlanCharges = (List<zqu__ProductRatePlanCharge__c>) Test.loadData(zqu__ProductRatePlanCharge__c.sObjectType, 'TestData_ProductRatePlanCharge');
    }

    public static void load_configs(){
        /*data.gracings = (List<Gracing__c>) Test.loadData(Gracing__c.sObjectType, 'TestData_Gracing');
        data.configurazioniTipoAvvisoRinnovo = (List<Configurazione_Tipo_Avviso_Rinnovo__c>) Test.loadData(Configurazione_Tipo_Avviso_Rinnovo__c.sObjectType, 'TestData_ConfigurazioneTipoAvvisoRinnovo');
        data.mappingDocumentoEmesso = (List<Mapping_Documento_Emesso__c>) Test.loadData(Mapping_Documento_Emesso__c.sObjectType, 'TestData_MappingDocumentoEmesso');
        data.UOMS = (List<zqu__ZUnitOfMeasure__c>) Test.loadData(zqu__ZUnitOfMeasure__c.sObjectType, 'TestData_ZUnitOfMeasure');
        data.regoleAvvisoRinnovo = (List<Regola_Avviso_Rinnovo__c>) Test.loadData(Regola_Avviso_Rinnovo__c.sObjectType, 'TestData_RegolaAvvisoRinnovo');
        */
        List<Setting__c> settings = new List<Setting__c>{
            new Setting__c(Name = 'CalcoloSpeseSpedizione', URL__c = 'http://domuscap.edidomus.it/api/v1/spedizioni'),
            new Setting__c(Name = 'CalcoloSpeseSpedizioneBulk', URL__c = 'http://domuscap.edidomus.it/api/v1/spedizionibulk'),
            new Setting__c(Name = 'DomusCap', URL__c = 'http://domuscap.edidomus.it/api/v1/'),
            new Setting__c(Name = 'TipiSpeseDiSpedizione', URL__c = 'http://domuscap.edidomus.it/api/v1/tipospedizioni/'),
            new Setting__c(Name = 'ZuoraEndpoint', URL__c = 'https://apisandbox.zuora.com/apps/services/a/82.0')
        };
        insert settings;
    }

    public static void load_testate_calendari(){
        data.testate = (List<Testata__c>) Test.loadData(Testata__c.sObjectType, 'TestData_Testata');
        data.calendari = (List<Calendario__c>) Test.loadData(Calendario__c.sObjectType, 'TestData_Calendario');
    }

    public static void load_campagne(){
        //data.campagne = (List<Campaign>) Test.loadData(Campaign.sObjectType, 'TestData_Campaign');
    }
}