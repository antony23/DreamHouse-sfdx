public class ProcessWizardController{

    public String fieldName;
    public String ObjName {get; set;}
    public List<String> lista {get; set;}
    public Map<String,String> mappa {get; set;}
    public Id idObj;
    public Boolean showMoreValue;
    public Boolean showMarkButton;
    public String statusValue;
    public String getfieldName(){
         return fieldName; 
    } 
    public void setfieldName(String s){
        if (s!=null){
          fieldName = s;
        }
    }
    
    public String getstatusValue(){
         return statusValue; 
    } 
    public void setstatusValue(String s){
        if (s!=null){
          statusValue= s;
        }
    }
  
    public id getidObj(){
         return idObj; 
    } 
    public void setidObj(id s){
        if (s!=null){
          idObj = s;
          GetContructorInformation();
        }
    }
    
    public Boolean getshowMoreValue(){
         return showMoreValue; 
    } 
    public void setshowMoreValue(Boolean s){
        if (s!=null){
          showMoreValue = s;
          //GetContructorInformation();
        }
    }
    
    public Boolean getshowMarkButton(){
         return showMarkButton; 
    } 
    public void setshowMarkButton(Boolean s){
        if (s!=null){
          showMarkButton = s;
          //GetContructorInformation();
        }
    }
    
    public processWizardController()
    {
        /* Since the constructor is called before the setter
        record_Id will always be null when the constructor is called.*/                   
        system.debug('idObj '+ idObj); 
        system.debug('fieldName '+ fieldName); 
      
    }
    
    public void GetContructorInformation(){
        SObjectType childObjectType = idObj.getSObjectType();
        ObjName = String.valueOf(childObjectType);
        System.debug('TYPE OBJ' + childObjectType);
        lista = getValues(ObjName, fieldName);
        mappa = getMappaValues(ObjName, fieldName);
        system.debug(lista);
    }
    
    public List<String> getValues(String obj, String field)
    {
       List<String> listRet = new List<String>();
       system.debug('OBJ' + obj);
       system.debug('FIELD' + field);
       Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap().get(field).getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {
          //options.add(new SelectOption(f.getLabel(), f.getValue()));
          Set<String> mySet = new Set<String>();
          mySet.addAll(listRet);
          if(!f.getLabel().contains('Closed') && f.getLabel() != 'Qualified Business' && f.getLabel() != 'Qualified Consumer' )
              listRet.add(f.getLabel());
          else if(f.getLabel().contains('Closed') && !mySet.contains('Closed'))
              listRet.add('Closed');
              
       }       
       return listRet;
    }
    
    public Map<String,String> getMappaValues(String obj, String field)
    {
       Map<String,String> listRet = new Map<String,String>();
       system.debug('OBJ' + obj);
       system.debug('FIELD' + field);
       Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap().get(field).getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {

              listRet.put(f.getLabel(),f.getValue());
              
       }       
       return listRet;
    }
    
  
}