public class MergeRequestTriggerHandler extends AbstractTriggerHandler {
	private static MergeRequestTriggerHandler singleton = new MergeRequestTriggerHandler();

	private MergeRequestTriggerHandler() {
		super('Merge_Request__c');
	}

	public static void handle() {
		singleton.handle();
	}

	public static void enable() {
		singleton.enable();
	}

	public static void disable() {
		singleton.disable();
	}

	override
	public void onBeforeInsert() {
		final List<Merge_Request__c> records = Trigger.new;
		final Map<Id, String> recordTypes = loadRecordTypes();
		final Map<Id, Account> accounts = loadAccounts(records, recordTypes, new String[]{'Id', 'Codice_Cliente__c', 'Codice_SAP__c'});
		final Map<Id, Contact> contacts = loadContacts(records, recordTypes, new String[]{'Id', 'CodiceCliente__c', 'Codice_SAP__c'});

		System.debug('Involved Accounts: ' + accounts.size());
		System.debug('Involved Contacts: ' + contacts.size());

		for(Merge_Request__c mr : records) {
			System.debug('Merge_Request.old = ' + JSON.serialize(mr));

			final String rtName = recordTypes.get(mr.RecordTypeId);
			String masterId, masterSFDCode, masterSAPCode;
			String slaveId, slaveSFDCode, slaveSAPCode;
			Boolean masterInfo = False, slaveInfo = False;

			switch on rtName {
	            when 'Account' { 
	            	final Account ma = accounts.get(mr.Master_Account__c);
	            	final Account sa = accounts.get(mr.Slave_Account__c);

	            	if(ma != null) {
	            		masterId = ma.Id;
		            	masterSFDCode = ma.Codice_Cliente__c;
		            	masterSAPCode = ma.Codice_SAP__c;
		            	masterInfo = True;
	            	}

	            	if(sa != null) {
	            		slaveId = sa.Id;
		            	slaveSFDCode = sa.Codice_Cliente__c;
		            	slaveSAPCode = sa.Codice_SAP__c;
		            	slaveInfo = True;
	            	}
	            }
	            when 'Contact' {
	            	final Contact mc = contacts.get(mr.Master_Contact__c);
	            	final Contact sc = contacts.get(mr.Slave_Contact__c);

	            	if(mc != null) {
	            		masterId = mc.Id;
		            	masterSFDCode = mc.CodiceCliente__c;
		            	masterSAPCode = mc.Codice_SAP__c;
		            	masterInfo = True;
	            	}
	            	
	            	if(sc != null) {
	            		slaveId = sc.Id;
		            	slaveSFDCode = sc.CodiceCliente__c;
		            	slaveSAPCode = sc.Codice_SAP__c;
		            	slaveInfo = True;
	            	}
	            }
	        }

	        if(masterInfo) {
	        	mr.Master_Id__c = masterId;
		        mr.Master_Salesforce_Code__c = masterSFDCode;
		        mr.Master_SAP_Code__c = masterSAPCode;
	        }

	        if(slaveInfo) {
	        	mr.Slave_Id__c = slaveId;
		        mr.Slave_Salesforce_Code__c = slaveSFDCode;
		        mr.Slave_SAP_Code__c = slaveSAPCode;
	        }

	        System.debug('Merge_Request.new = ' + JSON.serialize(mr));
		}
	}

	override
	public void onAfterInsert(){
		if(beforeAfterLoss() == 0) {
			MergeRequestConsumerBatch.consume(Trigger.newMap.keySet());
		}
	}

	private static Map<Id, Account> loadAccounts(List<Merge_Request__c> requests, Map<Id, String> recordTypes, String[] fields) {
		final Set<Id> ids = new Set<Id>();

		for(Merge_Request__c mr : requests) {
			if(recordTypes.get(mr.RecordTypeId) != 'Account') continue;

			ids.add(mr.Master_Account__c);
			ids.add(mr.Slave_Account__c);
		}

		final String query = 'SELECT ' + String.join(fields, ',') + ' FROM Account WHERE Id IN :ids';

		return new Map<Id, Account>((List<Account>) Database.query(query));
	}

	private static Map<Id, Contact> loadContacts(List<Merge_Request__c> requests, Map<Id, String> recordTypes, String[] fields) {
		final Set<Id> ids = new Set<Id>();

		for(Merge_Request__c mr : requests) {
			if(recordTypes.get(mr.RecordTypeId) != 'Contact') continue;

			ids.add(mr.Master_Contact__c);
			ids.add(mr.Slave_Contact__c);
		}

		final String query = 'SELECT ' + String.join(fields, ',') + ' FROM Contact WHERE Id IN :ids';

		return new Map<Id, Contact>((List<Contact>) Database.query(query));
	}

	private static Map<Id, String> loadRecordTypes() {
		final Map<Id, String> rtMap = new Map<Id, String>();
		final RecordType[] rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Merge_Request__c'];

		for(RecordType rt : rtList) {
			rtMap.put(rt.Id, rt.DeveloperName);
		}

		return rtMap;
	}
}