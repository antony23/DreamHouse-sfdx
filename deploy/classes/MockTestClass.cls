@isTest
public class MockTestClass implements HttpCalloutMock {

	protected String serviceName;

	public MockTestClass(String service) {
		this.serviceName = service;
	}

	public HttpResponse respond(HttpRequest req){

		HttpResponse res = new HttpResponse();
		res.setStatusCode(200);
		res.setStatus('OK');
		res.setHeader('Content-Type', 'application/json');
		
		if(serviceName == null || String.isBlank(serviceName)){

			// WHAT ? NOTHING ? ARE YOU KIDDING ME?

		}else if(serviceName.equalsIgnoreCase('TipiSpeseDiSpedizione')){

			res.setBody('{"data":{"SpedizioneDirette":[{"TipoSpedizione":"Corriere Estero","Zona":"ZONA 2","AreaGeografica":"Africa - America - Asia","TipiConsegna":[{"CodiceSAP":"CORX","TipoConsegna":"Corriere Estero Camionistico","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere Espresso Estero","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Posta Prioritaria","Zona":"ZONA 2","AreaGeografica":"Africa - America - Asia","TipiConsegna":[{"CodiceSAP":"PRIO","TipoConsegna":"Posta Prioritaria","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Estero","Zona":"ZONA 1;ZONA 2;ZONA 3","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Raccomandata Estero","Zona":"ZONA 1","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"RACC","TipoConsegna":"Raccomandata","MessaggioConsegnaITA":"Raccomandata","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Estero","Zona":"ZONA 1;ZONA 2;ZONA 3","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]},{"TipoSpedizione":"Spedizioniere Estero","Zona":"ZONA 1;ZONA 2;ZONA 3","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"SPED","TipoConsegna":"Spedizioniere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":""}]},{"TipoSpedizione":"Corriere Estero","Zona":"ZONA 1","AreaGeografica":"Europa e Bacino Mediterraneo","TipiConsegna":[{"CodiceSAP":"CORX","TipoConsegna":"Corriere Estero Camionistico","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere Espresso Estero","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Posta Prioritaria","Zona":"ZONA 1","AreaGeografica":"Europa e Bacino Mediterraneo","TipiConsegna":[{"CodiceSAP":"PRIO","TipoConsegna":"Posta Prioritaria","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Contrassegno","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"COCO","TipoConsegna":"Contrassegno","MessaggioConsegnaITA":"Contrassegno","MessaggioConsegnaENG":"Cash on delivery"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"CONO","TipoConsegna":"Corriere Normale SDA","MessaggioConsegnaITA":"Corriere SDA","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Mezzo Cliente Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MCLI","TipoConsegna":"Mezzo Cliente","MessaggioConsegnaITA":"Come concordato con il cliente","MessaggioConsegnaENG":"Delivery arranged with the customer"}]},{"TipoSpedizione":"Mezzo Domus","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"MDOM","TipoConsegna":"Mezzo Domus","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Scarico Magazzino Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"FORN","TipoConsegna":"Scarico Magazzino","MessaggioConsegnaITA":"","MessaggioConsegnaENG":""}]},{"TipoSpedizione":"Corriere Estero","Zona":"ZONA 3","AreaGeografica":"Oceania","TipiConsegna":[{"CodiceSAP":"CORX","TipoConsegna":"Corriere Estero Camionistico","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"CORR","TipoConsegna":"Corriere Espresso Estero","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Posta Prioritaria","Zona":"ZONA 3","AreaGeografica":"Oceania","TipiConsegna":[{"CodiceSAP":"PRIO","TipoConsegna":"Posta Prioritaria","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]}],"SpedizioneAbbonamenti":[{"TipoSpedizione":"Prioritaria Estero","Zona":"ZONA 2","AreaGeografica":"Africa - America - Asia","TipiConsegna":[{"CodiceSAP":"ZN","TipoConsegna":"Fascettario Estero (FASAIR)","MessaggioConsegnaITA":"Posta prioritaria","MessaggioConsegnaENG":"Priority mail"}]},{"TipoSpedizione":"Corriere Estero","Zona":"ZONA 1;ZONA 2;ZONA 3","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Priority mail"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Priority mail"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Priority mail"}]},{"TipoSpedizione":"Standard Estero","Zona":"ZONA 1;ZONA 2;ZONA 3","AreaGeografica":"Estero","TipiConsegna":[{"CodiceSAP":"ZM","TipoConsegna":"Fascettario Estero (FASEST)","MessaggioConsegnaITA":"Posta ordinaria","MessaggioConsegnaENG":"Ordinary Mail"}]},{"TipoSpedizione":"Prioritaria Estero","Zona":"ZONA 1","AreaGeografica":"Europa e Bacino Mediterraneo","TipiConsegna":[{"CodiceSAP":"ZN","TipoConsegna":"Fascettario Estero (FASAIR)","MessaggioConsegnaITA":"Posta prioritaria","MessaggioConsegnaENG":"Priority mail"}]},{"TipoSpedizione":"Consultazione in Digitale","Zona":"Indifferente","AreaGeografica":"Indifferente","TipiConsegna":[{"CodiceSAP":"","TipoConsegna":"Consultazione in Digitale","MessaggioConsegnaITA":"Consultazione in Digitale","MessaggioConsegnaENG":"Digital Access"}]},{"TipoSpedizione":"Corriere Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZI","TipoConsegna":"Fascettario Etichette Corriere da Magazzino Domus ZI","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZH","TipoConsegna":"Fascettario Etichette Generiche per Corriere ZH","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"},{"CodiceSAP":"ZG","TipoConsegna":"Fascettario Etichette Personalizzate per Corriere ZG","MessaggioConsegnaITA":"Corriere","MessaggioConsegnaENG":"Courier"}]},{"TipoSpedizione":"Standard Italia","Zona":"ZONA 1;IT","AreaGeografica":"Italia","TipiConsegna":[{"CodiceSAP":"ZL","TipoConsegna":"Fascettario Standard Italia","MessaggioConsegnaITA":"Poste italiane","MessaggioConsegnaENG":"Ordinary Mail"}]},{"TipoSpedizione":"Prioritaria Estero","Zona":"ZONA 3","AreaGeografica":"Oceania","TipiConsegna":[{"CodiceSAP":"ZN","TipoConsegna":"Fascettario Estero (FASAIR)","MessaggioConsegnaITA":"Posta prioritaria","MessaggioConsegnaENG":"Priority mail"}]}]}}');

		}else if(serviceName.equalsIgnoreCase('CalcoloSpeseDiSpedizione')){

			res.setBody('{"data": {"VenditeDirette": 0,"Abbonamenti": [3.9],"TotaleAbbonamenti_Ordinaria": 0,"TotaleAbbonamenti_Prioritaria": 0}}');

		}else if(serviceName.equalsIgnoreCase('CheckIban')){

			res.setBody('{"bank_data":{"bic":"SELBIT2B","branch":"FIRENZE 2 - VIALE DEI MILLE","bank":"BANCA SELLA SPA","address":"VIALE DEI MILLE 7-9","city":"FIRENZE","state":"FI","zip":"50131","phone":null,"fax":null,"www":null,"email":null,"country":"Italy","country_iso":"IT","account":"052879623060"},"errors":[],"validations":[{"code":"002","message":"Account Number check digit is correct"},{"code":"001","message":"IBAN Check digit is correct"},{"code":"005","message":"IBAN structure is correct"},{"code":"003","message":"IBAN Length is correct"}],"sepa_data":{"SCT":"YES","SDD":"YES","COR1":"YES","B2B":"YES","SCC":"NO"}}');

		}else if(serviceName.equalsIgnoreCase('ErrorIban')){

			res.setBody('{"bank_data":{"bic":"SELBIT2B","branch":"FIRENZE 2 - VIALE DEI MILLE","bank":"BANCA SELLA SPA","address":"VIALE DEI MILLE 7-9","city":"FIRENZE","state":"FI","zip":"50131","phone":null,"fax":null,"www":null,"email":null,"country":"Italy","country_iso":"IT","account":"05287962"},"errors":[],"validations":[{"code":"201","message":"Account Number check digit not correct"},{"code":"202","message":"IBAN Check digit not correct"},{"code":"205","message":"IBAN structure is not correct"},{"code":"203","message":"IBAN Length is not correct. Italy IBAN must be 27 characters long."}],"sepa_data":{"SCT":"YES","SDD":"YES","COR1":"YES","B2B":"YES","SCC":"NO"}}');

		}else if(serviceName.equalsIgnoreCase('getPrivacyVersion')){

			res.setBody('1');

		}else if(serviceName.equalsIgnoreCase('getPrivacyText')){

			res.setBody('Testo Informativa Corrente');

		}else if(serviceName.equalsIgnoreCase('CreateAccount')){

			res.setBody('{"success": true, "accountId": "402892c74c9193cd014c96bbe7c101f9", "accountNumber": "A00000004", "paymentMethodId": "402892c74c9193cd014c96bbe7d901fd"}');

		}else if(serviceName.equalsIgnoreCase('Subscribe')){

			res.setBody('[{"Success":true,"TotalMrr":3.804761905,"ChargeMetricsData":{"ChargeMetrics":[{"ProductRatePlanChargeId":"2c92c0f85e09721b015e0a7a63e76fa4","MRR":3.804761905,"DMRR":3.804761905,"TCV":7.99,"ProductRatePlanId":"2c92c0f85e097219015e0a7465ec39e4","DTCV":7.99},{"ProductRatePlanChargeId":"2c92c0f85e09721b015e0a7a63a56fa0","MRR":0,"DMRR":0,"TCV":0,"ProductRatePlanId":"2c92c0f85e097219015e0a7465ec39e4","DTCV":0}]},"InvoiceData":[{"InvoiceItem":[{"ProductId":"2c92c0f95e097cf3015e0a7455481e5d","ChargeAmount":0,"ServiceStartDate":"2019-02-01","ChargeDate":"2019-01-04T15:34:00.589+01:00","AccountingCode":"Debito verso Abbonati Digitale","TaxAmount":0,"ChargeName":"Contributo Spese di Spedizione","TaxExemptAmount":0,"UnitPrice":0,"UOM":"Copie","ServiceEndDate":"2019-04-04","Quantity":1,"ChargeDescription":"Contributo Spese di Spedizione","ProductRatePlanChargeId":"2c92c0f85e09721b015e0a7a63a56fa0"},{"ProductId":"2c92c0f95e097cf3015e0a7455481e5d","ChargeAmount":7.68,"ServiceStartDate":"2019-02-01","ChargeDate":"2019-01-04T15:34:00.590+01:00","AccountingCode":"Debito verso Abbonati Digitale","TaxAmount":0.31,"ChargeName":"Quattroruote - 3 Copie - Nuovo","TaxExemptAmount":0,"UnitPrice":7.99,"UOM":"Copie","ServiceEndDate":"2019-04-04","Quantity":1,"ChargeDescription":"Abbonamenti digitali Quattroruote  (3 Copie)","ProductRatePlanChargeId":"2c92c0f85e09721b015e0a7a63e76fa4"}],"Invoice":{"AccountId":"2c92c0f9680e6fd701681948fc7741ec","Amount":7.99,"TaxAmount":0.31,"AmountWithoutTax":7.68}}],"TotalTcv":7.99}]');

		}

		return res;

	}

}