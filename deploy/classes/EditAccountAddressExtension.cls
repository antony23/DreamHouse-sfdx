public class EditAccountAddressExtension {
    
    public Account a {get;set;}
    public EditAccountAddressExtension (ApexPages.StandardController controller) {
        controller.addFields(new String[]{
                                'BillingStreet','BillingCity','BillingPostalCode','BillingState','BillingCountry',
                                'PersonMailingStreet','PersonMailingCity','PersonMailingPostalCode','PersonMailingState','PersonMailingCountry',
                                'ShippingStreet','ShippingCity','ShippingPostalCode','ShippingState','ShippingCountry', 'Note_per_il_corriere__c',
                                'Via_Temporaneo__c', 'Citt_Temporaneo__c', 'StatoProvincia_Temporaneo__c', 'CAP_temporaneo__C' , 'Paese_temporaneo__c'

                                                           });
    }
    
}