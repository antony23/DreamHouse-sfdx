public class AdminEditQuoteExtension {

	public zqu.SelectProductComponentOptions options { get; set; }    
    public zqu__Quote__c q {get;set;}
    public zqu.Quote quoteZuora {get; set;}

    public Boolean hasAbbonamenti {get;set;}
    public List<SelectOption> calendarOptions{get;set;}
    public String endCalendarId {get;set;}
    public List<WrapCalendario> wrapCalList{get;set;}

    public String selectedSection {get;set;}
    public List<SelectOption> modifySectionList {get;set;}
    public Zuora__CustomerAccount__c custAcc {get;set;}

    public Integer numeroCopieIncluseNelCreditoResiduo {get;set;}
    public Decimal importoSingolaCopia {get;set;}

    private Date endCalendarDate;

    public AdminEditQuoteExtension(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()){
            controller.addFields(new List < String > {'Tipo_causale_sconto__c','Mantieni_Offerta__c', 'Testate__c', 'zqu__InvoiceOwnerId__c', 'zqu__ZuoraAccountID__c', 
                                'zqu__StartDate__c', 'zqu__ValidUntil__c', 'Calendario_Fine_Text__c', 'Utilizza_Credito_Residuo__c', 'zqu__Previewed_Total__c', 'FreezeDettaglioCopie__c', 'Italia_Estero_Zuora__c'});
        } 
        hasAbbonamenti = false;
        q = (zqu__Quote__c) controller.getRecord();
        quoteZuora = zqu.Quote.getInstance(q.Id);
        if(q.Id == null){
            return;
        }
        options = new zqu.SelectProductComponentOptions();
        options.mode = zqu.SelectProductComponentOptions.MODE_EDIT;
        options.quoteId = q.Id;
        options.saveUrl = '/' + q.Id;

        modifySectionList = new List<SelectOption>();
        selectedSection = 'terms';
        modifySectionList.add(new SelectOption('terms', 'Modifica i termini'));
        modifySectionList.add(new SelectOption('price', 'Modifica i prezzi'));

        List<zqu__QuoteRatePlanCharge__c> chargeAbbList = DomusUtil.getChargeAbbonamenti(q.Id);
        if(chargeAbbList != null && chargeAbbList.size() > 0){
            hasAbbonamenti = true;
        }


        String accountId;
        if(q.zqu__InvoiceOwnerId__c != null){
            accountId = q.zqu__InvoiceOwnerId__c;
        } else  if(q.zqu__ZuoraAccountID__c != null){
            accountId = q.zqu__ZuoraAccountID__c;
        } 


        if(accountId != null){
            List<Zuora__CustomerAccount__c> custAccList = [SELECT Id, Name, Metodo_di_pagamento__c,
                                                                Zuora__Credit_Balance__c
                                                                FROM Zuora__CustomerAccount__c 
                                                                WHERE Zuora__Zuora_Id__c = :accountId];
            System.debug('*************************custAccList******************' + custAccList);
            if(custAccList != null && custAccList.size() > 0){
                custAcc = custAccList.get(0);

            }
        }

        if(hasAbbonamenti){
            inizializzaParametriAbbonamento();
        }
    }

    public PageReference saveAndRedirect(){
        try{
            PageReference p = Page.ViewQuote;
            p.getParameters().put('Id',q.Id);
            saveOptions();
            return p;
        }
        catch(Exception e){
            appendErrorMessage(e);
            return null;
        }
    }

    public PageReference reload(){
        try{
            PageReference p = Page.AdminEditQuote;
            p.getParameters().put('Id',q.Id);
            system.debug(q.id);
            return p;
        }catch(Exception e){
            appendErrorMessage(e);
            return null;
        }
    }

    public void saveOptions(){
        try{

            update q;
            
            zqu.Quote quote = zqu.Quote.getInstance(q.Id);
            zqu__quote__c quoteObj = quote.getSObject();

            quoteObj.Calendario_Fine_Text__c = endCalendarId;
            System.debug(loggingLevel.Error, '*** endCalendarId: ' + endCalendarId);
            System.debug(loggingLevel.Error, '*** endCalendarDate: ' + endCalendarDate);
            //quoteObj.Dettaglio_Copie_per_Testata__c += ';'+startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;

            DomusUtil.setQuoteChargeDateParameters(q.zqu__StartDate__c, endCalendarDate, quoteObj.Id);
            DomusUtil.setQuoteDateParameters(q.zqu__StartDate__c, endCalendarDate, quoteObj);

            System.debug(loggingLevel.Error, '*** q.zqu__ValidUntil__c: ' + q.zqu__ValidUntil__c);

            Set<String> testataSet = new Set<String>(q.Testate__c.split(';'));
            List<Testata__c> testataList = [SELECT Id, Name FROM Testata__c WHERE IssueCalendar__c = :testataSet];
            List<Id> testataIdList = new List<Id>();
            for(Testata__c t : testataList){
                testataIdList.add(t.Id);
            }


            List<Calendario__c> startCalendarioList = [SELECT Id, Name, Data_Uscita__c, Testata__c FROM Calendario__c
                                                        WHERE Testata__c = :testataIdList
                                                        AND Data_Uscita__c >= :q.zqu__StartDate__c
                                                        AND Data_Uscita__C <= :endCalendarDate
                                                        ORDER BY Data_Uscita__c];
            
            Map<Id, Calendario__c> startDateMap = new Map<Id, Calendario__c>();
            Map<Id, Calendario__c> endDateMap = new Map<Id, Calendario__c>();

            for(Calendario__c cal : startCalendarioList){
                String testataName = cal.Testata__c;
                if(startDateMap.get(testataName) == null || startDateMap.get(testataName).Data_Uscita__c >= cal.Data_Uscita__c){
                    startDateMap.put(testataName, cal);
                }
                if(endDateMap.get(testataName) == null || endDateMap.get(testataName).Data_Uscita__c <= cal.Data_Uscita__c){
                    endDateMap.put(testataName, cal);
                }
            }

            String dettaglioCopiePerTestata;
            for(Id testataId : testataIdList){
                    if(String.isBlank(dettaglioCopiePerTestata)){
                        dettaglioCopiePerTestata = startDateMap.get(testataId).Name + ' - ' + endDateMap.get(testataId).Name;
                    }else{
                        dettaglioCopiePerTestata += ';'+startDateMap.get(testataId).Name + ' - ' + endDateMap.get(testataId).Name;
                    }
            }
            quoteObj.Dettaglio_Copie_per_Testata__c = dettaglioCopiePerTestata;
            quoteObj.FreezeDettaglioCopie__c = true;

            quote.save();

        } catch(Exception e){
            appendErrorMessage(e);
        }
    }


    public void inizializzaParametriAbbonamento(){
        try{
            System.debug('******************** sono entrato nel metodo: inizializzaParametriAbbonamento ****************** ');
            // calcolo valore singola copia
            Decimal totaleOrdine = q.zqu__Previewed_Total__c;
            System.debug(loggingLevel.Error, '*** totaleOrdine: ' + totaleOrdine);

            Decimal costoVenditeDirette = 0;
            Integer numeroIssueAbbonamento = 0;

            List<string> chargeFields = new List<string>();
            for (Schema.SObjectField ft : Schema.getGlobalDescribe().get('zqu__QuoteChargeDetail__c').getDescribe().Fields.getMap().values()){
                chargeFields.add(ft.getDescribe().getName().toLowerCase());
            }
            String query = 'SELECT ' + String.join(chargeFields, ',') + ',zqu__ProductRatePlanCharge__r.TipoProdotto__c,zqu__ProductRatePlanCharge__r.Uscite__c ' +
                            'FROM zqu__QuoteChargeDetail__c ' + 
                            'WHERE zqu__Quote__c = \'' + q.Id + '\'';
            List<zqu__QuoteChargeDetail__c> charges = (List<zqu__QuoteChargeDetail__c>) Database.query(query);
            for(zqu__QuoteChargeDetail__c charge : charges){
                if(charge.zqu__ProductRatePlanCharge__r.TipoProdotto__c == 'Abbonamento'){
                    numeroIssueAbbonamento += Integer.valueOf(charge.zqu__ProductRatePlanCharge__r.Uscite__c);
                }else if(charge.zqu__ProductRatePlanCharge__r.TipoProdotto__c == 'Vendita Diretta'){
                    costoVenditeDirette += charge.zqu__TCV__c;
                }
            }

            importoSingolaCopia = (totaleOrdine - costoVenditeDirette) / numeroIssueAbbonamento;
            System.debug('****************** importoSingolaCopia ************' + importoSingolaCopia);
            System.debug('***************** custAcc.Zuora__Credit_Balance__c *******************' + custAcc.Zuora__Credit_Balance__c);
            Decimal numeroCopie = custAcc.Zuora__Credit_Balance__c / importoSingolaCopia;
            
            System.debug('****************numeroCopie*************' + numeroCopie);
            numeroCopieIncluseNelCreditoResiduo = numeroCopie.intValue();
            if(numeroCopie - numeroCopie.intValue() > 0.8){
                numeroCopieIncluseNelCreditoResiduo = numeroCopieIncluseNelCreditoResiduo + 1;
            }

            // caricamento copie disponibili a calendario
            Set<String> testataSet = new Set<String>(q.Testate__c.split(';'));

            List<Calendario__c> copieAbbonamento = [SELECT Id, Name, Data_Uscita__c, Descrizione__c, 
                                                    Numero_Uscita__c, Data_Uscita_Effettiva__c
                                                    FROM Calendario__c
                                                    WHERE Testata__r.Name = :testataSet 
                                                    AND Data_Uscita__c >= :q.zqu__StartDate__c
                                                    ORDER BY Data_Uscita__c ASC];
            calendarOptions = new List<SelectOption>();
            for(Integer numeroCopia=0; numeroCopia<copieAbbonamento.size(); numeroCopia++){
                Calendario__c copia = copieAbbonamento.get(numeroCopia);
                if(
                    (q.Utilizza_Credito_Residuo__c == 'NO' && copia.Data_Uscita__c <= q.zqu__ValidUntil__c) || 
                    (q.Utilizza_Credito_Residuo__c == 'SI' && numeroCopia < numeroCopieIncluseNelCreditoResiduo)
                ){
                    endCalendarId = copia.Id;
                    endCalendarDate = copia.Data_Uscita__c;
                }
                calendarOptions.add(new SelectOption(copia.id, copia.Name));
            }
            Decimal importoUnRounded = importoSingolaCopia;
            importoSingolaCopia = importoSingolaCopia.round(System.RoundingMode.HALF_EVEN);
            calcolaCopie();
            importoSingolaCopia = importoUnRounded.setScale(2);
            System.debug('********************** inizializzaParametriAbbonamento calendar options ******************' + calendarOptions);
        }catch(Exception e){
            appendErrorMessage(e);
            System.debug('**************************ECCEZIONE********************' + e.getMessage() + '. ' + e.getStackTraceString());
        }
    }

    public void calcolaCopie(){
        try{
            Set<String> testataSet = new Set<String>(q.Testate__c.split(';'));
            Date startDate = q.zqu__StartDate__c;
            wrapCalList = new List<WrapCalendario>();
            Calendario__c endCalendar = DomusUtil.getCalendario(endCalendarId);
            if(endCalendar == null){ 
                appendErrorMessage('Impossibile trovare un calendario di fine');
                return ;
            }
            System.debug(loggingLevel.Error, '*** endCalendar: ' + endCalendar);
            endCalendarDate = endCalendar.Data_Uscita__c;


            for(String testata : testataSet){
                Integer numCopie = [SELECT COUNT() 
                                    FROM Calendario__c
                                    WHERE Testata__r.Name = :testata AND
                                    Data_Uscita__c >= :startDate AND 
                                    Data_Uscita__c <= :endCalendarDate];
                WrapCalendario wc = new WrapCalendario(testata, numCopie);
                wrapCalList.add(wc);
            }
        }catch(Exception e){
            appendErrorMessage(e);
        }
    }




    public class WrapCalendario{
        public Integer numeroCopie {get;set;}
        public String testata {get;set;}
        public WrapCalendario(String t, Integer copie){
            testata = t;
            numeroCopie = copie;
        }
    }

    public void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    public void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }    

}