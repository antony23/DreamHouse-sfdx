public class SubscriptionMailProcessAction {
    private static final String[] TESTATE_PROFESSIONAL = new String[]{'QVA'};
    private static final String[] TESTATE_DIFFUSIONE = new String[]{'AUTOPRO', 'DOMUS', 'DUERUOTE', 'MERIDIANI', 'MERIDIANI MONTAGNE', 'QUATTRORUOTE', 'RUOTECLASSICHE', 'TOPGEAR', 'TUTTOTRASPORTI', 'XOFFROAD'};

    private static final String SENDER = 'Editoriale Domus - No reply';
    private static final String[] BCCs = new String[]{'sfdc_cc_notifichecrm@edidomus.it'};

    @TestVisible private static final String MT1 = 'MT1';
    @TestVisible private static final String MT2 = 'MT2';
    @TestVisible private static final String MT3 = 'MT3';
    @TestVisible private static final String MT4 = 'MT4';
    @TestVisible private static final String MT5 = 'MT5';
    @TestVisible private static final String MT6 = 'MT6';
    @TestVisible private static final String MT8 = 'MT8';
    @TestVisible private static final String MT9 = 'MT9';

    @TestVisible private static final String NOTIFICHE_EMAIL_ABILITATE = 'areEnabled("notifiche email")';
    @TestVisible private static final String MT1_NON_INVIATA = 'isNotSent("MT1")';
    @TestVisible private static final String MT2_NON_INVIATA = 'isNotSent("MT2")';
    @TestVisible private static final String MT3_NON_INVIATA = 'isNotSent("MT3")';
    @TestVisible private static final String MT4_NON_INVIATA = 'isNotSent("MT4")';
    @TestVisible private static final String MT5_NON_INVIATA = 'isNotSent("MT5")';
    @TestVisible private static final String MT6_NON_INVIATA = 'isNotSent("MT6")';
    @TestVisible private static final String MT8_NON_INVIATA = 'isNotSent("MT8")';
    @TestVisible private static final String MT9_NON_INVIATA = 'isNotSent("MT9")';
    @TestVisible private static final String SOSPESA = 'isSuspended';
    @TestVisible private static final String OMAGGIO = 'isFree';
    @TestVisible private static final String FATTURA_A_TERZI = 'invoiceToThirdParty';
    @TestVisible private static final String CLIENTE_ABILITATO = 'isClient("B2C", "B2BSmall")';
    @TestVisible private static final String EMAIL_PRESENTE = 'hasEmail';
    @TestVisible private static final String ABBONAMENTO = 'isAbbonamento';
    @TestVisible private static final String VENDITA_DIRETTA = 'isVenditaDiretta';
    @TestVisible private static final String BUNDLE_MISTI = 'isBundleMisti';
    @TestVisible private static final String SUPPORTO_CARTA = 'hasFormat("carta")';
    @TestVisible private static final String SUPPORTO_DIGITALE = 'hasFormat("digitale")';
    @TestVisible private static final String PRODOTTI_PROFESSIONAL = 'hasProducts("professional")';
    @TestVisible private static final String PRODOTTI_DIFFUSIONE = 'hasProducts("diffusione")';
    @TestVisible private static final String PAGATA = 'isActive';
    @TestVisible private static final String IN_ATTESA_DI_PAGAMENTO = 'isPending';
    @TestVisible private static final String MDP_CBILL = 'hasPaymentMethod("cbill")';
    @TestVisible private static final String MDP_PAYPAL = 'hasPaymentMethod("paypal")';
    @TestVisible private static final String MDP_CONTRASSEGNO = 'hasPaymentMethod("contrassegno")';
    @TestVisible private static final String MDP_BONIFICO = 'hasPaymentMethod("bonifico")';
    @TestVisible private static final String MDP_BOLLETTINO = 'hasPaymentMethod("bollettino postale")';
    @TestVisible private static final String MDP_CARTA = 'hasPaymentMethod("carta di credito")';
	
    @InvocableMethod(label='Notifica Attivazione Subscription' description='Invia una email al Contact_Bill_To quando la Subscription viene creata o attivata.')
	public static void sendEmail(List<Zuora__Subscription__c> subscriptions) {
        System.debug('SubscriptionMailProcessAction started');

        subscriptions = reloadSubscriptions(subscriptions);

        final Blocco_Selettivo_Notifiche__c bsn = Blocco_Selettivo_Notifiche__c.getInstance(UserInfo.getOrganizationId());
        final List<EmailDataBundle> data = fetchEmailData(subscriptions, bsn);
		final List<Messaging.SingleEmailMessage> emails = EmailSendingUtil.prepareEmails(data, EmailSendingUtil.getOrgWideEmailAddressByDisplayName(SENDER), BCCs);
		final List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails, false);

		EmailSendingUtil.debug(results, data);
        checkNotificationFlag(results, data);

        System.debug('SubscriptionMailProcessAction stopped');
    }

    @TestVisible
    // Imposta a True i flag di notifica email (Notifica_*__c) se l'invio dell'email è avvenuto con successo
	private static void checkNotificationFlag(List<Messaging.SendEmailResult> results, List<EmailDataBundle> data) {
        final Map<Id, Zuora__Subscription__c> subToUpdate = new Map<Id, Zuora__Subscription__c>();

		Zuora__Subscription__c sub;
		Messaging.SendEmailResult result;
		EmailDataBundle bundle;
		EmailTemplate template;

		for(Integer i = 0; i < results.size(); i++) {
			result = results.get(i);
			bundle = data.get(i);

			if(result.isSuccess() || Test.isRunningTest()) {
				template = bundle.getTemplate();

                if(bundle.getRelatedTo() instanceof Zuora__Subscription__c) {
                    sub = (Zuora__Subscription__c) bundle.getRelatedTo();

                    if(equals(template.DeveloperName, new String[]{MT1, MT2, MT6, MT8, MT9})) {
                        sub.Notifica_Scontrino__c = true;
                    }

                    if(equals(template.DeveloperName, new String[]{MT1, MT2, MT5, MT9})) {
                        sub.Notifica_Decorrenza__c = true;
                    }

                    if(equals(template.DeveloperName, new String[]{MT1, MT3, MT6, MT9})) {
                        sub.Notifica_Pagamento__c = true;
                    }

                    if(equals(template.DeveloperName, MT4)) {
                        sub.Notifica_Istruzioni_Digital__c = true;
                    }

                    if(!subToUpdate.containsKey(sub.Id)) {
                        subToUpdate.put(sub.Id, sub);
                    }
                }
			}
		}

        System.debug('[SubscriptionMailProcess][UPDATED_SUBSCRIPTIONS] ' + subToUpdate.keySet());

		update subToUpdate.values();
	}

    @TestVisible
	// Recupera i dati necessari per l'invio delle email
	private static List<EmailDataBundle> fetchEmailData(List<Zuora__Subscription__c> subscriptions, Blocco_Selettivo_Notifiche__c bsn) {
		final Map<String, EmailTemplate> templates = EmailSendingUtil.loadTemplates(new String[]{MT1, MT2, MT3, MT4, MT5, MT6, MT8, MT9});

		final Map<Id, Zuora__Subscription__c> subMap = new Map<Id, Zuora__Subscription__c>(subscriptions);
		final Map<Id, Integer> ve2SubMap = countVirtualcommEntitlements(subMap.keySet());
        final Map<Id, Contact> billToContacts = new Map<Id, Contact>([SELECT Id, Name, Email, CodiceCliente__c, Codice_Nazione__c FROM Contact WHERE Id IN :fetchContactIds(subscriptions)]);
		final List<EmailDataBundle> data = new List<EmailDataBundle>();
        Map<String, Boolean> flags;

		Contact billToContact;

		for(Zuora__Subscription__c sub : subscriptions) {
            System.debug('>> Subscription: ' + sub);

			billToContact = billToContacts.get(sub.Contact_Bill_To__c);
			if(billToContact == null) continue; // Contact non trovato

            System.debug('>> Referente di fatturazione: ' + billToContact);

            flags = analyzeSubscription(sub);
            System.debug('>> Flags: ' + flags);

            if(flags.get(NOTIFICHE_EMAIL_ABILITATE) && flags.get(CLIENTE_ABILITATO) && flags.get(EMAIL_PRESENTE)) { // Controlli sul BillTo contact
                if(flags.get(SOSPESA) || flags.get(OMAGGIO)) {
                    continue;   // I clienti proprietari di subscription sospese o omaggio non ricevono notifiche in merito al momento
                }

                if(flags.get(ABBONAMENTO) && (!flags.get(PRODOTTI_PROFESSIONAL) || flags.get(PRODOTTI_DIFFUSIONE))) { // ABBONAMENTO
                    if(flags.get(SUPPORTO_DIGITALE)) {
                        if(bsn.MT4_Abilitato__c && flags.get(MT4_NON_INVIATA) && ve2SubMap.get(sub.Id) > 0) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT4)));
                        }
                    }

                    if(flags.get(FATTURA_A_TERZI)) {
                        if(bsn.MT5_Abilitato__c && flags.get(MT5_NON_INVIATA)) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT5)));
                        }
                    } else if(flags.get(PAGATA)) {
                        if(flags.get(MDP_PAYPAL) || flags.get(MDP_BONIFICO) || flags.get(MDP_CARTA)) {
                            if(bsn.MT1_Abilitato__c && flags.get(MT1_NON_INVIATA)) {
                                data.add(new EmailDataBundle(sub, billToContact, templates.get(MT1)));
                            }
                        } else if(flags.get(MDP_CBILL) || flags.get(MDP_BOLLETTINO)) {
                            if(bsn.MT3_Abilitato__c && flags.get(MT3_NON_INVIATA)) {
                                data.add(new EmailDataBundle(sub, billToContact, templates.get(MT3)));
                            }
                        }
                    } else if(flags.get(IN_ATTESA_DI_PAGAMENTO)) {
                        if(flags.get(MDP_CBILL) || flags.get(MDP_BOLLETTINO)) {
                            if(bsn.MT2_Abilitato__c && flags.get(MT2_NON_INVIATA)) {
                                data.add(new EmailDataBundle(sub, billToContact, templates.get(MT2)));
                            }
                        }
                    }
                } else if(flags.get(VENDITA_DIRETTA)) { // VENDITA DIRETTA
                    if(!flags.get(SUPPORTO_CARTA) && flags.get(SUPPORTO_DIGITALE)) {
                        if(bsn.MT4_Abilitato__c && flags.get(MT4_NON_INVIATA) && ve2SubMap.get(sub.Id) > 0) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT4)));
                        }
                    }

                    if(flags.get(FATTURA_A_TERZI)) {
                        continue;   // I clienti che acquistano attraverso terzi non ricevono notifiche al momento
                    }

                    if(flags.get(PAGATA) && (flags.get(MDP_PAYPAL) || flags.get(MDP_BONIFICO) || flags.get(MDP_CARTA))) {
                        if(bsn.MT6_Abilitato__c && flags.get(MT6_NON_INVIATA)) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT6)));
                        }
                    } else if(flags.get(PAGATA) && (flags.get(MDP_CONTRASSEGNO))) {
                        if(bsn.MT8_Abilitato__c && flags.get(MT8_NON_INVIATA)) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT8)));
                        }
                    }
                } else if(flags.get(BUNDLE_MISTI)) { // BUNDLE MISTI
                    if(flags.get(SUPPORTO_DIGITALE)) {
                        if(bsn.MT4_Abilitato__c && flags.get(MT4_NON_INVIATA) && ve2SubMap.get(sub.Id) > 0) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT4)));
                        }
                    }

                    if(flags.get(FATTURA_A_TERZI)) {
                        continue;   // I clienti che acquistano attraverso terzi non ricevono notifiche al momento
                    }

                    if(flags.get(PAGATA) && (flags.get(MDP_PAYPAL) || flags.get(MDP_BONIFICO) || flags.get(MDP_CARTA))) {
                        if(bsn.MT9_Abilitato__c && flags.get(MT9_NON_INVIATA)) {
                            data.add(new EmailDataBundle(sub, billToContact, templates.get(MT9)));
                        }
                    }
                }
            }
		}

		System.debug('EmailDataBundles: ' + data);
		return data;
	}

    /**
     * Analizza la Subscription e crea una mappa di flag contenente tutte le informazioni utili all'invio delle notifiche email.
     */

    @TestVisible
    private static Map<String, Boolean> analyzeSubscription(Zuora__Subscription__c sub) {
        final Map<String, Boolean> flags = new Map<String, Boolean>();

        flags.put(NOTIFICHE_EMAIL_ABILITATE, isNotBlank(sub.Zuora__Account__c) && isFalse(sub.Zuora__Account__r.Notifiche_Email_Disabilitate__c));
        
        flags.put(MT1_NON_INVIATA, isFalse(sub.Notifica_Scontrino__c) && isFalse(sub.Notifica_Decorrenza__c) && isFalse(sub.Notifica_Pagamento__c));
        flags.put(MT2_NON_INVIATA, isFalse(sub.Notifica_Scontrino__c) && isFalse(sub.Notifica_Decorrenza__c));
        flags.put(MT3_NON_INVIATA, isFalse(sub.Notifica_Pagamento__c));
        flags.put(MT4_NON_INVIATA, isFalse(sub.Notifica_Istruzioni_Digital__c));
        flags.put(MT5_NON_INVIATA, isFalse(sub.Notifica_Decorrenza__c));
        flags.put(MT6_NON_INVIATA, isFalse(sub.Notifica_Scontrino__c) && isFalse(sub.Notifica_Pagamento__c));
        flags.put(MT8_NON_INVIATA, isFalse(sub.Notifica_Scontrino__c));
        flags.put(MT9_NON_INVIATA, isFalse(sub.Notifica_Scontrino__c) && isFalse(sub.Notifica_Decorrenza__c) && isFalse(sub.Notifica_Pagamento__c));
        
        flags.put(SOSPESA, isTrue(sub.isAmendment__c));
        flags.put(OMAGGIO, isNotBlank(sub.Campagna__c) && isTrue(sub.Campagna__r.Omaggio__c));
        flags.put(FATTURA_A_TERZI, isTrue(sub.Fattura_a_azienda__c) || isTrue(sub.Fattura_a_intermediario__c));
        flags.put(CLIENTE_ABILITATO, equals(sub.Tipo_Cliente__c, new String[]{'B2C', 'B2BSmall'}));
        flags.put(EMAIL_PRESENTE, isNotBlank(sub.Contact_Bill_To__c) && isNotBlank(sub.Contact_Bill_To__r.Email));

        flags.put(ABBONAMENTO, equals(sub.Tipo_Subscription__c, new String[]{'Abbonamento', 'Bundle abbonamenti'}));
        flags.put(VENDITA_DIRETTA, equals(sub.Tipo_Subscription__c, new String[]{'Vendita diretta', 'Bundle vendite dirette'}));
        flags.put(BUNDLE_MISTI, equals(sub.Tipo_Subscription__c, new String[]{'Bundle misti'}));

        flags.put(SUPPORTO_CARTA, containsIgnoreCase(sub.Supporto__c, 'Carta'));
        flags.put(SUPPORTO_DIGITALE, containsIgnoreCase(sub.Supporto__c, 'Digitale'));

        flags.put(PRODOTTI_PROFESSIONAL, isNotBlank(sub.Rate_Plan_Abbonamento__c) && containsAnyIgnoreCase(sub.Rate_Plan_Abbonamento__r.Name, TESTATE_PROFESSIONAL));
        flags.put(PRODOTTI_DIFFUSIONE, isNotBlank(sub.Rate_Plan_Abbonamento__c) && containsAnyIgnoreCase(sub.Rate_Plan_Abbonamento__r.Name, TESTATE_DIFFUSIONE));

        flags.put(PAGATA, equals(sub.Zuora__Status__c, 'Active') && isNotBlank(sub.Data_pagamento_subscription__c));
        flags.put(IN_ATTESA_DI_PAGAMENTO, contains(sub.Zuora__Status__c, 'Pending') && isBlank(sub.Data_pagamento_subscription__c));

        flags.put(MDP_CBILL, containsIgnoreCase(sub.Metodo_di_Pagamento__c, 'cbill'));
        flags.put(MDP_PAYPAL, containsIgnoreCase(sub.Metodo_di_Pagamento__c, 'paypal'));
        flags.put(MDP_CONTRASSEGNO, containsIgnoreCase(sub.Metodo_di_Pagamento__c, 'contrassegno'));
        flags.put(MDP_BONIFICO, containsAllIgnoreCase(sub.Metodo_di_Pagamento__c, new String[]{'bonifico', 'sepa'}));
        flags.put(MDP_CARTA, containsAllIgnoreCase(sub.Metodo_di_Pagamento__c, new String[]{'carta', 'credito'}));
        flags.put(MDP_BOLLETTINO, containsAllIgnoreCase(sub.Metodo_di_Pagamento__c, new String[]{'bollettino', 'postale'}));

        return flags;
    }

    /**
     * Recupera tutte i campi utili relativi alle subscription indicate.
     */

    @TestVisible
    private static List<Zuora__Subscription__c> reloadSubscriptions(List<Zuora__Subscription__c> subscriptions) {
        final Set<Id> ids = fetchSubscriptionIds(subscriptions);

        return [SELECT Id, Name, CreatedDate, Zuora__Account__c, Notifica_Scontrino__c, Notifica_Decorrenza__c, Notifica_Pagamento__c, Notifica_Istruzioni_Digital__c, 
                isAmendment__c, Campagna__c, Fattura_a_azienda__c, Fattura_a_intermediario__c, Tipo_Cliente__c, Contact_Bill_To__c, Tipo_Subscription__c, 
                Zuora__Status__c, Supporto__c, Rate_Plan_Abbonamento__c, Data_pagamento_subscription__c, Metodo_di_Pagamento__c, Campagna__r.Omaggio__c, 
                Contact_Bill_To__r.CodiceCliente__c, Contact_Bill_To__r.Codice_Nazione__c, Contact_Bill_To__r.Email, Zuora__Account__r.Notifiche_Email_Disabilitate__c, 
                Rate_Plan_Abbonamento__r.Name FROM Zuora__Subscription__c WHERE Id IN :ids];
    }

    // Recupera l'id delle subscription
    @TestVisible
    private static Set<Id> fetchSubscriptionIds(List<Zuora__Subscription__c> subscriptions) {
        final Set<Id> ids = new Set<Id>();

		for(Zuora__Subscription__c sub : subscriptions) {
			ids.add(sub.Id);
		}

		System.debug('Subscription-Ids: ' + ids);
		return ids;
    }

    // Recupera l'id dei Contact_Bill_To__c (Referente di fatturazione)
    @TestVisible
	private static Set<Id> fetchContactIds(List<Zuora__Subscription__c> subscriptions) {
		final Set<Id> ids = new Set<Id>();

		for(Zuora__Subscription__c sub : subscriptions) {
			ids.add(sub.Contact_Bill_To__c);
		}

		System.debug('Contact-Ids: ' + ids);
		return ids;
	}

    // Conta il numero di Subscription Product Charge con supporto digitale il cui entitlement è di tipo Virtualcomm
    @TestVisible
    private static Map<Id, Integer> countVirtualcommEntitlements(Set<Id> subIds) {
        final Map<Id, Integer> countMap = new Map<Id, Integer>();
        final AggregateResult[] results = [SELECT Zuora__Subscription__c, COUNT(id) N FROM Zuora__SubscriptionProductCharge__c 
                                            WHERE Zuora__Subscription__c IN :subIds AND Supporto__c LIKE '%Digitale%' AND Entitlement__c = 'Virtualcomm' 
                                            GROUP BY Zuora__Subscription__c];

        for(AggregateResult ar : results) {
            countMap.put((Id) ar.get('Zuora__Subscription__c'), Integer.valueOf(ar.get('N')));
        }

        return countMap;
    }

    /********** Metodi statici di utilità **********/

    private static Boolean equals(String src, String value) {
        return String.isNotBlank(src) && String.isNotBlank(value) && src.equals(value);
    }

    private static Boolean equalsIgnoreCase(String src, String value) {
        return String.isNotBlank(src) && String.isNotBlank(value) && src.equalsIgnoreCase(value);
    }

    private static Boolean equals(String src, String[] values) {
        if(String.isNotBlank(src)) {
            for(String value : values) {
                if(String.isNotBlank(value) && src.equals(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Boolean equalsIgnoreCase(String src, String[] values) {
        if(String.isNotBlank(src)) {
            for(String value : values) {
                if(String.isNotBlank(value) && src.equalsIgnoreCase(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Boolean contains(String src, String value) {
        return String.isNotBlank(src) && String.isNotBlank(value) && src.contains(value);
    }

    private static Boolean containsIgnoreCase(String src, String value) {
        return String.isNotBlank(src) && String.isNotBlank(value) && src.toLowerCase().contains(value.toLowerCase());
    }

    private static Boolean containsAny(String src, String[] values) {
        if(String.isNotBlank(src)) {
            for(String value : values) {
                if(String.isNotBlank(value) && src.contains(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Boolean containsAnyIgnoreCase(String src, String[] values) {
        if(String.isNotBlank(src)) {
            src = src.toLowerCase();

            for(String value : values) {
                if(String.isNotBlank(value) && src.contains(value.toLowerCase())) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Boolean containsAll(String src, String[] values) {
        if(String.isNotBlank(src)) {
            for(String value : values) {
                if(String.isNotBlank(value) && !src.contains(value)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private static Boolean containsAllIgnoreCase(String src, String[] values) {
        if(String.isNotBlank(src)) {
            src = src.toLowerCase();

            for(String value : values) {
                if(String.isNotBlank(value) && !src.contains(value.toLowerCase())) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private static Boolean isTrue(Boolean src) {
        return src != null && src;
    }

    private static Boolean isFalse(Boolean src) {
        return src != null && !src;
    }

    private static Boolean isBlank(Object src) {
        if(src instanceof String) {
            return String.isBlank((String) src);
        } else {
            return src == null;
        }
    }

    private static Boolean isNotBlank(Object src) {
        if(src instanceof String) {
            return String.isNotBlank((String) src);
        } else {
            return src != null;
        }
    }
}