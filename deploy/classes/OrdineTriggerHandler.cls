public class OrdineTriggerHandler {
	
	public static Boolean skip = false;
    
    public void onBeforeInsert(List<Ordine__c> triggerNew, Map<Id, Ordine__c> triggerNewMap) { 
        //setFields(triggerNew);
        setOwnerId(triggerNew);
    }
    
    public void onAfterInsert(List<Ordine__c> triggerNew, Map<Id, Ordine__c> triggerNewMap) {
        aggiornaTipoCliente(triggerNew);
    }

    public void onBeforeUpdate(List<Ordine__c> triggerOld, List<Ordine__c> triggerNew, Map<Id, Ordine__c> triggerOldMap, Map<Id, Ordine__c> triggerNewMap){   
        setFields(triggerNew);
        setOwnerId(triggerNew);
    }

    public void onAfterUpdate(List<Ordine__c> triggerOld, List<Ordine__c> triggerNew, Map<Id, Ordine__c> triggerOldMap, Map<Id, Ordine__c> triggerNewMap){   

    }

    public void onBeforeDelete(List<Ordine__c> triggerOld, Map<Id, Ordine__c> triggerOldMap){  
        delete [SELECT Id FROM RigaOrdine__c WHERE Ordine__c IN :triggerOldMap.keySet()];   // Per aggiornare il campo Prodotto__c sui contatti
    }

    public void onAfterDelete(List<Ordine__c> triggerOld, Map<Id, Ordine__c> triggerOldMap){  
        
    }

    public static void setFields(List<Ordine__c> triggerNew){
    	Set<Id> accountIds = new Set<Id>();
        Map<Id,Date> dataUltimoOrdineAccount = new Map<Id,Date>();
        Set<String> accountFields = new Set<String>{'Cliente_Committente__c','Cliente_Esecutore_Pagamento__c','Cliente_Destinatario__c','Cliente_Socio_Attivatore__c'};
        for(Ordine__c ordine : triggerNew){
        	sObject ordineObj = (sObject) ordine;
        	for(String accountField : accountFields){
        		if(ordineObj.get(accountField) != null){
        			Id accountId = (Id) ordineObj.get(accountField); 
        			accountIds.add(accountId);
	        		if(ordineObj.get('Fine_validita__c') != null){
		        		if(!dataUltimoOrdineAccount.containsKey(accountId)){
		        			dataUltimoOrdineAccount.put(accountId, null);
		        		}
	        			Date dataFineValidita = Date.valueOf(ordineObj.get('Fine_validita__c'));
		        		Date d = dataUltimoOrdineAccount.get(accountId);
		        		if(d == null || d < dataFineValidita){
							dataUltimoOrdineAccount.put(accountId, dataFineValidita);		        			
		        		}
	        		}
        		}
        	}
        }

        List<Account> accounts = [SELECT Id,Stato__c,Data_Fine_Ultimo_Ordine__c 
        							FROM Account 
        							WHERE Id IN :accountIds
        							AND Stato__c != 'Cliente'];
        for(Account a : accounts){
        	a.Stato__c = 'Cliente';
        	if(dataUltimoOrdineAccount.containsKey(a.Id) && dataUltimoOrdineAccount.get(a.Id) != null){
        		a.Data_Fine_Ultimo_Ordine__c = dataUltimoOrdineAccount.get(a.Id);
        	}
        }
        AccountTriggerHandler.skip = true;
        update accounts;
        AccountTriggerHandler.skip = false;
    }

    public static void aggiornaTipoCliente(List<Ordine__c> ordini){
        
        Set<Id> accountIdsProf = new Set<Id>();
        for (ordine__c ordine : [SELECT id,Cliente_Committente__c From ordine__c where id in :ordini]) {
                    System.debug('******************** ordine ***************' + ordine);
                    accountIdsProf.add(ordine.Cliente_Committente__c);
            }
        List<Account> accountProf = [SELECT Id,Tipo_cliente__c FROM Account WHERE Id IN :accountIdsProf];
        for(Account acc : accountProf){
                    if(acc.Tipo_Cliente__c == 'Diffusione'){
                            acc.Tipo_Cliente__c = 'Professional;Diffusione';
                    } else { 
                        acc.Tipo_cliente__c = 'Professional';
                    }
        }

            AccountTriggerHandler.skip = true;
            update accountProf;
            AccountTriggerHandler.skip = false;
    }

    private static Map<String, User> getAgentiCommerciali(){

        Map<String, User> usersMap = new Map<String, User>();
        List<User> usersList = [SELECT Id, EmployeeNumber FROM User WHERE IsActive = true AND EmployeeNumber != null];
        
        for(User u : usersList){
            if(!usersMap.containsKey(u.EmployeeNumber)){
                usersMap.put(u.EmployeeNumber, u);
            }
        }

        return usersMap;
    }

    private static void setOwnerId(List<Ordine__c> triggerNew){

        Map<String, User> usersMap = getAgentiCommerciali();

        for(Ordine__c o : triggerNew){
            if(o.Cod_agente__c != null){
                if(usersMap.containsKey(o.Cod_agente__c)){
                    o.OwnerId = usersMap.get(o.Cod_agente__c).Id;
                }
            }
        }
    }
}