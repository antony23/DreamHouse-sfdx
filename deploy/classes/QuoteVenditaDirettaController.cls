public class QuoteVenditaDirettaController {

	private Id venditeDiretteId;
	private Set<Id> searchIdSet;
    private Set<Id> addedIdSet;

    public String removedCodice {get;set;}
    public String removedIndex {get;set;}

    public String selectedSupport {get;set;}
    //public String selectedLanguage {get;set;}
    public String selectedTestata {get;set;}
    public String selectedProductType {get;set;}
    public String searchText {get;set;}

    public Id productId {get;set;}
    public String codiceProdotto{get;set;}
    public Integer prodGiacenza{get;set;}
    public Integer prodQuantita{get;set;}

    public List<WrapProduct> addedList {get;set;}
    public List<WrapProduct> searchList {get;set;}
    public String codiceConvenzionePerScontistica {get;set;}
    public zqu.Quote quote {get;set;}
    public Boolean loadQuote = true;

	public Id quoteId {get;set{
			if(String.isBlank(quoteId)){
				quoteId = value;
				if(loadQuote && quote == null && quoteId != null){
					quote = zqu.Quote.getInstance(quoteId);
					loadQuote = false;
				}
				zqu__quote__c quoteWithIntermerdiario = [SELECT Id, Intermediario__c,Intermediario__r.Codice_Convenzione__c FROM zqu__quote__c WHERE Id = :quoteId];
	            if(quoteWithIntermerdiario.Intermediario__c != null){
	                codiceConvenzionePerScontistica = quoteWithIntermerdiario.Intermediario__r.Codice_Convenzione__c;
	            }
				getVenditeDirette();
			}
		}
	}
	public Boolean editMode{get;set;}

	public List<SelectOption> supportOptions {
    	get{
			supportOptions = new List<SelectOption>();
		   	supportOptions.add(new SelectOption('', 'Tutti i supporti'));
	        Schema.DescribeFieldResult supportFields = Prodotto_Vendita_Diretta__c.Supporto__c.getDescribe();
			List<Schema.PicklistEntry> supportEntries = supportFields.getPicklistValues();
			for( Schema.PicklistEntry f : supportEntries)	{
    			supportOptions.add(new SelectOption(f.getLabel(), f.getValue()));
   			} 
   			return supportOptions;
    	} 
    	set;
	}
    
    public List<SelectOption> prodTypeOptions {
    	get{
			prodTypeOptions = new List<SelectOption>();
		   	prodTypeOptions.add(new SelectOption('', 'Tutti i prodotti'));
	        Schema.DescribeFieldResult prodTypeFields = Prodotto_Vendita_Diretta__c.Tipologia_Prodotto__c.getDescribe();
			List<Schema.PicklistEntry> prodTypeEntries = prodTypeFields.getPicklistValues();
			for( Schema.PicklistEntry f : prodTypeEntries){
		    	prodTypeOptions.add(new SelectOption(f.getLabel(), f.getValue()));
		   	}   
		   	return prodTypeOptions;
    		}
    	set;
   	}
    public List<SelectOption> testataOptions {
    	get{
    		testataOptions = new List<SelectOption>();
		   	testataOptions.add(new SelectOption('', 'Tutte le testate'));
		   	List<Testata__c> testataList = [SELECT id, Name FROM Testata__c ORDER BY Name];
			for( Testata__c f : testataList)	{
		    	testataOptions.add(new SelectOption(f.Id, f.Name));
		   	} 
		   	return testataOptions;
    	}
    	set;
    }

	public QuoteVenditaDirettaController(){
		init();	
	}

	public class WrapProduct  {
        public Prodotto_Vendita_Diretta__c product {get; set;}
        public Integer quantita {get; set;}
        public Boolean isDigitale {get;set;}
        public Integer giacenza {get;set;}
        public Decimal prezzo {get;set;}
        public zqu.Product zProduct {get; set;}
        public String note {get;set;}
        public String codice {get;set;}
        public Boolean isSplit {get;set;}

        public WrapProduct(Prodotto_Vendita_Diretta__c p, Integer q){
            product = p;
            quantita = q;
            isDigitale = false;
        }

        public WrapProduct(Prodotto_Vendita_Diretta__c p){
            product = p;
            quantita = 1;
            isDigitale = false;
        }
    }

    public Boolean doInit = true;

	private void init(){
		System.debug('********* SONO ENTRATO IN INIT ***********');
		try{
			System.debug('********* doinit *****' + doInit);
			if(doInit){
				List<Product2> productList = [SELECT Id FROM Product2 WHERE Name ='VENDITE DIRETTE'];
				System.debug('*************** productlist ' + productlist);
				System.debug('*************** producltist.size ' + productlist.size());

			    if(productList.size() == 1){
			    	venditeDiretteId = productList.get(0).Id;
			    	System.debug('************ venditeDiretteId  ' + venditeDiretteId);
			    }
				addedList = new List<WrapProduct>();
			    searchList = new List<WrapProduct>();
			    addedIdSet = new Set<Id>();
			    searchIdSet = new Set<Id>();
				doInit = false;
			}
		} catch(Exception e) {
            appendErrorMessage(e);
            System.debug(loggingLevel.Error, '****ERRORE DOINIT *******: ' + e.getMessage() + ' ' + e.getStackTraceString());

        }
	}

	public PageReference goToCart(){
		try {
			System.debug(loggingLevel.Error, '*** quoteId: ' + quoteId);
	    	DomusUtil.aggiornaScontiVenditeDiretteNewV(quote,quoteId,codiceConvenzionePerScontistica);
	    	//DomusUtil.aggiornaScontiVenditeDirette(quoteId);
			return Page.WizardCart;
		} catch(Exception e) {
			appendErrorMessage(e);
		}
		return null;
    }

    public PageReference goToEdit(){
    	return Page.WizardDirectSales;
    }

	

	private void getVenditeDirette(){
		try{
			List<zqu__QuoteRatePlan__c> quoteVenditeDiretteList = DomusUtil.getQuoteVenditeDirette(quoteId);
			
			Map<Id, Decimal> prodAmountMap = new Map<Id, Decimal>();
			Map<Id, Boolean> prodIsSplitMap = new Map<Id, Boolean>();
			for(zqu__QuoteRatePlan__c qvd : quoteVenditeDiretteList){
				prodAmountMap.put(qvd.Prodotto_Vendita_Diretta__c, qvd.Prodotto_Vendita_Diretta_Qta__c);
				prodIsSplitMap.put(qvd.Prodotto_Vendita_Diretta__c, qvd.isSplit__c);
			}

			List<Id> venditeDiretteIdList = new List<Id>();
			for(zqu__QuoteRatePlan__c qrp : quoteVenditeDiretteList){
				venditeDiretteIdList.add(qrp.Prodotto_Vendita_Diretta__c);
				addedIdSet.add(qrp.Prodotto_Vendita_Diretta__c);
			}

			List<zqu__QuoteRatePlanCharge__c> quoteRatePlanChargeList =  [SELECT Id, Prodotto_Vendita_Diretta__c, zqu__EffectivePrice__c, zqu__Quantity__c,zqu__Total__c
																			FROM zqu__QuoteRatePlanCharge__c
																			WHERE Prodotto_Vendita_Diretta__c = :venditeDiretteIdList];
			
			Map<Id, Decimal> venditeEffectivePriceMap = new Map<Id, Decimal>();
			for(zqu__QuoteRatePlanCharge__c charge : quoteRatePlanChargeList){
				venditeEffectivePriceMap.put(charge.Prodotto_Vendita_Diretta__c, charge.zqu__Total__c); //charge.zqu__EffectivePrice__c * charge.zqu__Quantity__c);
			}

			Map<Id, Decimal> stockMap = DomusUtil.getMovimentazioneStock(venditeDiretteIdList);

			List<Prodotto_Vendita_Diretta__c> venditeDiretteList = DomusUtil.getVenditeDirette(venditeDiretteIdList);

			for(Prodotto_Vendita_Diretta__c pvd : venditeDiretteList){
				Decimal amount = prodAmountMap.get(pvd.Id);
				if(amount == null || amount == 0){
					amount = 1;
				}
				WrapProduct wp = new WrapProduct(pvd, Integer.valueOf(amount));
				wp.codice = pvd.id;

				if(pvd.Supporto__c == 'Digitale'){
					wp.isDigitale = true;
				}else{
    				wp.giacenza = Integer.valueOf(pvd.Giacenza__c);
					if(stockMap.containsKey(pvd.id)){
						 wp.giacenza += Integer.valueOf(stockMap.get(pvd.Id));
					}
				}
				if(prodIsSplitMap.get(pvd.Id)){
					wp.isSplit = true;
					Decimal prezzoIssue = 0;
					if(wp.product.Testata__r.Prezzo_Issue__c != null){
						prezzoIssue = wp.product.Testata__r.Prezzo_Issue__c;
					}
					wp.prezzo = wp.product.Prezzo_Attuale__c - prezzoIssue;
					wp.codice = pvd.id+'_SPLIT';
				}else{
					wp.isSplit = false;
					wp.prezzo = wp.product.Prezzo_Attuale__c;
					if(venditeEffectivePriceMap.get(wp.product.Id) != null){
						wp.prezzo = venditeEffectivePriceMap.get(wp.product.Id);
					}
				}
				addedList.add(wp);
				
			}
		} catch(Exception e) {
            appendErrorMessage(e);
        }
	}

	public void searchVenditeDirette(){
		try{
			if(!String.isBlank(selectedSupport) ||  (!String.isBlank(selectedProductType)) || (!String.isBlank(selectedTestata))||
				!String.isBlank(searchText))
			{
	    		searchIdSet = new Set<Id>();
	    		searchList = new List<WrapProduct>();

	    		Boolean firstSelected = false;
	    		String query = 'SELECT Id, Name, Tipologia_Prodotto__c, Testata__c, Supporto__c, Lingua__c, Prezzo_Attuale__c, Gruppo_Contabilizzazione_Prodotto__c, '+
	    						'Unit_di_peso__c, Testata__r.Prezzo_Issue__c, Peso__c, Riferimento_Magazzino__c, Testata__r.Name, Giacenza__c, Codice_Prodotto_SAP__c, Descrizione_Prodotto_SAP__c,Tipo_Vendibilita__c FROM Prodotto_Vendita_Diretta__c ';

	    		String professionalSupporto = 'Professional';
	    		query += 'WHERE Supporto__c !=  :professionalSupporto ';
	    		firstSelected = true;

	    		if(selectedSupport!=null && selectedSupport!=''){
	    			query += (firstSelected ? 'AND ' : 'WHERE ');
	    			if(!firstSelected) firstSelected = true;
	    			query += 'Supporto__c = :selectedSupport ';
	    		}

	    		if(selectedProductType!=null && selectedProductType!=''){
	    			query += (firstSelected ? 'AND ' : 'WHERE ');
	    			if(!firstSelected) firstSelected = true;
	    			query += 'Tipologia_Prodotto__c = :selectedProductType ';
	    		}

	    		if(selectedTestata!=null && selectedTestata!=''){
	    			query += (firstSelected ? 'AND ' : 'WHERE ');
	    			if(!firstSelected) firstSelected = true;
	    			query += 'Testata__c = :selectedTestata ';
	    		}

	    		if(!String.isBlank(searchText)){
	    			query += (firstSelected ? 'AND ' : 'WHERE ');
	    			if(!firstSelected) firstSelected = true;
	    			query += '(Name LIKE \'%'+ String.escapeSingleQuotes(searchText) +'%\' OR Codice_Prodotto_SAP__c LIKE \'%'+ String.escapeSingleQuotes(searchText) +'%\' )';
	    		}
	    		query += ' LIMIT 100';
	    		System.debug('Query: ' + query);

	    		List<Prodotto_Vendita_Diretta__c> productList = (List<Prodotto_Vendita_Diretta__c>) Database.query(query);

	    		System.debug('productList' + productList);

	    		searchList = new List<WrapProduct>();

	    		if (productList.size() > 0)
	    		{
	    			if(productList.size() >= 100){
	    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Attenzione trovati più di 100 prodotti, affinare la ricerca con ulteriori filtri'));
	    			}
		    		List<Id> venditeDiretteIdList = new List<Id>();
		    		for(Prodotto_Vendita_Diretta__c p : productList){
		    			venditeDiretteIdList.add(p.Id);
		    		}
		    		Map<Id, Decimal> stockMap = DomusUtil.getMovimentazioneStock(venditeDiretteIdList);
		    		for(Prodotto_Vendita_Diretta__c pvd : productList){
		    			System.debug(pvd.Name);	
		    			searchIdSet.add(pvd.Id);
		    			if(!addedIdSet.contains(pvd.Id) || pvd.Codice_Prodotto_SAP__c.containsIgnoreCase('split')){
		    				if(pvd.Giacenza__c != null){
		    					WrapProduct wp = new WrapProduct(pvd);
		    					wp.codice = pvd.id;
			    				wp.prezzo = wp.product.Prezzo_Attuale__c;

		    					if(pvd.Supporto__c == 'Digitale'){
		    						wp.isDigitale = true;
		    					}else{
				    				wp.giacenza = Integer.valueOf(pvd.Giacenza__c);
				    				if(stockMap.containsKey(pvd.id)){
				    					wp.giacenza += Integer.valueOf(stockMap.get(pvd.Id));
				    				}
		    					}
			    				wp.isSplit = false;
			    				searchList.add(wp);
			    				System.debug(pvd.name);
			    				if((pvd.Codice_Prodotto_SAP__c != null && pvd.Codice_Prodotto_SAP__c.containsIgnoreCase('split')) || (pvd.Descrizione_Prodotto_SAP__c != null && pvd.Descrizione_Prodotto_SAP__c.containsIgnoreCase('split'))){
			    					System.debug('SPLIT PRODUCT');
									WrapProduct splitWp = new WrapProduct(pvd);
									splitWp.codice = pvd.id+'_SPLIT';
									splitWp.giacenza = wp.giacenza;
									//splitWp.note = 'SOLO ABBONATI';
									splitWp.isSplit = true;
									Decimal prezzoIssue = 0;
									if(wp.product.Testata__r.Prezzo_Issue__c != null){
										prezzoIssue = wp.product.Testata__r.Prezzo_Issue__c;
									}
									splitWp.prezzo = wp.product.Prezzo_Attuale__c - prezzoIssue;
									searchlist.add(splitWp);
								}
		    				}
		    			}
		    		}
	    		}else{
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Nessun prodotto trovato'));
	    		}
	    	}
	    	else{
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Seleziona almeno un filtro'));
	    	}
		}
		catch(Exception e) {
            appendErrorMessage(e);
        }
	}


	public void addProduct(){
		try{
			Prodotto_Vendita_Diretta__c tmpProduct;
			Integer quantita;
			String note;
			WrapProduct addedProd;
			Integer index;
			String testata;
	    	for(Integer i = 0; i<searchList.size(); i++)
	    	{
	    		WrapProduct wp = searchList.get(i);
                System.debug('************ Nome testata: ' + wp.product.Testata__r.Name);
                System.debug('wp.codice, codiceProdotto: ' + wp.codice + ', ' + codiceProdotto);
	    		if(wp.codice == codiceProdotto)
	    		{
	    			System.debug('********** Product amount: ' + wp.quantita);
	    			tmpProduct = wp.product;
	    			System.debug('********** tmpProduct ************' + tmpProduct);
	    			testata = wp.product.Testata__r.Name;
	    			quantita = wp.quantita;
	    			note = wp.note;
	    			addedProd = wp;
	    			index = i;
	    			break;
	    		}
	    	}
	    	
	    	if( (String.isBlank(tmpProduct.Tipo_Vendibilita__c) || !tmpProduct.Tipo_Vendibilita__c.equalsIgnoreCase('P')) && tmpProduct.Supporto__c != 'Digitale' && addedProd.giacenza < quantita){
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: la quantità selezionata eccede il numero massimo di copie'));
	    	}else if(tmpProduct.Supporto__c != 'Digitale' &&  quantita < 1 ){
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: immettere almeno 1 come quantita'));
	    	}else if(addedIdSet.contains(addedProd.product.id)){
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Il prodotto selezionato è già stato aggiunto'));
	    	}else{
		    	Integer result = DomusUtil.setVenditaDiretta(quoteId, tmpProduct.Id, venditeDiretteId, quantita, note, addedProd.isSplit);
		    	System.debug('****************INTEGER RESULT*****************  ' + result);
		    	if(result == 0){
					addedList.add(addedProd);
					addedIdSet.add(addedProd.product.Id);
					searchList.remove(index);
		    	}else{
		    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result == 1 ? 'Nessun rateplan disponibile per il prodotto selezionato' : 'Errore generico'));
		    	}
	    	}
	    } catch(Exception e) {
            appendErrorMessage(e);
            System.debug(loggingLevel.Error, '****ERRORE*******: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
    }
	
	public void updateCharge(){
		try{
			String warningMsg;

			Prodotto_Vendita_Diretta__c tmpProduct;

			for(WrapProduct wp : addedList){
				if(wp.codice == codiceProdotto){
					tmpProduct = wp.product;
					break;
				}
			}

			if( (String.isBlank(tmpProduct.Tipo_Vendibilita__c) || !tmpProduct.Tipo_Vendibilita__c.equalsIgnoreCase('P')) && prodQuantita > prodGiacenza){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: la quantità selezionata eccede il numero massimo di copie'));
			}else if (prodQuantita < 1){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: immettere almeno 1 come quantita'));
			}
			else{
				if(prodQuantita != null && prodQuantita >=1 && (prodQuantita <= prodGiacenza || (String.isNotBlank(tmpProduct.Tipo_Vendibilita__c) && tmpProduct.Tipo_Vendibilita__c.equalsIgnoreCase('P')))){
					System.debug(loggingLevel.Error, '*** Aggiorno quantità: ' + prodQuantita);
					DomusUtil.updateVenditaDiretta(quoteId, codiceProdotto.replace('_SPLIT', ''), prodQuantita);
				}else{
					warningMsg = 'Quantità non valida';
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, warningMsg));
				}

				String testata;
				for(WrapProduct wp : addedList){
					if(wp.codice == codiceProdotto){
						testata = wp.product.Testata__r.Name;
						break;
					}
				}

			}
		} catch(Exception e) {
            appendErrorMessage(e);
        }
	}


	public void removeProduct(){
		try{
			String removedIndex;
			String testata;
			removedIndex = removedCodice.replace('_SPLIT', '');
			zqu__QuoteRatePlan__c qrp = DomusUtil.getQuoteVenditeDirette(quoteId, removedIndex);
			if(qrp != null){
				DomusUtil.removeRatePlan(quoteId, qrp.Id);
				zqu.Quote quote = zqu.Quote.getInstance(quoteId);
	            //quote.save();
	            //delete [SELECT Id FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c = :qrp.Id];
	            //delete [SELECT Id FROM zqu__QuoteRatePlan__c WHERE Id = :qrp.Id];

	            Id productId;
				for(Integer i=0; i < addedList.size(); i++){
	                WrapProduct wp = addedList.get(i);
	                System.debug('wp: '+wp);
	                if(wp.codice == removedCodice){
	                	System.debug('Inside: '+wp.product.Name);
	                    addedList.remove(i);
	                    wp.quantita = 1;
	                    //wp.prezzo = wp.product.Prezzo_Attuale__c;
	                    testata = wp.product.Testata__r.Name;
						addedIdSet.remove(wp.product.Id);
						productId = wp.product.Id;
						searchList.add(wp);
	                    break;
	                }
	            }

	            if(productId != null){
		            List<zqu__QuoteRatePlan__c> quoteCampaignList = [SELECT Id FROM zqu__QuoteRatePlan__c 
	                												WHERE Prodotto_Vendita_Diretta__c = :productId
	                												AND zqu__Quote__c = :quoteId
	                												AND Rate_Plan_Type__c = 'Campagna'];
	                List<Id> quoteCampaignRatePlanId = new List<Id>();
	                if(quoteCampaignList != null && quoteCampaignList.size() > 0){
	                    for(zqu__QuoteRatePlan__c q : quoteCampaignList){
	                        quoteCampaignRatePlanId.add(q.Id);
	                    }
	                    //DomusUtil.removeRatePlanList(quoteId, quoteCampaignRatePlanId);
	                    delete [SELECT Id FROM zqu__QuoteRatePlan__c WHERE Id IN :quoteCampaignRatePlanId];
	                }

		            delete [SELECT Id FROM Movimentazione_Stock__c WHERE Prodotto_Vendita_Diretta__c = :productId AND Quote__c = :quoteId];
	            	
	            }
	            //DomusUtil.aggiornaScontiVenditeDirette(quoteId);
	            DomusUtil.aggiornaScontiVenditeDiretteNewV(quote,quoteId,codiceConvenzionePerScontistica);
			}
		} catch(Exception e) {
            appendErrorMessage(e);
            System.debug(loggingLevel.Error, '****ERRORE*******: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }
	}

	private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }    

}