global class DeleteAvvisiFromRegolaBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.StateFul {
	
	String query;
	Id regolaId = null;
	DocumentLog log = null;
	
	global DeleteAvvisiFromRegolaBatch(Id rId) {
		regolaId = rId;
		log = new DocumentLog();
		query = 'SELECT Id FROM Avviso_di_Rinnovo__c WHERE Regola_Avvisi_e_Solleciti__c = \'' + regolaId + '\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		try {
			return Database.getQueryLocator(query);
		} catch(Exception e) {
			log.body += 'Errore.\r\nMessage:'+e.getMessage()+'\r\nStack:'+e.getStackTraceString()+'\r\n';
		}
		return null;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		try {
   			if(scope != null){
				delete scope;	
				log.body += 'OK';
   			}
   		} catch(Exception e) {
   			log.body += 'Errore.\r\nMessage:'+e.getMessage()+'\r\nStack:'+e.getStackTraceString()+'\r\n';
   		}
	}
	
	global void finish(Database.BatchableContext BC) {
		delete [SELECT Id FROM Document WHERE folderId = :UserInfo.getUserId() AND Name LIKE 'Log_DeleteAvvisiFromRegola_%'];
		insert new Document(
           folderId = UserInfo.getUserId(),
           Name = 'Log_DeleteAvvisiFromRegola_'+ System.now().getTime(),
           Body = Blob.valueOf(log.body)
       );
	}

	public class DocumentLog{
		public String body = '';
	}
	
}