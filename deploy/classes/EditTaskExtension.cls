public without sharing class EditTaskExtension {

	public Task task                        {get; set;}
    public Id taskId                        {get; set;}
    public Boolean isError                  {get; set;}
    public Id userId                        {get; set;}
    public Id profileId                     {get; set;}
    public String profileName               {get; set;}

    public EditTaskExtension(ApexPages.StandardController stdController) {
        isError = false;
        taskId = stdController.getRecord().Id;
        userId = UserInfo.getUserId();
        profileId = UserInfo.getprofileId();
        profileName = [SELECT Name FROM Profile WHERE Id =: profileId LIMIT 1].Name;
        List<String> taskFields = new List<String>();
        for(Schema.SObjectField ft : Schema.getGlobalDescribe().get('Task').getDescribe().Fields.getMap().values()){
            taskFields.add(ft.getDescribe().getName().toLowerCase());
        }
        this.task = (Task) Database.query('SELECT ' + String.join(taskFields, ',') + ' FROM Task WHERE Id = \'' + taskId + '\'');
    }

    public PageReference saveTask(){
        try{
            update task;
        }catch(Exception e){
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return new PageReference(ApexPages.currentPage().getUrl());
        }
        return new ApexPages.StandardController(task).view(); 
    }

    public PageReference back(){
        return new ApexPages.StandardController(task).view();
    }

    public PageReference redirectPage(){
        PageReference p = null;
        if(profileName.containsIgnoreCase('Professional')){
            if(userId != task.CreatedById && userId != task.OwnerId){
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, AppConstants.INSUFFICIENT_PRIVILEGES));
            } 
        }else{
            p = new PageReference('/' + Schema.getGlobalDescribe().get('Task').getDescribe().getKeyPrefix() + '/e');
            Map<String,String> m = p.getParameters();
            m.putAll(ApexPages.currentPage().getParameters());                       
            m.put('nooverride', '1');                        
            p.setRedirect(true);
            //p = new PageReference('/' + taskId + '/e?nooverride=1');
        }
        return p;
    }
}