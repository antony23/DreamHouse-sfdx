global class ProcessSubscription implements Database.Batchable<sObject>,
												Database.StateFul,
												Database.AllowsCallouts,
												Schedulable {
	// Schedulable
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new ProcessSubscription(),100);
	}

	// Batch
	String query;
	Set<Id> subscriptionsToProcessIds;
	Set<Id> subscriptionsProcessedIds;
	Boolean skipMS;
	
	global ProcessSubscription() {
		subscriptionsProcessedIds = new Set<Id>();
		skipMS = false;
		query = 'SELECT Id FROM Zuora__Subscription__c WHERE Label_Run_processed__c = false OR Label_Run_IsError__c = true';        
	}

	global ProcessSubscription(Set<Id> subsIds) {
		subscriptionsToProcessIds = subsIds;
		subscriptionsProcessedIds = new Set<Id>();
		skipMS = false;
		query = 'SELECT Id FROM Zuora__Subscription__c WHERE Id IN :subscriptionsToProcessIds';     
	}

	global ProcessSubscription(Set<Id> subsIds,Boolean skip){
		subscriptionsToProcessIds = subsIds;
		subscriptionsProcessedIds = new Set<Id>();
		query = 'SELECT Id FROM Zuora__Subscription__c WHERE Id IN :subscriptionsToProcessIds';
		skipMS = skip;    
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		Map<Id,Zuora__Subscription__c> subscriptions = ProcessSubscriptionUtil.getSubscriptionsByIds(
			( new Map<Id,Zuora__Subscription__c>( (List<Zuora__Subscription__c>) scope)).keySet()
		);
		try{
			Map<String,Testata__c> testateByName = DomusUtil.getMapTestateByName();
			Map<Id,List<Zuora__SubscriptionProductCharge__c>> subscriptionsCharges = DomusUtil.getSubscriptionRatePlanCharges(subscriptions.keySet());
			Map<String,List<Calendario__c>> calendarioTestate = ProcessSubscriptionUtil.getCalendarioTestate(subscriptionsCharges);

			// CR PROROGHE
			Map<Id, Map<Id, Integer>> numberOfStandardIssuesMap = ProcessSubscriptionUtil.numberOfStandardIssuesBySubscriptionProductCharge(
																							subscriptions.values(), 
																							subscriptionsCharges, 
																							calendarioTestate
																						);

			ProcessSubscriptionUtil.initLabelRunStatistics(subscriptions.values());
			ProcessSubscriptionUtil.calcolaGracingLatenzaSubscriptions(subscriptions, subscriptionsCharges, testateByName, calendarioTestate);

			subscriptionsCharges = DomusUtil.getSubscriptionRatePlanCharges(subscriptions.keySet());

			Map<String,CapDetails__c> capDetails = DomusUtil.getCapDetails();

			List<Asset> copie = ProcessSubscriptionUtil.createCopie(
				subscriptions.values(),
				subscriptionsCharges,
				testateByName,
				calendarioTestate,
				capDetails,
				numberOfStandardIssuesMap	// CR PROROGHE
			);
			List<Asset> copieCreated = DomusUtil.labelRunUpsertSObject(copie, subscriptions);

			List<Asset> prodottiVDDigitali = ProcessSubscriptionUtil.createProdottiVenditaDirettaDigitali(
				subscriptions.values(),
				subscriptionsCharges,
				testateByName,
				calendarioTestate
			);
			DomusUtil.labelRunUpsertSObject(prodottiVDDigitali, subscriptions);

			/* DH-748
			ProcessSubscriptionUtil.setADSFields(copieCreated, subscriptions);
			*/

			System.debug('************** ProcessSubscription subscription.values ***************' + subscriptions.values());
			System.debug('************** ProcessSubscription subscriptionsCharges ***************' + subscriptionsCharges);
			System.debug('************** ProcessSubscription calendarioTestate ***************' + calendarioTestate);
			List<Entitlement__c> entitlements = ProcessSubscriptionUtil.createEntitlements(
				subscriptions.values(),
				subscriptionsCharges,
				calendarioTestate
			);
			DomusUtil.labelRunUpsertSObject(entitlements, subscriptions);

			if(!skipMS){
				List<Movimentazione_Stock__c> movimentazioniStock = ProcessSubscriptionUtil.createMovimentazioniStock(
					subscriptions.values(),
					subscriptionsCharges
				);
				DomusUtil.labelRunUpsertSObject(movimentazioniStock, subscriptions);
			}
			
			ProcessSubscriptionUtil.calcolaPrimaUltimaCopia(subscriptions.values());

		}catch(Exception e){
			DomusUtil.labelRunSetErrorOnSubscriptions('CalcolaGracingLatenzaSubscriptionBatch', e, subscriptions.values(), true);
		}finally{
			SubscriptionTriggerHandler.skipTrigger = true;
			update subscriptions.values();
			SubscriptionTriggerHandler.skipTrigger = false;
	
			for(Zuora__Subscription__c subscription : subscriptions.values()){
				if(subscription.Label_Run_processed__c && !subscription.Label_Run_IsError__c){
					subscriptionsProcessedIds.add(subscription.Id);
				}
			}

			Set<Id> accountIds = new Set<Id>();
			for(Zuora__Subscription__c subscription : subscriptions.values()){ accountIds.add(subscription.Zuora__Account__c); }
			ProcessSubscriptionUtil.aggiornaStatoClienteExCliente(accountIds);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		if(!Test.isRunningTest()) Database.executeBatch(new DownloadAmendmentBatch(subscriptionsProcessedIds));
	}
	
}