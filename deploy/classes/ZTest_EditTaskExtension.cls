@isTest
private class ZTest_EditTaskExtension
{
	@isTest
	static void testEditTask() 
	{
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		User u = [SELECT Id FROM User WHERE Alias = 'newUser2' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		Test.setCurrentPage(Page.EditTask);
		ApexPages.currentPage().getParameters().put('id', t.Id);
		EditTaskExtension controller = new EditTaskExtension(new ApexPages.StandardController(t));
		t.Status='Non Inziato';
	    update t;
		controller.saveTask();
		Test.stopTest();
	}


	@isTest
	static void testBack() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		User u = [SELECT Id FROM User WHERE Alias = 'newUser2' LIMIT 1];
		System.runAs(u){
			Test.setCurrentPage(Page.EditTask);
			ApexPages.currentPage().getParameters().put('id', t.Id);
			EditTaskExtension controller = new EditTaskExtension(new ApexPages.StandardController(t));
			controller.back();
		}
		Test.stopTest();
	}

	@isTest
	static void testRedirect() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		User u = [SELECT Id FROM User WHERE Alias = 'newUser' LIMIT 1];
		System.runAs(u){
			Test.setCurrentPage(Page.EditTask);
			ApexPages.currentPage().getParameters().put('id', t.Id);
			EditTaskExtension controller = new EditTaskExtension(new ApexPages.StandardController(t));
			controller.redirectPage();
		}
		Test.stopTest();
	}

	/*@isTest
	static void testInsufficient_Privileges() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		User u = [SELECT Id FROM User WHERE Alias = 'newUser' LIMIT 1];
		User u2 = [SELECT Id FROM User WHERE Alias = 'newUser2' LIMIT 1];
		Test.setCurrentPage(Page.EditTask);
		ApexPages.currentPage().getParameters().put('id', t.Id);
		EditTaskExtension controller = new EditTaskExtension(new ApexPages.StandardController(t));
		t.Status = 'Non Inziato';
		t.OwnerId = u.Id;
		update t;
		System.runAs(u2){
			controller.redirectPage();
		}
		Test.stopTest();
	}*/


	@isTest
	static void testRedirect_else() {
		Test.startTest();
		Account a = [SELECT Id FROM Account WHERE Name = 'Test Attachment Account' LIMIT 1];
		Task t = [SELECT Id, OwnerId FROM Task WHERE WhatId =: a.Id LIMIT 1];
		User u2 = [SELECT Id FROM User WHERE Alias = 'newUser2' LIMIT 1];
		Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Ufficio Marketing'];
		System.runAs(u2){
			Test.setCurrentPage(Page.EditTask);
			ApexPages.currentPage().getParameters().put('id', t.Id);
			EditTaskExtension controller = new EditTaskExtension(new ApexPages.StandardController(t));
			controller.redirectPage();
		}
		Test.stopTest();
	}



	@testSetup
	public static void testSetup() {
		Account a = new Account(Name='Test Attachment Account');
        insert a;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Professional'];
        Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Ufficio Marketing'];
		String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
		User u = new User(	Alias = 'newUser', Email='newuser@testorg.com',
							EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							LocaleSidKey='en_US', ProfileId = p.Id,
							TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName);
		insert u;

		String uniqueUserName2 = 'standarduser2' + DateTime.now().getTime() + '@testorg.com';
		User u2 = new User(	Alias = 'newUser2', Email='newuser@testorg.com',
							EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
							LocaleSidKey='en_US', ProfileId = p2.Id,
							TimeZoneSidKey='America/Los_Angeles', UserName=uniqueUserName2);
		insert u2;

		Task task = new Task(WhatId = a.Id, OwnerId = u.Id, Subject = 'Email', Priority = 'Normale', Status = 'In corso');
        insert task;

  
	}
}