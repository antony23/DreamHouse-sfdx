@IsTest
public class CheckVatCfControllerTest {
	
    @isTest
    public static void test(){
        Account a = new Account(
            NomeClienteB2C__c = 'Pippo',
            CognomeClienteB2C__c = 'Disney',
            Sesso__c = 'M',
            Data_di_nascita__c = date.newInstance(1934,1,1),
            Luogo_di_nascita__c = 'ABANO TERME',
        	Codice_Fiscale__c = '',
            Codice_Nazione__c = '',
            codice_nazione_iva__c = '',
            Partita_IVA__c = ''
        );


        final String RIGHT_FC = 'DSNPPP34A01Z404D';
        Test.setMock(HttpCalloutMock.class, new WSHttpCalloutMock(RIGHT_FC));
        
        CheckVatCfController contr = new CheckVatCfController();
        contr = new CheckVatCfController(new ApexPages.StandardController(a));

        Test.startTest();
        
        contr.a.RecordType = new RecordType(DeveloperName='B2C');
        testCF(contr);

        contr.a.RecordType = new RecordType(DeveloperName='B2BSmall');
        testCF(contr);
        testPIVA(contr);
        testPIVAAndCF(contr);

        contr.a.RecordType = new RecordType(DeveloperName='B2BLarge');
        testPIVA(contr);

        ApexPages.currentPage().getParameters().put('forceValid','Y');
        contr.checkVatCf();

        ApexPages.currentPage().getParameters().put('forceValid','N');
        contr.checkVatCf();

        contr.saveIgnore();
        contr.goBack();

        Test.stopTest();
    }

    private static void testPIVA(CheckVatCfController contr) {
        contr.a.Codice_Nazione__c = '';
        contr.a.codice_nazione_iva__c = '';
        contr.mc.Codice_Fiscale__c = '';
        contr.a.Partita_IVA__c = '';

        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);
        
        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = '';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);
        
        contr.a.codice_nazione_iva__c = '';
        contr.a.Partita_IVA__c = 'test';
        contr.checkVatCf();
        System.assertEquals(false, contr.isValid);
        
        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = 'test';
        contr.checkVatCf();
        System.assertEquals(false, contr.isValid);
    }

    private static void testCF(CheckVatCfController contr) {
        contr.a.Codice_Nazione__c = '';
        contr.a.codice_nazione_iva__c = '';
        contr.mc.Codice_Fiscale__c = '';
        contr.a.Partita_IVA__c = '';

        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);
        
        contr.a.Codice_Nazione__c = '';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D_';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.Codice_Nazione__c = 'IT';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D_';
        contr.checkVatCf();
        System.assertEquals(false, contr.isValid);
        
        contr.a.Codice_Nazione__c = 'IT';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);
    }

    private static void testPIVAAndCF(CheckVatCfController contr) {
        contr.a.Codice_Nazione__c = '';
        contr.a.codice_nazione_iva__c = '';
        contr.mc.Codice_Fiscale__c = '';
        contr.a.Partita_IVA__c = '';

        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = 'test';
        contr.a.Codice_Nazione__c = 'IT';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.codice_nazione_iva__c = '';
        contr.a.Partita_IVA__c = 'test';
        contr.a.Codice_Nazione__c = 'IT';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = '';
        contr.a.Codice_Nazione__c = 'IT';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = 'test';
        contr.a.Codice_Nazione__c = '';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D_';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);

        contr.a.codice_nazione_iva__c = 'IT';
        contr.a.Partita_IVA__c = 'test';
        contr.a.Codice_Nazione__c = '';
        contr.mc.Codice_Fiscale__c = 'DSNPPP34A01Z404D_';
        contr.checkVatCf();
        System.assertEquals(true, contr.isValid);
    }
    
    @isTest
    private static void test1() {
        CheckVatServiceTypes t = new CheckVatServiceTypes();
        CheckVatServiceTypes.checkVatResponse_element t1 = new CheckVatServiceTypes.checkVatResponse_element();
        CheckVatServiceTypes.checkVat_element t2 = new CheckVatServiceTypes.checkVat_element();
        CheckVatServiceTypes.checkVatApproxResponse_element t3 = new CheckVatServiceTypes.checkVatApproxResponse_element();
        CheckVatServiceTypes.checkVatApprox_element t4 = new CheckVatServiceTypes.checkVatApprox_element();
    }

    class WSHttpCalloutMock implements HttpCalloutMock {
        private String stdCF;

        public WSHttpCalloutMock(String stdCF) {
            this.stdCF = stdCF;
        }

        public HTTPResponse respond(HTTPRequest req) {
            final Map<String, String> params = fetchParams(req);

            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);

            System.debug('params: ' + params);

            if(params.get('fiscalcode').equals(stdCF)) {
                res.setBody('OK');
            } else if(params.get('fiscalcode').equals('SERVER_ERROR_MOCK')) {
                res.setBody('ERROR');
                res.setStatusCode(500);
            } else {
                res.setBody('KO');
            }

            return res;
        }

        private Map<String, String> fetchParams(HTTPRequest req) {
            final String paramStr = req.getEndpoint().split('\\?')[1];
            final String[] spParamStr = paramStr.split('[=&]+');

            System.debug('splitted: ' + spParamStr);

            final Map<String, String> paramMap = new Map<String, String>();

            for(Integer i = 0; i < 14; i += 2) {
                paramMap.put(EncodingUtil.urlEncode(spParamStr[i], 'UTF-8'), 
                                EncodingUtil.urlEncode(spParamStr[i + 1], 'UTF-8'));
            }

            return paramMap;
        }
    }
}