@IsTest
public class UtilCodiceFiscaleTest {
    
    public static testMethod void testCalcoloCf(){
		String cf = UtilCodiceFiscale.calculateCodiceFiscale('Disney','Pippo',date.newInstance(1934,1,1),'M','Z404');
		system.assertEquals('DSNPPP34A01Z404D',cf);
		cf = UtilCodiceFiscale.calculateCodiceFiscale('Disney','Minny',date.newInstance(1936,6,21),'F','D612');
		system.assertEquals('DSNMNY36H61D612F',cf);
		cf = UtilCodiceFiscale.calculateCodiceFiscale('Bar','Foo',date.newInstance(2004,2,29),'M','F704');
		system.assertEquals('BRAFOO04B29F704D',cf);
		cf = UtilCodiceFiscale.calculateCodiceFiscale('Bu','Ba',date.newInstance(2013,12,11),'F','Z107');
		system.assertEquals('BUXBAX13T51Z107E',cf);
    }

	public testMethod static void  testCodiceFiscaleControllo(){    
        System.assertEquals('T', UtilCodiceFiscale.calculateControllo('PPPDNY34A01Z404'));
    }
		
	public testMethod static void  testCodiceFiscaleData(){       
         System.assertEquals('78', UtilCodiceFiscale.getYear(date.newInstance(1978, 1, 1)));
         System.assertEquals('A', UtilCodiceFiscale.getMonth(date.newInstance(1978, 1, 1)));        
         System.assertEquals('01', UtilCodiceFiscale.getDay(date.newInstance(1978, 1, 1),'M'));
         System.assertEquals('41', UtilCodiceFiscale.getDay(date.newInstance(1978, 1, 1),'F'));
    }	
	
    public testMethod static void  testCodiceFiscaleCognome(){       
         System.assertEquals('MNT', UtilCodiceFiscale.calculateCognome('MOIANTO')); 
         System.assertEquals('MAI', UtilCodiceFiscale.calculateCognome('MAIA'));         
         System.assertEquals('MNA', UtilCodiceFiscale.calculateCognome('MANA'));
         System.assertEquals('DRS', UtilCodiceFiscale.calculateCognome('DE ROSSI'));
         System.assertEquals('DEX', UtilCodiceFiscale.calculateCognome('DE'));
    }

    
    public static testMethod void testOmocodia(){
        String cfCalc = 'DSNPPP34A01Z404D';

		// 4 -> Q senza modificare codice di controllo
        String cfNoCalc = 'DSNPPP34A01Z40QD'; 
        System.assertEquals(false, UtilCodiceFiscale.checkOmocodia(cfNoCalc,cfCalc));

        // 4 -> Q modificando codice di controllo
        cfNoCalc = 'DSNPPP34A01Z40Q';
        cfNoCalc += UtilCodiceFiscale.calculateControllo(cfNoCalc); 
        System.assertEquals(true, UtilCodiceFiscale.checkOmocodia(cfNoCalc,cfCalc));
    }

    public static testMethod void testInfoPersona(){
        UtilCodiceFiscale.Person p = UtilCodiceFiscale.getPersonInfo('TSTTST50B52I829N');
        System.assertEquals('F',p.sex);
        System.assertEquals(Date.newInstance(1950,2,12),p.d);
        
    }
    
    @isTest (SeeAllData = true)
    public static void testValidateCF() {
        String cf = 'PRCMRD10C15D003A';

        Test.setMock(HttpCalloutMock.class, new WSHttpCalloutMock(cf));

        final String name = 'MEDARDO';
        final String surname = 'PERCHIAZZI';
        final String sex = 'M';
        final Date birthdate = Date.newInstance(2010, 3, 15);
        final String birthplace = 'CORI';

        Test.startTest();

        System.assertEquals(true, UtilCodiceFiscale.validateCF(name, surname, sex, birthdate, birthplace, cf, true, true, 3));
        System.assertEquals(true, UtilCodiceFiscale.validateCF(name, surname, sex, birthdate, birthplace, cf));

        cf = 'PRCMRD10C15D003A_';
        System.assertEquals(false, UtilCodiceFiscale.validateCF(name, surname, sex, birthdate, birthplace, cf));

        Test.stopTest();
    }

    class WSHttpCalloutMock implements HttpCalloutMock {
        private String stdCF;

        public WSHttpCalloutMock(String stdCF) {
            this.stdCF = stdCF;
        }

        public HTTPResponse respond(HTTPRequest req) {
            final Map<String, String> params = fetchParams(req);

            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);

            System.debug('params: ' + params);

            if(params.get('fiscalcode').equals(stdCF)) {
                res.setBody('OK');
            } else if(params.get('fiscalcode').equals('SERVER_ERROR_MOCK')) {
                res.setBody('ERROR');
                res.setStatusCode(500);
            } else {
                res.setBody('KO');
            }

            return res;
        }

        private Map<String, String> fetchParams(HTTPRequest req) {
            final String paramStr = req.getEndpoint().split('\\?')[1];
            final String[] spParamStr = paramStr.split('[=&]+');

            System.debug('splitted: ' + spParamStr);

            final Map<String, String> paramMap = new Map<String, String>();

            for(Integer i = 0; i < 14; i += 2) {
                paramMap.put(EncodingUtil.urlEncode(spParamStr[i], 'UTF-8'), 
                                EncodingUtil.urlEncode(spParamStr[i + 1], 'UTF-8'));
            }

            return paramMap;
        }
    }

}