@isTest
public class ZTest_Utils {

	public static Product2 createProduct(String testata){ 
		Product2 product= new Product2();
 		product.Name='Test product';
 		product.zqu__SKU2__c='AB00000005-';
 		product.zqu__ZuoraId__c='2c92c0f95e097cf3015e0a7455401e5b';
        product.Testata__c = testata;
 		return product;
	}

	public static zqu__ProductRatePlan__c createzquProductRatePlan(Id productId, String zuoraID, String productType){
		zqu__ProductRatePlan__c rp = new zqu__ProductRatePlan__c();
		rp.zqu__ZuoraId__c=zuoraID;
		rp.TipoProdotto__c=productType;
		rp.zqu__Product__c=productId;
        rp.ConAvvisi__c = 'Si';
        rp.SkipSpesediSpedizione__c = 'NO';
		return rp;
	}
    
    public static zqu__ProductRatePlanCharge__c  createzquProductRatePlanCharge(Id ratePlanId, String tipoProdotto){
        zqu__ProductRatePlanCharge__c  rpc= new zqu__ProductRatePlanCharge__c ();
        rpc.zqu__ProductRatePlan__c=ratePlanId;
        rpc.IssueCalendar__c='QUATTRORUOTE';
        rpc.TipoProdotto__c= tipoProdotto;
        rpc.Uscite__c='12';
        rpc.Testata__c = 'QUATTRORUOTE';
        return rpc;
    }
    
    public static Zuora__SubscriptionProductCharge__c createzuoraSubscriptionProductCharge(Id subscriptionRenId) {
        Zuora__SubscriptionProductCharge__c spc = new Zuora__SubscriptionProductCharge__c();
        spc.Zuora__Subscription__c = subscriptionRenId;
        spc.Supporto__c = 'Carta';

        return spc;
    }

    public static Testata__c  createTestata(String nameTestata){
        Testata__c  testata= new Testata__c();
        testata.Name=nameTestata;
        testata.IssueCalendar__c=nameTestata;
        
        return testata;
    }
    
    public static List<Calendario__c> createCalendar(Id testataId){
        List<Calendario__c> calendars= new List<Calendario__c>();
        
        for(Integer i=0;i<12;i++){
        	Calendario__c calendar=new Calendario__c();
         	calendar.testata__c=testataId;
        	calendar.Data_Uscita__c=Date.today()+i;
            calendars.add(calendar);
        }
        return calendars;
    }

	public static Zuora__Subscription__c createSubscriptionRen(String name, Zuora__CustomerAccount__c billingAcc, String zuoraStatus, String confirmOffer){

		Zuora__Subscription__c sub= new Zuora__Subscription__c();
		sub.Name=name;
		sub.Zuora__CustomerAccount__c=billingAcc.id;
        sub.Zuora__Account__c=billingAcc.Zuora__Account__c;
		sub.Rinnovata__c=true;
        sub.Zuora__Status__c=zuoraStatus;
        sub.Mantieni_Offerta__c=confirmOffer;

		return sub;
	}

	public static Avviso_di_Rinnovo__c createAvvisoDiRinnovo(Id subscriptionRenId, Id productrateplan, String italiaEstero){

		Avviso_di_rinnovo__c avvisoDiRinnovoSub= new Avviso_di_Rinnovo__c();
		avvisoDiRinnovoSub.Abbonamento__c=subscriptionRenId;
        avvisoDiRinnovoSub.Italia_Estero__c = italiaEstero;
        avvisoDiRinnovoSub.Offerta_di_Rinnovo__c = productrateplan;

		return avvisoDiRinnovoSub;
	}
      
    public static Account createAccount(){
        Account acc= new Account();
        acc.Name='Test';
        acc.Tipo_Cliente__c = 'B2C';
        return acc;
    }

    public static Contact createContact(String account, String country){
        Contact cnt= new Contact();
        cnt.LastName='Test';
        cnt.Codice_Nazione__c=country;
        cnt.AccountId = account;
        cnt.Cee_Nazione__c = 'CEE';

        return cnt;
    }
    
    public static Zuora__CustomerAccount__c createBillingAccount(Id billTo, Id soldTo, Id zuoraAccountId){
        Zuora__CustomerAccount__c billingAccount = new Zuora__CustomerAccount__c();
        billingAccount.Bill_To_Salesforce__c=billTo;
        billingAccount.Sold_To_Salesforce__c=soldTo;
        billingAccount.Zuora__Account__c=zuoraAccountId;
        return billingAccount;     
    }

    public static Mapping_Documento_Emesso__c createMappingDocumentoEmesso(String tipologiaCliente, String supporto, String consegna, String fatturazione) {
        Mapping_Documento_Emesso__c mde = new Mapping_Documento_Emesso__c();
        mde.Tipologia_Cliente__c = tipologiaCliente; //'B2C';
        mde.Supporto__c = supporto;//  'Carta';
        mde.Consegna__c = consegna;//'Italia';
        mde.Fatturazione__c = fatturazione;// 'Italia';

        return mde; 
    }

    public static Zuora__SubscriptionRatePlan__c createZuoraSubscriptionRatePlan(zqu__ProductRatePlan__c productRatePlan, Id subscriptionID) {

        Zuora__SubscriptionRatePlan__c subscriptionRatePlan = new Zuora__SubscriptionRatePlan__c();
        subscriptionRatePlan.Original_Product_RatePlan__c = productRatePlan.Id;
        subscriptionRatePlan.Zuora__Subscription__c = subscriptionID;

        return subscriptionRatePlan;

    }

}