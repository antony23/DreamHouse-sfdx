global class ModificaAccountBatch implements Database.Batchable<sObject>,
                                            Database.StateFul,
                                            Database.AllowsCallouts,
                                            Schedulable {
    
    global void execute(SchedulableContext sc) {
        Database.executebatch(new ModificaAccountBatch ());
    }

    String query;
    
    global ModificaAccountBatch () {
        query='SELECT Id, Data_Fine_Ultima_Subscription__c '+
                'FROM Account '+
                'WHERE Stato__c = \'Cliente\' '+
                'AND Data_Fine_Ultima_Subscription__c < TODAY';
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Account> accountList = (List<Account>) scope;
        for(Account a : accountList){
            a.Stato__c = 'Ex Cliente';
        }
        update accountList;
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

}