public class ProdottoVenditaDirettaHandler {

	public static Boolean skipTrigger = false;

	public void onBeforeInsert(List<Prodotto_Vendita_Diretta__c> pvdList){
		
	}

	public void onAfterInsert(List<Prodotto_Vendita_Diretta__c> pvdList){
		if(skipTrigger == false){
			handleSKUDisabled(pvdList);
		}
	}

	public void onBeforeUpdate(List<Prodotto_Vendita_Diretta__c> pvdList){

	}

	public void onAfterUpdate(List<Prodotto_Vendita_Diretta__c> pvdList){
		if(skipTrigger == false){
			handleSKUDisabled(pvdList);
		}
	}

	public void handleSKUDisabled(List<Prodotto_Vendita_Diretta__c> pList){

		Boolean goOn = (Blocco_Selettivo_MS__c.getValues(UserInfo.getUserId()) != null) ? Blocco_Selettivo_MS__c.getValues(UserInfo.getUserId()).Blocco_Attivo__c :
                (Blocco_Selettivo_MS__c.getValues(UserInfo.getProfileId()) != null) ? Blocco_Selettivo_MS__c.getValues(UserInfo.getProfileId()).Blocco_Attivo__c :
                Blocco_Selettivo_MS__c.getOrgDefaults().Blocco_Attivo__c;

        if(goOn){

			Set<Id> idsSet = new Set<Id>();
			for(Prodotto_Vendita_Diretta__c pvd : pList){
				idsSet.add(pvd.Id);
			}
			List<AggregateResult> movStockList = [SELECT Subscription__c FROM Movimentazione_Stock__c WHERE Stato__c = 'Da Trasmettere' AND Subscription__c != null AND Prodotto_Vendita_Diretta__c IN :idsSet GROUP BY Subscription__c];
			/*List<Zuora__Subscription__c> subList = new List<Zuora__Subscription__c>();
			for(AggregateResult agg : movStockList){
				Id tempId = (Id)agg.get('Subscription__c');
				Zuora__Subscription__c sub = new Zuora__Subscription__c(Id = tempId,PVD_Disabilitati__c = '');
				subList.add(sub);
			}

			update subList;*/

			List<Zuora__Subscription__c> subsList = new List<Zuora__Subscription__c>();
			for(AggregateResult agg : movStockList){
				subsList.add(new Zuora__Subscription__c(Id = (Id)agg.get('Subscription__c')));
			}

			Database.executeBatch(new BatchUnlockMovStock(subsList),1);
			
			/*List<Disabled_MS_Product_SKU__c> newRecordCS = new List<Disabled_MS_Product_SKU__c>();
			List<Disabled_MS_Product_SKU__c> delRecordCS = new List<Disabled_MS_Product_SKU__c>();
			Set<Id> idsSetToCreate = new Set<Id>();
			Set<Id> idsSetToDelete = new Set<Id>();

			for(Prodotto_Vendita_Diretta__c pvd : pList){
				if(pvd.MovStock_Abilitata__c == false){
					idsSetToCreate.add(pvd.Id);
				}else{
					idsSetToDelete.add(pvd.Id);
				}
			}

			for(Id tempId : idsSetToCreate){
				Disabled_MS_Product_SKU__c tempRecord = new Disabled_MS_Product_SKU__c();
				tempRecord.Name = tempId;
				newRecordCS.add(tempRecord);
			}

			for(Id tempId : idsSetToDelete){
				if(Disabled_MS_Product_SKU__c.getInstance(tempId) != null){
					delRecordCS.add(Disabled_MS_Product_SKU__c.getInstance(tempId));
				}
			}

			if(delRecordCS != null && !delRecordCS.isEmpty()){
				delete delRecordCS;
				// TO DO Aggiornamento subscriptions
			}

			if(newRecordCS != null && !newRecordCS.isEmpty()){
				insert newRecordCS;
				// TO DO Aggiornamento subscriptions
			}*/
		}

	}

}