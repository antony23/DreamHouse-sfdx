public class ExtendSubscriptionController {
  private Id subId;
  public Boolean hasError {get;set;}
    public ExtendSubscriptionController (ApexPages.StandardController controller){
    }

    public PageReference createQuoteAction(){
    subId = ApexPages.currentPage().getParameters().get('id');
    System.debug('subId: ' + subId);
    if(subId != null){
      Id quoteId = createQuote(subId);
      if (quoteId == null){
        hasError = true;  
        return null;
      }  
      PageReference ref = Page.WizardCart;
      ref.getParameters().put('id', quoteId);
      return ref;
    }
    return null;
  }


  private Id createQuote(Id subId){
    
        hasError = false;
    if(String.isNotBlank(subId)){
      System.debug('subId: ' +subId);
            List<Zuora__SubscriptionRatePlan__c> subRatePlanList = [SELECT Id, Original_Product_RatePlan__c FROM Zuora__SubscriptionRatePlan__c
                                                                     WHERE Zuora__Subscription__c = :subId AND
                                                                     (Original_Product_RatePlan__r.TipoProdotto__c = 'Abbonamento' OR
                                                                     Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle Abbonamenti' OR
                                                                     Original_Product_RatePlan__r.TipoProdotto__c = 'Bundle Misti')];

            String whereClause = 'Id = :subId ';
            String additionalFields = 'Zuora__CustomerAccount__r.Zuora__Zuora_Id__c';
            String queryString = Utils.getSelectAllQuery('Zuora__Subscription__c', whereClause, null, additionalFields, false);
            List<Zuora__Subscription__c> subList = (List<Zuora__Subscription__c>) Database.query(queryString);

            System.debug('subRatePlanList: ' + subRatePlanList);
            System.debug('subList: ' + subList);
            if(subRatePlanList != null && subRatePlanList.size() >0 && subList != null && subList.size()>0) {
                Id ratePlanId = subRatePlanList.get(0).Original_Product_RatePlan__c;
                if(ratePlanId != null){

                    Zuora__Subscription__c subscription = subList.get(0);

                    if(subscription.Zuora__Status__c != 'Cancelled'){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: l\'abbonamento si trova nello stato \''+subscription.Zuora__Status__c+'\' e non è rinnovabile'));
                        hasError = true;
                        return null;
                    }

                    zqu.quote quote = zqu.Quote.getNewInstance();
                    zqu__Quote__c quoteObj = quote.getSObject();
                    
                    quoteObj.zqu__Account__c = subscription.Zuora__Account__c;
                    //account = (Account) Database.query(Utils.getSelectAllQuery('Account', 'Id = \''+quoteObj.zqu__Account__c+'\'', null, null, true));

                    quoteObj.zqu__StartDate__c = subscription.Zuora__SubscriptionEndDate__c;
                    quoteObj.Canale_di_acquisizione__c = subscription.Canale_di_acquisizione__c;
                    quoteObj.Estensione__c = 'SI';
                    quoteObj.zqu__ValidUntil__c  = subscription.Zuora__SubscriptionEndDate__c;
                    quoteObj.zqu__InitialTerm__c = 1;
                    quoteObj.zqu__RenewalTerm__c = 1;
                    quoteObj.zqu__InitialTermPeriodType__c = 'Week';
                    quoteObj.zqu__RenewalTermPeriodType__c = 'Week';
                    quoteObj.zqu__AutoRenew__c = false;

                    if(subscription.Numero_di_Cicli__c  != null){
                        quoteObj.Numero_di_Cicli_Text__c = String.valueOf(subscription.Numero_di_Cicli__c);
                    }
                    quote.save();
                    List<Testata__c> testataList = DomusUtil.getTestataList(ratePlanId);
                    Set<Id> testataIdSet = new Set<Id>();
                    for(Testata__c t : testataList){
                        testataIdSet.add(t.id);
                    }
                    List<Id> testataIdList = new List<Id>(testataIdSet);
                    Calendario__c startCalendar = DomusUtil.getCalendarioStartCopy(testataIdList, subscription.Zuora__SubscriptionEndDate__c);

                    List<zqu__ProductRatePlanCharge__c> chargeList = DomusUtil.getChargeList(ratePlanId);
                    Date startDate = startCalendar.Data_Uscita__c;
                    Date endDate = startCalendar.Data_Uscita__c;
                    //Date startDate = Date.today();
                    //Date endDate = Date.today();

                    Map<String, Testata__c> testataMap = new Map<String, Testata__c>();
                    for(Testata__c t : testataList){
                        testataMap.put(t.IssueCalendar__c, t);
                    }

                    Set<String> supportSet = new Set<String>();
                    for(zqu__ProductRatePlanCharge__c charge : chargeList){
                        if(charge.TipoProdotto__c == 'Abbonamento'){
                            Testata__c testataTmp = testataMap.get(charge.IssueCalendar__c);
                            Calendario__c startCalendarTmp = DomusUtil.getCalendarioStartCopy(testataTmp.Id, startDate);
                            Calendario__c endCalendarTmp = DomusUtil.getCalendarioEndCopyByTestata(testataTmp.Id, startDate, Integer.valueOf(charge.Uscite__c));
                            if(endCalendarTmp == null){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: Ultima copia d\'uscita non trovata. Inserire ulteriori copie di uscita per la testata "'+charge.IssueCalendar__c+'"'));
                                hasError = true;
                                return null;
                            }
                            if(endCalendarTmp.Data_Uscita__c > endDate){
                                endDate = endCalendarTmp.Data_Uscita__c;
                            }
                            if(String.isBlank(quoteObj.Testate__c)){
                                quoteObj.Testate__c = charge.Testata__c;
                                quoteObj.Dettaglio_Copie_per_Testata__c = startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                            }else{
                                quoteObj.Testate__c += ';'+charge.Testata__c;
                                quoteObj.Dettaglio_Copie_per_Testata__c += ';'+startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                            }
                            if(String.isNotBlank(charge.Supporto__c)){
                                supportSet.add(charge.Supporto__c);
                            }
                        }
                    }
                    quoteObj.Supporto__c = String.join(new List<String>(supportSet), ';');
                    quoteObj.zqu__AutoRenew__c = false;
                    quoteObj.Subscription_Precedente__c = subscription.Id;
                    quoteObj.Quote_Status__c = 'Draft';
                    quoteObj.Metodo_di_Pagamento__c = subscription.Metodo_di_Pagamento__c;
                    quoteObj.zqu__InvoiceOwnerId__c = subscription.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c;
                    DomusUtil.setQuoteDateParameters(startDate, endDate, quoteObj);
                    //setQuoteValuesFromSub(quoteObj, subscription);


                    quote.save();

                    String tipoProdotto;
                    List<zqu__ProductRatePlan__c> ratePlanList = [SELECT Id, TipoProdotto__c FROM zqu__ProductRatePlan__c WHERE Id = :ratePlanId];
                    for(zqu__ProductRatePlan__c rp : ratePlanList){
                        if(rp.TipoProdotto__c == 'Abbonamento'){
                            tipoProdotto = rp.TipoProdotto__c;
                        }else if(rp.TipoProdotto__c == 'Bundle Misti' || rp.TipoProdotto__c == 'Bundle Abbonamenti'){
                            tipoProdotto = rp.TipoProdotto__c;
                        }
                    }

                    Id quoteRatePlanId;

                    if(tipoProdotto == 'Abbonamento'){
                        quoteRatePlanId = DomusUtil.setAbbonamento(quote.getId(), ratePlanId, startCalendar.Id);
                    } else if (tipoProdotto.containsIgnoreCase('Bundle')){
                        quoteRatePlanId = DomusUtil.setBundle(quote.getId(), ratePlanId, startCalendar.Id, tipoProdotto);
                    }

                    if(quoteRatePlanId != null){
                        List<zqu__QuoteRatePlanCharge__c> qChargeList = DomusUtil.getQuoteChargeList(quoteRatePlanId);
                        if(qChargeList != null && qChargeList.size()>0){
                            for(zqu__QuoteRatePlanCharge__c qCharge : qChargeList){
                                System.debug('Charge ' + qCharge.name + ', tipo: ' + qCharge.Tipo_Charge__c);
                                if(qCharge.Tipo_Charge__c == 'Abbonamento'){
                                    qCharge.zqu__Quantity__c = 1;
                                    qCharge.zqu__Total__c = qCharge.zqu__Quantity__c * qCharge.zqu__EffectivePrice__c;
                                    qCharge.Supporto__c = qCharge.zqu__ProductRatePlanCharge__r.Supporto__c;
                                }
                                if(qCharge.Tipo_Charge__c == 'Vendita Diretta'){
                                    qCharge.Supporto__c = qCharge.Prodotto_Vendita_Diretta__r.Supporto__c;
                                }
                               
                            }
                            update qChargeList;
                        }
                    }
                    DomusUtil.setQuoteChargeDateParameters(startDate, endDate, quoteObj.Id); 
                    subscription.ModificaERiattiva__c = true;
                    SubscriptionTriggerHandler.skipTrigger = true;
                    update subscription;
                    SubscriptionTriggerHandler.skipTrigger = false;
                    return quote.getId();
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: nessun rateplan trovato'));
                    hasError = true;
                    return null;
                }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attenzione: nessuna subscription trovata'));
                hasError = true;    
                return null;
            }

        }
        return null;
  }


  public PageReference goToSubscription(){
    PageReference ref = new PageReference('/'+subId);
    return ref;
  }

  public void appendErrorMessage(Exception e) {
      System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
      appendErrorMessage(e.getMessage());
  }

  public void appendErrorMessage(String messageError) {
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
  }
}