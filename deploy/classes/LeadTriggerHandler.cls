public class LeadTriggerHandler {
	
	public static Boolean skipTrigger = false;

	public void onBeforeInsert(List<Lead> triggerNew){
		
		System.debug(LoggingLevel.INFO, '*_* LeadTriggerHandler.onBeforeInsert()');
		
		setCommerciale(triggerNew);

	}

	public void onBeforeUpdate(List<Lead> triggerNew){}

	public void onAfterInsert(Map<Id, Lead> triggerOldMap, Map<Id, Lead> triggerNewMap){}

	public void onAfterUpdate(Map<Id, Lead> triggerOldMap, Map<Id, Lead> triggerNewMap){}

	private static Map<String, User> getAgentiCommerciali(){

        Map<String, User> usersMap = new Map<String, User>();
        List<User> usersList = [SELECT Id, EmployeeNumber FROM User WHERE EmployeeNumber != null];
        
        for(User u : usersList){
            if(!usersMap.containsKey(u.EmployeeNumber)){
                usersMap.put(u.EmployeeNumber, u);
            }
        }

        return usersMap;
    }

    private static void setCommerciale(List<Lead> triggerNew){

        Map<String, User> usersMap = getAgentiCommerciali();

       	for(Lead newLead : triggerNew){
       		if(newLead.Codice_Agente__c != null){
       			if(usersMap.containsKey(newLead.Codice_Agente__c)){
            		newLead.Commerciale__c = usersMap.get(newLead.Codice_Agente__c).Id;
            	}	
       		}
		}
    }

}