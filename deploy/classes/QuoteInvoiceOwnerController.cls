public class QuoteInvoiceOwnerController{
	public zqu.Quote quote {get;set{quote = value; initInvoiceOwner();}}
	public zqu__Quote__c quoteObj {get;set;}
	public Contact billToContact {get;set;}
	public Contact soldToContact {get;set;}
	public Zuora__CustomerAccount__c custAcc {get;set;}
	public Boolean displayBillingAccount {get;set{displayBillingAccount = value;}}
	public String title {get;set;}
	private Boolean doInit = true;


	public QuoteInvoiceOwnerController(){

	}

	private void initInvoiceOwner(){
		try {
            if(doInit){
            	quoteObj = quote.getSObject();
            	List<Id> contactIdList = new List<Id>(); 
		        Id soldToId;
		        Id billToId;

		        title = 'Titolare documento contabile';

		        String existingBillingAccountId = quoteObj.zqu__InvoiceOwnerId__c;
		        if(quoteObj.zqu__InvoiceOwnerId__c == null && displayBillingAccount){
		        	existingBillingAccountId = quoteObj.zqu__ZuoraAccountID__c;
		        	title = 'Billing Accoung';
		        }
            	List<Zuora__CustomerAccount__c> custAccList = [SELECT Id, Bill_To_Salesforce__c, Sold_To_Salesforce__c, 
            													Zuora__Balance__c, Metodo_di_pagamento__c, Zuora__PaymentTerm__c,
            													Zuora__Currency__c, Zuora__Credit_Balance__c
            													FROM Zuora__CustomerAccount__c WHERE Zuora__Zuora_Id__c = :existingBillingAccountId];
            	if(custAccList != null && custAccList.size() > 0){
	                custAcc = custAccList.get(0);
	                if(custAcc != null){
	                    contactIdList.add(custAcc.Bill_To_Salesforce__c);
	                    contactIdList.add(custAcc.Sold_To_Salesforce__c);
	                    soldToId = custAcc.Sold_To_Salesforce__c;
	                    billToId = custAcc.Bill_To_Salesforce__c;

	                    Map<Id,Contact> c = new Map<Id,Contact>([SELECT Id, Name, AccountId, Account.Partita_IVA__c, Account.Name, Codice_Nazione__c,Cee_Nazione__c,
                                                Codice_Fiscale__c, Codice_SAP__c, Account.Codice_Cliente_BillTo_Contact__c,zqu__County__c, 
                                                MailingState, Id_Area_Nazione__c, Area_Nazione__c, Presso__c, MailingStreet, MailingCity, MailingPostalCode, MailingCountry 
                                                FROM Contact WHERE Id = :contactIdList]);
				        billToContact = c.get(billToId);
				        soldToContact = c.get(soldToId);
	                }
            	}
	        	doInit = false;
	        }
        } catch(Exception e) {
			appendErrorMessage(e);
		}
	}





	private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }

}