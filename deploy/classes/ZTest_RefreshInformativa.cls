@isTest
private class ZTest_RefreshInformativa {

	@isTest
	static void testAction() {

		Contact c = [SELECT Id FROM Contact WHERE Name = 'Contact Test' LIMIT 1];
		Test.startTest();
		Test.setCurrentPage(Page.RefreshInformativa);
		ApexPages.currentPage().getParameters().put('id', c.Id);
		RefreshInformativa controller = new RefreshInformativa(new ApexPages.StandardController(c));
		controller.refreshInformativaContact();
		Test.stopTest();

	}

	@testSetup
	public static void testSetup(){
		Account a = new Account(Name = 'Account Test', RecordTypeId = '0120O000000oofrQAA');
        insert a;

        Contact c = new Contact(AccountId = a.Id, FirstName = 'Contact', LastName = 'Test', Email = 'email@test.it', Fonte__c = 'Email', Cee_Nazione__c = 'CEE');
        insert c;

        Informativa_Privacy__c ip = new Informativa_Privacy__c(Name = 'Informativa Corrente', URL_Informativa__c = 'Test URL', Versione_Informativa__c = '1');
        insert ip;
	}
}