global class ProductRatePlanChargeDelete {
	
	public ProductRatePlanChargeDelete() { }
	
	@RemoteAction
	webservice static void DeleteRatePlan() {
		
		List<zqu__ProductRatePlanCharge__c>listToDelete = [SELECT id,Name FROM zqu__ProductRatePlanCharge__c WHERE zqu__Deleted__c = true ];

		delete listToDelete;


	}
}