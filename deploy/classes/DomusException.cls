public virtual class DomusException extends Exception {

	/** Thrown if a call attempt has occurred on a method that cannot be executed when the object is in a certain status. **/
	public virtual class UnsupportedOperationException extends DomusException {}

	/** Thrown if an usage attempt has occurred on an object that has been terminated. **/
	public virtual class ResourceReleasedException extends UnsupportedOperationException {}

	/** Thrown if a security constraint has been violated. **/
	public virtual class SecurityException extends DomusException {}

	/** Thrown if a variable cannot be null and actually it is. **/
	public virtual class NullPointerException extends DomusException {}

	/** Thrown if a collection has no elements. **/
	public virtual class EmptyCollectionException extends DomusException {}

	/** Thrown if an index is out of the collection bounds. **/
	public virtual class IndexOutOfBoundsException extends DomusException {}

	/** Thrown if an object or a process has reached an invalid status. **/
	public virtual class IllegalStateException extends DomusException {}

	/** Thrown if an argument has an invalid value. **/
	public virtual class IllegalArgumentException extends DomusException {}

	/** Thrown if a textual argument has an invalid format. **/
	public virtual class IllegalFormatException extends DomusException {}

	/** Thrown inside a constructor if something goes wrong. **/
	public virtual class InstantiationException extends DomusException {}

	/** Thrown if an unexistent field has been referenced. **/
	public virtual class NoSuchFieldException extends DomusException {}

	/** Thrown if an unexistent SObject has been referenced. **/
	public virtual class NoSuchSObjectException extends DomusException {}

	/** Thrown if a field does not support one or more SObject types. **/
	public virtual class NoSuchSObjectTypeException extends DomusException {}

	/** Thrown if no element has been found after a lookup. **/
	public virtual class ElementNotFoundException extends DomusException {}

	/** Thrown if no record has been found after a query or a lookup. **/
	public virtual class RecordNotFoundException extends DomusException {}

}