public class AccountTriggerHandler{

    public static Boolean skip = false;

    public static final Id recordTypeB2C = Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId();

    public static void insertMainContact(List<Account> triggerNew){
        System.debug('********** recordtype' + triggerNew[0].RecordTypeId + '***********' + recordTypeB2C);
        System.debug('********** newAccounts' +triggerNew);
        Contact[] newContacts = new Contact[]{};
        for(Account a :triggerNew){
            if(a.RecordTypeId != recordTypeB2C && !a.Creato_da_Lead__c){
                newContacts.add(
                    new Contact(
                        //*** CR Rimozione Trattino ***//
                        //FirstName = '-',
                        AccountId = a.Id,
                        LastName = a.Name,
                        MainContact__c = true,
                        Email = a.Email__c
                    )
                );
            }
        }

        insert newContacts;
        System.debug('********************new Contacts*********************' + newContacts);
    }
//public static void checkNameB2C(Account[] newAccounts,Map<Id,Account> oldMap){
  //  if(!ContactTriggerHandler.fromContactTrigger){
    //  for(Account a : newAccounts){
      //  if(a.RecordTypeId == recordTypeB2C 
        //  && 
          //a.Name != oldMap.get(a.Id).Name
        //){
         // a.Name.addError('Non è possibile modifcare il nome di un cliente B2C, modificare il referente principale');
        //}
      //}
    //}
  //}
}