public class AmendmentUtil {

    public static List<String> retrieveAmendments(Zuora__Subscription__c subscription) {
        final Map<Id, List<String>> amendments = retrieveAmendments(new List<Zuora__Subscription__c>{ subscription });
        return amendments.containsKey(subscription.Id) ? amendments.get(subscription.Id) : new List<String>();
    }

    public static Map<Id, List<String>> retrieveAmendments(List<Zuora__Subscription__c> subscriptions) {
        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        final Map<Object, SObject> subByOriginalId = Tools.mapBy(subscriptions, 'Zuora__OriginalId__c');
        final Set<String> subOriginalIdSet = Tools.extractStringSet(subscriptions, 'Zuora__OriginalId__c');

        String fetchAllRelatedSubscriptionsZOQL = 'SELECT Id, OriginalId, Version FROM Subscription WHERE OriginalId = \'' + Tools.join(subOriginalIdSet, '\' OR OriginalId = \'') + '\'' ;
        List<Zuora.zObject> relatedSubscriptions = null;
        if(!Test.isRunningTest()) {
            relatedSubscriptions = zApiInstance.zquery(fetchAllRelatedSubscriptionsZOQL);
        } else{
            relatedSubscriptions = new List<Zuora.zObject>();
            Zuora.zObject obj = new Zuora.zObject('Subscription');
            obj.setValue('Id','Id');
            obj.setValue('OriginalId','OriginalId');
            relatedSubscriptions.add(obj);
        }

        final Map<String, List<Zuora.zObject>> relSubByOriginalId = new Map<String, List<Zuora.zObject>>();
        for(Zuora.zObject relatedSubscription : relatedSubscriptions){
            if(!relSubByOriginalId.containsKey((String) relatedSubscription.getValue('OriginalId'))) {
                relSubByOriginalId.put((String) relatedSubscription.getValue('OriginalId'), new List<Zuora.zObject>());
            }

            relSubByOriginalId.get((String) relatedSubscription.getValue('OriginalId')).add(relatedSubscription);
        }

        final Set<String> relSubZIDSet = new Set<String>();
        for(Zuora.zObject relSub : relatedSubscriptions) {
            relSubZIDSet.add((String) relSub.getValue('Id'));
        }

        String fetchAllAmendmentsZOQL = 'SELECT Id, SubscriptionId, Name, Code FROM Amendment ' + 
                                        'WHERE SubscriptionId = \'' + Tools.join(relSubZIDSet, '\' OR SubscriptionId = \'') + '\'' ;

        List<Zuora.zObject> zuoraAmendments = null;
        if(!Test.isRunningTest()) {
            zuoraAmendments = zApiInstance.zquery(fetchAllAmendmentsZOQL);
        } else{
            Zuora.zObject obj1 = new Zuora.zObject('Amendment');
            obj1.setValue('SubscriptionId','SubscriptionId');
            obj1.setValue('CreatedDate', DateTime.now());
            obj1.setValue('Type', 'SuspendSubscription');

            Zuora.zObject obj2 = new Zuora.zObject('Amendment');
            obj2.setValue('SubscriptionId','SubscriptionId');
            obj2.setValue('CreatedDate', DateTime.now());
            obj2.setValue('Type', 'ResumeSubscription');

            Zuora.zObject obj3 = new Zuora.zObject('Amendment');
            obj3.setValue('SubscriptionId','SubscriptionId');
            obj3.setValue('CreatedDate', DateTime.now());
            obj3.setValue('Type', 'TermsAndConditions');

            zuoraAmendments = new List<Zuora.zObject>();
            zuoraAmendments.add(obj1);
            zuoraAmendments.add(obj2);
            zuoraAmendments.add(obj3);
        }

        final Map<String, String> amendZIDBySubscriptionZID = new Map<String, String>();
        for(Zuora.zObject zAmend : zuoraAmendments) {
            amendZIDBySubscriptionZID.put((String) zAmend.getValue('SubscriptionId'), (String) zAmend.getValue('Id'));
        }

        final Map<Id, List<String>> output = new Map<Id, List<String>>();
        for(String originalId : relSubByOriginalId.keySet()) {
            final Zuora__Subscription__c subscription = (Zuora__Subscription__c) subByOriginalId.get(originalId);
            if(subscription == null) continue;

            if(!output.containsKey(subscription.Id)) {
                output.put(subscription.Id, new List<String>());
            }

            final List<Zuora.zObject> relSubList = relSubByOriginalId.get(originalId);
            if(relSubList == null) continue;

            for(Zuora.zObject relSub : sortSubscriptionsByVersion(relSubList)) {
                final String zAmendId = amendZIDBySubscriptionZID.get((String) relSub.getValue('Id'));
                if(zAmendId == null) continue;

                output.get(subscription.Id).add(zAmendId);
            }
        }

        return output;
    }

    public static Zuora.zApi.AmendResult suspend(Zuora__Subscription__c subscription, Date suspendDate, Date resumeDate, Boolean extendTerms, String suspensionType) {
        return suspend(new List<SuspensionInfo>{ new SuspensionInfo(subscription, suspendDate, resumeDate, extendTerms, suspensionType) });
    }

    public static Zuora.zApi.AmendResult suspend(List<SuspensionInfo> sInfoList) {
        final List<Zuora.zObject> amendments = new List<Zuora.zObject>();

        for(SuspensionInfo sInfo : sInfoList) {
            Zuora.zObject amendment = null;

            if(sInfo.extendTerms) {
                amendment = createTermsAndConditionsAmendment(sInfo.subscription, sInfo.suspendDate, sInfo.resumeDate);
                amendment.setValue('Tiposospensione__c', sInfo.suspensionType);
                amendments.add(amendment);
            }

            amendment = createSuspendSubscriptionAmendment(sInfo.subscription, sInfo.suspendDate);
            amendment.setValue('Tiposospensione__c', sInfo.suspensionType);
            amendments.add(amendment);

            amendment = createResumeSubscriptionAmendment(sInfo.subscription, sInfo.suspendDate, sInfo.resumeDate);
            amendment.setValue('Tiposospensione__c', sInfo.suspensionType);
            amendments.add(amendment);
        }

        Zuora.zApi.AmendResult result = zamend(amendments);
        return result;
    }

    public static Zuora.zApi.AmendResult zamend(List<Zuora.zObject> amendments) {
        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        Zuora.zApi.AmendRequest amendRequest = new Zuora.zApi.AmendRequest();
        amendRequest.amendments = amendments;

        Zuora.zApi.AmendOptions amendmentOption = new Zuora.zApi.AmendOptions();
        amendmentOption.GenerateInvoice = false;
        amendmentOption.ProcessPayments = false;

        amendRequest.amendOptions = amendmentOption;

        Zuora.zApi.AmendRequest[] amendRequests = new Zuora.zApi.AmendRequest[]{ amendRequest };
        Zuora.zApi.AmendResult result = (!Test.isRunningTest()) ? zApiInstance.zamend(amendRequests): new Zuora.zApi.AmendResult();
        System.debug(LoggingLevel.ERROR, '[AmendmentUtil][zamend] ' + generateErrorMessage(result));
        return result;
    }

    public static Zuora.zObject createSuspendSubscriptionAmendment(Zuora__Subscription__c subscription, Date suspendDate) {
        return createAmendment(subscription, 'SFDC Auto-generated Suspend Subscription Amendment', 'SuspendSubscription', suspendDate, null);
    }

    public static Zuora.zObject createSuspendSubscriptionAmendment(Zuora__Subscription__c subscription, String amendName, Date suspendDate) {
        return createAmendment(subscription, amendName, 'SuspendSubscription', suspendDate, null);
    }

    public static Zuora.zObject createResumeSubscriptionAmendment(Zuora__Subscription__c subscription, Date suspendDate, Date resumeDate) {
        return createAmendment(subscription, 'SFDC Auto-generated Resume Subscription Amendment', 'ResumeSubscription', suspendDate, resumeDate);
    }

    public static Zuora.zObject createResumeSubscriptionAmendment(Zuora__Subscription__c subscription, String amendName, Date suspendDate, Date resumeDate) {
        return createAmendment(subscription, amendName, 'ResumeSubscription', suspendDate, resumeDate);
    }

    public static Zuora.zObject createTermsAndConditionsAmendment(Zuora__Subscription__c subscription, Date suspendDate, Date resumeDate) {
        return createAmendment(subscription, 'SFDC Auto-generated Extends Subscription Term Amendment', 'TermsAndConditions', suspendDate, resumeDate);
    }

    public static Zuora.zObject createTermsAndConditionsAmendment(Zuora__Subscription__c subscription, String amendName, Date suspendDate, Date resumeDate) {
        return createAmendment(subscription, amendName, 'TermsAndConditions', suspendDate, resumeDate);
    }

    public static Zuora.zObject createAmendment(Zuora__Subscription__c subscription, String amendName, String amendType, Date startDate, Date endDate) {
        final Boolean isEvergreen = subscription.Zuora__TermSettingType__c == 'EVERGREEN';
        
        final String strStartDate = (startDate != null) ? Tools.format(startDate, 'yyyy-MM-dd') : null;
        final String strEndDate = (endDate != null) ? Tools.format(endDate, 'yyyy-MM-dd') : null;
        final String strTermStartDate = (subscription.Zuora__TermStartDate__c != null) 
                                            ? Tools.format(subscription.Zuora__TermStartDate__c, 'yyyy-MM-dd') : null;

        Zuora.zObject amendment = new Zuora.zObject('Amendment');
        //amendment.setValue('CustomerAcceptanceDate', strStartDate);
        amendment.setValue('ContractEffectiveDate', strStartDate);
        //amendment.setValue('EffectiveDate', strStartDate);
        amendment.setValue('SubscriptionId', subscription.Zuora__External_Id__c);
        //amendment.setValue('RenewalSetting', (isEvergreen ? 'RENEW_TO_EVERGREEN' : 'RENEW_WITH_SPECIFIC_TERM'));
        amendment.setValue('Status', 'Completed');
        amendment.setValue('Type', amendType);
        amendment.setValue('Name', amendName);

        switch on amendType {
            when 'SuspendSubscription' {
                amendment.setValue('SuspendDate', strStartDate);
            }
            when 'ResumeSubscription' {
                amendment.setValue('ResumeDate', strEndDate);
            }
            when 'TermsAndConditions' {
                final Integer renewalTerm = Integer.valueOf(subscription.Zuora__RenewalTerm__c.replaceAll('\\D+', ''));
                Date newEndDate = subscription.Zuora__SubscriptionEndDate__c.addDays(startDate.daysBetween(endDate));

                // Enhancement
                if(newEndDate.month() == subscription.Zuora__SubscriptionEndDate__c.month() || newEndDate.day() > 25) {
                    newEndDate = Date.newInstance(newEndDate.year(), newEndDate.month() + 1, 1);
                }

                final Integer currentTerm = subscription.Zuora__TermStartDate__c.daysBetween(newEndDate);

                amendment.setValue('ServiceActivationDate', strStartDate);
                amendment.setValue('AutoRenew', subscription.Zuora__AutoRenew__c);
                amendment.setValue('CurrentTerm', currentTerm);
                amendment.setValue('CurrentTermPeriodType', 'Day');
                amendment.setValue('RenewalTerm', renewalTerm);
                amendment.setValue('RenewalTermPeriodType', subscription.Zuora__RenewalTermPeriodType__c);
                amendment.setValue('TermStartDate', strTermStartDate);
                amendment.setValue('TermType', subscription.Zuora__TermSettingType__c);
            }
            when else {
                throw new DomusException.UnsupportedOperationException('Unsupported amendment type: "' + amendType + '". Needs implementation yet.');
            }
        }

        return amendment;
    }
    
    public static void deleteAmendments(List<String> amendmentZuoraIds) {
        System.debug('*** deleteAmendments(List<String>) ***');
        System.debug('Amendment Zuora Ids: ' + amendmentZuoraIds);
        
        final Zuora.zApi zApiInstance = new Zuora.zApi();
        if(!Test.isRunningTest()) zApiInstance.zlogin();

        List<Zuora.zApi.DeleteResult> results;

        if(!Test.isRunningTest()) {
            results = zApiInstance.zdelete('Amendment', amendmentZuoraIds);
        } else {
            results = new List<Zuora.zApi.DeleteResult>();

            Zuora.zApi.DeleteResult dr1 = new Zuora.zApi.DeleteResult();
            dr1.Success = True;

            Zuora.zApi.DeleteResult dr2 = new Zuora.zApi.DeleteResult();
            dr2.Success = False;
            dr2.Id = 'SubscriptionId';
            dr2.Errors = new List<Zuora.zObject>();

            results.add(dr1);
            results.add(dr2);
        }

        System.debug(LoggingLevel.ERROR, '*** deleteAmendments() ***\t' + generateErrorMessage(results));
    }

    private static String generateErrorMessage(List<Zuora.zApi.DeleteResult> results) {
        Boolean errorFound = false;

        final JSONGenerator jsonGen = JSON.createGenerator(false);
        jsonGen.writeStartObject();
        jsonGen.writeFieldName('ErrorMessage');
        jsonGen.writeStartArray();

        for(Integer i = 0; i < results.size(); i++) {
            final Zuora.zApi.DeleteResult result = results[i];

            if(result.Success) continue;
            errorFound = true;

            jsonGen.writeStartObject();
            jsonGen.writeStringField('ZObject', 'Amendment');
            jsonGen.writeStringField('Zuora Id', result.Id);

            for(Zuora.zObject error : result.Errors) {
                jsonGen.writeStringField('Field', error.getValue('Field') + '');
                jsonGen.writeStringField('Message', error.getValue('Message') + '');
                jsonGen.writeStringField('Code', error.getValue('Code') + '');
            }

            jsonGen.writeEndObject();
        }

        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
        jsonGen.close();

        if(errorFound) {
            return jsonGen.getAsString();
        }

        return 'Success';
    }

    private static String generateErrorMessage(Zuora.zApi.AmendResult result) {
        Boolean errorFound = false;

        final JSONGenerator jsonGen = JSON.createGenerator(false);
        jsonGen.writeStartObject();
        
        jsonGen.writeStringField('ZObject', 'Amendment');
        jsonGen.writeStringField('Subscription Zuora Id', result.SubscriptionId + '');

        jsonGen.writeFieldName('Zuora Amendment Ids');
        jsonGen.writeStartArray();

        if(result.AmendmentIds != null) {
            for(String amendId : result.AmendmentIds) {
                jsonGen.writeString(amendId);
            }
        }

        jsonGen.writeEndArray();

        jsonGen.writeStringField('Result', (Test.isRunningTest() || result.Success) ? 'Success' : 'Fail');
        jsonGen.writeFieldName('Zuora Errors');
        jsonGen.writeStartArray();

        if(result.Errors != null) {
            for(Zuora.zObject error : result.Errors) {
                jsonGen.writeStartObject();
                jsonGen.writeStringField('Field', error.getValue('Field') + '');
                jsonGen.writeStringField('Message', error.getValue('Message') + '');
                jsonGen.writeStringField('Code', error.getValue('Code') + '');
                jsonGen.writeEndObject();
            }
        }

        jsonGen.writeEndArray();

        jsonGen.writeEndObject();
        jsonGen.close();

        return jsonGen.getAsString();
    }

    private static List<Zuora.zObject> sortSubscriptionsByVersion(List<Zuora.zObject> subscriptions) {
		Boolean exchangeOccurred;

		do {
			exchangeOccurred = False;

			for(Integer i = 0; i < subscriptions.size() - 1; i++) {
				final Zuora.zObject currSub = subscriptions[i];
				final Integer csVersion = (Integer) currSub.getValue('Version');

				for(Integer j = i + 1; j < subscriptions.size(); j++) {
					final Zuora.zObject nextSub = subscriptions[j];
				    final Integer nsVersion = (Integer) nextSub.getValue('Version');

					if(csVersion > nsVersion) {
						exchangeOccurred = True;
						exchange(subscriptions, i, j);
					}
				}
			}

		} while(exchangeOccurred);

		return subscriptions;
	}

	private static void exchange(List<Zuora.zObject> subscriptions, Integer a, Integer b) {
		final Zuora.zObject bkp = subscriptions[b];
		subscriptions[b] = subscriptions[a];
		subscriptions[a] = bkp;
	}

    public class SuspensionInfo {
        public Zuora__Subscription__c subscription;
        public Date suspendDate;
        public Date resumeDate;
        public Boolean extendTerms;
        public String suspensionType;

        public SuspensionInfo(Zuora__Subscription__c subscription, Date suspendDate, Date resumeDate, Boolean extendTerms, String suspensionType) {
            this.subscription = subscription;
            this.suspendDate = suspendDate;
            this.resumeDate = resumeDate;
            this.extendTerms = extendTerms;
            this.suspensionType = suspensionType;
        }
    }

}