public class QuoteBundleController{


    public String selectedBundleType {get;set;}
    public String selectedTestata1 {get;set;}
    public String selectedTestata2 {get;set;}
    public String selectedLingua {get;set;}
    public String selectedRatePlan {get;set;}
    public String selectedCalendar {get;set;}
    public Integer selectedQuantity {get;set;}

    public Boolean isLinguaSection {get;set;} 
    public Boolean isRatePlanSection {get;set;}
    public Boolean isDetailSection {get;set;}
    public Boolean isCalendarSection {get;set;}

    public Boolean isRatePlanError{get;set;}
    public Boolean isRatePlanListEmpty {get;set;}
    public Boolean isCalendarListEmpty {get;set;}

    public List<WrapBundle> wrapBundleList {get;set;}
    public List<zqu__ProductRatePlanCharge__c> chargeList {get;set;}

    public List<Zuora__Subscription__c> previousSubscriptionList {get;set;}
    public Id targetSubscription {get;set;}

    public String deleteQuoteId{get;set;}

    public String initString{get{
        initBundle();
        return '';
        } set;}

    public List<zqu__QuoteRatePlan__c> bundleList {get;set;}

    public Boolean editMode{get;set;}
    /*public Id quoteId {get;set{
            if(String.isBlank(quoteId)){
                quoteId = value;
                initBundle();
        }
    }*/
    public Id quoteId {get;set;}
    public zqu__Quote__c quoteObj {get;set;}

    public zqu.quote quote {get;set{
            quote = value;
            quoteObj = quote.getSObject();
            quoteId = quoteObj.id;
        }
    }

    public List<SelectOption> bundleOptions {get;set;}
    public List<SelectOption> ratePlanOptions{get;set;}
    public List<SelectOption> calendarOptions{get;set;}


    private void initBundle(){
        try{
            bundleOptions = new List<SelectOption>();
            previousSubscriptionList = null;
            System.debug('******[QuoteBundleController-initBundle] quoteId: ' + quoteId);
            bundleList = DomusUtil.getQuoteBundle(quoteId);
            System.debug('******[QuoteBundleController-initBundle] bundleList: ' + bundleList);
            List<Id> bundleIdList = new List<Id>();
            for(zqu__QuoteRatePlan__c bundle : bundleList){
                bundleIdList.add(bundle.Id);
            }
            Map<Id, Integer> bundleQtaMap = new Map<Id, Integer>();
            System.debug('******[QuoteBundleController-initBundle] bundleIdList: ' + bundleIdList);
            List<zqu__QuoteRatePlanCharge__c> bundleChargeList = DomusUtil.getMultiQuoteChargeList(bundleIdList);
            Map<Id, List<zqu__QuoteRatePlanCharge__c>> bundleChargeMap = new Map<Id, List<zqu__QuoteRatePlanCharge__c>>();
            System.debug('******[QuoteBundleController-initBundle] bundleChargeList: ' + bundleChargeList);
            for(zqu__QuoteRatePlanCharge__c b : bundleChargeList){
                if(b.Tipo_Charge__c == 'Abbonamento' || b.Tipo_Charge__c == 'Vendita Diretta'){
                    Id quoteRatePlanId = b.zqu__QuoteRatePlan__c;
                    if(bundleChargeMap.get(quoteRatePlanId) == null){
                        bundleChargeMap.put(quoteRatePlanId, new List<zqu__QuoteRatePlanCharge__c>());
                        if(bundleQtaMap.get(quoteRatePlanId) == null && b.zqu__Quantity__c != null){
                            bundleQtaMap.put(quoteRatePlanId, Integer.valueOf(b.zqu__Quantity__c));
                        }
                    }
                    bundleChargeMap.get(quoteRatePlanId).add(b);
                }
            }
            System.debug('*** bundleChargeMap: ' + bundleChargeMap);
            wrapBundleList = new List<WrapBundle>();
            for(zqu__QuoteRatePlan__c qrp : bundleList){
                WrapBundle wb = new WrapBundle(qrp, bundleChargeMap.get(qrp.Id));
                wb.qta = bundleQtaMap.get(qrp.Id);
                wrapBundleList.add(wb);
            }
            System.debug('*** wrapBundleList: ' + wrapBundleList);


            List<zqu__QuoteRatePlanCharge__c> abbonamentiList = [SELECT Id 
                                                                FROM zqu__QuoteRatePlanCharge__c 
                                                                WHERE zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteId
                                                                AND Tipo_Charge__c = 'Abbonamento'];
            if(abbonamentiList == null || abbonamentiList.size()==0){
                Schema.DescribeFieldResult supportFields = zqu__ProductRatePlan__c.TipoProdotto__c.getDescribe();
                List<Schema.PicklistEntry> supportEntries = supportFields.getPicklistValues();
                bundleOptions.add(new SelectOption('', '- Seleziona il tipo del Bundle -'));
                zqu__Quote__c quoteObj = quote.getSObject();
                
                for( Schema.PicklistEntry f : supportEntries)   {
                    System.debug(f.getLabel() + ' - ' + f.getValue());
                    if(f.getLabel().contains('Bundle')){
                        if(quoteObj.Subscription_Precedente__c == null){
                            bundleOptions.add(new SelectOption(f.getLabel(), f.getValue()));
                        }else if (quoteObj.Subscription_Precedente__c != null && !f.getLabel().contains('Vendite')){
                            bundleOptions.add(new SelectOption(f.getLabel(), f.getValue()));
                        }
                    }
                } 
            }else{
                selectedBundleType = 'Bundle Vendite Dirette';
                getRatePlanList();
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public PageReference goToCart(){
        return Page.WizardCart;
    }

    public void getRatePlanList(){
        try{
            ratePlanOptions = new List<SelectOption>();
            resetErrors();
            isCalendarSection = false;
            isRatePlanSection = false;
            isDetailSection = false;
            selectedRatePlan = null;
            if(!String.isBlank(selectedBundleType)){
                //zqu.Quote quote = zqu.Quote.getInstance(quoteId);
                //zqu__Quote__c quoteObj = quote.getSObject();
                

                Account acc = [SELECT RecordType.DeveloperName FROM Account WHERE Id = :quoteObj.zqu__Account__c LIMIT 1];
                String recordType = acc.RecordType.DeveloperName;

                Set<String> testateSet = new Set<String>();

                zqu__Quote__c quoteObj = quote.getSObject();
                System.debug(loggingLevel.Error, '*** quoteObj.Subscription_Precedente__c: ' + quoteObj.Subscription_Precedente__c);
                if(quoteObj.Subscription_Precedente__c != null){
                    Id subId = quoteObj.Subscription_Precedente__c;
                    List<Zuora__SubscriptionProductCharge__c> chargeList = [SELECT Id, Testata__c FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Subscription__c = :subId AND Tipo_Prodotto__c = 'Abbonamento'];
                    System.debug(loggingLevel.Error, '*** chargeList.size(): ' + chargeList.size());
                    for(Zuora__SubscriptionProductCharge__c charge : chargeList){
                        testateSet.add(charge.Testata__c.toLowerCase());
                    }
                    System.debug(loggingLevel.Error, '*** testateSet: ' + testateSet);

                    Date searchDate = Date.today();
                    Zuora__Subscription__c sub = [SELECT Id, Zuora__SubscriptionEndDate__c FROM Zuora__Subscription__c WHERE Id = :subId LIMIT 1];
                    System.debug(loggingLevel.Error, '*** sub: ' + sub);
                    System.debug(loggingLevel.Error, '*** selectedBundleType: ' + selectedBundleType);
                    System.debug(loggingLevel.Error, '*** recordType: ' + recordType);
                    System.debug(loggingLevel.Error, '*** searchDate: ' + searchDate);
                    System.debug(loggingLevel.Error, '*** testateSet: ' + testateSet);
                    if(sub != null && sub.Zuora__SubscriptionEndDate__c != null){
                        searchDate = sub.Zuora__SubscriptionEndDate__c;
                        Map<Id, String> ratePlanMap = DomusUtil.getBundleRenewal(selectedBundleType, recordType, searchDate, testateSet);
                        System.debug(loggingLevel.Error, '*** ratePlanMap: ' + ratePlanMap);
                        if(ratePlanMap != null){
                            isRatePlanSection = true;
                            ratePlanOptions.add(new SelectOption('','- Seleziona un Bundle -'));
                            for(Id idRp : ratePlanMap.keySet()){
                                 ratePlanOptions.add(new SelectOption(idRp, ratePlanMap.get(idRp)));
                            }
                        }
                    }else{
                        isRatePlanListEmpty = true;
                    }
                }else{
                    Account intermediario;
                    if(quoteObj.Intermediario__c != null){
                        List<Account> intermedList = [SELECT Id,Name,Codice_Convenzione__c FROM Account WHERE Id = :quoteObj.Intermediario__c];
                        if(intermedList != null && intermedList.size() > 0){
                            intermediario = intermedList.get(0);
                        }
                    }

                    List<zqu__QuoteRatePlan__c> abbQuoteRPList = [SELECT Id, zqu__ProductRatePlan__c FROM zqu__QuoteRatePlan__c
                                                                WHERE zqu__Quote__c = :quoteId
                                                                AND Rate_Plan_Type__c = 'Bundle Vendite Dirette'];
                    Set<Id> addedRatePlanId = new Set<Id>();
                    for(zqu__QuoteRatePlan__c qrp : abbQuoteRPList){
                        if(qrp.zqu__ProductRatePlan__c != null){
                            addedRatePlanId.add(qrp.zqu__ProductRatePlan__c);
                        }
                    }

                    List<zqu__ProductRatePlan__c> ratePlanList = DomusUtil.getBundleRatePlan(selectedBundleType, recordType, intermediario);
                    if(ratePlanList != null && ratePlanList.size()>0){
                        isRatePlanSection = true;
                        ratePlanOptions.add(new SelectOption('','- Seleziona un Bundle -'));
                        for(zqu__ProductRatePlan__c rp : ratePlanList){  
                            if(!addedRatePlanId.contains(rp.Id)){
                                System.debug('Added option: ' +rp.Id+', '+rp.Name);
                                ratePlanOptions.add(new SelectOption(rp.Id, rp.Name));
                            }
                        }
                    }else{
                        isRatePlanListEmpty = true;
                    }   
                }
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void getRatePlanInfo(){
        try{
            selectedQuantity = 1; 
            isCalendarListEmpty = false;
            isCalendarSection = false;
            isDetailSection = false;
            selectedCalendar = null;
            calendarOptions = new List<SelectOption>();

            System.debug('Recupero i charge per il rateplan: ' + selectedRatePlan);
            chargeList = DomusUtil.getChargeList(selectedRatePlan);
            System.debug(loggingLevel.Error, '*** chargeList: ' + chargeList);
            if(chargeList == null || chargeList.size() == 0){
                isRatePlanError = true;
            }
            else{
                isDetailSection = true;
            }

            if(selectedBundleType != 'Bundle Vendite Dirette'){
                List<Testata__c> testataList = DomusUtil.getTestataList(selectedRatePlan);
                System.debug(loggingLevel.Error, '*** testataList: ' + testataList);
                if(testataList != null && testataList.size() > 0){                    
                    List<Id> testataIdList = new List<Id>();
                    for(Testata__c t : testataList){
                        testataIdList.add(t.Id);
                    }
                    

                    zqu__Quote__c quoteObj = quote.getSObject();
                    System.debug(loggingLevel.Error, '*** quoteObj.Subscription_Precedente__c: ' + quoteObj.Subscription_Precedente__c);
                    System.debug(loggingLevel.Error, '*** testataIdList: ' + testataIdList);
                    if(quoteObj.Subscription_Precedente__c != null){
                        Id subId = quoteObj.Subscription_Precedente__c;
                        Zuora__Subscription__c sub = [SELECT Id, Zuora__SubscriptionEndDate__c FROM Zuora__Subscription__c WHERE Id = :subId LIMIT 1];
                        if(sub != null && sub.Zuora__SubscriptionEndDate__c != null){
                            //*** DH-45 ***//
                            /*
                            isCalendarSection = true;
                            System.debug(loggingLevel.Error, '*** sub.Zuora__SubscriptionEndDate__c: ' + sub.Zuora__SubscriptionEndDate__c);
                            Calendario__c c = DomusUtil.getCalendarioStartCopyPerRinnovo(testataIdList, sub.Zuora__SubscriptionEndDate__c);
                            calendarOptions.add(new SelectOption(c.id, c.Name));
                            if(String.isBlank(selectedCalendar) && ((c.Data_Uscita_Effettiva__c != null && c.Data_Uscita_Effettiva__c > Date.today()) || c.Data_Uscita_Attesa__c > Date.today())){
                                selectedCalendar = c.id;
                            }
                            */
                            List<Calendario__c> listaCalendariPerCopiaPartenza = DomusUtil.getCalendarioStartCopyList(testataIdList, sub.Zuora__SubscriptionEndDate__c);
                            if(listaCalendariPerCopiaPartenza != null && listaCalendariPerCopiaPartenza.size() > 0){
                                isCalendarSection = true;
                                for(Calendario__c c : listaCalendariPerCopiaPartenza){
                                    calendarOptions.add(new SelectOption(c.id, c.Name));
                                    if(String.isBlank(selectedCalendar) && ((c.Data_Uscita_Effettiva__c != null && c.Data_Uscita_Effettiva__c > Date.today()) || c.Data_Uscita_Attesa__c > Date.today())){
                                        selectedCalendar = c.id;
                                    }
                                }
                            }
                            //*** DH-45 - Fine ***//
                        } else {
                            isCalendarListEmpty = true;
                        }
                    } else{
                        List<Calendario__c> calList = DomusUtil.getMultiCalendarioList(testataIdList);
                        if(calList != null && calList.size()>0){
                            isCalendarSection = true;
                            for(Calendario__c c : calList){
                                calendarOptions.add(new SelectOption(c.id, c.Name));
                                System.debug(c);
                                if(String.isBlank(selectedCalendar) && ((c.Data_Uscita_Effettiva__c != null && c.Data_Uscita_Effettiva__c > Date.today()) || c.Data_Uscita_Attesa__c > Date.today())){
                                    selectedCalendar = c.id;
                                }
                            }

                            Id accountId = quoteObj.zqu__Account__c;
                            Set<String> testateStringSet = new Set<String>();
                            for(zqu__ProductRatePlanCharge__c c : chargeList){
                                if(c.TipoProdotto__c == 'Abbonamento'){
                                    testateStringSet.add('Testate__c LIKE \'%'+c.Testata__c+'%\'');
                                }
                            }
                            if(testateStringSet.size() > 0){
                                String whereClause = 'Zuora__Account__c = :accountId AND ('+ String.join(new List<String>(testateStringSet), ' OR ') +')';


                                String additionalFields = 'Zuora__CustomerAccount__r.Sold_To_Salesforce__c, Zuora__CustomerAccount__r.Bill_To_Salesforce__c,'+
                                                        'Rate_Plan_Abbonamento__r.Name';
                                String queryString = Utils.getSelectAllQuery('Zuora__Subscription__c', whereClause, null, additionalFields, true);
                                previousSubscriptionList = (List<Zuora__Subscription__c>) Database.query(queryString);
                            }
                        }else{
                            isCalendarListEmpty = true;
                        }
                    }



                }else{
                    isCalendarListEmpty = true;
                }
            }
            
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    }

    public void deleteSubList(){
        previousSubscriptionList = null;
    }

    public PageReference goToSubscription(){
        zqu__Quote__c quoteObj = quote.getSObject();
        quoteObj.Quote_Status__c = 'Cancelled';
        quote.save();
        System.debug('TargetSub: ' +targetSubscription);
        PageReference ref = new PageReference('/'+targetSubscription);
        return ref;
    }

    public PageReference save(){
        Savepoint sp = null;
        if(!Test.isRunningTest()) sp = Database.setSavepoint();
        try{
            if(selectedQuantity <=0){
                appendErrorMessage('La quantità deve avere un valore maggiore o uguale a 1');
                return null;
            }
            /*Integer numberOfBundle = [SELECT COUNT() FROM zqu__QuoteRatePlan__c
                                            WHERE zqu__quote__c = :quoteId AND (Rate_Plan_Type__c = 'Bundle Abbonamenti' 
                                            OR Rate_Plan_Type__c = 'Bundle Misti' OR Rate_Plan_Type__c = 'Bundle Vendite Dirette')];
            if(numberOfBundle > 0){
                appendErrorMessage('Impossibile aggiungere altri bundle');
                return null;
            } */
            if(!String.isBlank(selectedRatePlan)&&!String.isBlank(selectedBundleType)){
                Id quoteRatePlanId = DomusUtil.setBundle(quoteId, selectedRatePlan, selectedCalendar, selectedBundleType);
                if(quoteRatePlanId != null && selectedBundleType!='Bundle vendite dirette'){

                    zqu__ProductRatePlan__c productRatePlan = [SELECT Id, ConAvvisi__c FROM zqu__ProductRatePlan__c WHERE Id = :selectedRatePlan LIMIT 1];
                    if (productRatePlan != null){
                         quoteObj.ConAvvisi__c = productRatePlan.ConAvvisi__c;
                    }
                   
                }
                List<String> codiceSAPList = new List<String>();

                List<zqu__QuoteRatePlanCharge__c> quoteChargeList = [SELECT zqu__Period__c, Supporto__c, zqu__ProductRatePlanCharge__r.CodiceSAP__c,
                        zqu__SpecificBillingPeriod__c, zqu__EffectivePrice__c, zqu__Quantity__c, zqu__Total__c, Codice_Prodotto_SAP__c,
                        Tipo_Charge__c, zqu__ProductRatePlanCharge__r.Supporto__c, Prodotto_Vendita_Diretta__r.Supporto__c
                        FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c= :quoteRatePlanId];
                    System.debug('quoteChargeList : ' + quoteChargeList);
                    for(zqu__QuoteRatePlanCharge__c charge : quoteChargeList){
                        if(charge.Tipo_Charge__c == 'Abbonamento' || charge.Tipo_Charge__c =='Vendita Diretta'){
                            charge.zqu__Quantity__c = selectedQuantity;
                            charge.zqu__Total__c = charge.zqu__Quantity__c * charge.zqu__EffectivePrice__c;
                            if(charge.Tipo_Charge__c == 'Abbonamento' || charge.Tipo_Charge__c == 'Vendita Diretta'){
                                charge.Supporto__c = charge.zqu__ProductRatePlanCharge__r.Supporto__c;
                            }//else if(charge.Tipo_Charge__c == 'Vendita Diretta'){
                               // charge.Supporto__c = charge.Prodotto_Vendita_Diretta__r.Supporto__c; 
                            //}
                            if(charge.Tipo_Charge__c == 'Vendita Diretta'){// && charge.Supporto__c != 'Digitale'){
                               codiceSAPList.add(charge.zqu__ProductRatePlanCharge__r.CodiceSAP__c);
                               charge.Codice_Prodotto_SAP__c = charge.zqu__ProductRatePlanCharge__r.CodiceSAP__c;
                            }
                    }
                }

                if(codiceSAPList.size() > 0){
                    //List<Movimentazione_Stock__c> movStockList = new List<Movimentazione_Stock__c>();
                    List<Prodotto_Vendita_Diretta__c> pvdList = [SELECT Id,Name, Descrizione_Prodotto_SAP__c, Giacenza__c, Riferimento_Magazzino__c,
                                                    Codice_Prodotto_SAP__c, Supporto__c
                                                    FROM Prodotto_Vendita_Diretta__c
                                                    WHERE Codice_Prodotto_SAP__c = :codiceSAPList];
                                                    //AND Supporto__c != 'Digitale'];

                    if(codiceSAPList.size() != pvdList.size()){
                        if(!Test.isRunningTest()) Database.rollback(sp);
                        appendErrorMessage('Prodotto vendita diretta non trovato');
                        return null;
                    }
                    List<Id> venditeDiretteIdList = new List<Id>();
                    Map<String, Id> venditeDiretteCodiceSapMap = new Map<String, Id>();
                    for(Prodotto_Vendita_Diretta__c pvdTmp : pvdList){
                        venditeDiretteIdList.add(pvdTmp.Id);
                        if(pvdTmp.Codice_Prodotto_SAP__c != null){
                            venditeDiretteCodiceSapMap.put(pvdTmp.Codice_Prodotto_SAP__c, pvdTmp.Id);
                        }
                    }

                    Map<Id, Decimal> stockMap = DomusUtil.getMovimentazioneStock(venditeDiretteIdList);

                    for(Prodotto_Vendita_Diretta__c pvd : pvdList){
                        if(pvd.Supporto__c != 'Digitale'){
                            Integer giacenza = Integer.valueOf(pvd.Giacenza__c);
                            if(stockMap.containsKey(pvd.id)){
                                giacenza += Integer.valueOf(stockMap.get(pvd.Id));
                            }

                            if(giacenza < selectedQuantity){
                                if(!Test.isRunningTest()) Database.rollback(sp);
                                appendErrorMessage('Errore la quantità richiesta per il prodotto ' + pvd.Name + ' non è disponibile');
                                return null;
                            }
                        }


                            /*Movimentazione_Stock__c movStockTmp = new Movimentazione_Stock__c();
                            movStockTmp.Codice_Prodotto_SAP__c = pvd.Codice_Prodotto_SAP__c;
                            movStockTmp.Quantita__c = selectedQuantity;
                            movStockTmp.Segno_del_movimento_del__c = '-';
                            movStockTmp.Riferimento_Magazzino__c = pvd.Riferimento_Magazzino__c;
                            movStockTmp.Stato__c = pvd.Supporto__c != 'Digitale' ? 'Nuovo' : 'Recepito da SAP';    
                            movStockTmp.Quote__c = quoteId;
                            movStockTmp.Prodotto_Vendita_Diretta__c = pvd.Id;
                            movStockList.add(movStockTmp);*/
                    }   
                    //insert(movStockList);

                    for(zqu__QuoteRatePlanCharge__c charge : quoteChargeList){
                        if(venditeDiretteCodiceSapMap.get(charge.Codice_Prodotto_SAP__c) != null){
                            charge.Prodotto_Vendita_Diretta__c = venditeDiretteCodiceSapMap.get(charge.Codice_Prodotto_SAP__c);
                        }
                    }
                }
                update quoteChargeList;
            } 
            return goToCart();
            
        }
        catch(Exception e) {
            if(!Test.isRunningTest()) Database.rollback(sp);
            appendErrorMessage(e);
            return null;
        }
    }


    public void removeBundle(){
        try{
            if(!String.isBlank(deleteQuoteId)){
            
            List<zqu__QuoteRatePlan__c> quoteRPList = [SELECT zqu__ProductRatePlan__c FROM zqu__QuoteRatePlan__c WHERE Id = :deleteQuoteId];
                List<Id> ratePlanIdList = new List<Id>();
                if(quoteRPList != null && quoteRPList != null){
                    for(zqu__QuoteRatePlan__c qrp : quoteRPList){
                        ratePlanIdList.add(qrp.zqu__ProductRatePlan__c);
                    }
                    List<Campaign> campaignList = [SELECT Id FROM Campaign WHERE Rate_Plan_in_Campagna__c = :ratePlanIdList];
                    List<Id> campaignIdList = new List<Id>();
                    if(campaignList != null && campaignList.size() > 0){
                        for(Campaign c : campaignList){
                            campaignIdList.add(c.Id);
                        }
                        List<zqu__QuoteRatePlan__c> quoteCampaignList = [SELECT Id FROM zqu__QuoteRatePlan__c WHERE Campaign__c  = :campaignIdList];
                        List<Id> quoteCampaignRatePlanId = new List<Id>();
                        if(quoteCampaignList != null && quoteCampaignList.size() > 0){
                            for(zqu__QuoteRatePlan__c q : quoteCampaignList){
                                quoteCampaignRatePlanId.add(q.Id);
                            }
                            DomusUtil.removeRatePlanList(quoteId, quoteCampaignRatePlanId);
                        }
                    }
                }

                String ratePlanType;

                List<zqu__quoterateplancharge__c> quoteRatePlanChargeList = [SELECT Codice_Prodotto_SAP__c FROM zqu__quoterateplancharge__c 
                                                                        WHERE zqu__quoterateplan__c = :deleteQuoteId
                                                                        AND Codice_Prodotto_SAP__c != null];
                List<String> codiceSAPList = new List<String>();
                for(zqu__quoterateplancharge__c tmpCharge : quoteRatePlanChargeList){
                    codiceSAPList.add(tmpCharge.Codice_Prodotto_SAP__c);
                }

                DomusUtil.removeRatePlan(quoteId, deleteQuoteId);
                for(Integer i=0; i < bundleList.size(); i++){
                    zqu__QuoteRatePlan__c qrp = bundleList.get(i);
                    if(qrp.Id == deleteQuoteId){
                        ratePlanType = qrp.Rate_Plan_Type__c;
                        bundleList.remove(i);
                        break;
                    }
                }

                

                List<Movimentazione_Stock__c> movimentazioneList = [SELECT Id FROM Movimentazione_Stock__c WHERE Codice_Prodotto_SAP__c = :codiceSAPList
                                                                AND Quote__c = :quoteId];
                if(movimentazioneList != null && movimentazioneList.size()>0){
                    delete movimentazioneList;
                }

                deleteQuoteId = null;

                if(ratePlanType == 'Bundle Abbonamenti' || ratePlanType == 'Bundle Misti'){
                    quoteObj.zqu__StartDate__c = Date.today();
                    quoteObj.zqu__Service_Activation_Date__c = null;
                    quoteObj.zqu__ValidUntil__c  = Date.today();
                    quoteObj.zqu__InitialTerm__c = 1;
                    quoteObj.zqu__RenewalTerm__c = 1;
                    quoteObj.ConAvvisi__c = null;
                    quoteObj.Testate__c = '';
                    quoteObj.Supporto__c = '';
                    if(quoteObj.Subscription_Precedente_Text__c == null){
                        quoteObj.Numero_di_Cicli_Text__c = '1';
                    }
                }
 
                quote.save();
            }
        } catch(Exception e) {
            appendErrorMessage(e);
        }
    } 


    public class WrapBundle  {
        public zqu__QuoteRatePlan__c bundle {get;set;}
        public List<zqu__QuoteRatePlanCharge__c> chargeList {get;set;}
        public Integer qta {get;set;}
        public WrapBundle(zqu__QuoteRatePlan__c qb, List<zqu__QuoteRatePlanCharge__c> cl){
            bundle = qb;
            chargeList = cl;
        }



    }


    private void resetErrors(){
        isRatePlanError = false;
        isRatePlanListEmpty = false;
        isCalendarListEmpty = false;
    }


    private void appendErrorMessage(Exception e) {
        System.debug(loggingLevel.Error, 'Message: ' + e.getMessage() + '. Line Number: '+e.getLineNumber() + '. Stack: '+e.getStackTraceString());
        appendErrorMessage(e.getMessage());
    }

    private void appendErrorMessage(String messageError) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
    }    

}