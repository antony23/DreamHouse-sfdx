public class NewAccountExtension {
    
    public Account acc {get;set;}
    
    public NewAccountExtension (ApexPages.StandardController stdController) {
        acc = (Account) stdController.getRecord();
    }

    public PageReference init(){
        
        PageReference retUrl = null;

        if(acc.RecordTypeId != Schema.SObjectType.Account.getRecordTypeInfosByName().get('B2C').getRecordTypeId()){
            retUrl = new PageReference('/001/e?retURL=/001/o&RecordType='+acc.RecordTypeId+'&ent=Account&nooverride=1');
        }else{
            retUrl = Page.EditContact;
            retUrl.getParameters().put('newAccountB2C','1');
            retUrl.getParameters().put('retURL','/001/o');
        }

        return retUrl;

    }

}