@isTest (SeeAllData=true)
private class ExtendSubscriptionControllerTest{
	
	@isTest
	static void testM(){
		ExtendSubscriptionController ctrl = new ExtendSubscriptionController(null);
		ctrl.appendErrorMessage(new DomusException('test'));
		ctrl.appendErrorMessage('test');

		SubscriptionTriggerHandler.skipTrigger = true;
		
		Zuora__Subscription__c sub1 = [SELECT Id FROM Zuora__Subscription__c WHERE Zuora__Status__c = 'Active' AND Numero_di_Cicli__c > 0 AND Testate__c LIKE '%quattroruote%' LIMIT 1];
		ctrl.createQuoteAction();

		ApexPages.currentPage().getParameters().put('id',sub1.Id);
		ctrl.createQuoteAction();

		sub1.Zuora__Status__c = 'Cancelled';
		update sub1;
		ctrl.createQuoteAction();

		ctrl.goToSubscription();
	}
}