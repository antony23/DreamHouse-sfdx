public class SubscriptionUtil{
	

	public static Map<String,Object> getSubscriptionParameters(Id ratePlanId, Date targetDate, Boolean isRESTRequest){
        System.debug(loggingLevel.Error, '*** ratePlanId: ' + ratePlanId);
        System.debug(loggingLevel.Error, '*** targetDate: ' + targetDate);
		Map<String, Object> resultMap = new Map<String,Object>();
		String testate;
		String dettaglioCopiePerTestata;
		String supporto;
        Integer numVenditeDirette = 0;
		List<Testata__c> testataList = DomusUtil.getTestataList(ratePlanId);
		Set<Id> testataIdSet = new Set<Id>();
		for(Testata__c t : testataList){
			testataIdSet.add(t.id);
		}
		List<Id> testataIdList = new List<Id>(testataIdSet);

		Calendario__c startCalendar = (isRESTRequest)   ? DomusUtil.getCalendarioStartCopySubscriptionREST(testataIdList, targetDate) 
                                                        : DomusUtil.getCalendarioStartCopySubscription(testataIdList, targetDate);
                                                        
        System.debug(loggingLevel.Error, '*** startCalendar: ' + startCalendar);
        Calendario__c endCalendar ;
	
		List<zqu__ProductRatePlanCharge__c> chargeList = DomusUtil.getChargeList(ratePlanId);
		Date startDate = startCalendar.Data_Uscita__c;
		Date endDate = startCalendar.Data_Uscita__c;
        //Date startDate = Date.today();
        //Date endDate = Date.today();

        Map<String, Testata__c> testataMap = new Map<String, Testata__c>();
        for(Testata__c t : testataList){
            testataMap.put(t.IssueCalendar__c, t);
        }

        Set<String> supportSet = new Set<String>();
        for(zqu__ProductRatePlanCharge__c charge : chargeList){
            if(charge.TipoProdotto__c == 'Abbonamento'){
                Testata__c testataTmp = testataMap.get(charge.IssueCalendar__c);

                Calendario__c startCalendarTmp = (isRESTRequest)    ? DomusUtil.getCalendarioStartCopySubscriptionREST(testataTmp.Id, targetDate)
                                                                    : DomusUtil.getCalendarioStartCopySubscription(testataTmp.Id, startDate);

                Calendario__c endCalendarTmp = (isRESTRequest)  ? DomusUtil.getCalendarioEndCopyByTestataREST(testataTmp.Id, targetDate, Integer.valueOf(charge.Uscite__c))
                                                                : DomusUtil.getCalendarioEndCopyByTestata(testataTmp.Id, startDate, Integer.valueOf(charge.Uscite__c));

                System.debug(loggingLevel.Error, '*** startCalendarTmp: ' + startCalendarTmp);
                System.debug(loggingLevel.Error, '*** endCalendarTmp: ' + endCalendarTmp);
                System.debug(loggingLevel.Error, '*** endDate: ' + endDate);
                System.debug(loggingLevel.Error, '*** testataTmp.Id: ' + testataTmp.Id);
                System.debug(loggingLevel.Error, '*** startDate: ' + startDate);
                System.debug(loggingLevel.Error, '*** Integer.valueOf(charge.Uscite__c): ' + Integer.valueOf(charge.Uscite__c));
                if(endCalendarTmp.Data_Uscita__c >= endDate){
                    endDate = endCalendarTmp.Data_Uscita__c;	
                    endCalendar = endCalendarTmp;
                }
                if(String.isBlank(testate)){
                    testate = charge.Testata__c;
                    dettaglioCopiePerTestata = startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }else{
                    testate += ';'+charge.Testata__c;
                    dettaglioCopiePerTestata += ';'+startCalendarTmp.Name + ' - ' + endCalendarTmp.Name;
                }
                if(String.isNotBlank(charge.Supporto__c)){
                    supportSet.add(charge.Supporto__c);
                }
        	}
            if(charge.TipoProdotto__c == 'Vendita diretta'){
               numVenditeDirette++; 
            }
        }
        supporto = String.join(new List<String>(supportSet), ';');

        dettaglioCopiePerTestata = dettaglioCopiePerTestata.length() > 240 ? dettaglioCopiePerTestata.left(240) + '...' : dettaglioCopiePerTestata;
        
        resultMap.put('supporto', supporto);
        resultMap.put('testate', testate);
        resultMap.put('dettaglioCopiePerTestata', dettaglioCopiePerTestata);
        resultMap.put('calendarioInizio', startCalendar.Id);
        resultMap.put('calendarioFine',endCalendar.Id);
        resultMap.put('numVenditeDirette', numVenditeDirette);
        
        Decimal numWeeks = ( (Decimal) startDate.daysBetween(endDate))/7;
        Integer numWeeksInt = numWeeks.round(System.RoundingMode.CEILING).intValue();
        endDate  = startDate.addDays(7*numWeeksInt);
        if(numWeeksInt == 0) numWeeksInt = 1;
        resultMap.put('startDate', startDate);
        resultMap.put('endDate', endDate);
        resultMap.put('term', numWeeksInt);
        resultMap.put('termPeriodType', 'Week');
        resultMap.put('period', 'Specific Weeks');

        return resultMap;
	}    



    public static Map<String,Object> getQuoteDettaglioCopie(Id quoteId){
        Map<String, Object> resultMap = new Map<String,Object>();
        String dettaglioCopie = '';
        List<zqu__QuoteRatePlan__c> ratePlanList = [SELECT Id, zqu__ProductRatePlan__c
                                                FROM zqu__QuoteRatePlan__c
                                                WHERE zqu__quote__c = :quoteId
                                                AND (Rate_Plan_Type__c = 'Abbonamento' 
                                                OR Rate_Plan_Type__c = 'Bundle Abbonamenti' 
                                                OR Rate_Plan_Type__c = 'Bundle Misti')];
        if(ratePlanList != null && ratePlanList.size() >0){
            List<zqu__Quote__c> quoteList = [SELECT Id,zqu__StartDate__c FROM zqu__quote__c WHERE Id = :quoteId];
            if(quoteList != null && quoteList.size() > 0){
                Date dataInizio = quoteLIst.get(0).zqu__StartDate__c;
                Id ratePlanId = ratePLanList.get(0).zqu__ProductRatePlan__c;
                Map<String,Object> subParamMap = getSubscriptionParameters(ratePlanId, dataInizio, false);
                if(subParamMap != null && subParamMap.size() >0){
                    dettaglioCopie = (String) subParamMap.get('dettaglioCopiePerTestata');
                }
            }
        }
        

        List<zqu__QuoteRatePlanCharge__c> chargeList = [SELECT Id, Prodotto_Vendita_Diretta__r.Name
                                                        FROM zqu__QuoteRatePlanCharge__c 
                                                        WHERE zqu__QuoteRatePlan__r.zqu__Quote__c = :quoteId
                                                        AND Tipo_Charge__c = 'Vendita Diretta'];
        for(zqu__QuoteRatePlanCharge__c charge : chargeList){
            if(!String.isBlank(dettaglioCopie)){
                dettaglioCopie += ';';
            }
            dettaglioCopie += charge.Prodotto_Vendita_Diretta__r.Name;
        }

        dettaglioCopie = dettaglioCopie.length() > 240 ? dettaglioCopie.left(240) + '...' : dettaglioCopie;        

        resultMap.put('dettaglioCopie', dettaglioCopie);

        return resultMap;
    }

}