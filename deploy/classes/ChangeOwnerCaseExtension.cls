public with sharing class ChangeOwnerCaseExtension {
    public Case c {get;set;}
    Id oldOwnerId;
    public Boolean notifyOwner {get;set;}
    public UserWrapper[] users {get;set;}
    
    public ChangeOwnerCaseExtension(ApexPages.StandardController controller) {
        c = (Case) controller.getRecord();

        oldOwnerId = c.OwnerId;
        users = new UserWrapper[]{};
            
        for(User u : [Select FirstName,LastName,Email From User 
                      Where Id !=: UserInfo.getUserId()
                      And UserType = 'Standard'
                      And IsActive = true
                      Order By LastName,FirstName
                     ]){
            users.add(new UserWrapper(u));
        }
    }
    
    public PageReference changeOwner(){
        Savepoint sp = null;
        if(!Test.isRunningTest()) sp = Database.setSavepoint();
        PageReference p = new PageReference('/'+c.Id);

        if(c.OwnerId != oldOwnerId){
            try{
                Id templateId = [Select Id From EmailTemplate Where developerName = 'Notifica_Cambio_Titolare_Case_vf'].Id;            
                
                Database.DMLOptions options = new Database.DMLOptions();
                options.EmailHeader.triggerUserEmail = true;
                c.setOptions(options);
                update c;
                
                Messaging.Email[] emails = new Messaging.Email[]{};
                for(UserWrapper uw : users){
                    if(uw.selected){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTargetObjectId(uw.u.Id);
                        mail.setWhatId(c.Id);
                        mail.setTemplateId(templateId);
                        mail.setSaveAsActivity(false);
                        emails.add(mail);
                    }       
                }
                
                Messaging.sendEmail(emails);
                
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
                if(!Test.isRunningTest()) Database.rollback(sp);
                p = null;
                
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Per riassegnare il Caso è necessario modificare il titolare'));
            p = null;
        }
        return p;
    }
    
    public class UserWrapper{
        public User u {get;private set;}
        public Boolean selected {get;set;}
        public UserWrapper(User u){
            this.u = u;
        }
    }
}