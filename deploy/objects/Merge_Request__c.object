<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Error_Message__c</fullName>
        <externalId>false</externalId>
        <label>Error Message</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Master_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Master Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Merge Requests (Master)</relationshipLabel>
        <relationshipName>MasterMergeRequests</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Master_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Master Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Merge Requests (Master)</relationshipLabel>
        <relationshipName>MasterMergeRequests</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Master_Id__c</fullName>
        <description>Salesforce Id of the master record.</description>
        <externalId>false</externalId>
        <label>Master Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Master_SAP_Code__c</fullName>
        <description>Codice SAP del master record.</description>
        <externalId>false</externalId>
        <label>Codice SAP Master</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Master_Salesforce_Code__c</fullName>
        <description>Codice Salesforce del master record.</description>
        <externalId>false</externalId>
        <label>Codice Salesforce Master</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Slave_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Slave Account</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR (2 AND 3)</booleanFilter>
            <errorMessage>I campi Master Account e Slave Account non possono puntare allo stesso record.</errorMessage>
            <filterItems>
                <field>Account.Id</field>
                <operation>notEqual</operation>
                <valueField>$Source.Master_Account__c</valueField>
            </filterItems>
            <filterItems>
                <field>$Source.Master_Account__c</field>
                <operation>equals</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>Account.Id</field>
                <operation>equals</operation>
                <value></value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Merge Requests (Slave)</relationshipLabel>
        <relationshipName>SlaveMergeRequests</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Slave_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Slave Contact</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR (2 AND 3)</booleanFilter>
            <errorMessage>I campi Master Contact e Slave Contact non possono puntare allo stesso record.</errorMessage>
            <filterItems>
                <field>Contact.Id</field>
                <operation>notEqual</operation>
                <valueField>$Source.Master_Contact__c</valueField>
            </filterItems>
            <filterItems>
                <field>$Source.Master_Contact__c</field>
                <operation>equals</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>Contact.Id</field>
                <operation>equals</operation>
                <value></value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Merge Requests (Slave)</relationshipLabel>
        <relationshipName>SlaveMergeRequests</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Slave_Id__c</fullName>
        <description>Salesforce Id of the slave record.</description>
        <externalId>false</externalId>
        <label>Slave Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Slave_SAP_Code__c</fullName>
        <description>Codice SAP dello slave record.</description>
        <externalId>false</externalId>
        <label>Codice SAP Slave</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Slave_Salesforce_Code__c</fullName>
        <description>Codice Salesforce dello slave record.</description>
        <externalId>false</externalId>
        <label>Codice Salesforce Slave</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stato__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Status__c)</formula>
        <label>Stato</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
		<required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
		<valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Nuovo</fullName>
                    <default>true</default>
                    <label>Nuovo</label>
                </value>
				<value>
                    <fullName>Elaborato</fullName>
                    <default>false</default>
                    <label>Elaborato</label>
                </value>
				<value>
                    <fullName>Errore</fullName>
                    <default>false</default>
                    <label>Errore</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <gender>Feminine</gender>
    <label>Merge Request</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>RECORDTYPE</columns>
        <columns>Master_Account__c</columns>
        <columns>Slave_Account__c</columns>
        <columns>Master_Contact__c</columns>
        <columns>Slave_Contact__c</columns>
        <columns>Stato__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Merge_Account</fullName>
        <columns>NAME</columns>
        <columns>Master_Account__c</columns>
        <columns>Slave_Account__c</columns>
        <columns>Stato__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Merge_Request__c.Account</value>
        </filters>
        <label>Account</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Merge_Contact</fullName>
        <columns>NAME</columns>
        <columns>Master_Contact__c</columns>
        <columns>Slave_Contact__c</columns>
        <columns>Stato__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Merge_Request__c.Contact</value>
        </filters>
        <label>Contact</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>MR-{0000000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Merge Requests</pluralLabel>
    <recordTypes>
        <fullName>Account</fullName>
        <active>true</active>
        <label>Account</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Elaborato</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Errore</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nuovo</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Contact</fullName>
        <active>true</active>
        <label>Contact</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Elaborato</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Errore</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nuovo</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <customTabListAdditionalFields>Master_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Slave_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Master_Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Slave_Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stato__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>RECORDTYPE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Master_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Slave_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Master_Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Slave_Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Stato__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>RECORDTYPE</searchFilterFields>
        <searchFilterFields>Master_Account__c</searchFilterFields>
        <searchFilterFields>Slave_Account__c</searchFilterFields>
        <searchFilterFields>Master_Contact__c</searchFilterFields>
        <searchFilterFields>Slave_Contact__c</searchFilterFields>
        <searchFilterFields>Stato__c</searchFilterFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Master_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Slave_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Master_Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Slave_Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Stato__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Account_MR_Master_Validation</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Account che il Master Account sia valorizzato.</description>
        <errorConditionFormula>AND(
	ISNEW(),
	RecordType.DeveloperName = &apos;Account&apos;,
	ISPICKVAL(Status__c, &apos;Nuovo&apos;),
	ISBLANK(Master_Account__c)
)</errorConditionFormula>
        <errorDisplayField>Master_Account__c</errorDisplayField>
        <errorMessage>Campo necessario</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Account_MR_Slave_Validation</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Account che lo Slave Account sia valorizzato.</description>
        <errorConditionFormula>AND(
	ISNEW(),
	RecordType.DeveloperName = &apos;Account&apos;,
	ISPICKVAL(Status__c, &apos;Nuovo&apos;),
	ISBLANK(Slave_Account__c)
)</errorConditionFormula>
        <errorDisplayField>Slave_Account__c</errorDisplayField>
        <errorMessage>Campo necessario</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Account_MR_Supported_Fields</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Account che siano valorizzati solo i campi supportati.</description>
        <errorConditionFormula>AND(
	RecordType.DeveloperName = &apos;Account&apos;,
	OR(
		NOT(ISBLANK(Master_Contact__c)), 
		NOT(ISBLANK(Slave_Contact__c))
	)
)</errorConditionFormula>
        <errorMessage>I campi Master Contact e Slave Contact non sono supportati dal Record Type attuale.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Contact_MR_Master_Validation</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Contact che il Master Contact sia valorizzato.</description>
        <errorConditionFormula>AND(
	ISNEW(),
	RecordType.DeveloperName = &apos;Contact&apos;,
	ISPICKVAL(Status__c, &apos;Nuovo&apos;),
	ISBLANK(Master_Contact__c)
)</errorConditionFormula>
        <errorDisplayField>Master_Contact__c</errorDisplayField>
        <errorMessage>Campo necessario</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Contact_MR_Same_Account</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Contact che sia il Master Contact che lo Slave Contact appartengano allo stesso Account.</description>
        <errorConditionFormula>AND(
	RecordType.DeveloperName = &apos;Contact&apos;,
	NOT(ISBLANK(Master_Contact__c)),
	NOT(ISBLANK(Slave_Contact__c)),
	Master_Contact__r.AccountId  &lt;&gt; Slave_Contact__r.AccountId
)</errorConditionFormula>
        <errorMessage>Entrambi i contatti devono appartenere allo stesso account.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Contact_MR_Slave_Validation</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Contact che lo Slave Contact sia valorizzato.</description>
        <errorConditionFormula>AND(
	ISNEW(),
	RecordType.DeveloperName = &apos;Contact&apos;,
	ISPICKVAL(Status__c, &apos;Nuovo&apos;),
	ISBLANK(Slave_Contact__c)
)</errorConditionFormula>
        <errorDisplayField>Slave_Contact__c</errorDisplayField>
        <errorMessage>Campo necessario</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Contact_MR_Supported_Fields</fullName>
        <active>true</active>
        <description>Garantisce per un record di tipo Contact che siano valorizzati solo i campi supportati.</description>
        <errorConditionFormula>AND(
	RecordType.DeveloperName = &apos;Contact&apos;,
	OR(
		NOT(ISBLANK(Master_Account__c)), 
		NOT(ISBLANK(Slave_Account__c))
	)
)</errorConditionFormula>
        <errorMessage>I campi Master Account e Slave Account non sono supportati dal Record Type attuale.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Status_Validation</fullName>
        <active>true</active>
        <description>Garantisce che il campo Status sia sempre valorizzato</description>
        <errorConditionFormula>ISPICKVAL(Status__c, &apos;&apos;)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>Field cannot be empty.</errorMessage>
    </validationRules>
</CustomObject>
